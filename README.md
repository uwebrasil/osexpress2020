


> Written with [StackEdit](https://stackedit.io/).
> ## OsExpress - Cadastro de Clientes
> ***Login - Orçamentos - Clientes***
> abre um DataGridView dos Clientes já cadastrados
> que segue o padrao da navigação de OsExpress (Barra em cima)
> 
> **TODO:**
- a area de filtrar e copiado e não funciona, já que não existe 
  um campo ativo/desativado no banco.
 - o DataGridView contem  o campo "Código Sigposto" que não
  precisamos mais.

**Observação:**
tabelas:
OsCliente
OsClienteContato
relação 1 : n
ou seja, antes de podemos cadastrar os contatos de um cliente
precisamos cadastrar o cliente mesmo ( icone GRAVAR da navigação padrão)
depois podemos editar o cliente ( tecla "S" ou double clique no DatagridView) e fazer as operações CRUD quanto os contatos 
na aba Contatos 

**Fluxo de trabalho:**

 **1. Criar novo Cliente**
      Botao padrão INSERIR
      preencher todos campos
      e botão padrão GRAVAR
      campo OsCliente.Email falta nos imagens
      campo REPRESENTANTE LEGAL falta nos imagens
      GRAVAR volta pra DatagridView, ai para cadastrar contatos
      precisa-se escolher o cliente de novo (para ler o id gerado que
      funciona como chave estrangeiro em OsClienteContato!!) 
	  
 **2. Editar Cliente / CRUD Contatos**	  
      DataGridView Clientes - doubleClique Cliente ou "S" 
	  Campo Principal ?? criar no banco, tabela OsClienteContato ??
	  "Heberth: não precisa criar os campos novos agora, é so para futuro"
	  
	  "[14:03, 07/10/2020] Heberth: Todos os contatos podem ter e-mail"
	  "[14:03, 07/10/2020] Heberth: não precisa de e-mail na primeira tela (DADOS)"
	  ==> a primeira tela (DADOS) e tela do cliente, não e tela do contato, ai, 
	      precisamos o campo email la (consta na tabela do cliente)
	  
	  2 funções diferentes ...
	  principal/responsavel  :: email / cpf
	  pode ter principal que não é responsavel ...?
	  pode ter responsavel que não e principal
	  
	  Constraints:
	  principal precisa preencher email
	  principal so pode existir um
	  
	  responsavel precisa preencher CPF
	  responsavel so pode existir um