﻿namespace osExpress
{
    partial class SysPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SysPrincipal));
            this.staPrincipal = new System.Windows.Forms.StatusStrip();
            this.tosPosto = new System.Windows.Forms.ToolStripStatusLabel();
            this.tosUsuario = new System.Windows.Forms.ToolStripStatusLabel();
            this.tosData = new System.Windows.Forms.ToolStripStatusLabel();
            this.tosHora = new System.Windows.Forms.ToolStripStatusLabel();
            this.tmrDataHora = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSair = new System.Windows.Forms.Button();
            this.btn_troca_empresa = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnFlashDriver = new System.Windows.Forms.Button();
            this.btnIbametroX = new System.Windows.Forms.Button();
            this.btnIbametro = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnRede = new System.Windows.Forms.Button();
            this.btnProdutos = new System.Windows.Forms.Button();
            this.btnEvento = new System.Windows.Forms.Button();
            this.btnClientes = new System.Windows.Forms.Button();
            this.btnOrcamentos = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnOrcamento = new System.Windows.Forms.Button();
            this.panelConfig = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.btnUnidadeDeProduto = new System.Windows.Forms.Button();
            this.btnGrupoDeProduto = new System.Windows.Forms.Button();
            this.btnParametros = new System.Windows.Forms.Button();
            this.panelConfigLeft = new System.Windows.Forms.Panel();
            this.btnConfig = new System.Windows.Forms.Button();
            this.btnRelatorios = new System.Windows.Forms.Button();
            this.btnParametros_old = new System.Windows.Forms.Button();
            this.btnOrdemServico = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.a30CLIENTESTableAdapter1 = new osExpress.OsExpressDataSetTableAdapters.A30CLIENTESTableAdapter();
            this.flpEventos = new System.Windows.Forms.FlowLayoutPanel();
            this.picPrincipal = new System.Windows.Forms.PictureBox();
            this.btnFuncionario = new System.Windows.Forms.Button();
            this.staPrincipal.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panelConfig.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picPrincipal)).BeginInit();
            this.SuspendLayout();
            // 
            // staPrincipal
            // 
            this.staPrincipal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tosPosto,
            this.tosUsuario,
            this.tosData,
            this.tosHora});
            this.staPrincipal.Location = new System.Drawing.Point(0, 789);
            this.staPrincipal.Name = "staPrincipal";
            this.staPrincipal.Size = new System.Drawing.Size(794, 22);
            this.staPrincipal.TabIndex = 1;
            this.staPrincipal.Text = "statusStrip1";
            // 
            // tosPosto
            // 
            this.tosPosto.Name = "tosPosto";
            this.tosPosto.Size = new System.Drawing.Size(43, 17);
            this.tosPosto.Text = "Posto: ";
            // 
            // tosUsuario
            // 
            this.tosUsuario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.tosUsuario.Name = "tosUsuario";
            this.tosUsuario.Size = new System.Drawing.Size(50, 17);
            this.tosUsuario.Text = "Usuário:";
            // 
            // tosData
            // 
            this.tosData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.tosData.Name = "tosData";
            this.tosData.Size = new System.Drawing.Size(37, 17);
            this.tosData.Text = "Data: ";
            // 
            // tosHora
            // 
            this.tosHora.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.tosHora.Name = "tosHora";
            this.tosHora.Size = new System.Drawing.Size(39, 17);
            this.tosHora.Text = "Hora: ";
            // 
            // tmrDataHora
            // 
            this.tmrDataHora.Enabled = true;
            this.tmrDataHora.Interval = 1000;
            this.tmrDataHora.Tick += new System.EventHandler(this.tmrDataHora_Tick);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnSair);
            this.panel1.Controls.Add(this.btn_troca_empresa);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.btnFlashDriver);
            this.panel1.Controls.Add(this.btnIbametroX);
            this.panel1.Controls.Add(this.btnIbametro);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.btnOrcamento);
            this.panel1.Controls.Add(this.panelConfig);
            this.panel1.Controls.Add(this.btnConfig);
            this.panel1.Controls.Add(this.btnRelatorios);
            this.panel1.Controls.Add(this.btnParametros_old);
            this.panel1.Controls.Add(this.btnOrdemServico);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(165, 789);
            this.panel1.TabIndex = 3;
            // 
            // btnSair
            // 
            this.btnSair.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnSair.Image = global::osExpress.Properties.Resources.gohome;
            this.btnSair.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSair.Location = new System.Drawing.Point(0, 700);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(163, 35);
            this.btnSair.TabIndex = 26;
            this.btnSair.Text = "Sair";
            this.btnSair.UseVisualStyleBackColor = true;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // btn_troca_empresa
            // 
            this.btn_troca_empresa.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_troca_empresa.Image = global::osExpress.Properties.Resources.empresa;
            this.btn_troca_empresa.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_troca_empresa.Location = new System.Drawing.Point(0, 665);
            this.btn_troca_empresa.Name = "btn_troca_empresa";
            this.btn_troca_empresa.Size = new System.Drawing.Size(163, 35);
            this.btn_troca_empresa.TabIndex = 27;
            this.btn_troca_empresa.Text = "Troca de Empresa";
            this.btn_troca_empresa.UseVisualStyleBackColor = true;
            this.btn_troca_empresa.Visible = false;
            this.btn_troca_empresa.Click += new System.EventHandler(this.btn_troca_empresa_Click);
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button1.Image = global::osExpress.Properties.Resources.inbox;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(0, 630);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(163, 35);
            this.button1.TabIndex = 25;
            this.button1.Text = "Selos / Lacres";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnFlashDriver
            // 
            this.btnFlashDriver.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnFlashDriver.Image = global::osExpress.Properties.Resources.cd;
            this.btnFlashDriver.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFlashDriver.Location = new System.Drawing.Point(0, 595);
            this.btnFlashDriver.Name = "btnFlashDriver";
            this.btnFlashDriver.Size = new System.Drawing.Size(163, 35);
            this.btnFlashDriver.TabIndex = 23;
            this.btnFlashDriver.Text = "FlashDrive";
            this.btnFlashDriver.UseVisualStyleBackColor = true;
            this.btnFlashDriver.Visible = false;
            this.btnFlashDriver.Click += new System.EventHandler(this.btnFlashDriver_Click);
            // 
            // btnIbametroX
            // 
            this.btnIbametroX.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnIbametroX.Enabled = false;
            this.btnIbametroX.Image = global::osExpress.Properties.Resources.hardware;
            this.btnIbametroX.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIbametroX.Location = new System.Drawing.Point(0, 560);
            this.btnIbametroX.Name = "btnIbametroX";
            this.btnIbametroX.Size = new System.Drawing.Size(163, 35);
            this.btnIbametroX.TabIndex = 29;
            this.btnIbametroX.Text = "Ibametro X";
            this.btnIbametroX.UseVisualStyleBackColor = true;
            this.btnIbametroX.Visible = false;
            this.btnIbametroX.Click += new System.EventHandler(this.btnIbametroX_Click);
            // 
            // btnIbametro
            // 
            this.btnIbametro.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnIbametro.Image = global::osExpress.Properties.Resources.hardware;
            this.btnIbametro.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIbametro.Location = new System.Drawing.Point(0, 525);
            this.btnIbametro.Name = "btnIbametro";
            this.btnIbametro.Size = new System.Drawing.Size(163, 35);
            this.btnIbametro.TabIndex = 21;
            this.btnIbametro.Text = "Ibametro";
            this.btnIbametro.UseVisualStyleBackColor = true;
            this.btnIbametro.Click += new System.EventHandler(this.btnIbametro_Click);
            // 
            // button2
            // 
            this.button2.Dock = System.Windows.Forms.DockStyle.Top;
            this.button2.Enabled = false;
            this.button2.Image = global::osExpress.Properties.Resources.suport_F_online;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(0, 490);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(163, 35);
            this.button2.TabIndex = 19;
            this.button2.Text = "Treinamento";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnFuncionario);
            this.panel2.Controls.Add(this.btnRede);
            this.panel2.Controls.Add(this.btnProdutos);
            this.panel2.Controls.Add(this.btnEvento);
            this.panel2.Controls.Add(this.btnClientes);
            this.panel2.Controls.Add(this.btnOrcamentos);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 280);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(163, 210);
            this.panel2.TabIndex = 18;
            this.panel2.Visible = false;
            // 
            // btnRede
            // 
            this.btnRede.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRede.Image = global::osExpress.Properties.Resources.Organizational_Unit;
            this.btnRede.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRede.Location = new System.Drawing.Point(20, 140);
            this.btnRede.Name = "btnRede";
            this.btnRede.Size = new System.Drawing.Size(143, 35);
            this.btnRede.TabIndex = 21;
            this.btnRede.Text = "Rede";
            this.btnRede.UseVisualStyleBackColor = true;
            this.btnRede.Click += new System.EventHandler(this.btnRede_Click);
            // 
            // btnProdutos
            // 
            this.btnProdutos.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnProdutos.Image = global::osExpress.Properties.Resources.chart;
            this.btnProdutos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnProdutos.Location = new System.Drawing.Point(20, 105);
            this.btnProdutos.Name = "btnProdutos";
            this.btnProdutos.Size = new System.Drawing.Size(143, 35);
            this.btnProdutos.TabIndex = 20;
            this.btnProdutos.Text = "Produtos";
            this.btnProdutos.UseVisualStyleBackColor = true;
            this.btnProdutos.Click += new System.EventHandler(this.btnProdutos_Click);
            // 
            // btnEvento
            // 
            this.btnEvento.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnEvento.Image = global::osExpress.Properties.Resources.cascade;
            this.btnEvento.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEvento.Location = new System.Drawing.Point(20, 70);
            this.btnEvento.Name = "btnEvento";
            this.btnEvento.Size = new System.Drawing.Size(143, 35);
            this.btnEvento.TabIndex = 19;
            this.btnEvento.Text = "Eventos";
            this.btnEvento.UseVisualStyleBackColor = true;
            this.btnEvento.Click += new System.EventHandler(this.btnEvento_Click);
            // 
            // btnClientes
            // 
            this.btnClientes.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnClientes.Image = global::osExpress.Properties.Resources.users;
            this.btnClientes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClientes.Location = new System.Drawing.Point(20, 35);
            this.btnClientes.Name = "btnClientes";
            this.btnClientes.Size = new System.Drawing.Size(143, 35);
            this.btnClientes.TabIndex = 18;
            this.btnClientes.Text = "Clientes";
            this.btnClientes.UseVisualStyleBackColor = true;
            this.btnClientes.Click += new System.EventHandler(this.btnClientes_Click);
            // 
            // btnOrcamentos
            // 
            this.btnOrcamentos.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnOrcamentos.Image = global::osExpress.Properties.Resources.paste;
            this.btnOrcamentos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOrcamentos.Location = new System.Drawing.Point(20, 0);
            this.btnOrcamentos.Name = "btnOrcamentos";
            this.btnOrcamentos.Size = new System.Drawing.Size(143, 35);
            this.btnOrcamentos.TabIndex = 17;
            this.btnOrcamentos.Text = "Orçamentos e Pedidos";
            this.btnOrcamentos.UseVisualStyleBackColor = true;
            this.btnOrcamentos.Click += new System.EventHandler(this.btnOrcamentos_Click);
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(20, 210);
            this.panel3.TabIndex = 8;
            // 
            // btnOrcamento
            // 
            this.btnOrcamento.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnOrcamento.Image = global::osExpress.Properties.Resources.sale;
            this.btnOrcamento.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOrcamento.Location = new System.Drawing.Point(0, 245);
            this.btnOrcamento.Name = "btnOrcamento";
            this.btnOrcamento.Size = new System.Drawing.Size(163, 35);
            this.btnOrcamento.TabIndex = 12;
            this.btnOrcamento.Text = "CRM";
            this.btnOrcamento.UseVisualStyleBackColor = true;
            this.btnOrcamento.Click += new System.EventHandler(this.btnOrcamento_Click);
            // 
            // panelConfig
            // 
            this.panelConfig.Controls.Add(this.button3);
            this.panelConfig.Controls.Add(this.btnUnidadeDeProduto);
            this.panelConfig.Controls.Add(this.btnGrupoDeProduto);
            this.panelConfig.Controls.Add(this.btnParametros);
            this.panelConfig.Controls.Add(this.panelConfigLeft);
            this.panelConfig.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelConfig.Location = new System.Drawing.Point(0, 140);
            this.panelConfig.Name = "panelConfig";
            this.panelConfig.Size = new System.Drawing.Size(163, 105);
            this.panelConfig.TabIndex = 28;
            this.panelConfig.Visible = false;
            // 
            // button3
            // 
            this.button3.Dock = System.Windows.Forms.DockStyle.Top;
            this.button3.Image = global::osExpress.Properties.Resources.chart;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(20, 105);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(143, 35);
            this.button3.TabIndex = 20;
            this.button3.Text = "Not Visible";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Visible = false;
            // 
            // btnUnidadeDeProduto
            // 
            this.btnUnidadeDeProduto.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnUnidadeDeProduto.Image = global::osExpress.Properties.Resources.inbox;
            this.btnUnidadeDeProduto.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUnidadeDeProduto.Location = new System.Drawing.Point(20, 70);
            this.btnUnidadeDeProduto.Name = "btnUnidadeDeProduto";
            this.btnUnidadeDeProduto.Size = new System.Drawing.Size(143, 35);
            this.btnUnidadeDeProduto.TabIndex = 19;
            this.btnUnidadeDeProduto.Text = "Produto - Unidade";
            this.btnUnidadeDeProduto.UseVisualStyleBackColor = true;
            this.btnUnidadeDeProduto.Click += new System.EventHandler(this.btnUnidadeDeProduto_Click);
            // 
            // btnGrupoDeProduto
            // 
            this.btnGrupoDeProduto.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnGrupoDeProduto.Image = global::osExpress.Properties.Resources.group;
            this.btnGrupoDeProduto.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGrupoDeProduto.Location = new System.Drawing.Point(20, 35);
            this.btnGrupoDeProduto.Name = "btnGrupoDeProduto";
            this.btnGrupoDeProduto.Size = new System.Drawing.Size(143, 35);
            this.btnGrupoDeProduto.TabIndex = 18;
            this.btnGrupoDeProduto.Text = "Produto - Grupo";
            this.btnGrupoDeProduto.UseVisualStyleBackColor = true;
            this.btnGrupoDeProduto.Click += new System.EventHandler(this.btnGrupoDeProduto_Click);
            // 
            // btnParametros
            // 
            this.btnParametros.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnParametros.Image = global::osExpress.Properties.Resources.preferences;
            this.btnParametros.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnParametros.Location = new System.Drawing.Point(20, 0);
            this.btnParametros.Name = "btnParametros";
            this.btnParametros.Size = new System.Drawing.Size(143, 35);
            this.btnParametros.TabIndex = 17;
            this.btnParametros.Text = "Parâmetros";
            this.btnParametros.UseVisualStyleBackColor = true;
            this.btnParametros.Click += new System.EventHandler(this.parâmetrosDaOrdemDeServiçoToolStripMenuItem_Click);
            // 
            // panelConfigLeft
            // 
            this.panelConfigLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelConfigLeft.Location = new System.Drawing.Point(0, 0);
            this.panelConfigLeft.Name = "panelConfigLeft";
            this.panelConfigLeft.Size = new System.Drawing.Size(20, 105);
            this.panelConfigLeft.TabIndex = 8;
            // 
            // btnConfig
            // 
            this.btnConfig.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnConfig.Image = global::osExpress.Properties.Resources.organize;
            this.btnConfig.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfig.Location = new System.Drawing.Point(0, 105);
            this.btnConfig.Name = "btnConfig";
            this.btnConfig.Size = new System.Drawing.Size(163, 35);
            this.btnConfig.TabIndex = 13;
            this.btnConfig.Text = "Configurações";
            this.btnConfig.UseVisualStyleBackColor = true;
            this.btnConfig.Click += new System.EventHandler(this.btnConfig_Click);
            // 
            // btnRelatorios
            // 
            this.btnRelatorios.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRelatorios.Image = global::osExpress.Properties.Resources.statistics;
            this.btnRelatorios.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRelatorios.Location = new System.Drawing.Point(0, 70);
            this.btnRelatorios.Name = "btnRelatorios";
            this.btnRelatorios.Size = new System.Drawing.Size(163, 35);
            this.btnRelatorios.TabIndex = 6;
            this.btnRelatorios.Text = "Relatórios";
            this.btnRelatorios.UseVisualStyleBackColor = true;
            this.btnRelatorios.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnParametros_old
            // 
            this.btnParametros_old.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnParametros_old.Image = global::osExpress.Properties.Resources.preferences;
            this.btnParametros_old.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnParametros_old.Location = new System.Drawing.Point(0, 35);
            this.btnParametros_old.Name = "btnParametros_old";
            this.btnParametros_old.Size = new System.Drawing.Size(163, 35);
            this.btnParametros_old.TabIndex = 4;
            this.btnParametros_old.Text = "Parâmetros";
            this.btnParametros_old.UseVisualStyleBackColor = true;
            this.btnParametros_old.Visible = false;
            this.btnParametros_old.Click += new System.EventHandler(this.parâmetrosDaOrdemDeServiçoToolStripMenuItem_Click);
            // 
            // btnOrdemServico
            // 
            this.btnOrdemServico.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnOrdemServico.Image = global::osExpress.Properties.Resources.conference;
            this.btnOrdemServico.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOrdemServico.Location = new System.Drawing.Point(0, 0);
            this.btnOrdemServico.Name = "btnOrdemServico";
            this.btnOrdemServico.Size = new System.Drawing.Size(163, 35);
            this.btnOrdemServico.TabIndex = 5;
            this.btnOrdemServico.Text = "Ordens de Serviço";
            this.btnOrdemServico.UseVisualStyleBackColor = true;
            this.btnOrdemServico.Click += new System.EventHandler(this.ordemDeServiçoToolStripMenuItem_Click);
            // 
            // textBox1
            // 
            this.textBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textBox1.Enabled = false;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(165, 766);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(629, 23);
            this.textBox1.TabIndex = 6;
            this.textBox1.Text = " Lista dos Eventos\r\n";
            this.textBox1.Visible = false;
            this.textBox1.Click += new System.EventHandler(this.textBox1_Click);
            // 
            // textBox2
            // 
            this.textBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.textBox2.Location = new System.Drawing.Point(323, 100);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(1105, 300);
            this.textBox2.TabIndex = 9;
            this.textBox2.Text = resources.GetString("textBox2.Text");
            this.textBox2.Visible = false;
            this.textBox2.Click += new System.EventHandler(this.textBox2_Click);
            // 
            // a30CLIENTESTableAdapter1
            // 
            this.a30CLIENTESTableAdapter1.ClearBeforeFill = true;
            // 
            // flpEventos
            // 
            this.flpEventos.AutoScroll = true;
            this.flpEventos.Location = new System.Drawing.Point(325, 150);
            this.flpEventos.Name = "flpEventos";
            this.flpEventos.Size = new System.Drawing.Size(1100, 245);
            this.flpEventos.TabIndex = 11;
            // 
            // picPrincipal
            // 
            this.picPrincipal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picPrincipal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picPrincipal.Image = global::osExpress.Properties.Resources.logo;
            this.picPrincipal.Location = new System.Drawing.Point(0, 0);
            this.picPrincipal.Name = "picPrincipal";
            this.picPrincipal.Size = new System.Drawing.Size(794, 789);
            this.picPrincipal.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picPrincipal.TabIndex = 2;
            this.picPrincipal.TabStop = false;
            // 
            // btnFuncionario
            // 
            this.btnFuncionario.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnFuncionario.Image = global::osExpress.Properties.Resources.empresa;
            this.btnFuncionario.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFuncionario.Location = new System.Drawing.Point(20, 175);
            this.btnFuncionario.Name = "btnFuncionario";
            this.btnFuncionario.Size = new System.Drawing.Size(143, 35);
            this.btnFuncionario.TabIndex = 22;
            this.btnFuncionario.Text = "Funcionário";
            this.btnFuncionario.UseVisualStyleBackColor = true;
            this.btnFuncionario.Click += new System.EventHandler(this.btnFuncionario_Click);
            // 
            // SysPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 811);
            this.Controls.Add(this.flpEventos);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.picPrincipal);
            this.Controls.Add(this.staPrincipal);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "SysPrincipal";
            this.Text = "osExpress";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Shown += new System.EventHandler(this.SysPrincipal_Shown);
            this.staPrincipal.ResumeLayout(false);
            this.staPrincipal.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panelConfig.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picPrincipal)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip staPrincipal;
        private System.Windows.Forms.ToolStripStatusLabel tosUsuario;
        private System.Windows.Forms.ToolStripStatusLabel tosData;
        private System.Windows.Forms.ToolStripStatusLabel tosHora;
        private System.Windows.Forms.Timer tmrDataHora;
        private System.Windows.Forms.ToolStripStatusLabel tosPosto;
        private System.Windows.Forms.PictureBox picPrincipal;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnParametros_old;
        private System.Windows.Forms.Button btnOrdemServico;
        private System.Windows.Forms.Button btnRelatorios;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnOrcamento;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnClientes;
        private System.Windows.Forms.Button btnOrcamentos;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button btnIbametro;
        private System.Windows.Forms.Button btnFlashDriver;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnSair;
        private OsExpressDataSetTableAdapters.A30CLIENTESTableAdapter a30CLIENTESTableAdapter1;
        private System.Windows.Forms.Button btn_troca_empresa;
        private System.Windows.Forms.FlowLayoutPanel flpEventos;
        private System.Windows.Forms.Button btnEvento;
        private System.Windows.Forms.Button btnProdutos;
        private System.Windows.Forms.Button btnConfig;
        private System.Windows.Forms.Panel panelConfig;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btnUnidadeDeProduto;
        private System.Windows.Forms.Button btnGrupoDeProduto;
        private System.Windows.Forms.Button btnParametros;
        private System.Windows.Forms.Panel panelConfigLeft;
        private System.Windows.Forms.Button btnRede;
        private System.Windows.Forms.Button btnIbametroX;
        private System.Windows.Forms.Button btnFuncionario;
    }
}