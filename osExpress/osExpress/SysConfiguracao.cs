﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using osExpress.Helper;
using TecnoExpress.XML;

namespace osExpress
{
    public partial class SysConfiguracao : Form
    {
        private string ip;
        private string usuario;
        private string senha;
        private string instancia;
        private bool xmlEists;

        Conexao conexao = new Conexao();
        // Caminho do xml.
        private const string caminho = "Conexao.xml";
        // Versão do programa.
        private string versao;
        //
        private Boolean erro = false;

        public SysConfiguracao()
        {
            InitializeComponent();
        }

        private Boolean OpenSqlConnection(string connectionString)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    tsbStatus.Text = "Status: CONECTADO";
                    tsbStatus.ForeColor = Color.Green;
                    return true;
                }
                catch (Exception)
                {
                    tsbStatus.Text = "Status: SEM CONEXÃO";
                    tsbStatus.ForeColor = Color.Red;
                    return false;
                }
            }
        }

        private void validaVersao() 
        {
            // TODO: This line of code loads data into the 'osExpressDataSet.OsVersao' table. You can move, or remove it, as needed.
            this.osVersaoTableAdapter.Fill(this.osExpressDataSet.OsVersao);
        } 

        /// <summary>
        /// Load
        /// </summary>     
        private void SysConfiguracao_Load(object sender, EventArgs e)
        {
            versao = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

            // Obtém a versão do programa.
            this.Text = "osExpress v" + versao;
                       
            // Criar arquivo caso não exista
            if (File.Exists(caminho))
            {
                xmlEists = true;
                var c = new XmlManager<Conexao>().Load("Conexao.xml");
                txtEndereco.Text = c.ip;
                txtUsuarioConexao.Text = c.usuario;
                txtSenhaConexao.Text = MD5Crypt.Descriptografar(c.Senha);

                this.ip = c.ip;
                this.usuario = c.usuario;
                this.senha = MD5Crypt.Descriptografar(c.Senha);
                this.instancia = c.instancia;
                this.conexao.ip = c.ip;
                this.conexao.usuario = c.usuario;
                this.conexao.Senha = MD5Crypt.Descriptografar(c.Senha);
                this.conexao.instancia = c.instancia;                
            }
            else
            {
                conexao = new Conexao
                {
                    //ip = "192.168.25.254",
                    //ip = "25.96.50.85",
                    ip = "25.52.219.187",
                    usuario = "sa",
                    Senha = MD5Crypt.Criptografar("@tecno12hpa"),
                    instancia = "sigtecno"
                };
                new XmlManager<Conexao>().Save("Conexao.xml",conexao);

                this.ip = conexao.ip;
                this.usuario = conexao.usuario;
                this.senha = MD5Crypt.Descriptografar(conexao.Senha);
                this.instancia = conexao.instancia;               

                txtEndereco.Text = conexao.ip;
                txtUsuarioConexao.Text = conexao.usuario;               
            }
            
        }

        /// <summary>
        /// Aplicar
        /// </summary>       
        private void btnAplicar_Click(object sender, EventArgs e)
        {          
            try
            {
                if (!configurarConexao())
                {
                    return;
                };

                // Se o arquivo .xml existir edita o mesmo.
                //if (File.Exists(caminho))
                //{                   
                //    new XmlManager<Conexao>().Save("Conexao.xml", this.conexao);
                //}
                
                
                OsConfiguracoes.IpServidor = txtEndereco.Text;
                OsConfiguracoes.Instancia = "sigtecno";//txtEndereco.Text;
                OsConfiguracoes.Senha = this.senha;
                // Valida a versão do executável com a do banco de dados.
                validaVersao();

                bdsVersao.Filter = "VERSAO = '" + versao + "' AND STATUS = 1";
                if (bdsVersao.Count == 0)
                {
                    erro = true;
                    MessageBox.Show(
                        @"Atenção: Essa versão é invalida verifique seu email ou entre em contato com o setor de desenvolvimento.");
                    Close();
                }
                else
                {
                    Close();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace, ex.Message);
            }
        }
        
        /// <summary>
        /// Testar
        /// </summary>     
        private void btnTestar_Click(object sender, EventArgs e)
        {
            configurarConexao();
        }
        
        /// <summary>
        /// configurar
        /// </summary>
        /// <returns>bool</returns>
        private bool configurarConexao()
        {
            // Altera a string de conexão com o ip informado
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            //https:// social.msdn.microsoft.com/Forums/sqlserver/en-US/c3a09697-a980-4351-8f40-155a9bcfc939/how-to-hide-sql-server-password-from-user-in-an-windows-forms-application?forum=sqlsecurity
            /*
            ConnectionStringsSection section =
             config.GetSection("connectionStrings")
             as ConnectionStringsSection;
           */
           // if (!section.SectionInformation.IsProtected)
           // {
                // Encrypt the section.
             //   section.SectionInformation.ProtectSection(
               //   "DataProtectionConfigurationProvider");
               
               // section.SectionInformation.ForceSave = true;
               // config.Save(ConfigurationSaveMode.Modified);
                //config.Save(ConfigurationSaveMode.Full);
            //}


            //string newCnnStr = "Data Source=" + txtEndereco.Text + @"\azuresigtecno" + ";Initial Catalog=OsExpress;Persist Security Info=True;User ID=" + txtUsuarioConexao.Text + ";Password=" + txtSenhaConexao.Text;
            //string newCnnStr = "Data Source=" + txtEndereco.Text + @"\sigtecno" + ";Initial Catalog=OsExpress;Persist Security Info=True;User ID=" + txtUsuarioConexao.Text + ";Password=" + txtSenhaConexao.Text;
            string newCnnStr = "Data Source=" + txtEndereco.Text + @"\" + this.instancia 
                             + ";Initial Catalog=OsExpress;Persist Security Info=True;User ID="
                             + txtUsuarioConexao.Text + ";Password="
                             + txtSenhaConexao.Text;
            if (txtEndereco.Text.Equals("192.168.186.1"))
            {
                newCnnStr = "Data Source=" + txtEndereco.Text + @"\posto" 
                          + ";Initial Catalog=OsExpress;Persist Security Info=True;User ID=" 
                          + txtUsuarioConexao.Text + ";Password=" + txtSenhaConexao.Text;
                MessageBox.Show(newCnnStr,@"LOCAL");
            }

            config.ConnectionStrings.ConnectionStrings["osExpress.Properties.Settings.OsExpressConnectionString"].ConnectionString = newCnnStr;

            
            //config.Save(ConfigurationSaveMode.Modified, true);
            /* 
            if (!section.SectionInformation.IsProtected)
            {
                // Encrypt the section.
                section.SectionInformation.ProtectSection(
                  "DataProtectionConfigurationProvider");

                config.Save(ConfigurationSaveMode.Modified, true);

                //section.SectionInformation.ForceSave = true;
                //config.Save(ConfigurationSaveMode.Modified);
                //config.Save(ConfigurationSaveMode.Full);
            }
            */

            Properties.Settings.Default["OsExpressConnectionString"] = newCnnStr;

            if (!OpenSqlConnection(newCnnStr))
            {
                tsbStatus.Text = @"Status: Verifique a conexão antes de continuar.";
                return false;
            }
            else { 
                // schreibt in IDE OsExpress.vshost.exe.Config, sonst osExpress.exe.Config !!!
                config.Save(ConfigurationSaveMode.Modified, true);
                //string sectionName = "connectionStrings";
                //string sectionName = "ConnectionStringsSection";
                //ConfigurationManager.RefreshSection(sectionName);

                string sectionName = "connectionStrings";
                ConfigurationManager.RefreshSection(sectionName);

                // save conexao.xml
                //conexao.ip = txtEndereco.Text;
                Conexao conex = new Conexao
                {
                    ip = txtEndereco.Text,
                    usuario = "sa",
                    Senha = MD5Crypt.Criptografar("@tecno12hpa"),
                    instancia = "sigtecno"
                };
                new XmlManager<Conexao>().Save("Conexao.xml", conex);

                this.ip = conexao.ip;
                //this.usuario = conexao.usuario;
                //this.senha = MD5Crypt.Descriptografar(conexao.Senha);
                //this.instancia = conexao.instancia;

                txtEndereco.Text = conexao.ip;
                txtUsuarioConexao.Text = conexao.usuario;
           
            }
            return true;

        }

        private void SysConfiguracao_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (erro == true)
            {
                Application.Exit(); 
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void SysConfiguracao_KeyDown(object sender, KeyEventArgs e)
       {
            if (e.KeyCode == Keys.Enter)
            {
                btnAplicar.PerformClick();
            }
        }

        private void SysConfiguracao_Shown(object sender, EventArgs e)
        {
            if (xmlEists)
                ActiveControl = btnAplicar;
            else
                ActiveControl = txtSenhaConexao;
        }       
    }
}