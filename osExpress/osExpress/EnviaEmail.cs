﻿using System;
using System.Net.Mail;
using System.Windows.Forms;

namespace osExpress
{
    public class EnviaEmail
    {

        static System.Net.Mail.SmtpClient cliente;
        static MailMessage mensagem;

        // Configura o servidor de email.
        public EnviaEmail()
        {
            cliente = new System.Net.Mail.SmtpClient();
            
            //cliente.Host = "smtp.tecnoexpress.com.br";
            //cliente.Host = "email-ssl.com.br";
            cliente.Host = "mail.tecnoexpress.com.br";
            cliente.Port = 587;
            cliente.EnableSsl = false;
            cliente.Credentials = new System.Net.NetworkCredential("osexpress@tecnoexpress.com.br", "tecno2012");
        }

        public void enviarTecnoExpress(string numeroDaOS, string nomeFuncionario, string email, string tipo)
        {
            // Verifica qual o tipo de documento para o respectivo tipo de mensagem do email.
            string mensagem = "";

            if (tipo == "P")
            {
                mensagem = "O Pedido: ";
            }
            else
            {
                mensagem = "A Ordem de Serviço: ";
            }

            try
            {
                cliente.Send("osexpress@tecnoexpress.com.br",
                             email,
                             "Sistema OsExpress - Fechamento de OS.",
                             mensagem + numeroDaOS + " foi fechada pelo Funcionário: " + nomeFuncionario + ".");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro ao enviar email: "+ex.Message);
            }
        }

        public void enviarCliente(string emailCliente, string nomeDocumento, string nomeAnexo, string corpo)
        {
            // Configura os dados do email e envia para o cliente.
            try
            {
                mensagem = new MailMessage();
                mensagem.From = new MailAddress("osexpress@tecnoexpress.com.br", "osExpress");
                //mensagem.To.Add(new MailAddress(emailCliente, emailCliente));
                mensagem.To.Add(new MailAddress("erikcedraz@gmail.com", "erikcedraz@gmail.com"));

                mensagem.Subject =  "Sistema OsExpress - Fechamento de/da "+nomeDocumento+".";
                mensagem.Body = "Em anexo o/a " + nomeAnexo + ".\n " + corpo;
                mensagem.IsBodyHtml = false;
                mensagem.Priority = MailPriority.High;

                Attachment at = new Attachment(nomeAnexo);
                mensagem.Attachments.Add(at);

                cliente.Send(mensagem);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro ao enviar email: " + ex.Message);
            }

        }

    }
}
