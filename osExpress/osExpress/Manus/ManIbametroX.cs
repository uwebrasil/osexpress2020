﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using TExpress;

namespace osExpress.Manus
{
    public partial class ManIbametroX : osExpress.FrmPrincipal
    {
        int ultimoCliente = -1;
        Boolean inserindo = false;
        private Relatorios.RelIbametroCliente relIbametroCliente { get; set; }

        private BindingSource bs_empresa; 

        public ManIbametroX()
        {
            InitializeComponent();          
            string f = OsClienteBindingSource.Filter;           

            #region Empresa
            // BindingSource bs_empresa = new BindingSource();
            var sql_empresa = @"SELECT id, razao, codigo FROM A30POSTOS";
            var empresa = DB.GetTableFromSQL(sql_empresa);
            bs_empresa = new BindingSource();
            bs_empresa.DataSource = empresa;
            bs_empresa.Sort = "codigo";


            cmbEmpresa.DataSource = null;
            cmbEmpresa.DataSource = bs_empresa;
            cmbEmpresa.DisplayMember = "razao";
            cmbEmpresa.ValueMember = "codigo";

            cmbEmpresa.DataBindings.Add(
                new Binding("SelectedValue", bs_empresa, "codigo", true));

            cmbEmpresa.SelectedIndex = OsConfiguracoes.idPosto -1;
            #endregion
        }

        private void estadoInicial()
        {
            // *** Controles *** //

            tsbInserir.Enabled = true;
            tsbGravar.Enabled = false; 

            cmbCodigoCliente.SelectedIndex = -1;
            cmbNomeCliente.SelectedIndex = -1;
            //cmbMarcaInstrumento.SelectedIndex = -1;
            //osIbametroBindingSource.Filter = "ID = 0";
            //osIbametroInmetroBindingSource.Filter = "IDIBAMETRO = 0";
            OsIbametroOsClienteBindingSource.Filter = "ID = 0";
            OsIbametroInmetroOsClienteBindingSource.Filter = "IDIBAMETRO = 0";
            pesquisar();
            cmbNomeCliente.Focus();
        }

        private void ManIbametro_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dtsPrincipal.OsCliente' table. You can move, or remove it, as needed.
            this.osClienteTableAdapter.Fill(this.dtsPrincipal.OsCliente);           
            // TODO: This line of code loads data into the 'dtsPrincipal.OsIbametroInmetroOsCliente' table. You can move, or remove it, as needed.
            this.osIbametroInmetroOsClienteTableAdapter.Fill(this.dtsPrincipal.OsIbametroInmetroOsCliente);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsIbametroOsCliente' table. You can move, or remove it, as needed.
            this.osIbametroOsClienteTableAdapter.Fill(this.dtsPrincipal.OsIbametroOsCliente);
          
            int w = (int)(this.Parent.ClientSize.Width * 0.9);
            int h = (int)(this.Parent.ClientSize.Height * 0.9);
            this.Size = new Size(w, h);
            int x = (int)(this.Parent.ClientSize.Width - w) / 2;
            int y = (int)(this.Parent.ClientSize.Height - h) / 2;
            this.Location = new Point(x, y);

            //// TODO: This line of code loads data into the 'dtsPrincipal.A30CLIENTES' table. You can move, or remove it, as needed.
            //this.a30CLIENTESTableAdapter.Fill(this.dtsPrincipal.A30CLIENTES);
            //// TODO: This line of code loads data into the 'dtsPrincipal.A30CLIENTES' table. You can move, or remove it, as needed.
            //this.a30CLIENTESTableAdapter.Fill(this.dtsPrincipal.A30CLIENTES);
            //// TODO: This line of code loads data into the 'dtsPrincipal.OsIbametroInmetro' table. You can move, or remove it, as needed.
            //this.osIbametroInmetroTableAdapter.Fill(this.dtsPrincipal.OsIbametroInmetro);
            //// TODO: This line of code loads data into the 'dtsPrincipal.OsIbametro' table. You can move, or remove it, as needed.
            //this.osIbametroTableAdapter.Fill(this.dtsPrincipal.OsIbametro);
            //
            //this.WindowState = FormWindowState.Maximized;

            estadoInicial();

            //osIbametroInmetroBindingSource.Filter = "IDIBAMETRO = 0";
            OsIbametroInmetroOsClienteBindingSource.Filter = "IDIBAMETRO = 0";

            //pesquisar();
        }

        private void tsbGravar_Click(object sender, EventArgs e)
        {

           // osIbametroBindingSource.CancelEdit();
            var debug = 1;
            OsIbametroOsClienteBindingSource.CancelEdit();

            // *** Controles *** //
            tsbInserir.Enabled = true;
            tsbGravar.Enabled = false;
            tsbPrimeiro.Enabled = true;
            tsbAnterior.Enabled = true;
            tsbProximo.Enabled = true;
            tsbUltimo.Enabled = true;
            // ***************** //

            cmbCodigoCliente.SelectedIndex = ultimoCliente;
            cmbNomeCliente.SelectedIndex = ultimoCliente;
            estadoCorrente = EstadoManutencao.smPesquisar;
            try
            {
                this.Validate();
                //this.osIbametroBindingSource.EndEdit();
                //this.osIbametroInmetroBindingSource.EndEdit();
                //this.tableAdapterManager.UpdateAll(this.dtsPrincipal);
                this.OsIbametroOsClienteBindingSource.EndEdit();
                this.OsIbametroInmetroOsClienteBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(this.dtsPrincipal);
                cmbNomeCliente.Enabled = true;
                cmbCodigoCliente.Enabled = true;
            }
            catch (Exception)
            {
                MessageBox.Show("ERRO: Você tentou gravar sem informar um Número de Série");
                cancelar();
            }
        }

        private void tsbInserir_Click(object sender, EventArgs e)
        {
            inserir();

            tsbGravar.Enabled = true;

            cmbMarcaInstrumento.Focus();

            cmbCodigoCliente.SelectedIndex = ultimoCliente;
            cmbNomeCliente.SelectedIndex = ultimoCliente;
            cmbNomeCliente.Enabled = false;
            cmbCodigoCliente.Enabled = false;

            inserindo = true;

            tsbInserir.Enabled = false;
        }

        private void ManIbametro_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }

        private void btnInserirMecanico_Enter(object sender, EventArgs e)
        {
            inserindo = false;
            tsbInserir.Enabled = true;
            //tsbGravar.PerformClick();

            // DataRowView current = (DataRowView)osIbametroBindingSource.Current;
            DataRowView current = (DataRowView)OsIbametroOsClienteBindingSource.Current;
            if (estadoCorrente == EstadoManutencao.smInserir)
            {
                current["MARCA_INSTRUMENTO"] = cmbMarcaInstrumento.Text;
            }

            tsbInserir.PerformClick();
            cmbMarcaInstrumento.Focus();
        }

        private void comboBox6_SelectedValueChanged(object sender, EventArgs e)
        {
            if (/*cmbNomeCliente.Text.Length > 0 ||*/ cmbCodigoCliente.Text.Length > 0)
            {
                ultimoCliente = cmbCodigoCliente.SelectedIndex;
                ultimoCliente = cmbNomeCliente.SelectedIndex;
                try
                {
                    if (estadoCorrente == EstadoManutencao.smInserir)
                    {
                       // osIbametroBindingSource.Filter = "IDCLIENTE = " + cmbCodigoCliente.SelectedValue.ToString() + " AND ID > " + osIbametroTableAdapter.SqlUltimoIbametroCadastrado(Int32.Parse(cmbNomeCliente.SelectedValue.ToString()));
                        OsIbametroOsClienteBindingSource.Filter = 
                            "IDCLIENTE = " + cmbCodigoCliente.SelectedValue.ToString() 
                        + " AND ID > " + osIbametroOsClienteTableAdapter.SqlUltimoIbametroCadastradoOsCliente(Int32.Parse(cmbNomeCliente.SelectedValue.ToString()));

                    }
                    else
                    {
                        OsIbametroOsClienteBindingSource.Filter = "IDCLIENTE = " + cmbCodigoCliente.SelectedValue.ToString();
                       // osIbametroBindingSource.Filter = "IDCLIENTE = " + cmbCodigoCliente.SelectedValue.ToString();
                        System.Diagnostics.Debug.WriteLine("X: " + OsIbametroOsClienteBindingSource.Filter);
                    }
                }
                catch (Exception)
                {
                    System.Diagnostics.Debug.WriteLine("X: ERROR");
                }

                if (ultimoCliente >= 0)
                {
                    cmbCodigoCliente.SelectedIndex = ultimoCliente;
                    cmbNomeCliente.SelectedIndex = ultimoCliente;

                }
            }
         }

        private void tsbPrimeiro_Click(object sender, EventArgs e)
        {
            cancelarAlteracoes();
            //a30CLIENTESBindingSource.MoveFirst();
            OsClienteBindingSource.MoveFirst();
        }

        private void tsbAnterior_Click(object sender, EventArgs e)
        {
            cancelarAlteracoes();
            OsClienteBindingSource.MovePrevious();
        }

        private void tsbProximo_Click(object sender, EventArgs e)
        {
            cancelarAlteracoes();
            OsClienteBindingSource.MoveNext();
        }

        private void tsbUltimo_Click(object sender, EventArgs e)
        {
            cancelarAlteracoes();
            OsClienteBindingSource.MoveLast();
        }

        private void btnInserirINMETRO_Enter(object sender, EventArgs e)
        {

                if (txtCodigoIBAMETRO.Text.Length > 0)
                {
                    //DataRowView current = (DataRowView)osIbametroInmetroBindingSource.AddNew();
                    DataRowView current = (DataRowView)OsIbametroInmetroOsClienteBindingSource.AddNew();

                    current["IDIBAMETRO"] = txtCodigoIBAMETRO.Text;
                    current["NUM_INMETRO"] = txtNumINMETRO.Text;

                    this.Validate();
                    //this.osIbametroInmetroBindingSource.EndEdit();
                    this.OsIbametroInmetroOsClienteBindingSource.EndEdit();

                    txtNumINMETRO.Clear();
                    txtNumINMETRO.Focus();
                    tsbGravar.Enabled = true;
                }
                else
                {
                    MessageBox.Show("Informe um cliente e uma bomba para adicionar o número do INMETRO.");
                }

        }

        private void osIbametroDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (estadoCorrente != EstadoManutencao.smInserir)
            {
                filtraINMETRO();  
            }
        }

        private void osIbametroDataGridView_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            tsbGravar.Enabled = true;
            excluir();
        }

        private void osIbametroDataGridView_KeyUp(object sender, KeyEventArgs e)
        {
            filtraINMETRO();
        }

        private void tsbExcluir_Click(object sender, EventArgs e)
        {
            excluir();
        }

        private void cancelar()
        {
            cmbNomeCliente.Enabled = true;
            cmbCodigoCliente.Enabled = true;
            //osIbametroBindingSource.CancelEdit();
            //osIbametroInmetroBindingSource.CancelEdit();
            OsIbametroOsClienteBindingSource.CancelEdit();
            OsIbametroInmetroOsClienteBindingSource.CancelEdit();

            dtsPrincipal.RejectChanges();
            pesquisar();
            tsbInserir.Enabled = true;
            tsbGravar.Enabled = false;
        }

        private void cancelarAlteracoes()
        {
            if (estadoCorrente == EstadoManutencao.smInserir ||
                estadoCorrente == EstadoManutencao.smExcluir ||
                estadoCorrente == EstadoManutencao.smEditar)
            {
                if (MessageBox.Show("Deseja abandonar sem gravar?",
                    null,
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    cancelar();
                }
            }
        }

        private void filtraINMETRO()
        { 
            if (txtCodigoIBAMETRO.Text.Length > 0)
            {
                //osIbametroInmetroBindingSource.Filter = "IDIBAMETRO = " + txtCodigoIBAMETRO.Text;
                OsIbametroInmetroOsClienteBindingSource.Filter = "IDIBAMETRO = " + txtCodigoIBAMETRO.Text;
            }
            else
            {
                //osIbametroInmetroBindingSource.Filter = "IDIBAMETRO = 0";
                OsIbametroInmetroOsClienteBindingSource.Filter = "IDIBAMETRO = 0";
            }
        }

        private void osIbametroDataGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            cancelar();
        }

        private void osIbametroDataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            tsbInserir.Enabled = false;
            tsbGravar.Enabled = true;
            editar();
        }

        private void osIbametroInmetroDataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            tsbInserir.Enabled = false;
            tsbGravar.Enabled = true;
            editar();
        }
        
        private void osIbametroBindingSource_BindingComplete(object sender, BindingCompleteEventArgs e)
        {
            filtraINMETRO();
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void bindingNavigator_RefreshItems(object sender, EventArgs e)
        {

        }

        private void tsbImprimir_Click(object sender, EventArgs e)
        {
            relIbametroCliente = new Relatorios.RelIbametroCliente();

            relIbametroCliente.configuraOS(cmbCodigoCliente.SelectedValue.ToString());

            relIbametroCliente.Show();
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {
                            
        }

        private void cmbEmpresa_SelectionChangeCommitted(object sender, EventArgs e)
        {
            DataRowView currentEmp = (DataRowView)bs_empresa.Current;

            OsConfiguracoes.idPosto = (int)((DataRowView)cmbEmpresa.SelectedItem)["id"];

            OsConfiguracoes.nomePosto = ((DataRowView)cmbEmpresa.SelectedItem)["razao"].ToString();

            OsConfiguracoes.codigoPosto = ((DataRowView)cmbEmpresa.SelectedItem)["codigo"].ToString();
        }

        private void osIbametroDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

       
    }
}
