﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace osExpress.Helper
{
    public class MyReflection
    {
        /// <summary>
        /// Get a Form instance if open
        /// </summary>
        /// <param name="name">string name of form</param>
        /// <returns>Form instance or null</returns>
        public static Form GetForm(string name)
        {
            Form form = null;
            FormCollection fc = Application.OpenForms;
            foreach (Form frm in fc)
            {
                if (frm.Name.Equals(name))
                {
                    form = frm;
                }
            }
            // null if not found
            return form; 
        }
    }
}
