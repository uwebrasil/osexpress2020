﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using TExpress;
using System.Data;

namespace osExpress
{
    public partial class SysPrincipal : Form
    {
        private SysLogin sysLogin;
        // Abre os formulários em MDI.
        // teste
        public void OpenForm(Type frmType)
        {
            bool bolCtl = false;
            
            picPrincipal.Visible = false;

            foreach (Form form in Application.OpenForms)
            {
                if (form.GetType().Equals(frmType))
                {
                    form.MdiParent = this;
                    form.Show();
                    form.Focus();
                    bolCtl = true;
                    break;
                }
            }

            if (!bolCtl)
            {
                Form frm = (Form)Activator.CreateInstance(frmType);
                frm.MdiParent = this;
                frm.Show();
                frm.Focus();
            }
        }

        public SysPrincipal()
        {
            InitializeComponent();            
        }

        private void sairToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnSair.PerformClick();
        }

        private void tmrDataHora_Tick(object sender, EventArgs e)
        {
            // Atualiza data e hora do statusTrip.
            tosData.Text = "Data: " + DateTime.Now.ToLongDateString();
            tosHora.Text = "Hora: "+DateTime.Now.ToLongTimeString();
            tosPosto.Text = "Posto: "+OsConfiguracoes.idPosto + " - " + OsConfiguracoes.nomePosto;
            tosUsuario.Text = "Usuário: "+OsConfiguracoes.codigoUsuario + " - " + OsConfiguracoes.nomeUsuario;
        }


        private void CheckBoxCheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox) sender;
            string id = cb.Tag.ToString();
            string A = cb.Checked ? "S" : "N";
            DB.ConnectionName = "osExpress.Properties.Settings.OsExpressConnectionString";
            string sql_update = @"UPDATE OsClienteEvento SET alertar='" + A + "' WHERE id=" + id;
            DB.ExecuteNonQuery(sql_update);
            // MessageBox.Show(@"Alertar alterado com sucesso!");
            
        }       
          
        private void dynamicDTP_ValueChanged(object sender, EventArgs e)  
        {             
            DateTimePicker dtp = (DateTimePicker)sender;
            string id = dtp.Tag.ToString();
            // DateTime value = ((DateTimePicker)sender).Value;
            string dt_value = dtp.Value.ToString();
            DB.ConnectionName = "osExpress.Properties.Settings.OsExpressConnectionString";
            string sql_update = @"UPDATE OsClienteEvento SET datahora_realizacao='" + dt_value + "' WHERE id=" + id;
            DB.ExecuteNonQuery(sql_update);
            //MessageBox.Show(@"Data Realização alterado com sucesso!");
          
        }
        
        private void SysPrincipal_Shown(object sender, EventArgs e)
        {
            // Quando essa tela for exibida, exibe em seguida a tela de Login.
            //SysLogin sysLogin = new SysLogin();
            //DB.ConnectionName = "osExpress.Properties.Settings.OsExpressConnectionString";
            sysLogin = new SysLogin();
            sysLogin.ShowDialog();

            textBox2.Visible = true;
            
            DB.ConnectionName = "osExpress.Properties.Settings.OsExpressConnectionString";

            //string sql_eventos_count = @"SELECT count(*) anz FROM OsClienteEvento WHERE 1=1 AND alertar = 'S'";

            string sql_eventos_count =
                @"SELECT count(*) anz FROM OsClienteEvento WHERE 1=1 AND alertar = 'S'
                and convert(date,datahora_realizacao) = convert(date,getdate())";

            int cnt = (int)DB.GetSingleValue(sql_eventos_count);
            
            if (cnt > 0)
                textBox2.Visible = true;
            else
            {
                textBox2.Visible = false;
                textBox1.Visible = false;
                flpEventos.Visible = false;
                return;
            }
            

            textBox2.Text = cnt.ToString() + " EVENTOS  .  CLIQUE AQUI PARA FECHAR";
            textBox2.Text += Environment.NewLine;
            textBox2.Text += "Alterações dos campos <alertar> e <data da realização> são gravadas imediatamente no banco.";
            textBox2.Text += Environment.NewLine;
            textBox2.Text += "Por enquanto são representadas na lista dos eventos depois do próximo login. ";
           
           
           
            string sql_eventos = @"select oce.id, id_cliente, razao_social,  
                                     FORMAT(datahora_realizacao,'dd/MM/yyyy HH:mm') datahora,
                                     alertar, apontamento,
                                     CASE WHEN len(apontamento) < 20 
                                     THEN apontamento
                                     ELSE substring(apontamento,1,20)
                                     END
                                     title
                                     from OsClienteEvento oce, OsCliente  oc  
                                     WHERE 1=1
                                     AND oce.id_cliente = oc.id
                                     AND oce.alertar = 'S'
                                     -- AND datahora_realizacao > GETDATE()
                                     and convert(date,datahora_realizacao) = convert(date,getdate())
                                     ORDER BY datahora_realizacao ";
            
            DataTable dt = DB.GetTableFromSQL(sql_eventos);

            TextBox alertar = new TextBox();
            alertar.Text = "Alertar";
            alertar.Width = 45;
            alertar.Enabled = false;
            flpEventos.Controls.Add(alertar);
            TextBox realizacao = new TextBox();
            realizacao.Text = "Data da Realização";
            realizacao.Width = 150;
            realizacao.Enabled = false;
            flpEventos.Controls.Add(realizacao);
            TextBox cliente = new TextBox();
            cliente.Text = "Cliente";
            cliente.Width = 300;
            cliente.Enabled = false;
            flpEventos.Controls.Add(cliente);
            TextBox evento = new TextBox();
            evento.Text = "Evento";
            evento.Width = 450;
            evento.Enabled = false;
            flpEventos.Controls.Add(evento);
            flpEventos.SetFlowBreak(evento, true); 

            foreach (DataRow row in dt.Rows)
            {
                // Checkbox alertar
                CheckBox dynamicCheckBox = new CheckBox();                
                dynamicCheckBox.Width = 45;
                dynamicCheckBox.Height = 30;
                //dynamicCheckBox.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
                //dynamicCheckBox.Text = "";                
                dynamicCheckBox.Tag = row["id"].ToString();
                flpEventos.Controls.Add(dynamicCheckBox);
                dynamicCheckBox.Margin = new Padding(5,0,0,8);
                dynamicCheckBox.Checked = true;
                dynamicCheckBox.CheckedChanged += new System.EventHandler(CheckBoxCheckedChanged);                             

                // DatetimePicker
                DateTimePicker dynamicDTP = new DateTimePicker();
                //dynamicDTP.Name = "DynmicDateTimePicker";
                flpEventos.Controls.Add(dynamicDTP);
                dynamicDTP.Size = new System.Drawing.Size(150, 25);
                dynamicDTP.Tag = row["id"].ToString();

                dynamicDTP.Format = DateTimePickerFormat.Custom;
                dynamicDTP.CustomFormat = "dd/MM/yyyy HH:mm";
                DateTime dx = Convert.ToDateTime(row["datahora"].ToString());
                dynamicDTP.Value = dx;

                dynamicDTP.ValueChanged += new System.EventHandler(dynamicDTP_ValueChanged);  
               
                // Button
                Button btn1 = new Button();               
                //String title = row["datahora"].ToString() + " - " + row["razao_social"].ToString();
                String title = row["razao_social"].ToString();
                btn1.Text = title;
                btn1.Height = 30;
              
                //btn1.AutoSize = true;
                //btn1.AutoSizeMode = AutoSizeMode.GrowAndShrink;

                btn1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
                btn1.Width = 300;
                 
                btn1.Tag = new { id_cliente = Convert.ToInt32(row["id_cliente"]), id = Convert.ToInt32(row["id"]) };
                btn1.Click += new EventHandler(btn_click);

                flpEventos.Controls.Add(btn1);
                TextBox tb = new TextBox();
                tb.Text = row["apontamento"].ToString();
                tb.Width = 450;
                tb.Height = 30;
                tb.Font = new System.Drawing.Font(tb.Font.FontFamily, 14);
                tb.Enabled = false;
                flpEventos.Controls.Add(tb);
                flpEventos.SetFlowBreak(tb, true);              
            }
           
        }
        
        private void btn_click(object sender, EventArgs e)
        {
           
            flpEventos.Visible = false;
            textBox2.Visible = false;           

            // abrir FORM
            OpenForm(typeof(Manutencoes.ManCliente));
            // obter instância de FORM "ManCliente"
            Form form = null;
            FormCollection fc = Application.OpenForms;
            foreach (Form frm in fc)
            {
                if (frm.Name.Equals("ManCliente"))
                {
                    form = frm;                   
                }
            }
            // obter referencia á DataGridView
            DataGridView grid = form.Controls.Find("osOrdemServicoDataGridView", true).FirstOrDefault() as DataGridView;
           
            int rowIndex = -1;
          
            var button = ((Button)sender);
            int id_cliente=((dynamic)button.Tag).id_cliente;
            int id = ((dynamic)button.Tag).id;

            foreach (DataGridViewRow row in grid.Rows)
            {               
                if ( (int)row.Cells[0].Value == id_cliente)
                {
                    rowIndex = row.Index;
                    break;
                }
            }
            grid.Rows[rowIndex].Selected = true;
            
            // acionar DoubleClick on DataGridView via Reflection
            MethodInfo onCellDoubleClick = typeof(DataGridView).GetMethod("OnCellDoubleClick",
                                                                BindingFlags.NonPublic | BindingFlags.Instance);          
            DataGridViewCellEventArgs ea = new DataGridViewCellEventArgs(0, rowIndex);           
            onCellDoubleClick.Invoke(grid, new object[] { ea });

            // select Tab Eventos
            TabControl tabc = form.Controls.Find("TabChild", true).FirstOrDefault() as TabControl;
            tabc.SelectedTab = tabc.TabPages["tabEventos"];
        }

        private void ordemDeServiçoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Verifica se o usuário tem permissão pra abrir essa tela.
            if ((OsConfiguracoes.PlanoConta) != "")
            {                
                OpenForm(typeof(Manutencoes.ManOrdemServico));
            }
            else
                MessageBox.Show("ATENÇÃO: O Usuário precisa configurar o Plano de Conta!");
        }

        private void sobreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Sistema de Cadastro de OS\n"+
                            "Desenvolvido pela Tecnoexpress - 2012 .. 2013\n"+
                            "Todos os Direitos Reservados.\n");
        }

        private void parâmetrosDaOrdemDeServiçoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenForm(typeof(Manutencoes.ManParOS));
        }

        private void relatórioSDeOrdensDeServiçoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenForm(typeof(RelOrdem));
        }

        private void button3_Click(object sender, EventArgs e)
        {
           //OpenForm(typeof(Relatorios.RelRelatorios));
           // MessageBox.Show("Trabalho em andamento ...");
           OpenForm(typeof(Relatorios.RelPanel));
        }

        private void btnOrcamento_Click(object sender, EventArgs e)
        {
            // Abre o panel que contém o cadastro de cliente e orçamento.
            if (panel2.Visible == true)
                panel2.Visible = false;
            else
                panel2.Visible = true;
        }

        private void btnClientes_Click(object sender, EventArgs e)
        {
            if (OsConfiguracoes.acessaPedido == true)
            {
                OpenForm(typeof(Manutencoes.ManCliente));
            }
            else
            {
                MessageBox.Show("ATENÇÃO: Você não tem acesso a essa manutenção!");
            } 
        }

        private void btnEvento_Click(object sender, EventArgs e)
        {
            if (OsConfiguracoes.acessaPedido == true)
            {
                OpenForm(typeof(Manutencoes.ManEvento));
            }
            else
            {
                MessageBox.Show("ATENÇÃO: Você não tem acesso a essa manutenção!");
            } 

        }
        private void btnProdutos_Click(object sender, EventArgs e)
        {
            if (OsConfiguracoes.acessaPedido == true)
            {
                OpenForm(typeof(Manutencoes.ManProduto));
            }
            else
            {
                MessageBox.Show("ATENÇÃO: Você não tem acesso a essa manutenção!");
            } 
        }
        private void btnOrcamentos_Click(object sender, EventArgs e)
        {
            if (OsConfiguracoes.acessaPedido == true)
            {
                //if (MessageBox.Show("Deseja cadastrar um novo cliente?",
                //    null,
                //    MessageBoxButtons.YesNo,
                //    MessageBoxIcon.Question) == DialogResult.Yes)
                //    btnClientes.PerformClick();
                //else
                    OpenForm(typeof(Manutencoes.ManPedido));
            }
            else
            {
                MessageBox.Show("ATENÇÃO: Você não tem acesso a essa manutenção!");
            }
        }

        private void btnRede_Click(object sender, EventArgs e)
        {
            if (OsConfiguracoes.acessaPedido == true)
            {
                //Application.UseWaitCursor = true;
                //OpenForm(typeof(Manus.ManPedidoX));
                OpenForm(typeof(Manutencoes.ManRede));
            }
            else
            {
                MessageBox.Show("ATENÇÃO: Você não tem acesso a essa manutenção!");
            }
           
        }
        private void btnFuncionario_Click(object sender, EventArgs e)
        {
            if (OsConfiguracoes.acessaPedido == true)
            {
                OpenForm(typeof(Manutencoes.ManFuncionario));
            }
            else
            {
                MessageBox.Show("ATENÇÃO: Você não tem acesso a essa manutenção!");
            }
        }   
        private void textBox1_Click(object sender, EventArgs e)
        {
            // Campo de atualização.
            if (textBox2.Visible)
            {
                textBox2.Visible = false;
                flpEventos.Visible = false;
            }
            else
            {
                textBox2.Visible = true;
                flpEventos.Visible = true;
            }
        }

        private void textBox2_Click(object sender, EventArgs e)
        {
            flpEventos.Visible = false;
            textBox2.Visible = false;
            textBox2.Enabled = false;            
        }

        private void btnIbametro_Click(object sender, EventArgs e)
        {
           /* if (OsConfiguracoes.acessaPedido == true)*/
            {
                OpenForm(typeof(Manutencoes.ManIbametro));
            }
          /*  else
            {
                MessageBox.Show("ATENÇÃO: Você não tem acesso a essa manutenção!");
            } */
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Deseja Realmente Sair?",
                    null,
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void btnFlashDriver_Click(object sender, EventArgs e)
        {
            /*Manutencoes.ManFlashDrive manFlashDrive = new Manutencoes.ManFlashDrive();
            manFlashDrive.ShowDialog();*/

            OpenForm(typeof(Manutencoes.ManFlashDrive));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenForm(typeof(Manutencoes.ManEtiqueta));
        }
        /// <summary>
        /// Habilita a troca dos credenciais de Login.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btn_troca_empresa_Click(object sender, EventArgs e)
        {                      
            // close all open MDI childs
            MdiChildren.ToList().ForEach(x => x.Close());
            // show Login dialog
            // sysLogin = new SysLogin();
            sysLogin.ShowDialog();              
        }

        private void btnConfig_Click(object sender, EventArgs e)
        {            
            panelConfig.Visible = !panelConfig.Visible;
        }

        private void btnUnidadeDeProduto_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Funcionalidade ainda não implementada.");
        }

        private void btnGrupoDeProduto_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Funcionalidade ainda não implementada.");
        }

        private void btnIbametroX_Click(object sender, EventArgs e)
        {
            OpenForm(typeof(Manus.ManIbametroX));
        }

             

    }
}
