﻿using System;
using System.Data;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using TExpress;

namespace osExpress
{
    public partial class RelOrdem : Form
    {
        public RelOrdem()
        {
            InitializeComponent();
        }

        private void SetDBLogonForReport(ConnectionInfo connectionInfo, ReportDocument ArquivoReport)
        {
            Tables tables = ArquivoReport.Database.Tables;
            foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
            {
                TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                tableLogonInfo.ConnectionInfo = connectionInfo;
                table.ApplyLogOnInfo(tableLogonInfo);
            }
        }

        public string GetTotalHoursOfRealizedActivities(string idordem)
        {
            string sql = @" SELECT CAST(t.time_sum/3600 AS VARCHAR(2)) + ':'
                             + CAST(t.time_sum%3600/60 AS VARCHAR(2)) 
                             -- + ':' + CAST(((t.time_sum%3600)%60) AS VARCHAR(2))
                            FROM ( SELECT SUM(DATEDIFF(S, '00:00:00', CONVERT(TIME, hora_total))) AS time_sum
                               from OsSituacoesOS,OsParSituacao
		                         where 1=1 
		                         and idsituacao = OsParSituacao.id
		                         and idordem = '" + idordem + @"'
		                         and not hora_inicio is null	   
	         ) t";
            string hours = @"0";
            try
            {
                hours = DB.GetSingleValue(sql).ToString();
            }
            catch (Exception e) { hours = @"-1"; }
            return hours;
        }

        public void configuraOS(string parametro, string arquivo, string solicitante = "")
        {            
            //parametro == OsOrdemServico.ID

            ReportDocument rpt = new ReportDocument(); 
            string reportPath = "";
            int cliente = 0;
            //bool isA30Clientes = true;
            //try
            //{
            //    cliente = (int)DB.GetSingleValue(@"SELECT idcliente FROM OsOrdemServico WHERE id=" + parametro);
            //    int cnt = (int)DB.GetSingleValue(@"SELECT count(1) FROM A30Clientes WHERE id=" + cliente);
            //    isA30Clientes = cnt > 0;
            //}
            //catch(Exception e) {
            //    MessageBox.Show(e.Message);
            //    return;
            //}
            // A30Clientes/OsCliente
            // 07.01.2021 uwe, depois importar os dados a30clientes em oscliente
            bool isA30Clientes = false;
            if (isA30Clientes)
                reportPath = (@"Relatorios\RelOrdemServico.rpt");
                //reportPath = (@"Relatorios\RelOrdemServicoOsCliente.rpt");
            else 
                reportPath = (@"Relatorios\RelOrdemServicoOsCliente.rpt");

            rpt.Load(reportPath);
            //
            ConnectionInfo minhaConexao = new ConnectionInfo();
            //minhaConexao.ServerName = OsConfiguracoes.IpServidor + @"\sigtecno";
            minhaConexao.ServerName = OsConfiguracoes.IpServidor + @"\" + OsConfiguracoes.Instancia;
              
            //teste local
            if (OsConfiguracoes.IpServidor.Equals("192.168.186.1"))
                minhaConexao.ServerName = OsConfiguracoes.IpServidor + @"\posto";

            minhaConexao.DatabaseName = "OsExpress";
            minhaConexao.UserID = "sa";
            //minhaConexao.Password = "@tecno12hpa";
            minhaConexao.Password = OsConfiguracoes.Senha;
            SetDBLogonForReport(minhaConexao, rpt);
            
            rpt.SetParameterValue(0, parametro);
          
            string lastline = getLastline();          
            rpt.SetParameterValue("lastline",lastline);
            string lastline2 = "";
            if (OsConfiguracoes.codigoPosto.Equals("002"))
                lastline2 = "A SERVIÇO DA TECNOEXPRESS";
            rpt.SetParameterValue("lastline2", lastline2);
           
            if (isA30Clientes == false)
            {
              //string sql = @"Select razao_social from OsCliente where id = " + cliente; 
              //string razao = 
              rpt.SetParameterValue("solicitante", solicitante);
            }

            string totalHours = GetTotalHoursOfRealizedActivities(parametro);
            string horasSomaAtividades = "Total de Horas: " + totalHours;
            rpt.SetParameterValue("horasSomaAtividades", horasSomaAtividades);

            crystalReportViewer1.ReportSource = rpt;
            try
            {
                rpt.ExportToDisk(ExportFormatType.PortableDocFormat, arquivo);
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message, @"ExportError", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        public String getLastline()
        {
            String qry = "Select razao, endereco, numero, bairro, telefone1, cgc, municipal, email" 
                         + " from a30postos where codigo = " + OsConfiguracoes.codigoPosto;
            DataTable dt = DB.GetTableFromSQL(qry);
            String ret = dt.Rows[0].Field<String>(0);
            ret += " - " + dt.Rows[0].Field<String>(1);
            ret += ", " + dt.Rows[0].Field<String>(2);
            ret += " - " + dt.Rows[0].Field<String>(3);
            ret += " - FONE " + dt.Rows[0].Field<String>(4);
            ret += " - CNPJ " + dt.Rows[0].Field<String>(5);
            ret += " - Insc. Munic. " + dt.Rows[0].Field<String>(6);
            ret += " - email: " + dt.Rows[0].Field<String>(7);
            return ret;
        }

        public void RelPrincipal_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;            
        }

        private void RelOrdem_KeyDown(object sender, KeyEventArgs e)
        {
            // Se for pressionado a tecla ESC fecha a tela.
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
