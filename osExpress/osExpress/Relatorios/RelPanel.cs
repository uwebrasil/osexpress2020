﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TecnoExpress;

namespace osExpress.Relatorios
{
    public partial class RelPanel : TecnoExpress.Forms.TForm
    {
        public RelPanel()
        {
            InitializeComponent();
        }

        private void RelPanel_Load(object sender, EventArgs e)
        {
           // por enquanto nada a fazer aqui ...
        }

        // reinicializar 
        private void reinicializePanel()
        {
            foreach (var form in splitContainer1.Panel2.Controls.OfType<CRSituacao.Form1>())
            {
                form.Dispose();
            }
            foreach (var form in splitContainer1.Panel2.Controls.OfType<CREtiqueta.RelEtiqueta2>())
            {
                form.Dispose();
            }
        }

        // Relatório Situação Ordem Serviço
        private void button1_Click(object sender, EventArgs e)
        {
            reinicializePanel();
            CRSituacao.Form1 cr = new CRSituacao.Form1();
            cr.TopLevel = false;
            cr.FormBorderStyle = FormBorderStyle.None;
            splitContainer1.Panel2.Controls.Add(cr);
            int sw = splitContainer1.Panel2.Width;
            int sh = splitContainer1.Panel2.Height;
            cr.Width = sw;
            cr.Height = sh;           
            cr.Visible = true;          
        }

        // Relatório Etiqueta
        private void btn_etiqueta_Click(object sender, EventArgs e)
        {
            reinicializePanel();
            CREtiqueta.RelEtiqueta2 cr = new CREtiqueta.RelEtiqueta2();
            cr.TopLevel = false;
            cr.FormBorderStyle = FormBorderStyle.None;
            splitContainer1.Panel2.Controls.Add(cr);
            int sw = splitContainer1.Panel2.Width;
            int sh = splitContainer1.Panel2.Height;
            cr.Width = sw;
            cr.Height = sh;
            cr.Visible = true; 
        }       
    }
}
