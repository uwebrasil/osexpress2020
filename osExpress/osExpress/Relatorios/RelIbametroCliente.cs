﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Windows.Forms;

namespace osExpress.Relatorios
{
    public partial class RelIbametroCliente : Form
    {
        public RelIbametroCliente()
        {
            InitializeComponent();
        }

        private void SetDBLogonForReport(ConnectionInfo connectionInfo, ReportDocument ArquivoReport)
        {
            Tables tables = ArquivoReport.Database.Tables;
            foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
            {
                TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                tableLogonInfo.ConnectionInfo = connectionInfo;
                table.ApplyLogOnInfo(tableLogonInfo);
            }
        }

        public void configuraOS(string parametro)
        {
            //
            ReportDocument rpt = new ReportDocument();
            string reportPath = (@"Relatorios\RelRptIbametroCliente.rpt");
            rpt.Load(reportPath);
            //
            ConnectionInfo minhaConexao = new ConnectionInfo();
            minhaConexao.ServerName = OsConfiguracoes.IpServidor + @"\sigtecno";
            minhaConexao.DatabaseName = "OsExpress";
            minhaConexao.UserID = "sa";
            minhaConexao.Password = "@tecno12hpa";
            SetDBLogonForReport(minhaConexao, rpt);
            rpt.SetParameterValue(0, parametro);
            crystalReportViewer1.ReportSource = rpt;
        }

        private void RelIbametroCliente_Load(object sender, System.EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;   
        }

        private void RelIbametroCliente_KeyDown(object sender, KeyEventArgs e)
        {
            // Se for pressionado a tecla ESC fecha a tela.
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void crystalReportViewer1_Load(object sender, System.EventArgs e)
        {

        }

    }

}
