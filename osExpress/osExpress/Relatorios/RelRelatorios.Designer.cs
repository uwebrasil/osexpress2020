﻿namespace osExpress.Relatorios
{
    partial class RelRelatorios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.bdsCliente = new System.Windows.Forms.BindingSource(this.components);
            this.osExpressDataSet = new osExpress.OsExpressDataSet();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbRelatorio = new System.Windows.Forms.ComboBox();
            this.a30CLIENTESTableAdapter = new osExpress.OsExpressDataSetTableAdapters.A30CLIENTESTableAdapter();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rptPrincipal = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsCliente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.osExpressDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.comboBox2);
            this.groupBox1.Controls.Add(this.comboBox1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cmbRelatorio);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(5, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(928, 97);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = " Opções ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(173, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Nome do Cliente:";
            // 
            // comboBox2
            // 
            this.comboBox2.DataSource = this.bdsCliente;
            this.comboBox2.DisplayMember = "Nome";
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(267, 41);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(189, 21);
            this.comboBox2.TabIndex = 13;
            this.comboBox2.ValueMember = "ID";
            // 
            // bdsCliente
            // 
            this.bdsCliente.DataMember = "A30CLIENTES";
            this.bdsCliente.DataSource = this.osExpressDataSet;
            this.bdsCliente.Filter = "INATIVO = \'0\' AND TIPO_PESSOA =  \'J\'";
            this.bdsCliente.Sort = "NOME";
            // 
            // osExpressDataSet
            // 
            this.osExpressDataSet.DataSetName = "OsExpressDataSet";
            this.osExpressDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // comboBox1
            // 
            this.comboBox1.DataSource = this.bdsCliente;
            this.comboBox1.DisplayMember = "Cliente";
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(63, 41);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(104, 21);
            this.comboBox1.TabIndex = 12;
            this.comboBox1.ValueMember = "ID";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Código:";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(471, 15);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 20);
            this.button2.TabIndex = 10;
            this.button2.Text = "Gerar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(471, 40);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 20);
            this.button1.TabIndex = 9;
            this.button1.Text = "Enviar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bdsCliente, "obs01", true));
            this.textBox1.Location = new System.Drawing.Point(63, 68);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(393, 20);
            this.textBox1.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Email:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Relatório:";
            // 
            // cmbRelatorio
            // 
            this.cmbRelatorio.FormattingEnabled = true;
            this.cmbRelatorio.Items.AddRange(new object[] {
            "Ordem de Serviço",
            "Ibametro",
            "Recibo"});
            this.cmbRelatorio.Location = new System.Drawing.Point(63, 14);
            this.cmbRelatorio.Name = "cmbRelatorio";
            this.cmbRelatorio.Size = new System.Drawing.Size(393, 21);
            this.cmbRelatorio.TabIndex = 5;
            // 
            // a30CLIENTESTableAdapter
            // 
            this.a30CLIENTESTableAdapter.ClearBeforeFill = true;
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(5, 102);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(928, 10);
            this.panel1.TabIndex = 7;
            // 
            // rptPrincipal
            // 
            this.rptPrincipal.ActiveViewIndex = -1;
            this.rptPrincipal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rptPrincipal.Cursor = System.Windows.Forms.Cursors.Default;
            this.rptPrincipal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rptPrincipal.Location = new System.Drawing.Point(5, 112);
            this.rptPrincipal.Name = "rptPrincipal";
            this.rptPrincipal.ShowLogo = false;
            this.rptPrincipal.Size = new System.Drawing.Size(928, 433);
            this.rptPrincipal.TabIndex = 8;
            this.rptPrincipal.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            // 
            // RelRelatorios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(938, 550);
            this.Controls.Add(this.rptPrincipal);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox1);
            this.KeyPreview = true;
            this.Name = "RelRelatorios";
            this.Padding = new System.Windows.Forms.Padding(5);
            this.Text = "Relatórios";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.RelRelatorios_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RelRelatorios_KeyDown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsCliente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.osExpressDataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbRelatorio;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.BindingSource bdsCliente;
        private OsExpressDataSet osExpressDataSet;
        private OsExpressDataSetTableAdapters.A30CLIENTESTableAdapter a30CLIENTESTableAdapter;
        private System.Windows.Forms.Panel panel1;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer rptPrincipal;
    }
}