﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

namespace osExpress
{
    public partial class RelRecibo : Form
    {
        public RelRecibo()
        {
            InitializeComponent();
        }

        private void RelRecibo_Load(object sender, EventArgs e)
        {
            ReportDocument rpt = new ReportDocument();
            string reportPath = (@"Relatorios\RelReciboServico.rpt");
            rpt.Load(reportPath);

            TextObject txt1 = (TextObject)rpt.ReportDefinition.ReportObjects["txtValorExtenso1"];
            TextObject txt2 = (TextObject)rpt.ReportDefinition.ReportObjects["txtValorExtenso2"];
            TextObject txt3 = (TextObject)rpt.ReportDefinition.ReportObjects["txtReferente1"];
            TextObject txt4 = (TextObject)rpt.ReportDefinition.ReportObjects["txtReferente2"];

            txt1.Text = OsConfiguracoes.literalExtenso;
            txt2.Text = OsConfiguracoes.literalExtenso;
            txt3.Text = OsConfiguracoes.literalRecibo;
            txt4.Text = OsConfiguracoes.literalRecibo;

            ConnectionInfo minhaConexao = new ConnectionInfo();
            minhaConexao.ServerName = OsConfiguracoes.IpServidor + "\\sigtecno";
            minhaConexao.DatabaseName = "OsExpress";
            minhaConexao.UserID = "sa";
            minhaConexao.Password = "@tecno12hpa";
            SetDBLogonForReport(minhaConexao, rpt);
            crystalReportViewer1.ReportSource = rpt;
        }


        private void SetDBLogonForReport(ConnectionInfo connectionInfo, ReportDocument ArquivoReport)
        {
            Tables tables = ArquivoReport.Database.Tables;
            foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
            {
                TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                tableLogonInfo.ConnectionInfo = connectionInfo;
                table.ApplyLogOnInfo(tableLogonInfo);
            }
        }

        private void RelRecibo_KeyDown(object sender, KeyEventArgs e)
        {
            // Se for pressionado a tecla ESC fecha a tela.
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {

        }

    }
}
