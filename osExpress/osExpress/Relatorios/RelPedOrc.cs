﻿using System;
using System.Reflection;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace osExpress
{
    public partial class RelPedido : Form
    {
        private ReportDocument rpt;

        public RelPedido()
        {
            InitializeComponent();
        }

        private void SetDBLogonForReport(ConnectionInfo connectionInfo, ReportDocument ArquivoReport)
        {
            Tables tables = ArquivoReport.Database.Tables;
            foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
            {
                TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                tableLogonInfo.ConnectionInfo = connectionInfo;
                table.ApplyLogOnInfo(tableLogonInfo);
            }
        }

        public void configuraPedido(string parametro,string isSingle, string responsavel, string principal, string principal_celular, string principal_email)
        {
            string fileName = "Pedido nº " + parametro + ".pdf";
            //
            rpt = new ReportDocument();
            string reportPath = (@"Relatorios\RelPedido.rpt");
            rpt.Load(reportPath);
            //
            ConnectionInfo minhaConexao = new ConnectionInfo();
            minhaConexao.ServerName = OsConfiguracoes.IpServidor + "\\sigtecno";
            // teste local
            if (OsConfiguracoes.IpServidor.Equals("192.168.186.1"))
              minhaConexao.ServerName = OsConfiguracoes.IpServidor + "\\posto";

            minhaConexao.DatabaseName = "OsExpress";
            minhaConexao.UserID = "sa";
            minhaConexao.Password = "@tecno12hpa";
            SetDBLogonForReport(minhaConexao, rpt);
            //rpt.SetDatabaseLogon("sa", "masterkey", OsConfiguracoes.IpServidor + "\\sigos","OsExpress");
            rpt.SetParameterValue(0, parametro);
            
            rpt.SetParameterValue(1, isSingle);

            rpt.SetParameterValue(2, responsavel);

            rpt.SetParameterValue(3, principal);

            rpt.SetParameterValue(4, principal_celular);

            rpt.SetParameterValue(5, principal_email);


            crystalReportViewer1.ReportSource = rpt;
            //crpt.SetDatabaseLogon("user", "password", "server", "database")
            //rptDoc.SetDatabaseLogon("user", "password", "host", "dbname");
            rpt.ExportToDisk(ExportFormatType.PortableDocFormat, fileName);
        }

        

        private void RelPrincipal_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }

        private void RelOrdem_KeyDown(object sender, KeyEventArgs e)
        {
            // Se for pressionado a tecla ESC fecha a tela.
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
