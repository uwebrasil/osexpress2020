﻿using System;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Data.SqlClient;
using System.Data;

namespace osExpress
{
    public partial class RelEtiquetas : Form
    {
        public RelEtiquetas()
        {
            InitializeComponent();
        }

        private void SetDBLogonForReport(ConnectionInfo connectionInfo, ReportDocument ArquivoReport)
        {
            Tables tables = ArquivoReport.Database.Tables;
            foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
            {
                TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                tableLogonInfo.ConnectionInfo = connectionInfo;
                table.ApplyLogOnInfo(tableLogonInfo);
            }
        }

        public void RelPrincipal_Load(object sender, EventArgs e)
        {
            this.osUsuarioTableAdapter.Fill(osExpressDataSet.OsUsuario);
        }

        private void RelOrdem_KeyDown(object sender, KeyEventArgs e)
        {
            // Se for pressionado a tecla ESC fecha a tela.
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void osUsuarioBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.osUsuarioBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.osExpressDataSet);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection cnn;
            string connectionString = null;
            string sql = null;
            string dataCadastro = "";
            string dataAtribuicao = "";
            string utilizados = "";

            //connectionString = @"Data Source=meuposto4122.cloudapp.net,49925\azuresigtecno;Initial Catalog=OsExpress;Persist Security Info=True;User ID=sa;Password=@tecno12hpa;";
            connectionString = @"Data Source=192.168.25.254\sigtecno;Initial Catalog=OsExpress;Persist Security Info=True;User ID=sa;Password=@tecno12hpa;";
            cnn = new SqlConnection(connectionString);
            cnn.Open();

            string usuario = " OsUsuario.ID = " + cmbUsuarios.SelectedValue.ToString() + " AND ";

            if (dtpDataCadastro.Checked && dtpDataCadastro.Text.Length > 0)
            {
                dataCadastro = " OsEtiqueta.DT_CADASTRO = '" + dtpDataCadastro.Text + "' AND ";
            }

            if (dtpDataAtribuicao.Checked && dtpDataAtribuicao.Text.Length > 0)
            {
                dataAtribuicao = " OsEtiqueta.DT_ATRIBUICAO = '" + dtpDataAtribuicao.Text + "' AND ";
            }

            switch (cmbUtilizados.SelectedIndex)
            {
                // Todos...
                case 0:
                    {
                        utilizados = "";
                        break;
                    }
                // Utilizados...
                case 1:
                    {
                        utilizados = " OsEtiqueta.UTILIZADO = 1 AND ";
                        break;
                    }
                // Não Utilizados...
                case 2:
                    {
                        utilizados = " OsEtiqueta.UTILIZADO = 0 AND ";
                        break;
                    }
                default:
                    break;
            }

            sql = " SELECT OsUsuario.ID, OsUsuario.NOME, OsEtiqueta.NUM_ETIQUETA, OsEtiqueta.UTILIZADO, case OsEtiqueta.TIPO when 'S' then 'Selo' when 'L' then 'Lacre' end as TIPO" +
                    " FROM OsExpress.dbo.OsEtiqueta OsEtiqueta INNER JOIN OsExpress.dbo.OsUsuario OsUsuario ON OsEtiqueta.IDUSUARIO=OsUsuario.ID" +
                    " WHERE " + usuario + dataCadastro + dataAtribuicao + utilizados;

            sql = sql.Substring(0, sql.Length - 4);
            
            SqlDataAdapter dscmd = new SqlDataAdapter(sql, cnn);
            DataSet ds = new DataSet();
            dscmd.Fill(ds, "Usuario");
            //MessageBox.Show(ds.Tables[0].Rows.Count.ToString());
            cnn.Close();

            TextObject to = (TextObject)RptRelEtiquetas.ReportDefinition.Sections["Section2"].ReportObjects["Text8"];
            to.Text = "USUÁRIO: "+cmbUsuarios.Text;

            RptRelEtiquetas.SetDatabaseLogon("sa", "masterkey");
            //RptRelEtiquetas.SetParameterValue(0 , cmbUsuarios.Text);
            RptRelEtiquetas.SetDataSource(ds.Tables[0]);
            crystalReportViewer1.ReportSource = RptRelEtiquetas;
            crystalReportViewer1.Refresh();
        }
    }
}
