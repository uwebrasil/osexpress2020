﻿namespace osExpress.Manutencoes
{
    partial class ManPedido
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label iDLabel;
            System.Windows.Forms.Label iDUSUARIOLabel;
            System.Windows.Forms.Label dT_CADASTROLabel;
            System.Windows.Forms.Label dT_FECHAMENTOLabel;
            System.Windows.Forms.Label oBSLabel;
            System.Windows.Forms.Label label48;
            System.Windows.Forms.Label lblNomeCliente;
            System.Windows.Forms.Label lblCodigoCliente;
            System.Windows.Forms.Label val_man_men_label;
            System.Windows.Forms.Label ven_man_men_label;
            System.Windows.Forms.Label label14;
            System.Windows.Forms.BindingSource a30PRODUTOBindingSource;
            System.Windows.Forms.Label label13;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.tsbInserir = new System.Windows.Forms.ToolStripButton();
            this.osOrdemServicoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.tsbCancelar = new System.Windows.Forms.ToolStripButton();
            this.tsbSalvar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbDescartar = new System.Windows.Forms.ToolStripButton();
            this.tsbFecharPedido = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbImprimirPedido = new System.Windows.Forms.ToolStripButton();
            this.tsbGerarCaixa = new System.Windows.Forms.ToolStripButton();
            this.tsbEmail = new System.Windows.Forms.ToolStripButton();
            this.tabCabecalho = new System.Windows.Forms.TabPage();
            this.lblLogotipoCliente = new System.Windows.Forms.Label();
            this.lblCGCCliente = new System.Windows.Forms.Label();
            this.cbSingle = new System.Windows.Forms.CheckBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.osClienteBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.vENCIMENTO_MANU_MENSALDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.mANUTENCAO_MENSALTextBox = new System.Windows.Forms.TextBox();
            this.pnlClienteSigposto = new System.Windows.Forms.Panel();
            this.cmbLogotipoClienteSigposto = new System.Windows.Forms.ComboBox();
            this.bdsFiltroCliente = new System.Windows.Forms.BindingSource(this.components);
            this.cmbCGCClienteSigposto = new System.Windows.Forms.ComboBox();
            this.cmbNomeCLienteSigPosto = new System.Windows.Forms.ComboBox();
            this.cmbCodCLienteSigPosto = new System.Windows.Forms.ComboBox();
            this.pnlClienteOsExpress = new System.Windows.Forms.Panel();
            this.cmbLogotipoCliente = new System.Windows.Forms.ComboBox();
            this.cmbCGCCliente = new System.Windows.Forms.ComboBox();
            this.cmbNomeCliente = new System.Windows.Forms.ComboBox();
            this.cmbCodCliente = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.rdbOrigOsExpress = new System.Windows.Forms.RadioButton();
            this.rdbOrigSigPosto = new System.Windows.Forms.RadioButton();
            this.cmbNomeUsuario = new System.Windows.Forms.ComboBox();
            this.OsXOSFuncionarioBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cmbCodigoUsuario = new System.Windows.Forms.ComboBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.txtCodOrdemServico = new System.Windows.Forms.TextBox();
            this.dtpDataCadastro = new System.Windows.Forms.DateTimePicker();
            this.dtpDataFechamento = new System.Windows.Forms.DateTimePicker();
            this.txtObservacao = new System.Windows.Forms.TextBox();
            this.osUsuarioBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.a30CLIENTESBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.osExpressDataSet = new osExpress.OsExpressDataSet();
            this.osOrdemParcelaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPecas = new System.Windows.Forms.TabPage();
            this.lbl_grupo = new System.Windows.Forms.Label();
            this.cmbGrupoProduto = new System.Windows.Forms.ComboBox();
            this.numQtdeProduto = new System.Windows.Forms.NumericUpDown();
            this.txtTotalProduto = new System.Windows.Forms.TextBox();
            this.txtPrecoProduto = new System.Windows.Forms.TextBox();
            this.cmbCodigoIDProduto = new System.Windows.Forms.ComboBox();
            this.cmbDescricaoProduto = new System.Windows.Forms.ComboBox();
            this.cmbCodigoProduto = new System.Windows.Forms.ComboBox();
            this.grdPecas = new System.Windows.Forms.DataGridView();
            this.qUANTIDADEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vALORDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tOTALDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Produto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descrição = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.osItemServicoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnAdicionarPecas = new System.Windows.Forms.Button();
            this.label37 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lblCabCliente = new System.Windows.Forms.Label();
            this.lblCabNCPJ = new System.Windows.Forms.Label();
            this.lblCabIE = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblCabTelefone = new System.Windows.Forms.Label();
            this.lblCabObservacao = new System.Windows.Forms.Label();
            this.lblSituacao2 = new System.Windows.Forms.Label();
            this.lblCabSituacao = new System.Windows.Forms.Label();
            this.osParContasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.txtTotalFinalOS = new System.Windows.Forms.TextBox();
            this.txtValorAPagar = new System.Windows.Forms.TextBox();
            this.bdsFiltroUsuario = new System.Windows.Forms.BindingSource(this.components);
            this.pnlConsultar = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label70 = new System.Windows.Forms.Label();
            this.lblTotalOS = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkPostoOS = new System.Windows.Forms.CheckBox();
            this.labelPostoOS = new System.Windows.Forms.Label();
            this.cmbPostoOS = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.chkFiltrarCliente = new System.Windows.Forms.CheckBox();
            this.cmbFiltroClienteNome = new System.Windows.Forms.ComboBox();
            this.chkFiltrarSituacao = new System.Windows.Forms.CheckBox();
            this.label65 = new System.Windows.Forms.Label();
            this.cmbFiltroSituacao = new System.Windows.Forms.ComboBox();
            this.osOrdemServicoDataGridView = new System.Windows.Forms.DataGridView();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IDPOSTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Usuario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL_OS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dTCADASTRODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dTFECHAMENTODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.comboBox8 = new System.Windows.Forms.ComboBox();
            this.lblCabFatura = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.lblNovoCodOS = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblCodigoOS = new System.Windows.Forms.Label();
            this.txtDesconto = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblCabTipoPed = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.tabParcelas = new System.Windows.Forms.TabPage();
            this.labelIntervalo = new System.Windows.Forms.Label();
            this.comboBoxIntervalo = new System.Windows.Forms.ComboBox();
            this.numQtdeParcela = new System.Windows.Forms.NumericUpDown();
            this.label23 = new System.Windows.Forms.Label();
            this.txtEntrada = new System.Windows.Forms.TextBox();
            this.rdbAPartirDiaPrimeiro = new System.Windows.Forms.RadioButton();
            this.osOrdemParcelaDataGridView = new System.Windows.Forms.DataGridView();
            this.vALORPARCELADataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dATAPARCELADataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nUMEROPARCELADataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rdbAPartirDataAtual = new System.Windows.Forms.RadioButton();
            this.btnInserirParcela = new System.Windows.Forms.Button();
            this.osOrdemServicoTableAdapter = new osExpress.OsExpressDataSetTableAdapters.OsOrdemServicoTableAdapter();
            this.osItemServicoTableAdapter = new osExpress.OsExpressDataSetTableAdapters.OsItemServicoTableAdapter();
            this.osParSituacaoTableAdapter = new osExpress.OsExpressDataSetTableAdapters.OsParSituacaoTableAdapter();
            this.a30POSTOSTableAdapter = new osExpress.OsExpressDataSetTableAdapters.A30POSTOSTableAdapter();
            this.a30PRODUTOTableAdapter = new osExpress.OsExpressDataSetTableAdapters.A30PRODUTOTableAdapter();
            this.tableAdapterManager = new osExpress.OsExpressDataSetTableAdapters.TableAdapterManager();
            this.osOrdemParcelaTableAdapter = new osExpress.OsExpressDataSetTableAdapters.OsOrdemParcelaTableAdapter();
            this.osParContasTableAdapter = new osExpress.OsExpressDataSetTableAdapters.OsParContasTableAdapter();
            this.osUsuarioTableAdapter = new osExpress.OsExpressDataSetTableAdapters.OsUsuarioTableAdapter();
            this.osOrdemServicoTableAdapterView = new osExpress.OsExpressDataSetTableAdapters.OsOrdemServicoTableAdapter();
            this.osClienteTableAdapter = new osExpress.OsExpressDataSetTableAdapters.OsClienteTableAdapter();
            this.a30CLIENTESTableAdapter = new osExpress.OsExpressDataSetTableAdapters.A30CLIENTESTableAdapter();
            this.label24 = new System.Windows.Forms.Label();
            this.txtTotalDasParcelas = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtDifParcelaTotal = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.osParametrosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.osParametrosTableAdapter = new osExpress.OsExpressDataSetTableAdapters.OsParametrosTableAdapter();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.lblCidade = new System.Windows.Forms.Label();
            this.lblCidade2 = new System.Windows.Forms.Label();
            this.lblUF = new System.Windows.Forms.Label();
            this.lblUF2 = new System.Windows.Forms.Label();
            this.lblCidadeNovoCliente = new System.Windows.Forms.Label();
            this.lblUFNovoCliente = new System.Windows.Forms.Label();
            this.cmbEmpresa = new System.Windows.Forms.ComboBox();
            this.lblEmpresa = new System.Windows.Forms.Label();
            this.xOsFuncionarioTableAdapter = new osExpress.OsExpressDataSetTableAdapters.XOsFuncionarioTableAdapter();
            iDLabel = new System.Windows.Forms.Label();
            iDUSUARIOLabel = new System.Windows.Forms.Label();
            dT_CADASTROLabel = new System.Windows.Forms.Label();
            dT_FECHAMENTOLabel = new System.Windows.Forms.Label();
            oBSLabel = new System.Windows.Forms.Label();
            label48 = new System.Windows.Forms.Label();
            lblNomeCliente = new System.Windows.Forms.Label();
            lblCodigoCliente = new System.Windows.Forms.Label();
            val_man_men_label = new System.Windows.Forms.Label();
            ven_man_men_label = new System.Windows.Forms.Label();
            label14 = new System.Windows.Forms.Label();
            a30PRODUTOBindingSource = new System.Windows.Forms.BindingSource(this.components);
            label13 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dtsPrincipal)).BeginInit();
            this.grpPrincipal.SuspendLayout();
            this.tabPrincipal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(a30PRODUTOBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator)).BeginInit();
            this.bindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.osOrdemServicoBindingSource)).BeginInit();
            this.tabCabecalho.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.osClienteBindingSource)).BeginInit();
            this.pnlClienteSigposto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsFiltroCliente)).BeginInit();
            this.pnlClienteOsExpress.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OsXOSFuncionarioBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.osUsuarioBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.a30CLIENTESBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.osExpressDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.osOrdemParcelaBindingSource)).BeginInit();
            this.tabPecas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numQtdeProduto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdPecas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.osItemServicoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.osParContasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsFiltroUsuario)).BeginInit();
            this.pnlConsultar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.osOrdemServicoDataGridView)).BeginInit();
            this.tabParcelas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numQtdeParcela)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.osOrdemParcelaDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.osParametrosBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // grpPrincipal
            // 
            this.grpPrincipal.Controls.Add(this.lblUFNovoCliente);
            this.grpPrincipal.Controls.Add(this.lblCidadeNovoCliente);
            this.grpPrincipal.Controls.Add(this.lblUF2);
            this.grpPrincipal.Controls.Add(this.lblUF);
            this.grpPrincipal.Controls.Add(this.lblCidade2);
            this.grpPrincipal.Controls.Add(this.lblCidade);
            this.grpPrincipal.Controls.Add(this.label30);
            this.grpPrincipal.Controls.Add(this.label28);
            this.grpPrincipal.Controls.Add(this.label26);
            this.grpPrincipal.Controls.Add(this.label25);
            this.grpPrincipal.Controls.Add(this.txtDifParcelaTotal);
            this.grpPrincipal.Controls.Add(this.label24);
            this.grpPrincipal.Controls.Add(this.txtTotalDasParcelas);
            this.grpPrincipal.Controls.Add(this.label9);
            this.grpPrincipal.Controls.Add(this.label19);
            this.grpPrincipal.Controls.Add(this.label20);
            this.grpPrincipal.Controls.Add(this.label21);
            this.grpPrincipal.Controls.Add(this.label22);
            this.grpPrincipal.Controls.Add(this.lblCabTipoPed);
            this.grpPrincipal.Controls.Add(this.label15);
            this.grpPrincipal.Controls.Add(this.label7);
            this.grpPrincipal.Controls.Add(this.txtTotalFinalOS);
            this.grpPrincipal.Controls.Add(this.label6);
            this.grpPrincipal.Controls.Add(this.txtDesconto);
            this.grpPrincipal.Controls.Add(this.label5);
            this.grpPrincipal.Controls.Add(this.txtValorAPagar);
            this.grpPrincipal.Controls.Add(this.lblNovoCodOS);
            this.grpPrincipal.Controls.Add(this.lblCabFatura);
            this.grpPrincipal.Controls.Add(this.label72);
            this.grpPrincipal.Controls.Add(this.lblCabSituacao);
            this.grpPrincipal.Controls.Add(this.lblSituacao2);
            this.grpPrincipal.Controls.Add(this.lblCabObservacao);
            this.grpPrincipal.Controls.Add(this.lblCabTelefone);
            this.grpPrincipal.Controls.Add(this.label11);
            this.grpPrincipal.Controls.Add(this.label10);
            this.grpPrincipal.Controls.Add(this.lblCabIE);
            this.grpPrincipal.Controls.Add(this.lblCabNCPJ);
            this.grpPrincipal.Controls.Add(this.lblCabCliente);
            this.grpPrincipal.Controls.Add(this.label4);
            this.grpPrincipal.Controls.Add(this.label3);
            this.grpPrincipal.Controls.Add(this.label2);
            this.grpPrincipal.Controls.Add(this.label1);
            this.grpPrincipal.Location = new System.Drawing.Point(11, 74);
            this.grpPrincipal.Size = new System.Drawing.Size(831, 165);
            this.grpPrincipal.Text = " Principal ";
            // 
            // tabPrincipal
            // 
            this.tabPrincipal.Controls.Add(this.tabCabecalho);
            this.tabPrincipal.Controls.Add(this.tabPecas);
            this.tabPrincipal.Controls.Add(this.tabParcelas);
            this.tabPrincipal.Location = new System.Drawing.Point(12, 239);
            this.tabPrincipal.Size = new System.Drawing.Size(820, 397);
            this.tabPrincipal.TabIndex = 0;
            this.tabPrincipal.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabPrincipal_Selected);
            // 
            // iDLabel
            // 
            iDLabel.AutoSize = true;
            iDLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            iDLabel.ForeColor = System.Drawing.Color.Red;
            iDLabel.Location = new System.Drawing.Point(17, 14);
            iDLabel.Name = "iDLabel";
            iDLabel.Size = new System.Drawing.Size(111, 13);
            iDLabel.TabIndex = 0;
            iDLabel.Text = "Código do Pedido:";
            // 
            // iDUSUARIOLabel
            // 
            iDUSUARIOLabel.AutoSize = true;
            iDUSUARIOLabel.Location = new System.Drawing.Point(82, 39);
            iDUSUARIOLabel.Name = "iDUSUARIOLabel";
            iDUSUARIOLabel.Size = new System.Drawing.Size(46, 13);
            iDUSUARIOLabel.TabIndex = 2;
            iDUSUARIOLabel.Text = "Usuário:";
            // 
            // dT_CADASTROLabel
            // 
            dT_CADASTROLabel.AutoSize = true;
            dT_CADASTROLabel.Location = new System.Drawing.Point(35, 322);
            dT_CADASTROLabel.Name = "dT_CADASTROLabel";
            dT_CADASTROLabel.Size = new System.Drawing.Size(93, 13);
            dT_CADASTROLabel.TabIndex = 10;
            dT_CADASTROLabel.Text = "Data de Cadastro:";
            // 
            // dT_FECHAMENTOLabel
            // 
            dT_FECHAMENTOLabel.AutoSize = true;
            dT_FECHAMENTOLabel.Location = new System.Drawing.Point(18, 348);
            dT_FECHAMENTOLabel.Name = "dT_FECHAMENTOLabel";
            dT_FECHAMENTOLabel.Size = new System.Drawing.Size(110, 13);
            dT_FECHAMENTOLabel.TabIndex = 12;
            dT_FECHAMENTOLabel.Text = "Data de Fechamento:";
            // 
            // oBSLabel
            // 
            oBSLabel.AutoSize = true;
            oBSLabel.Location = new System.Drawing.Point(3, 140);
            oBSLabel.Name = "oBSLabel";
            oBSLabel.Size = new System.Drawing.Size(125, 13);
            oBSLabel.TabIndex = 14;
            oBSLabel.Text = "Observações  p/ Cliente:";
            // 
            // label48
            // 
            label48.AutoSize = true;
            label48.Location = new System.Drawing.Point(19, 231);
            label48.Name = "label48";
            label48.Size = new System.Drawing.Size(109, 13);
            label48.TabIndex = 28;
            label48.Text = "Observações Interna:";
            // 
            // lblNomeCliente
            // 
            lblNomeCliente.AutoSize = true;
            lblNomeCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblNomeCliente.Location = new System.Drawing.Point(236, 86);
            lblNomeCliente.Name = "lblNomeCliente";
            lblNomeCliente.Size = new System.Drawing.Size(88, 13);
            lblNomeCliente.TabIndex = 29;
            lblNomeCliente.Text = "Nome do Cliente:";
            this.toolTip1.SetToolTip(lblNomeCliente, "Sortear por Nome do Cliente");
            lblNomeCliente.Click += new System.EventHandler(this.lblNomeCliente_Click);
            lblNomeCliente.MouseEnter += new System.EventHandler(this.lblNomeCliente_MouseEnter);
            lblNomeCliente.MouseLeave += new System.EventHandler(this.lblNomeCliente_MouseLeave);
            // 
            // lblCodigoCliente
            // 
            lblCodigoCliente.AutoSize = true;
            lblCodigoCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblCodigoCliente.Location = new System.Drawing.Point(35, 86);
            lblCodigoCliente.Name = "lblCodigoCliente";
            lblCodigoCliente.Size = new System.Drawing.Size(93, 13);
            lblCodigoCliente.TabIndex = 28;
            lblCodigoCliente.Text = "Código do Cliente:";
            this.toolTip1.SetToolTip(lblCodigoCliente, "Sortear por Código do Cliente");
            lblCodigoCliente.Click += new System.EventHandler(this.lblCodigoCliente_Click);
            lblCodigoCliente.MouseEnter += new System.EventHandler(this.lblCodigoCliente_MouseEnter);
            lblCodigoCliente.MouseLeave += new System.EventHandler(this.lblCodigoCliente_MouseLeave);
            // 
            // val_man_men_label
            // 
            val_man_men_label.AutoSize = true;
            val_man_men_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            val_man_men_label.ForeColor = System.Drawing.Color.Red;
            val_man_men_label.Location = new System.Drawing.Point(544, 44);
            val_man_men_label.Name = "val_man_men_label";
            val_man_men_label.Size = new System.Drawing.Size(156, 13);
            val_man_men_label.TabIndex = 39;
            val_man_men_label.Text = "Valor manutenção mensal:";
            // 
            // ven_man_men_label
            // 
            ven_man_men_label.AutoSize = true;
            ven_man_men_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ven_man_men_label.ForeColor = System.Drawing.Color.Red;
            ven_man_men_label.Location = new System.Drawing.Point(509, 18);
            ven_man_men_label.Name = "ven_man_men_label";
            ven_man_men_label.Size = new System.Drawing.Size(193, 13);
            ven_man_men_label.TabIndex = 40;
            ven_man_men_label.Text = "Vencimento manutenção mensal:";
            // 
            // label14
            // 
            label14.AutoSize = true;
            label14.Location = new System.Drawing.Point(27, 9);
            label14.Name = "label14";
            label14.Size = new System.Drawing.Size(92, 13);
            label14.TabIndex = 79;
            label14.Text = "Qtde de Parcelas:";
            // 
            // a30PRODUTOBindingSource
            // 
            a30PRODUTOBindingSource.AllowNew = false;
            a30PRODUTOBindingSource.DataMember = "A30PRODUTO";
            a30PRODUTOBindingSource.DataSource = this.dtsPrincipal;
            a30PRODUTOBindingSource.Filter = "TIPO <> 1 AND INATIVO = 0";
            a30PRODUTOBindingSource.Sort = "DESCRICAO";
            // 
            // label13
            // 
            label13.AutoSize = true;
            label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label13.Location = new System.Drawing.Point(574, 86);
            label13.Name = "label13";
            label13.Size = new System.Drawing.Size(76, 13);
            label13.TabIndex = 43;
            label13.Text = "Email a enviar:";
            // 
            // bindingNavigator
            // 
            this.bindingNavigator.AddNewItem = this.tsbInserir;
            this.bindingNavigator.BindingSource = this.osOrdemServicoBindingSource;
            this.bindingNavigator.CountItem = this.toolStripLabel1;
            this.bindingNavigator.CountItemFormat = "de {0}";
            this.bindingNavigator.DeleteItem = null;
            this.bindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bindingNavigator.ImageScalingSize = new System.Drawing.Size(48, 48);
            this.bindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton3,
            this.tsbInserir,
            this.tsbCancelar,
            this.tsbSalvar,
            this.toolStripSeparator4,
            this.toolStripButton1,
            this.toolStripButton5,
            this.toolStripSeparator1,
            this.toolStripTextBox1,
            this.toolStripLabel1,
            this.toolStripSeparator2,
            this.toolStripButton6,
            this.toolStripButton7,
            this.toolStripSeparator3,
            this.tsbDescartar,
            this.tsbFecharPedido,
            this.toolStripSeparator5,
            this.tsbImprimirPedido,
            this.tsbGerarCaixa,
            this.tsbEmail});
            this.bindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator.MoveFirstItem = this.toolStripButton1;
            this.bindingNavigator.MoveLastItem = this.toolStripButton7;
            this.bindingNavigator.MoveNextItem = this.toolStripButton6;
            this.bindingNavigator.MovePreviousItem = this.toolStripButton5;
            this.bindingNavigator.Name = "bindingNavigator";
            this.bindingNavigator.PositionItem = this.toolStripTextBox1;
            this.bindingNavigator.Size = new System.Drawing.Size(887, 70);
            this.bindingNavigator.TabIndex = 9;
            this.bindingNavigator.Text = "bindingNavigator1";
            this.bindingNavigator.RefreshItems += new System.EventHandler(this.bindingNavigator_RefreshItems);
            // 
            // tsbInserir
            // 
            this.tsbInserir.Image = global::osExpress.Properties.Resources.insert;
            this.tsbInserir.Name = "tsbInserir";
            this.tsbInserir.RightToLeftAutoMirrorImage = true;
            this.tsbInserir.Size = new System.Drawing.Size(52, 67);
            this.tsbInserir.Text = "&Inserir";
            this.tsbInserir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbInserir.Click += new System.EventHandler(this.tsbInserir_Click);
            // 
            // osOrdemServicoBindingSource
            // 
            this.osOrdemServicoBindingSource.DataMember = "OsOrdemServico";
            this.osOrdemServicoBindingSource.DataSource = this.dtsPrincipal;
            this.osOrdemServicoBindingSource.Filter = "STATUS_OS = \'A\' AND TIPO_DOC <> \'S\' ";
            this.osOrdemServicoBindingSource.Sort = "DT_CADASTRO ASC";
            this.osOrdemServicoBindingSource.CurrentChanged += new System.EventHandler(this.osOrdemServicoBindingSource_CurrentChanged);
            this.osOrdemServicoBindingSource.ListChanged += new System.ComponentModel.ListChangedEventHandler(this.osOrdemServicoBindingSource_ListChanged);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(37, 67);
            this.toolStripLabel1.Text = "de {0}";
            this.toolStripLabel1.ToolTipText = "Total number of items";
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.Image = global::osExpress.Properties.Resources.search;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(62, 67);
            this.toolStripButton3.Text = "Consultar";
            this.toolStripButton3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click_1);
            // 
            // tsbCancelar
            // 
            this.tsbCancelar.Image = global::osExpress.Properties.Resources.clean;
            this.tsbCancelar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbCancelar.Name = "tsbCancelar";
            this.tsbCancelar.Size = new System.Drawing.Size(57, 67);
            this.tsbCancelar.Text = "Cancelar";
            this.tsbCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbCancelar.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // tsbSalvar
            // 
            this.tsbSalvar.Image = global::osExpress.Properties.Resources.save;
            this.tsbSalvar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSalvar.Name = "tsbSalvar";
            this.tsbSalvar.Size = new System.Drawing.Size(52, 67);
            this.tsbSalvar.Text = "Grav&ar";
            this.tsbSalvar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbSalvar.Click += new System.EventHandler(this.tsbSalvar_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 70);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = global::osExpress.Properties.Resources.first;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.RightToLeftAutoMirrorImage = true;
            this.toolStripButton1.Size = new System.Drawing.Size(56, 67);
            this.toolStripButton1.Text = "&Primeiro";
            this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.Image = global::osExpress.Properties.Resources.previous;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.RightToLeftAutoMirrorImage = true;
            this.toolStripButton5.Size = new System.Drawing.Size(54, 67);
            this.toolStripButton5.Text = "Anteri&or";
            this.toolStripButton5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton5.Click += new System.EventHandler(this.toolStripButton5_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 70);
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.AccessibleName = "Position";
            this.toolStripTextBox1.AutoSize = false;
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(50, 23);
            this.toolStripTextBox1.Text = "0";
            this.toolStripTextBox1.ToolTipText = "Current position";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 70);
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.Image = global::osExpress.Properties.Resources.next;
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.RightToLeftAutoMirrorImage = true;
            this.toolStripButton6.Size = new System.Drawing.Size(56, 67);
            this.toolStripButton6.Text = "Próxi&mo";
            this.toolStripButton6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton6.Click += new System.EventHandler(this.toolStripButton6_Click);
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.Image = global::osExpress.Properties.Resources.last;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.RightToLeftAutoMirrorImage = true;
            this.toolStripButton7.Size = new System.Drawing.Size(52, 67);
            this.toolStripButton7.Text = "&Último";
            this.toolStripButton7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton7.Click += new System.EventHandler(this.toolStripButton7_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 70);
            // 
            // tsbDescartar
            // 
            this.tsbDescartar.Image = global::osExpress.Properties.Resources.remove;
            this.tsbDescartar.Name = "tsbDescartar";
            this.tsbDescartar.RightToLeftAutoMirrorImage = true;
            this.tsbDescartar.Size = new System.Drawing.Size(60, 67);
            this.tsbDescartar.Text = "&Descartar";
            this.tsbDescartar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbDescartar.Click += new System.EventHandler(this.tsbExcluir_Click);
            // 
            // tsbFecharPedido
            // 
            this.tsbFecharPedido.Image = global::osExpress.Properties.Resources.key;
            this.tsbFecharPedido.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbFecharPedido.Name = "tsbFecharPedido";
            this.tsbFecharPedido.Size = new System.Drawing.Size(86, 67);
            this.tsbFecharPedido.Text = "&Fechar Pedido";
            this.tsbFecharPedido.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbFecharPedido.Click += new System.EventHandler(this.tsbFecharOS_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 70);
            // 
            // tsbImprimirPedido
            // 
            this.tsbImprimirPedido.Image = global::osExpress.Properties.Resources.printer;
            this.tsbImprimirPedido.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbImprimirPedido.Name = "tsbImprimirPedido";
            this.tsbImprimirPedido.Size = new System.Drawing.Size(97, 67);
            this.tsbImprimirPedido.Text = "Imprimir Pedido";
            this.tsbImprimirPedido.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbImprimirPedido.Click += new System.EventHandler(this.tsbImprimirOS_Click);
            // 
            // tsbGerarCaixa
            // 
            this.tsbGerarCaixa.Enabled = false;
            this.tsbGerarCaixa.Image = global::osExpress.Properties.Resources.creditcard;
            this.tsbGerarCaixa.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbGerarCaixa.Name = "tsbGerarCaixa";
            this.tsbGerarCaixa.Size = new System.Drawing.Size(71, 67);
            this.tsbGerarCaixa.Text = "Gerar Caixa";
            this.tsbGerarCaixa.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbGerarCaixa.Visible = false;
            this.tsbGerarCaixa.Click += new System.EventHandler(this.tsbGerarCaixa_Click);
            // 
            // tsbEmail
            // 
            this.tsbEmail.Image = global::osExpress.Properties.Resources.email;
            this.tsbEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbEmail.Name = "tsbEmail";
            this.tsbEmail.Size = new System.Drawing.Size(96, 67);
            this.tsbEmail.Text = "Enviar por Email";
            this.tsbEmail.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tsbEmail.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbEmail.Click += new System.EventHandler(this.tsbEmail_Click);
            // 
            // tabCabecalho
            // 
            this.tabCabecalho.AutoScroll = true;
            this.tabCabecalho.Controls.Add(this.lblLogotipoCliente);
            this.tabCabecalho.Controls.Add(this.lblCGCCliente);
            this.tabCabecalho.Controls.Add(this.cbSingle);
            this.tabCabecalho.Controls.Add(label13);
            this.tabCabecalho.Controls.Add(this.txtEmail);
            this.tabCabecalho.Controls.Add(ven_man_men_label);
            this.tabCabecalho.Controls.Add(this.vENCIMENTO_MANU_MENSALDateTimePicker);
            this.tabCabecalho.Controls.Add(val_man_men_label);
            this.tabCabecalho.Controls.Add(this.mANUTENCAO_MENSALTextBox);
            this.tabCabecalho.Controls.Add(lblNomeCliente);
            this.tabCabecalho.Controls.Add(lblCodigoCliente);
            this.tabCabecalho.Controls.Add(this.pnlClienteSigposto);
            this.tabCabecalho.Controls.Add(this.pnlClienteOsExpress);
            this.tabCabecalho.Controls.Add(this.label16);
            this.tabCabecalho.Controls.Add(this.rdbOrigOsExpress);
            this.tabCabecalho.Controls.Add(this.rdbOrigSigPosto);
            this.tabCabecalho.Controls.Add(this.cmbNomeUsuario);
            this.tabCabecalho.Controls.Add(this.cmbCodigoUsuario);
            this.tabCabecalho.Controls.Add(label48);
            this.tabCabecalho.Controls.Add(this.textBox1);
            this.tabCabecalho.Controls.Add(iDLabel);
            this.tabCabecalho.Controls.Add(this.txtCodOrdemServico);
            this.tabCabecalho.Controls.Add(iDUSUARIOLabel);
            this.tabCabecalho.Controls.Add(dT_CADASTROLabel);
            this.tabCabecalho.Controls.Add(this.dtpDataCadastro);
            this.tabCabecalho.Controls.Add(dT_FECHAMENTOLabel);
            this.tabCabecalho.Controls.Add(this.dtpDataFechamento);
            this.tabCabecalho.Controls.Add(oBSLabel);
            this.tabCabecalho.Controls.Add(this.txtObservacao);
            this.tabCabecalho.Location = new System.Drawing.Point(4, 22);
            this.tabCabecalho.Name = "tabCabecalho";
            this.tabCabecalho.Padding = new System.Windows.Forms.Padding(3);
            this.tabCabecalho.Size = new System.Drawing.Size(812, 371);
            this.tabCabecalho.TabIndex = 2;
            this.tabCabecalho.Text = "Cabeçalho";
            this.tabCabecalho.UseVisualStyleBackColor = true;
            this.tabCabecalho.Enter += new System.EventHandler(this.tabCabecalho_Enter);
            // 
            // lblLogotipoCliente
            // 
            this.lblLogotipoCliente.AutoSize = true;
            this.lblLogotipoCliente.Location = new System.Drawing.Point(264, 113);
            this.lblLogotipoCliente.Name = "lblLogotipoCliente";
            this.lblLogotipoCliente.Size = new System.Drawing.Size(101, 13);
            this.lblLogotipoCliente.TabIndex = 48;
            this.lblLogotipoCliente.Text = "Logotipo de Cliente:";
            this.lblLogotipoCliente.Click += new System.EventHandler(this.lblLogotipoCliente_Click);
            this.lblLogotipoCliente.MouseEnter += new System.EventHandler(this.lblLogotipoCliente_MouseEnter);
            this.lblLogotipoCliente.MouseLeave += new System.EventHandler(this.lblLogotipoCliente_MouseLeave);
            // 
            // lblCGCCliente
            // 
            this.lblCGCCliente.AutoSize = true;
            this.lblCGCCliente.Location = new System.Drawing.Point(36, 113);
            this.lblCGCCliente.Name = "lblCGCCliente";
            this.lblCGCCliente.Size = new System.Drawing.Size(82, 13);
            this.lblCGCCliente.TabIndex = 47;
            this.lblCGCCliente.Text = "CGC de Cliente:";
            this.lblCGCCliente.Click += new System.EventHandler(this.lblCGCCliente_Click);
            this.lblCGCCliente.MouseEnter += new System.EventHandler(this.lblCGCCliente_MouseEnter);
            this.lblCGCCliente.MouseLeave += new System.EventHandler(this.lblCGCCliente_MouseLeave);
            // 
            // cbSingle
            // 
            this.cbSingle.AutoSize = true;
            this.cbSingle.Location = new System.Drawing.Point(707, 63);
            this.cbSingle.Name = "cbSingle";
            this.cbSingle.Size = new System.Drawing.Size(87, 17);
            this.cbSingle.TabIndex = 44;
            this.cbSingle.Text = "Incluir Aceite";
            this.cbSingle.UseVisualStyleBackColor = true;
            this.cbSingle.CheckedChanged += new System.EventHandler(this.cbSingle_CheckedChanged);
            // 
            // txtEmail
            // 
            this.txtEmail.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osClienteBindingSource, "EMAIL", true));
            this.txtEmail.Location = new System.Drawing.Point(655, 83);
            this.txtEmail.MaxLength = 50;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(149, 20);
            this.txtEmail.TabIndex = 5;
            // 
            // osClienteBindingSource
            // 
            this.osClienteBindingSource.DataMember = "OsCliente";
            this.osClienteBindingSource.DataSource = this.dtsPrincipal;
            this.osClienteBindingSource.CurrentChanged += new System.EventHandler(this.osClienteBindingSource_CurrentChanged);
            this.osClienteBindingSource.ListChanged += new System.ComponentModel.ListChangedEventHandler(this.osClienteBindingSource_ListChanged);
            this.osClienteBindingSource.PositionChanged += new System.EventHandler(this.osClienteBindingSource_PositionChanged);
            // 
            // vENCIMENTO_MANU_MENSALDateTimePicker
            // 
            this.vENCIMENTO_MANU_MENSALDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.osOrdemServicoBindingSource, "VENCIMENTO_MANU_MENSAL", true));
            this.vENCIMENTO_MANU_MENSALDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.vENCIMENTO_MANU_MENSALDateTimePicker.Location = new System.Drawing.Point(708, 11);
            this.vENCIMENTO_MANU_MENSALDateTimePicker.Name = "vENCIMENTO_MANU_MENSALDateTimePicker";
            this.vENCIMENTO_MANU_MENSALDateTimePicker.Size = new System.Drawing.Size(90, 20);
            this.vENCIMENTO_MANU_MENSALDateTimePicker.TabIndex = 3;
            // 
            // mANUTENCAO_MENSALTextBox
            // 
            this.mANUTENCAO_MENSALTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "MANUTENCAO_MENSAL", true));
            this.mANUTENCAO_MENSALTextBox.Location = new System.Drawing.Point(706, 38);
            this.mANUTENCAO_MENSALTextBox.Name = "mANUTENCAO_MENSALTextBox";
            this.mANUTENCAO_MENSALTextBox.Size = new System.Drawing.Size(91, 20);
            this.mANUTENCAO_MENSALTextBox.TabIndex = 4;
            this.mANUTENCAO_MENSALTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TplPermiteSoValor);
            // 
            // pnlClienteSigposto
            // 
            this.pnlClienteSigposto.Controls.Add(this.cmbLogotipoClienteSigposto);
            this.pnlClienteSigposto.Controls.Add(this.cmbCGCClienteSigposto);
            this.pnlClienteSigposto.Controls.Add(this.cmbNomeCLienteSigPosto);
            this.pnlClienteSigposto.Controls.Add(this.cmbCodCLienteSigPosto);
            this.pnlClienteSigposto.Enabled = false;
            this.pnlClienteSigposto.Location = new System.Drawing.Point(23, 182);
            this.pnlClienteSigposto.Name = "pnlClienteSigposto";
            this.pnlClienteSigposto.Size = new System.Drawing.Size(679, 55);
            this.pnlClienteSigposto.TabIndex = 39;
            this.pnlClienteSigposto.Visible = false;
            // 
            // cmbLogotipoClienteSigposto
            // 
            this.cmbLogotipoClienteSigposto.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbLogotipoClienteSigposto.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbLogotipoClienteSigposto.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osOrdemServicoBindingSource, "IDCLIENTE", true));
            this.cmbLogotipoClienteSigposto.DataSource = this.bdsFiltroCliente;
            this.cmbLogotipoClienteSigposto.DisplayMember = "Nome";
            this.cmbLogotipoClienteSigposto.Enabled = false;
            this.cmbLogotipoClienteSigposto.FormattingEnabled = true;
            this.cmbLogotipoClienteSigposto.Location = new System.Drawing.Point(352, 27);
            this.cmbLogotipoClienteSigposto.Name = "cmbLogotipoClienteSigposto";
            this.cmbLogotipoClienteSigposto.Size = new System.Drawing.Size(194, 21);
            this.cmbLogotipoClienteSigposto.TabIndex = 46;
            this.cmbLogotipoClienteSigposto.ValueMember = "ID";
            this.cmbLogotipoClienteSigposto.Visible = false;
            this.cmbLogotipoClienteSigposto.Click += new System.EventHandler(this.cmbLogotipoClienteSigposto_Click);
            // 
            // bdsFiltroCliente
            // 
            this.bdsFiltroCliente.DataMember = "A30CLIENTES";
            this.bdsFiltroCliente.DataSource = this.dtsPrincipal;
            this.bdsFiltroCliente.Filter = "INATIVO = \'0\'";
            this.bdsFiltroCliente.Sort = "NOME";
            // 
            // cmbCGCClienteSigposto
            // 
            this.cmbCGCClienteSigposto.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCGCClienteSigposto.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCGCClienteSigposto.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osOrdemServicoBindingSource, "IDCLIENTE", true));
            this.cmbCGCClienteSigposto.DataSource = this.bdsFiltroCliente;
            this.cmbCGCClienteSigposto.DisplayMember = "Cgc";
            this.cmbCGCClienteSigposto.FormattingEnabled = true;
            this.cmbCGCClienteSigposto.Location = new System.Drawing.Point(112, 27);
            this.cmbCGCClienteSigposto.Name = "cmbCGCClienteSigposto";
            this.cmbCGCClienteSigposto.Size = new System.Drawing.Size(120, 21);
            this.cmbCGCClienteSigposto.TabIndex = 45;
            this.cmbCGCClienteSigposto.ValueMember = "ID";
            this.cmbCGCClienteSigposto.Click += new System.EventHandler(this.cmbCGCClienteSigposto_Click);
            // 
            // cmbNomeCLienteSigPosto
            // 
            this.cmbNomeCLienteSigPosto.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbNomeCLienteSigPosto.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbNomeCLienteSigPosto.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osOrdemServicoBindingSource, "IDCLIENTE", true));
            this.cmbNomeCLienteSigPosto.DataSource = this.bdsFiltroCliente;
            this.cmbNomeCLienteSigPosto.DisplayMember = "Nome";
            this.cmbNomeCLienteSigPosto.Enabled = false;
            this.cmbNomeCLienteSigPosto.FormattingEnabled = true;
            this.cmbNomeCLienteSigPosto.Location = new System.Drawing.Point(308, 0);
            this.cmbNomeCLienteSigPosto.Name = "cmbNomeCLienteSigPosto";
            this.cmbNomeCLienteSigPosto.Size = new System.Drawing.Size(238, 21);
            this.cmbNomeCLienteSigPosto.TabIndex = 1;
            this.cmbNomeCLienteSigPosto.ValueMember = "ID";
            this.cmbNomeCLienteSigPosto.Visible = false;
            this.cmbNomeCLienteSigPosto.SelectionChangeCommitted += new System.EventHandler(this.cmbNomeCLienteSigPosto_SelectionChangeCommitted);
            this.cmbNomeCLienteSigPosto.Click += new System.EventHandler(this.cmbNomeCLienteSigPosto_Click);
            // 
            // cmbCodCLienteSigPosto
            // 
            this.cmbCodCLienteSigPosto.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCodCLienteSigPosto.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCodCLienteSigPosto.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osOrdemServicoBindingSource, "IDCLIENTE", true));
            this.cmbCodCLienteSigPosto.DataSource = this.bdsFiltroCliente;
            this.cmbCodCLienteSigPosto.DisplayMember = "Cliente";
            this.cmbCodCLienteSigPosto.Enabled = false;
            this.cmbCodCLienteSigPosto.FormattingEnabled = true;
            this.cmbCodCLienteSigPosto.Location = new System.Drawing.Point(112, 0);
            this.cmbCodCLienteSigPosto.Name = "cmbCodCLienteSigPosto";
            this.cmbCodCLienteSigPosto.Size = new System.Drawing.Size(91, 21);
            this.cmbCodCLienteSigPosto.TabIndex = 0;
            this.cmbCodCLienteSigPosto.ValueMember = "ID";
            this.cmbCodCLienteSigPosto.Visible = false;
            this.cmbCodCLienteSigPosto.SelectedIndexChanged += new System.EventHandler(this.cmbCodCLienteSigPosto_SelectedIndexChanged);
            this.cmbCodCLienteSigPosto.Click += new System.EventHandler(this.cmbCodCLienteSigPosto_Click);
            this.cmbCodCLienteSigPosto.MouseEnter += new System.EventHandler(this.cmbCodCLienteSigPosto_MouseEnter);
            // 
            // pnlClienteOsExpress
            // 
            this.pnlClienteOsExpress.Controls.Add(this.cmbLogotipoCliente);
            this.pnlClienteOsExpress.Controls.Add(this.cmbCGCCliente);
            this.pnlClienteOsExpress.Controls.Add(this.cmbNomeCliente);
            this.pnlClienteOsExpress.Controls.Add(this.cmbCodCliente);
            this.pnlClienteOsExpress.Location = new System.Drawing.Point(23, 82);
            this.pnlClienteOsExpress.Name = "pnlClienteOsExpress";
            this.pnlClienteOsExpress.Size = new System.Drawing.Size(679, 55);
            this.pnlClienteOsExpress.TabIndex = 38;
            // 
            // cmbLogotipoCliente
            // 
            this.cmbLogotipoCliente.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbLogotipoCliente.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbLogotipoCliente.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osOrdemServicoBindingSource, "IDCLIENTE_PEDIDO", true));
            this.cmbLogotipoCliente.DataSource = this.osClienteBindingSource;
            this.cmbLogotipoCliente.DisplayMember = "NOME_FANTASIA";
            this.cmbLogotipoCliente.FormattingEnabled = true;
            this.cmbLogotipoCliente.Location = new System.Drawing.Point(352, 27);
            this.cmbLogotipoCliente.Name = "cmbLogotipoCliente";
            this.cmbLogotipoCliente.Size = new System.Drawing.Size(194, 21);
            this.cmbLogotipoCliente.TabIndex = 3;
            this.cmbLogotipoCliente.ValueMember = "ID";
            this.cmbLogotipoCliente.Enter += new System.EventHandler(this.cmbLogotipoCliente_Enter);
            // 
            // cmbCGCCliente
            // 
            this.cmbCGCCliente.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCGCCliente.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCGCCliente.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osOrdemServicoBindingSource, "IDCLIENTE_PEDIDO", true));
            this.cmbCGCCliente.DataSource = this.osClienteBindingSource;
            this.cmbCGCCliente.DisplayMember = "CNPJ";
            this.cmbCGCCliente.FormattingEnabled = true;
            this.cmbCGCCliente.Location = new System.Drawing.Point(112, 27);
            this.cmbCGCCliente.Name = "cmbCGCCliente";
            this.cmbCGCCliente.Size = new System.Drawing.Size(120, 21);
            this.cmbCGCCliente.TabIndex = 2;
            this.cmbCGCCliente.ValueMember = "ID";
            // 
            // cmbNomeCliente
            // 
            this.cmbNomeCliente.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbNomeCliente.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbNomeCliente.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osOrdemServicoBindingSource, "IDCLIENTE_PEDIDO", true));
            this.cmbNomeCliente.DataSource = this.osClienteBindingSource;
            this.cmbNomeCliente.DisplayMember = "RAZAO_SOCIAL";
            this.cmbNomeCliente.FormattingEnabled = true;
            this.cmbNomeCliente.Location = new System.Drawing.Point(308, 0);
            this.cmbNomeCliente.Name = "cmbNomeCliente";
            this.cmbNomeCliente.Size = new System.Drawing.Size(238, 21);
            this.cmbNomeCliente.TabIndex = 1;
            this.cmbNomeCliente.ValueMember = "ID";
            this.cmbNomeCliente.SelectionChangeCommitted += new System.EventHandler(this.cmbNomeCliente_SelectionChangeCommitted);
            this.cmbNomeCliente.Click += new System.EventHandler(this.cmbNomeCliente_Click);
            this.cmbNomeCliente.Enter += new System.EventHandler(this.cmbNomeCliente_Enter);
            // 
            // cmbCodCliente
            // 
            this.cmbCodCliente.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCodCliente.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCodCliente.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osOrdemServicoBindingSource, "IDCLIENTE_PEDIDO", true));
            this.cmbCodCliente.DataSource = this.osClienteBindingSource;
            this.cmbCodCliente.DisplayMember = "ID";
            this.cmbCodCliente.FormattingEnabled = true;
            this.cmbCodCliente.Location = new System.Drawing.Point(112, 0);
            this.cmbCodCliente.Name = "cmbCodCliente";
            this.cmbCodCliente.Size = new System.Drawing.Size(91, 21);
            this.cmbCodCliente.TabIndex = 0;
            this.cmbCodCliente.ValueMember = "ID";
            this.cmbCodCliente.SelectionChangeCommitted += new System.EventHandler(this.cmbCodCliente_SelectionChangeCommitted);
            this.cmbCodCliente.Click += new System.EventHandler(this.cmbCodCliente_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(26, 63);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(102, 13);
            this.label16.TabIndex = 37;
            this.label16.Text = "Cadastro de Cliente:";
            this.label16.Visible = false;
            // 
            // rdbOrigOsExpress
            // 
            this.rdbOrigOsExpress.AutoSize = true;
            this.rdbOrigOsExpress.Checked = true;
            this.rdbOrigOsExpress.Enabled = false;
            this.rdbOrigOsExpress.Location = new System.Drawing.Point(134, 61);
            this.rdbOrigOsExpress.Name = "rdbOrigOsExpress";
            this.rdbOrigOsExpress.Size = new System.Drawing.Size(51, 17);
            this.rdbOrigOsExpress.TabIndex = 36;
            this.rdbOrigOsExpress.TabStop = true;
            this.rdbOrigOsExpress.Text = "Novo";
            this.rdbOrigOsExpress.UseVisualStyleBackColor = true;
            this.rdbOrigOsExpress.Visible = false;
            this.rdbOrigOsExpress.CheckedChanged += new System.EventHandler(this.rdbChangedOrigCliente);
            // 
            // rdbOrigSigPosto
            // 
            this.rdbOrigSigPosto.AutoSize = true;
            this.rdbOrigSigPosto.Enabled = false;
            this.rdbOrigSigPosto.Location = new System.Drawing.Point(225, 61);
            this.rdbOrigSigPosto.Name = "rdbOrigSigPosto";
            this.rdbOrigSigPosto.Size = new System.Drawing.Size(68, 17);
            this.rdbOrigSigPosto.TabIndex = 35;
            this.rdbOrigSigPosto.TabStop = true;
            this.rdbOrigSigPosto.Text = "Existente";
            this.rdbOrigSigPosto.UseVisualStyleBackColor = true;
            this.rdbOrigSigPosto.Visible = false;
            this.rdbOrigSigPosto.CheckedChanged += new System.EventHandler(this.rdbChangedOrigCliente);
            // 
            // cmbNomeUsuario
            // 
            this.cmbNomeUsuario.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbNomeUsuario.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbNomeUsuario.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osOrdemServicoBindingSource, "IDUSUARIO", true));
            this.cmbNomeUsuario.DataSource = this.OsXOSFuncionarioBindingSource;
            this.cmbNomeUsuario.DisplayMember = "uNome";
            this.cmbNomeUsuario.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbNomeUsuario.FormattingEnabled = true;
            this.cmbNomeUsuario.Location = new System.Drawing.Point(220, 37);
            this.cmbNomeUsuario.Name = "cmbNomeUsuario";
            this.cmbNomeUsuario.Size = new System.Drawing.Size(228, 21);
            this.cmbNomeUsuario.TabIndex = 2;
            this.cmbNomeUsuario.ValueMember = "idusuario";
            this.cmbNomeUsuario.SelectedIndexChanged += new System.EventHandler(this.cmbNomeUsuario_SelectedIndexChanged);
            this.cmbNomeUsuario.SelectionChangeCommitted += new System.EventHandler(this.cmbNomeUsuario_SelectionChangeCommitted);
            this.cmbNomeUsuario.Click += new System.EventHandler(this.cmbNomeUsuario_Click);
            // 
            // OsXOSFuncionarioBindingSource
            // 
            this.OsXOSFuncionarioBindingSource.DataMember = "XOsFuncionario";
            this.OsXOSFuncionarioBindingSource.DataSource = this.dtsPrincipal;
            this.OsXOSFuncionarioBindingSource.Filter = "isusuario = \'S\'";
            this.OsXOSFuncionarioBindingSource.CurrentChanged += new System.EventHandler(this.OsXOSFuncionarioBindingSource_CurrentChanged);
            // 
            // cmbCodigoUsuario
            // 
            this.cmbCodigoUsuario.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCodigoUsuario.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCodigoUsuario.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osOrdemServicoBindingSource, "IDUSUARIO", true));
            this.cmbCodigoUsuario.DataSource = this.OsXOSFuncionarioBindingSource;
            this.cmbCodigoUsuario.DisplayMember = "idusuario";
            this.cmbCodigoUsuario.FormattingEnabled = true;
            this.cmbCodigoUsuario.Location = new System.Drawing.Point(134, 37);
            this.cmbCodigoUsuario.Name = "cmbCodigoUsuario";
            this.cmbCodigoUsuario.Size = new System.Drawing.Size(70, 21);
            this.cmbCodigoUsuario.TabIndex = 1;
            this.cmbCodigoUsuario.ValueMember = "idusuario";
            this.cmbCodigoUsuario.Click += new System.EventHandler(this.cmbCodigoUsuario_Click);
            this.cmbCodigoUsuario.Validated += new System.EventHandler(this.cmbCodigoUsuario_Validated);
            // 
            // textBox1
            // 
            this.textBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "OBS_INTERNA", true));
            this.textBox1.Location = new System.Drawing.Point(134, 231);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox1.Size = new System.Drawing.Size(672, 79);
            this.textBox1.TabIndex = 7;
            // 
            // txtCodOrdemServico
            // 
            this.txtCodOrdemServico.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "ID", true));
            this.txtCodOrdemServico.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodOrdemServico.ForeColor = System.Drawing.Color.Red;
            this.txtCodOrdemServico.Location = new System.Drawing.Point(134, 11);
            this.txtCodOrdemServico.Name = "txtCodOrdemServico";
            this.txtCodOrdemServico.ReadOnly = true;
            this.txtCodOrdemServico.Size = new System.Drawing.Size(50, 20);
            this.txtCodOrdemServico.TabIndex = 0;
            // 
            // dtpDataCadastro
            // 
            this.dtpDataCadastro.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.osOrdemServicoBindingSource, "DT_CADASTRO", true));
            this.dtpDataCadastro.Enabled = false;
            this.dtpDataCadastro.Location = new System.Drawing.Point(134, 318);
            this.dtpDataCadastro.Name = "dtpDataCadastro";
            this.dtpDataCadastro.Size = new System.Drawing.Size(260, 20);
            this.dtpDataCadastro.TabIndex = 8;
            // 
            // dtpDataFechamento
            // 
            this.dtpDataFechamento.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.osOrdemServicoBindingSource, "DT_FECHAMENTO", true));
            this.dtpDataFechamento.Enabled = false;
            this.dtpDataFechamento.Location = new System.Drawing.Point(134, 344);
            this.dtpDataFechamento.Name = "dtpDataFechamento";
            this.dtpDataFechamento.Size = new System.Drawing.Size(260, 20);
            this.dtpDataFechamento.TabIndex = 9;
            // 
            // txtObservacao
            // 
            this.txtObservacao.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "OBS", true));
            this.txtObservacao.Location = new System.Drawing.Point(134, 140);
            this.txtObservacao.Multiline = true;
            this.txtObservacao.Name = "txtObservacao";
            this.txtObservacao.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtObservacao.Size = new System.Drawing.Size(672, 80);
            this.txtObservacao.TabIndex = 6;
            // 
            // osUsuarioBindingSource
            // 
            this.osUsuarioBindingSource.DataMember = "OsUsuario";
            this.osUsuarioBindingSource.DataSource = this.dtsPrincipal;
            this.osUsuarioBindingSource.Sort = "NOME";
            // 
            // a30CLIENTESBindingSource
            // 
            this.a30CLIENTESBindingSource.DataMember = "A30CLIENTES";
            this.a30CLIENTESBindingSource.DataSource = this.osExpressDataSet;
            // 
            // osExpressDataSet
            // 
            this.osExpressDataSet.DataSetName = "OsExpressDataSet";
            this.osExpressDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // osOrdemParcelaBindingSource
            // 
            this.osOrdemParcelaBindingSource.AllowNew = true;
            this.osOrdemParcelaBindingSource.DataMember = "OsOrdemParcela";
            this.osOrdemParcelaBindingSource.DataSource = this.dtsPrincipal;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Cliente............:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "CNPJ.............:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 60);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "I.E..................:";
            // 
            // tabPecas
            // 
            this.tabPecas.AutoScroll = true;
            this.tabPecas.Controls.Add(this.lbl_grupo);
            this.tabPecas.Controls.Add(this.cmbGrupoProduto);
            this.tabPecas.Controls.Add(this.numQtdeProduto);
            this.tabPecas.Controls.Add(this.txtTotalProduto);
            this.tabPecas.Controls.Add(this.txtPrecoProduto);
            this.tabPecas.Controls.Add(this.cmbCodigoIDProduto);
            this.tabPecas.Controls.Add(this.cmbDescricaoProduto);
            this.tabPecas.Controls.Add(this.cmbCodigoProduto);
            this.tabPecas.Controls.Add(this.grdPecas);
            this.tabPecas.Controls.Add(this.btnAdicionarPecas);
            this.tabPecas.Controls.Add(this.label37);
            this.tabPecas.Controls.Add(this.label35);
            this.tabPecas.Controls.Add(this.label29);
            this.tabPecas.Controls.Add(this.label27);
            this.tabPecas.Controls.Add(this.label12);
            this.tabPecas.Location = new System.Drawing.Point(4, 22);
            this.tabPecas.Name = "tabPecas";
            this.tabPecas.Padding = new System.Windows.Forms.Padding(3);
            this.tabPecas.Size = new System.Drawing.Size(812, 371);
            this.tabPecas.TabIndex = 7;
            this.tabPecas.Text = "Produtos e Serviços";
            this.tabPecas.UseVisualStyleBackColor = true;
            // 
            // lbl_grupo
            // 
            this.lbl_grupo.AutoSize = true;
            this.lbl_grupo.Location = new System.Drawing.Point(27, 10);
            this.lbl_grupo.Name = "lbl_grupo";
            this.lbl_grupo.Size = new System.Drawing.Size(94, 13);
            this.lbl_grupo.TabIndex = 46;
            this.lbl_grupo.Text = "Grupo do Produto:";
            // 
            // cmbGrupoProduto
            // 
            this.cmbGrupoProduto.FormattingEnabled = true;
            this.cmbGrupoProduto.Location = new System.Drawing.Point(127, 6);
            this.cmbGrupoProduto.Name = "cmbGrupoProduto";
            this.cmbGrupoProduto.Size = new System.Drawing.Size(381, 21);
            this.cmbGrupoProduto.TabIndex = 45;
            this.cmbGrupoProduto.SelectionChangeCommitted += new System.EventHandler(this.cmbGrupoProduto_SelectionChangeCommitted);
            // 
            // numQtdeProduto
            // 
            this.numQtdeProduto.Location = new System.Drawing.Point(127, 61);
            this.numQtdeProduto.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numQtdeProduto.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numQtdeProduto.Name = "numQtdeProduto";
            this.numQtdeProduto.Size = new System.Drawing.Size(57, 20);
            this.numQtdeProduto.TabIndex = 2;
            this.numQtdeProduto.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numQtdeProduto.ValueChanged += new System.EventHandler(this.calculaItemPeca);
            // 
            // txtTotalProduto
            // 
            this.txtTotalProduto.Enabled = false;
            this.txtTotalProduto.Location = new System.Drawing.Point(127, 112);
            this.txtTotalProduto.Name = "txtTotalProduto";
            this.txtTotalProduto.ReadOnly = true;
            this.txtTotalProduto.Size = new System.Drawing.Size(85, 20);
            this.txtTotalProduto.TabIndex = 4;
            // 
            // txtPrecoProduto
            // 
            this.txtPrecoProduto.Enabled = false;
            this.txtPrecoProduto.Location = new System.Drawing.Point(127, 86);
            this.txtPrecoProduto.Name = "txtPrecoProduto";
            this.txtPrecoProduto.Size = new System.Drawing.Size(85, 20);
            this.txtPrecoProduto.TabIndex = 3;
            this.txtPrecoProduto.TextChanged += new System.EventHandler(this.calculaItemPeca);
            // 
            // cmbCodigoIDProduto
            // 
            this.cmbCodigoIDProduto.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCodigoIDProduto.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCodigoIDProduto.DataSource = a30PRODUTOBindingSource;
            this.cmbCodigoIDProduto.DisplayMember = "ID";
            this.cmbCodigoIDProduto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCodigoIDProduto.Enabled = false;
            this.cmbCodigoIDProduto.FormattingEnabled = true;
            this.cmbCodigoIDProduto.Location = new System.Drawing.Point(753, 31);
            this.cmbCodigoIDProduto.Name = "cmbCodigoIDProduto";
            this.cmbCodigoIDProduto.Size = new System.Drawing.Size(26, 21);
            this.cmbCodigoIDProduto.TabIndex = 44;
            this.cmbCodigoIDProduto.ValueMember = "ID";
            this.cmbCodigoIDProduto.Visible = false;
            this.cmbCodigoIDProduto.TextChanged += new System.EventHandler(this.calculaItemPeca);
            // 
            // cmbDescricaoProduto
            // 
            this.cmbDescricaoProduto.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbDescricaoProduto.DataSource = a30PRODUTOBindingSource;
            this.cmbDescricaoProduto.DisplayMember = "Descricao";
            this.cmbDescricaoProduto.FormattingEnabled = true;
            this.cmbDescricaoProduto.Location = new System.Drawing.Point(127, 33);
            this.cmbDescricaoProduto.Name = "cmbDescricaoProduto";
            this.cmbDescricaoProduto.Size = new System.Drawing.Size(381, 21);
            this.cmbDescricaoProduto.TabIndex = 1;
            this.cmbDescricaoProduto.ValueMember = "ID";
            this.cmbDescricaoProduto.TextChanged += new System.EventHandler(this.calculaItemPeca);
            // 
            // cmbCodigoProduto
            // 
            this.cmbCodigoProduto.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCodigoProduto.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCodigoProduto.DataSource = a30PRODUTOBindingSource;
            this.cmbCodigoProduto.DisplayMember = "Produto";
            this.cmbCodigoProduto.FormattingEnabled = true;
            this.cmbCodigoProduto.Location = new System.Drawing.Point(633, 31);
            this.cmbCodigoProduto.Name = "cmbCodigoProduto";
            this.cmbCodigoProduto.Size = new System.Drawing.Size(114, 21);
            this.cmbCodigoProduto.TabIndex = 0;
            this.cmbCodigoProduto.TextChanged += new System.EventHandler(this.calculaItemPeca);
            // 
            // grdPecas
            // 
            this.grdPecas.AllowUserToAddRows = false;
            this.grdPecas.AllowUserToOrderColumns = true;
            this.grdPecas.AutoGenerateColumns = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdPecas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.grdPecas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdPecas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.qUANTIDADEDataGridViewTextBoxColumn,
            this.vALORDataGridViewTextBoxColumn,
            this.tOTALDataGridViewTextBoxColumn,
            this.Produto,
            this.Descrição});
            this.grdPecas.DataSource = this.osItemServicoBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdPecas.DefaultCellStyle = dataGridViewCellStyle2;
            this.grdPecas.Location = new System.Drawing.Point(3, 143);
            this.grdPecas.Name = "grdPecas";
            this.grdPecas.ReadOnly = true;
            this.grdPecas.Size = new System.Drawing.Size(806, 150);
            this.grdPecas.TabIndex = 6;
            this.grdPecas.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.grdPecas_UserDeletedRow);
            // 
            // qUANTIDADEDataGridViewTextBoxColumn
            // 
            this.qUANTIDADEDataGridViewTextBoxColumn.DataPropertyName = "QUANTIDADE";
            this.qUANTIDADEDataGridViewTextBoxColumn.HeaderText = "Quantidade";
            this.qUANTIDADEDataGridViewTextBoxColumn.Name = "qUANTIDADEDataGridViewTextBoxColumn";
            this.qUANTIDADEDataGridViewTextBoxColumn.ReadOnly = true;
            this.qUANTIDADEDataGridViewTextBoxColumn.Width = 153;
            // 
            // vALORDataGridViewTextBoxColumn
            // 
            this.vALORDataGridViewTextBoxColumn.DataPropertyName = "VALOR";
            this.vALORDataGridViewTextBoxColumn.HeaderText = "Valor";
            this.vALORDataGridViewTextBoxColumn.Name = "vALORDataGridViewTextBoxColumn";
            this.vALORDataGridViewTextBoxColumn.ReadOnly = true;
            this.vALORDataGridViewTextBoxColumn.Width = 152;
            // 
            // tOTALDataGridViewTextBoxColumn
            // 
            this.tOTALDataGridViewTextBoxColumn.DataPropertyName = "TOTAL";
            this.tOTALDataGridViewTextBoxColumn.HeaderText = "Total";
            this.tOTALDataGridViewTextBoxColumn.Name = "tOTALDataGridViewTextBoxColumn";
            this.tOTALDataGridViewTextBoxColumn.ReadOnly = true;
            this.tOTALDataGridViewTextBoxColumn.Width = 153;
            // 
            // Produto
            // 
            this.Produto.DataPropertyName = "Produto";
            this.Produto.HeaderText = "Produto";
            this.Produto.Name = "Produto";
            this.Produto.ReadOnly = true;
            this.Produto.Width = 152;
            // 
            // Descrição
            // 
            this.Descrição.DataPropertyName = "DESCRICAO";
            this.Descrição.HeaderText = "Descrição";
            this.Descrição.Name = "Descrição";
            this.Descrição.ReadOnly = true;
            this.Descrição.Width = 153;
            // 
            // osItemServicoBindingSource
            // 
            this.osItemServicoBindingSource.AllowNew = true;
            this.osItemServicoBindingSource.DataMember = "OsItemServico";
            this.osItemServicoBindingSource.DataSource = this.dtsPrincipal;
            // 
            // btnAdicionarPecas
            // 
            this.btnAdicionarPecas.Location = new System.Drawing.Point(444, 110);
            this.btnAdicionarPecas.Name = "btnAdicionarPecas";
            this.btnAdicionarPecas.Size = new System.Drawing.Size(75, 23);
            this.btnAdicionarPecas.TabIndex = 5;
            this.btnAdicionarPecas.Text = "Inserir";
            this.btnAdicionarPecas.UseVisualStyleBackColor = true;
            this.btnAdicionarPecas.Click += new System.EventHandler(this.btnAdicionarPecas_Click);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Red;
            this.label37.Location = new System.Drawing.Point(70, 118);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(51, 13);
            this.label37.TabIndex = 35;
            this.label37.Text = "TOTAL:";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(83, 92);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(38, 13);
            this.label35.TabIndex = 13;
            this.label35.Text = "Preço:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(56, 66);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(65, 13);
            this.label29.TabIndex = 11;
            this.label29.Text = "Quantidade:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(529, 37);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(98, 13);
            this.label27.TabIndex = 10;
            this.label27.Text = "Código do Produto:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(8, 39);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(113, 13);
            this.label12.TabIndex = 5;
            this.label12.Text = "Descrição do Produto:";
            // 
            // lblCabCliente
            // 
            this.lblCabCliente.AutoSize = true;
            this.lblCabCliente.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osClienteBindingSource, "RAZAO_SOCIAL", true));
            this.lblCabCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCabCliente.Location = new System.Drawing.Point(87, 41);
            this.lblCabCliente.Name = "lblCabCliente";
            this.lblCabCliente.Size = new System.Drawing.Size(27, 13);
            this.lblCabCliente.TabIndex = 6;
            this.lblCabCliente.Text = "(...)";
            // 
            // lblCabNCPJ
            // 
            this.lblCabNCPJ.AutoSize = true;
            this.lblCabNCPJ.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osClienteBindingSource, "CNPJ", true));
            this.lblCabNCPJ.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCabNCPJ.Location = new System.Drawing.Point(87, 79);
            this.lblCabNCPJ.Name = "lblCabNCPJ";
            this.lblCabNCPJ.Size = new System.Drawing.Size(27, 13);
            this.lblCabNCPJ.TabIndex = 7;
            this.lblCabNCPJ.Text = "(...)";
            // 
            // lblCabIE
            // 
            this.lblCabIE.AutoSize = true;
            this.lblCabIE.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osClienteBindingSource, "IE", true));
            this.lblCabIE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCabIE.Location = new System.Drawing.Point(87, 60);
            this.lblCabIE.Name = "lblCabIE";
            this.lblCabIE.Size = new System.Drawing.Size(27, 13);
            this.lblCabIE.TabIndex = 8;
            this.lblCabIE.Text = "(...)";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 96);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(76, 13);
            this.label10.TabIndex = 10;
            this.label10.Text = "Telefone........:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 114);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(77, 13);
            this.label11.TabIndex = 11;
            this.label11.Text = "Endereço.......:";
            // 
            // lblCabTelefone
            // 
            this.lblCabTelefone.AutoSize = true;
            this.lblCabTelefone.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osClienteBindingSource, "TELEFONE", true));
            this.lblCabTelefone.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCabTelefone.Location = new System.Drawing.Point(87, 96);
            this.lblCabTelefone.Name = "lblCabTelefone";
            this.lblCabTelefone.Size = new System.Drawing.Size(27, 13);
            this.lblCabTelefone.TabIndex = 12;
            this.lblCabTelefone.Text = "(...)";
            // 
            // lblCabObservacao
            // 
            this.lblCabObservacao.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osClienteBindingSource, "ENDERECO", true));
            this.lblCabObservacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCabObservacao.Location = new System.Drawing.Point(87, 113);
            this.lblCabObservacao.Name = "lblCabObservacao";
            this.lblCabObservacao.Size = new System.Drawing.Size(473, 13);
            this.lblCabObservacao.TabIndex = 13;
            this.lblCabObservacao.Text = "(...)";
            // 
            // lblSituacao2
            // 
            this.lblSituacao2.AutoSize = true;
            this.lblSituacao2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSituacao2.ForeColor = System.Drawing.Color.Red;
            this.lblSituacao2.Location = new System.Drawing.Point(9, 133);
            this.lblSituacao2.Name = "lblSituacao2";
            this.lblSituacao2.Size = new System.Drawing.Size(81, 17);
            this.lblSituacao2.TabIndex = 33;
            this.lblSituacao2.Text = "Situação.:";
            // 
            // lblCabSituacao
            // 
            this.lblCabSituacao.AutoSize = true;
            this.lblCabSituacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCabSituacao.ForeColor = System.Drawing.Color.Red;
            this.lblCabSituacao.Location = new System.Drawing.Point(87, 133);
            this.lblCabSituacao.Name = "lblCabSituacao";
            this.lblCabSituacao.Size = new System.Drawing.Size(35, 17);
            this.lblCabSituacao.TabIndex = 34;
            this.lblCabSituacao.Text = "(...)";
            // 
            // osParContasBindingSource
            // 
            this.osParContasBindingSource.DataMember = "OsParContas";
            this.osParContasBindingSource.DataSource = this.dtsPrincipal;
            // 
            // txtTotalFinalOS
            // 
            this.txtTotalFinalOS.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "TOTAL_OS", true));
            this.txtTotalFinalOS.Enabled = false;
            this.txtTotalFinalOS.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalFinalOS.Location = new System.Drawing.Point(711, 33);
            this.txtTotalFinalOS.Name = "txtTotalFinalOS";
            this.txtTotalFinalOS.ReadOnly = true;
            this.txtTotalFinalOS.Size = new System.Drawing.Size(100, 23);
            this.txtTotalFinalOS.TabIndex = 33;
            this.txtTotalFinalOS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtValorAPagar
            // 
            this.txtValorAPagar.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "VALOR_RESTANTE", true));
            this.txtValorAPagar.Enabled = false;
            this.txtValorAPagar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorAPagar.Location = new System.Drawing.Point(711, 91);
            this.txtValorAPagar.Name = "txtValorAPagar";
            this.txtValorAPagar.ReadOnly = true;
            this.txtValorAPagar.Size = new System.Drawing.Size(100, 21);
            this.txtValorAPagar.TabIndex = 47;
            this.txtValorAPagar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // bdsFiltroUsuario
            // 
            this.bdsFiltroUsuario.DataMember = "OsUsuario";
            this.bdsFiltroUsuario.DataSource = this.dtsPrincipal;
            this.bdsFiltroUsuario.Sort = "NOME";
            // 
            // pnlConsultar
            // 
            this.pnlConsultar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlConsultar.Controls.Add(this.label8);
            this.pnlConsultar.Controls.Add(this.pictureBox1);
            this.pnlConsultar.Controls.Add(this.label70);
            this.pnlConsultar.Controls.Add(this.lblTotalOS);
            this.pnlConsultar.Controls.Add(this.groupBox2);
            this.pnlConsultar.Controls.Add(this.osOrdemServicoDataGridView);
            this.pnlConsultar.Controls.Add(this.comboBox8);
            this.pnlConsultar.Location = new System.Drawing.Point(770, 96);
            this.pnlConsultar.Name = "pnlConsultar";
            this.pnlConsultar.Size = new System.Drawing.Size(803, 547);
            this.pnlConsultar.TabIndex = 10;
            this.pnlConsultar.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(8, 156);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(309, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Clique em \'S\' ou dois cliques do mouse para selecionar o Pedido";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::osExpress.Properties.Resources.search;
            this.pictureBox1.Location = new System.Drawing.Point(336, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(51, 50);
            this.pictureBox1.TabIndex = 16;
            this.pictureBox1.TabStop = false;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.Location = new System.Drawing.Point(385, 15);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(122, 31);
            this.label70.TabIndex = 15;
            this.label70.Text = "Consulta";
            // 
            // lblTotalOS
            // 
            this.lblTotalOS.AutoSize = true;
            this.lblTotalOS.Location = new System.Drawing.Point(3, 512);
            this.lblTotalOS.Name = "lblTotalOS";
            this.lblTotalOS.Size = new System.Drawing.Size(37, 13);
            this.lblTotalOS.TabIndex = 13;
            this.lblTotalOS.Text = "Total: ";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkPostoOS);
            this.groupBox2.Controls.Add(this.labelPostoOS);
            this.groupBox2.Controls.Add(this.cmbPostoOS);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.chkFiltrarCliente);
            this.groupBox2.Controls.Add(this.cmbFiltroClienteNome);
            this.groupBox2.Controls.Add(this.chkFiltrarSituacao);
            this.groupBox2.Controls.Add(this.label65);
            this.groupBox2.Controls.Add(this.cmbFiltroSituacao);
            this.groupBox2.Location = new System.Drawing.Point(6, 52);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(784, 99);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = " Filtros ";
            // 
            // chkPostoOS
            // 
            this.chkPostoOS.AutoSize = true;
            this.chkPostoOS.Location = new System.Drawing.Point(405, 63);
            this.chkPostoOS.Name = "chkPostoOS";
            this.chkPostoOS.Size = new System.Drawing.Size(51, 17);
            this.chkPostoOS.TabIndex = 48;
            this.chkPostoOS.Text = "Filtrar";
            this.chkPostoOS.UseVisualStyleBackColor = true;
            this.chkPostoOS.CheckedChanged += new System.EventHandler(this.chkFiltrarSelecoes_CheckedChanged);
            // 
            // labelPostoOS
            // 
            this.labelPostoOS.AutoSize = true;
            this.labelPostoOS.Location = new System.Drawing.Point(5, 65);
            this.labelPostoOS.Name = "labelPostoOS";
            this.labelPostoOS.Size = new System.Drawing.Size(51, 13);
            this.labelPostoOS.TabIndex = 47;
            this.labelPostoOS.Text = "Empresa:";
            // 
            // cmbPostoOS
            // 
            this.cmbPostoOS.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbPostoOS.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbPostoOS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPostoOS.FormattingEnabled = true;
            this.cmbPostoOS.Location = new System.Drawing.Point(114, 61);
            this.cmbPostoOS.Name = "cmbPostoOS";
            this.cmbPostoOS.Size = new System.Drawing.Size(280, 21);
            this.cmbPostoOS.TabIndex = 46;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(287, 21);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(73, 13);
            this.label17.TabIndex = 10;
            this.label17.Text = "Nome Cliente:";
            // 
            // chkFiltrarCliente
            // 
            this.chkFiltrarCliente.AutoSize = true;
            this.chkFiltrarCliente.Location = new System.Drawing.Point(727, 19);
            this.chkFiltrarCliente.Name = "chkFiltrarCliente";
            this.chkFiltrarCliente.Size = new System.Drawing.Size(51, 17);
            this.chkFiltrarCliente.TabIndex = 9;
            this.chkFiltrarCliente.Text = "Filtrar";
            this.chkFiltrarCliente.UseVisualStyleBackColor = true;
            this.chkFiltrarCliente.CheckedChanged += new System.EventHandler(this.chkFiltrarSelecoes_CheckedChanged);
            // 
            // cmbFiltroClienteNome
            // 
            this.cmbFiltroClienteNome.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbFiltroClienteNome.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbFiltroClienteNome.DataSource = this.osOrdemServicoBindingSource;
            this.cmbFiltroClienteNome.DisplayMember = "IDCLIENTE";
            this.cmbFiltroClienteNome.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFiltroClienteNome.FormattingEnabled = true;
            this.cmbFiltroClienteNome.Location = new System.Drawing.Point(367, 17);
            this.cmbFiltroClienteNome.Name = "cmbFiltroClienteNome";
            this.cmbFiltroClienteNome.Size = new System.Drawing.Size(354, 21);
            this.cmbFiltroClienteNome.TabIndex = 8;
            this.cmbFiltroClienteNome.ValueMember = "IDCLIENTE";
            // 
            // chkFiltrarSituacao
            // 
            this.chkFiltrarSituacao.AutoSize = true;
            this.chkFiltrarSituacao.Location = new System.Drawing.Point(226, 19);
            this.chkFiltrarSituacao.Name = "chkFiltrarSituacao";
            this.chkFiltrarSituacao.Size = new System.Drawing.Size(51, 17);
            this.chkFiltrarSituacao.TabIndex = 7;
            this.chkFiltrarSituacao.Text = "Filtrar";
            this.chkFiltrarSituacao.UseVisualStyleBackColor = true;
            this.chkFiltrarSituacao.CheckedChanged += new System.EventHandler(this.chkFiltrarSelecoes_CheckedChanged);
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(5, 19);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(103, 13);
            this.label65.TabIndex = 6;
            this.label65.Text = "Situação do Pedido:";
            // 
            // cmbFiltroSituacao
            // 
            this.cmbFiltroSituacao.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbFiltroSituacao.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbFiltroSituacao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFiltroSituacao.FormattingEnabled = true;
            this.cmbFiltroSituacao.Items.AddRange(new object[] {
            "A - Aberta",
            "F - Fechada",
            "D - Descartada"});
            this.cmbFiltroSituacao.Location = new System.Drawing.Point(114, 17);
            this.cmbFiltroSituacao.Name = "cmbFiltroSituacao";
            this.cmbFiltroSituacao.Size = new System.Drawing.Size(106, 21);
            this.cmbFiltroSituacao.TabIndex = 3;
            // 
            // osOrdemServicoDataGridView
            // 
            this.osOrdemServicoDataGridView.AllowUserToAddRows = false;
            this.osOrdemServicoDataGridView.AllowUserToDeleteRows = false;
            this.osOrdemServicoDataGridView.AllowUserToOrderColumns = true;
            this.osOrdemServicoDataGridView.AutoGenerateColumns = false;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.osOrdemServicoDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.osOrdemServicoDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.osOrdemServicoDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.IDPOSTO,
            this.Usuario,
            this.Cliente,
            this.TOTAL_OS,
            this.dTCADASTRODataGridViewTextBoxColumn,
            this.dTFECHAMENTODataGridViewTextBoxColumn});
            this.osOrdemServicoDataGridView.DataSource = this.osOrdemServicoBindingSource;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.osOrdemServicoDataGridView.DefaultCellStyle = dataGridViewCellStyle5;
            this.osOrdemServicoDataGridView.Location = new System.Drawing.Point(6, 174);
            this.osOrdemServicoDataGridView.Name = "osOrdemServicoDataGridView";
            this.osOrdemServicoDataGridView.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.osOrdemServicoDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.osOrdemServicoDataGridView.Size = new System.Drawing.Size(785, 335);
            this.osOrdemServicoDataGridView.TabIndex = 2;
            this.osOrdemServicoDataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.osOrdemServicoDataGridView_CellDoubleClick);
            this.osOrdemServicoDataGridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.osOrdemServicoDataGridView_KeyDown);
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "# Pedido";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.ReadOnly = true;
            this.iDDataGridViewTextBoxColumn.Width = 75;
            // 
            // IDPOSTO
            // 
            this.IDPOSTO.DataPropertyName = "IDPOSTO";
            this.IDPOSTO.HeaderText = "Empresa";
            this.IDPOSTO.Name = "IDPOSTO";
            this.IDPOSTO.ReadOnly = true;
            this.IDPOSTO.Width = 50;
            // 
            // Usuario
            // 
            this.Usuario.DataPropertyName = "NOME_USUARIO";
            this.Usuario.HeaderText = "Nome Usuário";
            this.Usuario.Name = "Usuario";
            this.Usuario.ReadOnly = true;
            // 
            // Cliente
            // 
            this.Cliente.DataPropertyName = "NOME_CLIENTE";
            this.Cliente.HeaderText = "Nome Cliente";
            this.Cliente.Name = "Cliente";
            this.Cliente.ReadOnly = true;
            this.Cliente.Width = 214;
            // 
            // TOTAL_OS
            // 
            this.TOTAL_OS.DataPropertyName = "TOTAL_OS";
            this.TOTAL_OS.HeaderText = "Valor";
            this.TOTAL_OS.Name = "TOTAL_OS";
            this.TOTAL_OS.ReadOnly = true;
            // 
            // dTCADASTRODataGridViewTextBoxColumn
            // 
            this.dTCADASTRODataGridViewTextBoxColumn.DataPropertyName = "DT_CADASTRO";
            this.dTCADASTRODataGridViewTextBoxColumn.HeaderText = "Dt. Cadastro";
            this.dTCADASTRODataGridViewTextBoxColumn.Name = "dTCADASTRODataGridViewTextBoxColumn";
            this.dTCADASTRODataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dTFECHAMENTODataGridViewTextBoxColumn
            // 
            this.dTFECHAMENTODataGridViewTextBoxColumn.DataPropertyName = "DT_FECHAMENTO";
            this.dTFECHAMENTODataGridViewTextBoxColumn.HeaderText = "Dt. Fechamento";
            this.dTFECHAMENTODataGridViewTextBoxColumn.Name = "dTFECHAMENTODataGridViewTextBoxColumn";
            this.dTFECHAMENTODataGridViewTextBoxColumn.ReadOnly = true;
            this.dTFECHAMENTODataGridViewTextBoxColumn.Width = 110;
            // 
            // comboBox8
            // 
            this.comboBox8.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox8.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox8.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osOrdemServicoBindingSource, "IDCLIENTE", true));
            this.comboBox8.DisplayMember = "ID";
            this.comboBox8.FormattingEnabled = true;
            this.comboBox8.Location = new System.Drawing.Point(662, 203);
            this.comboBox8.Name = "comboBox8";
            this.comboBox8.Size = new System.Drawing.Size(91, 21);
            this.comboBox8.TabIndex = 14;
            this.comboBox8.ValueMember = "ID";
            // 
            // lblCabFatura
            // 
            this.lblCabFatura.AutoSize = true;
            this.lblCabFatura.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "NUMERO_FATURA", true));
            this.lblCabFatura.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCabFatura.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblCabFatura.Location = new System.Drawing.Point(260, 133);
            this.lblCabFatura.Name = "lblCabFatura";
            this.lblCabFatura.Size = new System.Drawing.Size(35, 17);
            this.lblCabFatura.TabIndex = 41;
            this.lblCabFatura.Text = "(...)";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label72.Location = new System.Drawing.Point(198, 133);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(60, 17);
            this.label72.TabIndex = 40;
            this.label72.Text = "Fatura:";
            // 
            // lblNovoCodOS
            // 
            this.lblNovoCodOS.AutoSize = true;
            this.lblNovoCodOS.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "ID", true));
            this.lblNovoCodOS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNovoCodOS.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblNovoCodOS.Location = new System.Drawing.Point(87, 22);
            this.lblNovoCodOS.Name = "lblNovoCodOS";
            this.lblNovoCodOS.Size = new System.Drawing.Size(27, 13);
            this.lblNovoCodOS.TabIndex = 43;
            this.lblNovoCodOS.Text = "(...)";
            this.lblNovoCodOS.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(9, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Pedido.......:";
            // 
            // lblCodigoOS
            // 
            this.lblCodigoOS.AutoSize = true;
            this.lblCodigoOS.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "ID", true));
            this.lblCodigoOS.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigoOS.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblCodigoOS.Location = new System.Drawing.Point(126, 214);
            this.lblCodigoOS.Name = "lblCodigoOS";
            this.lblCodigoOS.Size = new System.Drawing.Size(97, 17);
            this.lblCodigoOS.TabIndex = 44;
            this.lblCodigoOS.Text = "lblCodigoOS";
            // 
            // txtDesconto
            // 
            this.txtDesconto.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "VALOR_DESCONTO", true));
            this.txtDesconto.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesconto.Location = new System.Drawing.Point(711, 62);
            this.txtDesconto.Name = "txtDesconto";
            this.txtDesconto.Size = new System.Drawing.Size(100, 23);
            this.txtDesconto.TabIndex = 0;
            this.txtDesconto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDesconto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TplPermiteSoValor);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label5.Location = new System.Drawing.Point(566, 94);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(139, 17);
            this.label5.TabIndex = 48;
            this.label5.Text = "Valor a Pagar (=):";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label6.Location = new System.Drawing.Point(601, 65);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 17);
            this.label6.TabIndex = 49;
            this.label6.Text = "Desconto (-):";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label7.Location = new System.Drawing.Point(629, 36);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 17);
            this.label7.TabIndex = 50;
            this.label7.Text = "Total (+):";
            // 
            // lblCabTipoPed
            // 
            this.lblCabTipoPed.AutoSize = true;
            this.lblCabTipoPed.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCabTipoPed.ForeColor = System.Drawing.Color.Green;
            this.lblCabTipoPed.Location = new System.Drawing.Point(389, 133);
            this.lblCabTipoPed.Name = "lblCabTipoPed";
            this.lblCabTipoPed.Size = new System.Drawing.Size(35, 17);
            this.lblCabTipoPed.TabIndex = 54;
            this.lblCabTipoPed.Text = "(...)";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Green;
            this.label15.Location = new System.Drawing.Point(338, 133);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(45, 17);
            this.label15.TabIndex = 53;
            this.label15.Text = "Tipo:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bdsFiltroCliente, "Endereco", true));
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(87, 113);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(27, 13);
            this.label9.TabIndex = 59;
            this.label9.Text = "(...)";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bdsFiltroCliente, "Telefone1", true));
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(87, 96);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(27, 13);
            this.label19.TabIndex = 58;
            this.label19.Text = "(...)";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bdsFiltroCliente, "Inscricao", true));
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(87, 60);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(27, 13);
            this.label20.TabIndex = 57;
            this.label20.Text = "(...)";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.SystemColors.Control;
            this.label21.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bdsFiltroCliente, "Cgc", true));
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(87, 79);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(27, 13);
            this.label21.TabIndex = 56;
            this.label21.Text = "(...)";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bdsFiltroCliente, "Nome", true));
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(87, 41);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(27, 13);
            this.label22.TabIndex = 55;
            this.label22.Text = "(...)";
            // 
            // tabParcelas
            // 
            this.tabParcelas.Controls.Add(this.labelIntervalo);
            this.tabParcelas.Controls.Add(this.comboBoxIntervalo);
            this.tabParcelas.Controls.Add(this.numQtdeParcela);
            this.tabParcelas.Controls.Add(this.label23);
            this.tabParcelas.Controls.Add(this.txtEntrada);
            this.tabParcelas.Controls.Add(this.rdbAPartirDiaPrimeiro);
            this.tabParcelas.Controls.Add(this.osOrdemParcelaDataGridView);
            this.tabParcelas.Controls.Add(this.rdbAPartirDataAtual);
            this.tabParcelas.Controls.Add(this.btnInserirParcela);
            this.tabParcelas.Controls.Add(label14);
            this.tabParcelas.Location = new System.Drawing.Point(4, 22);
            this.tabParcelas.Name = "tabParcelas";
            this.tabParcelas.Padding = new System.Windows.Forms.Padding(3);
            this.tabParcelas.Size = new System.Drawing.Size(812, 371);
            this.tabParcelas.TabIndex = 8;
            this.tabParcelas.Text = "Parcelas";
            this.tabParcelas.UseVisualStyleBackColor = true;
            // 
            // labelIntervalo
            // 
            this.labelIntervalo.AutoSize = true;
            this.labelIntervalo.Location = new System.Drawing.Point(277, 14);
            this.labelIntervalo.Name = "labelIntervalo";
            this.labelIntervalo.Size = new System.Drawing.Size(48, 13);
            this.labelIntervalo.TabIndex = 88;
            this.labelIntervalo.Text = "Intervalo";
            // 
            // comboBoxIntervalo
            // 
            this.comboBoxIntervalo.FormattingEnabled = true;
            this.comboBoxIntervalo.Items.AddRange(new object[] {
            "mesualmente",
            "5 dias",
            "10 dias",
            "15 dias",
            "20 dias",
            "25 dias",
            "30 dias"});
            this.comboBoxIntervalo.Location = new System.Drawing.Point(274, 31);
            this.comboBoxIntervalo.Name = "comboBoxIntervalo";
            this.comboBoxIntervalo.Size = new System.Drawing.Size(121, 21);
            this.comboBoxIntervalo.TabIndex = 87;
            // 
            // numQtdeParcela
            // 
            this.numQtdeParcela.Location = new System.Drawing.Point(125, 6);
            this.numQtdeParcela.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.numQtdeParcela.Name = "numQtdeParcela";
            this.numQtdeParcela.Size = new System.Drawing.Size(45, 20);
            this.numQtdeParcela.TabIndex = 0;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(30, 35);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(89, 13);
            this.label23.TabIndex = 86;
            this.label23.Text = "Valor da Entrada:";
            // 
            // txtEntrada
            // 
            this.txtEntrada.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "VALOR_PAGO", true));
            this.txtEntrada.Location = new System.Drawing.Point(125, 32);
            this.txtEntrada.Name = "txtEntrada";
            this.txtEntrada.Size = new System.Drawing.Size(128, 20);
            this.txtEntrada.TabIndex = 1;
            this.txtEntrada.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TplPermiteSoValor);
            // 
            // rdbAPartirDiaPrimeiro
            // 
            this.rdbAPartirDiaPrimeiro.AutoSize = true;
            this.rdbAPartirDiaPrimeiro.Checked = true;
            this.rdbAPartirDiaPrimeiro.Location = new System.Drawing.Point(125, 58);
            this.rdbAPartirDiaPrimeiro.Name = "rdbAPartirDiaPrimeiro";
            this.rdbAPartirDiaPrimeiro.Size = new System.Drawing.Size(103, 17);
            this.rdbAPartirDiaPrimeiro.TabIndex = 2;
            this.rdbAPartirDiaPrimeiro.TabStop = true;
            this.rdbAPartirDiaPrimeiro.Text = "A partir do dia 1º";
            this.rdbAPartirDiaPrimeiro.UseVisualStyleBackColor = true;
            // 
            // osOrdemParcelaDataGridView
            // 
            this.osOrdemParcelaDataGridView.AllowUserToAddRows = false;
            this.osOrdemParcelaDataGridView.AutoGenerateColumns = false;
            this.osOrdemParcelaDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.osOrdemParcelaDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.osOrdemParcelaDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.vALORPARCELADataGridViewTextBoxColumn,
            this.dATAPARCELADataGridViewTextBoxColumn,
            this.nUMEROPARCELADataGridViewTextBoxColumn});
            this.osOrdemParcelaDataGridView.DataSource = this.osOrdemParcelaBindingSource;
            this.osOrdemParcelaDataGridView.Location = new System.Drawing.Point(6, 134);
            this.osOrdemParcelaDataGridView.Name = "osOrdemParcelaDataGridView";
            this.osOrdemParcelaDataGridView.Size = new System.Drawing.Size(800, 227);
            this.osOrdemParcelaDataGridView.TabIndex = 5;
            this.osOrdemParcelaDataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.osOrdemParcelaDataGridView_CellEndEdit);
            // 
            // vALORPARCELADataGridViewTextBoxColumn
            // 
            this.vALORPARCELADataGridViewTextBoxColumn.DataPropertyName = "VALOR_PARCELA";
            dataGridViewCellStyle3.Format = "C2";
            dataGridViewCellStyle3.NullValue = "0,00";
            this.vALORPARCELADataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.vALORPARCELADataGridViewTextBoxColumn.HeaderText = "Valor da Parcela";
            this.vALORPARCELADataGridViewTextBoxColumn.Name = "vALORPARCELADataGridViewTextBoxColumn";
            this.vALORPARCELADataGridViewTextBoxColumn.Width = 120;
            // 
            // dATAPARCELADataGridViewTextBoxColumn
            // 
            this.dATAPARCELADataGridViewTextBoxColumn.DataPropertyName = "DATA_PARCELA";
            this.dATAPARCELADataGridViewTextBoxColumn.HeaderText = "Data da Parcela";
            this.dATAPARCELADataGridViewTextBoxColumn.Name = "dATAPARCELADataGridViewTextBoxColumn";
            this.dATAPARCELADataGridViewTextBoxColumn.Width = 120;
            // 
            // nUMEROPARCELADataGridViewTextBoxColumn
            // 
            this.nUMEROPARCELADataGridViewTextBoxColumn.DataPropertyName = "NUMERO_PARCELA";
            this.nUMEROPARCELADataGridViewTextBoxColumn.HeaderText = "Nº da Parcela";
            this.nUMEROPARCELADataGridViewTextBoxColumn.Name = "nUMEROPARCELADataGridViewTextBoxColumn";
            // 
            // rdbAPartirDataAtual
            // 
            this.rdbAPartirDataAtual.AutoSize = true;
            this.rdbAPartirDataAtual.Location = new System.Drawing.Point(125, 81);
            this.rdbAPartirDataAtual.Name = "rdbAPartirDataAtual";
            this.rdbAPartirDataAtual.Size = new System.Drawing.Size(123, 17);
            this.rdbAPartirDataAtual.TabIndex = 3;
            this.rdbAPartirDataAtual.Text = "A partir da data atual";
            this.rdbAPartirDataAtual.UseVisualStyleBackColor = true;
            // 
            // btnInserirParcela
            // 
            this.btnInserirParcela.Location = new System.Drawing.Point(125, 104);
            this.btnInserirParcela.Name = "btnInserirParcela";
            this.btnInserirParcela.Size = new System.Drawing.Size(87, 23);
            this.btnInserirParcela.TabIndex = 4;
            this.btnInserirParcela.Text = "Gerar Parcelas";
            this.btnInserirParcela.UseVisualStyleBackColor = true;
            this.btnInserirParcela.Click += new System.EventHandler(this.btnInserirParcela_Click);
            // 
            // osOrdemServicoTableAdapter
            // 
            this.osOrdemServicoTableAdapter.ClearBeforeFill = true;
            // 
            // osItemServicoTableAdapter
            // 
            this.osItemServicoTableAdapter.ClearBeforeFill = true;
            // 
            // osParSituacaoTableAdapter
            // 
            this.osParSituacaoTableAdapter.ClearBeforeFill = true;
            // 
            // a30POSTOSTableAdapter
            // 
            this.a30POSTOSTableAdapter.ClearBeforeFill = true;
            // 
            // a30PRODUTOTableAdapter
            // 
            this.a30PRODUTOTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.A30CAIXA_PROG_EXTableAdapter = null;
            this.tableAdapterManager.A30CAIXA_PROGTableAdapter = null;
            this.tableAdapterManager.A30CLIENTESTableAdapter = null;
            this.tableAdapterManager.A30FUNCIONARIOTableAdapter = null;
            this.tableAdapterManager.A30PLANO_CONTATableAdapter = null;
            this.tableAdapterManager.A30POSTOSTableAdapter = null;
            this.tableAdapterManager.A30PRODUTOTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.OsClienteTableAdapter = null;
            this.tableAdapterManager.OsEtiquetaOsClienteTableAdapter = null;
            this.tableAdapterManager.OsEtiquetaTableAdapter = null;
            this.tableAdapterManager.OsFlashDriveTableAdapter = null;
            this.tableAdapterManager.OsFormasPagamentoTableAdapter = null;
            this.tableAdapterManager.OsIbametroInmetroOsClienteTableAdapter = null;
            this.tableAdapterManager.OsIbametroInmetroTableAdapter = null;
            this.tableAdapterManager.OsIbametroOsCliente1TableAdapter = null;
            this.tableAdapterManager.OsIbametroOsClienteTableAdapter = null;
            this.tableAdapterManager.OsIbametroTableAdapter = null;
            this.tableAdapterManager.OsInstrumentoServicoOsClienteTableAdapter = null;
            this.tableAdapterManager.OsInstrumentoServicoTableAdapter = null;
            this.tableAdapterManager.OsItemServicoTableAdapter = this.osItemServicoTableAdapter;
            this.tableAdapterManager.OsOrdemParcelaTableAdapter = this.osOrdemParcelaTableAdapter;
            this.tableAdapterManager.OsOrdemServicoTableAdapter = this.osOrdemServicoTableAdapter;
            this.tableAdapterManager.OsParametrosTableAdapter = null;
            this.tableAdapterManager.OsParContasTableAdapter = null;
            this.tableAdapterManager.OsParSituacaoTableAdapter = null;
            this.tableAdapterManager.OsProdutosServicosTableAdapter = null;
            this.tableAdapterManager.OsReciboTableAdapter = null;
            this.tableAdapterManager.OsSituacoesOSTableAdapter = null;
            this.tableAdapterManager.OsUsuarioTableAdapter = null;
            this.tableAdapterManager.OsVersaoTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = osExpress.OsExpressDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.UpdateInsertDelete;
            this.tableAdapterManager.XOsFuncionarioTableAdapter = null;
            // 
            // osOrdemParcelaTableAdapter
            // 
            this.osOrdemParcelaTableAdapter.ClearBeforeFill = true;
            // 
            // osParContasTableAdapter
            // 
            this.osParContasTableAdapter.ClearBeforeFill = true;
            // 
            // osUsuarioTableAdapter
            // 
            this.osUsuarioTableAdapter.ClearBeforeFill = true;
            // 
            // osOrdemServicoTableAdapterView
            // 
            this.osOrdemServicoTableAdapterView.ClearBeforeFill = true;
            // 
            // osClienteTableAdapter
            // 
            this.osClienteTableAdapter.ClearBeforeFill = true;
            // 
            // a30CLIENTESTableAdapter
            // 
            this.a30CLIENTESTableAdapter.ClearBeforeFill = true;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label24.Location = new System.Drawing.Point(555, 121);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(150, 17);
            this.label24.TabIndex = 61;
            this.label24.Text = "Valor das Parcelas:";
            // 
            // txtTotalDasParcelas
            // 
            this.txtTotalDasParcelas.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "VALOR_RESTANTE", true));
            this.txtTotalDasParcelas.Enabled = false;
            this.txtTotalDasParcelas.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalDasParcelas.Location = new System.Drawing.Point(711, 118);
            this.txtTotalDasParcelas.Name = "txtTotalDasParcelas";
            this.txtTotalDasParcelas.ReadOnly = true;
            this.txtTotalDasParcelas.Size = new System.Drawing.Size(100, 21);
            this.txtTotalDasParcelas.TabIndex = 60;
            this.txtTotalDasParcelas.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label25.Location = new System.Drawing.Point(622, 148);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(83, 17);
            this.label25.TabIndex = 63;
            this.label25.Text = "Diferença:";
            // 
            // txtDifParcelaTotal
            // 
            this.txtDifParcelaTotal.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "VALOR_RESTANTE", true));
            this.txtDifParcelaTotal.Enabled = false;
            this.txtDifParcelaTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDifParcelaTotal.Location = new System.Drawing.Point(711, 145);
            this.txtDifParcelaTotal.Name = "txtDifParcelaTotal";
            this.txtDifParcelaTotal.ReadOnly = true;
            this.txtDifParcelaTotal.Size = new System.Drawing.Size(100, 21);
            this.txtDifParcelaTotal.TabIndex = 62;
            this.txtDifParcelaTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(192, 96);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(71, 13);
            this.label26.TabIndex = 64;
            this.label26.Text = "Contato........:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bdsFiltroCliente, "Contato", true));
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(269, 96);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(27, 13);
            this.label28.TabIndex = 65;
            this.label28.Text = "(...)";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(269, 96);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(27, 13);
            this.label30.TabIndex = 66;
            this.label30.Text = "(...)";
            // 
            // osParametrosBindingSource
            // 
            this.osParametrosBindingSource.DataMember = "OsParametros";
            this.osParametrosBindingSource.DataSource = this.dtsPrincipal;
            // 
            // osParametrosTableAdapter
            // 
            this.osParametrosTableAdapter.ClearBeforeFill = true;
            // 
            // lblCidade
            // 
            this.lblCidade.AutoSize = true;
            this.lblCidade.Location = new System.Drawing.Point(185, 22);
            this.lblCidade.Name = "lblCidade";
            this.lblCidade.Size = new System.Drawing.Size(58, 13);
            this.lblCidade.TabIndex = 67;
            this.lblCidade.Text = "Cidade.....:";
            // 
            // lblCidade2
            // 
            this.lblCidade2.AutoSize = true;
            this.lblCidade2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bdsFiltroCliente, "Cidade", true));
            this.lblCidade2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCidade2.Location = new System.Drawing.Point(248, 22);
            this.lblCidade2.Name = "lblCidade2";
            this.lblCidade2.Size = new System.Drawing.Size(27, 13);
            this.lblCidade2.TabIndex = 68;
            this.lblCidade2.Text = "(...)";
            // 
            // lblUF
            // 
            this.lblUF.AutoSize = true;
            this.lblUF.Location = new System.Drawing.Point(188, 41);
            this.lblUF.Name = "lblUF";
            this.lblUF.Size = new System.Drawing.Size(58, 13);
            this.lblUF.TabIndex = 69;
            this.lblUF.Text = "Estado.....:";
            // 
            // lblUF2
            // 
            this.lblUF2.AutoSize = true;
            this.lblUF2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bdsFiltroCliente, "Estado", true));
            this.lblUF2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUF2.Location = new System.Drawing.Point(249, 41);
            this.lblUF2.Name = "lblUF2";
            this.lblUF2.Size = new System.Drawing.Size(27, 13);
            this.lblUF2.TabIndex = 70;
            this.lblUF2.Text = "(...)";
            // 
            // lblCidadeNovoCliente
            // 
            this.lblCidadeNovoCliente.AutoSize = true;
            this.lblCidadeNovoCliente.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osClienteBindingSource, "CIDADE", true));
            this.lblCidadeNovoCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCidadeNovoCliente.Location = new System.Drawing.Point(301, 22);
            this.lblCidadeNovoCliente.Name = "lblCidadeNovoCliente";
            this.lblCidadeNovoCliente.Size = new System.Drawing.Size(115, 13);
            this.lblCidadeNovoCliente.TabIndex = 71;
            this.lblCidadeNovoCliente.Text = "CidadeNovoCliente";
            // 
            // lblUFNovoCliente
            // 
            this.lblUFNovoCliente.AutoSize = true;
            this.lblUFNovoCliente.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osClienteBindingSource, "UF", true));
            this.lblUFNovoCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUFNovoCliente.Location = new System.Drawing.Point(304, 42);
            this.lblUFNovoCliente.Name = "lblUFNovoCliente";
            this.lblUFNovoCliente.Size = new System.Drawing.Size(115, 13);
            this.lblUFNovoCliente.TabIndex = 72;
            this.lblUFNovoCliente.Text = "EstadoNovoCliente";
            // 
            // cmbEmpresa
            // 
            this.cmbEmpresa.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbEmpresa.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbEmpresa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEmpresa.FormattingEnabled = true;
            this.cmbEmpresa.Items.AddRange(new object[] {
            "A - Aberta",
            "F - Fechada",
            "D - Descartada"});
            this.cmbEmpresa.Location = new System.Drawing.Point(100, 90);
            this.cmbEmpresa.Name = "cmbEmpresa";
            this.cmbEmpresa.Size = new System.Drawing.Size(280, 21);
            this.cmbEmpresa.TabIndex = 45;
            this.cmbEmpresa.SelectionChangeCommitted += new System.EventHandler(this.cmbEmpresa_SelectionChangeCommitted);
            // 
            // lblEmpresa
            // 
            this.lblEmpresa.AutoSize = true;
            this.lblEmpresa.Location = new System.Drawing.Point(25, 95);
            this.lblEmpresa.Name = "lblEmpresa";
            this.lblEmpresa.Size = new System.Drawing.Size(51, 13);
            this.lblEmpresa.TabIndex = 46;
            this.lblEmpresa.Text = "Empresa:";
            // 
            // xOsFuncionarioTableAdapter
            // 
            this.xOsFuncionarioTableAdapter.ClearBeforeFill = true;
            // 
            // ManPedido
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(887, 665);
            this.Controls.Add(this.lblEmpresa);
            this.Controls.Add(this.cmbEmpresa);
            this.Controls.Add(this.pnlConsultar);
            this.Controls.Add(this.bindingNavigator);
            this.Controls.Add(this.lblCodigoOS);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.MaximizeBox = true;
            this.MinimizeBox = true;
            this.Name = "ManPedido";
            this.Text = "Manutenção de Pedidos";
            this.Load += new System.EventHandler(this.ManOrdemServico_Load);
            this.Shown += new System.EventHandler(this.ManOrdemServico_Shown);
            this.Controls.SetChildIndex(this.tabPrincipal, 0);
            this.Controls.SetChildIndex(this.lblCodigoOS, 0);
            this.Controls.SetChildIndex(this.bindingNavigator, 0);
            this.Controls.SetChildIndex(this.grpPrincipal, 0);
            this.Controls.SetChildIndex(this.pnlConsultar, 0);
            this.Controls.SetChildIndex(this.cmbEmpresa, 0);
            this.Controls.SetChildIndex(this.lblEmpresa, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dtsPrincipal)).EndInit();
            this.grpPrincipal.ResumeLayout(false);
            this.grpPrincipal.PerformLayout();
            this.tabPrincipal.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(a30PRODUTOBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator)).EndInit();
            this.bindingNavigator.ResumeLayout(false);
            this.bindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.osOrdemServicoBindingSource)).EndInit();
            this.tabCabecalho.ResumeLayout(false);
            this.tabCabecalho.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.osClienteBindingSource)).EndInit();
            this.pnlClienteSigposto.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bdsFiltroCliente)).EndInit();
            this.pnlClienteOsExpress.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.OsXOSFuncionarioBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.osUsuarioBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.a30CLIENTESBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.osExpressDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.osOrdemParcelaBindingSource)).EndInit();
            this.tabPecas.ResumeLayout(false);
            this.tabPecas.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numQtdeProduto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdPecas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.osItemServicoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.osParContasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsFiltroUsuario)).EndInit();
            this.pnlConsultar.ResumeLayout(false);
            this.pnlConsultar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.osOrdemServicoDataGridView)).EndInit();
            this.tabParcelas.ResumeLayout(false);
            this.tabParcelas.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numQtdeParcela)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.osOrdemParcelaDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.osParametrosBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingNavigator bindingNavigator;
        private System.Windows.Forms.ToolStripButton tsbInserir;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripButton tsbDescartar;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButton6;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton tsbSalvar;
        private System.Windows.Forms.ToolStripButton tsbImprimirPedido;
        private System.Windows.Forms.TabPage tabCabecalho;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.BindingSource osOrdemServicoBindingSource;
        private OsExpressDataSetTableAdapters.OsOrdemServicoTableAdapter osOrdemServicoTableAdapter;
        //private OsExpressDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.TextBox txtCodOrdemServico;
        private System.Windows.Forms.DateTimePicker dtpDataCadastro;
        private System.Windows.Forms.DateTimePicker dtpDataFechamento;
        private System.Windows.Forms.TextBox txtObservacao;
        private System.Windows.Forms.ToolStripButton tsbFecharPedido;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabPage tabPecas;
        private System.Windows.Forms.Label lblCabIE;
        private System.Windows.Forms.Label lblCabNCPJ;
        private System.Windows.Forms.Label lblCabCliente;
        private OsExpressDataSetTableAdapters.OsParSituacaoTableAdapter osParSituacaoTableAdapter;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblCabObservacao;
        private System.Windows.Forms.Label lblCabTelefone;
        private OsExpressDataSetTableAdapters.A30POSTOSTableAdapter a30POSTOSTableAdapter;
        private OsExpressDataSetTableAdapters.A30PRODUTOTableAdapter a30PRODUTOTableAdapter;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.BindingSource osItemServicoBindingSource;
        private OsExpressDataSetTableAdapters.OsItemServicoTableAdapter osItemServicoTableAdapter;
        //private System.Windows.Forms.DataGridViewTextBoxColumn hORATOTALDataGridViewTextBoxColumn1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private OsExpressDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.Button btnAdicionarPecas;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.Label lblCabSituacao;
        private System.Windows.Forms.Label lblSituacao2;
        private System.Windows.Forms.BindingSource osParContasBindingSource;
        private OsExpressDataSetTableAdapters.OsParContasTableAdapter osParContasTableAdapter;
        private System.Windows.Forms.ComboBox cmbCodigoUsuario;
        private System.Windows.Forms.ComboBox cmbNomeUsuario;
        private System.Windows.Forms.BindingSource osUsuarioBindingSource;
        private OsExpressDataSetTableAdapters.OsUsuarioTableAdapter osUsuarioTableAdapter;
        private System.Windows.Forms.TextBox txtTotalFinalOS;
        private System.Windows.Forms.TextBox txtValorAPagar;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDFORMADataGridViewTextBoxColumn;
        private System.Windows.Forms.ToolStripButton tsbCancelar;
        private System.Windows.Forms.BindingSource bdsFiltroUsuario;
        private System.Windows.Forms.BindingSource bdsFiltroCliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDPRODUTODataGridViewTextBoxColumn;
        private System.Windows.Forms.Panel pnlConsultar;
        private System.Windows.Forms.Label lblTotalOS;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox chkFiltrarSituacao;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.ComboBox cmbFiltroSituacao;
        private System.Windows.Forms.DataGridView osOrdemServicoDataGridView;
        private System.Windows.Forms.ComboBox comboBox8;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.PictureBox pictureBox1;
        private OsExpressDataSetTableAdapters.OsOrdemServicoTableAdapter osOrdemServicoTableAdapterView;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblCabFatura;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label lblNovoCodOS;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblCodigoOS;
        private System.Windows.Forms.BindingSource osClienteBindingSource;
        private OsExpressDataSetTableAdapters.OsClienteTableAdapter osClienteTableAdapter;
        private System.Windows.Forms.TextBox txtDesconto;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblCabTipoPed;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.BindingSource osOrdemParcelaBindingSource;
        private OsExpressDataSetTableAdapters.OsOrdemParcelaTableAdapter osOrdemParcelaTableAdapter;
        private System.Windows.Forms.Panel pnlClienteSigposto;
        private System.Windows.Forms.ComboBox cmbNomeCLienteSigPosto;
        private System.Windows.Forms.ComboBox cmbCodCLienteSigPosto;
        private System.Windows.Forms.Panel pnlClienteOsExpress;
        private System.Windows.Forms.ComboBox cmbNomeCliente;
        private System.Windows.Forms.ComboBox cmbCodCliente;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.RadioButton rdbOrigOsExpress;
        private System.Windows.Forms.RadioButton rdbOrigSigPosto;
        private OsExpressDataSetTableAdapters.A30CLIENTESTableAdapter a30CLIENTESTableAdapter;
        private System.Windows.Forms.TextBox mANUTENCAO_MENSALTextBox;
        private System.Windows.Forms.DateTimePicker vENCIMENTO_MANU_MENSALDateTimePicker;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDCLIENTEDataGridViewTextBoxColumn;
        private System.Windows.Forms.TabPage tabParcelas;
        private System.Windows.Forms.RadioButton rdbAPartirDiaPrimeiro;
        private System.Windows.Forms.DataGridView osOrdemParcelaDataGridView;
        private System.Windows.Forms.RadioButton rdbAPartirDataAtual;
        private System.Windows.Forms.Button btnInserirParcela;
        private System.Windows.Forms.TextBox txtEntrada;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.NumericUpDown numQtdeParcela;
        private System.Windows.Forms.DataGridView grdPecas;
        private System.Windows.Forms.DataGridViewTextBoxColumn qUANTIDADEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vALORDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tOTALDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Produto;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descrição;
        private System.Windows.Forms.ToolStripButton tsbGerarCaixa;
        private System.Windows.Forms.NumericUpDown numQtdeProduto;
        private System.Windows.Forms.TextBox txtTotalProduto;
        private System.Windows.Forms.TextBox txtPrecoProduto;
        private System.Windows.Forms.ComboBox cmbDescricaoProduto;
        private System.Windows.Forms.ComboBox cmbCodigoProduto;
        private System.Windows.Forms.ComboBox cmbCodigoIDProduto;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtDifParcelaTotal;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtTotalDasParcelas;
        private System.Windows.Forms.DataGridViewTextBoxColumn vALORPARCELADataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dATAPARCELADataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nUMEROPARCELADataGridViewTextBoxColumn;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ToolStripButton tsbEmail;
        private System.Windows.Forms.BindingSource osParametrosBindingSource;
        private OsExpressDataSetTableAdapters.OsParametrosTableAdapter osParametrosTableAdapter;
        private System.Windows.Forms.Label lbl_grupo;
        private System.Windows.Forms.ComboBox cmbGrupoProduto;
        private System.Windows.Forms.Label labelIntervalo;
        private System.Windows.Forms.ComboBox comboBoxIntervalo;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label lblUF2;
        private System.Windows.Forms.Label lblUF;
        private System.Windows.Forms.Label lblCidade2;
        private System.Windows.Forms.Label lblCidade;
        private System.Windows.Forms.Label lblUFNovoCliente;
        private System.Windows.Forms.Label lblCidadeNovoCliente;
        private System.Windows.Forms.CheckBox cbSingle;
        private System.Windows.Forms.BindingSource a30CLIENTESBindingSource;
        private OsExpressDataSet osExpressDataSet;
        private System.Windows.Forms.ComboBox cmbCGCClienteSigposto;
        private System.Windows.Forms.ComboBox cmbLogotipoClienteSigposto;
        private System.Windows.Forms.Label lblCGCCliente;
        private System.Windows.Forms.Label lblLogotipoCliente;
        private System.Windows.Forms.ComboBox cmbCGCCliente;
        private System.Windows.Forms.ComboBox cmbLogotipoCliente;
        private System.Windows.Forms.CheckBox chkFiltrarCliente;
        private System.Windows.Forms.ComboBox cmbFiltroClienteNome;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox cmbEmpresa;
        private System.Windows.Forms.Label lblEmpresa;
        private System.Windows.Forms.ComboBox cmbPostoOS;
        private System.Windows.Forms.CheckBox chkPostoOS;
        private System.Windows.Forms.Label labelPostoOS;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDPOSTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn Usuario;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn TOTAL_OS;
        private System.Windows.Forms.DataGridViewTextBoxColumn dTCADASTRODataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dTFECHAMENTODataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource OsXOSFuncionarioBindingSource;
        private OsExpressDataSetTableAdapters.XOsFuncionarioTableAdapter xOsFuncionarioTableAdapter;
    }
}
