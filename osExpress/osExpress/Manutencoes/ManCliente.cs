﻿using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TExpress;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.ComponentModel;
using System.Globalization;

namespace osExpress.Manutencoes
{
    public partial class ManCliente : osExpress.FrmPrincipal
    {
        bool cliente_just_inserted = false;
        BindingSource bs = new BindingSource();
        // TabChild orcamento, pedidos, serviços
        BindingSource bs_orcamento = new BindingSource();
        BindingSource bs_pedido = new BindingSource();
        BindingSource bs_servico = new BindingSource();
        BindingSource bs_instrumentos = new BindingSource();
        BindingSource bs_inmetro = new BindingSource();

        public ManCliente()
        {
            InitializeComponent();
            corrigirLayout();

            DB.ConnectionName = "osExpress.Properties.Settings.OsExpressConnectionString";

            // tsbDescartar.Enabled = (string) current["STATUS_OS"] == "A";

            // enable Checkbox S/N (SIM/NAO)
            Binding binding = new Binding("Checked", osClienteBindingSource, "ativo", true);
            binding.Format += new ConvertEventHandler(binding_Format);
            binding.Parse += new ConvertEventHandler(binding_Parse);
            checkboxAtivo.DataBindings.Add(binding);            
                       
            // labelCadastrarCliente.Location = new Point(300, 150);

            listView1.View = View.Details;
            listView1.GridLines = true;
            listView1.FullRowSelect = true;

            //Add column header
            listView1.Columns.Add("Nome", 200);
            //listView1.Columns.Add("R", 30);
            listView1.Columns.Add("Email", 200);
            listView1.Columns.Add("Celular", 120);
            listView1.Columns.Add("CPF", 120);
            listView1.Columns.Add("Obs.", 250);

            listView1.Columns.Add("Princ.", 50);
            listView1.Columns.Add("Resp.", 50);

            listView1.Columns.Add("id", 0);
            listView1.Columns.Add("id_cliente", 0);

            // 10/10/20 future use ...
            // EVENTOS
            listviewEventos.View = View.Details;
            listviewEventos.GridLines = true;
            listviewEventos.FullRowSelect = true;

            //Add column header
            listviewEventos.Columns.Add("Registro", 120);
            listviewEventos.Columns.Add("Realização", 120);
            listviewEventos.Columns.Add("Apertar", 20);
            listviewEventos.Columns.Add("Apontamento", 600);

            listviewEventos.Columns.Add("id", 0);
            listviewEventos.Columns.Add("id_cliente", 0);                   

           
            //
            tabPrincipal.TabPages.Remove(Contatos);
            tabPrincipal.TabPages.Remove(Outros);

            

            FillComboBoxTipoNegocio();
            //comboboxTipoNegocio.SelectedIndex = -1 ;     
            comboboxTipoNegocio.SelectedItem = "Posto";

            datetimeRealizacao.Value = DateTime.Today.AddDays(0);

            // Filtragem
            #region FILTRAGEM
            // Page inicial Consulta
            string xsql =
                @"select id, razao_social nome, nome_fantasia from OsCliente A where 1=1 AND NOT LTRIM(RTRIM(razao_social)) IS NULL";           
           
            var mysource = DB.GetTableFromSQL(xsql);
            bs.DataSource = mysource;
            bs.Sort = "nome";

            // Nome = razao_social
            cmbFiltroClienteNome.DataSource = null;
            cmbFiltroClienteNome.DataSource = bs;
            cmbFiltroClienteNome.DisplayMember = "nome";

            // Id do cliente
            cmbFiltroClienteCodigo.DataSource = null;
            cmbFiltroClienteCodigo.DataSource = bs;
            cmbFiltroClienteCodigo.DisplayMember = "id";

            // Nome = nome_fantasia
            cmbFiltroClienteFantasia.DataSource = null;
            cmbFiltroClienteFantasia.DataSource = bs;
            cmbFiltroClienteFantasia.DisplayMember = "nome_fantasia";
            #endregion
         
        }

        // Fill ComboBox TipoNegocio
        private void FillComboBoxTipoNegocio()
        {
            try
            {
                string sql = @"SELECT id, id_rede, nome_rede FROM OsClienteRede order by id_rede";
                DataTable dt = DB.GetTableFromSQL(sql);
                comboboxTipoNegocio.DataSource = dt;
                comboboxTipoNegocio.DisplayMember = "nome_rede";
                comboboxTipoNegocio.ValueMember = "id";
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }
        private bool IsOutros()
        {
            DataRowView current = (DataRowView)osClienteBindingSource.Current;
            var idcliente = current["id"];
            if( current["bandeira"].ToString().Length > 0) return true;
            if (current["modelo_cpu"].ToString().Length > 0) return true;           
            if (current["codigo_simp"].ToString().Length > 0) return true;

            if (current["qtde_bombas"].ToString().Length > 0) return true;
            if (current["qtde_bicos"].ToString().Length > 0) return true;
            if (current["tipo_bomba"].ToString().Length > 0) return true;

            if (current["qtde_sumpers"].ToString().Length > 0) return true;
            if (current["qtde_tanques"].ToString().Length > 0) return true;
            if (current["concentrador"].ToString().Length > 0) return true; 

            return false;
        }
        // sets Combobox TipoNegocio value
        private void SetTipoNegocio()
        {
            DataRowView current = (DataRowView)osClienteBindingSource.Current;
            var idcliente = current["id"];
            string sql = @"SELECT count(1) FROM OsClienteRedeLink WHERE id_OsCliente = " + idcliente;
            int count = (int)DB.GetSingleValue(sql);
            if (count > 0)
            {
                sql = @"SELECT id_OsClienteRede FROM OsClienteRedeLink WHERE id_OsCliente = " + idcliente;
                int idOsClienteRede = (int)DB.GetSingleValue(sql);

                sql = @"SELECT nome_rede FROM OsClienteRede WHERE id = " + idOsClienteRede;
                string nome_rede = "";
                try
                {
                    nome_rede = DB.GetSingleValue(sql).ToString();
                }
                catch (Exception ex) {
                    nome_rede = "OsClienteRede.id "+idOsClienteRede;
                }

                int index = comboboxTipoNegocio.FindString(nome_rede);
                comboboxTipoNegocio.SelectedIndex = index;
                //uweabril
                //AbaOutros(nome_rede);
            }
            else 
            {
                comboboxTipoNegocio.SelectedIndex = -1;
            }
            checkboxPostoDeCombustivel.Checked = IsOutros();
        }
        // usado pelo checkbox ativo
        private void binding_Format(object sender, ConvertEventArgs e)
        {
            if (e.Value.ToString() == "S") e.Value = true;
            else e.Value = false;
        }
        // usado pelo checkbox ativo
        private void binding_Parse(object sender, ConvertEventArgs e) 
        {
            if ((bool)e.Value) e.Value = "S";
            else e.Value = "N";
        }

        private void corrigirLayout()
        {
            // padronizar janelas, panels, tabcontrols
            pnlConsultar.Location = Helper.Layout.LocationClientePanel; // (10,90)
            tabPrincipal.Location = Helper.Layout.LocationClienteTabControl; // ()

             //public static Point LocationClientePanel = new Point(10, 90);
             //public static Point LocationClienteTabControl = new Point(10, 190);

            pnlConsultar.Size = new Size(1080, 655); //Helper.Layout.SizeClientePanel;
            tabPrincipal.Size = new Size(1080, 555); //Helper.Layout.SizeClienteTabControl;
            //Helper.Layout.SizeClienteTabControl;

            
            
            // layout test start
            // PNLCONSULTAR           
                                   
            pnlConsultar.Size = new Size(1300, 655);          
                        
            int gloc_x = (pnlConsultar.Size.Width - osOrdemServicoDataGridView.Size.Width) / 2;
            int gloc_y = osOrdemServicoDataGridView.Location.Y;
            osOrdemServicoDataGridView.Location = new Point(gloc_x,gloc_y);
            // Filter
            groupBox2.Location = new Point(gloc_x, groupBox2.Location.Y);
            // em vermelho, informar como abrir detalhes referente ao cliente 
            label8.Location = new Point(gloc_x, label8.Location.Y);

            // 07.01.2021
            osOrdemServicoDataGridView.Location = new Point(gloc_x, gloc_y+100);
            label8.Location = new Point(gloc_x, label8.Location.Y+100);

            labelLike.Location = new Point(gloc_x, gloc_y+10);
            tbLike.Location = new Point(gloc_x, gloc_y + 25);
            tbLike.Size = new Size(osOrdemServicoDataGridView.Size.Width, 20);


            // TABPRINCIPAL                      
          
            tabPrincipal.Location = new Point(10, 90);
            //tabPrincipal.Size = new Size(1080, 490);
            tabPrincipal.Size = new Size(1080, 580);
            labelCadastrarCliente.Location = new Point(300, 70);

            tabChild.Location = new Point(10,180);
            //tabChild.Size = new Size(1060, 270);
            tabChild.Size = new Size(1060, 370);
            listView1.Size = new Size(1020,100);

            pnlEditarBomba.Location = new Point(390, 10);
            btnGravarBomba.Location = new Point(19, 295);
            //btnLimparBomba.Location = new Point(104, 295);
            btnCancelar.Location = new Point(104, 295);
            btnLimparBomba.Location = new Point(18, 170);
            btnDeletarBomba.Location = new Point(189, 295);

            rtbHelp2.Location = new Point(15, 10);
            //rtbHelp2.Size = new Size(340, 150);
            rtbHelp2.Size = new Size(250, 250);
            
            // end layout test

           

            // esconder campos originais os quais permanecem ser importante
            // porque os bindings com campos de mascara vem com o problema
            // da mascara ...
            //CNPJ
            cNPJTextBox.Location = mtbCNPJ.Location;
            cNPJTextBox.Size = mtbCNPJ.Size;
            cNPJTextBox.SendToBack();
            //CPF
            cPFTextBox.Location = mtbCPF.Location;
            cPFTextBox.Size = mtbCPF.Size;
            cPFTextBox.SendToBack();
            //Telefone
            tELEFONETextBox.Location = mtbTelefone.Location;
            tELEFONETextBox.Size = mtbTelefone.Size;
            tELEFONETextBox.SendToBack();

            // fazer "filtro" invisible 
            //cmbFiltroClienteCodigo.Visible = false;
            //cmbFiltroClienteCodigo.Enabled = false;
            //chkFiltrarCliente.Visible = false;
            //chkFiltrarCliente.Enabled = false;
            //chkFiltrarSituacao.Visible = false;
            //chkFiltrarSituacao.Enabled = false;
            //label65.Visible = false;
            //label64.Visible = false;
            //cmbFiltroSituacao.Visible = false;
            //cmbFiltroSituacao.Visible = false;
            //cmbFiltroClienteNome.Visible = false;
            //cmbFiltroClienteNome.Enabled = false;

            // posicionar botões
            //btnEventosTodos.Location = new Point(20,25);
            //btnEventosTodos.Size = new Size(150,30);
            //btnEventosConcluidos.Location = new Point(180, 25);
            //btnEventosConcluidos.Size = new Size(150, 30);
            //btnEventosPendentes.Location = new Point(340, 25);
            //btnEventosPendentes.Size = new Size(150, 30);
            textboxFiltroCliente.Visible = false;
            labelFiltroCliente.Visible = false;
            btnEventosTodos.Visible = false;
            btnEventosConcluidos.Visible = false;
            btnEventosPendentes.Visible = false;
            
        }

        private void ManCliente_Load(object sender, EventArgs e)
        {
            //this.WindowState = FormWindowState.Maximized;
            int w = (int)(this.Parent.ClientSize.Width * 0.9);
            int h = (int)(this.Parent.ClientSize.Height * 0.9);
            this.Size = new Size(w, h);
            int x = (int)(this.Parent.ClientSize.Width - w) / 2;
            int y = (int)(this.Parent.ClientSize.Height - h) / 2;
            this.Location = new Point(x, y);

            // TODO: This line of code loads data into the 'dtsPrincipal.OsCliente' table. You can move, or remove it, as needed.
            this.osClienteTableAdapter.Fill(this.dtsPrincipal.OsCliente);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsCliente' table. You can move, or remove it, as needed.
            //this.osClienteTableAdapter.Fill(this.dtsPrincipal.OsCliente);   

            tsbCancelar.Enabled = false;
            tsbInserir.Enabled = true;

            cmbFiltroSituacao.SelectedIndex = 0;
                
            pesquisar();
            tsbDescartar.Enabled = false;
            labelCadastrarCliente.Visible = false;

            // TabChild oracamentos, pedidod, serviços
            dataGridViewOrcamentos.DataSource = bs_orcamento;
            dataGridViewPedidos.DataSource = bs_pedido;
            dataGridViewServicos.DataSource = bs_servico;
            dgvBombas.DataSource = bs_instrumentos;
            dgvLacre.DataSource = bs_inmetro;

            tbSerie.DataBindings.Add(new Binding("Text", bs_instrumentos, "num_serie", true));

            //tbSerie.DataBindings.Add(new Binding("Text", bs_instrumentos, "num_serie", true));
            tbQBicos.DataBindings.Add(new Binding("Text", bs_instrumentos, "cnt", true));
            tbIlha.DataBindings.Add(new Binding("Text", bs_instrumentos, "ilha", true));
            tbDescricao.DataBindings.Add(new Binding("Text", bs_instrumentos, "descricao", true));
            tbFabricante.DataBindings.Add(new Binding("Text", bs_instrumentos, "fabricante", true));

            cmbMarcaInstrumento.DataBindings.Add("SelectedItem", bs_instrumentos, "marca_instrumento");                    
           
        }

        private void  selectCliente()
        {
            DataRowView current = (DataRowView)osClienteBindingSource.Current;
            checkboxRFID.Checked = current["RFID"].ToString().Equals("S") ? true : false;         
           
            if (comboBox2.SelectedItem.ToString().Equals("F"))
            {
                mtbCPF.Enabled = true; 
                mtbCNPJ.Enabled = false;
            }
            if (comboBox2.SelectedItem.ToString().Equals("J"))
            {
                mtbCPF.Enabled = false;
                mtbCNPJ.Enabled = true;
            }

            tabPrincipal.SelectedTab = tabPage1;
            tabChild.SelectedTab = tabEndereco;

            pnlConsultar.Visible = false;
            labelCadastrarCliente.Text = "Cadastro Cliente - alterar";
            labelCadastrarCliente.Visible = true;
            editar();
            tsbDescartar.Enabled = true;

            SetTipoNegocio();
        }

        private void osOrdemServicoDataGridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.S)
            {

                selectCliente();

            }
        }

        private string stripAllButDigits(string str)
        {
            return new string((from c in str
                              where char.IsDigit(c)
                              select c
            ).ToArray());
        }

        private bool isValidCliente()
        {
            // Ou CPF (Pessoa Física) ou CNPJ (Pessoa Jurídica), mas não os dois ...
            string cnpj = stripAllButDigits(mtbCNPJ.Text);
            string cpf = stripAllButDigits(mtbCPF.Text);
            if ( (!mtbCNPJ.MaskFull) & (!mtbCPF.MaskFull) )
            {
                MessageBox.Show(@"Favor preencher ou CPF (Pessoa Física) ou CNPJ (Pessoa Jurídica)",
                                @"E R R O",
                                MessageBoxButtons.OK,MessageBoxIcon.Stop);
                return false;
            }
            // não os dois ...
            if ((mtbCNPJ.MaskFull) & (mtbCPF.MaskFull))
            {
                MessageBox.Show(@"Favor preencher ou CPF (Pessoa Física) ou CNPJ (Pessoa Jurídica)",
                                @"E R R O",
                                MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
            // CNPJ válido ?
            if (comboBox2.SelectedItem.ToString().Equals("J"))
            {               
                if (!Helper.CNPJHelper.IsCnpj(cnpj))
                {
                    MessageBox.Show(@"CNPJ inválido!",
                        @"E R R O",
                        MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return false;
                }
            }
            // CPF válido ?
            if (comboBox2.SelectedItem.ToString().Equals("F"))
            {
                if (!Helper.CPFHelper.IsCpf(cpf))
                {
                    MessageBox.Show(@"CPF inválido!",
                        @"E R R O",
                        MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return false;
                }
            }
            // Email válido ? ( só caso preenchido )
            string email = textBox1.Text;
            if (email.Length > 0)
                if (!Helper.Validation.IsValidEmail(email))
                {
                    MessageBox.Show(@"Email inválido!",
                                   @"E R R O",
                                   MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return false;
                }
            
            // set campos "hidden" (x-coord 20.000 ...)
            // Binding com maskedTextBox não funciona ..
            cNPJTextBox.Text = cnpj;
            cPFTextBox.Text = cpf;
            return true;
        }
        private void confirmar(string aba, EstadoManutencao estado)
        {
            string x = estado == EstadoManutencao.smInserir
                ? " inserido com êxito! "
                : estado == EstadoManutencao.smExcluir
                    ? " excluido com êxito! "
                    : " alterado com êxito! ";
            MessageBox.Show(aba + x, @"I n f o m a ç ã o",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void tsbGravar_Click(object sender, EventArgs e)
        {
            if (!isValidCliente())
                return;
            // Binding com maskedTextBox não funciona ..
            mtbCNPJ.DataBindings.Clear();
            mtbCPF.DataBindings.Clear();
            
            tsbInserir.Enabled = true;
            tsbCancelar.Enabled = false;
            this.Validate();
            //
            DataRowView current = (DataRowView)osClienteBindingSource.Current;
            // Os TextBoxes "cNPJTextBox" e "cPFTextBox" encontram-se fora da area visível. (Location.x > 10000)
            // Não é possível configurar Visible=false porque isso desativaria o Binding !
            // O Binding dos masked-TextBoxes traz a mascara e por isso precisa ser desativado.
            current["CNPJ"] = cNPJTextBox.Text;
            current["CPF"] = cPFTextBox.Text;
            // 
            // ID estadoCorrente == SMINSERIR ...
            // current["RFID"] ist true wenn die Checkbox angeclickt ist ..

            current["RFID"] = "N";
            if (checkboxRFID.Checked) {
                current["RFID"] = "S"; 
            }

            this.osClienteBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dtsPrincipal);

            // gravar TipoNegocio caso SMINSERIR em OsClienteRedeLink
            if (estadoCorrente == EstadoManutencao.smInserir)
            {
                if (comboboxTipoNegocio.SelectedIndex > -1)
                {
                    var item = comboboxTipoNegocio.SelectedItem;
                    string nome_rede = ((DataRowView)item).Row["nome_rede"].ToString();
                    
                    var id_OsCliente = current["id"];
                    var id_OsClienteRede = comboboxTipoNegocio.SelectedValue;

                    // só pode existir um ...link...
                    var del = @"DELETE FROM OsClienteRedeLink WHERE id_OsCliente = " + id_OsCliente;
                    var ins = @"INSERT INTO OsClienteRedeLink(id_OsCliente, id_OsClienteRede)
                        VALUES(" + id_OsCliente + "," + id_OsClienteRede + ")";
                    DB.ExecuteNonQuery(del);
                    DB.ExecuteNonQuery(ins);

                }
            }
            //

            // restore Binding 4 maskedTextBox
            mtbCNPJ.DataBindings.Add(
                new Binding("Text", osClienteBindingSource, "cnpj", true));
            mtbCPF.DataBindings.Add(
                new Binding("Text", osClienteBindingSource, "cpf", true));

            confirmar("Cliente ", estadoCorrente);
            // flag para reabrir o cadastro do cliente
            // para que, agora, ele possa cadastrar contatos e eventos
            if (estadoCorrente == EstadoManutencao.smInserir)
                cliente_just_inserted = true;

            estadoCorrente = EstadoManutencao.smGravar; 
            //MessageBox.Show(@"Gravado com sucesso!");          
            toolStripButton3.Enabled = true;
            pesquisar();
            tsbDescartar.Enabled = false;
            labelCadastrarCliente.Visible = false;
            tabPrincipal.SelectedTab = tabPage1;
            toolStripButton3.PerformClick();
        }

        private void tsbInserir_Click(object sender, EventArgs e)
        {
            checkboxAtivo.Checked = true;

            tabPrincipal.SelectedTab = tabPage1;
            tabChild.SelectedTab = tabEndereco;
            //var labelId = tabPrincipal.TabPages["Dados"].Controls.OfType<Label>().Where(x => x.Name.Contains("labelId"));
            labelCadastrarCliente.Text = "Cadastro Cliente";
            labelCadastrarCliente.Visible = true;
            pnlConsultar.Visible = false;
            tsbInserir.Enabled = false;
            tsbCancelar.Enabled = true;

            // mod 23.03.2015 uwe
            toolStripButton3.Enabled = false; // botão CONSULTAR
            toolStripButton1.Enabled = false; // botão PRIMEIRO
            toolStripButton5.Enabled = false; // botão ANTERIOR

            toolStripButton6.Enabled = false; // botão PROXIMO 
            toolStripButton7.Enabled = false; // botão ULTIMO  
            // end mod 23.03.2015

            inserir();
            if (estadoCorrente == EstadoManutencao.smInserir)
            {
                //cPFTextBox.Text = "";
                //cPFTextBox.Enabled = false;
                //cNPJTextBox.Enabled = true;

                comboBox2.SelectedIndex = 1; // "J" pessoa jurídica
                mtbCPF.Enabled = false;
                mtbCNPJ.Enabled = true;
                mtbCPF.Text = "";
                mtbCNPJ.Text = "";

                checkboxRFID.Checked = false;
            }
           // rAZAO_SOCIALTextBox.Focus();

            // Default : "Posto"
            //int index = comboboxTipoNegocio.FindString("Posto");
            //comboboxTipoNegocio.SelectedIndex = index;
            checkboxPostoDeCombustivel.Checked = true;

            comboBox2.Focus();
        }

        private void tsbCancelar_Click(object sender, EventArgs e)
        {
            tabPrincipal.SelectedTab = tabPage1;
            tabChild.SelectedTab = tabEndereco;
            tsbInserir.Enabled = true;
            tsbCancelar.Enabled = false;
            osClienteBindingSource.CancelEdit();
            pnlConsultar.Visible = true;
            // mod 23.03.2015 uwe
            toolStripButton3.Enabled = true;
            pesquisar();
            tsbDescartar.Enabled = false;
            labelCadastrarCliente.Visible = false;
            // end mod 23.03.2015
        }

        private void reopenOrcamento()
        {
            // isso não funciona bem
            Form form = null;
            Form principal = null;
            FormCollection fc = Application.OpenForms;
            foreach (Form frm in fc)
            {
                if (frm.Name.Equals("ManPedido"))
                {
                    form = frm;                   
                }
                if (frm.Name.Equals("SysPrincipal"))
                {
                    principal = frm;
                }
            }
            if (form != null) {
                form.Dispose();
                form.Close();
            }
           //principal.Invoke(
            Button orca = principal.Controls.Find("btnOrcamentos", true).FirstOrDefault() as Button;
            orca.PerformClick();
            //
            Form form2 = null;
            FormCollection fc2 = null;

            while (form2 == null)
            {
                fc2 = Application.OpenForms;
                foreach (Form frm in fc2)
                {
                    if (frm.Name.Equals("ManPedido"))
                    {
                        form2 = frm;
                    }
                }
            }
            var dummy = 1;
            // NAO FUNCIOMA AQUI
            //BindingNavigator bn = form2.Controls.Find("bindingNavigator", true).FirstOrDefault() as BindingNavigator;
            //ToolStripItem tsi = bn.AddNewItem;
            //tsi.PerformClick();
            
        }
        private void RefreshBindingsOrcamento()
        {
            Form form = null;           
            FormCollection fc = Application.OpenForms;
            foreach (Form frm in fc)
            {
                if (frm.Name.Equals("ManPedido"))
                {
                    form = frm;
                }               
            }
            if (form == null) {
                return;
            }            

            var methodInfo = form.GetType().GetMethod("FillOsClienteFromReflection");
            methodInfo.Invoke(form, null );

            #region analisar e testar
            //var bindingSources = form.GetType().GetFields(System.Reflection.BindingFlags.NonPublic |
            //                        System.Reflection.BindingFlags.Public |
            //                        System.Reflection.BindingFlags.Instance)
            //                        .Where(x => typeof(BindingSource).IsAssignableFrom(x.FieldType))
            //                        .ToDictionary(x => x.Name, x => (BindingSource)x.GetValue(form));
            ////var bs = bindingSources["osOrdemServicoBindingSource"];
            //var bs = bindingSources["osClienteBindingSource"];

            //var SIC = bs.DataSource;
            //bs.DataSource = null; // da pau
            //bs.DataSource = SIC;
            //bs.ResetBindings(false);

            //bs.DataSource = bs.DataSource;
          
            //bs.ResetBindings(true);

            //form.osClienteTableAdapter.Fill(form.dtsPrincipal.OsCliente);

            // DataGridView grid = form.Controls.Find("osOrdemServicoDataGridView", true).FirstOrDefault() as DataGridView;

            //object tableAdapter;
            //DataTable table;

            
            //var methodInfo = tableAdapter.GetType().GetMethod("Fill");
            //methodInfo.Invoke(tableAdapter, new object[] { table });

    //        SELECT        ID, RAZAO_SOCIAL, NOME_FANTASIA, CNPJ, ENDERECO, CIDADE, TELEFONE, REP_LEGAL, IE, BAIRRO, CEP, UF, CONTATO, FAX, CPF, BANDEIRA, MODELO_CPU, QTDE_BICOS, QTDE_BOMBAS, TIPO_BOMBA, OBSERVACAO, 
    //                     CODIGO_SIGPOSTO, INTEGRADO, TIPO_PESSOA, EMAIL, concentrador, RFID, QTDE_SUMPERS, QTDE_TANQUES, CODIGO_SIMP, ativo
    // FROM            OsCliente

            /*
             * comp.GetType().Name = "OsClienteTableAdapter"
             * 		((BindingSource)comp).DataMember	"OsCliente"	string
             * 				((BindingSource)comp).DataSource is DataSet	true	bool


             *  public void Fill()
        {
            var methodInfo = tableAdapter.GetType().GetMethod("Fill");
            methodInfo.Invoke(tableAdapter, new object[] { table });
        }
             * LOOP CLASS PROPERTIES
             * private void ListReflector(objSystem obj)
                {
                    Type type = obj.GetType();
                    PropertyInfo[] properties = type.GetProperties();
    
                    foreach (PropertyInfo property in properties)
                    {
                        //This was more or less copied from online tutorials and if I'm honest, 
                        //I don't understand what these GetValue() parameters actually do.
                        MessageBox.Show("Name: " + property.Name + ", Value: " + property.GetValue(obj, null));  
                    }
                } 

             * */
            // CONTINUE HERE
            /*
            var components = EnumerateComponents(form);
            foreach (var comp in components)
            {
                //Console.Write(number.ToString() + " ");
                Debug.WriteLine(comp);
                if (comp.GetType().Name.Equals("OsClienteTableAdapter")) {
                    Debug.WriteLine("FOUNDIT");
                    DataTable table = new DataTable("OsCliente");
                    var methodInfo = comp.GetType().GetMethod("Fill");
                    //methodInfo.Invoke(comp, new object[] { table });
                    //methodInfo.Invoke(comp, null);
                  // methodInfo.Invoke(comp, new object[] { });
                }
                //((BindingSource)comp).DataMember
                if (comp.GetType().Name.Equals("BindingSource"))
                {
                    if ( ((BindingSource)comp).DataMember.Equals("OsCliente")) {
                      Debug.WriteLine("FOUND BINDINGSOURCE");
                      
                    }
                }
               // if (comp.GetType() is DataSet)
               // {
               //     Debug.WriteLine("FOUND DataSet");
               // }

            }
            */
            #endregion
        }

        private IEnumerable<Component> EnumerateComponents(Form form)
        {
            return from field in form.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                   where typeof(Component).IsAssignableFrom(field.FieldType)
                   let component = (Component)field.GetValue(form)
                   where component != null
                   select component;
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            osClienteBindingSource.CancelEdit();
            pnlConsultar.Visible = true;
            this.osClienteTableAdapter.Fill(this.dtsPrincipal.OsCliente);
            pesquisar();
            tsbDescartar.Enabled = false;
            labelCadastrarCliente.Visible = false;
            tabPrincipal.SelectedTab = tabPage1;
            tabChild.SelectedTab = tabEndereco;

            // depois do cadastro de novo cliente
            // reabrir o cadastro para poder, agora,
            // acionar contatos e eventos.
            if (cliente_just_inserted)
            {
                cliente_just_inserted = false;
               
                // acionar DoubleClick on DataGridView via Reflection
                int rowIndex = osOrdemServicoDataGridView.Rows.Count - 1;

                DataGridView grid = this.Controls.Find("osOrdemServicoDataGridView", true).FirstOrDefault() as DataGridView;
                MethodInfo onCellDoubleClick = typeof(DataGridView).GetMethod("OnCellDoubleClick",
                                                                    BindingFlags.NonPublic | BindingFlags.Instance);
                DataGridViewCellEventArgs ea = new DataGridViewCellEventArgs(0, rowIndex);
                onCellDoubleClick.Invoke(grid, new object[] { ea });

                // select Tab Eventos
                TabControl tabc = this.Controls.Find("TabChild", true).FirstOrDefault() as TabControl;
                tabc.SelectedTab = tabc.TabPages["tabEventos"];

                //reopenOrcamento();
                RefreshBindingsOrcamento();
            }
        }

        private void osOrdemServicoDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            // necessário porque quando acionado via reflection from SysPrincipal
            // osClienteBindingSource.Position = 0 !!
            osClienteBindingSource.Position = e.RowIndex;

            selectCliente();
        }

        private void tsbDesativar_Click(object sender, EventArgs e)
        {
            
        }

        private void cmbFiltroClienteNome_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        #region somente DIGITOS
        // mod 23.03.2015 uwe
        /// <summary>
        /// Rejeitar tudo que não seja um digito 
        /// </summary>       
        private void iETextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);            
        }

        private void iETextBox_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(iETextBox.Text, "[^0-9]"))
            {
                // MessageBox.Show(@"Favor só preencher digitos IE");                            
                // iETextBox.Text = "";
            }
        }

        private void qTDE_BOMBASTextBox_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(qTDE_BOMBASTextBox.Text, "[^0-9]"))
            {
                MessageBox.Show(@"Favor só preencher digitos A");
                qTDE_BOMBASTextBox.Text = "";
            }
        }

        private void qTDE_BICOSTextBox_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(qTDE_BICOSTextBox.Text, "[^0-9]"))
            {
                MessageBox.Show(@"Favor só preencher digitos B");
                qTDE_BICOSTextBox.Text = "";
            }
        }

        private void qTDE_BICOSTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);   
        }

        private void qTDE_BOMBASTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        //contatos
        private void maskedTextboxContatosCpf_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void maskedTextboxContatosCpf_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(stripAllButDigits(maskedTextboxContatosCpf.Text), "[^0-9]"))
            {
                MessageBox.Show(@"Favor só preencher digitos C");
                maskedTextboxContatosCpf.Text = "";
            }
        }
         
        private void maskedTextboxContatosCelular_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void maskedTextboxContatosCelular_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(stripAllButDigits(maskedTextboxContatosCelular.Text), "[^0-9]"))
            {
                MessageBox.Show(@"Favor só preencher digitos D");
                maskedTextboxContatosCelular.Text = "";
            }
        }
       
        #endregion

        private void comboBox2_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (comboBox2.SelectedItem.ToString().Equals("F"))
            {               
                // if (estadoCorrente == EstadoManutencao.smInserir)
                {                   
                    mtbCNPJ.Text = "";
                    mtbCNPJ.Enabled = false;
                    mtbCPF.Enabled = true;
                }
            }
            if (comboBox2.SelectedItem.ToString().Equals("J"))
            {               
                //if (estadoCorrente == EstadoManutencao.smInserir)
                {                   
                    mtbCPF.Text = "";
                    mtbCPF.Enabled = false;
                    mtbCNPJ.Enabled = true;
                }
            }
        }

        private void tabPrincipal_SelectedIndexChanged(object sender, EventArgs e)
        {            
        }

        private void limparContatosCampos()
        {
            textboxContatosEmail.Text = "";
            textboxContatosNome.Text = "";
            textboxContatosObs.Text = "";
            maskedTextboxContatosCelular.Text = "";
            maskedTextboxContatosCpf.Text = "";
            textboxHiddenId.Text = "";
            checkBox1.Checked = false;
            checkboxPrincipal.Checked = false;
            //estadoCorrente = EstadoManutencao.ccCreate;
            create();
        }
        private void limparEventosCampos()
        {
            textboxApontamento.Text = "";
            datetimeRealizacao.Value = DateTime.Today.AddDays(0);            
            //textboxHiddenId.Text = "";
            textboxHiddenIdEvento.Text = "";
            checkboxAlertar.Checked = false;
            textboxApontamento.Text = "";
            //estadoCorrente = EstadoManutencao.ceCreate;
            createEvento();
        }
        private string formatCelular(string celular)
        {
            if (celular.Length != 11) return celular; 
            string ret = Convert.ToUInt64(celular).ToString(@"(00) 0 0000-0000");
            return ret;
        }
        private string formatCpf(string cpf)
        {
            if (cpf.Length != 11) return cpf; 
            string ret = Convert.ToUInt64(cpf).ToString(@"000\.000\.000\-00");
            return ret;
        }
        private void fillList(int id_cliente) {
            // create listview with eventhandler ...
            string sql = @"SELECT id, id_cliente, celular, email, cpf, obs, nome, principal, responsavel FROM OsClienteContato
                       WHERE id_cliente = " + id_cliente;
            DataTable dt = DB.GetTableFromSQL(sql);

            // limpa o ListView
            listView1.Items.Clear();

            // exibe os itens no controle ListView 
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow drow = dt.Rows[i];

                // Somente as linhas que não foram deletadas
                if (drow.RowState != DataRowState.Deleted)
                {
                    // Define os itens da lista
                    ListViewItem lvi = new ListViewItem(drow["nome"].ToString());
                    //lvi.SubItems.Add(drow["responsavel"].ToString());
                    lvi.SubItems.Add(drow["email"].ToString());
                    lvi.SubItems.Add( formatCelular(drow["celular"].ToString()));
                    lvi.SubItems.Add( formatCpf(drow["cpf"].ToString()) );
                    lvi.SubItems.Add(drow["obs"].ToString());
                    lvi.SubItems.Add(drow["principal"].ToString());
                    lvi.SubItems.Add(drow["responsavel"].ToString());

                    lvi.SubItems.Add(drow["id"].ToString());
                    lvi.SubItems.Add(drow["id_cliente"].ToString());

                    // Inclui os itens no ListView
                    listView1.Items.Add(lvi);
                }
            }
        
        } 
        private void fillListEventos(int id_cliente)
        {
            // 
            string sql = @"SELECT id, id_cliente, datahora_registro, datahora_realizacao, 
                        apontamento, alertar, obs FROM OsClienteEvento
                       WHERE id_cliente = " + id_cliente;
            DataTable dt = DB.GetTableFromSQL(sql);

            // limpa o ListView
            listviewEventos.Items.Clear();

            // exibe os itens no controle ListView 
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow drow = dt.Rows[i];

                // Somente as linhas que não foram deletadas
                if (drow.RowState != DataRowState.Deleted)
                {
                    // Define os itens da lista                   

                    DateTime dx = (DateTime)drow["datahora_registro"];
                   
                    ListViewItem lvi = new ListViewItem(dx.ToString("dd/MM/yyyy HH:mm"));
                    dx = (DateTime)drow["datahora_realizacao"];
                    lvi.SubItems.Add(dx.ToString("dd/MM/yyyy HH:mm"));
                   
                    lvi.SubItems.Add(drow["alertar"].ToString());
                    lvi.SubItems.Add(drow["apontamento"].ToString());
                   
                    
                    lvi.SubItems.Add(drow["id"].ToString());
                    lvi.SubItems.Add(drow["id_cliente"].ToString());

                    // Inclui os itens no ListView
                    listviewEventos.Items.Add(lvi);
                }
            }

        }
        private int countContatos(int id_cliente)
        {                     
            string sql = @"SELECT count(*) cnt FROM OsClienteContato WHERE id_cliente = " + id_cliente.ToString();
            int cnt = (int)DB.GetSingleValue(sql);
            return cnt;
        }
        private int countEventos(int id_cliente)
        {
            string sql = @"SELECT count(*) cnt FROM OsClienteEvento WHERE id_cliente = " + id_cliente.ToString();
            int cnt = (int)DB.GetSingleValue(sql);
            return cnt;
        }
        private void tabContatos_Enter(object sender, EventArgs e)
        {
            if (estadoCorrente == EstadoManutencao.smInserir)
            {
                MessageBox.Show(@"Favor cadastrar primeiro o cliente",
                                @"I N F O",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                tabChild.SelectedTab = tabEndereco;
                return;
            }

            create();

            limparContatosCampos();
            listView1.Items.Clear();


            DataRowView current = (DataRowView)osClienteBindingSource.Current;

            var id_cliente = current["id"];
            var nome_cliente = current["razao_social"];

            int cnt = countContatos((int)id_cliente);
            
            foreach (Control c in tabChild.TabPages["tabContatos"].Controls)
            {
                if (c.GetType() == typeof(Label))
                {
                    if (c.Name == "labelContatosCliente")
                        c.Text = nome_cliente.ToString();
                    if (c.Name == "labelContatosNumero")
                        c.Text = "Número dos contatos: " + cnt.ToString();
                }
            }

            if (cnt > 0)
            {
                fillList((int)id_cliente);
            }
        }  
       
        private void setLabelText(string label, string text) 
        {
            //foreach (Control c in tabPrincipal.TabPages["Contatos"].Controls)
            foreach (Control c in tabChild.TabPages["tabContatos"].Controls)
            {
                if (c.GetType() == typeof(Label))
                {
                    if (c.Name == label)
                        c.Text = text;
                }
            }
        }
        private void setLabelTextEventos(string label, string text)
        {            
            foreach (Control c in tabChild.TabPages["tabEventos"].Controls)
            {
                if (c.GetType() == typeof(Label))
                {
                    if (c.Name == label)
                        c.Text = text;
                }
            }
        }
        // inserir Contato
        private void buttonContatosGravar_Click(object sender, EventArgs e)
        {
            DataRowView current = (DataRowView)osClienteBindingSource.Current;
            var id_cliente = current["id"];
            var nome_cliente = current["razao_social"];

            #region validar Nome
            string nome = textboxContatosNome.Text;
            if (String.IsNullOrWhiteSpace(textboxContatosNome.Text))
            {
                MessageBox.Show(@"Nome obrigatório!",
                                   @"E R R O",
                                   MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            #endregion
            #region validar email
            string email = textboxContatosEmail.Text;
            if (email.Length > 0)
                if (!Helper.Validation.IsValidEmail(email))
                {
                    MessageBox.Show(@"Email inválido!",
                                   @"E R R O",
                                   MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            #endregion
            #region validar CPF
            
            string cpf = stripAllButDigits(maskedTextboxContatosCpf.Text);
            if (cpf.Length > 0 && cpf.Length != 11)
            {
                MessageBox.Show(@"Favor informar 11 digitos de CPF!",
                                  @"E R R O",
                                  MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (cpf.Length > 0) {
                if (!Helper.CPFHelper.IsCpf(cpf))
                {
                    MessageBox.Show(@"CPF inválido!",
                        @"E R R O",
                        MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            #endregion
            #region validar celular
            string celular = stripAllButDigits(maskedTextboxContatosCelular.Text);
            if (celular.Length > 0 && celular.Length != 11)
            {
                MessageBox.Show(@"Favor informar 11 digitos de Celular!",
                                  @"E R R O",
                                  MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            #endregion
            #region validar responsavel
            // responsavel precisa informar CPF
            string R = "N";
            if (checkBox1.Checked && cpf.Length == 0) 
            {
                MessageBox.Show(@"Responsável precisa informar CPF!",
                       @"E R R O",
                       MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            #endregion
            
            #region validar responsavel
            // principal precisa informar email
            //string principal = stripAllButDigits(maskedTextboxContatosCpf.Text);
            string P = "N";
            if (checkboxPrincipal.Checked && email.Length == 0)
            {
                MessageBox.Show(@"Principal precisa informar email!",
                       @"E R R O",
                       MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            #endregion


            //string celular = stripAllButDigits(maskedTextboxContatosCelular.Text);
            //string email = textboxContatosEmail.Text;
            //string cpf = maskedTextboxContatosCpf.Text;
            string obs = textboxContatosObs.Text;
            //string nome = textboxContatosNome.Text;
            // R = responsavel (S|N)


            // 
            string hiddenId = textboxHiddenId.Text;

            if (checkBox1.Checked) R = "S";
            // somente um responsável pode existir

            string sql_count_responsavel =
                //@"SELECT count(*) anz FROM OsClienteContato WHERE id_cliente=" + id_cliente + " AND responsavel='S'";
            @"SELECT count(*) anz FROM OsClienteContato WHERE id_cliente="
             + id_cliente;

            if (!hiddenId.Equals(""))
                sql_count_responsavel += " AND NOT id = " + hiddenId;
             
             sql_count_responsavel +=  " AND responsavel='S'";

            int count_responsavel = (int)DB.GetSingleValue(sql_count_responsavel);
            if (R.Equals("S") && count_responsavel > 0) {
                MessageBox.Show(@"Responsável somente pode ser um!",
                       @"E R R O",
                       MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (checkboxPrincipal.Checked) P = "S";
            // somente um principal pode existir

            string sql_count_principal =
                //@"SELECT count(*) anz FROM OsClienteContato WHERE id_cliente=" + id_cliente + " AND principal='S'";
            @"SELECT count(*) anz FROM OsClienteContato WHERE id_cliente="
            + id_cliente;
            
            //+ " AND NOT id = " + hiddenId
            if (!hiddenId.Equals(""))
                sql_count_principal += " AND NOT id = " + hiddenId;
            sql_count_principal += " AND principal='S'";

            int count_principal = (int)DB.GetSingleValue(sql_count_principal);
            if (P.Equals("S") && count_principal > 0)
            {
                MessageBox.Show(@"Principal somente pode ser um!",
                       @"E R R O",
                       MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            string sql_insert = @"INSERT INTO OsClienteContato(id_cliente, celular, email,responsavel,principal, cpf, obs, nome) VALUES("
                              + id_cliente
                              + ",'" + celular + "'"
                              + ",'" + email + "'"
                               + ",'" + R + "'"
                               + ",'" + P + "'"
                              + ",'" + cpf + "'"
                              + ",'" + obs + "'"
                              + ",'" + nome + "')";
            //string hiddenId = textboxHiddenId.Text;
            string sql_update = @"UPDATE OsClienteContato set "
                              + "nome = '" + nome + "',"
                              + "celular = '" + celular + "',"
                              + "email = '" + email + "',"
                              + "responsavel = '" + R + "',"
                              + "principal = '" + P + "',"
                              + "cpf = '" + cpf + "',"
                              + "obs = '" + obs + "' "
                              + "WHERE id=" + hiddenId;
 
            if (estadoCorrente == EstadoManutencao.ccCreate) 
              DB.ExecuteNonQuery(sql_insert);
            if (estadoCorrente == EstadoManutencao.ccUpdate)
                DB.ExecuteNonQuery(sql_update);
            limparContatosCampos();
            fillList((int)id_cliente);
            int cnt = countContatos((int) id_cliente);
            setLabelText("labelContatosNumero", "Número dos contatos: " + cnt.ToString());
            create();
            buttonContatosDeletar.Visible = false;
        } 

        // visible so no estado alterar !!
        private void buttonContatosDeletar_Click(object sender, EventArgs e)
        {
            if (!(estadoCorrente == EstadoManutencao.ccUpdate))
                return;

            DataRowView current = (DataRowView)osClienteBindingSource.Current;
            var id_cliente = current["id"];
 
            string hiddenId = textboxHiddenId.Text;
            string sql_delete = @"DELETE FROM OsClienteContato WHERE id = " + hiddenId;

            var confirmResult = MessageBox.Show("Tem certeza que queira apagar este contato ??",
                                     "Confirmar",
                                     MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                // If 'Yes', do something here.
                DB.ExecuteNonQuery(sql_delete);
                limparContatosCampos();
                fillList((int)id_cliente);
                int cnt = countContatos((int)id_cliente);
                setLabelText("labelContatosNumero", "Número dos contatos: " + cnt.ToString());
                create();
                buttonContatosDeletar.Visible = false;
            }
            else
            {
                // nada pra fazer
            }
            
        }
       
        private void tabContatos_Leave(object sender, EventArgs e)
        {
            if (estadoCorrente != EstadoManutencao.smInserir)
              editar();
            buttonContatosDeletar.Visible = false;
        }
        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                ListViewItem item = listView1.SelectedItems[0];
                textboxContatosNome.Text = item.SubItems[0].Text;
                
                // checkbox principal
                if (item.SubItems[5].Text.ToString().Equals("N"))
                    checkboxPrincipal.Checked = false;
                else
                    checkboxPrincipal.Checked = true;
                // checkbox responsavel
                if (item.SubItems[6].Text.ToString().Equals("N"))
                    checkBox1.Checked = false;
                else
                    checkBox1.Checked = true;
                
                textboxContatosEmail.Text = item.SubItems[1].Text;
                maskedTextboxContatosCelular.Text = stripAllButDigits(item.SubItems[2].Text);
                maskedTextboxContatosCpf.Text = stripAllButDigits(item.SubItems[3].Text);
                textboxContatosObs.Text = item.SubItems[4].Text;

                //textboxHiddenId.Text = item.SubItems[6].Text;
                textboxHiddenId.Text = item.SubItems[7].Text;
                //tssStatus.Text = "Status: Alterar Contato ...";
                //estadoCorrente = EstadoManutencao.ccUpdate;
                update();
                buttonContatosDeletar.Visible = true;
            }
            else
            {
                textboxContatosObs.Text = "listView1.SelectedItems.Count is 0!";
            }
        }

        private void buttonContatosCancelar_Click(object sender, EventArgs e)
        {
            limparContatosCampos();            
            create();
            buttonContatosDeletar.Visible = false;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                //checkBox1.Text = "Checked";
            }
            else
            {
                //checkBox1.Text = "Unchecked";
            }  
        }

        private void fillListFromCurrent()
        {
            limparContatosCampos();
            DataRowView current = (DataRowView)osClienteBindingSource.Current;
            var id_cliente = current["id"];
            fillList((int)id_cliente);           
            var rfid = current["RFID"];
            if (rfid == System.DBNull.Value)
            {
               checkboxRFID.Checked = false;
               current["RFID"] = "N";
            }
            else
             checkboxRFID.Checked = current["RFID"].Equals("S") ? true : false;


            int cnt = countContatos((int)id_cliente);
            setLabelTextEventos("labelContatosNumero", "Número dos contatos : " + cnt.ToString());
            estadoCorrente = EstadoManutencao.ccCreate;
            create();
           
        }
        private void fillListEventosFromCurrent()
        {
            limparEventosCampos();
           
            DataRowView current = (DataRowView)osClienteBindingSource.Current;
            var id_cliente = current["id"];
            fillListEventos((int)id_cliente);

            int cnt = countEventos((int)id_cliente);
            setLabelTextEventos("labelEventosNumero", "Número dos eventos : " + cnt.ToString());
            estadoCorrente = EstadoManutencao.ceCreate;
            createEvento();
        }
        // proximo
        private void toolStripButton6_Click(object sender, EventArgs e)
        {           
            if (tabChild.SelectedTab == tabChild.TabPages["tabContatos"])
              fillListFromCurrent();
            if (tabChild.SelectedTab == tabChild.TabPages["tabEventos"])
              fillListEventosFromCurrent();
            if (tabChild.SelectedTab == tabChild.TabPages["tabInstrumentos"])
                bindBomba();
            if (tabChild.SelectedTab == tabChild.TabPages["tabPedidos"])
                bindPedidos();
           
            if (tabChild.SelectedTab == tabChild.TabPages["tabOrcamentos"])
                bindOrcamentos();
            if (tabChild.SelectedTab == tabChild.TabPages["tabServicos"])
                bindServicos();
        }
        // anterior
        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            if (tabChild.SelectedTab == tabChild.TabPages["tabContatos"])
                fillListFromCurrent();
            if (tabChild.SelectedTab == tabChild.TabPages["tabEventos"])
                fillListEventosFromCurrent();
            if (tabChild.SelectedTab == tabChild.TabPages["tabInstrumentos"])
                bindBomba();
            if (tabChild.SelectedTab == tabChild.TabPages["tabPedidos"])
                bindPedidos();
            if (tabChild.SelectedTab == tabChild.TabPages["tabOrcamentos"])
                bindOrcamentos();
            if (tabChild.SelectedTab == tabChild.TabPages["tabServicos"])
                bindServicos();
        }
        // primeiro
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if (tabChild.SelectedTab == tabChild.TabPages["tabContatos"])
                fillListFromCurrent();
            if (tabChild.SelectedTab == tabChild.TabPages["tabEventos"])
                fillListEventosFromCurrent();
            if (tabChild.SelectedTab == tabChild.TabPages["tabInstrumentos"])
                bindBomba();
            if (tabChild.SelectedTab == tabChild.TabPages["tabPedidos"])
                bindPedidos();
            if (tabChild.SelectedTab == tabChild.TabPages["tabOrcamentos"])
                bindOrcamentos();
            if (tabChild.SelectedTab == tabChild.TabPages["tabServicos"])
                bindServicos();
        }
        // último
        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            if (tabChild.SelectedTab == tabChild.TabPages["tabContatos"])
                fillListFromCurrent();
            if (tabChild.SelectedTab == tabChild.TabPages["tabEventos"])
                fillListEventosFromCurrent();
            if (tabChild.SelectedTab == tabChild.TabPages["tabInstrumentos"])
                bindBomba();
            if (tabChild.SelectedTab == tabChild.TabPages["tabPedidos"])
                bindPedidos();
            if (tabChild.SelectedTab == tabChild.TabPages["tabOrcamentos"])
                bindOrcamentos();
            if (tabChild.SelectedTab == tabChild.TabPages["tabServicos"])
                bindServicos();
        }

        private void checkboxPostoDeCombustivel_CheckedChanged(object sender, EventArgs e)
        {
            //uweabril           
            if (((CheckBox)sender).Checked)
            {
                if (tabChild.TabPages.Contains(tabOutros) == false)
                {
                    tabChild.TabPages.Insert(2, tabOutros);
                    tabChild.SelectedTab = tabOutros;
                }
            }
            else { 
                  if (tabChild.TabPages.Contains(tabOutros) == true)
                      tabChild.TabPages.Remove(tabOutros);            
            }

        }

        private void comboboxTipoNegocio_SelectionChangeCommitted(object sender, EventArgs e)
        {
            var item = ((ComboBox)sender).SelectedItem;
            string nome_rede = ((DataRowView)item).Row["nome_rede"].ToString();
            //AbaOutros(nome_rede);

            if (estadoCorrente == EstadoManutencao.smEditar)
            {
                DataRowView current = (DataRowView)osClienteBindingSource.Current;
                var id_OsCliente = current["id"];
                var id_OsClienteRede = comboboxTipoNegocio.SelectedValue;

                // só pode existir um ...link...
                var del = @"DELETE FROM OsClienteRedeLink WHERE id_OsCliente = " + id_OsCliente;
                var ins = @"INSERT INTO OsClienteRedeLink(id_OsCliente, id_OsClienteRede)
                        VALUES(" + id_OsCliente + "," + id_OsClienteRede + ")";
                DB.ExecuteNonQuery(del);
                DB.ExecuteNonQuery(ins);
            }
           
          
        }
        private void AbaOutros(string nome_rede) 
        {
            //uweabril
            switch (nome_rede)
            {
                case "Posto":
                    // code block
                    if (tabChild.TabPages.Contains(tabOutros) == false)
                    {
                        tabChild.TabPages.Insert(2, tabOutros);
                        tabChild.SelectedTab = tabOutros;
                    }
                    break;

                default:
                    // code block
                    if (tabChild.TabPages.Contains(tabOutros) == true)
                        tabChild.TabPages.Remove(tabOutros);
                    break;
            }
        }

        private void tabEventos_Enter(object sender, EventArgs e)
        {
            if (estadoCorrente == EstadoManutencao.smInserir)
            {
                MessageBox.Show(@"Favor cadastrar primeiro o cliente",
                                @"I N F O",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                tabChild.SelectedTab = tabEndereco;
                return;
            }

            createEvento();

            limparEventosCampos();
            listviewEventos.Items.Clear();


            DataRowView current = (DataRowView)osClienteBindingSource.Current;

            var id_cliente = current["id"];
            var nome_cliente = current["razao_social"];

            int cnt = countEventos((int)id_cliente);

            //foreach (Control c in tabPrincipal.TabPages["Contatos"].Controls)
            foreach (Control c in tabChild.TabPages["tabEventos"].Controls)
            {
                if (c.GetType() == typeof(Label))
                {
                    //if (c.Name == "labelContatosCliente")
                    //    c.Text = nome_cliente.ToString();
                    if (c.Name == "labelEventosNumero")
                        c.Text = "Número dos contatos: " + cnt.ToString();
                }
            }

            if (cnt > 0)
            {
                fillListEventos((int)id_cliente);
            }
        }

        private void tabEventos_Leave(object sender, EventArgs e)
        {
            if (estadoCorrente != EstadoManutencao.smInserir)
              editar();
            buttonEventosDeletar.Visible = false;
        }

        private void buttonEventosCancelar_Click(object sender, EventArgs e)
        {
            limparEventosCampos();           
            createEvento();
            buttonEventosDeletar.Visible = false;
        }

        private void buttonEventosGravar_Click(object sender, EventArgs e)
        {
            DataRowView current = (DataRowView)osClienteBindingSource.Current;
            var id_cliente = current["id"];
            var nome_cliente = current["razao_social"];

            string apontamento = textboxApontamento.Text;
            string obs = "";
            var A = "N";
            if (checkboxAlertar.Checked) A = "S";
            
            DateTime registro = DateTime.Now;
            DateTime realizacao = DateTime.Now;

            string dt_value = datetimeRealizacao.Value.ToString(); 

            string sql_insert = @"INSERT INTO OsClienteEvento(id_cliente, datahora_registro, datahora_realizacao, alertar, apontamento, obs) VALUES("
                              + id_cliente
                              + ",'" + registro + "'"
                              + ",'" + dt_value + "'"
                               + ",'" + A + "'"
                               + ",'" + apontamento + "'"                                                        
                              + ",'" + obs + "')";
            string hiddenId = textboxHiddenIdEvento.Text;
            string sql_update = @"UPDATE OsClienteEvento set "                              
                              + "datahora_realizacao = '" + dt_value + "',"
                              + "alertar = '" + A + "',"
                              + "apontamento = '" + apontamento + "',"                            
                              + "obs = '" + obs + "' "
                              + "WHERE id=" + hiddenId;

            if (estadoCorrente == EstadoManutencao.ceCreate)
                DB.ExecuteNonQuery(sql_insert);
            if (estadoCorrente == EstadoManutencao.ceUpdate)
                DB.ExecuteNonQuery(sql_update);
            limparEventosCampos();
            fillListEventos((int)id_cliente);
            int cnt = countEventos((int)id_cliente);
            setLabelTextEventos("labelEventosNumero", "Número dos eventos : " + cnt.ToString());
            createEvento();
            buttonEventosDeletar.Visible = false;
        }

        private void buttonEventosDeletar_Click(object sender, EventArgs e)
        {
            if (!(estadoCorrente == EstadoManutencao.ceUpdate))
                return;

            DataRowView current = (DataRowView)osClienteBindingSource.Current;
            var id_cliente = current["id"];

            string hiddenId = textboxHiddenIdEvento.Text;
            string sql_delete = @"DELETE FROM OsClienteEvento WHERE id = " + hiddenId;

            var confirmResult = MessageBox.Show("Tem certeza que queira apagar este evento ??",
                                     "Confirmar",
                                     MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                // If 'Yes', do something here.
                DB.ExecuteNonQuery(sql_delete);
                limparEventosCampos();
                fillListEventos((int)id_cliente);
                int cnt = countEventos((int)id_cliente);
                setLabelTextEventos("labelEventosNumero", "Número dos eventos: " + cnt.ToString());
                createEvento();
                buttonEventosDeletar.Visible = false;
            }
            else
            {
                // nada pra fazer
            }
            
        }

        private void listviewEventos_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listviewEventos.SelectedItems.Count > 0)
            {
                ListViewItem item = listviewEventos.SelectedItems[0];
                //textboxContatosNome.Text = item.SubItems[0].Text; 
                DateTime dx = Convert.ToDateTime(item.SubItems[1].Text);
                datetimeRealizacao.Value = dx;

                // checkbox alertar
                if (item.SubItems[2].Text.ToString().Equals("S"))
                    checkboxAlertar.Checked = true;
                else
                    checkboxAlertar.Checked = false;

                textboxApontamento.Text = item.SubItems[3].Text;
              
                //textboxContatosObs.Text = item.SubItems[4].Text;

                textboxHiddenIdEvento.Text = item.SubItems[4].Text;
                //textboxHiddenId.Text = item.SubItems[7].Text;
                //tssStatus.Text = "Status: Alterar Contato ...";
                //estadoCorrente = EstadoManutencao.ccUpdate;
                updateEvento();
                buttonEventosDeletar.Visible = true;
            }
            else
            {
                //textboxContatosObs.Text = "listView1.SelectedItems.Count is 0!";
            }
        }

        private void datetimeRealizacao_ValueChanged(object sender, EventArgs e)
        {
            DateTime hoje = DateTime.Now;
            DateTime value = ((DateTimePicker)sender).Value;

            // Tooltip is possible because the checkbox is Enabled
             ToolTip toolTip = new ToolTip();                    

            int result = DateTime.Compare(value, hoje);
            if (result < 0)
            {
                Debug.WriteLine("Passado");
                checkboxAlertar.ForeColor = Color.Gray; 
                checkboxAlertar.AutoCheck = false;
                checkboxAlertar.Checked = false;
                toolTip.SetToolTip(checkboxAlertar, "Data da realização no passado!");                              
            }
            else if (result == 0)
                Debug.WriteLine("Presente");
            else
            {
                Debug.WriteLine("Futuro");                              
                checkboxAlertar.ForeColor = SystemColors.ControlText; 
                checkboxAlertar.AutoCheck = true;
                toolTip.SetToolTip(checkboxAlertar, "Data  da  realização  no  futuro!");              
            }
        }

        private int countCliente(string id_cliente, string columnName, string tableName) 
        {
          string sql = @"SELECT count(*) cnt FROM " + tableName + " WHERE " + columnName +" = " + id_cliente;
          int cnt = (int)DB.GetSingleValue(sql);
          return cnt;
        }
        /*
         * SELECT c.name AS ColName, t.name AS TableName
         *  FROM sys.columns c
         *  JOIN sys.tables t ON c.object_id = t.object_id
         *  WHERE c.name LIKE '%CLIENTE%';
         *  
         * ColName	TableName
            IDCLIENTE	OsOrdemServico
            IDCLIENTE_PEDIDO	OsOrdemServico
            ORIGEM_CLIENTE_PEDIDO	OsOrdemServico
           
            IDCLIENTE	OsFlashDrive
           
            Cliente	A30CAIXA_PROG_EX
            Cliente	A30CAIXA_PROG
            Cliente	A30CLIENTES
          
            IDCLIENTE	OsIbametro
         * 
            id_cliente	OsClienteEvento
            id_cliente	OsClienteContato
         */
        private List<string> analisarCliente(string id_cliente) 
        {
            List<string> liste  = new List<string>();
            List<string> column = new List<string>();
            List<string> table  = new List<string>();
            column.Add("IDCLIENTE");table.Add("OsOrdemServico");
            column.Add("IDCLIENTE_PEDIDO");table.Add("OsOrdemServico");
            // column.Add("ORIGEM_CLIENTE_PEDIDO");table.Add("OsOrdemServico");

            column.Add("IDCLIENTE");table.Add("OsFlashDrive");

            column.Add("IDCLIENTE");table.Add("OsIbametro");
            column.Add("id_cliente");table.Add("OsClienteContato");
            column.Add("id_cliente");table.Add("OsClienteEvento");
           
            for(int i=0; i<table.Count ; i++)
            {
                if (countCliente(id_cliente, column[i], table[i]) > 0) liste.Add("Cliente referenced in " + table[i] + "." + column[i]  ); 
            }  

            return liste;
        }

        private void retornarPesquisar() {
            pesquisar();
            tsbDescartar.Enabled = false;
            labelCadastrarCliente.Visible = false;
            tabPrincipal.SelectedTab = tabPage1;
            tabChild.SelectedTab = tabEndereco;
            toolStripButton3.PerformClick();
        }

        private void descartarCliente(string current_id) 
        {
            string sql_delete = @"DELETE FROM OsCliente WHERE id = " + current_id;
            try
            {
                DB.ExecuteNonQuery(sql_delete);
                // tudo ok box deletar
                //MessageBox.Show(@"Registro descartado com sucesso!");
                //retornarPesquisar();                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,
                                "E R R O",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }
        private void descartarClienteContatosEventos(string current_id)
        {
            string sql_deleteContatos = @"DELETE FROM OsClienteContato WHERE id_cliente = " + current_id;
            string sql_deleteEventos  = @"DELETE FROM OsClienteEvento WHERE id_cliente = " + current_id;
            try
            {
                DB.ExecuteNonQuery(sql_deleteContatos);
                DB.ExecuteNonQuery(sql_deleteEventos);
                // tudo ok box deletar
                //MessageBox.Show(@"Registro descartado com sucesso!");
                //retornarPesquisar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,
                                "E R R O",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        private string acrescentarDependencias(List<string> dependencias)
        {
            string ret = Environment.NewLine + "Dependências";
            
            foreach (string dep in dependencias)
            {
                if (dep.Contains("OsClienteContato"))
                    ret += Environment.NewLine + "OsClienteContato";
                else
                    if (dep.Contains("OsClienteEvento"))
                        ret += Environment.NewLine + "OsClienteEvento";
                    else ret += Environment.NewLine + dep;
            }
            return ret;
        }

        private void tsbDescartar_Click(object sender, EventArgs e)
        {
            DataRowView current = (DataRowView)osClienteBindingSource.Current;
            string current_id = current["id"].ToString();
            List<string> liste = analisarCliente(current_id);

            // cliente tem dependências nas tabelas da movimentação?
            int cnt_movimentacao = 0;
            int cnt_contatos = 0;
            int cnt_eventos = 0; 

            string pergunta = "";
            string perguntaMe = "";
            string verbo = "descartar";

            foreach (string dep in liste) 
            {
                if (dep.Contains("OsClienteContato"))
                    cnt_contatos += 1;
                else
                    if (dep.Contains("OsClienteEvento")) cnt_eventos += 1;
                    else cnt_movimentacao += 1;                
            }
            
            #region Cliente não tem nenhuma dependência
            if (liste.Count == 0)
            {
                pergunta = "Deseja realmente descartar esse cliente ?"; 
                if (MessageBox.Show(pergunta,
                                   "alerta da confirmação",
                                   MessageBoxButtons.YesNo,
                                   MessageBoxIcon.Warning) == DialogResult.Yes)
                { 
                  //Descartar o cliente
                  current["ativo"] = "N";
                  osClienteBindingSource.EndEdit();
                  osClienteTableAdapter.Update(this.dtsPrincipal);

                  descartarCliente(current_id);
                  MessageBox.Show(@"Registro descartado com sucesso!");
                  retornarPesquisar(); 
                }
            }
            #endregion

            if (liste.Count > 0)
            {
                // cliente com dependências nas tabelas de movimentação
                // só pode ser desativado e não descartado
                if (cnt_movimentacao > 0)
                {
                    pergunta = "Deseja realmente desativar esse cliente ?";
                    pergunta += acrescentarDependencias(liste);
                    if (MessageBox.Show(pergunta,
                                       "alerta da confirmação",
                                       MessageBoxButtons.YesNo,
                                       MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        //Desativar o cliente
                        current["ativo"] = "N";
                        osClienteBindingSource.EndEdit();
                        osClienteTableAdapter.Update(this.dtsPrincipal);
                        MessageBox.Show(@"Registro desativado com sucesso!");
                        retornarPesquisar();
                    }
                }
                // cliente só tem dependências nas tabelas OsClienteContato/OsClienteEvento
                // pode descartar ou desativar (inclusive os registros nessas tabelas)
                if (cnt_movimentacao == 0) { 
                    // DESCARTAR ??
                    pergunta = "Deseja realmente descartar esse cliente ?";
                    pergunta += acrescentarDependencias(liste);
                    if (MessageBox.Show(pergunta,
                                       "alerta da confirmação",
                                       MessageBoxButtons.YesNo,
                                       MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        //Descartar o cliente
                        current["ativo"] = "N";
                        osClienteBindingSource.EndEdit();
                        osClienteTableAdapter.Update(this.dtsPrincipal);

                        descartarCliente(current_id);
                        // todo: descartar Contatos/Eventos
                        descartarClienteContatosEventos(current_id);
                        MessageBox.Show(@"Registros descartados com sucesso!");
                        retornarPesquisar();
                    }
                    // não quero descartar, pode ser desativar ?
                    else {
                        pergunta = "Deseja realmente desativar esse cliente ?";
                        pergunta += acrescentarDependencias(liste);

                        if (MessageBox.Show(pergunta,
                                           "alerta da confirmação",
                                           MessageBoxButtons.YesNo,
                                           MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            //Desativar o cliente
                            current["ativo"] = "N";
                            osClienteBindingSource.EndEdit();
                            osClienteTableAdapter.Update(this.dtsPrincipal);                           
                            MessageBox.Show(@"Registro desativado com sucesso!");
                            retornarPesquisar();
                        }
                    }

                
                }

            }

            //if (liste.Count > 0)
            //{
            //    verbo = "desativar";
            //    foreach (var x in liste) 
            //    {
            //        perguntaMe += Environment.NewLine + x;
            //    }
            //}
            //string pergunta = "Deseja realmente " + verbo + " esse Cliente?" + perguntaMe;
            //if (MessageBox.Show(pergunta,
            //                   "alerta da confirmação",
            //                   MessageBoxButtons.YesNo,
            //                   MessageBoxIcon.Warning) == DialogResult.Yes)
            //{
            //    if (verbo.Equals("desativar"))
            //    {
            //        current["ativo"] = "N";
            //        osClienteBindingSource.EndEdit();
            //        osClienteTableAdapter.Update(this.dtsPrincipal);
            //        // alles ok box desativar
            //        MessageBox.Show(@"Registro deativado com sucesso!");
            //    }

            //    if (verbo.Equals("descartar"))
            //    {
            //        current["ativo"] = "N";
            //        osClienteBindingSource.EndEdit();
            //        osClienteTableAdapter.Update(this.dtsPrincipal);

            //        string sql_delete = @"DELETE FROM OsCliente WHERE id = " + current["id"].ToString();
            //        try
            //        {
            //            DB.ExecuteNonQuery(sql_delete);
            //            // alles ok box deletar
            //            MessageBox.Show(@"Registro descartado com sucesso!");
            //            pesquisar();
            //            tsbDescartar.Enabled = false;
            //            labelCadastrarCliente.Visible = false;
            //            tabPrincipal.SelectedTab = tabPage1;
            //            tabChild.SelectedTab = tabEndereco;
            //            toolStripButton3.PerformClick();
            //        }
            //        catch (Exception ex) {
            //            MessageBox.Show(ex.Message,
            //                   "E R R O",
            //                   MessageBoxButtons.OK,
            //                   MessageBoxIcon.Error);
            //        }
            //    }

            //}
        }

        private void qTDE_BOMBASTextBox_Leave(object sender, EventArgs e)
        {
            if (qTDE_BOMBASTextBox.Text.Equals("")) qTDE_BOMBASTextBox.Text = "0";
        }

        private void qTDE_BICOSTextBox_Leave(object sender, EventArgs e)
        {
            if (qTDE_BICOSTextBox.Text.Equals("")) qTDE_BICOSTextBox.Text = "0";
        }
        //////////////////////////////////////////////////////////////////////////////////
        /// Manutenção Eventos
        //////////////////////////////////////////////////////////////////////////////////
      
        private DataTable GetEventos(string SNTODOS, string filtroCliente = "")
        {
            // SNTODOS : SimNaoTodos
            DB.ConnectionName = "osExpress.Properties.Settings.OsExpressConnectionString";
            string sql = @"select oce.id, id_cliente, razao_social,  
                                     FORMAT(datahora_realizacao,'dd/MM/yyyy HH:mm') datahora,
                                     alertar, apontamento,
                                     CASE WHEN len(apontamento) < 100 
                                     THEN apontamento
                                     ELSE CONCAT(substring(apontamento,1,100),'...')
                                     END
                                     title
                                     from OsClienteEvento oce, OsCliente  oc  
                                     WHERE 1=1
                                     AND oce.id_cliente = oc.id";
            if (SNTODOS.Equals("S") || SNTODOS.Equals("N"))
            {
                sql += @" AND oce.alertar = '" + SNTODOS + "' ";
            }
            if (filtroCliente.Length > 0)
            {
                sql += @" AND Upper(RAZAO_SOCIAL) like '%" + filtroCliente + "%'";
            }
            sql += @" ORDER BY datahora_realizacao";
            DataTable data = DB.GetTableFromSQL(sql);
            return data;
        }
        private void btnEventosTodos_Click(object sender, EventArgs e)
        {
            createListManEventos("Todos");
        }

        private void btnEventosConcluidos_Click(object sender, EventArgs e)
        {
            createListManEventos("Concluidos");
        }

        private void btnEventosPendentes_Click(object sender, EventArgs e)
        {
            createListManEventos("Pendentes");
        }

        private void createListManEventos(string what) {

            flpManEventos.Controls.Clear();
            foreach (Control item in flpManEventos.Controls.OfType<Control>())
            {
                if (item.Name == "BTN_FECHAR")
                    continue;
                if (item.Name == "TB_FECHAR")
                    continue;
                var x = item.GetType();
                flpManEventos.Controls.Remove(item);
            }
            
            string sntodos = ""; 
            if (what.Equals("Concluidos")) sntodos = "N"; 
            if (what.Equals("Pendentes")) sntodos = "S";
            string filtro = textboxFiltroCliente.Text;

            DataTable data = GetEventos(sntodos, filtro);

            flpManEventos.Location = new Point(50,120);
            flpManEventos.Size = new Size(1000, 300);
            flpManEventos.BorderStyle = BorderStyle.FixedSingle;
            flpManEventos.Visible = true;
            
            if (flpManEventos.Controls.Find("TB_FECHAR", true).Length == 0)
            {
                TextBox title = new TextBox();
                title.Name = "TB_FECHAR";
                title.Text = what;
                title.Height = 30;
                title.Enabled = false;
                flpManEventos.Controls.Add(title);
            }
            else {
                TextBox TB = flpManEventos.Controls.Find("TB_FECHAR", true)[0] as TextBox;
                TB.Text = what;
            }
            // Button
            if (flpManEventos.Controls.Find("BTN_FECHAR", true).Length == 0)
            {
                Button btn1 = new Button();
                btn1.Name = "BTN_FECHAR";
                String btntxt = "Fechar";
                btn1.Text = btntxt;
                btn1.Height = 25;

                btn1.AutoSize = true;
                   btn1.AutoSizeMode = AutoSizeMode.GrowAndShrink;

                btn1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
                //btn1.Width = 300;
                //btn1.Tag = new { id_cliente = Convert.ToInt32(row["id_cliente"]), id = Convert.ToInt32(row["id"]) };
                btn1.Click += new EventHandler(btn_fechar);
                flpManEventos.Controls.Add(btn1);
                flpManEventos.SetFlowBreak(btn1, true);                
            }

            // loop data
            foreach (DataRow row in data.Rows)
            {
                // Checkbox alertar
                /*
                CheckBox dynamicCheckBox = new CheckBox();
                dynamicCheckBox.Width = 45;
                dynamicCheckBox.Height = 30;                                 
                dynamicCheckBox.Tag = row["id"].ToString();
                flpEventos.Controls.Add(dynamicCheckBox);
                dynamicCheckBox.Margin = new Padding(5, 0, 0, 8);
                dynamicCheckBox.Checked = true;
                dynamicCheckBox.CheckedChanged += new System.EventHandler(CheckBoxCheckedChanged);
                */
                // DatetimePicker
                /*
                DateTimePicker dynamicDTP = new DateTimePicker();                   
                flpEventos.Controls.Add(dynamicDTP);
                dynamicDTP.Size = new System.Drawing.Size(150, 25);
                dynamicDTP.Tag = row["id"].ToString();
                dynamicDTP.Format = DateTimePickerFormat.Custom;
                dynamicDTP.CustomFormat = "dd/MM/yyyy HH:mm";
                DateTime dx = Convert.ToDateTime(row["datahora"].ToString());
                dynamicDTP.Value = dx;
                dynamicDTP.ValueChanged += new System.EventHandler(dynamicDTP_ValueChanged);
                */

                // Button
                Button btnCliente = new Button();
                String title = row["razao_social"].ToString();
                // programmatically there can be multiple controls of same name
                btnCliente.Name = "BUTTONCLIENTE";
                btnCliente.Text = title;
                btnCliente.Height = 30;
                //btn1.AutoSize = true;
                //btn1.AutoSizeMode = AutoSizeMode.GrowAndShrink;
                btnCliente.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
                btnCliente.Width = 300;

                btnCliente.Tag = new { id_cliente = Convert.ToInt32(row["id_cliente"]), id = Convert.ToInt32(row["id"]) };
                btnCliente.Click += new EventHandler(btn_click);

                flpManEventos.Controls.Add(btnCliente);
                TextBox tb = new TextBox();
                // title = substr(apontamento,20)
                tb.Text = row["title"].ToString();
                tb.Width = 450;
                tb.Height = 30;
                tb.Font = new System.Drawing.Font(tb.Font.FontFamily, 12);
                tb.Enabled = false;
                flpManEventos.Controls.Add(tb);
                flpManEventos.SetFlowBreak(tb, true);
            }
        }

        private void btn_fechar(object sender, EventArgs e)
        {
            flpManEventos.Visible = false;
            flpManEventos.Controls.Clear();
            
            foreach (Control item in flpManEventos.Controls.OfType<Control>())
            {
                if (item.Name == "BTN_FECHAR")
                    continue;
                if (item.Name == "TB_FECHAR")
                    continue;
                flpManEventos.Controls.Remove(item);
            }

        }

        private void btn_click(object sender, EventArgs e)
        {
           
            flpManEventos.Visible = false;
            //textBox2.Visible = false;           

            // abrir FORM
            //OpenForm(typeof(Manutencoes.ManCliente));
            //// obter instância de FORM "ManCliente"
            Form form = null;
            FormCollection fc = Application.OpenForms;
            foreach (Form frm in fc)
            {
                if (frm.Name.Equals("ManCliente"))
                {
                   form = frm;                   
               }
            }
            // obter referencia á DataGridView
            DataGridView grid = form.Controls.Find("osOrdemServicoDataGridView", true).FirstOrDefault() as DataGridView;
           
            int rowIndex = -1;
          
            var button = ((Button)sender);
            int id_cliente=((dynamic)button.Tag).id_cliente;
            int id = ((dynamic)button.Tag).id;

            foreach (DataGridViewRow row in grid.Rows)
            {               
                if ( (int)row.Cells[0].Value == id_cliente)
                {
                    rowIndex = row.Index;
                    break;
                }
            }
            grid.Rows[rowIndex].Selected = true;
            
            // acionar DoubleClick on DataGridView via Reflection
            MethodInfo onCellDoubleClick = typeof(DataGridView).GetMethod("OnCellDoubleClick",
                                                                BindingFlags.NonPublic | BindingFlags.Instance);          
            DataGridViewCellEventArgs ea = new DataGridViewCellEventArgs(0, rowIndex);           
            onCellDoubleClick.Invoke(grid, new object[] { ea });

            // select Tab Eventos
            TabControl tabc = form.Controls.Find("TabChild", true).FirstOrDefault() as TabControl;
            tabc.SelectedTab = tabc.TabPages["tabEventos"];

            ListView liste = form.Controls.Find("listviewEventos", true).FirstOrDefault() as ListView;

            //MethodInfo onSelectedIndexChanged = typeof(ListView).GetMethod("OnSelectedIndexChanged",
            //   BindingFlags.NonPublic | BindingFlags.Instance);
            // EventArgs lea = EventArgs(0, rowIndex);
            //onSelectedIndexChanged.Invoke(liste, new object[] { id, id_cliente });
            int c = listviewEventos.Items.Count;
            for (int i = 0; i < c; i++)
            {
                //Debug.WriteLine(i);
                if ( (Convert.ToInt32(listviewEventos.Items[i].SubItems[4].Text) == id))
                {
                    listviewEventos.Items[i].Selected = true;
                }
            }

            
        }

        private void btnListEventos_Click(object sender, EventArgs e)
        {
            Debug.WriteLine("btnListEventos_Click");
            flpManEventos.Visible = true;
            toolStripButton3.PerformClick();
        }
       
        private void chkFiltros_CheckedChanged(object sender, EventArgs e)
        {
            string sAnd = " AND ";
            string sFilter = "";

            osClienteBindingSource.Filter = "";

            if (chkFiltrarSituacao.Checked)
            {               
                sFilter = sFilter + " ativo = " + "'" + "S" + "'" + sAnd;
            }
            else {
                sFilter = sFilter + " ativo = " + "'" + "N" + "'" + sAnd;
            }

            if (chkFiltrarCliente.Checked)
            {
                sFilter = sFilter + "id = " + Convert.ToInt32(cmbFiltroClienteCodigo.Text) + "" + sAnd;
            }
            if (chkFiltrarClienteFantasia.Checked)
            {
                sFilter = sFilter + "id = " + Convert.ToInt32(cmbFiltroClienteCodigo.Text) + "" + sAnd;
            }
            if (sFilter.Length > 0)
            {
                sFilter = sFilter.Substring(0, sFilter.Length - 5);
                osClienteBindingSource.Filter = sFilter;             
               
                this.osClienteTableAdapter.Fill(this.dtsPrincipal.OsCliente);
            }
          }

        private void labelRazao_Click(object sender, EventArgs e)
        {
            bs.Sort = "nome";
        }

        private void labelFantasia_Click(object sender, EventArgs e)
        {
            bs.Sort = "nome_fantasia";
        }

        private void execDoubleClick(Form form, string RemoteFunction, int nPedido)
        {
            DataGridView grid = form.Controls.Find("osOrdemServicoDataGridView", true).FirstOrDefault() as DataGridView;

            int rowIndex = -1;
            DataGridViewRow row = grid.Rows
                .Cast<DataGridViewRow>()
                .Where(r => r.Cells[0].Value.ToString().Equals(nPedido.ToString()))
                .First();

            rowIndex = row.Index;
            
            grid.Rows[rowIndex].Selected = true;
            grid.FirstDisplayedScrollingRowIndex = grid.SelectedRows[0].Index; // move selected to first line
            grid.CurrentCell = grid.Rows[rowIndex].Cells[1];

            DataGridViewCellEventArgs ea = new DataGridViewCellEventArgs(0, rowIndex);
            //MethodInfo onCellDoubleClick = typeof(DataGridView).GetMethod("OnCellDoubleClick",
            //                                                    BindingFlags.NonPublic | BindingFlags.Instance);
            //onCellDoubleClick.Invoke(grid, new object[] { ea });

            var methodInfo = form.GetType().GetMethod(RemoteFunction);
            methodInfo.Invoke(form, new object[] { ea });

            //Panel panel = form.Controls.Find("pnlConsultar", true).FirstOrDefault() as Panel; 
            //var mi = form.GetType().GetMethod("MakePanelInvisible");
            //mi.Invoke(form, null);

            // select Tab Eventos
            //TabControl tabc = form.Controls.Find("tabPrincipal", true).FirstOrDefault() as TabControl;
            //tabc.SelectedTab = tabc.TabPages["tabCabecalho"];
        }
        // Aba orcamento
        private void dataGridViewOrcamentos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            int nPedido = 0;
            var dgv = ((DataGridView)sender);
            //try
            //{
               // nPedido = Convert.ToInt32(dgv[e.ColumnIndex, e.RowIndex].Value);
                  nPedido = Convert.ToInt32(dgv[0, e.RowIndex].Value);
            //}
            //catch (Exception ex) {
            //    nPedido = Convert.ToInt32(dgv[0, e.RowIndex].Value);
            //}
            Form form = Helper.MyReflection.GetForm("ManPedido");

            //if (dgv.Name.Contains("Orcamentos") || dgv.Name.Contains("Pedidos") || dgv.Name.Contains("Servicos"))
            if (dgv.Name.Contains("Orcamentos") || dgv.Name.Contains("Pedidos"))
            {
                //Form form = Helper.MyReflection.GetForm("ManPedido");
                // ManPedido NOT open
                if (form == null)
                {
                    // open Form
                    Form principal = Helper.MyReflection.GetForm("SysPrincipal");
                    Button orca = principal.Controls.Find("btnOrcamentos", true).FirstOrDefault() as Button;
                    orca.PerformClick();
                    //
                    form = Helper.MyReflection.GetForm("ManPedido");
                    form.WindowState = FormWindowState.Normal;
                    //
                    execDoubleClick(form, "CallDataGridViewCellDoubleClickFromReflection", nPedido);
                }
                else  // ManPedido is open
                {
                    form.WindowState = FormWindowState.Normal;
                    // open pnlConsultar - DataGridView if not open
                    //
                    var methodInfo = form.GetType().GetMethod("ClickConsultar");
                    methodInfo.Invoke(form, null);

                    execDoubleClick(form, "CallDataGridViewCellDoubleClickFromReflection", nPedido);
                }
                form.BringToFront();
            }
            if (dgv.Name.Contains("Pedidos"))
            {
            }
            if (dgv.Name.Contains("Servicos"))
            {
               //TabControl tabc = form.Controls.Find("tabPrincipal", true).FirstOrDefault() as TabControl;
               //tabc.SelectedTab = tabc.TabPages["tabPecas"];
            }
            Cursor = Cursors.Arrow;
                    
            //this.MdiParent.LayoutMdi(MdiLayout.TileHorizontal);
            //this.MdiParent.LayoutMdi(MdiLayout.TileVertical);
            this.MdiParent.LayoutMdi(MdiLayout.Cascade);
            // 1 - ManPedidos
            this.MdiParent.MdiChildren[1].Location = new Point(100,100);

        }        

        private void dataGridViewOrcamentos_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            var dataGridView = sender as DataGridView;
            if (dataGridView != null)
            {
                if (dataGridView.ColumnCount > 0)
                {
                    dataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                    dataGridView.Columns[dataGridView.ColumnCount - 1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }
            }

        }

        private void dataGridViewOrcamentos_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            var dataGridView = sender as DataGridView;
            if (e.ColumnIndex == 3)
            {
                e.FormattingApplied = true; // <===VERY, VERY important tell it you've taken care of it.
                string temp = dataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
                switch (temp)
                {
                    case "A":
                        e.Value = "Aberto";
                        break;
                    case "F":
                        e.Value = "Fechado";
                        break;
                    case "D":
                        e.Value = "Descartado";
                        break;
                    default:
                        e.Value = "Desconhecido";
                        break;
                }
            }
        }

        private void bindOrcamentos()
        {
            DataRowView current = (DataRowView)osClienteBindingSource.Current;
            //
            string sql = @"select o.id #Pedido, o.dt_cadastro Dt_Cadastro, total_os Valor,        
	                           status_os Situação, u.nome Nome_Usuário	   	                            
	                           from OsOrdemServico o, OsUsuario u
	                           where o.idusuario = u.id
	                           and tipo_doc = 'O'	
                               and o.idposto = " + OsConfiguracoes.idPosto;
            sql += @" and idcliente_pedido = " + current["id"];
            sql += " order by o.dt_cadastro desc";


            bs_orcamento.DataSource = DB.GetTableFromSQL(sql);
            if (dataGridViewOrcamentos.ColumnCount >= 3)
            {
                dataGridViewOrcamentos.Columns[2].DefaultCellStyle.Format = "C2";
                dataGridViewOrcamentos.Columns[2].DefaultCellStyle.FormatProvider = CultureInfo.GetCultureInfo("pt-BR");
            }

            // center. 
            dataGridViewOrcamentos.Location =
                new Point((tabOrcamentos.Width - dataGridViewOrcamentos.Width) / 2, (tabOrcamentos.Height - dataGridViewOrcamentos.Height) / 2);
        }
        private void tabOrcamentos_Enter(object sender, EventArgs e)
        {
            bindOrcamentos();
        }

        private void bindPedidos()
        {
            DataRowView current = (DataRowView)osClienteBindingSource.Current;

            string sql = @"select o.id #Pedido, o.dt_cadastro Dt_Cadastro, total_os Valor,        
	                           status_os Situação, u.nome Nome_Usuário	   	                            
	                           from OsOrdemServico o, OsUsuario u
	                           where o.idusuario = u.id
	                           and tipo_doc = 'P' and status_os = 'F'
                               and o.idposto = " + OsConfiguracoes.idPosto;
            sql += @" and idcliente_pedido = " + current["id"];
            sql += " order by o.dt_cadastro desc";


            bs_pedido.DataSource = DB.GetTableFromSQL(sql);
            if (dataGridViewPedidos.ColumnCount >= 3)
            {
                dataGridViewPedidos.Columns[2].DefaultCellStyle.Format = "C2";
                dataGridViewPedidos.Columns[2].DefaultCellStyle.FormatProvider = CultureInfo.GetCultureInfo("pt-BR");
            }
            dataGridViewPedidos.Columns["Situação"].Visible = false;
            // center
            dataGridViewPedidos.Location =
                new Point((tabPedidos.Width - dataGridViewPedidos.Width) / 2, (tabPedidos.Height - dataGridViewPedidos.Height) / 2);
        }
        private void tabPedidos_Enter(object sender, EventArgs e)
        {
            bindPedidos();
        }

        private void bindServicos()
        {
            DataRowView current = (DataRowView)osClienteBindingSource.Current;

            string sql = @"select o.id #Pedido, o.dt_cadastro Dt_Cadastro, total_os Valor,        
	                           status_os Situação, u.nome Nome_Usuário	   	                            
	                           from OsOrdemServico o, OsUsuario u
	                           where o.idusuario = u.id
	                           and tipo_doc = 'S'	
                               and o.idposto = " + OsConfiguracoes.idPosto;
            //sql += @" and idcliente_pedido = " + current["id"];
            sql += @" and idcliente = " + current["id"];
            sql += " order by o.dt_cadastro desc";


            bs_servico.DataSource = DB.GetTableFromSQL(sql);
            if (dataGridViewServicos.ColumnCount >= 3)
            {
                dataGridViewServicos.Columns[2].DefaultCellStyle.Format = "C2";
                dataGridViewServicos.Columns[2].DefaultCellStyle.FormatProvider = CultureInfo.GetCultureInfo("pt-BR");
            }

            // center
            dataGridViewServicos.Location =
                new Point((tabServicos.Width - dataGridViewServicos.Width) / 2, (tabServicos.Height - dataGridViewServicos.Height) / 2);
        }
        private void tabServicos_Enter(object sender, EventArgs e)
        {
            bindServicos();
        }

        private void FiltrarDataGridViewViaLike(string txt)
        {
            osClienteBindingSource.Filter = " razao_social LIKE '%"+txt+"%'";
         
            this.osClienteTableAdapter.Fill(this.dtsPrincipal.OsCliente);        
        }

        private void tbLike_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                FiltrarDataGridViewViaLike( ((TextBox)sender).Text );
            }
        }

        //////////////////////////////////////////////////////////////////////////////////
        /// Manutenção Bombas
        //////////////////////////////////////////////////////////////////////////////////
        private void refreshBomba()
        {            
            DataRowView current = (DataRowView)osClienteBindingSource.Current;
            string sql = @"select m.id, m.num_serie, m.marca_instrumento, 
                                  COALESCE(c.cnt,0) cnt, m.ilha, m.descricao, m.fabricante 
                           from OsIbametroOsCliente m
                           LEFT OUTER JOIN 
                            ( select idibametro,count(1) cnt from OsIbametroInMetroOsCliente group by idibametro) c
                           ON (m.id = c.idibametro )  
		                   where idcliente = " + current["id"];

            bs_instrumentos.DataSource = DB.GetTableFromSQL(sql);
        }

        private void refreshLacre()
        {
//            DataRowView current = (DataRowView)osClienteBindingSource.Current;
//            string sql = @"select m.id, m.num_serie, m.marca_instrumento, 
//                                  COALESCE(c.cnt,0) cnt, m.ilha, m.descricao, m.fabricante 
//                           from OsIbametroOsCliente m
//                           LEFT OUTER JOIN 
//                            ( select idibametro,count(1) cnt from OsIbametroInMetroOsCliente group by idibametro) c
//                           ON (m.id = c.idibametro )  
//		                   where idcliente = " + current["id"];

//            bs_instrumentos.DataSource = DB.GetTableFromSQL(sql);

            //dgvLacre.DataSource = null;

            DataRowView current = (DataRowView)bs_instrumentos.Current;

            string sql = @"select id, num_inmetro Lacre from OsIbametroInmetroOsCliente where 1=1 ";
            sql += @" and idibametro = " + current["id"].ToString();

            bs_inmetro.DataSource = DB.GetTableFromSQL(sql);

            dgvLacre.DataSource = bs_inmetro;

            //id
            dgvLacre.Columns[0].ReadOnly = true;
        }
        private void bindBomba()
        {
            //pnlEditarBomba.Visible = false;

            //cmbMarcaInstrumento.SelectedIndex = -1;

            //dataGridView1.Rows.Clear();
            //dataGridView1.Refresh();
            //datagridview.DataSource = null;

            DataRowView current = (DataRowView)osClienteBindingSource.Current;

            string sql = @"select m.id, m.num_serie, m.marca_instrumento, 
                                  COALESCE(c.cnt,0) cnt, m.ilha, m.descricao, m.fabricante 
                           from OsIbametroOsCliente m
                           LEFT OUTER JOIN 
                            ( select idibametro,count(1) cnt from OsIbametroInMetroOsCliente group by idibametro) c
                           ON (m.id = c.idibametro )  
		                   where idcliente = " + current["id"];
            bs_instrumentos.DataSource = DB.GetTableFromSQL(sql);
            if (bs_instrumentos.Count == 0)
            {
                createBomba();
                btnGerarLacre.Enabled = false;
                pnlEditarBomba.Visible = true;
                pnlEditarBomba.Focus();
                tbSerie.Focus();
                tbSerie.Select();
            }
            else 
            {
               
                pnlEditarBomba.Visible = false;
                btnGerarLacre.Enabled = true;

                setStatusUpdateBomba();
            }

            dgvBombas.Columns[0].Visible = false;
            dgvBombas.Columns[4].Visible = false;
            //dgvBombas.Columns[5].Visible = false;
            dgvBombas.Columns[6].Visible = false;

            dgvBombas.Columns[1].HeaderText = "Série (ID)";
            dgvBombas.Columns[2].HeaderText = "Fabricante";
            dgvBombas.Columns[3].HeaderText = "Qtde. Bicos";
            dgvBombas.Columns[5].HeaderText = "Descrição";

        }

        private void tabInstrumentos_Enter(object sender, EventArgs e)
        {
            //var debug = 1;  
            // id - readonly
            //if (dgvLacre.Columns.Count > 0)
            //  dgvLacre.Columns[0].ReadOnly = true;
            //limparCamposBomba();
            //bindBomba();
            //btnDeletarBomba.Enabled = true;
            //btnLimparBomba.Enabled = true;

            // pra observar
            dgvLacre.Visible = true;
            btnLimparBomba.Visible = true;
           
        }

        private void dgvBombas_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            // fill datagridview space right side ...    
            // when set, its no more possible user resize column width
            var dataGridView = sender as DataGridView;
            //if (dataGridView != null)
            //{
            //    if (dataGridView.ColumnCount > 0)
            //    {
            //        dataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            //        dataGridView.Columns[dataGridView.ColumnCount - 1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            //    }
            //}

            if (dataGridView.Rows.Count > 0)
            {
                dataGridView.Focus();
                try
                {
                    //dataGridView.Rows[0].Selected = true;
                    //dataGridView.Rows[0].Cells[0].Selected = true;
                    dataGridView.CurrentCell = dataGridView[0, 0];
                    //grid.CurrentCell = grid.Rows[rowIndex].Cells[1];
                }
                catch (Exception ee) { }
            }

            //bs_instrumentos.Position = 1;

            /*
             * resumindo:
             * para focusar uma linha de dataGridView:
             * Propriedades: SelectionMode: Cell
             * dataGridView.Focus();
             * dataGridView.CurrentCell = dataGridView[0, 0];
             * */
        }

        private void dgvBombas_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            selectBomba();
        }

        private void dgvBombas_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.S)
            {
                selectBomba();           
            }
        }
        private void selectBomba()
        {
            dgvBombas.Visible = false;
            rtbHelp0.Visible = false;
            rtbHelp2.Visible = true;
            btnLimparBomba.Visible = false;

            //cancelar(false);
            pnlEditarBomba.Visible = true;
            pnlEditarBomba.Focus();
            tbSerie.Focus();
            tbSerie.Select();      
        }

        private void manBomba()
        {
            //pnlEditarBomba.Visible = true;

            btnDeletarBomba.Enabled = true;
            btnLimparBomba.Enabled = true;
            btnGerarLacre.Enabled = true;

            DataRowView current = (DataRowView)bs_instrumentos.Current;

            if (current != null)
            {
                //dataGridView1.Rows.Clear();
                //dataGridView1.Refresh();
                dgvLacre.DataSource = null;

                string sql = @"select id,num_inmetro Lacre from OsIbametroInmetroOsCliente where 1=1 ";
                sql += @" and idibametro = " + current["id"].ToString();

                bs_inmetro.DataSource = DB.GetTableFromSQL(sql);

                dgvLacre.DataSource = bs_inmetro;

                //id
                dgvLacre.Columns[0].ReadOnly = true;

                dgvBombas.Columns[1].HeaderText = "Série (ID)";
                dgvBombas.Columns[2].HeaderText = "Fabricante";
                dgvBombas.Columns[3].HeaderText = "Qtde. Bicos";
                dgvBombas.Columns[5].HeaderText = "Descrição";
            }
            else {
                dgvLacre.DataSource = null;
            }

        }

        private void dgvBombas_SelectionChanged(object sender, EventArgs e)
        {
            manBomba();
        }

        private void dgvLacre_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            var dataGridView = sender as DataGridView;
            if (dataGridView != null)
            {
                if (dataGridView.ColumnCount > 0)
                {
                    dataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                    dataGridView.Columns[dataGridView.ColumnCount - 1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;                    
                }
            }
        }

        private bool isValidInputBomba() {
                      
            if (string.IsNullOrWhiteSpace(tbSerie.Text))
            {
                MessageBox.Show(@"Favor preencher Série",
                                   @"E R R O",
                                   MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
            if (string.IsNullOrWhiteSpace(tbQBicos.Text))
            {
                MessageBox.Show(@"Favor preencher Quantidade de Bicos (1-8)",
                                   @"E R R O",
                                   MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
            if (cmbMarcaInstrumento.SelectedIndex < 0) {
                MessageBox.Show(@"Favor escolher marca do instrumento",
                                 @"E R R O",
                                 MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
           
            if (lacreExists()) return false;
            return true;
        }

        private bool lacreExists() {
            if (!String.IsNullOrWhiteSpace(tbLacre.Text))
            {
                //int q = 0;
                //try
                //{
                //    q = Convert.ToInt32(tbQBicos.Text);
                //}
                //catch (Exception ex) {
                //    MessageBox.Show(ex.Message,
                //                           @"E R R O - Qtde. de Bicos",
                //                           MessageBoxButtons.OK, MessageBoxIcon.Stop);
                //    return true;
                //}

                //long lacre = Convert.ToInt64(tbLacre.Text);

                //for (int i = 0; i < q; i++)
                //{
                string lacre = tbLacre.Text;
                    //string sql = @"Select count(1) from OsIbametroInmetroOsCliente 
                    //            where num_inmetro ='" + (lacre + i) + @"'";
                string sql = @"Select count(1) from OsIbametroInmetroOsCliente 
                                where num_inmetro ='" + (lacre) + @"'";
                    int cnt = (int)DB.GetSingleValue(sql);
                    if (cnt > 0)
                    {
                        string getInfo = @"select o.id, o.razao_social, num_serie, oo.descricao 
                                         from oscliente o, osibametrooscliente oo, osibametroinmetrooscliente ooo
                                         where o.id = oo.idcliente
                                          and oo.id = ooo.idibametro
                                          and ooo.num_inmetro = '" +(lacre)+ "'";
                        DataTable dt = DB.GetTableFromSQL(getInfo);

                        string info = @"Lacre precisa ser unico (" + lacre + @")!"
                                      + Environment.NewLine
                                      + @"Já é usado em: "
                                      + Environment.NewLine
                                      + dt.Rows[0][1].ToString()
                                      + Environment.NewLine
                                      + dt.Rows[0][2].ToString()
                                      + Environment.NewLine
                                      + dt.Rows[0][3].ToString();
                                      
                        MessageBox.Show(info,
                                        @"E R R O",
                                        MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return true;
                    }
                //}                
            }
            return false;
        }
        //Inserir
        private void btnGravarBomba_Click(object sender, EventArgs e)
        {
            // 19.03.
            // limpar o campo tbLacre 
            // para que a rotina "LacreExists" NÂO seja chamada pela
            // validação
            tbLacre.Text = "";

            var debug = 1;
            if (isValidInputBomba() == false)
                return;
            if (estadoCorrente == EstadoManutencao.cbCreate)
            {
                insertBomba();
            }
            else
            {
                if (estadoCorrente == EstadoManutencao.cbUpdate)
                {
                    //setStatusUpdateBomba();
                    doUpdateBomba();
                }
            }
            pnlEditarBomba.Visible = false;
            cancelar(true);
            btnLimparBomba.Enabled = true;
            refreshBomba();
           
        }
        private void insertBomba() 
        {

            DataRowView current = (DataRowView)osClienteBindingSource.Current;
            var id_cliente = current["id"].ToString();
            
            string posto = OsConfiguracoes.idPosto.ToString();
            DateTime now = DateTime.Now;
            string serie = tbSerie.Text;
            string bicos = tbQBicos.Text;
            string ilha = tbIlha.Text;
            string descricao = tbDescricao.Text;
            string fabricante = tbFabricante.Text;
            string marca = cmbMarcaInstrumento.SelectedItem.ToString();

            string sql = @"INSERT INTO osIbametroOsCliente(idcliente, posto, num_serie,marca_instrumento,ilha,descricao,fabricante,dt_cadastro) 
                            VALUES(";
            sql += id_cliente + @",";
            sql += @"'" + posto + @"',";
            sql += @"'" + serie + @"',";
            sql += @"'" + marca + @"',";
            sql += @"'" + ilha + @"',";
            sql += @"'" + descricao + @"',";
            sql += @"'" + fabricante + @"',";
            sql += @"'" + now + @"')";
            try
            {
                DB.ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,
                                @"E R R O - Insert",
                                MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
          
            // incluir Lacres
            string maxid = DB.GetSingleValue(@"Select max(id) from osIbametroOsCliente").ToString();           
            int q = Convert.ToInt32(tbQBicos.Text);

            if (!String.IsNullOrWhiteSpace(tbLacre.Text))
            {
                //int lacre = Convert.ToInt32(tbLacre.Text);
                long lacre = Convert.ToInt64(tbLacre.Text);
                for (int i = 0; i < q; i++)
                {
                    long num = lacre + i;
                    string sql_inmetro = @"INSERT INTO OsIbametroInmetroOsCliente(num_inmetro, idibametro) 
                                   VALUES('" + num.ToString() + "'," + maxid + ")";
                    DB.ExecuteNonQuery(sql_inmetro);
                }
            }

            //bindBomba();
            //refreshBomba();
            // after insert enter update mode
            setStatusUpdateBomba();
        }

        private void doUpdateBomba() {
            DataRowView current = (DataRowView)osClienteBindingSource.Current;
            var id_cliente = current["id"].ToString();

            DataRowView currentBomba = (DataRowView)bs_instrumentos.Current;
            var id_bomba = currentBomba["id"].ToString();

            string posto = OsConfiguracoes.idPosto.ToString();
            DateTime now = DateTime.Now;
            string num_serie = tbSerie.Text;
            string bicos = tbQBicos.Text;
            string ilha = tbIlha.Text;
            string descricao = tbDescricao.Text;
            string fabricante = tbFabricante.Text;
            string marca = cmbMarcaInstrumento.SelectedItem.ToString();
            
            string sql = @"UPDATE OsIbametroOsCliente SET 
                         posto = '" + posto + @"'";
            sql += @",num_serie='" + num_serie + @"'";
            sql += @",marca_instrumento='" + marca + @"'";
            sql += @",ilha='" + ilha + @"'";
            sql += @",descricao='" + descricao + @"'";
            sql += @",fabricante='" + fabricante + @"'";
            sql += @",dt_cadastro='" + now + @"'";

            sql += @" WHERE id=" + id_bomba;

            try
            {
                DB.ExecuteNonQuery(sql);
            }
            catch(Exception ex) {
                MessageBox.Show(ex.Message,
                                @"E R R O - Update",
                                MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            //refreshBomba();
        }
        private void setStatusUpdateBomba()
        {
            updateBomba();
        }
        private void tbQBicos_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);  
        }

        private void tabInstrumentos_Leave(object sender, EventArgs e)
        {
           
            //var x = tabChild.SelectedTab;
            //var debug = 1;
            //if (estadoCorrente != EstadoManutencao.smInserir)
            //    editar();
            //buttonContatosDeletar.Visible = false;

            if (tabChild.SelectedTab != tabChild.TabPages["tabInstrumentos"]) 
            {
                editar();
            }
        }

        private void limparCamposBomba()
        {
            tbSerie.Text = "";
            tbQBicos.Text = "";
            tbIlha.Text = "";
            tbDescricao.Text = "";
            tbFabricante.Text = "";
            cmbMarcaInstrumento.SelectedIndex = -1;
            tbLacre.Text = "";

            dgvLacre.DataSource = null;
        }

        private void btnLimparBomba_Click(object sender, EventArgs e)
        {
            limparCamposBomba();
            createBomba();
            pnlEditarBomba.Visible = true;
            pnlEditarBomba.Focus();
            tbSerie.Focus();
            tbSerie.Select();

            btnDeletarBomba.Enabled = false;
            btnLimparBomba.Enabled = false;
            btnGerarLacre.Enabled = false;

            // hide tela 1
            cancelar(false);
        }

        private void btnDeletarBomba_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Tem certeza que queira apagar esta bomba ??",
                                     "Confirmar",
                                     MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                // If 'Yes', do something here.
                //DB.ExecuteNonQuery(sql_delete);
                //limparContatosCampos();
                //fillList((int)id_cliente);
                //int cnt = countContatos((int)id_cliente);
                //setLabelText("labelContatosNumero", "Número dos contatos: " + cnt.ToString());
                //create();
                //buttonContatosDeletar.Visible = false;

                DataRowView current = (DataRowView)bs_instrumentos.Current;
                var id = current["id"].ToString();

                string del = @"DELETE FROM OsIbametroOsCliente WHERE id=" + id;
                DB.ExecuteNonQuery(del);
                del = @"DELETE FROM OsIbametroInMetroOsCliente WHERE idibametro=" + id;
                DB.ExecuteNonQuery(del);

                refreshLacre();
                refreshBomba();

                btnDeletarBomba.Enabled = true;
                btnLimparBomba.Enabled = true;
                btnGerarLacre.Enabled = true;

                cancelar(true);
                pnlEditarBomba.Visible = false;

            }
            else
            {
                // nada pra fazer
            }
        }

        private void btnGerarLacre_Click(object sender, EventArgs e)
        {
            //string maxid = DB.GetSingleValue(@"Select max(id) from osIbametroOsCliente").ToString();
            DataRowView current = (DataRowView)bs_instrumentos.Current;
            var id = current["id"].ToString();
            //int q = 0;

            //try
            //{
            //    q = Convert.ToInt32(tbQBicos.Text);
            //}
            //catch (Exception ex) {
            //    MessageBox.Show(ex.Message,
            //                       @"E R R O - Gerar",
            //                       MessageBoxButtons.OK, MessageBoxIcon.Stop);
            //    return;
            //}

            if (lacreExists()) return;

            if (!String.IsNullOrWhiteSpace(tbLacre.Text))
            {
//                // first delete existing
//                string del = @"DELETE FROM OsIbametroInmetroOsCliente WHERE idibametro = " + id;
//                DB.ExecuteNonQuery(del);

//                int lacre = Convert.ToInt32(tbLacre.Text);
//                for (int i = 0; i < q; i++)
//                {
//                    int num = lacre + i;
//                    string sql_inmetro = @"INSERT INTO OsIbametroInmetroOsCliente(num_inmetro, idibametro) 
//                                   VALUES('" + num.ToString() + "'," + id + ")";
//                    DB.ExecuteNonQuery(sql_inmetro);
//                }
                string lacre = tbLacre.Text;
                string sql_inmetro = @"INSERT INTO OsIbametroInmetroOsCliente(num_inmetro, idibametro) 
                                       VALUES('" + lacre + "'," + id + ")";
                try
                {
                    DB.ExecuteNonQuery(sql_inmetro);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message,
                                    @"E R R O - Insert Lacre",
                                    MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }

            refreshLacre();
            refreshBomba();
        }

        private void dgvLacre_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            // cell = lacre
            if (dgvLacre.CurrentCell.ColumnIndex == 1)
            {
                dgvLacre.Tag = dgvLacre.CurrentCell.Value;
            }
        }

        private void dgvLacre_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {                      
            // cell = lacre
            if (dgvLacre.CurrentCell.ColumnIndex == 1)
            {

                if (dgvLacre.CurrentCell.Value != dgvLacre.Tag)
                {
                    string sql = @"Select count(1) from OsIbametroInmetroOsCliente 
                                where num_inmetro ='" + dgvLacre.CurrentCell.Value + @"'";
                    int cnt = (int)DB.GetSingleValue(sql);
                    if (cnt > 0)
                    {
                        MessageBox.Show(@"Lacre " + dgvLacre.CurrentCell.Value + @" já existe!",
                                      @"E R R O - Alterar Lacre",
                                      MessageBoxButtons.OK, MessageBoxIcon.Stop);

                        dgvLacre.CurrentCell.Value = dgvLacre.Tag;
                    }
                    else
                    {
                        DataRowView current = (DataRowView)bs_inmetro.Current;
                        var id = current["id"].ToString();
                        string update = @"UPDATE OsIbametroInmetroOsCliente SET num_inmetro='"
                                          + dgvLacre.CurrentCell.Value
                                          + @"' WHERE id = " + id;
                        DB.ExecuteNonQuery(update);
                        MessageBox.Show(@"Lacre modificado!",
                                      @"S U C E S S O",
                                      MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }

        private void cancelar(bool x) 
        {
            // x == true , tela 1 visivel
            // x == false, tela 1 invisivel
            dgvBombas.Visible = x;
            rtbHelp0.Visible = x;
            rtbHelp2.Visible =!x;
            btnLimparBomba.Visible = x;
            // set statusline e modus
            if (x)
            {
                updateBomba();
                btnLimparBomba.Enabled = true;
                refreshBomba();
            }
            else
            {
                createBomba();
                btnLimparBomba.Enabled = false;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            // tela 1
            cancelar(true);
            // tela 2
            pnlEditarBomba.Visible = false;
        }

        private void tabChild_SelectedIndexChanged(object sender, EventArgs e)
        {
            var debug = 1;
            if (tabChild.SelectedTab == tabChild.TabPages["tabInstrumentos"]) {
                var x = 2;
                limparCamposBomba();
                bindBomba();
                btnDeletarBomba.Enabled = true;
                btnLimparBomba.Enabled = true;

                //var dataGridView = dgvBombas;
                //if (dataGridView.Rows.Count > 0)
                //{
                //    dataGridView.Focus();
                //    try
                //    {
                //        //dataGridView.Rows[0].Selected = true;
                //        //dataGridView.Rows[0].Cells[0].Selected = true;
                //        dataGridView.CurrentCell = dataGridView[0, 0];
                //        //grid.CurrentCell = grid.Rows[rowIndex].Cells[1];
                //    }
                //    catch (Exception ee) { }
                //}
            };
        }

        private void dgvLacre_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {                
                var confirmResult = MessageBox.Show("Tem certeza que queira apagar este lacre ??",
                                     "Confirmar",
                                     MessageBoxButtons.YesNo);
                if (confirmResult == DialogResult.Yes)
                {                  
                    DataRowView current = (DataRowView)bs_inmetro.Current;
                    string id = current["id"].ToString();
                    string del = @"DELETE FROM OsIbametroInmetroOsCliente WHERE id = "+id;
                    DB.ExecuteNonQuery(del);

                    refreshBomba();
                    refreshLacre();
                }
                else { }
            }
        }

        private void rtbHelp2_Click(object sender, EventArgs e)
        {
           // pnlEditarBomba.Visible = !pnlEditarBomba.Visible;
            
        }
                       
    }
}
