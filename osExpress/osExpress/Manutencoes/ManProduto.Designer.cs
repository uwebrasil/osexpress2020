﻿namespace osExpress.Manutencoes
{
    partial class ManProduto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ProdutoBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bnInserir = new System.Windows.Forms.ToolStripButton();
            this.OsProdutoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dtsA30Produto = new osExpress.dtsA30Produto();
            this.bnCount = new System.Windows.Forms.ToolStripLabel();
            this.bnConsultar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.bnGravar = new System.Windows.Forms.ToolStripButton();
            this.bnCancelar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.bnPrimeiro = new System.Windows.Forms.ToolStripButton();
            this.bnAnterior = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bnPosition = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bnProximo = new System.Windows.Forms.ToolStripButton();
            this.bnUltimo = new System.Windows.Forms.ToolStripButton();
            this.bnDescartar = new System.Windows.Forms.ToolStripButton();
            this.a30PRODUTOTableAdapter1 = new osExpress.dtsA30ProdutoTableAdapters.A30PRODUTOTableAdapter();
            this.tableLayoutPanelConsultar = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.headLabel = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutFilter = new System.Windows.Forms.TableLayoutPanel();
            this.cbFiltroAtivo = new System.Windows.Forms.CheckBox();
            this.flowLayoutPanelFiltroNome = new System.Windows.Forms.FlowLayoutPanel();
            this.labelFiltroNome = new System.Windows.Forms.Label();
            this.tbFiltroNome = new System.Windows.Forms.TextBox();
            this.dgvProduto = new System.Windows.Forms.DataGridView();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.postoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.produtoDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descricaoDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codBarraDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grupoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.comissaoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vlComisDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtLanctoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.aliquotaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.estMinimoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.precoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uniCompraDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uniVendaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.relacaoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.custoMedioDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtUltCompraDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vlUltCompraDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vlUltPrecoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.icmsRetDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.claFiscalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sitTributDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.paginaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codInternoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pontoQuantDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pontoValDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtAlterDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.odometroDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.inativoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mP135DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sefazDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.margemMaxDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codigoTCSDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pesoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.limiteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grupoIFDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grupoTCSDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.volDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codigoPetroDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fotoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cozinhaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codigoNCMDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codigoANPDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codigoValeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bloquearAcimaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bloquearFracionarioDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codigoFidelizeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sTPISDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sTCofinsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.aliquotaPISDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.aliquotaCofinsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codigoEXCardDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.natOperDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel4ConsultarBottom = new System.Windows.Forms.Panel();
            this.flowLayoutPanelEditar = new System.Windows.Forms.FlowLayoutPanel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.headLabelEditar = new System.Windows.Forms.Label();
            this.tableLayoutPanelEditar = new System.Windows.Forms.TableLayoutPanel();
            this.panel2Editar = new System.Windows.Forms.Panel();
            this.lblTrackBar = new System.Windows.Forms.Label();
            this.tbTrackValue = new System.Windows.Forms.TextBox();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.btnResizeColWidth = new System.Windows.Forms.Button();
            this.tabControlEditar = new System.Windows.Forms.TabControl();
            this.tabPage1Dados = new System.Windows.Forms.TabPage();
            this.tableLayoutPanelDados = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelTipoProduto = new System.Windows.Forms.TableLayoutPanel();
            this.labelTipoProduto = new System.Windows.Forms.Label();
            this.cmbTipoProduto = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanelAtivo = new System.Windows.Forms.TableLayoutPanel();
            this.labelDummyAtivo = new System.Windows.Forms.Label();
            this.cbAtivo = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanelRow2Col1_2 = new System.Windows.Forms.TableLayoutPanel();
            this.labelCodigo = new System.Windows.Forms.Label();
            this.tbCodigo = new System.Windows.Forms.TextBox();
            this.tableLayoutPanelNome = new System.Windows.Forms.TableLayoutPanel();
            this.labelNome = new System.Windows.Forms.Label();
            this.tbNome = new System.Windows.Forms.TextBox();
            this.tableLayoutPanelGrupo = new System.Windows.Forms.TableLayoutPanel();
            this.labelGrupo = new System.Windows.Forms.Label();
            this.cmbGrupo = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanelUnidade = new System.Windows.Forms.TableLayoutPanel();
            this.labelUnidade = new System.Windows.Forms.Label();
            this.cmbUnidade = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanelComissao = new System.Windows.Forms.TableLayoutPanel();
            this.labelComissao = new System.Windows.Forms.Label();
            this.tbComissao = new System.Windows.Forms.TextBox();
            this.tableLayoutPanelComissaoValor = new System.Windows.Forms.TableLayoutPanel();
            this.labelComissaoValor = new System.Windows.Forms.Label();
            this.tbComissaoValor = new System.Windows.Forms.TextBox();
            this.tableLayoutPanelPreco = new System.Windows.Forms.TableLayoutPanel();
            this.labelPreco = new System.Windows.Forms.Label();
            this.tbPreco = new System.Windows.Forms.TextBox();
            this.panelFillerRow1 = new System.Windows.Forms.Panel();
            this.panelFillRow1ColLast = new System.Windows.Forms.Panel();
            this.tableLayoutPanelManutensao = new System.Windows.Forms.TableLayoutPanel();
            this.labelManutencao = new System.Windows.Forms.Label();
            this.tbManutencao = new System.Windows.Forms.TextBox();
            this.panel4Editar = new System.Windows.Forms.Panel();
            this.produtoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descricaoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dtsPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProdutoBindingNavigator)).BeginInit();
            this.ProdutoBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OsProdutoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtsA30Produto)).BeginInit();
            this.tableLayoutPanelConsultar.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tableLayoutFilter.SuspendLayout();
            this.flowLayoutPanelFiltroNome.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProduto)).BeginInit();
            this.flowLayoutPanelEditar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.tableLayoutPanelEditar.SuspendLayout();
            this.panel2Editar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.tabControlEditar.SuspendLayout();
            this.tabPage1Dados.SuspendLayout();
            this.tableLayoutPanelDados.SuspendLayout();
            this.tableLayoutPanelTipoProduto.SuspendLayout();
            this.tableLayoutPanelAtivo.SuspendLayout();
            this.tableLayoutPanelRow2Col1_2.SuspendLayout();
            this.tableLayoutPanelNome.SuspendLayout();
            this.tableLayoutPanelGrupo.SuspendLayout();
            this.tableLayoutPanelUnidade.SuspendLayout();
            this.tableLayoutPanelComissao.SuspendLayout();
            this.tableLayoutPanelComissaoValor.SuspendLayout();
            this.tableLayoutPanelPreco.SuspendLayout();
            this.tableLayoutPanelManutensao.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpPrincipal
            // 
            this.grpPrincipal.Location = new System.Drawing.Point(1200, 87);
            this.grpPrincipal.Visible = false;
            // 
            // tabPrincipal
            // 
            this.tabPrincipal.Location = new System.Drawing.Point(1020, 187);
            this.tabPrincipal.Visible = false;
            // 
            // ProdutoBindingNavigator
            // 
            this.ProdutoBindingNavigator.AddNewItem = this.bnInserir;
            this.ProdutoBindingNavigator.BindingSource = this.OsProdutoBindingSource;
            this.ProdutoBindingNavigator.CountItem = this.bnCount;
            this.ProdutoBindingNavigator.CountItemFormat = "de {0}";
            this.ProdutoBindingNavigator.DeleteItem = null;
            this.ProdutoBindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.ProdutoBindingNavigator.ImageScalingSize = new System.Drawing.Size(48, 48);
            this.ProdutoBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bnConsultar,
            this.toolStripSeparator3,
            this.bnInserir,
            this.bnGravar,
            this.bnCancelar,
            this.toolStripSeparator4,
            this.bnPrimeiro,
            this.bnAnterior,
            this.toolStripSeparator1,
            this.bnPosition,
            this.bnCount,
            this.toolStripSeparator2,
            this.bnProximo,
            this.bnUltimo,
            this.bnDescartar});
            this.ProdutoBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.ProdutoBindingNavigator.MoveFirstItem = this.bnPrimeiro;
            this.ProdutoBindingNavigator.MoveLastItem = this.bnUltimo;
            this.ProdutoBindingNavigator.MoveNextItem = this.bnProximo;
            this.ProdutoBindingNavigator.MovePreviousItem = this.bnAnterior;
            this.ProdutoBindingNavigator.Name = "ProdutoBindingNavigator";
            this.ProdutoBindingNavigator.PositionItem = this.bnPosition;
            this.ProdutoBindingNavigator.Size = new System.Drawing.Size(959, 70);
            this.ProdutoBindingNavigator.TabIndex = 14;
            this.ProdutoBindingNavigator.Text = "ProdutoBindingNavigator";
            // 
            // bnInserir
            // 
            this.bnInserir.Image = global::osExpress.Properties.Resources.insert;
            this.bnInserir.Name = "bnInserir";
            this.bnInserir.RightToLeftAutoMirrorImage = true;
            this.bnInserir.Size = new System.Drawing.Size(52, 67);
            this.bnInserir.Text = "&Inserir";
            this.bnInserir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnInserir.Click += new System.EventHandler(this.bnInserir_Click);
            // 
            // OsProdutoBindingSource
            // 
            this.OsProdutoBindingSource.DataMember = "A30PRODUTO";
            this.OsProdutoBindingSource.DataSource = this.dtsA30Produto;
            // 
            // dtsA30Produto
            // 
            this.dtsA30Produto.DataSetName = "dtsA30Produto";
            this.dtsA30Produto.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bnCount
            // 
            this.bnCount.Name = "bnCount";
            this.bnCount.Size = new System.Drawing.Size(37, 67);
            this.bnCount.Text = "de {0}";
            this.bnCount.ToolTipText = "Total number of items";
            // 
            // bnConsultar
            // 
            this.bnConsultar.Image = global::osExpress.Properties.Resources.search;
            this.bnConsultar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnConsultar.Name = "bnConsultar";
            this.bnConsultar.Size = new System.Drawing.Size(62, 67);
            this.bnConsultar.Text = "Consultar";
            this.bnConsultar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnConsultar.Click += new System.EventHandler(this.bnConsultar_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 70);
            // 
            // bnGravar
            // 
            this.bnGravar.Image = global::osExpress.Properties.Resources.save;
            this.bnGravar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnGravar.Name = "bnGravar";
            this.bnGravar.Size = new System.Drawing.Size(52, 67);
            this.bnGravar.Text = "Grav&ar";
            this.bnGravar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnGravar.Click += new System.EventHandler(this.bnGravar_Click);
            // 
            // bnCancelar
            // 
            this.bnCancelar.Enabled = false;
            this.bnCancelar.Image = global::osExpress.Properties.Resources.clean;
            this.bnCancelar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnCancelar.Name = "bnCancelar";
            this.bnCancelar.Size = new System.Drawing.Size(57, 67);
            this.bnCancelar.Text = "Cancelar";
            this.bnCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnCancelar.Click += new System.EventHandler(this.bnCancelar_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 70);
            // 
            // bnPrimeiro
            // 
            this.bnPrimeiro.Image = global::osExpress.Properties.Resources.first;
            this.bnPrimeiro.Name = "bnPrimeiro";
            this.bnPrimeiro.RightToLeftAutoMirrorImage = true;
            this.bnPrimeiro.Size = new System.Drawing.Size(56, 67);
            this.bnPrimeiro.Text = "&Primeiro";
            this.bnPrimeiro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnPrimeiro.Click += new System.EventHandler(this.bnPrimeiro_Click);
            // 
            // bnAnterior
            // 
            this.bnAnterior.Image = global::osExpress.Properties.Resources.previous;
            this.bnAnterior.Name = "bnAnterior";
            this.bnAnterior.RightToLeftAutoMirrorImage = true;
            this.bnAnterior.Size = new System.Drawing.Size(54, 67);
            this.bnAnterior.Text = "Anteri&or";
            this.bnAnterior.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnAnterior.Click += new System.EventHandler(this.bnAnterior_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 70);
            // 
            // bnPosition
            // 
            this.bnPosition.AccessibleName = "Position";
            this.bnPosition.AutoSize = false;
            this.bnPosition.Name = "bnPosition";
            this.bnPosition.Size = new System.Drawing.Size(50, 23);
            this.bnPosition.Text = "0";
            this.bnPosition.ToolTipText = "Current position";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 70);
            // 
            // bnProximo
            // 
            this.bnProximo.Image = global::osExpress.Properties.Resources.next;
            this.bnProximo.Name = "bnProximo";
            this.bnProximo.RightToLeftAutoMirrorImage = true;
            this.bnProximo.Size = new System.Drawing.Size(56, 67);
            this.bnProximo.Text = "Próxi&mo";
            this.bnProximo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnProximo.Click += new System.EventHandler(this.bnProximo_Click);
            // 
            // bnUltimo
            // 
            this.bnUltimo.Image = global::osExpress.Properties.Resources.last;
            this.bnUltimo.Name = "bnUltimo";
            this.bnUltimo.RightToLeftAutoMirrorImage = true;
            this.bnUltimo.Size = new System.Drawing.Size(52, 67);
            this.bnUltimo.Text = "&Último";
            this.bnUltimo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnUltimo.Click += new System.EventHandler(this.bnUltimo_Click);
            // 
            // bnDescartar
            // 
            this.bnDescartar.Enabled = false;
            this.bnDescartar.Image = global::osExpress.Properties.Resources.remove;
            this.bnDescartar.Name = "bnDescartar";
            this.bnDescartar.RightToLeftAutoMirrorImage = true;
            this.bnDescartar.Size = new System.Drawing.Size(60, 67);
            this.bnDescartar.Text = "&Descartar";
            this.bnDescartar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnDescartar.Click += new System.EventHandler(this.bnDescartar_Click);
            // 
            // a30PRODUTOTableAdapter1
            // 
            this.a30PRODUTOTableAdapter1.ClearBeforeFill = true;
            // 
            // tableLayoutPanelConsultar
            // 
            this.tableLayoutPanelConsultar.BackColor = System.Drawing.Color.BurlyWood;
            this.tableLayoutPanelConsultar.ColumnCount = 1;
            this.tableLayoutPanelConsultar.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelConsultar.Controls.Add(this.flowLayoutPanel1, 0, 0);
            this.tableLayoutPanelConsultar.Controls.Add(this.groupBox1, 0, 1);
            this.tableLayoutPanelConsultar.Controls.Add(this.dgvProduto, 0, 2);
            this.tableLayoutPanelConsultar.Controls.Add(this.panel4ConsultarBottom, 0, 3);
            this.tableLayoutPanelConsultar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelConsultar.Location = new System.Drawing.Point(0, 70);
            this.tableLayoutPanelConsultar.Name = "tableLayoutPanelConsultar";
            this.tableLayoutPanelConsultar.RowCount = 4;
            this.tableLayoutPanelConsultar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13F));
            this.tableLayoutPanelConsultar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanelConsultar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 67F));
            this.tableLayoutPanelConsultar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanelConsultar.Size = new System.Drawing.Size(959, 419);
            this.tableLayoutPanelConsultar.TabIndex = 15;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.flowLayoutPanel1.Controls.Add(this.pictureBox1);
            this.flowLayoutPanel1.Controls.Add(this.headLabel);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(308, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(342, 48);
            this.flowLayoutPanel1.TabIndex = 18;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::osExpress.Properties.Resources.search;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(51, 50);
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            // 
            // headLabel
            // 
            this.headLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.headLabel.AutoSize = true;
            this.headLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.headLabel.Location = new System.Drawing.Point(60, 18);
            this.headLabel.Name = "headLabel";
            this.headLabel.Size = new System.Drawing.Size(278, 20);
            this.headLabel.TabIndex = 0;
            this.headLabel.Text = "Manutenção Produtos - Consultar";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutFilter);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 57);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(953, 35);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filtro";
            // 
            // tableLayoutFilter
            // 
            this.tableLayoutFilter.BackColor = System.Drawing.Color.Chocolate;
            this.tableLayoutFilter.ColumnCount = 2;
            this.tableLayoutFilter.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutFilter.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutFilter.Controls.Add(this.cbFiltroAtivo, 0, 0);
            this.tableLayoutFilter.Controls.Add(this.flowLayoutPanelFiltroNome, 1, 0);
            this.tableLayoutFilter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutFilter.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutFilter.Name = "tableLayoutFilter";
            this.tableLayoutFilter.RowCount = 1;
            this.tableLayoutFilter.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutFilter.Size = new System.Drawing.Size(947, 16);
            this.tableLayoutFilter.TabIndex = 21;
            // 
            // cbFiltroAtivo
            // 
            this.cbFiltroAtivo.AutoSize = true;
            this.cbFiltroAtivo.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbFiltroAtivo.Checked = true;
            this.cbFiltroAtivo.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbFiltroAtivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFiltroAtivo.Location = new System.Drawing.Point(3, 8);
            this.cbFiltroAtivo.Margin = new System.Windows.Forms.Padding(3, 8, 3, 3);
            this.cbFiltroAtivo.Name = "cbFiltroAtivo";
            this.cbFiltroAtivo.Size = new System.Drawing.Size(61, 5);
            this.cbFiltroAtivo.TabIndex = 0;
            this.cbFiltroAtivo.Text = "ativo?";
            this.cbFiltroAtivo.UseVisualStyleBackColor = true;
            this.cbFiltroAtivo.CheckedChanged += new System.EventHandler(this.cbFiltroAtivo_CheckedChanged);
            // 
            // flowLayoutPanelFiltroNome
            // 
            this.flowLayoutPanelFiltroNome.Controls.Add(this.labelFiltroNome);
            this.flowLayoutPanelFiltroNome.Controls.Add(this.tbFiltroNome);
            this.flowLayoutPanelFiltroNome.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanelFiltroNome.Location = new System.Drawing.Point(192, 3);
            this.flowLayoutPanelFiltroNome.Name = "flowLayoutPanelFiltroNome";
            this.flowLayoutPanelFiltroNome.Size = new System.Drawing.Size(752, 10);
            this.flowLayoutPanelFiltroNome.TabIndex = 1;
            // 
            // labelFiltroNome
            // 
            this.labelFiltroNome.AutoSize = true;
            this.labelFiltroNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFiltroNome.Location = new System.Drawing.Point(3, 6);
            this.labelFiltroNome.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.labelFiltroNome.Name = "labelFiltroNome";
            this.labelFiltroNome.Size = new System.Drawing.Size(57, 13);
            this.labelFiltroNome.TabIndex = 0;
            this.labelFiltroNome.Text = "%Nome%";
            // 
            // tbFiltroNome
            // 
            this.tbFiltroNome.Location = new System.Drawing.Point(66, 3);
            this.tbFiltroNome.Name = "tbFiltroNome";
            this.tbFiltroNome.Size = new System.Drawing.Size(386, 20);
            this.tbFiltroNome.TabIndex = 1;
            this.tbFiltroNome.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbFiltroNome_KeyDown);
            // 
            // dgvProduto
            // 
            this.dgvProduto.AllowUserToAddRows = false;
            this.dgvProduto.AllowUserToDeleteRows = false;
            this.dgvProduto.AutoGenerateColumns = false;
            this.dgvProduto.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvProduto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProduto.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.postoDataGridViewTextBoxColumn,
            this.produtoDataGridViewTextBoxColumn1,
            this.descricaoDataGridViewTextBoxColumn1,
            this.codBarraDataGridViewTextBoxColumn,
            this.tipoDataGridViewTextBoxColumn,
            this.grupoDataGridViewTextBoxColumn,
            this.comissaoDataGridViewTextBoxColumn,
            this.vlComisDataGridViewTextBoxColumn,
            this.dtLanctoDataGridViewTextBoxColumn,
            this.aliquotaDataGridViewTextBoxColumn,
            this.estMinimoDataGridViewTextBoxColumn,
            this.precoDataGridViewTextBoxColumn,
            this.uniCompraDataGridViewTextBoxColumn,
            this.uniVendaDataGridViewTextBoxColumn,
            this.relacaoDataGridViewTextBoxColumn,
            this.custoMedioDataGridViewTextBoxColumn,
            this.dtUltCompraDataGridViewTextBoxColumn,
            this.vlUltCompraDataGridViewTextBoxColumn,
            this.vlUltPrecoDataGridViewTextBoxColumn,
            this.icmsRetDataGridViewTextBoxColumn,
            this.claFiscalDataGridViewTextBoxColumn,
            this.sitTributDataGridViewTextBoxColumn,
            this.paginaDataGridViewTextBoxColumn,
            this.codInternoDataGridViewTextBoxColumn,
            this.pontoQuantDataGridViewTextBoxColumn,
            this.pontoValDataGridViewTextBoxColumn,
            this.dtAlterDataGridViewTextBoxColumn,
            this.odometroDataGridViewTextBoxColumn,
            this.inativoDataGridViewTextBoxColumn,
            this.mP135DataGridViewTextBoxColumn,
            this.sefazDataGridViewTextBoxColumn,
            this.margemMaxDataGridViewTextBoxColumn,
            this.codigoTCSDataGridViewTextBoxColumn,
            this.pesoDataGridViewTextBoxColumn,
            this.limiteDataGridViewTextBoxColumn,
            this.grupoIFDataGridViewTextBoxColumn,
            this.grupoTCSDataGridViewTextBoxColumn,
            this.volDataGridViewTextBoxColumn,
            this.codigoPetroDataGridViewTextBoxColumn,
            this.fotoDataGridViewTextBoxColumn,
            this.cozinhaDataGridViewTextBoxColumn,
            this.codigoNCMDataGridViewTextBoxColumn,
            this.codigoANPDataGridViewTextBoxColumn,
            this.codigoValeDataGridViewTextBoxColumn,
            this.bloquearAcimaDataGridViewTextBoxColumn,
            this.bloquearFracionarioDataGridViewTextBoxColumn,
            this.codigoFidelizeDataGridViewTextBoxColumn,
            this.sTPISDataGridViewTextBoxColumn,
            this.sTCofinsDataGridViewTextBoxColumn,
            this.aliquotaPISDataGridViewTextBoxColumn,
            this.aliquotaCofinsDataGridViewTextBoxColumn,
            this.codigoEXCardDataGridViewTextBoxColumn,
            this.natOperDataGridViewTextBoxColumn});
            this.dgvProduto.DataSource = this.OsProdutoBindingSource;
            this.dgvProduto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvProduto.Location = new System.Drawing.Point(3, 98);
            this.dgvProduto.Name = "dgvProduto";
            this.dgvProduto.ReadOnly = true;
            this.dgvProduto.Size = new System.Drawing.Size(953, 274);
            this.dgvProduto.TabIndex = 19;
            this.dgvProduto.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProduto_CellDoubleClick);
            this.dgvProduto.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvProduto_KeyDown);
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // postoDataGridViewTextBoxColumn
            // 
            this.postoDataGridViewTextBoxColumn.DataPropertyName = "Posto";
            this.postoDataGridViewTextBoxColumn.HeaderText = "Posto";
            this.postoDataGridViewTextBoxColumn.Name = "postoDataGridViewTextBoxColumn";
            this.postoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // produtoDataGridViewTextBoxColumn1
            // 
            this.produtoDataGridViewTextBoxColumn1.DataPropertyName = "Produto";
            this.produtoDataGridViewTextBoxColumn1.HeaderText = "Produto";
            this.produtoDataGridViewTextBoxColumn1.Name = "produtoDataGridViewTextBoxColumn1";
            this.produtoDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // descricaoDataGridViewTextBoxColumn1
            // 
            this.descricaoDataGridViewTextBoxColumn1.DataPropertyName = "Descricao";
            this.descricaoDataGridViewTextBoxColumn1.HeaderText = "Descricao";
            this.descricaoDataGridViewTextBoxColumn1.Name = "descricaoDataGridViewTextBoxColumn1";
            this.descricaoDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // codBarraDataGridViewTextBoxColumn
            // 
            this.codBarraDataGridViewTextBoxColumn.DataPropertyName = "Cod_Barra";
            this.codBarraDataGridViewTextBoxColumn.HeaderText = "Cod_Barra";
            this.codBarraDataGridViewTextBoxColumn.Name = "codBarraDataGridViewTextBoxColumn";
            this.codBarraDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tipoDataGridViewTextBoxColumn
            // 
            this.tipoDataGridViewTextBoxColumn.DataPropertyName = "Tipo";
            this.tipoDataGridViewTextBoxColumn.HeaderText = "Tipo";
            this.tipoDataGridViewTextBoxColumn.Name = "tipoDataGridViewTextBoxColumn";
            this.tipoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // grupoDataGridViewTextBoxColumn
            // 
            this.grupoDataGridViewTextBoxColumn.DataPropertyName = "Grupo";
            this.grupoDataGridViewTextBoxColumn.HeaderText = "Grupo";
            this.grupoDataGridViewTextBoxColumn.Name = "grupoDataGridViewTextBoxColumn";
            this.grupoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // comissaoDataGridViewTextBoxColumn
            // 
            this.comissaoDataGridViewTextBoxColumn.DataPropertyName = "Comissao";
            this.comissaoDataGridViewTextBoxColumn.HeaderText = "Comissao";
            this.comissaoDataGridViewTextBoxColumn.Name = "comissaoDataGridViewTextBoxColumn";
            this.comissaoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // vlComisDataGridViewTextBoxColumn
            // 
            this.vlComisDataGridViewTextBoxColumn.DataPropertyName = "Vl_Comis";
            this.vlComisDataGridViewTextBoxColumn.HeaderText = "Vl_Comis";
            this.vlComisDataGridViewTextBoxColumn.Name = "vlComisDataGridViewTextBoxColumn";
            this.vlComisDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dtLanctoDataGridViewTextBoxColumn
            // 
            this.dtLanctoDataGridViewTextBoxColumn.DataPropertyName = "Dt_Lancto";
            this.dtLanctoDataGridViewTextBoxColumn.HeaderText = "Dt_Lancto";
            this.dtLanctoDataGridViewTextBoxColumn.Name = "dtLanctoDataGridViewTextBoxColumn";
            this.dtLanctoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // aliquotaDataGridViewTextBoxColumn
            // 
            this.aliquotaDataGridViewTextBoxColumn.DataPropertyName = "Aliquota";
            this.aliquotaDataGridViewTextBoxColumn.HeaderText = "Aliquota";
            this.aliquotaDataGridViewTextBoxColumn.Name = "aliquotaDataGridViewTextBoxColumn";
            this.aliquotaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // estMinimoDataGridViewTextBoxColumn
            // 
            this.estMinimoDataGridViewTextBoxColumn.DataPropertyName = "Est_Minimo";
            this.estMinimoDataGridViewTextBoxColumn.HeaderText = "Est_Minimo";
            this.estMinimoDataGridViewTextBoxColumn.Name = "estMinimoDataGridViewTextBoxColumn";
            this.estMinimoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // precoDataGridViewTextBoxColumn
            // 
            this.precoDataGridViewTextBoxColumn.DataPropertyName = "Preco";
            this.precoDataGridViewTextBoxColumn.HeaderText = "Preco";
            this.precoDataGridViewTextBoxColumn.Name = "precoDataGridViewTextBoxColumn";
            this.precoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // uniCompraDataGridViewTextBoxColumn
            // 
            this.uniCompraDataGridViewTextBoxColumn.DataPropertyName = "Uni_Compra";
            this.uniCompraDataGridViewTextBoxColumn.HeaderText = "Uni_Compra";
            this.uniCompraDataGridViewTextBoxColumn.Name = "uniCompraDataGridViewTextBoxColumn";
            this.uniCompraDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // uniVendaDataGridViewTextBoxColumn
            // 
            this.uniVendaDataGridViewTextBoxColumn.DataPropertyName = "Uni_Venda";
            this.uniVendaDataGridViewTextBoxColumn.HeaderText = "Uni_Venda";
            this.uniVendaDataGridViewTextBoxColumn.Name = "uniVendaDataGridViewTextBoxColumn";
            this.uniVendaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // relacaoDataGridViewTextBoxColumn
            // 
            this.relacaoDataGridViewTextBoxColumn.DataPropertyName = "Relacao";
            this.relacaoDataGridViewTextBoxColumn.HeaderText = "Relacao";
            this.relacaoDataGridViewTextBoxColumn.Name = "relacaoDataGridViewTextBoxColumn";
            this.relacaoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // custoMedioDataGridViewTextBoxColumn
            // 
            this.custoMedioDataGridViewTextBoxColumn.DataPropertyName = "Custo_Medio";
            this.custoMedioDataGridViewTextBoxColumn.HeaderText = "Custo_Medio";
            this.custoMedioDataGridViewTextBoxColumn.Name = "custoMedioDataGridViewTextBoxColumn";
            this.custoMedioDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dtUltCompraDataGridViewTextBoxColumn
            // 
            this.dtUltCompraDataGridViewTextBoxColumn.DataPropertyName = "Dt_Ult_Compra";
            this.dtUltCompraDataGridViewTextBoxColumn.HeaderText = "Dt_Ult_Compra";
            this.dtUltCompraDataGridViewTextBoxColumn.Name = "dtUltCompraDataGridViewTextBoxColumn";
            this.dtUltCompraDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // vlUltCompraDataGridViewTextBoxColumn
            // 
            this.vlUltCompraDataGridViewTextBoxColumn.DataPropertyName = "Vl_Ult_Compra";
            this.vlUltCompraDataGridViewTextBoxColumn.HeaderText = "Vl_Ult_Compra";
            this.vlUltCompraDataGridViewTextBoxColumn.Name = "vlUltCompraDataGridViewTextBoxColumn";
            this.vlUltCompraDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // vlUltPrecoDataGridViewTextBoxColumn
            // 
            this.vlUltPrecoDataGridViewTextBoxColumn.DataPropertyName = "Vl_Ult_Preco";
            this.vlUltPrecoDataGridViewTextBoxColumn.HeaderText = "Vl_Ult_Preco";
            this.vlUltPrecoDataGridViewTextBoxColumn.Name = "vlUltPrecoDataGridViewTextBoxColumn";
            this.vlUltPrecoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // icmsRetDataGridViewTextBoxColumn
            // 
            this.icmsRetDataGridViewTextBoxColumn.DataPropertyName = "Icms_Ret";
            this.icmsRetDataGridViewTextBoxColumn.HeaderText = "Icms_Ret";
            this.icmsRetDataGridViewTextBoxColumn.Name = "icmsRetDataGridViewTextBoxColumn";
            this.icmsRetDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // claFiscalDataGridViewTextBoxColumn
            // 
            this.claFiscalDataGridViewTextBoxColumn.DataPropertyName = "Cla_Fiscal";
            this.claFiscalDataGridViewTextBoxColumn.HeaderText = "Cla_Fiscal";
            this.claFiscalDataGridViewTextBoxColumn.Name = "claFiscalDataGridViewTextBoxColumn";
            this.claFiscalDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // sitTributDataGridViewTextBoxColumn
            // 
            this.sitTributDataGridViewTextBoxColumn.DataPropertyName = "Sit_Tribut";
            this.sitTributDataGridViewTextBoxColumn.HeaderText = "Sit_Tribut";
            this.sitTributDataGridViewTextBoxColumn.Name = "sitTributDataGridViewTextBoxColumn";
            this.sitTributDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // paginaDataGridViewTextBoxColumn
            // 
            this.paginaDataGridViewTextBoxColumn.DataPropertyName = "Pagina";
            this.paginaDataGridViewTextBoxColumn.HeaderText = "Pagina";
            this.paginaDataGridViewTextBoxColumn.Name = "paginaDataGridViewTextBoxColumn";
            this.paginaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // codInternoDataGridViewTextBoxColumn
            // 
            this.codInternoDataGridViewTextBoxColumn.DataPropertyName = "Cod_Interno";
            this.codInternoDataGridViewTextBoxColumn.HeaderText = "Cod_Interno";
            this.codInternoDataGridViewTextBoxColumn.Name = "codInternoDataGridViewTextBoxColumn";
            this.codInternoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // pontoQuantDataGridViewTextBoxColumn
            // 
            this.pontoQuantDataGridViewTextBoxColumn.DataPropertyName = "Ponto_Quant";
            this.pontoQuantDataGridViewTextBoxColumn.HeaderText = "Ponto_Quant";
            this.pontoQuantDataGridViewTextBoxColumn.Name = "pontoQuantDataGridViewTextBoxColumn";
            this.pontoQuantDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // pontoValDataGridViewTextBoxColumn
            // 
            this.pontoValDataGridViewTextBoxColumn.DataPropertyName = "Ponto_Val";
            this.pontoValDataGridViewTextBoxColumn.HeaderText = "Ponto_Val";
            this.pontoValDataGridViewTextBoxColumn.Name = "pontoValDataGridViewTextBoxColumn";
            this.pontoValDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dtAlterDataGridViewTextBoxColumn
            // 
            this.dtAlterDataGridViewTextBoxColumn.DataPropertyName = "Dt_Alter";
            this.dtAlterDataGridViewTextBoxColumn.HeaderText = "Dt_Alter";
            this.dtAlterDataGridViewTextBoxColumn.Name = "dtAlterDataGridViewTextBoxColumn";
            this.dtAlterDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // odometroDataGridViewTextBoxColumn
            // 
            this.odometroDataGridViewTextBoxColumn.DataPropertyName = "Odometro";
            this.odometroDataGridViewTextBoxColumn.HeaderText = "Odometro";
            this.odometroDataGridViewTextBoxColumn.Name = "odometroDataGridViewTextBoxColumn";
            this.odometroDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // inativoDataGridViewTextBoxColumn
            // 
            this.inativoDataGridViewTextBoxColumn.DataPropertyName = "Inativo";
            this.inativoDataGridViewTextBoxColumn.HeaderText = "Inativo";
            this.inativoDataGridViewTextBoxColumn.Name = "inativoDataGridViewTextBoxColumn";
            this.inativoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // mP135DataGridViewTextBoxColumn
            // 
            this.mP135DataGridViewTextBoxColumn.DataPropertyName = "MP135";
            this.mP135DataGridViewTextBoxColumn.HeaderText = "MP135";
            this.mP135DataGridViewTextBoxColumn.Name = "mP135DataGridViewTextBoxColumn";
            this.mP135DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // sefazDataGridViewTextBoxColumn
            // 
            this.sefazDataGridViewTextBoxColumn.DataPropertyName = "Sefaz";
            this.sefazDataGridViewTextBoxColumn.HeaderText = "Sefaz";
            this.sefazDataGridViewTextBoxColumn.Name = "sefazDataGridViewTextBoxColumn";
            this.sefazDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // margemMaxDataGridViewTextBoxColumn
            // 
            this.margemMaxDataGridViewTextBoxColumn.DataPropertyName = "Margem_Max";
            this.margemMaxDataGridViewTextBoxColumn.HeaderText = "Margem_Max";
            this.margemMaxDataGridViewTextBoxColumn.Name = "margemMaxDataGridViewTextBoxColumn";
            this.margemMaxDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // codigoTCSDataGridViewTextBoxColumn
            // 
            this.codigoTCSDataGridViewTextBoxColumn.DataPropertyName = "Codigo_TCS";
            this.codigoTCSDataGridViewTextBoxColumn.HeaderText = "Codigo_TCS";
            this.codigoTCSDataGridViewTextBoxColumn.Name = "codigoTCSDataGridViewTextBoxColumn";
            this.codigoTCSDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // pesoDataGridViewTextBoxColumn
            // 
            this.pesoDataGridViewTextBoxColumn.DataPropertyName = "Peso";
            this.pesoDataGridViewTextBoxColumn.HeaderText = "Peso";
            this.pesoDataGridViewTextBoxColumn.Name = "pesoDataGridViewTextBoxColumn";
            this.pesoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // limiteDataGridViewTextBoxColumn
            // 
            this.limiteDataGridViewTextBoxColumn.DataPropertyName = "Limite";
            this.limiteDataGridViewTextBoxColumn.HeaderText = "Limite";
            this.limiteDataGridViewTextBoxColumn.Name = "limiteDataGridViewTextBoxColumn";
            this.limiteDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // grupoIFDataGridViewTextBoxColumn
            // 
            this.grupoIFDataGridViewTextBoxColumn.DataPropertyName = "GrupoIF";
            this.grupoIFDataGridViewTextBoxColumn.HeaderText = "GrupoIF";
            this.grupoIFDataGridViewTextBoxColumn.Name = "grupoIFDataGridViewTextBoxColumn";
            this.grupoIFDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // grupoTCSDataGridViewTextBoxColumn
            // 
            this.grupoTCSDataGridViewTextBoxColumn.DataPropertyName = "Grupo_TCS";
            this.grupoTCSDataGridViewTextBoxColumn.HeaderText = "Grupo_TCS";
            this.grupoTCSDataGridViewTextBoxColumn.Name = "grupoTCSDataGridViewTextBoxColumn";
            this.grupoTCSDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // volDataGridViewTextBoxColumn
            // 
            this.volDataGridViewTextBoxColumn.DataPropertyName = "Vol";
            this.volDataGridViewTextBoxColumn.HeaderText = "Vol";
            this.volDataGridViewTextBoxColumn.Name = "volDataGridViewTextBoxColumn";
            this.volDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // codigoPetroDataGridViewTextBoxColumn
            // 
            this.codigoPetroDataGridViewTextBoxColumn.DataPropertyName = "Codigo_Petro";
            this.codigoPetroDataGridViewTextBoxColumn.HeaderText = "Codigo_Petro";
            this.codigoPetroDataGridViewTextBoxColumn.Name = "codigoPetroDataGridViewTextBoxColumn";
            this.codigoPetroDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fotoDataGridViewTextBoxColumn
            // 
            this.fotoDataGridViewTextBoxColumn.DataPropertyName = "Foto";
            this.fotoDataGridViewTextBoxColumn.HeaderText = "Foto";
            this.fotoDataGridViewTextBoxColumn.Name = "fotoDataGridViewTextBoxColumn";
            this.fotoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cozinhaDataGridViewTextBoxColumn
            // 
            this.cozinhaDataGridViewTextBoxColumn.DataPropertyName = "Cozinha";
            this.cozinhaDataGridViewTextBoxColumn.HeaderText = "Cozinha";
            this.cozinhaDataGridViewTextBoxColumn.Name = "cozinhaDataGridViewTextBoxColumn";
            this.cozinhaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // codigoNCMDataGridViewTextBoxColumn
            // 
            this.codigoNCMDataGridViewTextBoxColumn.DataPropertyName = "Codigo_NCM";
            this.codigoNCMDataGridViewTextBoxColumn.HeaderText = "Codigo_NCM";
            this.codigoNCMDataGridViewTextBoxColumn.Name = "codigoNCMDataGridViewTextBoxColumn";
            this.codigoNCMDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // codigoANPDataGridViewTextBoxColumn
            // 
            this.codigoANPDataGridViewTextBoxColumn.DataPropertyName = "Codigo_ANP";
            this.codigoANPDataGridViewTextBoxColumn.HeaderText = "Codigo_ANP";
            this.codigoANPDataGridViewTextBoxColumn.Name = "codigoANPDataGridViewTextBoxColumn";
            this.codigoANPDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // codigoValeDataGridViewTextBoxColumn
            // 
            this.codigoValeDataGridViewTextBoxColumn.DataPropertyName = "Codigo_Vale";
            this.codigoValeDataGridViewTextBoxColumn.HeaderText = "Codigo_Vale";
            this.codigoValeDataGridViewTextBoxColumn.Name = "codigoValeDataGridViewTextBoxColumn";
            this.codigoValeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // bloquearAcimaDataGridViewTextBoxColumn
            // 
            this.bloquearAcimaDataGridViewTextBoxColumn.DataPropertyName = "Bloquear_Acima";
            this.bloquearAcimaDataGridViewTextBoxColumn.HeaderText = "Bloquear_Acima";
            this.bloquearAcimaDataGridViewTextBoxColumn.Name = "bloquearAcimaDataGridViewTextBoxColumn";
            this.bloquearAcimaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // bloquearFracionarioDataGridViewTextBoxColumn
            // 
            this.bloquearFracionarioDataGridViewTextBoxColumn.DataPropertyName = "Bloquear_Fracionario";
            this.bloquearFracionarioDataGridViewTextBoxColumn.HeaderText = "Bloquear_Fracionario";
            this.bloquearFracionarioDataGridViewTextBoxColumn.Name = "bloquearFracionarioDataGridViewTextBoxColumn";
            this.bloquearFracionarioDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // codigoFidelizeDataGridViewTextBoxColumn
            // 
            this.codigoFidelizeDataGridViewTextBoxColumn.DataPropertyName = "Codigo_Fidelize";
            this.codigoFidelizeDataGridViewTextBoxColumn.HeaderText = "Codigo_Fidelize";
            this.codigoFidelizeDataGridViewTextBoxColumn.Name = "codigoFidelizeDataGridViewTextBoxColumn";
            this.codigoFidelizeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // sTPISDataGridViewTextBoxColumn
            // 
            this.sTPISDataGridViewTextBoxColumn.DataPropertyName = "ST_PIS";
            this.sTPISDataGridViewTextBoxColumn.HeaderText = "ST_PIS";
            this.sTPISDataGridViewTextBoxColumn.Name = "sTPISDataGridViewTextBoxColumn";
            this.sTPISDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // sTCofinsDataGridViewTextBoxColumn
            // 
            this.sTCofinsDataGridViewTextBoxColumn.DataPropertyName = "ST_Cofins";
            this.sTCofinsDataGridViewTextBoxColumn.HeaderText = "ST_Cofins";
            this.sTCofinsDataGridViewTextBoxColumn.Name = "sTCofinsDataGridViewTextBoxColumn";
            this.sTCofinsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // aliquotaPISDataGridViewTextBoxColumn
            // 
            this.aliquotaPISDataGridViewTextBoxColumn.DataPropertyName = "Aliquota_PIS";
            this.aliquotaPISDataGridViewTextBoxColumn.HeaderText = "Aliquota_PIS";
            this.aliquotaPISDataGridViewTextBoxColumn.Name = "aliquotaPISDataGridViewTextBoxColumn";
            this.aliquotaPISDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // aliquotaCofinsDataGridViewTextBoxColumn
            // 
            this.aliquotaCofinsDataGridViewTextBoxColumn.DataPropertyName = "Aliquota_Cofins";
            this.aliquotaCofinsDataGridViewTextBoxColumn.HeaderText = "Aliquota_Cofins";
            this.aliquotaCofinsDataGridViewTextBoxColumn.Name = "aliquotaCofinsDataGridViewTextBoxColumn";
            this.aliquotaCofinsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // codigoEXCardDataGridViewTextBoxColumn
            // 
            this.codigoEXCardDataGridViewTextBoxColumn.DataPropertyName = "Codigo_EXCard";
            this.codigoEXCardDataGridViewTextBoxColumn.HeaderText = "Codigo_EXCard";
            this.codigoEXCardDataGridViewTextBoxColumn.Name = "codigoEXCardDataGridViewTextBoxColumn";
            this.codigoEXCardDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // natOperDataGridViewTextBoxColumn
            // 
            this.natOperDataGridViewTextBoxColumn.DataPropertyName = "Nat_Oper";
            this.natOperDataGridViewTextBoxColumn.HeaderText = "Nat_Oper";
            this.natOperDataGridViewTextBoxColumn.Name = "natOperDataGridViewTextBoxColumn";
            this.natOperDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // panel4ConsultarBottom
            // 
            this.panel4ConsultarBottom.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel4ConsultarBottom.BackColor = System.Drawing.Color.ForestGreen;
            this.panel4ConsultarBottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4ConsultarBottom.Location = new System.Drawing.Point(3, 378);
            this.panel4ConsultarBottom.Name = "panel4ConsultarBottom";
            this.panel4ConsultarBottom.Size = new System.Drawing.Size(953, 38);
            this.panel4ConsultarBottom.TabIndex = 23;
            // 
            // flowLayoutPanelEditar
            // 
            this.flowLayoutPanelEditar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.flowLayoutPanelEditar.Controls.Add(this.pictureBox2);
            this.flowLayoutPanelEditar.Controls.Add(this.headLabelEditar);
            this.flowLayoutPanelEditar.Location = new System.Drawing.Point(318, 4);
            this.flowLayoutPanelEditar.Name = "flowLayoutPanelEditar";
            this.flowLayoutPanelEditar.Size = new System.Drawing.Size(342, 53);
            this.flowLayoutPanelEditar.TabIndex = 19;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::osExpress.Properties.Resources.edit2;
            this.pictureBox2.Location = new System.Drawing.Point(3, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(51, 50);
            this.pictureBox2.TabIndex = 17;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // headLabelEditar
            // 
            this.headLabelEditar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.headLabelEditar.AutoSize = true;
            this.headLabelEditar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.headLabelEditar.Location = new System.Drawing.Point(60, 18);
            this.headLabelEditar.Name = "headLabelEditar";
            this.headLabelEditar.Size = new System.Drawing.Size(249, 20);
            this.headLabelEditar.TabIndex = 0;
            this.headLabelEditar.Text = "Manutenção Produtos - Editar";
            // 
            // tableLayoutPanelEditar
            // 
            this.tableLayoutPanelEditar.BackColor = System.Drawing.Color.BurlyWood;
            this.tableLayoutPanelEditar.ColumnCount = 1;
            this.tableLayoutPanelEditar.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelEditar.Controls.Add(this.flowLayoutPanelEditar, 0, 0);
            this.tableLayoutPanelEditar.Controls.Add(this.panel2Editar, 0, 1);
            this.tableLayoutPanelEditar.Controls.Add(this.tabControlEditar, 0, 2);
            this.tableLayoutPanelEditar.Controls.Add(this.panel4Editar, 0, 3);
            this.tableLayoutPanelEditar.Location = new System.Drawing.Point(105, 90);
            this.tableLayoutPanelEditar.Name = "tableLayoutPanelEditar";
            this.tableLayoutPanelEditar.RowCount = 4;
            this.tableLayoutPanelEditar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanelEditar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanelEditar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanelEditar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanelEditar.Size = new System.Drawing.Size(978, 619);
            this.tableLayoutPanelEditar.TabIndex = 16;
            // 
            // panel2Editar
            // 
            this.panel2Editar.BackColor = System.Drawing.Color.ForestGreen;
            this.panel2Editar.Controls.Add(this.lblTrackBar);
            this.panel2Editar.Controls.Add(this.tbTrackValue);
            this.panel2Editar.Controls.Add(this.trackBar1);
            this.panel2Editar.Controls.Add(this.btnResizeColWidth);
            this.panel2Editar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2Editar.Location = new System.Drawing.Point(3, 64);
            this.panel2Editar.Name = "panel2Editar";
            this.panel2Editar.Size = new System.Drawing.Size(972, 55);
            this.panel2Editar.TabIndex = 20;
            // 
            // lblTrackBar
            // 
            this.lblTrackBar.AutoSize = true;
            this.lblTrackBar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrackBar.Location = new System.Drawing.Point(8, 5);
            this.lblTrackBar.Name = "lblTrackBar";
            this.lblTrackBar.Size = new System.Drawing.Size(144, 13);
            this.lblTrackBar.TabIndex = 3;
            this.lblTrackBar.Text = "ajuste da espaciosidade";
            // 
            // tbTrackValue
            // 
            this.tbTrackValue.Location = new System.Drawing.Point(329, 5);
            this.tbTrackValue.Name = "tbTrackValue";
            this.tbTrackValue.ReadOnly = true;
            this.tbTrackValue.Size = new System.Drawing.Size(40, 20);
            this.tbTrackValue.TabIndex = 2;
            this.tbTrackValue.Text = "0";
            // 
            // trackBar1
            // 
            this.trackBar1.Location = new System.Drawing.Point(158, 0);
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(104, 45);
            this.trackBar1.TabIndex = 1;
            this.trackBar1.TickFrequency = 10;
            this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            this.trackBar1.ValueChanged += new System.EventHandler(this.trackBar1_ValueChanged);
            // 
            // btnResizeColWidth
            // 
            this.btnResizeColWidth.Location = new System.Drawing.Point(683, -1);
            this.btnResizeColWidth.Name = "btnResizeColWidth";
            this.btnResizeColWidth.Size = new System.Drawing.Size(75, 23);
            this.btnResizeColWidth.TabIndex = 0;
            this.btnResizeColWidth.Text = "button1";
            this.btnResizeColWidth.UseVisualStyleBackColor = true;
            this.btnResizeColWidth.Visible = false;
            this.btnResizeColWidth.Click += new System.EventHandler(this.btnResizeColWidth_Click);
            // 
            // tabControlEditar
            // 
            this.tabControlEditar.Controls.Add(this.tabPage1Dados);
            this.tabControlEditar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlEditar.Location = new System.Drawing.Point(3, 125);
            this.tabControlEditar.Name = "tabControlEditar";
            this.tabControlEditar.SelectedIndex = 0;
            this.tabControlEditar.Size = new System.Drawing.Size(972, 427);
            this.tabControlEditar.TabIndex = 21;
            this.tabControlEditar.TabStop = false;
            // 
            // tabPage1Dados
            // 
            this.tabPage1Dados.Controls.Add(this.tableLayoutPanelDados);
            this.tabPage1Dados.Location = new System.Drawing.Point(4, 22);
            this.tabPage1Dados.Name = "tabPage1Dados";
            this.tabPage1Dados.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1Dados.Size = new System.Drawing.Size(964, 401);
            this.tabPage1Dados.TabIndex = 0;
            this.tabPage1Dados.Text = "Dados";
            this.tabPage1Dados.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanelDados
            // 
            this.tableLayoutPanelDados.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanelDados.ColumnCount = 20;
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.Controls.Add(this.tableLayoutPanelTipoProduto, 1, 0);
            this.tableLayoutPanelDados.Controls.Add(this.tableLayoutPanelAtivo, 5, 0);
            this.tableLayoutPanelDados.Controls.Add(this.tableLayoutPanelRow2Col1_2, 1, 1);
            this.tableLayoutPanelDados.Controls.Add(this.tableLayoutPanelNome, 3, 1);
            this.tableLayoutPanelDados.Controls.Add(this.tableLayoutPanelGrupo, 1, 2);
            this.tableLayoutPanelDados.Controls.Add(this.tableLayoutPanelUnidade, 6, 2);
            this.tableLayoutPanelDados.Controls.Add(this.tableLayoutPanelComissao, 9, 2);
            this.tableLayoutPanelDados.Controls.Add(this.tableLayoutPanelComissaoValor, 12, 2);
            this.tableLayoutPanelDados.Controls.Add(this.tableLayoutPanelPreco, 15, 2);
            this.tableLayoutPanelDados.Controls.Add(this.panelFillerRow1, 0, 0);
            this.tableLayoutPanelDados.Controls.Add(this.panelFillRow1ColLast, 19, 0);
            this.tableLayoutPanelDados.Controls.Add(this.tableLayoutPanelManutensao, 12, 1);
            this.tableLayoutPanelDados.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelDados.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanelDados.Name = "tableLayoutPanelDados";
            this.tableLayoutPanelDados.RowCount = 4;
            this.tableLayoutPanelDados.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelDados.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelDados.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelDados.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelDados.Size = new System.Drawing.Size(958, 395);
            this.tableLayoutPanelDados.TabIndex = 0;
            // 
            // tableLayoutPanelTipoProduto
            // 
            this.tableLayoutPanelTipoProduto.BackColor = System.Drawing.Color.LightGray;
            this.tableLayoutPanelTipoProduto.ColumnCount = 1;
            this.tableLayoutPanelDados.SetColumnSpan(this.tableLayoutPanelTipoProduto, 3);
            this.tableLayoutPanelTipoProduto.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelTipoProduto.Controls.Add(this.labelTipoProduto, 0, 0);
            this.tableLayoutPanelTipoProduto.Controls.Add(this.cmbTipoProduto, 0, 1);
            this.tableLayoutPanelTipoProduto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelTipoProduto.Location = new System.Drawing.Point(51, 4);
            this.tableLayoutPanelTipoProduto.Name = "tableLayoutPanelTipoProduto";
            this.tableLayoutPanelTipoProduto.RowCount = 2;
            this.tableLayoutPanelTipoProduto.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelTipoProduto.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanelTipoProduto.Size = new System.Drawing.Size(134, 91);
            this.tableLayoutPanelTipoProduto.TabIndex = 0;
            // 
            // labelTipoProduto
            // 
            this.labelTipoProduto.AutoSize = true;
            this.labelTipoProduto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelTipoProduto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTipoProduto.Location = new System.Drawing.Point(3, 0);
            this.labelTipoProduto.Name = "labelTipoProduto";
            this.labelTipoProduto.Size = new System.Drawing.Size(128, 18);
            this.labelTipoProduto.TabIndex = 0;
            this.labelTipoProduto.Text = "Tipo de Produto";
            // 
            // cmbTipoProduto
            // 
            this.cmbTipoProduto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbTipoProduto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTipoProduto.FormattingEnabled = true;
            this.cmbTipoProduto.Location = new System.Drawing.Point(3, 21);
            this.cmbTipoProduto.Name = "cmbTipoProduto";
            this.cmbTipoProduto.Size = new System.Drawing.Size(128, 21);
            this.cmbTipoProduto.TabIndex = 0;
            this.cmbTipoProduto.SelectionChangeCommitted += new System.EventHandler(this.cmbTipoProduto_SelectionChangeCommitted);
            this.cmbTipoProduto.Enter += new System.EventHandler(this.cmb_DropMeDown);
            this.cmbTipoProduto.KeyDown += new System.Windows.Forms.KeyEventHandler(this.enter4tab_KeyDown);
            this.cmbTipoProduto.Leave += new System.EventHandler(this.cmbTipoProduto_Leave);
            // 
            // tableLayoutPanelAtivo
            // 
            this.tableLayoutPanelAtivo.BackColor = System.Drawing.Color.Silver;
            this.tableLayoutPanelAtivo.ColumnCount = 1;
            this.tableLayoutPanelDados.SetColumnSpan(this.tableLayoutPanelAtivo, 2);
            this.tableLayoutPanelAtivo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelAtivo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelAtivo.Controls.Add(this.labelDummyAtivo, 0, 0);
            this.tableLayoutPanelAtivo.Controls.Add(this.cbAtivo, 0, 1);
            this.tableLayoutPanelAtivo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelAtivo.Location = new System.Drawing.Point(239, 4);
            this.tableLayoutPanelAtivo.Name = "tableLayoutPanelAtivo";
            this.tableLayoutPanelAtivo.RowCount = 2;
            this.tableLayoutPanelAtivo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelAtivo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanelAtivo.Size = new System.Drawing.Size(87, 91);
            this.tableLayoutPanelAtivo.TabIndex = 1;
            // 
            // labelDummyAtivo
            // 
            this.labelDummyAtivo.AutoSize = true;
            this.labelDummyAtivo.Location = new System.Drawing.Point(3, 0);
            this.labelDummyAtivo.Name = "labelDummyAtivo";
            this.labelDummyAtivo.Size = new System.Drawing.Size(48, 13);
            this.labelDummyAtivo.TabIndex = 0;
            this.labelDummyAtivo.Text = "DUMMY";
            // 
            // cbAtivo
            // 
            this.cbAtivo.AutoSize = true;
            this.cbAtivo.Checked = true;
            this.cbAtivo.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAtivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbAtivo.Location = new System.Drawing.Point(3, 21);
            this.cbAtivo.Name = "cbAtivo";
            this.cbAtivo.Size = new System.Drawing.Size(61, 17);
            this.cbAtivo.TabIndex = 1;
            this.cbAtivo.Text = "ativo?";
            this.cbAtivo.UseVisualStyleBackColor = true;
            this.cbAtivo.Enter += new System.EventHandler(this.cbAtivo_Enter);
            this.cbAtivo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.enter4tab_KeyDown);
            // 
            // tableLayoutPanelRow2Col1_2
            // 
            this.tableLayoutPanelRow2Col1_2.BackColor = System.Drawing.Color.LightGray;
            this.tableLayoutPanelRow2Col1_2.ColumnCount = 1;
            this.tableLayoutPanelDados.SetColumnSpan(this.tableLayoutPanelRow2Col1_2, 2);
            this.tableLayoutPanelRow2Col1_2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelRow2Col1_2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelRow2Col1_2.Controls.Add(this.labelCodigo, 0, 0);
            this.tableLayoutPanelRow2Col1_2.Controls.Add(this.tbCodigo, 0, 1);
            this.tableLayoutPanelRow2Col1_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelRow2Col1_2.Location = new System.Drawing.Point(51, 102);
            this.tableLayoutPanelRow2Col1_2.Name = "tableLayoutPanelRow2Col1_2";
            this.tableLayoutPanelRow2Col1_2.RowCount = 2;
            this.tableLayoutPanelRow2Col1_2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelRow2Col1_2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanelRow2Col1_2.Size = new System.Drawing.Size(87, 91);
            this.tableLayoutPanelRow2Col1_2.TabIndex = 2;
            // 
            // labelCodigo
            // 
            this.labelCodigo.AutoSize = true;
            this.labelCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCodigo.Location = new System.Drawing.Point(3, 0);
            this.labelCodigo.Name = "labelCodigo";
            this.labelCodigo.Size = new System.Drawing.Size(46, 13);
            this.labelCodigo.TabIndex = 0;
            this.labelCodigo.Text = "Código";
            // 
            // tbCodigo
            // 
            this.tbCodigo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbCodigo.Enabled = false;
            this.tbCodigo.Location = new System.Drawing.Point(3, 21);
            this.tbCodigo.Name = "tbCodigo";
            this.tbCodigo.ReadOnly = true;
            this.tbCodigo.Size = new System.Drawing.Size(81, 20);
            this.tbCodigo.TabIndex = 1;
            this.tbCodigo.TabStop = false;
            this.tbCodigo.Text = "999999";
            // 
            // tableLayoutPanelNome
            // 
            this.tableLayoutPanelNome.BackColor = System.Drawing.Color.Silver;
            this.tableLayoutPanelNome.ColumnCount = 1;
            this.tableLayoutPanelDados.SetColumnSpan(this.tableLayoutPanelNome, 9);
            this.tableLayoutPanelNome.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelNome.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelNome.Controls.Add(this.labelNome, 0, 0);
            this.tableLayoutPanelNome.Controls.Add(this.tbNome, 0, 1);
            this.tableLayoutPanelNome.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelNome.Location = new System.Drawing.Point(145, 102);
            this.tableLayoutPanelNome.Name = "tableLayoutPanelNome";
            this.tableLayoutPanelNome.RowCount = 2;
            this.tableLayoutPanelNome.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelNome.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanelNome.Size = new System.Drawing.Size(416, 91);
            this.tableLayoutPanelNome.TabIndex = 2;
            // 
            // labelNome
            // 
            this.labelNome.AutoSize = true;
            this.labelNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNome.Location = new System.Drawing.Point(3, 0);
            this.labelNome.Name = "labelNome";
            this.labelNome.Size = new System.Drawing.Size(39, 13);
            this.labelNome.TabIndex = 0;
            this.labelNome.Text = "Nome";
            // 
            // tbNome
            // 
            this.tbNome.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbNome.Location = new System.Drawing.Point(3, 21);
            this.tbNome.Name = "tbNome";
            this.tbNome.Size = new System.Drawing.Size(410, 20);
            this.tbNome.TabIndex = 2;
            this.tbNome.Enter += new System.EventHandler(this.tbNome_Enter);
            this.tbNome.KeyDown += new System.Windows.Forms.KeyEventHandler(this.enter4tab_KeyDown);
            this.tbNome.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tbNome_MouseUp);
            // 
            // tableLayoutPanelGrupo
            // 
            this.tableLayoutPanelGrupo.BackColor = System.Drawing.Color.LightGray;
            this.tableLayoutPanelGrupo.ColumnCount = 1;
            this.tableLayoutPanelDados.SetColumnSpan(this.tableLayoutPanelGrupo, 5);
            this.tableLayoutPanelGrupo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelGrupo.Controls.Add(this.labelGrupo, 0, 0);
            this.tableLayoutPanelGrupo.Controls.Add(this.cmbGrupo, 0, 1);
            this.tableLayoutPanelGrupo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelGrupo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanelGrupo.Location = new System.Drawing.Point(51, 200);
            this.tableLayoutPanelGrupo.Name = "tableLayoutPanelGrupo";
            this.tableLayoutPanelGrupo.RowCount = 2;
            this.tableLayoutPanelGrupo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelGrupo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanelGrupo.Size = new System.Drawing.Size(228, 91);
            this.tableLayoutPanelGrupo.TabIndex = 4;
            // 
            // labelGrupo
            // 
            this.labelGrupo.AutoSize = true;
            this.labelGrupo.Location = new System.Drawing.Point(3, 0);
            this.labelGrupo.Name = "labelGrupo";
            this.labelGrupo.Size = new System.Drawing.Size(41, 13);
            this.labelGrupo.TabIndex = 0;
            this.labelGrupo.Text = "Grupo";
            // 
            // cmbGrupo
            // 
            this.cmbGrupo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbGrupo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGrupo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbGrupo.FormattingEnabled = true;
            this.cmbGrupo.Location = new System.Drawing.Point(3, 21);
            this.cmbGrupo.Name = "cmbGrupo";
            this.cmbGrupo.Size = new System.Drawing.Size(222, 21);
            this.cmbGrupo.TabIndex = 4;
            this.cmbGrupo.Enter += new System.EventHandler(this.cmb_DropMeDown);
            this.cmbGrupo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.enter4tab_KeyDown);
            // 
            // tableLayoutPanelUnidade
            // 
            this.tableLayoutPanelUnidade.BackColor = System.Drawing.Color.Silver;
            this.tableLayoutPanelUnidade.ColumnCount = 1;
            this.tableLayoutPanelDados.SetColumnSpan(this.tableLayoutPanelUnidade, 3);
            this.tableLayoutPanelUnidade.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelUnidade.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelUnidade.Controls.Add(this.labelUnidade, 0, 0);
            this.tableLayoutPanelUnidade.Controls.Add(this.cmbUnidade, 0, 1);
            this.tableLayoutPanelUnidade.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelUnidade.Location = new System.Drawing.Point(286, 200);
            this.tableLayoutPanelUnidade.Name = "tableLayoutPanelUnidade";
            this.tableLayoutPanelUnidade.RowCount = 2;
            this.tableLayoutPanelUnidade.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelUnidade.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanelUnidade.Size = new System.Drawing.Size(134, 91);
            this.tableLayoutPanelUnidade.TabIndex = 5;
            // 
            // labelUnidade
            // 
            this.labelUnidade.AutoSize = true;
            this.labelUnidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUnidade.Location = new System.Drawing.Point(3, 0);
            this.labelUnidade.Name = "labelUnidade";
            this.labelUnidade.Size = new System.Drawing.Size(97, 13);
            this.labelUnidade.TabIndex = 0;
            this.labelUnidade.Text = "Unidade_Venda";
            // 
            // cmbUnidade
            // 
            this.cmbUnidade.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbUnidade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUnidade.FormattingEnabled = true;
            this.cmbUnidade.Location = new System.Drawing.Point(3, 21);
            this.cmbUnidade.Name = "cmbUnidade";
            this.cmbUnidade.Size = new System.Drawing.Size(128, 21);
            this.cmbUnidade.TabIndex = 5;
            this.cmbUnidade.Enter += new System.EventHandler(this.cmb_DropMeDown);
            this.cmbUnidade.KeyDown += new System.Windows.Forms.KeyEventHandler(this.enter4tab_KeyDown);
            // 
            // tableLayoutPanelComissao
            // 
            this.tableLayoutPanelComissao.BackColor = System.Drawing.Color.LightGray;
            this.tableLayoutPanelComissao.ColumnCount = 1;
            this.tableLayoutPanelDados.SetColumnSpan(this.tableLayoutPanelComissao, 3);
            this.tableLayoutPanelComissao.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelComissao.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelComissao.Controls.Add(this.labelComissao, 0, 0);
            this.tableLayoutPanelComissao.Controls.Add(this.tbComissao, 0, 1);
            this.tableLayoutPanelComissao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelComissao.Location = new System.Drawing.Point(427, 200);
            this.tableLayoutPanelComissao.Name = "tableLayoutPanelComissao";
            this.tableLayoutPanelComissao.RowCount = 2;
            this.tableLayoutPanelComissao.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelComissao.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanelComissao.Size = new System.Drawing.Size(134, 91);
            this.tableLayoutPanelComissao.TabIndex = 6;
            // 
            // labelComissao
            // 
            this.labelComissao.AutoSize = true;
            this.labelComissao.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelComissao.Location = new System.Drawing.Point(3, 0);
            this.labelComissao.Name = "labelComissao";
            this.labelComissao.Size = new System.Drawing.Size(81, 13);
            this.labelComissao.TabIndex = 0;
            this.labelComissao.Text = "Comissão (%)";
            // 
            // tbComissao
            // 
            this.tbComissao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbComissao.Location = new System.Drawing.Point(3, 21);
            this.tbComissao.Name = "tbComissao";
            this.tbComissao.Size = new System.Drawing.Size(128, 20);
            this.tbComissao.TabIndex = 1;
            this.tbComissao.Enter += new System.EventHandler(this.tbComissao_Enter);
            this.tbComissao.KeyDown += new System.Windows.Forms.KeyEventHandler(this.enter4tab_KeyDown);
            this.tbComissao.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_OnlyDigits_KeyPress);
            this.tbComissao.Leave += new System.EventHandler(this.tbComissao_Leave);
            this.tbComissao.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tbComissao_MouseUp);
            // 
            // tableLayoutPanelComissaoValor
            // 
            this.tableLayoutPanelComissaoValor.BackColor = System.Drawing.Color.Silver;
            this.tableLayoutPanelComissaoValor.ColumnCount = 1;
            this.tableLayoutPanelDados.SetColumnSpan(this.tableLayoutPanelComissaoValor, 3);
            this.tableLayoutPanelComissaoValor.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelComissaoValor.Controls.Add(this.labelComissaoValor, 0, 0);
            this.tableLayoutPanelComissaoValor.Controls.Add(this.tbComissaoValor, 0, 1);
            this.tableLayoutPanelComissaoValor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelComissaoValor.Location = new System.Drawing.Point(568, 200);
            this.tableLayoutPanelComissaoValor.Name = "tableLayoutPanelComissaoValor";
            this.tableLayoutPanelComissaoValor.RowCount = 2;
            this.tableLayoutPanelComissaoValor.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelComissaoValor.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanelComissaoValor.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelComissaoValor.Size = new System.Drawing.Size(134, 91);
            this.tableLayoutPanelComissaoValor.TabIndex = 7;
            // 
            // labelComissaoValor
            // 
            this.labelComissaoValor.AutoSize = true;
            this.labelComissaoValor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelComissaoValor.Location = new System.Drawing.Point(3, 0);
            this.labelComissaoValor.Name = "labelComissaoValor";
            this.labelComissaoValor.Size = new System.Drawing.Size(88, 13);
            this.labelComissaoValor.TabIndex = 0;
            this.labelComissaoValor.Text = "Comissão (R$)";
            // 
            // tbComissaoValor
            // 
            this.tbComissaoValor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbComissaoValor.Location = new System.Drawing.Point(3, 21);
            this.tbComissaoValor.Name = "tbComissaoValor";
            this.tbComissaoValor.Size = new System.Drawing.Size(128, 20);
            this.tbComissaoValor.TabIndex = 1;
            this.tbComissaoValor.Enter += new System.EventHandler(this.tbComissaoValor_Enter);
            this.tbComissaoValor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.enter4tab_KeyDown);
            this.tbComissaoValor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_OnlyDigits_KeyPress);
            this.tbComissaoValor.Leave += new System.EventHandler(this.tbComissaoValor_Leave);
            this.tbComissaoValor.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tbComissaoValor_MouseUp);
            // 
            // tableLayoutPanelPreco
            // 
            this.tableLayoutPanelPreco.BackColor = System.Drawing.Color.LightGray;
            this.tableLayoutPanelPreco.ColumnCount = 1;
            this.tableLayoutPanelDados.SetColumnSpan(this.tableLayoutPanelPreco, 3);
            this.tableLayoutPanelPreco.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelPreco.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelPreco.Controls.Add(this.labelPreco, 0, 0);
            this.tableLayoutPanelPreco.Controls.Add(this.tbPreco, 0, 1);
            this.tableLayoutPanelPreco.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelPreco.Location = new System.Drawing.Point(709, 200);
            this.tableLayoutPanelPreco.Name = "tableLayoutPanelPreco";
            this.tableLayoutPanelPreco.RowCount = 2;
            this.tableLayoutPanelPreco.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelPreco.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanelPreco.Size = new System.Drawing.Size(134, 91);
            this.tableLayoutPanelPreco.TabIndex = 8;
            // 
            // labelPreco
            // 
            this.labelPreco.AutoSize = true;
            this.labelPreco.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPreco.Location = new System.Drawing.Point(3, 0);
            this.labelPreco.Name = "labelPreco";
            this.labelPreco.Size = new System.Drawing.Size(68, 13);
            this.labelPreco.TabIndex = 0;
            this.labelPreco.Text = "Preço (R$)";
            // 
            // tbPreco
            // 
            this.tbPreco.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbPreco.Location = new System.Drawing.Point(3, 21);
            this.tbPreco.Name = "tbPreco";
            this.tbPreco.Size = new System.Drawing.Size(128, 20);
            this.tbPreco.TabIndex = 1;
            this.tbPreco.Enter += new System.EventHandler(this.tbPreco_Enter);
            this.tbPreco.KeyDown += new System.Windows.Forms.KeyEventHandler(this.enter4tab_KeyDown);
            this.tbPreco.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_OnlyDigits_KeyPress);
            this.tbPreco.Leave += new System.EventHandler(this.tbPreco_Leave);
            this.tbPreco.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tbPreco_MouseUp);
            // 
            // panelFillerRow1
            // 
            this.panelFillerRow1.BackColor = System.Drawing.Color.DarkRed;
            this.panelFillerRow1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelFillerRow1.Location = new System.Drawing.Point(4, 4);
            this.panelFillerRow1.Name = "panelFillerRow1";
            this.tableLayoutPanelDados.SetRowSpan(this.panelFillerRow1, 3);
            this.panelFillerRow1.Size = new System.Drawing.Size(40, 287);
            this.panelFillerRow1.TabIndex = 9;
            // 
            // panelFillRow1ColLast
            // 
            this.panelFillRow1ColLast.BackColor = System.Drawing.Color.DarkRed;
            this.panelFillRow1ColLast.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelFillRow1ColLast.Location = new System.Drawing.Point(897, 4);
            this.panelFillRow1ColLast.Name = "panelFillRow1ColLast";
            this.tableLayoutPanelDados.SetRowSpan(this.panelFillRow1ColLast, 3);
            this.panelFillRow1ColLast.Size = new System.Drawing.Size(57, 287);
            this.panelFillRow1ColLast.TabIndex = 10;
            // 
            // tableLayoutPanelManutensao
            // 
            this.tableLayoutPanelManutensao.BackColor = System.Drawing.Color.Silver;
            this.tableLayoutPanelManutensao.ColumnCount = 1;
            this.tableLayoutPanelDados.SetColumnSpan(this.tableLayoutPanelManutensao, 3);
            this.tableLayoutPanelManutensao.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelManutensao.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelManutensao.Controls.Add(this.labelManutencao, 0, 0);
            this.tableLayoutPanelManutensao.Controls.Add(this.tbManutencao, 0, 1);
            this.tableLayoutPanelManutensao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelManutensao.Location = new System.Drawing.Point(568, 102);
            this.tableLayoutPanelManutensao.Name = "tableLayoutPanelManutensao";
            this.tableLayoutPanelManutensao.RowCount = 2;
            this.tableLayoutPanelManutensao.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelManutensao.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanelManutensao.Size = new System.Drawing.Size(134, 91);
            this.tableLayoutPanelManutensao.TabIndex = 3;
            // 
            // labelManutencao
            // 
            this.labelManutencao.AutoSize = true;
            this.labelManutencao.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelManutencao.Location = new System.Drawing.Point(3, 0);
            this.labelManutencao.Name = "labelManutencao";
            this.labelManutencao.Size = new System.Drawing.Size(105, 13);
            this.labelManutencao.TabIndex = 0;
            this.labelManutencao.Text = "Manutenção (R$)";
            // 
            // tbManutencao
            // 
            this.tbManutencao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbManutencao.Location = new System.Drawing.Point(3, 21);
            this.tbManutencao.Name = "tbManutencao";
            this.tbManutencao.ShortcutsEnabled = false;
            this.tbManutencao.Size = new System.Drawing.Size(128, 20);
            this.tbManutencao.TabIndex = 1;
            this.tbManutencao.Text = " ";
            this.tbManutencao.Enter += new System.EventHandler(this.tbManutencao_Enter);
            this.tbManutencao.KeyDown += new System.Windows.Forms.KeyEventHandler(this.enter4tab_KeyDown);
            this.tbManutencao.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_OnlyDigits_KeyPress);
            this.tbManutencao.Leave += new System.EventHandler(this.tbManutencao_Leave);
            this.tbManutencao.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tbManutencao_MouseUp);
            // 
            // panel4Editar
            // 
            this.panel4Editar.BackColor = System.Drawing.Color.ForestGreen;
            this.panel4Editar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4Editar.Location = new System.Drawing.Point(3, 558);
            this.panel4Editar.Name = "panel4Editar";
            this.panel4Editar.Size = new System.Drawing.Size(972, 58);
            this.panel4Editar.TabIndex = 22;
            // 
            // produtoDataGridViewTextBoxColumn
            // 
            this.produtoDataGridViewTextBoxColumn.DataPropertyName = "Produto";
            this.produtoDataGridViewTextBoxColumn.HeaderText = "Produto";
            this.produtoDataGridViewTextBoxColumn.Name = "produtoDataGridViewTextBoxColumn";
            this.produtoDataGridViewTextBoxColumn.Width = 151;
            // 
            // descricaoDataGridViewTextBoxColumn
            // 
            this.descricaoDataGridViewTextBoxColumn.DataPropertyName = "Descricao";
            this.descricaoDataGridViewTextBoxColumn.HeaderText = "Descricao";
            this.descricaoDataGridViewTextBoxColumn.Name = "descricaoDataGridViewTextBoxColumn";
            this.descricaoDataGridViewTextBoxColumn.Width = 152;
            // 
            // ManProduto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(959, 511);
            this.Controls.Add(this.tableLayoutPanelEditar);
            this.Controls.Add(this.tableLayoutPanelConsultar);
            this.Controls.Add(this.ProdutoBindingNavigator);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.MaximizeBox = true;
            this.MinimizeBox = true;
            this.Name = "ManProduto";
            this.Text = "    ";
            this.Load += new System.EventHandler(this.ManProduto_Load);
            this.Controls.SetChildIndex(this.ProdutoBindingNavigator, 0);
            this.Controls.SetChildIndex(this.tableLayoutPanelConsultar, 0);
            this.Controls.SetChildIndex(this.tableLayoutPanelEditar, 0);
            this.Controls.SetChildIndex(this.grpPrincipal, 0);
            this.Controls.SetChildIndex(this.tabPrincipal, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dtsPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProdutoBindingNavigator)).EndInit();
            this.ProdutoBindingNavigator.ResumeLayout(false);
            this.ProdutoBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OsProdutoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtsA30Produto)).EndInit();
            this.tableLayoutPanelConsultar.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutFilter.ResumeLayout(false);
            this.tableLayoutFilter.PerformLayout();
            this.flowLayoutPanelFiltroNome.ResumeLayout(false);
            this.flowLayoutPanelFiltroNome.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProduto)).EndInit();
            this.flowLayoutPanelEditar.ResumeLayout(false);
            this.flowLayoutPanelEditar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.tableLayoutPanelEditar.ResumeLayout(false);
            this.panel2Editar.ResumeLayout(false);
            this.panel2Editar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.tabControlEditar.ResumeLayout(false);
            this.tabPage1Dados.ResumeLayout(false);
            this.tableLayoutPanelDados.ResumeLayout(false);
            this.tableLayoutPanelTipoProduto.ResumeLayout(false);
            this.tableLayoutPanelTipoProduto.PerformLayout();
            this.tableLayoutPanelAtivo.ResumeLayout(false);
            this.tableLayoutPanelAtivo.PerformLayout();
            this.tableLayoutPanelRow2Col1_2.ResumeLayout(false);
            this.tableLayoutPanelRow2Col1_2.PerformLayout();
            this.tableLayoutPanelNome.ResumeLayout(false);
            this.tableLayoutPanelNome.PerformLayout();
            this.tableLayoutPanelGrupo.ResumeLayout(false);
            this.tableLayoutPanelGrupo.PerformLayout();
            this.tableLayoutPanelUnidade.ResumeLayout(false);
            this.tableLayoutPanelUnidade.PerformLayout();
            this.tableLayoutPanelComissao.ResumeLayout(false);
            this.tableLayoutPanelComissao.PerformLayout();
            this.tableLayoutPanelComissaoValor.ResumeLayout(false);
            this.tableLayoutPanelComissaoValor.PerformLayout();
            this.tableLayoutPanelPreco.ResumeLayout(false);
            this.tableLayoutPanelPreco.PerformLayout();
            this.tableLayoutPanelManutensao.ResumeLayout(false);
            this.tableLayoutPanelManutensao.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingNavigator ProdutoBindingNavigator;
        private System.Windows.Forms.ToolStripButton bnInserir;
        private System.Windows.Forms.ToolStripLabel bnCount;
        private System.Windows.Forms.ToolStripButton bnConsultar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton bnGravar;
        private System.Windows.Forms.ToolStripButton bnCancelar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton bnPrimeiro;
        private System.Windows.Forms.ToolStripButton bnAnterior;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripTextBox bnPosition;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton bnProximo;
        private System.Windows.Forms.ToolStripButton bnUltimo;
        private System.Windows.Forms.ToolStripButton bnDescartar;
        private System.Windows.Forms.BindingSource OsProdutoBindingSource;
        private dtsA30Produto dtsA30Produto;
        private dtsA30ProdutoTableAdapters.A30PRODUTOTableAdapter a30PRODUTOTableAdapter1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelConsultar;
        private System.Windows.Forms.Label headLabel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.DataGridView dgvProduto;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelEditar;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label headLabelEditar;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelEditar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutFilter;
        private System.Windows.Forms.Panel panel4ConsultarBottom;
        private System.Windows.Forms.Panel panel2Editar;
        private System.Windows.Forms.TabControl tabControlEditar;
        private System.Windows.Forms.TabPage tabPage1Dados;
        private System.Windows.Forms.Panel panel4Editar;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelDados;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelTipoProduto;
        private System.Windows.Forms.Label labelTipoProduto;
        private System.Windows.Forms.ComboBox cmbTipoProduto;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelAtivo;
        private System.Windows.Forms.Label labelDummyAtivo;
        private System.Windows.Forms.CheckBox cbAtivo;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelRow2Col1_2;
        private System.Windows.Forms.Label labelCodigo;
        private System.Windows.Forms.TextBox tbCodigo;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelNome;
        private System.Windows.Forms.Label labelNome;
        private System.Windows.Forms.TextBox tbNome;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelGrupo;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelUnidade;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelComissao;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelComissaoValor;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelPreco;
        private System.Windows.Forms.Label labelGrupo;
        private System.Windows.Forms.Label labelUnidade;
        private System.Windows.Forms.Label labelComissao;
        private System.Windows.Forms.Label labelComissaoValor;
        private System.Windows.Forms.Label labelPreco;
        private System.Windows.Forms.ComboBox cmbGrupo;
        private System.Windows.Forms.ComboBox cmbUnidade;
        private System.Windows.Forms.Panel panelFillerRow1;
        private System.Windows.Forms.Button btnResizeColWidth;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Panel panelFillRow1ColLast;
        private System.Windows.Forms.TextBox tbTrackValue;
        private System.Windows.Forms.Label lblTrackBar;
        private System.Windows.Forms.DataGridViewTextBoxColumn produtoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn descricaoDataGridViewTextBoxColumn;
        private System.Windows.Forms.CheckBox cbFiltroAtivo;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelFiltroNome;
        private System.Windows.Forms.Label labelFiltroNome;
        private System.Windows.Forms.TextBox tbFiltroNome;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn postoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn produtoDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn descricaoDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn codBarraDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn grupoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn comissaoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vlComisDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtLanctoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn aliquotaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn estMinimoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn precoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn uniCompraDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn uniVendaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn relacaoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn custoMedioDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtUltCompraDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vlUltCompraDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vlUltPrecoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn icmsRetDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn claFiscalDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sitTributDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn paginaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn codInternoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pontoQuantDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pontoValDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtAlterDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn odometroDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn inativoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mP135DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sefazDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn margemMaxDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn codigoTCSDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pesoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn limiteDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn grupoIFDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn grupoTCSDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn volDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn codigoPetroDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fotoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cozinhaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn codigoNCMDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn codigoANPDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn codigoValeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bloquearAcimaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bloquearFracionarioDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn codigoFidelizeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sTPISDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sTCofinsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn aliquotaPISDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn aliquotaCofinsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn codigoEXCardDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn natOperDataGridViewTextBoxColumn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelManutensao;
        private System.Windows.Forms.Label labelManutencao;
        private System.Windows.Forms.TextBox tbManutencao;
        private System.Windows.Forms.TextBox tbPreco;
        private System.Windows.Forms.TextBox tbComissaoValor;
        private System.Windows.Forms.TextBox tbComissao;
    }
}