﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TExpress;

namespace osExpress.Manutencoes
{
    public partial class ManRede : osExpress.FrmPrincipal
    {
        // CRUD OsClienteRede
        public ManRede()
        {
            InitializeComponent();
            DB.ConnectionName = "osExpress.Properties.Settings.OsExpressConnectionString";
        }

        private void ManRede_Load(object sender, EventArgs e)
        {
            int w = (int)(this.Parent.ClientSize.Width * 0.9);
            int h = (int)(this.Parent.ClientSize.Height * 0.9);
            this.Size = new Size(w, h);
            int x = (int)(this.Parent.ClientSize.Width - w) / 2;
            int y = (int)(this.Parent.ClientSize.Height - h) / 2;
            this.Location = new Point(x, y);

            tableLayoutPanelConsultar.BackColor = System.Drawing.Color.Transparent;
            panel4ConsultarBottom.BackColor = System.Drawing.Color.Transparent;
            panel4Editar.BackColor = System.Drawing.Color.Transparent;

            groupBox1.Visible = false;

            // nur hier
            dgvRede.Columns.Clear();
            SetupDatagridView();

            tableLayoutPanelEditar.Location = new Point(tableLayoutPanelConsultar.Location.X, tableLayoutPanelConsultar.Location.Y);
            tableLayoutPanelEditar.Dock = DockStyle.Fill;
            tableLayoutPanelEditar.Visible = false;
            panel2Editar.BackColor = System.Drawing.Color.Transparent;
            tableLayoutPanelEditar.BackColor = System.Drawing.Color.Transparent;
            tableLayoutPanelDados.CellBorderStyle = TableLayoutPanelCellBorderStyle.None;

            foreach (Control c in tableLayoutPanelDados.Controls)
            {
                if (c.GetType() == typeof(TableLayoutPanel))
                {                  
                   c.BackColor = System.Drawing.Color.Transparent;
                }
            }

            TableLayoutColumnStyleCollection columnStyles;
            columnStyles = tableLayoutPanelDados.ColumnStyles;
            columnStyles[0].SizeType = SizeType.Absolute;
           
            //float totalWidth = tableLayoutPanelDados.Width;
            //float wpixel = totalWidth / 20;
          
            panelFillerRow1.BackColor = System.Drawing.Color.Transparent;
            columnStyles[0].Width = 10;
                 
            panelFillRow1ColLast.BackColor = System.Drawing.Color.Transparent;
            columnStyles[0].Width = 10;
                       
            // TODO: This line of code loads data into the 'dtsRede._OsClienteRede' table. You can move, or remove it, as needed.
            this.osClienteRedeTableAdapter.Fill(this.dtsRede._OsClienteRede);

        }
        private void SetupDatagridView()
        {
            dgvRede.AutoGenerateColumns = true;
            //DataTable datasource = GetProdutos(this.DESCRICAO);
            //OsProdutoBindingSource.DataSource = datasource;
            // ajustar DatagridView
            //dgvProduto.AutoGenerateColumns = true;          

            dgvRede.Columns["id"].HeaderText = "Id";
            dgvRede.Columns["id_rede"].HeaderText = "Id Rede";
            dgvRede.Columns["nome_rede"].HeaderText = "Nome Rede";
            dgvRede.Columns["obs"].HeaderText = "Observação";


            this.osClienteRedeTableAdapter.Fill(this.dtsRede._OsClienteRede);

            //DataGridViewColumn column = dgvProduto.Columns["produto"];
            //column.Width = 50;
          
            //column = dgvProduto.Columns["inativo"];
            //column.Width = 20;
           

            //dgvProduto.Columns["id"].Visible = false;
          
            //this.a30PRODUTOTableAdapter1.Fill(this.dtsA30Produto.A30PRODUTO);
        }

        private void bnConsultar_Click(object sender, EventArgs e)
        {
            tableLayoutPanelConsultar.Visible = true;
            tableLayoutPanelEditar.Visible = false;

            OsClienteRedeBindingSource.CancelEdit();

            bnDescartar.Enabled = false;
            bnInserir.Enabled = true;
            bnPrimeiro.Enabled = true;
            bnAnterior.Enabled = true;

            pesquisar();
            
        }

        private void bnCancelar_Click(object sender, EventArgs e)
        {
            OsClienteRedeBindingSource.CancelEdit();
            tableLayoutPanelConsultar.Visible = true;
            tableLayoutPanelEditar.Visible = false;

            bnCancelar.Enabled = false;
            bnDescartar.Enabled = false;
            bnInserir.Enabled = true;
            bnPrimeiro.Enabled = true;
            bnAnterior.Enabled = true;

            pesquisar();
        }

        private void dgvRede_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            SelectRede();
        }

        private void dgvRede_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.S)
            {
                SelectRede();
            }
        }

        private void SelectRede()
        {
            limparCampos();
            editar();
            tableLayoutPanelConsultar.Visible = false;
            tableLayoutPanelEditar.Visible = true;

            bnCancelar.Enabled = true;
            bnDescartar.Enabled = true;

            // preencher campos atuais
            DataRowView current = (DataRowView)OsClienteRedeBindingSource.Current;
                     
            tbRedeId.Text = current["id_rede"].ToString();
            tbRedeNome.Text = current["nome_rede"].ToString();
            tbObservacao.Text = current["obs"].ToString();
                       
        }
        private void errorMessage(string m)
        {
            MessageBox.Show(m,
                               "E R R O - Validação",
                               MessageBoxButtons.OK,
                               MessageBoxIcon.Error);
        }
        private Boolean validar()
        {
            // id não pode ser vazio
            if (String.IsNullOrWhiteSpace(tbRedeId.Text))
            {
                errorMessage(@"<Id Rede> não pode ser vazio.");
                return false;
            }
            // nome não pode ser vazio
            if (String.IsNullOrWhiteSpace(tbRedeNome.Text))
            {
                errorMessage(@"<Nome Rede> não pode ser vazio.");
                return false;
            }
                    
            return true;
        }

        private void limparCampos()
        {
           tbRedeId.Clear();
           tbRedeNome.Clear();
           tbObservacao.Clear();                     
        }

        private void tbRedeId_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
            //int n = (int)e.KeyChar;
            //e.Handled = !(n == 8 || (n >= 48 && n <= 57) );
        }

        private void bnPrimeiro_Click(object sender, EventArgs e)
        {
            SelectRede();
        }

        private void bnAnterior_Click(object sender, EventArgs e)
        {
            SelectRede();
        }

        private void bnProximo_Click(object sender, EventArgs e)
        {
            SelectRede();
        }

        private void bnUltimo_Click(object sender, EventArgs e)
        {
            SelectRede();
        }

        private string proximoRede()
        {           
            var sql = @"select RIGHT('00000' + cast( max(id) + 1 as varchar), 6)  from OsClienteRede";
            return DB.GetSingleValue(sql).ToString();
        }
        private void bnInserir_Click(object sender, EventArgs e)
        {
            limparCampos();

            // get next codigo produto A30PRODUTO.produto (varchar(6))
            tbRedeId.Text = proximoRede();

            tableLayoutPanelConsultar.Visible = false;
            tableLayoutPanelEditar.Visible = true;

            inserir();
            
            bnConsultar.Enabled = true;
            bnCancelar.Enabled = true;
            bnInserir.Enabled = false;
            bnPrimeiro.Enabled = false;
            bnAnterior.Enabled = false;
            bnDescartar.Enabled = false;
                       
        }

        private void bnGravar_Click(object sender, EventArgs e)
        {
            DataRowView current = (DataRowView)OsClienteRedeBindingSource.Current;

            var xid = current["id"];            

            Boolean ok = validar();
            if (!ok)
            {
                return;
            }

            if (estadoCorrente == EstadoManutencao.smInserir)
            {
                int res = InsertMe();
                if (res == 0)
                {
                    bnInserir.Enabled = true;
                    pesquisar();
                    SetupDatagridView();
                    bnConsultar.PerformClick();
                    dgvRede.FirstDisplayedScrollingRowIndex = 0;
                    try
                    {
                        dgvRede.Rows[0].Selected = true;
                    }
                    catch (Exception de) { }

                }
            }

            if (estadoCorrente == EstadoManutencao.smEditar)
            {
                UpdateMe();
            }

            bnInserir.Enabled = true;
            bnPrimeiro.Enabled = true;
            bnAnterior.Enabled = true;
            bnConsultar.PerformClick();
        }


        private void UpdateMe()
        {
            DataRowView current = (DataRowView)OsClienteRedeBindingSource.Current;
            string id = current["id"].ToString();
            string id_rede = tbRedeId.Text;
            string nome_rede = tbRedeNome.Text;
            string observacao = tbObservacao.Text;
            
            string sql_update = @"UPDATE OsClienteRede SET "
                              + "id_rede = '" + id_rede + "',"
                              + "nome_rede = '" + nome_rede + "',"
                              + "obs = '" + observacao + "'"                            
                              + " WHERE id=" + id;
            try
            {
                // update database        
                DB.ExecuteNonQuery(sql_update);
                // update BindingSource ...
                this.OsClienteRedeBindingSource.EndEdit();
                this.osClienteRedeTableAdapter.Update(this.dtsRede._OsClienteRede);

                SetupDatagridView();
            }
            catch (Exception ie)
            {
                MessageBox.Show(ie.Message,
                              "E R R O - Update",
                              MessageBoxButtons.OK,
                              MessageBoxIcon.Information);

            }
            MessageBox.Show(@"Produto alterado com sucesso!");
        }

        private void DeleteMe()
        {
            DataRowView current = (DataRowView)OsClienteRedeBindingSource.Current;
            var id = current["id"];
            //
            string sql_delete = @"DELETE FROM OsClienteRede WHERE id = " + id;
            try
            {
                OsClienteRedeBindingSource.EndEdit();
                osClienteRedeTableAdapter.Update(this.dtsRede);

                DB.ExecuteNonQuery(sql_delete);
                MessageBox.Show(@"Rede descartado com sucesso!");
                pesquisar();
                bnDescartar.Enabled = false;
                SetupDatagridView();
                bnConsultar.PerformClick();
                dgvRede.FirstDisplayedScrollingRowIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,
                                "E R R O - Delete",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        private int InsertMe()
        {
            DateTime registro = DateTime.Now;

            string id_rede = tbRedeId.Text;
            string nome_rede = tbRedeNome.Text;
            string observacao = tbObservacao.Text;

            string sql = @"INSERT INTO OsClienteRede(id_rede, nome_rede, obs) 
                           VALUES('" + id_rede + "', '"
                                   + nome_rede + "', '"
                                   + observacao + "'"                                   
                                   + ")";
            try
            {
                DB.ExecuteNonQuery(sql);
            }
            catch (Exception ie)
            {
                MessageBox.Show(ie.Message,
                              "E R R O - Insert",
                              MessageBoxButtons.OK,
                              MessageBoxIcon.Error);
                return -1;
            }
            MessageBox.Show(@"Rede cadastrado com sucesso!");
            return 0;
        }

        private void bnDescartar_Click(object sender, EventArgs e)
        {
            string pergunta = "Deseja realmente descartar essa rede ?";
            if (MessageBox.Show(pergunta,
                               "alerta da confirmação",
                               MessageBoxButtons.YesNo,
                               MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                DeleteMe();
            }
        }
       
    }
}
