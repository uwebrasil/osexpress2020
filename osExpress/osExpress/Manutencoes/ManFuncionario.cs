﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TExpress;

namespace osExpress.Manutencoes
{
    public partial class ManFuncionario : osExpress.FrmPrincipal
    {        
        public string FILTER_FUNCIONARIO_NOME = "";

        public BindingSource bdsInstrumentos;
        public BindingSource bdsOrcamentos;
        public BindingSource bdsPedidos;
        public BindingSource bdsServicos;


        public ManFuncionario()
        {
            InitializeComponent();
            corrigirLayout();

            DB.ConnectionName = "osExpress.Properties.Settings.OsExpressConnectionString";

            // enable Checkbox S/N (SIM/NAO)
            Binding binding = new Binding("Checked", bsXOSFuncionario, "isusuario", true);
            binding.Format += new ConvertEventHandler(binding_Format);
            binding.Parse += new ConvertEventHandler(binding_Parse);
            checkboxIsUsuario.DataBindings.Add(binding);

            //foreach (TabPage tab in tabChild.TabPages)
            //{
            //    tab.Enabled = false;
            //   
            //}
            //(tabChild.TabPages[0] as TabPage).Enabled = true;

            bdsInstrumentos = new BindingSource();
            string sql = @"SELECT o.IDINMETRO, 
                                  i.MARCA_INSTRUMENTO, 
                                  b.NUM_INMETRO, 
                                  i.NUM_SERIE, 
                                  e.NUM_ETIQUETA, 
                                  o.IDETIQUETA, 
                                  o.IDIBAMETRO,
                                  o.IDORDEM, 
                                  --o.ID,
                                  o.marca_inicial, o.selo_anterior,
			                      e.utilizado, e.dt_cadastro, e.dt_atribuicao,
                                  i.IDCLIENTE,c.razao_social,e.idusuario
                         FROM OsInstrumentoServicoOsCliente AS o INNER JOIN
                              OsIbametroOsCliente AS i ON i.ID = o.IDIBAMETRO INNER JOIN
                              OsEtiqueta AS e ON e.ID = o.IDETIQUETA INNER JOIN
                              OsIbametroInmetroOsCliente AS b ON b.ID = o.IDINMETRO INNER JOIN
                              OsCliente c On c.id = i.idcliente 
                         WHERE (1 = 1) 
                         ORDER BY o.idordem desc";
            bdsInstrumentos.DataSource = DB.GetTableFromSQL(sql);
            dgvInstrumentos.DataSource = bdsInstrumentos;

            bdsOrcamentos = new BindingSource();
            sql = @"select tipo_doc, status_os, id,id_final,dt_cadastro,dt_fechamento,
                     idcliente,idcliente_pedido,obs,idusuario                    
                      from osordemservico o
                    where 1=1             
                      and tipo_doc = 'P' and status_os='A'        
                     order by o.dt_cadastro desc,o.tipo_doc ";
            bdsOrcamentos.DataSource = DB.GetTableFromSQL(sql);
            dgvOrcamentos.DataSource = bdsOrcamentos;

            bdsPedidos = new BindingSource();
            sql = @"select tipo_doc, status_os, id,id_final,dt_cadastro,dt_fechamento,
                     idcliente,idcliente_pedido,obs,idusuario                    
                      from osordemservico o
                    where 1=1             
                      and tipo_doc = 'P' and status_os='F'        
                     order by o.dt_cadastro desc,o.tipo_doc ";
            bdsPedidos.DataSource = DB.GetTableFromSQL(sql);
            dgvPedidos.DataSource = bdsPedidos;

            bdsServicos = new BindingSource();
            sql = @"select tipo_doc, status_os, id,id_final,dt_cadastro,dt_fechamento,
                     idcliente,idcliente_pedido,obs,idusuario                    
                      from osordemservico o
                    where 1=1             
                      and tipo_doc = 'S'        
                     order by o.dt_cadastro desc,o.tipo_doc ";
            bdsServicos.DataSource = DB.GetTableFromSQL(sql);
            dgvServicos.DataSource = bdsServicos;
          
        }

        // usado pelo checkbox isUsuario
        private void binding_Format(object sender, ConvertEventArgs e)
        {
            if (e.Value.ToString() == "S") e.Value = true;
            else e.Value = false;
        }
        // usado pelo checkbox isUsuario
        private void binding_Parse(object sender, ConvertEventArgs e)
        {
            if ((bool)e.Value) e.Value = "S";
            else e.Value = "N";
        }

        private void corrigirLayout()
        {
            labelCadastrarFuncionario.Location = new Point(300, 70);
            
            // padronizar janelas, panels, tabcontrols
            pnlConsultar.Location = Helper.Layout.LocationClientePanel; // (10,90)
            tabPrincipal.Location = Helper.Layout.LocationClienteTabControl; // ()

            //public static Point LocationClientePanel = new Point(10, 90);
            //public static Point LocationClienteTabControl = new Point(10, 190);

            pnlConsultar.Size = new Size(1080, 655); //Helper.Layout.SizeClientePanel;
            tabPrincipal.Size = new Size(1080, 555); //Helper.Layout.SizeClienteTabControl;
            //Helper.Layout.SizeClienteTabControl;

            // layout test start
            // PNLCONSULTAR           

            pnlConsultar.Size = new Size(1300, 655);
            dgvFuncionario.Size = new Size(1100, 260);

            //int gloc_x = (pnlConsultar.Size.Width - dgvFuncionario.Size.Width) / 2;
            //int gloc_y = dgvFuncionario.Location.Y;

            //dgvFuncionario.Location = new Point(gloc_x, gloc_y);

                                   
            // Filter
            //groupBox2.Location = new Point(gloc_x, groupBox2.Location.Y);
            // em vermelho, informar como abrir detalhes referente ao cliente 
            //label8.Location = new Point(gloc_x, label8.Location.Y);

            // 07.01.2021
            //dgvFuncionario.Location = new Point(gloc_x, gloc_y + 100);
            //label8.Location = new Point(gloc_x, label8.Location.Y + 100);

            
            dgvFuncionario.Location = new Point(pnlConsultar.Location.X + 50, dgvFuncionario.Location.Y + 50);
            label8.Location = new Point(pnlConsultar.Location.X + 50, dgvFuncionario.Location.Y - 20);

            labelLike.Location = new Point(pnlConsultar.Location.X + 50, dgvFuncionario.Location.Y - 70);
            tbLike.Location = new Point(pnlConsultar.Location.X + 50, dgvFuncionario.Location.Y - 50);
            tbLike.Size = new Size(dgvFuncionario.Size.Width, 20);


            // TABPRINCIPAL                      
            
           // tabPrincipal.Location = new Point(10, 90);
            tabPrincipal.Location = new Point(10, 100);
          
            tabPrincipal.Size = new Size(1080, 580);
            //labelCadastrarCliente.Location = new Point(300, 70);

            tabChild.Location = new Point(10, 180);
            
            tabChild.Size = new Size(1060, 370);
            //listView1.Size = new Size(1020, 100);

            //pnlEditarBomba.Location = new Point(390, 10);
            //btnGravarBomba.Location = new Point(19, 295);
           
            //btnCancelar.Location = new Point(104, 295);
            //btnLimparBomba.Location = new Point(18, 170);
            //btnDeletarBomba.Location = new Point(189, 295);
            
            //rtbHelp2.Location = new Point(15, 10);
            //rtbHelp2.Size = new Size(340, 150);
            //rtbHelp2.Size = new Size(250, 250);

            // end layout test

            // esconder campos originais os quais permanecem ser importante
            // porque os bindings com campos de mascara vem com o problema
            // da mascara ...
            //CNPJ
            cNPJTextBox.Location = mtbCNPJ.Location;
            cNPJTextBox.Size = mtbCNPJ.Size;
            cNPJTextBox.SendToBack();
            //CPF
            cPFTextBox.Location = mtbCPF.Location;
            cPFTextBox.Size = mtbCPF.Size;
            cPFTextBox.SendToBack();
            //Telefone
            tELEFONETextBox.Location = mtbTelefone.Location;
            tELEFONETextBox.Size = mtbTelefone.Size;
            tELEFONETextBox.SendToBack();

            //textboxFiltroCliente.Visible = false;
            //labelFiltroCliente.Visible = false;
          
            
        }

        private void ManFuncionario_Load(object sender, EventArgs e)
        {           
            int w = (int)(this.Parent.ClientSize.Width * 0.9);
            int h = (int)(this.Parent.ClientSize.Height * 0.9);
            this.Size = new Size(w, h);
            int x = (int)(this.Parent.ClientSize.Width - w) / 2;
            int y = (int)(this.Parent.ClientSize.Height - h) / 2;
            this.Location = new Point(x, y);
            // TODO: This line of code loads data into the 'dtsXOSFuncionario.XOsFuncionario' table. You can move, or remove it, as needed.
            //this.xOsFuncionarioTableAdapter.Fill(this.dtsXOSFuncionario.XOsFuncionario);           

            dgvFuncionario.Columns.Clear();
            SetupDatagridView();

        }

        private DataTable GetFuncionarios(string filter = "")
        {
            string sql = @"select 
                                  --Posto, Funcionario, 
                                  id,Nome, idusuario, Tipo_Pessoa, Cgc, RG, isusuario, uNome, uSenha,
                                  cep, estado, endereco, bairro, cidade,
                                  obs01,obs02
                                  --Turno, Tipo_Pessoa, Cgc, Inscricao, Endereco, Bairro, Cidade, 
                                  --Estado, Cep, obs01, obs02, obs03, obs04, obs05, obs06, obs07, obs08, 
                                  --obs09, obs10, Noturno, ID, Inativo, Dt_Alter, Obs11, Obs12, Obs13, Obs14, 
                                  --Obs15, Obs16, Obs17, Obs18, Obs19, Obs20, RFID, Biometria, CTA_Caixa, Tipo_VIP,
                                  --uNome, uSenha, tipo_acesso, dt_cadastro, ultimo_acesso, plano_conta, acessa_pedido, digita_selo_lacre, 
                                  --idusuario, isusuario 
                                 FROM XOSFUNCIONARIO
                                     WHERE 1=1 ";
            if (filter.Length > 0)
            {
                sql += @" AND Upper( nome) like '%" + filter + "%'";
            }
           
            sql += @" ORDER BY nome";
            DataTable data = DB.GetTableFromSQL(sql);
            return data;
        }

        private void SetupDatagridView()
        {              

            dgvFuncionario.AutoGenerateColumns = true;
            
            DataTable datasource = GetFuncionarios(this.FILTER_FUNCIONARIO_NOME);
            // or only set filter...
            
            bsXOSFuncionario.DataSource = datasource;
            // ajustar DatagridView
            //dgvFuncionario.AutoGenerateColumns = true;

            //gridview.Columns["ColumnName"].Visible = false;
            //  id,Nome, idusuario, Tipo_Pessoa, Cgc, RG, isusuario, uNome, uSenha,
            //    9 - 13                  cep, estado, endereco, bairro, cidade,
            //    14,15                  obs01,obs02

            // id (funcionario)
            dgvFuncionario.Columns[0].Visible = false;
            // nome
            dgvFuncionario.Columns[1].Width = 200;
            // id (usuario)
            dgvFuncionario.Columns[2].Visible = false;
            // tipo_pessoa
            dgvFuncionario.Columns[3].Visible = false;
            // isusuario
            dgvFuncionario.Columns[6].Visible = false;

            // RG
            dgvFuncionario.Columns[5].Width = 80;
            // Endereço
            dgvFuncionario.Columns[11].Width = 180;
            // cgc (CPF)
            dgvFuncionario.Columns[4].HeaderText = "CPF";
            dgvFuncionario.Columns[4].Width = 80;
            // unome
            dgvFuncionario.Columns[7].HeaderText = "Login";
            dgvFuncionario.Columns[7].Visible = false;
            // usenha
            dgvFuncionario.Columns[8].HeaderText = "Senha";
            dgvFuncionario.Columns[8].Visible = false;
            // CEP
            dgvFuncionario.Columns[9].HeaderText = "CEP";
            dgvFuncionario.Columns[9].Width = 60;
            //dgvFuncionario.Columns[9].DisplayIndex = 1;
            // Estado
            dgvFuncionario.Columns["Estado"].HeaderText = "UF";
            dgvFuncionario.Columns["Estado"].Width = 30;
            // email obs01
            dgvFuncionario.Columns[14].HeaderText = "Email";
            // telefone obs02
            dgvFuncionario.Columns[15].HeaderText = "Telefone";


            /*
            dgvFuncionario.Columns["vr_manutencao"].DefaultCellStyle.Format = "C2";
            dgvFuncionario.Columns["vr_manutencao"].DefaultCellStyle.FormatProvider = CultureInfo.GetCultureInfo("pt-BR");

            dgvFuncionario.Columns["preco"].DefaultCellStyle.Format = "C2";
            dgvFuncionario.Columns["preco"].DefaultCellStyle.FormatProvider = CultureInfo.GetCultureInfo("pt-BR");

            dgvFuncionario.Columns["comissao"].DefaultCellStyle.Format = "0.00\\%";
            dgvFuncionario.Columns["comissao"].DefaultCellStyle.FormatProvider = CultureInfo.GetCultureInfo("pt-BR");

            dgvFuncionario.Columns["vl_comis"].DefaultCellStyle.Format = "C2";
            dgvFuncionario.Columns["vl_comis"].DefaultCellStyle.FormatProvider = CultureInfo.GetCultureInfo("pt-BR");

           
            */
          
        }
        private void FiltrarDataGridViewViaLike(string txt)
        {
            bsXOSFuncionario.Filter = " nome LIKE '%" + txt + "%'";

            this.xOsFuncionarioTableAdapter.Fill(this.dtsXOSFuncionario.XOsFuncionario);
        }

        private void dgvFuncionario_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            selectFuncionario();
        }
        private void tbLike_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                FiltrarDataGridViewViaLike(((TextBox)sender).Text);
            }
        }

        private void selectFuncionario()
        {
            //
            DataRowView current = (DataRowView) bsXOSFuncionario.Current;

            bdsInstrumentos.Filter = @"idusuario = " + current["idusuario"].ToString();

            bdsOrcamentos.Filter = @"idusuario = " + current["idusuario"].ToString();
            bdsPedidos.Filter = @"idusuario = " + current["idusuario"].ToString();
            bdsServicos.Filter = @"idusuario = " + current["idusuario"].ToString();

            tbLogin.Enabled = false;
            tbSenha.Enabled = false;
            if (current["isUsuario"].ToString().Equals("S"))
            {
                tbLogin.Enabled = true;
                tbSenha.Enabled = true;
            }


            //if (comboBox2.SelectedItem.ToString().Equals("F"))
            //{
                mtbCPF.Enabled = true;
                mtbCNPJ.Enabled = false;
                mtbCNPJ.Text = "";
            //}
            //if (comboBox2.SelectedItem.ToString().Equals("J"))
            //{
            //    mtbCPF.Enabled = false;
            //    mtbCNPJ.Enabled = true;
            //    mtbCPF.Text = "";
            //}
            

            tabPrincipal.Visible = true;

            tabPrincipal.SelectedTab = tabPage1;
            //tabChild.SelectedTab = tabEndereco;

            pnlConsultar.Visible = false;

            labelCadastrarFuncionario.Text = "Cadastro Funcionário - alterar";
            labelCadastrarFuncionario.Visible = true;
            editar();
            //tsbDescartar.Enabled = true;
            bnCancelar.Enabled = true;
            bnDescartar.Enabled = true;
            //SetTipoNegocio();
        }

        private string stripAllButDigits(string str)
        {
            return new string((from c in str
                               where char.IsDigit(c)
                               select c
            ).ToArray());
        }

        private bool isValidFuncionario()
        {

            string nome = tbNome.Text.Trim();
            if (nome.Length == 0)
            {
                MessageBox.Show(@"Favor preencher Nome do Funcionário",
                                @"E R R O",
                                MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }

            // Ou CPF (Pessoa Física) ou CNPJ (Pessoa Jurídica), mas não os dois ...
            //string cnpj = stripAllButDigits(mtbCNPJ.Text);
            string cpf = stripAllButDigits(mtbCPF.Text);
            if ( (!mtbCPF.MaskFull))
            {
                MessageBox.Show(@"Favor preencher CPF (Pessoa Física)",
                                @"E R R O",
                                MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
                       
            // CPF válido ?
            //if (comboBox2.SelectedItem.ToString().Equals("F"))
            //{
                if (!Helper.CPFHelper.IsCpf(cpf))
                {
                    MessageBox.Show(@"CPF inválido!",
                        @"E R R O",
                        MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return false;
                }
            //}
            // Email válido ? ( só caso preenchido )
            string email = tb_email.Text;
            if (email.Length > 0)
                if (!Helper.Validation.IsValidEmail(email))
                {
                    MessageBox.Show(@"Email inválido!",
                                   @"E R R O",
                                   MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return false;
                }

          
            //cNPJTextBox.Text = cnpj;
            cPFTextBox.Text = cpf;
            return true;
        }

        private void dgvFuncionario_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.S)
            {

                selectFuncionario();

            }
        }

        private void bnConsultar_Click(object sender, EventArgs e)
        {
            bdsInstrumentos.Filter = @"";
            bdsOrcamentos.Filter = @"";
            bdsPedidos.Filter = @"";
            bdsServicos.Filter = @"";

            bsXOSFuncionario.CancelEdit();
            pesquisar();
            labelCadastrarFuncionario.Text = "Cadastro Funcionário - consultar";
            labelCadastrarFuncionario.Visible = false;
            tabPrincipal.Visible = false;
            pnlConsultar.Visible = true;

            bnInserir.Enabled = true;
            bnPrimeiro.Enabled = true;
            bnAnterior.Enabled = true;
            bnProximo.Enabled = true;
            bnUltimo.Enabled = true;

            bnDescartar.Enabled = false;
            tabPrincipal.SelectedTab = tabPage1;
            tabChild.SelectedTab = tabEndereco;

            /*
            osClienteBindingSource.CancelEdit();
            pnlConsultar.Visible = true;
            this.osClienteTableAdapter.Fill(this.dtsPrincipal.OsCliente);
            pesquisar();
            tsbDescartar.Enabled = false;
            labelCadastrarCliente.Visible = false;
            tabPrincipal.SelectedTab = tabPage1;
            tabChild.SelectedTab = tabEndereco;
             * */


        }

        private void comboBox2_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (comboBox2.SelectedItem.ToString().Equals("F"))
            {
                // if (estadoCorrente == EstadoManutencao.smInserir)
                {
                    mtbCNPJ.Text = "";
                    mtbCNPJ.Enabled = false;
                    mtbCPF.Enabled = true;
                }
            }
            if (comboBox2.SelectedItem.ToString().Equals("J"))
            {
                //if (estadoCorrente == EstadoManutencao.smInserir)
                {
                    mtbCPF.Text = "";
                    mtbCPF.Enabled = false;
                    mtbCNPJ.Enabled = true;
                }
            }
        }

        private void bnGravar_Click(object sender, EventArgs e)
        {           
            DataRowView current = (DataRowView)bsXOSFuncionario.Current;
            var xid = current["id"];           

            Boolean ok = isValidFuncionario();
            if (!ok)
            {
                return;
            }

            if (estadoCorrente == EstadoManutencao.smInserir)
            {
                
                int res = InsertMe();
                if (res == 0)
                {
                    bnInserir.Enabled = true;
                    pesquisar();
                    SetupDatagridView();
                    bnConsultar.PerformClick();
                    dgvFuncionario.FirstDisplayedScrollingRowIndex = 0;
                    try
                    {
                        dgvFuncionario.Rows[0].Selected = true;
                    }
                    catch (Exception de) { }

                }
                 
            }

            if (estadoCorrente == EstadoManutencao.smEditar)
            {
                UpdateMe();
            }

            bnInserir.Enabled = true;
            bnPrimeiro.Enabled = true;
            bnAnterior.Enabled = true;
            bnConsultar.PerformClick();
            
        }

        private string GetNextIdUsuario()
        {
            // criar autoincrement e preservar dados velhos : dificil
            // preservar dados velhos                       
            string sql = @"select max(idusuario) from xosfuncionario";
            int maxid = (int)DB.GetSingleValue(sql);
            maxid = maxid < 120000 ? maxid + 10000 : maxid + 1;

            return maxid.ToString();
        }

        private string GetNextIdFuncionario()
        {
            string sql = @"select max(id) from xosfuncionario"; 
            int maxid = (int) DB.GetSingleValue(sql);
            maxid = maxid < 1000 ? maxid + 1000 : maxid + 1;

            return maxid.ToString();
        }
        private void UpdateMe()
        {
            /*
             *  Nome, idusuario, Tipo_Pessoa, Cgc, RG, isusuario, uNome, uSenha,
                                  cep, estado, endereco, bairro, cidade,
                                  obs02
             */
            this.bsXOSFuncionario.EndEdit();

            DataRowView current = (DataRowView)bsXOSFuncionario.Current;
            string id = current["id"].ToString();

            string nome = current["nome"].ToString().Trim(); //tbNome.Text;
            string isUsuario = checkboxIsUsuario.Checked ? "S" : "N";
            string cgc = current["cgc"].ToString().Trim();
            string rg  = current["rg"].ToString().Trim();
            string unome = current["uNome"].ToString().Trim();
            string usenha = current["uSenha"].ToString().Trim();

            string cep = current["cep"].ToString().Trim();
            string estado = current["estado"].ToString().Trim();
            string endereco = current["endereco"].ToString().Trim();
            string bairro = current["bairro"].ToString().Trim();
            string cidade = current["cidade"].ToString().Trim();
            // email
            string obs01 = current["obs01"].ToString().Trim();
            // telefone
            string obs02 = current["obs02"].ToString().Trim();
         
            // vai perder todos os vinculações com os movimentos !!
            // if (isUsuario.Equals("N")) sql_update += @"idusuario = null,";
            string idUsuario = current["idusuario"].ToString().Trim();

            string sql_update = @"UPDATE XOSFuncionario SET "
                              + "nome = '" + nome + "',";
            if (isUsuario.Equals("S") && (idUsuario.Length == 0 || idUsuario.Equals("0")))
            { 
                idUsuario = GetNextIdUsuario();
                sql_update += @"idUsuario = '" + idUsuario + "',";
            }
                   sql_update += @"isUsuario = '" + isUsuario + "',"
                              + "cgc = '" + cgc + "',"
                              + "rg = '" + rg + "',"

                              + "unome = '" +  unome + "',"
                              + "usenha = '" + usenha + "',"

                              + "cep = '" + cep + "',"
                              + "estado = '" + estado + "',"
                              + "endereco = '" + endereco + "',"
                              + "bairro = '" + bairro + "',"
                              + "cidade = '" + cidade + "',"

                              + "obs01 = '" + obs01 + "',"
                              + "obs02 = '" + obs02 + "'"
                             
                              + " WHERE id=" + id;
            try
            {
                // update database        
                DB.ExecuteNonQuery(sql_update);
                // update BindingSource ...
                //this.bsXOSFuncionario.EndEdit();
                this.xOsFuncionarioTableAdapter.Update(this.dtsXOSFuncionario.XOsFuncionario);

                SetupDatagridView();
            }
            catch (Exception ie)
            {
                MessageBox.Show(ie.Message,
                              "E R R O - Update",
                              MessageBoxButtons.OK,
                              MessageBoxIcon.Information);

            }
            MessageBox.Show(@"Funcionário alterado com sucesso!");
             
        }

        private int InsertMe()
        {
            DateTime registro = DateTime.Now;

          
            /*
             *  Nome, idusuario, Tipo_Pessoa, Cgc, RG, isusuario, uNome, uSenha,
                                  cep, estado, endereco, bairro, cidade,
                                  obs02
             */
            
            string isUsuario = checkboxIsUsuario.Checked ? "S" : "N";

            string id = GetNextIdFuncionario();
            string idUsuario = isUsuario == "S" ? GetNextIdUsuario() : "";
            string nome = tbNome.Text;
            string cgc = mtbCPF.Text;
            string rg = tb_rg.Text;
            string unome = tbLogin.Text;
            string usenha = tbSenha.Text;
            string cep = tbCep.Text;
            string estado = comboBox1.SelectedItem.ToString();
            string endereco = tbEndereco.Text;
            string bairro = tbBairro.Text;
            string cidade = tbCidade.Text;
            //obs01
            string email = tb_email.Text;
            //obs02
            string telefone = mtbTelefone.Text;

            // tipo_acesso = 'A' por enquanto
            string tipo_acesso = "A";


            string sql = @"INSERT INTO XOSFUNCIONARIO(idusuario,nome,cgc,rg,isusuario,uNome,uSenha,
                                                 cep,estado,endereco,bairro,cidade,obs01,obs02,tipo_acesso) 
                           VALUES('" 
                                   + idUsuario + "', '"
                                   + nome + "', '"
                                   + cgc + "','"
                                   + rg + "','"
                                   + isUsuario + "','"
                                   + unome + "','"
                                   + usenha + "','"
                                   + cep + "','"
                                   + estado + "','"
                                   + endereco + "','"
                                   + bairro + "','"
                                   + cidade + "','"
                                   + email + "','"
                                   + telefone + "','"
                                   + tipo_acesso + "'"
                                   + ")";
            try
            {
                DB.ExecuteNonQuery(sql);
            }
            catch (Exception ie)
            {
                MessageBox.Show(ie.Message,
                              "E R R O - Insert",
                              MessageBoxButtons.OK,
                              MessageBoxIcon.Error);
                return -1;
            }
            MessageBox.Show(@"Funcionário cadastrado com sucesso!");
            return 0;
        }

        private void bnInserir_Click(object sender, EventArgs e)
        {
            // get next codigo produto A30PRODUTO.produto (varchar(6))
            //tbCodigo.Text = proximoProduto();

            //tableLayoutPanelConsultar.Visible = false;
            //tableLayoutPanelEditar.Visible = true;

            pnlConsultar.Visible = false;
            tabPrincipal.Visible = true;

            inserir();

            //limparCampos();

            bnConsultar.Enabled = true;
            bnCancelar.Enabled = true;
            bnInserir.Enabled = false;
            bnPrimeiro.Enabled = false;
            bnAnterior.Enabled = false;
            bnDescartar.Enabled = false;

            //tbManutencao.Enabled = false;

            //cmbTipoProduto.Focus();
            //cmbTipoProduto.Select();
            //cmbTipoProduto.DroppedDown = true;           

        }
        private void confirmar(string aba, EstadoManutencao estado)
        {
            string x = estado == EstadoManutencao.smInserir
                ? " inserido com êxito! "
                : estado == EstadoManutencao.smExcluir
                    ? " excluido com êxito! "
                    : " alterado com êxito! ";
            MessageBox.Show(aba + x, @"I n f o m a ç ã o",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void checkboxIsUsuario_CheckedChanged(object sender, EventArgs e)
        {
            tbLogin.Enabled = false;
            tbSenha.Enabled = false;
            if (checkboxIsUsuario.Checked) {
                tbLogin.Enabled = true;
                tbSenha.Enabled = true;
            }
        }

        private void bnDescartar_Click(object sender, EventArgs e)
        {
            string pergunta = "Deseja realmente descartar esse produto ?";
            if (MessageBox.Show(pergunta,
                               "alerta da confirmação",
                               MessageBoxButtons.YesNo,
                               MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                DeleteMe();
            }
        }

        private void DeleteMe()
        {
            DataRowView current = (DataRowView)bsXOSFuncionario.Current;
            var id = current["id"];

            string sql_delete = @"DELETE FROM XOSFuncionario WHERE id = " + id;
            try
            {
                bsXOSFuncionario.EndEdit();
                this.xOsFuncionarioTableAdapter.Update(this.dtsXOSFuncionario.XOsFuncionario);

                DB.ExecuteNonQuery(sql_delete);
                MessageBox.Show(@"Funcionário descartado com sucesso!");
                pesquisar();
                bnDescartar.Enabled = false;
                SetupDatagridView();
                bnConsultar.PerformClick();
                dgvFuncionario.FirstDisplayedScrollingRowIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,
                                "E R R O - Delete",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        private void bnCancelar_Click(object sender, EventArgs e)
        {
            bdsInstrumentos.Filter = @"";
            bdsOrcamentos.Filter = @"";
            bdsPedidos.Filter = @"";
            bdsServicos.Filter = @"";

            bsXOSFuncionario.CancelEdit();
            pnlConsultar.Visible = true;
            tabPrincipal.Visible = false;

            bnCancelar.Enabled = false;
            bnDescartar.Enabled = false;
            bnInserir.Enabled = true;
            bnPrimeiro.Enabled = true;
            bnAnterior.Enabled = true;

            pesquisar();

        }

        private void bnProximo_Click(object sender, EventArgs e)
        {

            DataRowView current = (DataRowView)bsXOSFuncionario.Current;
            bdsInstrumentos.Filter = @"idusuario = " + current["idusuario"].ToString();
            bdsOrcamentos.Filter = @"idusuario = " + current["idusuario"].ToString();
            bdsPedidos.Filter = @"idusuario = " + current["idusuario"].ToString();
            bdsServicos.Filter = @"idusuario = " + current["idusuario"].ToString();
        }

        private void bnAnterior_Click(object sender, EventArgs e)
        {
            DataRowView current = (DataRowView)bsXOSFuncionario.Current;
            bdsInstrumentos.Filter = @"idusuario = " + current["idusuario"].ToString();
            bdsOrcamentos.Filter = @"idusuario = " + current["idusuario"].ToString();
            bdsPedidos.Filter = @"idusuario = " + current["idusuario"].ToString();
            bdsServicos.Filter = @"idusuario = " + current["idusuario"].ToString();
        }

        private void bnUltimo_Click(object sender, EventArgs e)
        {
            DataRowView current = (DataRowView)bsXOSFuncionario.Current;
            bdsInstrumentos.Filter = @"idusuario = " + current["idusuario"].ToString();
            bdsOrcamentos.Filter = @"idusuario = " + current["idusuario"].ToString();
            bdsPedidos.Filter = @"idusuario = " + current["idusuario"].ToString();
            bdsServicos.Filter = @"idusuario = " + current["idusuario"].ToString();
        }

        private void bnPrimeiro_Click(object sender, EventArgs e)
        {
            DataRowView current = (DataRowView)bsXOSFuncionario.Current;
            bdsInstrumentos.Filter = @"idusuario = " + current["idusuario"].ToString();
            bdsOrcamentos.Filter = @"idusuario = " + current["idusuario"].ToString();
            bdsPedidos.Filter = @"idusuario = " + current["idusuario"].ToString();
            bdsServicos.Filter = @"idusuario = " + current["idusuario"].ToString();
        }
    }
}
