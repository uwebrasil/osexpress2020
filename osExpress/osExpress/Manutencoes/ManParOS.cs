﻿using System;
using System.Data;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.ViewerObjectModel;
using TExpress;

namespace osExpress.Manutencoes
{
    public partial class ManParOS : osExpress.FrmPrincipal
    {
        // memorizar usuario a deletar
        public int Delete_IdUsuario = -1;

        public ManParOS()
        {
            InitializeComponent();
            //a30PLANO_CONTABindingSource.Filter = string.Format("Posto = '{0}'", OsConfiguracoes.codigoPosto);
            #region Modificações uwe
            cbPosto001.Text = DB.GetSingleValue(@"select razao from a30postos where codigo = '001'").ToString();
            cbPosto002.Text = DB.GetSingleValue(@"select razao from a30postos where codigo = '002'").ToString();

            string f = a30PLANO_CONTABindingSource.Filter;
            a30PLANO_CONTABindingSource.Filter = f 
                    + " and " + string.Format("Posto = '{0}'", OsConfiguracoes.codigoPosto);

            //uwe 04.03.2015
            string xsql = 
                @"select distinct id,nome,Posto+' '+nome postonome,posto from A30Funcionario
                  where obs09='PESSOAS' order by nome";
            xsql = @"select id,nome  from A30Funcionario A where obs09='PESSOAS'   
                        and A.ID = (select MIN(id) from A30Funcionario where Nome=a.Nome) ";
            BindingSource bs = new BindingSource();
            var mysource = DB.GetTableFromSQL(xsql);
            bs.DataSource = mysource;
            bs.Sort = "nome";

            // Nome do funcionario
            comboBox2.DataSource = null;
            comboBox2.DataSource = bs;
            comboBox2.DisplayMember = "nome";

            // Id do funcionario
            comboBox3.DataSource = null;
            comboBox3.DataSource = bs;
            comboBox3.DisplayMember = "id";
            #endregion
        }


        private void ManParOS_Load_1(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dtsPrincipal.OsParametros' table. You can move, or remove it, as needed.
            this.osParametrosTableAdapter.Fill(this.dtsPrincipal.OsParametros);
            this.WindowState = FormWindowState.Maximized;
            //
            // TODO: This line of code loads data into the 'dtsPrincipal.A30PRODUTO' table. You can move, or remove it, as needed.
            this.a30PRODUTOTableAdapter.Fill(this.dtsPrincipal.A30PRODUTO);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsProdutosServicos' table. You can move, or remove it, as needed.
            this.osProdutosServicosTableAdapter.Fill(this.dtsPrincipal.OsProdutosServicos);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsParContas' table. You can move, or remove it, as needed.
            this.osParContasTableAdapter.Fill(this.dtsPrincipal.OsParContas);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsParContas' table. You can move, or remove it, as needed.
            this.osParContasTableAdapter.Fill(this.dtsPrincipal.OsParContas);
            // TODO: This line of code loads data into the 'dtsPrincipal.A30PLANO_CONTA' table. You can move, or remove it, as needed.
            this.a30PLANO_CONTATableAdapter.Fill(this.dtsPrincipal.A30PLANO_CONTA);
            // TODO: This line of code loads data into the 'dtsPrincipal.A30PLANO_CONTA' table. You can move, or remove it, as needed.
            this.a30PLANO_CONTATableAdapter.Fill(this.dtsPrincipal.A30PLANO_CONTA);
            // TODO: This line of code loads data into the 'dtsPrincipal.A30FUNCIONARIO' table. You can move, or remove it, as needed.
            this.a30FUNCIONARIOTableAdapter.Fill(this.dtsPrincipal.A30FUNCIONARIO);
            // TODO: This line of code loads data into the 'dtsPrincipal.A30CLIENTES' table. You can move, or remove it, as needed.
            this.a30CLIENTESTableAdapter.Fill(this.dtsPrincipal.A30CLIENTES);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsUsuario' table. You can move, or remove it, as needed.
            this.osUsuarioTableAdapter.Fill(this.dtsPrincipal.OsUsuario);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsParSituacao' table. You can move, or remove it, as needed.
            this.osParSituacaoTableAdapter.Fill(this.dtsPrincipal.OsParSituacao);

            // Inicializa os componentes.
            cmbTipoSituacao.SelectedIndex = 0;
            cmbTipoAcesso.SelectedIndex = 0;

            // Quando a tela for exibida aponta o navigator para a primeira aba.
            bindingNavigator.BindingSource = osParSituacaoBindingSource;

            //tsbGravar.Enabled = false;

            //
            if (OsConfiguracoes.tipoAcesso == char.Parse("F"))
            {
                pnlAdmnistrador1.Enabled = false;
                pnlAdmnistrador2.Enabled = false;
                tbpFormPag.Enabled = false;
                tabParametrizacao.Enabled = false;
            }

            tsbCancelar.Enabled = false;
        }


        private void tabPrincipal_Selected(object sender, TabControlEventArgs e)
        {
            tsbCancelar.PerformClick();
            
            //tsbGravar.Enabled = false;

            int abaSelecionda = tabPrincipal.SelectedIndex;

            switch (abaSelecionda)
            {
                case 0: 
                    bindingNavigator.BindingSource = osParSituacaoBindingSource;
                    tsbInserir.Enabled = true;
                    tsbExcluir.Enabled = true;
                    break;
                case 1:
                    bindingNavigator.BindingSource = osUsuarioBindingSource;
                    tsbInserir.Enabled = true;
                    tsbExcluir.Enabled = true;

                    if (OsConfiguracoes.tipoAcesso == char.Parse("F"))
                    {
                        osUsuarioBindingSource.Filter = "ID = "+OsConfiguracoes.codigoUsuario;
                        tsbInserir.Enabled = false;
                        tsbExcluir.Enabled = false;
                    }
                    SetControlsUsuario();
                    break;
                case 2:
                    bindingNavigator.BindingSource = osParContasBindingSource;
                    tsbInserir.Enabled = true;
                    tsbExcluir.Enabled = true;
                    break;
                case 3:
                    bindingNavigator.BindingSource = osProdutosServicosBindingSource;
                    tsbInserir.Enabled = false;
                    tsbExcluir.Enabled = false;
                    break;
                case 4:
                    bindingNavigator.BindingSource = osParametrosBindingSource;
                    tsbInserir.Enabled = false;
                    tsbExcluir.Enabled = false;
                    break;
                default: 
                    break;
            }
        }

        #region Helper uwe
        private int getIdPosto(string posto)
        {
            return (int) DB.GetSingleValue("select id from a30postos where codigo='"+posto+"'");       
        }

        private bool JaTemUsuarioPosto(int idusuario, string posto)
        {
            if (DB.ConnectionName.Equals(""))
                DB.ConnectionName = "osExpress.Properties.Settings.OsExpressConnectionString";
            string sql = @"select count(*) from osusuarioposto where idusuario="
                             + idusuario
                             + " and idposto=(select id from A30Postos where codigo='"+posto+"')";
            return Convert.ToInt32(DB.GetSingleValue(sql)) > 0;
        }

        private bool isValidInsertUsuario()
        {
            if (comboBox3.Text.Equals(""))
            {
                MessageBox.Show(@"ID - Usuário não válido",@"Erro INSERT",
                    MessageBoxButtons.OK,MessageBoxIcon.Stop);
                return false;
            }
            if (contaComboBox.Text.Equals(""))
            {
                MessageBox.Show(@"Conta não válido", @"Erro INSERT",
                    MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
            if (nOMETextBox3.Text.Equals(""))
            {
                MessageBox.Show(@"Nome não válido", @"Erro INSERT",
                    MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
            if (txtSenha.Text.Equals(""))
            {
                MessageBox.Show(@"Senha não válido", @"Erro INSERT",
                    MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
            if (txtConfirmaSenha.Text.Equals(""))
            {
                MessageBox.Show(@"ConfirmaSenha não válido", @"Erro INSERT",
                    MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
            bool p1 = cbPosto001.Checked;
            bool p2 = cbPosto002.Checked;
            if ( (p1==false) & (p2==false) )
            {
                MessageBox.Show(@"Favor escolher um posto", @"Erro INSERT",
                    MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
            return true;
        }

        private void confirmar(string aba, EstadoManutencao estado)
        {
            string x = estado == EstadoManutencao.smInserir
                ? " inserido com êxito! "
                : estado == EstadoManutencao.smExcluir
                    ? " excluido com êxito! "
                    : " alterado com êxito! ";      
            MessageBox.Show(aba+x , @"I n f o m a ç ã o",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        #endregion

        private void tsbGravar_Click(object sender, EventArgs e)
        {
            tsbCancelar.Enabled = false;
            tsbExcluir.Enabled = true;
            tsbInserir.Enabled = true;

            //tsbGravar.Enabled = false;
            int abaSelecionda = tabPrincipal.SelectedIndex;

            switch (abaSelecionda)
            {
                // Situação
                case 0:
                    if (estadoCorrente == EstadoManutencao.smInserir)
                    {
                        DataRowView current = (DataRowView)osParSituacaoBindingSource.Current;
                        current["TIPO_SITUACAO"] = cmbTipoSituacao.Text.Substring(0, 1);
                        current["DT_CADASTRO"] = DateTime.Now;
                    }

                    this.Validate();
                    this.osParSituacaoBindingSource.EndEdit();
                    this.tableAdapterManager.UpdateAll(this.dtsPrincipal);

                    confirmar("Situação ", estadoCorrente);
                    pesquisar();
                    break;
                // Usuário
                case 1:
                    DataRowView currentUsuario = (DataRowView)osUsuarioBindingSource.Current;

                    if (estadoCorrente == EstadoManutencao.smInserir)
                    {
                        #region Validação
                        if (!isValidInsertUsuario())
                        {
                            tsbInserir.Enabled = false;
                            tsbExcluir.Enabled = false;
                            tsbCancelar.Enabled = true;
                            break;
                        }
                        #endregion
                    }

                    if (estadoCorrente == EstadoManutencao.smExcluir)
                    {
                        if (DB.ConnectionName.Equals(""))
                            DB.ConnectionName = "osExpress.Properties.Settings.OsExpressConnectionString";
                        string delete = @"DELETE FROM OSUsuarioPosto WHERE idusuario="
                                        + Delete_IdUsuario;
                        DB.ExecuteNonQuery(delete);
                    }

                    if (estadoCorrente == EstadoManutencao.smInserir)
                    {
                        currentUsuario["ULTIMO_ACESSO"] = DateTime.Now;
                        currentUsuario["DT_CADASTRO"] = DateTime.Now;
                        // uwe 04.03.2015                        
                        //currentUsuario["IDPOSTO"] = comboBox2.Text.Substring(0, 3);
                    }
                    currentUsuario["TIPO_ACESSO"] = cmbTipoAcesso.Text.Substring(0, 1);
                    currentUsuario["SENHA"] = txtSenha.Text;
                   
                    this.Validate();
                    this.osUsuarioBindingSource.EndEdit();
                    this.tableAdapterManager.UpdateAll(this.dtsPrincipal);
                     
                    if (estadoCorrente == EstadoManutencao.smInserir)
                    {
                        int idusuario = Convert.ToInt32(currentUsuario["ID"]);
                        #region inserir osUsuarioPosto
                        if (cbPosto001.Checked)
                        {
                            if (JaTemUsuarioPosto(idusuario, "001"))
                                //MessageBox.Show(@"Posto 001 - Usuário ja existe!", @"ERRO Insert",
                                //    MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                ;
                            else
                            {
                                if (DB.ConnectionName.Equals(""))
                                    DB.ConnectionName = "osExpress.Properties.Settings.OsExpressConnectionString";
                                string insert = @"INSERT INTO OSUsuarioPosto(idusuario,idposto) VALUES("
                                                + idusuario + "," + getIdPosto("001") + ")";
                                DB.ExecuteNonQuery(insert);
                            }
                        }


                        if (cbPosto002.Checked)
                        {
                            if (JaTemUsuarioPosto(idusuario, "002"))
                                //MessageBox.Show(@"Posto 002 - Usuário ja existe!", @"ERRO Insert",
                                //    MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                ;
                            else
                            {
                                if (DB.ConnectionName.Equals(""))
                                    DB.ConnectionName = "osExpress.Properties.Settings.OsExpressConnectionString";
                                string insert = @"INSERT INTO OSUsuarioPosto(idusuario,idposto) VALUES("
                                                + idusuario + "," + getIdPosto("002") + ")";
                                DB.ExecuteNonQuery(insert);
                            }
                        }
                        #endregion
                    }
                    if (estadoCorrente == EstadoManutencao.smPesquisar)
                    {
                        #region TEM QUE SELECIONAR um posto
                        if (!cbPosto001.Checked & !cbPosto002.Checked)
                        {
                            MessageBox.Show("Nenhum posto selecionado\nGravação de posto cancelado.",@"ERRO",
                                MessageBoxButtons.OK,MessageBoxIcon.Stop);
                            break;
                        }
                        #endregion
                        int idusuario = Convert.ToInt32(currentUsuario["ID"]);
                        #region update osUsuarioPosto
                        if (cbPosto001.Checked)
                        {
                            if (JaTemUsuarioPosto(idusuario, "001"))                               
                                ;
                            else
                            {
                                if (DB.ConnectionName.Equals(""))
                                    DB.ConnectionName = "osExpress.Properties.Settings.OsExpressConnectionString";
                                string insert = @"INSERT INTO OSUsuarioPosto(idusuario,idposto) VALUES("
                                                + idusuario + "," + getIdPosto("001") + ")";
                                DB.ExecuteNonQuery(insert);
                            }
                        }
                        else // unchecked
                        {
                            // delete
                            if (JaTemUsuarioPosto(idusuario, "001"))
                            {
                                if (DB.ConnectionName.Equals(""))
                                    DB.ConnectionName = "osExpress.Properties.Settings.OsExpressConnectionString";
                                string delete = @"DELETE FROM OSUsuarioPosto WHERE idusuario="
                                               + idusuario + " and idposto=" + getIdPosto("001");
                                DB.ExecuteNonQuery(delete);
                            }
                        }
                        if (cbPosto002.Checked)
                        {
                            if (JaTemUsuarioPosto(idusuario, "002"))                               
                                ;
                            else
                            {
                                if (DB.ConnectionName.Equals(""))
                                    DB.ConnectionName = "osExpress.Properties.Settings.OsExpressConnectionString";
                                string insert = @"INSERT INTO OSUsuarioPosto(idusuario,idposto) VALUES("
                                                + idusuario + "," + getIdPosto("002") + ")";
                                DB.ExecuteNonQuery(insert);
                            }
                        }
                        else // unchecked
                        {
                            // delete
                            if (JaTemUsuarioPosto(idusuario, "002"))
                            {
                                if (DB.ConnectionName.Equals(""))
                                    DB.ConnectionName = "osExpress.Properties.Settings.OsExpressConnectionString";
                                string delete = @"DELETE FROM OSUsuarioPosto WHERE idusuario="
                                               + idusuario + " and idposto=" + getIdPosto("002");
                                DB.ExecuteNonQuery(delete);
                            }
                        }
                        #endregion
                    }

                    confirmar("Usuário ", estadoCorrente);
                    // 09.03.2015 uwe: Êxito insert -> voltar ao estado pesquisar ...                   
                    pesquisar();
                    break;
                // Contas
                case 2:
                    this.Validate();
                    this.osParContasBindingSource.EndEdit();
                    this.osParContasTableAdapter.Update(this.dtsPrincipal);
                    confirmar("Conta ", estadoCorrente);
                    pesquisar();
                    break;
                case 3:
                    this.Validate();
                    this.osProdutosServicosBindingSource.EndEdit();
                    this.osProdutosServicosTableAdapter.Update(this.dtsPrincipal);
                    tsbInserir.Enabled = false;
                    tsbExcluir.Enabled = false;
                    confirmar("Parametrização ", estadoCorrente);
                    pesquisar();
                    break;
                case 4:
                    this.Validate();
                    this.osParametrosBindingSource.EndEdit();
                    this.osParametrosTableAdapter.Update(this.dtsPrincipal);
                    tsbInserir.Enabled = false;
                    tsbExcluir.Enabled = false;
                    confirmar("Template ", estadoCorrente);
                    pesquisar();
                    break;
                // Outras
                default:
                    break;
            }

            
        }

        private void tsbInserir_Click(object sender, EventArgs e)
        {
            inserir();
            tsbCancelar.Enabled = true;
            tsbExcluir.Enabled = false;
            tsbInserir.Enabled = false;
            //tsbGravar.Enabled = true;
            tsbAnterior.Enabled = false;
            tsbPrimeiro.Enabled = false;
            tsbProximo.Enabled = false;
            tsbUltimo.Enabled = false;

            int abaSelecionda = tabPrincipal.SelectedIndex;

            switch (abaSelecionda)
            {
                // Situação
                case 0:

                    break;
                // Etiqueta -> USUARIO!! uwe 
                case 1:
                    cbPosto001.Checked = false;
                    cbPosto002.Checked = false;
                    break;
                // Usuário
                case 2:
                    
                    break;
                // Contas
                case 3:

                    break;
                case 4:

                    break;
                case 5:

                    break;
                // Outras
                default:
                    break;
            }
        }


        private void tsbExcluir_Click(object sender, EventArgs e)
        {            
            if (MessageBox.Show("Deseja Realmente Excluir?",
                                null,
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question) == DialogResult.Yes)
            {
                excluir();
                //tsbGravar.Enabled = true;
                //estadoCorrente = EstadoManutencao.smPesquisar;
            }
        }


        private void btnSituacao_Click(object sender, EventArgs e)
        {
            tsbGravar_Click(sender, e);
        }


        public void TplValidaCampoTexto(object sender, EventArgs e)
        {
            /*TextBox userInput;
            userInput = sender as TextBox;

            if (userInput.Text == "")
            {
                MessageBox.Show("Atenção! Campo Obrigatório");
                userInput.Focus();
            }*/
        }

        private void txtConfirmaSenha_Validated(object sender, EventArgs e)
        {
            TplValidaCampoTexto(sender, e);
            if (txtSenha.Text != txtConfirmaSenha.Text)
            {
                lblSenhaNaoConfere.Visible = true;
                lblSenhaNaoConfere.Text = "Atenção! Senha não confere.";
            }
            else 
                lblSenhaNaoConfere.Visible = false;
        }


        private void txtSenha_Validated(object sender, EventArgs e)
        {
            TplValidaCampoTexto(sender, e);
            if (txtSenha.Text.Length < 5)
            {
                lblSenhaDigitos.Visible = true;
                lblSenhaDigitos.Text = "Atenção! A senha deve conter no mínimo 5 dígitos.";
            } 
            else
                lblSenhaDigitos.Visible = false;
        }


        private void tsbCancelar_Click(object sender, EventArgs e)
        {
            tsbCancelar.Enabled = false;
            tsbInserir.Enabled = true;
            tsbExcluir.Enabled = true;

            int abaSelecionda = tabPrincipal.SelectedIndex;

            osParSituacaoBindingSource.CancelEdit();

            osUsuarioBindingSource.CancelEdit();

            osParContasBindingSource.CancelEdit();

            osProdutosServicosBindingSource.CancelEdit();

            osParametrosBindingSource.CancelEdit();
        }

        private void osParSituacaoBindingSource_CurrentChanged(object sender, EventArgs e)
        {
            DataRowView current = (DataRowView)osParSituacaoBindingSource.Current;

            String situacao = current["TIPO_SITUACAO"].ToString();

            if (situacao == "S")
            {
                cmbTipoSituacao.Text = "S - Solicitada";
            }
            else if (situacao == "E")
            {
                cmbTipoSituacao.Text = "E - Encontrada";
            }
            else if (situacao == "R")
            {
                cmbTipoSituacao.Text = "R - Realizada";
            }
            else if (situacao == "F")
            {
                cmbTipoSituacao.Text = "F - Final";
            }
        }


        private void osUsuarioBindingSource_CurrentChanged(object sender, EventArgs e)
        {
            DataRowView current = (DataRowView)osUsuarioBindingSource.Current;

            if (current["TIPO_ACESSO"].ToString() == "A")
            {
                cmbTipoAcesso.Text = "A - Administrador";
            }
            else
            {
                cmbTipoAcesso.Text = "F - Funcionário";
            }
        }

         /// <summary>
        /// Uwe:
        /// Handles the CellClick event of the osUsuarioDataGridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DataGridViewCellEventArgs"/> instance containing the event data.</param>
        private void osUsuarioDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {           
            SetControlsUsuario();        

            // corrigir Comboboxes Conta/Descricao manualmente
            DataRowView currentU = (DataRowView)osUsuarioBindingSource.Current;
            int id_pconta = Convert.ToInt32(currentU["plano_conta"]);

            string sql = @"Select conta,descricao from a30plano_conta where id="+id_pconta;
            DataTable res = DB.GetTableFromSQL(sql);
            string conta = res.Rows[0]["conta"].ToString();
            string desc  = res.Rows[0]["descricao"].ToString();

            contaComboBox.Text = conta;
            comboBox4.Text = desc;

        }
        /// <summary>
        /// Uwe:Inicializar Checkboxes, memorizar currentUsuario
        /// </summary>
        private void SetControlsUsuario()
        {
            DataRowView current = (DataRowView)osUsuarioBindingSource.Current;
            int idusuario = Convert.ToInt32(current["ID"]);
            Delete_IdUsuario = idusuario;

            if (DB.ConnectionName.Equals(""))
                DB.ConnectionName = "osExpress.Properties.Settings.OsExpressConnectionString";
            string sql = @"select idposto from osusuarioposto where idusuario=" + idusuario;
            var postos = DB.GetList<int>(sql);
            cbPosto001.Checked = false;
            cbPosto002.Checked = false;
            foreach (var posto in postos)
            {
                var codigo = DB.GetSingleValue("select codigo from a30postos where id=" + posto).ToString();
                if (codigo.Equals("001")) cbPosto001.Checked = true;
                if (codigo.Equals("002")) cbPosto002.Checked = true;
            }
        }        
       
    }
}
