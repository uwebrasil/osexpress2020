﻿namespace osExpress.Manutencoes
{
    partial class ManRede
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.RedeBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bnInserir = new System.Windows.Forms.ToolStripButton();
            this.OsClienteRedeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dtsRede = new osExpress.Manutencoes.OsClienteRede();
            this.bnCount = new System.Windows.Forms.ToolStripLabel();
            this.bnConsultar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.bnGravar = new System.Windows.Forms.ToolStripButton();
            this.bnCancelar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.bnPrimeiro = new System.Windows.Forms.ToolStripButton();
            this.bnAnterior = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bnPosition = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bnProximo = new System.Windows.Forms.ToolStripButton();
            this.bnUltimo = new System.Windows.Forms.ToolStripButton();
            this.bnDescartar = new System.Windows.Forms.ToolStripButton();
            this.tableLayoutPanelConsultar = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.headLabel = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutFilter = new System.Windows.Forms.TableLayoutPanel();
            this.cbFiltroAtivo = new System.Windows.Forms.CheckBox();
            this.flowLayoutPanelFiltroNome = new System.Windows.Forms.FlowLayoutPanel();
            this.labelFiltroNome = new System.Windows.Forms.Label();
            this.tbFiltroNome = new System.Windows.Forms.TextBox();
            this.dgvRede = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idredeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomeredeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel4ConsultarBottom = new System.Windows.Forms.Panel();
            this.osClienteRedeTableAdapter = new osExpress.Manutencoes.OsClienteRedeTableAdapters.OsClienteRedeTableAdapter();
            this.tableLayoutPanelEditar = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanelEditar = new System.Windows.Forms.FlowLayoutPanel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.headLabelEditar = new System.Windows.Forms.Label();
            this.panel2Editar = new System.Windows.Forms.Panel();
            this.lblTrackBar = new System.Windows.Forms.Label();
            this.tbTrackValue = new System.Windows.Forms.TextBox();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.btnResizeColWidth = new System.Windows.Forms.Button();
            this.tabControlEditar = new System.Windows.Forms.TabControl();
            this.tabPage1Dados = new System.Windows.Forms.TabPage();
            this.tableLayoutPanelDados = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelRow0Col1 = new System.Windows.Forms.TableLayoutPanel();
            this.labelRedeId = new System.Windows.Forms.Label();
            this.tbRedeId = new System.Windows.Forms.TextBox();
            this.tableLayoutPanelNome = new System.Windows.Forms.TableLayoutPanel();
            this.labelRedeNome = new System.Windows.Forms.Label();
            this.tbRedeNome = new System.Windows.Forms.TextBox();
            this.panelFillerRow1 = new System.Windows.Forms.Panel();
            this.panelFillRow1ColLast = new System.Windows.Forms.Panel();
            this.tableLayoutPanelObs = new System.Windows.Forms.TableLayoutPanel();
            this.labelObservacao = new System.Windows.Forms.Label();
            this.tbObservacao = new System.Windows.Forms.TextBox();
            this.panel4Editar = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.dtsPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RedeBindingNavigator)).BeginInit();
            this.RedeBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OsClienteRedeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtsRede)).BeginInit();
            this.tableLayoutPanelConsultar.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tableLayoutFilter.SuspendLayout();
            this.flowLayoutPanelFiltroNome.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRede)).BeginInit();
            this.tableLayoutPanelEditar.SuspendLayout();
            this.flowLayoutPanelEditar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel2Editar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.tabControlEditar.SuspendLayout();
            this.tabPage1Dados.SuspendLayout();
            this.tableLayoutPanelDados.SuspendLayout();
            this.tableLayoutPanelRow0Col1.SuspendLayout();
            this.tableLayoutPanelNome.SuspendLayout();
            this.tableLayoutPanelObs.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpPrincipal
            // 
            this.grpPrincipal.Location = new System.Drawing.Point(900, 87);
            this.grpPrincipal.Visible = false;
            // 
            // tabPrincipal
            // 
            this.tabPrincipal.Location = new System.Drawing.Point(900, 187);
            this.tabPrincipal.Visible = false;
            // 
            // RedeBindingNavigator
            // 
            this.RedeBindingNavigator.AddNewItem = this.bnInserir;
            this.RedeBindingNavigator.BindingSource = this.OsClienteRedeBindingSource;
            this.RedeBindingNavigator.CountItem = this.bnCount;
            this.RedeBindingNavigator.CountItemFormat = "de {0}";
            this.RedeBindingNavigator.DeleteItem = null;
            this.RedeBindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.RedeBindingNavigator.ImageScalingSize = new System.Drawing.Size(48, 48);
            this.RedeBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bnConsultar,
            this.toolStripSeparator3,
            this.bnInserir,
            this.bnGravar,
            this.bnCancelar,
            this.toolStripSeparator4,
            this.bnPrimeiro,
            this.bnAnterior,
            this.toolStripSeparator1,
            this.bnPosition,
            this.bnCount,
            this.toolStripSeparator2,
            this.bnProximo,
            this.bnUltimo,
            this.bnDescartar});
            this.RedeBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.RedeBindingNavigator.MoveFirstItem = this.bnPrimeiro;
            this.RedeBindingNavigator.MoveLastItem = this.bnUltimo;
            this.RedeBindingNavigator.MoveNextItem = this.bnProximo;
            this.RedeBindingNavigator.MovePreviousItem = this.bnAnterior;
            this.RedeBindingNavigator.Name = "RedeBindingNavigator";
            this.RedeBindingNavigator.PositionItem = this.bnPosition;
            this.RedeBindingNavigator.Size = new System.Drawing.Size(959, 70);
            this.RedeBindingNavigator.TabIndex = 15;
            this.RedeBindingNavigator.Text = "ProdutoBindingNavigator";
            // 
            // bnInserir
            // 
            this.bnInserir.Image = global::osExpress.Properties.Resources.insert;
            this.bnInserir.Name = "bnInserir";
            this.bnInserir.RightToLeftAutoMirrorImage = true;
            this.bnInserir.Size = new System.Drawing.Size(52, 67);
            this.bnInserir.Text = "&Inserir";
            this.bnInserir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnInserir.Click += new System.EventHandler(this.bnInserir_Click);
            // 
            // OsClienteRedeBindingSource
            // 
            this.OsClienteRedeBindingSource.DataMember = "OsClienteRede";
            this.OsClienteRedeBindingSource.DataSource = this.dtsRede;
            // 
            // dtsRede
            // 
            this.dtsRede.DataSetName = "OsClienteRede";
            this.dtsRede.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bnCount
            // 
            this.bnCount.Name = "bnCount";
            this.bnCount.Size = new System.Drawing.Size(37, 67);
            this.bnCount.Text = "de {0}";
            this.bnCount.ToolTipText = "Total number of items";
            // 
            // bnConsultar
            // 
            this.bnConsultar.Image = global::osExpress.Properties.Resources.search;
            this.bnConsultar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnConsultar.Name = "bnConsultar";
            this.bnConsultar.Size = new System.Drawing.Size(62, 67);
            this.bnConsultar.Text = "Consultar";
            this.bnConsultar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnConsultar.Click += new System.EventHandler(this.bnConsultar_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 70);
            // 
            // bnGravar
            // 
            this.bnGravar.Image = global::osExpress.Properties.Resources.save;
            this.bnGravar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnGravar.Name = "bnGravar";
            this.bnGravar.Size = new System.Drawing.Size(52, 67);
            this.bnGravar.Text = "Grav&ar";
            this.bnGravar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnGravar.Click += new System.EventHandler(this.bnGravar_Click);
            // 
            // bnCancelar
            // 
            this.bnCancelar.Enabled = false;
            this.bnCancelar.Image = global::osExpress.Properties.Resources.clean;
            this.bnCancelar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnCancelar.Name = "bnCancelar";
            this.bnCancelar.Size = new System.Drawing.Size(57, 67);
            this.bnCancelar.Text = "Cancelar";
            this.bnCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnCancelar.Click += new System.EventHandler(this.bnCancelar_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 70);
            // 
            // bnPrimeiro
            // 
            this.bnPrimeiro.Image = global::osExpress.Properties.Resources.first;
            this.bnPrimeiro.Name = "bnPrimeiro";
            this.bnPrimeiro.RightToLeftAutoMirrorImage = true;
            this.bnPrimeiro.Size = new System.Drawing.Size(56, 67);
            this.bnPrimeiro.Text = "&Primeiro";
            this.bnPrimeiro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnPrimeiro.Click += new System.EventHandler(this.bnPrimeiro_Click);
            // 
            // bnAnterior
            // 
            this.bnAnterior.Image = global::osExpress.Properties.Resources.previous;
            this.bnAnterior.Name = "bnAnterior";
            this.bnAnterior.RightToLeftAutoMirrorImage = true;
            this.bnAnterior.Size = new System.Drawing.Size(54, 67);
            this.bnAnterior.Text = "Anteri&or";
            this.bnAnterior.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnAnterior.Click += new System.EventHandler(this.bnAnterior_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 70);
            // 
            // bnPosition
            // 
            this.bnPosition.AccessibleName = "Position";
            this.bnPosition.AutoSize = false;
            this.bnPosition.Name = "bnPosition";
            this.bnPosition.Size = new System.Drawing.Size(50, 23);
            this.bnPosition.Text = "0";
            this.bnPosition.ToolTipText = "Current position";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 70);
            // 
            // bnProximo
            // 
            this.bnProximo.Image = global::osExpress.Properties.Resources.next;
            this.bnProximo.Name = "bnProximo";
            this.bnProximo.RightToLeftAutoMirrorImage = true;
            this.bnProximo.Size = new System.Drawing.Size(56, 67);
            this.bnProximo.Text = "Próxi&mo";
            this.bnProximo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnProximo.Click += new System.EventHandler(this.bnProximo_Click);
            // 
            // bnUltimo
            // 
            this.bnUltimo.Image = global::osExpress.Properties.Resources.last;
            this.bnUltimo.Name = "bnUltimo";
            this.bnUltimo.RightToLeftAutoMirrorImage = true;
            this.bnUltimo.Size = new System.Drawing.Size(52, 67);
            this.bnUltimo.Text = "&Último";
            this.bnUltimo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnUltimo.Click += new System.EventHandler(this.bnUltimo_Click);
            // 
            // bnDescartar
            // 
            this.bnDescartar.Enabled = false;
            this.bnDescartar.Image = global::osExpress.Properties.Resources.remove;
            this.bnDescartar.Name = "bnDescartar";
            this.bnDescartar.RightToLeftAutoMirrorImage = true;
            this.bnDescartar.Size = new System.Drawing.Size(60, 67);
            this.bnDescartar.Text = "&Descartar";
            this.bnDescartar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnDescartar.Click += new System.EventHandler(this.bnDescartar_Click);
            // 
            // tableLayoutPanelConsultar
            // 
            this.tableLayoutPanelConsultar.BackColor = System.Drawing.Color.BurlyWood;
            this.tableLayoutPanelConsultar.ColumnCount = 1;
            this.tableLayoutPanelConsultar.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelConsultar.Controls.Add(this.flowLayoutPanel1, 0, 0);
            this.tableLayoutPanelConsultar.Controls.Add(this.groupBox1, 0, 1);
            this.tableLayoutPanelConsultar.Controls.Add(this.dgvRede, 0, 2);
            this.tableLayoutPanelConsultar.Controls.Add(this.panel4ConsultarBottom, 0, 3);
            this.tableLayoutPanelConsultar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelConsultar.Location = new System.Drawing.Point(0, 70);
            this.tableLayoutPanelConsultar.Name = "tableLayoutPanelConsultar";
            this.tableLayoutPanelConsultar.RowCount = 4;
            this.tableLayoutPanelConsultar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13F));
            this.tableLayoutPanelConsultar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanelConsultar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 67F));
            this.tableLayoutPanelConsultar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanelConsultar.Size = new System.Drawing.Size(959, 419);
            this.tableLayoutPanelConsultar.TabIndex = 16;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.flowLayoutPanel1.Controls.Add(this.pictureBox1);
            this.flowLayoutPanel1.Controls.Add(this.headLabel);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(308, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(342, 48);
            this.flowLayoutPanel1.TabIndex = 18;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::osExpress.Properties.Resources.search;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(51, 50);
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            // 
            // headLabel
            // 
            this.headLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.headLabel.AutoSize = true;
            this.headLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.headLabel.Location = new System.Drawing.Point(60, 18);
            this.headLabel.Name = "headLabel";
            this.headLabel.Size = new System.Drawing.Size(258, 20);
            this.headLabel.TabIndex = 0;
            this.headLabel.Text = "Manutenção Redes - Consultar";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutFilter);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 57);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(953, 35);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filtro";
            // 
            // tableLayoutFilter
            // 
            this.tableLayoutFilter.BackColor = System.Drawing.Color.Chocolate;
            this.tableLayoutFilter.ColumnCount = 2;
            this.tableLayoutFilter.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutFilter.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutFilter.Controls.Add(this.cbFiltroAtivo, 0, 0);
            this.tableLayoutFilter.Controls.Add(this.flowLayoutPanelFiltroNome, 1, 0);
            this.tableLayoutFilter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutFilter.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutFilter.Name = "tableLayoutFilter";
            this.tableLayoutFilter.RowCount = 1;
            this.tableLayoutFilter.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutFilter.Size = new System.Drawing.Size(947, 16);
            this.tableLayoutFilter.TabIndex = 21;
            // 
            // cbFiltroAtivo
            // 
            this.cbFiltroAtivo.AutoSize = true;
            this.cbFiltroAtivo.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbFiltroAtivo.Checked = true;
            this.cbFiltroAtivo.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbFiltroAtivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFiltroAtivo.Location = new System.Drawing.Point(3, 8);
            this.cbFiltroAtivo.Margin = new System.Windows.Forms.Padding(3, 8, 3, 3);
            this.cbFiltroAtivo.Name = "cbFiltroAtivo";
            this.cbFiltroAtivo.Size = new System.Drawing.Size(61, 5);
            this.cbFiltroAtivo.TabIndex = 0;
            this.cbFiltroAtivo.Text = "ativo?";
            this.cbFiltroAtivo.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanelFiltroNome
            // 
            this.flowLayoutPanelFiltroNome.Controls.Add(this.labelFiltroNome);
            this.flowLayoutPanelFiltroNome.Controls.Add(this.tbFiltroNome);
            this.flowLayoutPanelFiltroNome.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanelFiltroNome.Location = new System.Drawing.Point(192, 3);
            this.flowLayoutPanelFiltroNome.Name = "flowLayoutPanelFiltroNome";
            this.flowLayoutPanelFiltroNome.Size = new System.Drawing.Size(752, 10);
            this.flowLayoutPanelFiltroNome.TabIndex = 1;
            // 
            // labelFiltroNome
            // 
            this.labelFiltroNome.AutoSize = true;
            this.labelFiltroNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFiltroNome.Location = new System.Drawing.Point(3, 6);
            this.labelFiltroNome.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.labelFiltroNome.Name = "labelFiltroNome";
            this.labelFiltroNome.Size = new System.Drawing.Size(57, 13);
            this.labelFiltroNome.TabIndex = 0;
            this.labelFiltroNome.Text = "%Nome%";
            // 
            // tbFiltroNome
            // 
            this.tbFiltroNome.Location = new System.Drawing.Point(66, 3);
            this.tbFiltroNome.Name = "tbFiltroNome";
            this.tbFiltroNome.Size = new System.Drawing.Size(386, 20);
            this.tbFiltroNome.TabIndex = 1;
            // 
            // dgvRede
            // 
            this.dgvRede.AllowUserToAddRows = false;
            this.dgvRede.AllowUserToDeleteRows = false;
            this.dgvRede.AutoGenerateColumns = false;
            this.dgvRede.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvRede.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRede.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.idredeDataGridViewTextBoxColumn,
            this.nomeredeDataGridViewTextBoxColumn,
            this.obsDataGridViewTextBoxColumn});
            this.dgvRede.DataSource = this.OsClienteRedeBindingSource;
            this.dgvRede.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRede.Location = new System.Drawing.Point(3, 98);
            this.dgvRede.Name = "dgvRede";
            this.dgvRede.ReadOnly = true;
            this.dgvRede.Size = new System.Drawing.Size(953, 274);
            this.dgvRede.TabIndex = 19;
            this.dgvRede.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvRede_CellDoubleClick);
            this.dgvRede.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvRede_KeyDown);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn.HeaderText = "id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // idredeDataGridViewTextBoxColumn
            // 
            this.idredeDataGridViewTextBoxColumn.DataPropertyName = "id_rede";
            this.idredeDataGridViewTextBoxColumn.HeaderText = "id_rede";
            this.idredeDataGridViewTextBoxColumn.Name = "idredeDataGridViewTextBoxColumn";
            this.idredeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nomeredeDataGridViewTextBoxColumn
            // 
            this.nomeredeDataGridViewTextBoxColumn.DataPropertyName = "nome_rede";
            this.nomeredeDataGridViewTextBoxColumn.HeaderText = "nome_rede";
            this.nomeredeDataGridViewTextBoxColumn.Name = "nomeredeDataGridViewTextBoxColumn";
            this.nomeredeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obsDataGridViewTextBoxColumn
            // 
            this.obsDataGridViewTextBoxColumn.DataPropertyName = "obs";
            this.obsDataGridViewTextBoxColumn.HeaderText = "obs";
            this.obsDataGridViewTextBoxColumn.Name = "obsDataGridViewTextBoxColumn";
            this.obsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // panel4ConsultarBottom
            // 
            this.panel4ConsultarBottom.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel4ConsultarBottom.BackColor = System.Drawing.Color.ForestGreen;
            this.panel4ConsultarBottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4ConsultarBottom.Location = new System.Drawing.Point(3, 378);
            this.panel4ConsultarBottom.Name = "panel4ConsultarBottom";
            this.panel4ConsultarBottom.Size = new System.Drawing.Size(953, 38);
            this.panel4ConsultarBottom.TabIndex = 23;
            // 
            // osClienteRedeTableAdapter
            // 
            this.osClienteRedeTableAdapter.ClearBeforeFill = true;
            // 
            // tableLayoutPanelEditar
            // 
            this.tableLayoutPanelEditar.BackColor = System.Drawing.Color.BurlyWood;
            this.tableLayoutPanelEditar.ColumnCount = 1;
            this.tableLayoutPanelEditar.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelEditar.Controls.Add(this.flowLayoutPanelEditar, 0, 0);
            this.tableLayoutPanelEditar.Controls.Add(this.panel2Editar, 0, 1);
            this.tableLayoutPanelEditar.Controls.Add(this.tabControlEditar, 0, 2);
            this.tableLayoutPanelEditar.Controls.Add(this.panel4Editar, 0, 3);
            this.tableLayoutPanelEditar.Location = new System.Drawing.Point(10, 100);
            this.tableLayoutPanelEditar.Name = "tableLayoutPanelEditar";
            this.tableLayoutPanelEditar.RowCount = 4;
            this.tableLayoutPanelEditar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanelEditar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanelEditar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanelEditar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanelEditar.Size = new System.Drawing.Size(978, 619);
            this.tableLayoutPanelEditar.TabIndex = 17;
            // 
            // flowLayoutPanelEditar
            // 
            this.flowLayoutPanelEditar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.flowLayoutPanelEditar.Controls.Add(this.pictureBox2);
            this.flowLayoutPanelEditar.Controls.Add(this.headLabelEditar);
            this.flowLayoutPanelEditar.Location = new System.Drawing.Point(318, 4);
            this.flowLayoutPanelEditar.Name = "flowLayoutPanelEditar";
            this.flowLayoutPanelEditar.Size = new System.Drawing.Size(342, 53);
            this.flowLayoutPanelEditar.TabIndex = 19;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::osExpress.Properties.Resources.edit2;
            this.pictureBox2.Location = new System.Drawing.Point(3, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(51, 50);
            this.pictureBox2.TabIndex = 17;
            this.pictureBox2.TabStop = false;
            // 
            // headLabelEditar
            // 
            this.headLabelEditar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.headLabelEditar.AutoSize = true;
            this.headLabelEditar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.headLabelEditar.Location = new System.Drawing.Point(60, 18);
            this.headLabelEditar.Name = "headLabelEditar";
            this.headLabelEditar.Size = new System.Drawing.Size(220, 20);
            this.headLabelEditar.TabIndex = 0;
            this.headLabelEditar.Text = "Manutenção Rede - Editar";
            // 
            // panel2Editar
            // 
            this.panel2Editar.BackColor = System.Drawing.Color.ForestGreen;
            this.panel2Editar.Controls.Add(this.lblTrackBar);
            this.panel2Editar.Controls.Add(this.tbTrackValue);
            this.panel2Editar.Controls.Add(this.trackBar1);
            this.panel2Editar.Controls.Add(this.btnResizeColWidth);
            this.panel2Editar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2Editar.Location = new System.Drawing.Point(3, 64);
            this.panel2Editar.Name = "panel2Editar";
            this.panel2Editar.Size = new System.Drawing.Size(972, 55);
            this.panel2Editar.TabIndex = 20;
            // 
            // lblTrackBar
            // 
            this.lblTrackBar.AutoSize = true;
            this.lblTrackBar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrackBar.Location = new System.Drawing.Point(8, 5);
            this.lblTrackBar.Name = "lblTrackBar";
            this.lblTrackBar.Size = new System.Drawing.Size(144, 13);
            this.lblTrackBar.TabIndex = 3;
            this.lblTrackBar.Text = "ajuste da espaciosidade";
            this.lblTrackBar.Visible = false;
            // 
            // tbTrackValue
            // 
            this.tbTrackValue.Location = new System.Drawing.Point(329, 5);
            this.tbTrackValue.Name = "tbTrackValue";
            this.tbTrackValue.ReadOnly = true;
            this.tbTrackValue.Size = new System.Drawing.Size(40, 20);
            this.tbTrackValue.TabIndex = 2;
            this.tbTrackValue.Text = "0";
            this.tbTrackValue.Visible = false;
            // 
            // trackBar1
            // 
            this.trackBar1.Location = new System.Drawing.Point(158, 0);
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(104, 45);
            this.trackBar1.TabIndex = 1;
            this.trackBar1.TickFrequency = 10;
            this.trackBar1.Visible = false;
            // 
            // btnResizeColWidth
            // 
            this.btnResizeColWidth.Location = new System.Drawing.Point(683, -1);
            this.btnResizeColWidth.Name = "btnResizeColWidth";
            this.btnResizeColWidth.Size = new System.Drawing.Size(75, 23);
            this.btnResizeColWidth.TabIndex = 0;
            this.btnResizeColWidth.Text = "button1";
            this.btnResizeColWidth.UseVisualStyleBackColor = true;
            this.btnResizeColWidth.Visible = false;
            // 
            // tabControlEditar
            // 
            this.tabControlEditar.Controls.Add(this.tabPage1Dados);
            this.tabControlEditar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlEditar.Location = new System.Drawing.Point(3, 125);
            this.tabControlEditar.Name = "tabControlEditar";
            this.tabControlEditar.SelectedIndex = 0;
            this.tabControlEditar.Size = new System.Drawing.Size(972, 427);
            this.tabControlEditar.TabIndex = 21;
            this.tabControlEditar.TabStop = false;
            // 
            // tabPage1Dados
            // 
            this.tabPage1Dados.Controls.Add(this.tableLayoutPanelDados);
            this.tabPage1Dados.Location = new System.Drawing.Point(4, 22);
            this.tabPage1Dados.Name = "tabPage1Dados";
            this.tabPage1Dados.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1Dados.Size = new System.Drawing.Size(964, 401);
            this.tabPage1Dados.TabIndex = 0;
            this.tabPage1Dados.Text = "Dados";
            this.tabPage1Dados.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanelDados
            // 
            this.tableLayoutPanelDados.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanelDados.ColumnCount = 20;
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanelDados.Controls.Add(this.tableLayoutPanelRow0Col1, 1, 0);
            this.tableLayoutPanelDados.Controls.Add(this.tableLayoutPanelNome, 5, 0);
            this.tableLayoutPanelDados.Controls.Add(this.panelFillerRow1, 0, 0);
            this.tableLayoutPanelDados.Controls.Add(this.panelFillRow1ColLast, 19, 0);
            this.tableLayoutPanelDados.Controls.Add(this.tableLayoutPanelObs, 1, 1);
            this.tableLayoutPanelDados.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelDados.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanelDados.Name = "tableLayoutPanelDados";
            this.tableLayoutPanelDados.RowCount = 4;
            this.tableLayoutPanelDados.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelDados.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelDados.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelDados.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelDados.Size = new System.Drawing.Size(958, 395);
            this.tableLayoutPanelDados.TabIndex = 0;
            // 
            // tableLayoutPanelRow0Col1
            // 
            this.tableLayoutPanelRow0Col1.BackColor = System.Drawing.Color.LightGray;
            this.tableLayoutPanelRow0Col1.ColumnCount = 1;
            this.tableLayoutPanelDados.SetColumnSpan(this.tableLayoutPanelRow0Col1, 3);
            this.tableLayoutPanelRow0Col1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelRow0Col1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelRow0Col1.Controls.Add(this.labelRedeId, 0, 0);
            this.tableLayoutPanelRow0Col1.Controls.Add(this.tbRedeId, 0, 1);
            this.tableLayoutPanelRow0Col1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelRow0Col1.Location = new System.Drawing.Point(51, 4);
            this.tableLayoutPanelRow0Col1.Name = "tableLayoutPanelRow0Col1";
            this.tableLayoutPanelRow0Col1.RowCount = 2;
            this.tableLayoutPanelRow0Col1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelRow0Col1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanelRow0Col1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelRow0Col1.Size = new System.Drawing.Size(134, 91);
            this.tableLayoutPanelRow0Col1.TabIndex = 2;
            // 
            // labelRedeId
            // 
            this.labelRedeId.AutoSize = true;
            this.labelRedeId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRedeId.Location = new System.Drawing.Point(3, 0);
            this.labelRedeId.Name = "labelRedeId";
            this.labelRedeId.Size = new System.Drawing.Size(52, 13);
            this.labelRedeId.TabIndex = 0;
            this.labelRedeId.Text = "Rede Id";
            // 
            // tbRedeId
            // 
            this.tbRedeId.AcceptsTab = true;
            this.tbRedeId.BackColor = System.Drawing.SystemColors.Window;
            this.tbRedeId.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbRedeId.Location = new System.Drawing.Point(3, 21);
            this.tbRedeId.MaxLength = 50;
            this.tbRedeId.Name = "tbRedeId";
            this.tbRedeId.Size = new System.Drawing.Size(128, 20);
            this.tbRedeId.TabIndex = 1;
            this.tbRedeId.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbRedeId_KeyPress);
            // 
            // tableLayoutPanelNome
            // 
            this.tableLayoutPanelNome.BackColor = System.Drawing.Color.Silver;
            this.tableLayoutPanelNome.ColumnCount = 1;
            this.tableLayoutPanelDados.SetColumnSpan(this.tableLayoutPanelNome, 9);
            this.tableLayoutPanelNome.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelNome.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelNome.Controls.Add(this.labelRedeNome, 0, 0);
            this.tableLayoutPanelNome.Controls.Add(this.tbRedeNome, 0, 1);
            this.tableLayoutPanelNome.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelNome.Location = new System.Drawing.Point(239, 4);
            this.tableLayoutPanelNome.Name = "tableLayoutPanelNome";
            this.tableLayoutPanelNome.RowCount = 2;
            this.tableLayoutPanelNome.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelNome.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanelNome.Size = new System.Drawing.Size(416, 91);
            this.tableLayoutPanelNome.TabIndex = 2;
            // 
            // labelRedeNome
            // 
            this.labelRedeNome.AutoSize = true;
            this.labelRedeNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRedeNome.Location = new System.Drawing.Point(3, 0);
            this.labelRedeNome.Name = "labelRedeNome";
            this.labelRedeNome.Size = new System.Drawing.Size(73, 13);
            this.labelRedeNome.TabIndex = 0;
            this.labelRedeNome.Text = "Rede Nome";
            // 
            // tbRedeNome
            // 
            this.tbRedeNome.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbRedeNome.Location = new System.Drawing.Point(3, 21);
            this.tbRedeNome.MaxLength = 100;
            this.tbRedeNome.Name = "tbRedeNome";
            this.tbRedeNome.Size = new System.Drawing.Size(410, 20);
            this.tbRedeNome.TabIndex = 2;
            // 
            // panelFillerRow1
            // 
            this.panelFillerRow1.BackColor = System.Drawing.Color.DarkRed;
            this.panelFillerRow1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelFillerRow1.Location = new System.Drawing.Point(4, 4);
            this.panelFillerRow1.Name = "panelFillerRow1";
            this.tableLayoutPanelDados.SetRowSpan(this.panelFillerRow1, 3);
            this.panelFillerRow1.Size = new System.Drawing.Size(40, 287);
            this.panelFillerRow1.TabIndex = 9;
            // 
            // panelFillRow1ColLast
            // 
            this.panelFillRow1ColLast.BackColor = System.Drawing.Color.DarkRed;
            this.panelFillRow1ColLast.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelFillRow1ColLast.Location = new System.Drawing.Point(897, 4);
            this.panelFillRow1ColLast.Name = "panelFillRow1ColLast";
            this.tableLayoutPanelDados.SetRowSpan(this.panelFillRow1ColLast, 3);
            this.panelFillRow1ColLast.Size = new System.Drawing.Size(57, 287);
            this.panelFillRow1ColLast.TabIndex = 10;
            // 
            // tableLayoutPanelObs
            // 
            this.tableLayoutPanelObs.BackColor = System.Drawing.Color.Silver;
            this.tableLayoutPanelObs.ColumnCount = 1;
            this.tableLayoutPanelDados.SetColumnSpan(this.tableLayoutPanelObs, 13);
            this.tableLayoutPanelObs.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelObs.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelObs.Controls.Add(this.labelObservacao, 0, 0);
            this.tableLayoutPanelObs.Controls.Add(this.tbObservacao, 0, 1);
            this.tableLayoutPanelObs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelObs.Location = new System.Drawing.Point(51, 102);
            this.tableLayoutPanelObs.Name = "tableLayoutPanelObs";
            this.tableLayoutPanelObs.RowCount = 2;
            this.tableLayoutPanelDados.SetRowSpan(this.tableLayoutPanelObs, 2);
            this.tableLayoutPanelObs.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanelObs.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 88.88889F));
            this.tableLayoutPanelObs.Size = new System.Drawing.Size(604, 189);
            this.tableLayoutPanelObs.TabIndex = 3;
            // 
            // labelObservacao
            // 
            this.labelObservacao.AutoSize = true;
            this.labelObservacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelObservacao.Location = new System.Drawing.Point(3, 0);
            this.labelObservacao.Name = "labelObservacao";
            this.labelObservacao.Size = new System.Drawing.Size(75, 13);
            this.labelObservacao.TabIndex = 0;
            this.labelObservacao.Text = "Observação";
            // 
            // tbObservacao
            // 
            this.tbObservacao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbObservacao.Location = new System.Drawing.Point(3, 23);
            this.tbObservacao.MaxLength = 200;
            this.tbObservacao.Multiline = true;
            this.tbObservacao.Name = "tbObservacao";
            this.tbObservacao.Size = new System.Drawing.Size(598, 163);
            this.tbObservacao.TabIndex = 3;
            // 
            // panel4Editar
            // 
            this.panel4Editar.BackColor = System.Drawing.Color.ForestGreen;
            this.panel4Editar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4Editar.Location = new System.Drawing.Point(3, 558);
            this.panel4Editar.Name = "panel4Editar";
            this.panel4Editar.Size = new System.Drawing.Size(972, 58);
            this.panel4Editar.TabIndex = 22;
            // 
            // ManRede
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(959, 511);
            this.Controls.Add(this.tableLayoutPanelEditar);
            this.Controls.Add(this.tableLayoutPanelConsultar);
            this.Controls.Add(this.RedeBindingNavigator);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.Name = "ManRede";
            this.Text = "ManRede";
            this.Load += new System.EventHandler(this.ManRede_Load);
            this.Controls.SetChildIndex(this.grpPrincipal, 0);
            this.Controls.SetChildIndex(this.tabPrincipal, 0);
            this.Controls.SetChildIndex(this.RedeBindingNavigator, 0);
            this.Controls.SetChildIndex(this.tableLayoutPanelConsultar, 0);
            this.Controls.SetChildIndex(this.tableLayoutPanelEditar, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dtsPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RedeBindingNavigator)).EndInit();
            this.RedeBindingNavigator.ResumeLayout(false);
            this.RedeBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OsClienteRedeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtsRede)).EndInit();
            this.tableLayoutPanelConsultar.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutFilter.ResumeLayout(false);
            this.tableLayoutFilter.PerformLayout();
            this.flowLayoutPanelFiltroNome.ResumeLayout(false);
            this.flowLayoutPanelFiltroNome.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRede)).EndInit();
            this.tableLayoutPanelEditar.ResumeLayout(false);
            this.flowLayoutPanelEditar.ResumeLayout(false);
            this.flowLayoutPanelEditar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel2Editar.ResumeLayout(false);
            this.panel2Editar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.tabControlEditar.ResumeLayout(false);
            this.tabPage1Dados.ResumeLayout(false);
            this.tableLayoutPanelDados.ResumeLayout(false);
            this.tableLayoutPanelRow0Col1.ResumeLayout(false);
            this.tableLayoutPanelRow0Col1.PerformLayout();
            this.tableLayoutPanelNome.ResumeLayout(false);
            this.tableLayoutPanelNome.PerformLayout();
            this.tableLayoutPanelObs.ResumeLayout(false);
            this.tableLayoutPanelObs.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingNavigator RedeBindingNavigator;
        private System.Windows.Forms.ToolStripButton bnInserir;
        private System.Windows.Forms.ToolStripLabel bnCount;
        private System.Windows.Forms.ToolStripButton bnConsultar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton bnGravar;
        private System.Windows.Forms.ToolStripButton bnCancelar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton bnPrimeiro;
        private System.Windows.Forms.ToolStripButton bnAnterior;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripTextBox bnPosition;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton bnProximo;
        private System.Windows.Forms.ToolStripButton bnUltimo;
        private System.Windows.Forms.ToolStripButton bnDescartar;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelConsultar;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label headLabel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutFilter;
        private System.Windows.Forms.CheckBox cbFiltroAtivo;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelFiltroNome;
        private System.Windows.Forms.Label labelFiltroNome;
        private System.Windows.Forms.TextBox tbFiltroNome;
        private System.Windows.Forms.DataGridView dgvRede;
        private System.Windows.Forms.Panel panel4ConsultarBottom;
        private OsClienteRede dtsRede;
        private System.Windows.Forms.BindingSource OsClienteRedeBindingSource;
        private OsClienteRedeTableAdapters.OsClienteRedeTableAdapter osClienteRedeTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idredeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomeredeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obsDataGridViewTextBoxColumn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelEditar;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelEditar;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label headLabelEditar;
        private System.Windows.Forms.Panel panel2Editar;
        private System.Windows.Forms.Label lblTrackBar;
        private System.Windows.Forms.TextBox tbTrackValue;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Button btnResizeColWidth;
        private System.Windows.Forms.TabControl tabControlEditar;
        private System.Windows.Forms.TabPage tabPage1Dados;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelDados;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelRow0Col1;
        private System.Windows.Forms.Label labelRedeId;
        private System.Windows.Forms.TextBox tbRedeId;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelNome;
        private System.Windows.Forms.Label labelRedeNome;
        private System.Windows.Forms.TextBox tbRedeNome;
        private System.Windows.Forms.Panel panelFillerRow1;
        private System.Windows.Forms.Panel panelFillRow1ColLast;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelObs;
        private System.Windows.Forms.Label labelObservacao;
        private System.Windows.Forms.Panel panel4Editar;
        private System.Windows.Forms.TextBox tbObservacao;
    }
}