﻿namespace osExpress.Manutencoes
{
    partial class ManCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label iDLabel;
            System.Windows.Forms.Label cNPJLabel;
            System.Windows.Forms.Label iELabel;
            System.Windows.Forms.Label cONTATOLabel;
            System.Windows.Forms.Label cPFLabel;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label tIPO_PESSOALabel;
            System.Windows.Forms.Label labelContatosHeader;
            System.Windows.Forms.Label labelContatosCliente;
            System.Windows.Forms.Label rAZAO_SOCIALLabel;
            System.Windows.Forms.Label nOME_FANTASIALabel;
            System.Windows.Forms.Label cEPLabel;
            System.Windows.Forms.Label uFLabel;
            System.Windows.Forms.Label cIDADELabel;
            System.Windows.Forms.Label bAIRROLabel;
            System.Windows.Forms.Label eNDERECOLabel;
            System.Windows.Forms.Label tELEFONELabel;
            System.Windows.Forms.Label fAXLabel;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label labelContatosNome;
            System.Windows.Forms.Label labelContatosEmail;
            System.Windows.Forms.Label labelContatosCelular;
            System.Windows.Forms.Label labelContatosCPF;
            System.Windows.Forms.Label labelContatosObs;
            System.Windows.Forms.Label labelContatosNumero;
            System.Windows.Forms.Label cODIGO_SIGPOSTOLabel;
            System.Windows.Forms.Label bANDEIRALabel;
            System.Windows.Forms.Label mODELO_CPULabel;
            System.Windows.Forms.Label tIPO_BOMBALabel;
            System.Windows.Forms.Label qTDE_BICOSLabel;
            System.Windows.Forms.Label qTDE_BOMBASLabel;
            System.Windows.Forms.Label labelTipoNegocio;
            System.Windows.Forms.Label labelCodigoSimp;
            System.Windows.Forms.Label labelEventosNumero;
            System.Windows.Forms.Label lblMarcaInstrumento;
            System.Windows.Forms.Label lblSerie;
            System.Windows.Forms.Label lblQBicos;
            System.Windows.Forms.Label lblIlha;
            System.Windows.Forms.Label lblDescricao;
            System.Windows.Forms.Label lblFabricante;
            System.Windows.Forms.Label lblLacre;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ManCliente));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.checkboxAtivo = new System.Windows.Forms.CheckBox();
            this.comboboxTipoNegocio = new System.Windows.Forms.ComboBox();
            this.osClienteBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.checkboxPostoDeCombustivel = new System.Windows.Forms.CheckBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tabChild = new System.Windows.Forms.TabControl();
            this.tabEndereco = new System.Windows.Forms.TabPage();
            this.fAXTextBox = new System.Windows.Forms.TextBox();
            this.tELEFONETextBox = new System.Windows.Forms.TextBox();
            this.mtbTelefone = new System.Windows.Forms.MaskedTextBox();
            this.bAIRROTextBox = new System.Windows.Forms.TextBox();
            this.eNDERECOTextBox = new System.Windows.Forms.TextBox();
            this.cIDADETextBox = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.cEPTextBox = new System.Windows.Forms.TextBox();
            this.tabContatos = new System.Windows.Forms.TabPage();
            this.checkboxPrincipal = new System.Windows.Forms.CheckBox();
            this.listView1 = new System.Windows.Forms.ListView();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.textboxContatosObs = new System.Windows.Forms.TextBox();
            this.maskedTextboxContatosCpf = new System.Windows.Forms.MaskedTextBox();
            this.buttonContatosDeletar = new System.Windows.Forms.Button();
            this.buttonContatosCancelar = new System.Windows.Forms.Button();
            this.buttonContatosGravar = new System.Windows.Forms.Button();
            this.maskedTextboxContatosCelular = new System.Windows.Forms.MaskedTextBox();
            this.textboxContatosEmail = new System.Windows.Forms.TextBox();
            this.textboxContatosNome = new System.Windows.Forms.TextBox();
            this.textboxHiddenId = new System.Windows.Forms.TextBox();
            this.tabOutros = new System.Windows.Forms.TabPage();
            this.textboxCodigoSimp = new System.Windows.Forms.TextBox();
            this.textboxQuantSumpers = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textboxQuantTanques = new System.Windows.Forms.TextBox();
            this.checkboxRFID = new System.Windows.Forms.CheckBox();
            this.labelConsentrador = new System.Windows.Forms.Label();
            this.labelQuantSumpers = new System.Windows.Forms.Label();
            this.labelQuantTanques = new System.Windows.Forms.Label();
            this.qTDE_BOMBASTextBox = new System.Windows.Forms.TextBox();
            this.tIPO_BOMBATextBox = new System.Windows.Forms.TextBox();
            this.qTDE_BICOSTextBox = new System.Windows.Forms.TextBox();
            this.bANDEIRATextBox = new System.Windows.Forms.TextBox();
            this.mODELO_CPUTextBox = new System.Windows.Forms.TextBox();
            this.tabObservacoes = new System.Windows.Forms.TabPage();
            this.oBSERVACAOTextBox = new System.Windows.Forms.TextBox();
            this.cODIGO_SIGPOSTOTextBox = new System.Windows.Forms.TextBox();
            this.tabEventos = new System.Windows.Forms.TabPage();
            this.btnListEventos = new System.Windows.Forms.Button();
            this.textboxHiddenIdEvento = new System.Windows.Forms.TextBox();
            this.buttonEventosDeletar = new System.Windows.Forms.Button();
            this.buttonEventosGravar = new System.Windows.Forms.Button();
            this.buttonEventosCancelar = new System.Windows.Forms.Button();
            this.datetimeRealizacao = new System.Windows.Forms.DateTimePicker();
            this.listviewEventos = new System.Windows.Forms.ListView();
            this.checkboxAlertar = new System.Windows.Forms.CheckBox();
            this.textboxApontamento = new System.Windows.Forms.TextBox();
            this.labelApontamento = new System.Windows.Forms.Label();
            this.labelDataEHora = new System.Windows.Forms.Label();
            this.labelEM = new System.Windows.Forms.Label();
            this.tabOrcamentos = new System.Windows.Forms.TabPage();
            this.dataGridViewOrcamentos = new System.Windows.Forms.DataGridView();
            this.tabPedidos = new System.Windows.Forms.TabPage();
            this.dataGridViewPedidos = new System.Windows.Forms.DataGridView();
            this.tabServicos = new System.Windows.Forms.TabPage();
            this.dataGridViewServicos = new System.Windows.Forms.DataGridView();
            this.tabInstrumentos = new System.Windows.Forms.TabPage();
            this.rtbHelp2 = new System.Windows.Forms.RichTextBox();
            this.rtbHelp0 = new System.Windows.Forms.RichTextBox();
            this.btnLimparBomba = new System.Windows.Forms.Button();
            this.pnlEditarBomba = new System.Windows.Forms.Panel();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.rtbHelp1 = new System.Windows.Forms.RichTextBox();
            this.btnGerarLacre = new System.Windows.Forms.Button();
            this.btnDeletarBomba = new System.Windows.Forms.Button();
            this.btnGravarBomba = new System.Windows.Forms.Button();
            this.dgvLacre = new System.Windows.Forms.DataGridView();
            this.tbLacre = new System.Windows.Forms.TextBox();
            this.tbFabricante = new System.Windows.Forms.TextBox();
            this.tbDescricao = new System.Windows.Forms.TextBox();
            this.tbIlha = new System.Windows.Forms.TextBox();
            this.tbQBicos = new System.Windows.Forms.TextBox();
            this.tbSerie = new System.Windows.Forms.TextBox();
            this.cmbMarcaInstrumento = new System.Windows.Forms.ComboBox();
            this.dgvBombas = new System.Windows.Forms.DataGridView();
            this.rAZAO_SOCIALTextBox = new System.Windows.Forms.TextBox();
            this.nOME_FANTASIATextBox = new System.Windows.Forms.TextBox();
            this.mtbCPF = new System.Windows.Forms.MaskedTextBox();
            this.mtbCNPJ = new System.Windows.Forms.MaskedTextBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.iDTextBox = new System.Windows.Forms.TextBox();
            this.iETextBox = new System.Windows.Forms.TextBox();
            this.cPFTextBox = new System.Windows.Forms.TextBox();
            this.cNPJTextBox = new System.Windows.Forms.TextBox();
            this.cONTATOTextBox = new System.Windows.Forms.TextBox();
            this.bindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.tsbInserir = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbGravar = new System.Windows.Forms.ToolStripButton();
            this.tsbCancelar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.tsbDescartar = new System.Windows.Forms.ToolStripButton();
            this.pnlConsultar = new System.Windows.Forms.Panel();
            this.tbLike = new System.Windows.Forms.TextBox();
            this.labelLike = new System.Windows.Forms.Label();
            this.flpManEventos = new System.Windows.Forms.FlowLayoutPanel();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label70 = new System.Windows.Forms.Label();
            this.lblTotalOS = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.labelSortear = new System.Windows.Forms.Label();
            this.labelFantasia = new System.Windows.Forms.Label();
            this.labelRazao = new System.Windows.Forms.Label();
            this.chkFiltrarClienteFantasia = new System.Windows.Forms.CheckBox();
            this.cmbFiltroClienteFantasia = new System.Windows.Forms.ComboBox();
            this.labelFiltroCliente = new System.Windows.Forms.Label();
            this.textboxFiltroCliente = new System.Windows.Forms.TextBox();
            this.btnEventosPendentes = new System.Windows.Forms.Button();
            this.btnEventosConcluidos = new System.Windows.Forms.Button();
            this.btnEventosTodos = new System.Windows.Forms.Button();
            this.cmbFiltroClienteCodigo = new System.Windows.Forms.ComboBox();
            this.chkFiltrarCliente = new System.Windows.Forms.CheckBox();
            this.chkFiltrarSituacao = new System.Windows.Forms.CheckBox();
            this.label65 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.cmbFiltroSituacao = new System.Windows.Forms.ComboBox();
            this.cmbFiltroClienteNome = new System.Windows.Forms.ComboBox();
            this.osOrdemServicoDataGridView = new System.Windows.Forms.DataGridView();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rAZAOSOCIALDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nOMEFANTASIADataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CNPJ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.labelCadastrarCliente = new System.Windows.Forms.Label();
            this.Contatos = new System.Windows.Forms.TabPage();
            this.Outros = new System.Windows.Forms.TabPage();
            this.tableAdapterManager = new osExpress.OsExpressDataSetTableAdapters.TableAdapterManager();
            this.osClienteTableAdapter = new osExpress.OsExpressDataSetTableAdapters.OsClienteTableAdapter();
            iDLabel = new System.Windows.Forms.Label();
            cNPJLabel = new System.Windows.Forms.Label();
            iELabel = new System.Windows.Forms.Label();
            cONTATOLabel = new System.Windows.Forms.Label();
            cPFLabel = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            tIPO_PESSOALabel = new System.Windows.Forms.Label();
            labelContatosHeader = new System.Windows.Forms.Label();
            labelContatosCliente = new System.Windows.Forms.Label();
            rAZAO_SOCIALLabel = new System.Windows.Forms.Label();
            nOME_FANTASIALabel = new System.Windows.Forms.Label();
            cEPLabel = new System.Windows.Forms.Label();
            uFLabel = new System.Windows.Forms.Label();
            cIDADELabel = new System.Windows.Forms.Label();
            bAIRROLabel = new System.Windows.Forms.Label();
            eNDERECOLabel = new System.Windows.Forms.Label();
            tELEFONELabel = new System.Windows.Forms.Label();
            fAXLabel = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            labelContatosNome = new System.Windows.Forms.Label();
            labelContatosEmail = new System.Windows.Forms.Label();
            labelContatosCelular = new System.Windows.Forms.Label();
            labelContatosCPF = new System.Windows.Forms.Label();
            labelContatosObs = new System.Windows.Forms.Label();
            labelContatosNumero = new System.Windows.Forms.Label();
            cODIGO_SIGPOSTOLabel = new System.Windows.Forms.Label();
            bANDEIRALabel = new System.Windows.Forms.Label();
            mODELO_CPULabel = new System.Windows.Forms.Label();
            tIPO_BOMBALabel = new System.Windows.Forms.Label();
            qTDE_BICOSLabel = new System.Windows.Forms.Label();
            qTDE_BOMBASLabel = new System.Windows.Forms.Label();
            labelTipoNegocio = new System.Windows.Forms.Label();
            labelCodigoSimp = new System.Windows.Forms.Label();
            labelEventosNumero = new System.Windows.Forms.Label();
            lblMarcaInstrumento = new System.Windows.Forms.Label();
            lblSerie = new System.Windows.Forms.Label();
            lblQBicos = new System.Windows.Forms.Label();
            lblIlha = new System.Windows.Forms.Label();
            lblDescricao = new System.Windows.Forms.Label();
            lblFabricante = new System.Windows.Forms.Label();
            lblLacre = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dtsPrincipal)).BeginInit();
            this.tabPrincipal.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.osClienteBindingSource)).BeginInit();
            this.tabChild.SuspendLayout();
            this.tabEndereco.SuspendLayout();
            this.tabContatos.SuspendLayout();
            this.tabOutros.SuspendLayout();
            this.tabObservacoes.SuspendLayout();
            this.tabEventos.SuspendLayout();
            this.tabOrcamentos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOrcamentos)).BeginInit();
            this.tabPedidos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPedidos)).BeginInit();
            this.tabServicos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewServicos)).BeginInit();
            this.tabInstrumentos.SuspendLayout();
            this.pnlEditarBomba.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLacre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBombas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator)).BeginInit();
            this.bindingNavigator.SuspendLayout();
            this.pnlConsultar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.osOrdemServicoDataGridView)).BeginInit();
            this.Contatos.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpPrincipal
            // 
            this.grpPrincipal.BackColor = System.Drawing.SystemColors.Control;
            this.grpPrincipal.ForeColor = System.Drawing.SystemColors.ControlText;
            this.grpPrincipal.Location = new System.Drawing.Point(12, 887);
            this.grpPrincipal.Margin = new System.Windows.Forms.Padding(4);
            this.grpPrincipal.Padding = new System.Windows.Forms.Padding(4);
            this.grpPrincipal.Size = new System.Drawing.Size(760, 15);
            this.grpPrincipal.Visible = false;
            // 
            // tabPrincipal
            // 
            this.tabPrincipal.Controls.Add(this.tabPage1);
            this.tabPrincipal.Controls.Add(this.Contatos);
            this.tabPrincipal.Controls.Add(this.Outros);
            this.tabPrincipal.Location = new System.Drawing.Point(12, 80);
            this.tabPrincipal.Margin = new System.Windows.Forms.Padding(4);
            this.tabPrincipal.Size = new System.Drawing.Size(1200, 550);
            this.tabPrincipal.SelectedIndexChanged += new System.EventHandler(this.tabPrincipal_SelectedIndexChanged);
            // 
            // iDLabel
            // 
            iDLabel.AutoSize = true;
            iDLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            iDLabel.Location = new System.Drawing.Point(25, 100);
            iDLabel.Name = "iDLabel";
            iDLabel.Size = new System.Drawing.Size(46, 13);
            iDLabel.TabIndex = 0;
            iDLabel.Text = "Código";
            // 
            // cNPJLabel
            // 
            cNPJLabel.AutoSize = true;
            cNPJLabel.Location = new System.Drawing.Point(110, 55);
            cNPJLabel.Name = "cNPJLabel";
            cNPJLabel.Size = new System.Drawing.Size(34, 13);
            cNPJLabel.TabIndex = 0;
            cNPJLabel.Text = "CNPJ";
            // 
            // iELabel
            // 
            iELabel.AutoSize = true;
            iELabel.Location = new System.Drawing.Point(360, 55);
            iELabel.Name = "iELabel";
            iELabel.Size = new System.Drawing.Size(89, 13);
            iELabel.TabIndex = 0;
            iELabel.Text = "Incrição Estadual";
            // 
            // cONTATOLabel
            // 
            cONTATOLabel.AutoSize = true;
            cONTATOLabel.Location = new System.Drawing.Point(463, 15);
            cONTATOLabel.Name = "cONTATOLabel";
            cONTATOLabel.Size = new System.Drawing.Size(55, 13);
            cONTATOLabel.TabIndex = 0;
            cONTATOLabel.Text = "Contato:";
            cONTATOLabel.Visible = false;
            // 
            // cPFLabel
            // 
            cPFLabel.AutoSize = true;
            cPFLabel.Location = new System.Drawing.Point(110, 10);
            cPFLabel.Name = "cPFLabel";
            cPFLabel.Size = new System.Drawing.Size(27, 13);
            cPFLabel.TabIndex = 0;
            cPFLabel.Text = "CPF";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label1.Location = new System.Drawing.Point(750, 10);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(71, 13);
            label1.TabIndex = 30;
            label1.Text = "Status: (...)";
            label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            label1.Visible = false;
            // 
            // tIPO_PESSOALabel
            // 
            tIPO_PESSOALabel.AutoSize = true;
            tIPO_PESSOALabel.Location = new System.Drawing.Point(25, 10);
            tIPO_PESSOALabel.Name = "tIPO_PESSOALabel";
            tIPO_PESSOALabel.Size = new System.Drawing.Size(66, 13);
            tIPO_PESSOALabel.TabIndex = 0;
            tIPO_PESSOALabel.Text = "Tipo Pessoa";
            // 
            // labelContatosHeader
            // 
            labelContatosHeader.AutoSize = true;
            labelContatosHeader.Location = new System.Drawing.Point(160, 20);
            labelContatosHeader.Name = "labelContatosHeader";
            labelContatosHeader.Size = new System.Drawing.Size(50, 13);
            labelContatosHeader.TabIndex = 0;
            labelContatosHeader.Text = "Cliente ";
            // 
            // labelContatosCliente
            // 
            labelContatosCliente.AutoSize = true;
            labelContatosCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            labelContatosCliente.Location = new System.Drawing.Point(400, 10);
            labelContatosCliente.Name = "labelContatosCliente";
            labelContatosCliente.Size = new System.Drawing.Size(28, 24);
            labelContatosCliente.TabIndex = 0;
            labelContatosCliente.Text = "...";
            // 
            // rAZAO_SOCIALLabel
            // 
            rAZAO_SOCIALLabel.AutoSize = true;
            rAZAO_SOCIALLabel.Location = new System.Drawing.Point(100, 100);
            rAZAO_SOCIALLabel.Name = "rAZAO_SOCIALLabel";
            rAZAO_SOCIALLabel.Size = new System.Drawing.Size(116, 13);
            rAZAO_SOCIALLabel.TabIndex = 47;
            rAZAO_SOCIALLabel.Text = "Nome ou Razão Social";
            // 
            // nOME_FANTASIALabel
            // 
            nOME_FANTASIALabel.AutoSize = true;
            nOME_FANTASIALabel.Location = new System.Drawing.Point(100, 140);
            nOME_FANTASIALabel.Name = "nOME_FANTASIALabel";
            nOME_FANTASIALabel.Size = new System.Drawing.Size(78, 13);
            nOME_FANTASIALabel.TabIndex = 48;
            nOME_FANTASIALabel.Text = "Nome Fantasia";
            // 
            // cEPLabel
            // 
            cEPLabel.AutoSize = true;
            cEPLabel.Location = new System.Drawing.Point(10, 10);
            cEPLabel.Name = "cEPLabel";
            cEPLabel.Size = new System.Drawing.Size(28, 13);
            cEPLabel.TabIndex = 50;
            cEPLabel.Text = "CEP";
            // 
            // uFLabel
            // 
            uFLabel.AutoSize = true;
            uFLabel.Location = new System.Drawing.Point(170, 10);
            uFLabel.Name = "uFLabel";
            uFLabel.Size = new System.Drawing.Size(40, 13);
            uFLabel.TabIndex = 55;
            uFLabel.Text = "Estado";
            // 
            // cIDADELabel
            // 
            cIDADELabel.AutoSize = true;
            cIDADELabel.Location = new System.Drawing.Point(240, 10);
            cIDADELabel.Name = "cIDADELabel";
            cIDADELabel.Size = new System.Drawing.Size(40, 13);
            cIDADELabel.TabIndex = 57;
            cIDADELabel.Text = "Cidade";
            // 
            // bAIRROLabel
            // 
            bAIRROLabel.AutoSize = true;
            bAIRROLabel.Location = new System.Drawing.Point(450, 55);
            bAIRROLabel.Name = "bAIRROLabel";
            bAIRROLabel.Size = new System.Drawing.Size(34, 13);
            bAIRROLabel.TabIndex = 60;
            bAIRROLabel.Text = "Bairro";
            // 
            // eNDERECOLabel
            // 
            eNDERECOLabel.AutoSize = true;
            eNDERECOLabel.Location = new System.Drawing.Point(10, 55);
            eNDERECOLabel.Name = "eNDERECOLabel";
            eNDERECOLabel.Size = new System.Drawing.Size(53, 13);
            eNDERECOLabel.TabIndex = 58;
            eNDERECOLabel.Text = "Endereço";
            // 
            // tELEFONELabel
            // 
            tELEFONELabel.AutoSize = true;
            tELEFONELabel.Location = new System.Drawing.Point(10, 95);
            tELEFONELabel.Name = "tELEFONELabel";
            tELEFONELabel.Size = new System.Drawing.Size(49, 13);
            tELEFONELabel.TabIndex = 61;
            tELEFONELabel.Text = "Telefone";
            // 
            // fAXLabel
            // 
            fAXLabel.AutoSize = true;
            fAXLabel.Location = new System.Drawing.Point(300, 95);
            fAXLabel.Name = "fAXLabel";
            fAXLabel.Size = new System.Drawing.Size(24, 13);
            fAXLabel.TabIndex = 66;
            fAXLabel.Text = "Fax";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(360, 10);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(32, 13);
            label2.TabIndex = 52;
            label2.Text = "Email";
            label2.Visible = false;
            // 
            // labelContatosNome
            // 
            labelContatosNome.AutoSize = true;
            labelContatosNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            labelContatosNome.Location = new System.Drawing.Point(10, 10);
            labelContatosNome.Name = "labelContatosNome";
            labelContatosNome.Size = new System.Drawing.Size(35, 13);
            labelContatosNome.TabIndex = 13;
            labelContatosNome.Text = "Nome";
            // 
            // labelContatosEmail
            // 
            labelContatosEmail.AutoSize = true;
            labelContatosEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            labelContatosEmail.Location = new System.Drawing.Point(270, 10);
            labelContatosEmail.Name = "labelContatosEmail";
            labelContatosEmail.Size = new System.Drawing.Size(32, 13);
            labelContatosEmail.TabIndex = 15;
            labelContatosEmail.Text = "Email";
            // 
            // labelContatosCelular
            // 
            labelContatosCelular.AutoSize = true;
            labelContatosCelular.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            labelContatosCelular.Location = new System.Drawing.Point(530, 10);
            labelContatosCelular.Name = "labelContatosCelular";
            labelContatosCelular.Size = new System.Drawing.Size(39, 13);
            labelContatosCelular.TabIndex = 17;
            labelContatosCelular.Text = "Celular";
            // 
            // labelContatosCPF
            // 
            labelContatosCPF.AutoSize = true;
            labelContatosCPF.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            labelContatosCPF.Location = new System.Drawing.Point(530, 55);
            labelContatosCPF.Name = "labelContatosCPF";
            labelContatosCPF.Size = new System.Drawing.Size(27, 13);
            labelContatosCPF.TabIndex = 22;
            labelContatosCPF.Text = "CPF";
            // 
            // labelContatosObs
            // 
            labelContatosObs.AutoSize = true;
            labelContatosObs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            labelContatosObs.Location = new System.Drawing.Point(10, 55);
            labelContatosObs.Name = "labelContatosObs";
            labelContatosObs.Size = new System.Drawing.Size(65, 13);
            labelContatosObs.TabIndex = 24;
            labelContatosObs.Text = "Observação";
            // 
            // labelContatosNumero
            // 
            labelContatosNumero.AutoSize = true;
            labelContatosNumero.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            labelContatosNumero.Location = new System.Drawing.Point(10, 105);
            labelContatosNumero.Name = "labelContatosNumero";
            labelContatosNumero.Size = new System.Drawing.Size(135, 13);
            labelContatosNumero.TabIndex = 35;
            labelContatosNumero.Text = "Número dos contatos: ";
            // 
            // cODIGO_SIGPOSTOLabel
            // 
            cODIGO_SIGPOSTOLabel.AutoSize = true;
            cODIGO_SIGPOSTOLabel.Location = new System.Drawing.Point(527, 170);
            cODIGO_SIGPOSTOLabel.Name = "cODIGO_SIGPOSTOLabel";
            cODIGO_SIGPOSTOLabel.Size = new System.Drawing.Size(88, 13);
            cODIGO_SIGPOSTOLabel.TabIndex = 64;
            cODIGO_SIGPOSTOLabel.Text = "Código SigPosto:";
            cODIGO_SIGPOSTOLabel.Visible = false;
            // 
            // bANDEIRALabel
            // 
            bANDEIRALabel.AutoSize = true;
            bANDEIRALabel.Location = new System.Drawing.Point(10, 10);
            bANDEIRALabel.Name = "bANDEIRALabel";
            bANDEIRALabel.Size = new System.Drawing.Size(52, 13);
            bANDEIRALabel.TabIndex = 45;
            bANDEIRALabel.Text = "Bandeira:";
            // 
            // mODELO_CPULabel
            // 
            mODELO_CPULabel.AutoSize = true;
            mODELO_CPULabel.Location = new System.Drawing.Point(260, 10);
            mODELO_CPULabel.Name = "mODELO_CPULabel";
            mODELO_CPULabel.Size = new System.Drawing.Size(80, 13);
            mODELO_CPULabel.TabIndex = 46;
            mODELO_CPULabel.Text = "Marca/Modelo:";
            // 
            // tIPO_BOMBALabel
            // 
            tIPO_BOMBALabel.AutoSize = true;
            tIPO_BOMBALabel.Location = new System.Drawing.Point(510, 60);
            tIPO_BOMBALabel.Name = "tIPO_BOMBALabel";
            tIPO_BOMBALabel.Size = new System.Drawing.Size(82, 13);
            tIPO_BOMBALabel.TabIndex = 50;
            tIPO_BOMBALabel.Text = "Tipo de Bomba:";
            // 
            // qTDE_BICOSLabel
            // 
            qTDE_BICOSLabel.AutoSize = true;
            qTDE_BICOSLabel.Location = new System.Drawing.Point(260, 60);
            qTDE_BICOSLabel.Name = "qTDE_BICOSLabel";
            qTDE_BICOSLabel.Size = new System.Drawing.Size(80, 13);
            qTDE_BICOSLabel.TabIndex = 52;
            qTDE_BICOSLabel.Text = "Qtde. de Bicos:";
            // 
            // qTDE_BOMBASLabel
            // 
            qTDE_BOMBASLabel.AutoSize = true;
            qTDE_BOMBASLabel.Location = new System.Drawing.Point(10, 60);
            qTDE_BOMBASLabel.Name = "qTDE_BOMBASLabel";
            qTDE_BOMBASLabel.Size = new System.Drawing.Size(92, 13);
            qTDE_BOMBASLabel.TabIndex = 51;
            qTDE_BOMBASLabel.Text = "Qtde. de Bombas:";
            // 
            // labelTipoNegocio
            // 
            labelTipoNegocio.AutoSize = true;
            labelTipoNegocio.Location = new System.Drawing.Point(734, 95);
            labelTipoNegocio.Name = "labelTipoNegocio";
            labelTipoNegocio.Size = new System.Drawing.Size(33, 13);
            labelTipoNegocio.TabIndex = 56;
            labelTipoNegocio.Text = "Rede";
            // 
            // labelCodigoSimp
            // 
            labelCodigoSimp.AutoSize = true;
            labelCodigoSimp.Location = new System.Drawing.Point(510, 10);
            labelCodigoSimp.Name = "labelCodigoSimp";
            labelCodigoSimp.Size = new System.Drawing.Size(69, 13);
            labelCodigoSimp.TabIndex = 61;
            labelCodigoSimp.Text = "Código SIMP";
            // 
            // labelEventosNumero
            // 
            labelEventosNumero.AutoSize = true;
            labelEventosNumero.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            labelEventosNumero.Location = new System.Drawing.Point(17, 93);
            labelEventosNumero.Name = "labelEventosNumero";
            labelEventosNumero.Size = new System.Drawing.Size(131, 13);
            labelEventosNumero.TabIndex = 36;
            labelEventosNumero.Text = "Número dos eventos: ";
            // 
            // lblMarcaInstrumento
            // 
            lblMarcaInstrumento.AutoSize = true;
            lblMarcaInstrumento.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblMarcaInstrumento.Location = new System.Drawing.Point(405, 48);
            lblMarcaInstrumento.Name = "lblMarcaInstrumento";
            lblMarcaInstrumento.Size = new System.Drawing.Size(95, 13);
            lblMarcaInstrumento.TabIndex = 42;
            lblMarcaInstrumento.Text = "Marca Instrumento";
            // 
            // lblSerie
            // 
            lblSerie.AutoSize = true;
            lblSerie.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblSerie.Location = new System.Drawing.Point(21, 5);
            lblSerie.Name = "lblSerie";
            lblSerie.Size = new System.Drawing.Size(51, 13);
            lblSerie.TabIndex = 44;
            lblSerie.Text = "Série (ID)";
            // 
            // lblQBicos
            // 
            lblQBicos.AutoSize = true;
            lblQBicos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblQBicos.Location = new System.Drawing.Point(351, 5);
            lblQBicos.Name = "lblQBicos";
            lblQBicos.Size = new System.Drawing.Size(77, 13);
            lblQBicos.TabIndex = 46;
            lblQBicos.Text = "Qtde. de Bicos";
            // 
            // lblIlha
            // 
            lblIlha.AutoSize = true;
            lblIlha.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblIlha.Location = new System.Drawing.Point(456, 5);
            lblIlha.Name = "lblIlha";
            lblIlha.Size = new System.Drawing.Size(24, 13);
            lblIlha.TabIndex = 48;
            lblIlha.Text = "Ilha";
            // 
            // lblDescricao
            // 
            lblDescricao.AutoSize = true;
            lblDescricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblDescricao.Location = new System.Drawing.Point(21, 49);
            lblDescricao.Name = "lblDescricao";
            lblDescricao.Size = new System.Drawing.Size(55, 13);
            lblDescricao.TabIndex = 50;
            lblDescricao.Text = "Descrição";
            // 
            // lblFabricante
            // 
            lblFabricante.AutoSize = true;
            lblFabricante.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblFabricante.Location = new System.Drawing.Point(213, 49);
            lblFabricante.Name = "lblFabricante";
            lblFabricante.Size = new System.Drawing.Size(57, 13);
            lblFabricante.TabIndex = 52;
            lblFabricante.Text = "Fabricante";
            // 
            // lblLacre
            // 
            lblLacre.AutoSize = true;
            lblLacre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblLacre.Location = new System.Drawing.Point(21, 100);
            lblLacre.Name = "lblLacre";
            lblLacre.Size = new System.Drawing.Size(34, 13);
            lblLacre.TabIndex = 54;
            lblLacre.Text = "Lacre";
            // 
            // tabPage1
            // 
            this.tabPage1.AutoScroll = true;
            this.tabPage1.Controls.Add(this.checkboxAtivo);
            this.tabPage1.Controls.Add(labelTipoNegocio);
            this.tabPage1.Controls.Add(this.comboboxTipoNegocio);
            this.tabPage1.Controls.Add(this.checkboxPostoDeCombustivel);
            this.tabPage1.Controls.Add(label2);
            this.tabPage1.Controls.Add(this.textBox1);
            this.tabPage1.Controls.Add(this.tabChild);
            this.tabPage1.Controls.Add(rAZAO_SOCIALLabel);
            this.tabPage1.Controls.Add(this.rAZAO_SOCIALTextBox);
            this.tabPage1.Controls.Add(nOME_FANTASIALabel);
            this.tabPage1.Controls.Add(this.nOME_FANTASIATextBox);
            this.tabPage1.Controls.Add(this.mtbCPF);
            this.tabPage1.Controls.Add(this.mtbCNPJ);
            this.tabPage1.Controls.Add(this.comboBox2);
            this.tabPage1.Controls.Add(tIPO_PESSOALabel);
            this.tabPage1.Controls.Add(label1);
            this.tabPage1.Controls.Add(iDLabel);
            this.tabPage1.Controls.Add(this.iDTextBox);
            this.tabPage1.Controls.Add(cNPJLabel);
            this.tabPage1.Controls.Add(iELabel);
            this.tabPage1.Controls.Add(this.iETextBox);
            this.tabPage1.Controls.Add(cPFLabel);
            this.tabPage1.Controls.Add(this.cPFTextBox);
            this.tabPage1.Controls.Add(this.cNPJTextBox);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1192, 524);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = " Dados";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // checkboxAtivo
            // 
            this.checkboxAtivo.AutoSize = true;
            this.checkboxAtivo.Checked = true;
            this.checkboxAtivo.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkboxAtivo.Location = new System.Drawing.Point(732, 70);
            this.checkboxAtivo.Name = "checkboxAtivo";
            this.checkboxAtivo.Size = new System.Drawing.Size(55, 17);
            this.checkboxAtivo.TabIndex = 57;
            this.checkboxAtivo.Text = "ativo?";
            this.checkboxAtivo.UseVisualStyleBackColor = true;
            // 
            // comboboxTipoNegocio
            // 
            this.comboboxTipoNegocio.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboboxTipoNegocio.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboboxTipoNegocio.DataBindings.Add(new System.Windows.Forms.Binding("SelectedItem", this.osClienteBindingSource, "TIPO_PESSOA", true));
            this.comboboxTipoNegocio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboboxTipoNegocio.FormattingEnabled = true;
            this.comboboxTipoNegocio.Items.AddRange(new object[] {
            "Posto",
            "Não posto"});
            this.comboboxTipoNegocio.Location = new System.Drawing.Point(731, 114);
            this.comboboxTipoNegocio.Name = "comboboxTipoNegocio";
            this.comboboxTipoNegocio.Size = new System.Drawing.Size(137, 21);
            this.comboboxTipoNegocio.TabIndex = 55;
            this.comboboxTipoNegocio.SelectionChangeCommitted += new System.EventHandler(this.comboboxTipoNegocio_SelectionChangeCommitted);
            // 
            // osClienteBindingSource
            // 
            this.osClienteBindingSource.DataMember = "OsCliente";
            this.osClienteBindingSource.DataSource = this.dtsPrincipal;
            // 
            // checkboxPostoDeCombustivel
            // 
            this.checkboxPostoDeCombustivel.AutoSize = true;
            this.checkboxPostoDeCombustivel.Checked = true;
            this.checkboxPostoDeCombustivel.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkboxPostoDeCombustivel.Location = new System.Drawing.Point(732, 157);
            this.checkboxPostoDeCombustivel.Name = "checkboxPostoDeCombustivel";
            this.checkboxPostoDeCombustivel.Size = new System.Drawing.Size(136, 17);
            this.checkboxPostoDeCombustivel.TabIndex = 54;
            this.checkboxPostoDeCombustivel.Text = "Posto de Combustível?";
            this.checkboxPostoDeCombustivel.UseVisualStyleBackColor = true;
            this.checkboxPostoDeCombustivel.CheckedChanged += new System.EventHandler(this.checkboxPostoDeCombustivel_CheckedChanged);
            // 
            // textBox1
            // 
            this.textBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osClienteBindingSource, "EMAIL", true));
            this.textBox1.Location = new System.Drawing.Point(360, 25);
            this.textBox1.MaxLength = 50;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(320, 20);
            this.textBox1.TabIndex = 53;
            this.textBox1.Text = " ";
            this.textBox1.Visible = false;
            // 
            // tabChild
            // 
            this.tabChild.Controls.Add(this.tabEndereco);
            this.tabChild.Controls.Add(this.tabContatos);
            this.tabChild.Controls.Add(this.tabOutros);
            this.tabChild.Controls.Add(this.tabObservacoes);
            this.tabChild.Controls.Add(this.tabEventos);
            this.tabChild.Controls.Add(this.tabOrcamentos);
            this.tabChild.Controls.Add(this.tabPedidos);
            this.tabChild.Controls.Add(this.tabServicos);
            this.tabChild.Controls.Add(this.tabInstrumentos);
            this.tabChild.Location = new System.Drawing.Point(10, 207);
            this.tabChild.Name = "tabChild";
            this.tabChild.SelectedIndex = 0;
            this.tabChild.Size = new System.Drawing.Size(980, 248);
            this.tabChild.TabIndex = 51;
            this.tabChild.SelectedIndexChanged += new System.EventHandler(this.tabChild_SelectedIndexChanged);
            // 
            // tabEndereco
            // 
            this.tabEndereco.Controls.Add(fAXLabel);
            this.tabEndereco.Controls.Add(this.fAXTextBox);
            this.tabEndereco.Controls.Add(this.tELEFONETextBox);
            this.tabEndereco.Controls.Add(this.mtbTelefone);
            this.tabEndereco.Controls.Add(tELEFONELabel);
            this.tabEndereco.Controls.Add(bAIRROLabel);
            this.tabEndereco.Controls.Add(this.bAIRROTextBox);
            this.tabEndereco.Controls.Add(eNDERECOLabel);
            this.tabEndereco.Controls.Add(this.eNDERECOTextBox);
            this.tabEndereco.Controls.Add(cIDADELabel);
            this.tabEndereco.Controls.Add(this.cIDADETextBox);
            this.tabEndereco.Controls.Add(this.comboBox1);
            this.tabEndereco.Controls.Add(uFLabel);
            this.tabEndereco.Controls.Add(cEPLabel);
            this.tabEndereco.Controls.Add(this.cEPTextBox);
            this.tabEndereco.Location = new System.Drawing.Point(4, 22);
            this.tabEndereco.Name = "tabEndereco";
            this.tabEndereco.Padding = new System.Windows.Forms.Padding(3);
            this.tabEndereco.Size = new System.Drawing.Size(972, 222);
            this.tabEndereco.TabIndex = 0;
            this.tabEndereco.Text = "Endereço";
            this.tabEndereco.UseVisualStyleBackColor = true;
            // 
            // fAXTextBox
            // 
            this.fAXTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osClienteBindingSource, "FAX", true));
            this.fAXTextBox.Location = new System.Drawing.Point(300, 110);
            this.fAXTextBox.MaxLength = 20;
            this.fAXTextBox.Name = "fAXTextBox";
            this.fAXTextBox.Size = new System.Drawing.Size(243, 20);
            this.fAXTextBox.TabIndex = 65;
            // 
            // tELEFONETextBox
            // 
            this.tELEFONETextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osClienteBindingSource, "TELEFONE", true));
            this.tELEFONETextBox.Location = new System.Drawing.Point(63, 90);
            this.tELEFONETextBox.MaxLength = 20;
            this.tELEFONETextBox.Name = "tELEFONETextBox";
            this.tELEFONETextBox.Size = new System.Drawing.Size(220, 20);
            this.tELEFONETextBox.TabIndex = 64;
            this.tELEFONETextBox.TabStop = false;
            // 
            // mtbTelefone
            // 
            this.mtbTelefone.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osClienteBindingSource, "TELEFONE", true));
            this.mtbTelefone.Location = new System.Drawing.Point(10, 110);
            this.mtbTelefone.Mask = "(##)####-####";
            this.mtbTelefone.Name = "mtbTelefone";
            this.mtbTelefone.Size = new System.Drawing.Size(243, 20);
            this.mtbTelefone.TabIndex = 62;
            // 
            // bAIRROTextBox
            // 
            this.bAIRROTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osClienteBindingSource, "BAIRRO", true));
            this.bAIRROTextBox.Location = new System.Drawing.Point(450, 70);
            this.bAIRROTextBox.MaxLength = 30;
            this.bAIRROTextBox.Name = "bAIRROTextBox";
            this.bAIRROTextBox.Size = new System.Drawing.Size(246, 20);
            this.bAIRROTextBox.TabIndex = 5;
            // 
            // eNDERECOTextBox
            // 
            this.eNDERECOTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osClienteBindingSource, "ENDERECO", true));
            this.eNDERECOTextBox.Location = new System.Drawing.Point(10, 70);
            this.eNDERECOTextBox.MaxLength = 50;
            this.eNDERECOTextBox.Name = "eNDERECOTextBox";
            this.eNDERECOTextBox.Size = new System.Drawing.Size(408, 20);
            this.eNDERECOTextBox.TabIndex = 4;
            // 
            // cIDADETextBox
            // 
            this.cIDADETextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osClienteBindingSource, "CIDADE", true));
            this.cIDADETextBox.Location = new System.Drawing.Point(240, 25);
            this.cIDADETextBox.MaxLength = 30;
            this.cIDADETextBox.Name = "cIDADETextBox";
            this.cIDADETextBox.Size = new System.Drawing.Size(456, 20);
            this.cIDADETextBox.TabIndex = 3;
            // 
            // comboBox1
            // 
            this.comboBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox1.DataBindings.Add(new System.Windows.Forms.Binding("SelectedItem", this.osClienteBindingSource, "UF", true));
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.ItemHeight = 13;
            this.comboBox1.Items.AddRange(new object[] {
            "AC",
            "AL",
            "AP",
            "AM",
            "BA",
            "CE",
            "DF",
            "ES",
            "GO",
            "MA",
            "MT",
            "MS",
            "MG",
            "PA",
            "PB",
            "PR",
            "PE",
            "PI",
            "RJ",
            "RN",
            "RS",
            "RO",
            "RR",
            "SC",
            "SP",
            "SE",
            "TO"});
            this.comboBox1.Location = new System.Drawing.Point(170, 25);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(50, 21);
            this.comboBox1.TabIndex = 2;
            // 
            // cEPTextBox
            // 
            this.cEPTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osClienteBindingSource, "CEP", true));
            this.cEPTextBox.Location = new System.Drawing.Point(10, 25);
            this.cEPTextBox.MaxLength = 8;
            this.cEPTextBox.Name = "cEPTextBox";
            this.cEPTextBox.Size = new System.Drawing.Size(134, 20);
            this.cEPTextBox.TabIndex = 1;
            // 
            // tabContatos
            // 
            this.tabContatos.Controls.Add(this.checkboxPrincipal);
            this.tabContatos.Controls.Add(this.listView1);
            this.tabContatos.Controls.Add(labelContatosNumero);
            this.tabContatos.Controls.Add(this.checkBox1);
            this.tabContatos.Controls.Add(this.textboxContatosObs);
            this.tabContatos.Controls.Add(labelContatosObs);
            this.tabContatos.Controls.Add(labelContatosCPF);
            this.tabContatos.Controls.Add(this.maskedTextboxContatosCpf);
            this.tabContatos.Controls.Add(this.buttonContatosDeletar);
            this.tabContatos.Controls.Add(this.buttonContatosCancelar);
            this.tabContatos.Controls.Add(this.buttonContatosGravar);
            this.tabContatos.Controls.Add(labelContatosCelular);
            this.tabContatos.Controls.Add(this.maskedTextboxContatosCelular);
            this.tabContatos.Controls.Add(this.textboxContatosEmail);
            this.tabContatos.Controls.Add(labelContatosEmail);
            this.tabContatos.Controls.Add(this.textboxContatosNome);
            this.tabContatos.Controls.Add(labelContatosNome);
            this.tabContatos.Controls.Add(this.textboxHiddenId);
            this.tabContatos.Location = new System.Drawing.Point(4, 22);
            this.tabContatos.Name = "tabContatos";
            this.tabContatos.Padding = new System.Windows.Forms.Padding(3);
            this.tabContatos.Size = new System.Drawing.Size(972, 222);
            this.tabContatos.TabIndex = 1;
            this.tabContatos.Text = "Contatos";
            this.tabContatos.UseVisualStyleBackColor = true;
            this.tabContatos.Enter += new System.EventHandler(this.tabContatos_Enter);
            this.tabContatos.Leave += new System.EventHandler(this.tabContatos_Leave);
            // 
            // checkboxPrincipal
            // 
            this.checkboxPrincipal.AutoSize = true;
            this.checkboxPrincipal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkboxPrincipal.Location = new System.Drawing.Point(445, 103);
            this.checkboxPrincipal.Name = "checkboxPrincipal";
            this.checkboxPrincipal.Size = new System.Drawing.Size(65, 17);
            this.checkboxPrincipal.TabIndex = 37;
            this.checkboxPrincipal.Text = "principal";
            this.checkboxPrincipal.UseVisualStyleBackColor = true;
            // 
            // listView1
            // 
            this.listView1.Location = new System.Drawing.Point(15, 136);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(1020, 120);
            this.listView1.TabIndex = 36;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.Location = new System.Drawing.Point(444, 70);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(83, 17);
            this.checkBox1.TabIndex = 34;
            this.checkBox1.Text = "responsável";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // textboxContatosObs
            // 
            this.textboxContatosObs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textboxContatosObs.Location = new System.Drawing.Point(10, 70);
            this.textboxContatosObs.MaxLength = 50;
            this.textboxContatosObs.Name = "textboxContatosObs";
            this.textboxContatosObs.Size = new System.Drawing.Size(425, 20);
            this.textboxContatosObs.TabIndex = 25;
            // 
            // maskedTextboxContatosCpf
            // 
            this.maskedTextboxContatosCpf.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maskedTextboxContatosCpf.Location = new System.Drawing.Point(530, 70);
            this.maskedTextboxContatosCpf.Mask = "###.###.###-##";
            this.maskedTextboxContatosCpf.Name = "maskedTextboxContatosCpf";
            this.maskedTextboxContatosCpf.Size = new System.Drawing.Size(250, 20);
            this.maskedTextboxContatosCpf.TabIndex = 23;
            // 
            // buttonContatosDeletar
            // 
            this.buttonContatosDeletar.ForeColor = System.Drawing.Color.Red;
            this.buttonContatosDeletar.Location = new System.Drawing.Point(788, 105);
            this.buttonContatosDeletar.Name = "buttonContatosDeletar";
            this.buttonContatosDeletar.Size = new System.Drawing.Size(75, 23);
            this.buttonContatosDeletar.TabIndex = 21;
            this.buttonContatosDeletar.Text = "Deletar";
            this.buttonContatosDeletar.UseVisualStyleBackColor = true;
            this.buttonContatosDeletar.Visible = false;
            this.buttonContatosDeletar.Click += new System.EventHandler(this.buttonContatosDeletar_Click);
            // 
            // buttonContatosCancelar
            // 
            this.buttonContatosCancelar.Location = new System.Drawing.Point(788, 24);
            this.buttonContatosCancelar.Name = "buttonContatosCancelar";
            this.buttonContatosCancelar.Size = new System.Drawing.Size(75, 23);
            this.buttonContatosCancelar.TabIndex = 20;
            this.buttonContatosCancelar.Text = "Limpar";
            this.buttonContatosCancelar.UseVisualStyleBackColor = true;
            this.buttonContatosCancelar.Click += new System.EventHandler(this.buttonContatosCancelar_Click);
            // 
            // buttonContatosGravar
            // 
            this.buttonContatosGravar.Location = new System.Drawing.Point(788, 68);
            this.buttonContatosGravar.Name = "buttonContatosGravar";
            this.buttonContatosGravar.Size = new System.Drawing.Size(75, 23);
            this.buttonContatosGravar.TabIndex = 19;
            this.buttonContatosGravar.Text = "Gravar";
            this.buttonContatosGravar.UseVisualStyleBackColor = true;
            this.buttonContatosGravar.Click += new System.EventHandler(this.buttonContatosGravar_Click);
            // 
            // maskedTextboxContatosCelular
            // 
            this.maskedTextboxContatosCelular.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maskedTextboxContatosCelular.Location = new System.Drawing.Point(530, 25);
            this.maskedTextboxContatosCelular.Mask = "(##)#####-####";
            this.maskedTextboxContatosCelular.Name = "maskedTextboxContatosCelular";
            this.maskedTextboxContatosCelular.Size = new System.Drawing.Size(250, 20);
            this.maskedTextboxContatosCelular.TabIndex = 18;
            // 
            // textboxContatosEmail
            // 
            this.textboxContatosEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textboxContatosEmail.Location = new System.Drawing.Point(270, 25);
            this.textboxContatosEmail.MaxLength = 50;
            this.textboxContatosEmail.Name = "textboxContatosEmail";
            this.textboxContatosEmail.Size = new System.Drawing.Size(250, 20);
            this.textboxContatosEmail.TabIndex = 16;
            // 
            // textboxContatosNome
            // 
            this.textboxContatosNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textboxContatosNome.Location = new System.Drawing.Point(10, 25);
            this.textboxContatosNome.MaxLength = 50;
            this.textboxContatosNome.Name = "textboxContatosNome";
            this.textboxContatosNome.Size = new System.Drawing.Size(250, 20);
            this.textboxContatosNome.TabIndex = 14;
            // 
            // textboxHiddenId
            // 
            this.textboxHiddenId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textboxHiddenId.Location = new System.Drawing.Point(760, 44);
            this.textboxHiddenId.MaxLength = 50;
            this.textboxHiddenId.Name = "textboxHiddenId";
            this.textboxHiddenId.Size = new System.Drawing.Size(20, 20);
            this.textboxHiddenId.TabIndex = 12;
            this.textboxHiddenId.Visible = false;
            // 
            // tabOutros
            // 
            this.tabOutros.Controls.Add(this.textboxCodigoSimp);
            this.tabOutros.Controls.Add(labelCodigoSimp);
            this.tabOutros.Controls.Add(this.textboxQuantSumpers);
            this.tabOutros.Controls.Add(this.textBox2);
            this.tabOutros.Controls.Add(this.textboxQuantTanques);
            this.tabOutros.Controls.Add(this.checkboxRFID);
            this.tabOutros.Controls.Add(this.labelConsentrador);
            this.tabOutros.Controls.Add(this.labelQuantSumpers);
            this.tabOutros.Controls.Add(this.labelQuantTanques);
            this.tabOutros.Controls.Add(tIPO_BOMBALabel);
            this.tabOutros.Controls.Add(this.qTDE_BOMBASTextBox);
            this.tabOutros.Controls.Add(this.tIPO_BOMBATextBox);
            this.tabOutros.Controls.Add(this.qTDE_BICOSTextBox);
            this.tabOutros.Controls.Add(qTDE_BICOSLabel);
            this.tabOutros.Controls.Add(qTDE_BOMBASLabel);
            this.tabOutros.Controls.Add(this.bANDEIRATextBox);
            this.tabOutros.Controls.Add(bANDEIRALabel);
            this.tabOutros.Controls.Add(this.mODELO_CPUTextBox);
            this.tabOutros.Controls.Add(mODELO_CPULabel);
            this.tabOutros.Location = new System.Drawing.Point(4, 22);
            this.tabOutros.Name = "tabOutros";
            this.tabOutros.Size = new System.Drawing.Size(972, 222);
            this.tabOutros.TabIndex = 2;
            this.tabOutros.Text = "   Outros";
            this.tabOutros.UseVisualStyleBackColor = true;
            // 
            // textboxCodigoSimp
            // 
            this.textboxCodigoSimp.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osClienteBindingSource, "CODIGO_SIMP", true));
            this.textboxCodigoSimp.Location = new System.Drawing.Point(510, 25);
            this.textboxCodigoSimp.MaxLength = 50;
            this.textboxCodigoSimp.Name = "textboxCodigoSimp";
            this.textboxCodigoSimp.Size = new System.Drawing.Size(220, 20);
            this.textboxCodigoSimp.TabIndex = 62;
            // 
            // textboxQuantSumpers
            // 
            this.textboxQuantSumpers.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osClienteBindingSource, "QTDE_SUMPERS", true));
            this.textboxQuantSumpers.Location = new System.Drawing.Point(260, 125);
            this.textboxQuantSumpers.MaxLength = 3;
            this.textboxQuantSumpers.Name = "textboxQuantSumpers";
            this.textboxQuantSumpers.Size = new System.Drawing.Size(220, 20);
            this.textboxQuantSumpers.TabIndex = 60;
            // 
            // textBox2
            // 
            this.textBox2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osClienteBindingSource, "concentrador", true));
            this.textBox2.Location = new System.Drawing.Point(510, 125);
            this.textBox2.MaxLength = 50;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(220, 20);
            this.textBox2.TabIndex = 59;
            // 
            // textboxQuantTanques
            // 
            this.textboxQuantTanques.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osClienteBindingSource, "QTDE_TANQUES", true));
            this.textboxQuantTanques.Location = new System.Drawing.Point(10, 125);
            this.textboxQuantTanques.MaxLength = 3;
            this.textboxQuantTanques.Name = "textboxQuantTanques";
            this.textboxQuantTanques.Size = new System.Drawing.Size(220, 20);
            this.textboxQuantTanques.TabIndex = 58;
            // 
            // checkboxRFID
            // 
            this.checkboxRFID.AutoSize = true;
            this.checkboxRFID.Location = new System.Drawing.Point(750, 25);
            this.checkboxRFID.Name = "checkboxRFID";
            this.checkboxRFID.Size = new System.Drawing.Size(91, 17);
            this.checkboxRFID.TabIndex = 57;
            this.checkboxRFID.Text = "Possui RFID?";
            this.checkboxRFID.UseVisualStyleBackColor = true;
            // 
            // labelConsentrador
            // 
            this.labelConsentrador.AutoSize = true;
            this.labelConsentrador.Location = new System.Drawing.Point(510, 110);
            this.labelConsentrador.Name = "labelConsentrador";
            this.labelConsentrador.Size = new System.Drawing.Size(128, 13);
            this.labelConsentrador.TabIndex = 56;
            this.labelConsentrador.Text = "Concentrador Automação";
            // 
            // labelQuantSumpers
            // 
            this.labelQuantSumpers.AutoSize = true;
            this.labelQuantSumpers.Location = new System.Drawing.Point(260, 110);
            this.labelQuantSumpers.Name = "labelQuantSumpers";
            this.labelQuantSumpers.Size = new System.Drawing.Size(77, 13);
            this.labelQuantSumpers.TabIndex = 55;
            this.labelQuantSumpers.Text = "Qtde. Sumpers";
            // 
            // labelQuantTanques
            // 
            this.labelQuantTanques.AutoSize = true;
            this.labelQuantTanques.Location = new System.Drawing.Point(10, 110);
            this.labelQuantTanques.Name = "labelQuantTanques";
            this.labelQuantTanques.Size = new System.Drawing.Size(78, 13);
            this.labelQuantTanques.TabIndex = 54;
            this.labelQuantTanques.Text = "Qtde. Tanques";
            // 
            // qTDE_BOMBASTextBox
            // 
            this.qTDE_BOMBASTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osClienteBindingSource, "QTDE_BOMBAS", true));
            this.qTDE_BOMBASTextBox.Location = new System.Drawing.Point(10, 75);
            this.qTDE_BOMBASTextBox.MaxLength = 3;
            this.qTDE_BOMBASTextBox.Name = "qTDE_BOMBASTextBox";
            this.qTDE_BOMBASTextBox.Size = new System.Drawing.Size(220, 20);
            this.qTDE_BOMBASTextBox.TabIndex = 48;
            this.qTDE_BOMBASTextBox.Text = " ";
            this.qTDE_BOMBASTextBox.TextChanged += new System.EventHandler(this.qTDE_BOMBASTextBox_TextChanged);
            this.qTDE_BOMBASTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.qTDE_BOMBASTextBox_KeyPress);
            this.qTDE_BOMBASTextBox.Leave += new System.EventHandler(this.qTDE_BOMBASTextBox_Leave);
            // 
            // tIPO_BOMBATextBox
            // 
            this.tIPO_BOMBATextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osClienteBindingSource, "TIPO_BOMBA", true));
            this.tIPO_BOMBATextBox.Location = new System.Drawing.Point(510, 75);
            this.tIPO_BOMBATextBox.MaxLength = 20;
            this.tIPO_BOMBATextBox.Name = "tIPO_BOMBATextBox";
            this.tIPO_BOMBATextBox.Size = new System.Drawing.Size(220, 20);
            this.tIPO_BOMBATextBox.TabIndex = 47;
            // 
            // qTDE_BICOSTextBox
            // 
            this.qTDE_BICOSTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osClienteBindingSource, "QTDE_BICOS", true));
            this.qTDE_BICOSTextBox.Location = new System.Drawing.Point(260, 75);
            this.qTDE_BICOSTextBox.MaxLength = 5;
            this.qTDE_BICOSTextBox.Name = "qTDE_BICOSTextBox";
            this.qTDE_BICOSTextBox.Size = new System.Drawing.Size(220, 20);
            this.qTDE_BICOSTextBox.TabIndex = 49;
            this.qTDE_BICOSTextBox.Text = "  ";
            this.qTDE_BICOSTextBox.TextChanged += new System.EventHandler(this.qTDE_BICOSTextBox_TextChanged);
            this.qTDE_BICOSTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.qTDE_BICOSTextBox_KeyPress);
            this.qTDE_BICOSTextBox.Leave += new System.EventHandler(this.qTDE_BICOSTextBox_Leave);
            // 
            // bANDEIRATextBox
            // 
            this.bANDEIRATextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osClienteBindingSource, "BANDEIRA", true));
            this.bANDEIRATextBox.Location = new System.Drawing.Point(10, 25);
            this.bANDEIRATextBox.MaxLength = 20;
            this.bANDEIRATextBox.Name = "bANDEIRATextBox";
            this.bANDEIRATextBox.Size = new System.Drawing.Size(220, 20);
            this.bANDEIRATextBox.TabIndex = 43;
            // 
            // mODELO_CPUTextBox
            // 
            this.mODELO_CPUTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osClienteBindingSource, "MODELO_CPU", true));
            this.mODELO_CPUTextBox.Location = new System.Drawing.Point(260, 25);
            this.mODELO_CPUTextBox.MaxLength = 50;
            this.mODELO_CPUTextBox.Name = "mODELO_CPUTextBox";
            this.mODELO_CPUTextBox.Size = new System.Drawing.Size(220, 20);
            this.mODELO_CPUTextBox.TabIndex = 44;
            // 
            // tabObservacoes
            // 
            this.tabObservacoes.Controls.Add(this.oBSERVACAOTextBox);
            this.tabObservacoes.Controls.Add(this.cODIGO_SIGPOSTOTextBox);
            this.tabObservacoes.Controls.Add(cODIGO_SIGPOSTOLabel);
            this.tabObservacoes.Location = new System.Drawing.Point(4, 22);
            this.tabObservacoes.Name = "tabObservacoes";
            this.tabObservacoes.Size = new System.Drawing.Size(972, 222);
            this.tabObservacoes.TabIndex = 3;
            this.tabObservacoes.Text = "Observações";
            this.tabObservacoes.UseVisualStyleBackColor = true;
            // 
            // oBSERVACAOTextBox
            // 
            this.oBSERVACAOTextBox.AcceptsReturn = true;
            this.oBSERVACAOTextBox.AcceptsTab = true;
            this.oBSERVACAOTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osClienteBindingSource, "OBSERVACAO", true));
            this.oBSERVACAOTextBox.Location = new System.Drawing.Point(18, 21);
            this.oBSERVACAOTextBox.MaxLength = 200;
            this.oBSERVACAOTextBox.Multiline = true;
            this.oBSERVACAOTextBox.Name = "oBSERVACAOTextBox";
            this.oBSERVACAOTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.oBSERVACAOTextBox.Size = new System.Drawing.Size(940, 180);
            this.oBSERVACAOTextBox.TabIndex = 1;
            // 
            // cODIGO_SIGPOSTOTextBox
            // 
            this.cODIGO_SIGPOSTOTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osClienteBindingSource, "CODIGO_SIGPOSTO", true));
            this.cODIGO_SIGPOSTOTextBox.Enabled = false;
            this.cODIGO_SIGPOSTOTextBox.Location = new System.Drawing.Point(526, 186);
            this.cODIGO_SIGPOSTOTextBox.Name = "cODIGO_SIGPOSTOTextBox";
            this.cODIGO_SIGPOSTOTextBox.ReadOnly = true;
            this.cODIGO_SIGPOSTOTextBox.Size = new System.Drawing.Size(220, 20);
            this.cODIGO_SIGPOSTOTextBox.TabIndex = 63;
            this.cODIGO_SIGPOSTOTextBox.Visible = false;
            // 
            // tabEventos
            // 
            this.tabEventos.Controls.Add(this.btnListEventos);
            this.tabEventos.Controls.Add(this.textboxHiddenIdEvento);
            this.tabEventos.Controls.Add(labelEventosNumero);
            this.tabEventos.Controls.Add(this.buttonEventosDeletar);
            this.tabEventos.Controls.Add(this.buttonEventosGravar);
            this.tabEventos.Controls.Add(this.buttonEventosCancelar);
            this.tabEventos.Controls.Add(this.datetimeRealizacao);
            this.tabEventos.Controls.Add(this.listviewEventos);
            this.tabEventos.Controls.Add(this.checkboxAlertar);
            this.tabEventos.Controls.Add(this.textboxApontamento);
            this.tabEventos.Controls.Add(this.labelApontamento);
            this.tabEventos.Controls.Add(this.labelDataEHora);
            this.tabEventos.Controls.Add(this.labelEM);
            this.tabEventos.Location = new System.Drawing.Point(4, 22);
            this.tabEventos.Name = "tabEventos";
            this.tabEventos.Size = new System.Drawing.Size(972, 222);
            this.tabEventos.TabIndex = 4;
            this.tabEventos.Text = "Eventos";
            this.tabEventos.UseVisualStyleBackColor = true;
            this.tabEventos.Enter += new System.EventHandler(this.tabEventos_Enter);
            this.tabEventos.Leave += new System.EventHandler(this.tabEventos_Leave);
            // 
            // btnListEventos
            // 
            this.btnListEventos.Location = new System.Drawing.Point(138, 68);
            this.btnListEventos.Margin = new System.Windows.Forms.Padding(2);
            this.btnListEventos.Name = "btnListEventos";
            this.btnListEventos.Size = new System.Drawing.Size(130, 19);
            this.btnListEventos.TabIndex = 38;
            this.btnListEventos.Text = "Listar Eventos";
            this.btnListEventos.UseVisualStyleBackColor = true;
            this.btnListEventos.Click += new System.EventHandler(this.btnListEventos_Click);
            // 
            // textboxHiddenIdEvento
            // 
            this.textboxHiddenIdEvento.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textboxHiddenIdEvento.Location = new System.Drawing.Point(250, 56);
            this.textboxHiddenIdEvento.MaxLength = 50;
            this.textboxHiddenIdEvento.Name = "textboxHiddenIdEvento";
            this.textboxHiddenIdEvento.Size = new System.Drawing.Size(20, 20);
            this.textboxHiddenIdEvento.TabIndex = 37;
            this.textboxHiddenIdEvento.Visible = false;
            // 
            // buttonEventosDeletar
            // 
            this.buttonEventosDeletar.ForeColor = System.Drawing.Color.Red;
            this.buttonEventosDeletar.Location = new System.Drawing.Point(835, 77);
            this.buttonEventosDeletar.Name = "buttonEventosDeletar";
            this.buttonEventosDeletar.Size = new System.Drawing.Size(75, 23);
            this.buttonEventosDeletar.TabIndex = 23;
            this.buttonEventosDeletar.Text = "Deletar";
            this.buttonEventosDeletar.UseVisualStyleBackColor = true;
            this.buttonEventosDeletar.Visible = false;
            this.buttonEventosDeletar.Click += new System.EventHandler(this.buttonEventosDeletar_Click);
            // 
            // buttonEventosGravar
            // 
            this.buttonEventosGravar.Location = new System.Drawing.Point(835, 48);
            this.buttonEventosGravar.Name = "buttonEventosGravar";
            this.buttonEventosGravar.Size = new System.Drawing.Size(75, 23);
            this.buttonEventosGravar.TabIndex = 22;
            this.buttonEventosGravar.Text = "Gravar";
            this.buttonEventosGravar.UseVisualStyleBackColor = true;
            this.buttonEventosGravar.Click += new System.EventHandler(this.buttonEventosGravar_Click);
            // 
            // buttonEventosCancelar
            // 
            this.buttonEventosCancelar.Location = new System.Drawing.Point(835, 19);
            this.buttonEventosCancelar.Name = "buttonEventosCancelar";
            this.buttonEventosCancelar.Size = new System.Drawing.Size(75, 23);
            this.buttonEventosCancelar.TabIndex = 21;
            this.buttonEventosCancelar.Text = "Limpar";
            this.buttonEventosCancelar.UseVisualStyleBackColor = true;
            this.buttonEventosCancelar.Click += new System.EventHandler(this.buttonEventosCancelar_Click);
            // 
            // datetimeRealizacao
            // 
            this.datetimeRealizacao.CustomFormat = "dd/MM/yyyy HH:mm";
            this.datetimeRealizacao.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datetimeRealizacao.Location = new System.Drawing.Point(50, 30);
            this.datetimeRealizacao.Name = "datetimeRealizacao";
            this.datetimeRealizacao.Size = new System.Drawing.Size(220, 20);
            this.datetimeRealizacao.TabIndex = 1;
            this.datetimeRealizacao.ValueChanged += new System.EventHandler(this.datetimeRealizacao_ValueChanged);
            // 
            // listviewEventos
            // 
            this.listviewEventos.Location = new System.Drawing.Point(15, 110);
            this.listviewEventos.Name = "listviewEventos";
            this.listviewEventos.Size = new System.Drawing.Size(940, 105);
            this.listviewEventos.TabIndex = 6;
            this.listviewEventos.UseCompatibleStateImageBehavior = false;
            this.listviewEventos.SelectedIndexChanged += new System.EventHandler(this.listviewEventos_SelectedIndexChanged);
            // 
            // checkboxAlertar
            // 
            this.checkboxAlertar.AutoSize = true;
            this.checkboxAlertar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkboxAlertar.Location = new System.Drawing.Point(20, 70);
            this.checkboxAlertar.Name = "checkboxAlertar";
            this.checkboxAlertar.Size = new System.Drawing.Size(74, 17);
            this.checkboxAlertar.TabIndex = 3;
            this.checkboxAlertar.Text = "Alertar ?";
            this.checkboxAlertar.UseVisualStyleBackColor = true;
            // 
            // textboxApontamento
            // 
            this.textboxApontamento.AcceptsReturn = true;
            this.textboxApontamento.AcceptsTab = true;
            this.textboxApontamento.Location = new System.Drawing.Point(300, 30);
            this.textboxApontamento.Multiline = true;
            this.textboxApontamento.Name = "textboxApontamento";
            this.textboxApontamento.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textboxApontamento.Size = new System.Drawing.Size(500, 70);
            this.textboxApontamento.TabIndex = 2;
            // 
            // labelApontamento
            // 
            this.labelApontamento.AutoSize = true;
            this.labelApontamento.Location = new System.Drawing.Point(300, 10);
            this.labelApontamento.Name = "labelApontamento";
            this.labelApontamento.Size = new System.Drawing.Size(70, 13);
            this.labelApontamento.TabIndex = 2;
            this.labelApontamento.Text = "Apontamento";
            // 
            // labelDataEHora
            // 
            this.labelDataEHora.AutoSize = true;
            this.labelDataEHora.Location = new System.Drawing.Point(60, 10);
            this.labelDataEHora.Name = "labelDataEHora";
            this.labelDataEHora.Size = new System.Drawing.Size(65, 13);
            this.labelDataEHora.TabIndex = 1;
            this.labelDataEHora.Text = "Data e Hora";
            // 
            // labelEM
            // 
            this.labelEM.AutoSize = true;
            this.labelEM.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEM.Location = new System.Drawing.Point(15, 35);
            this.labelEM.Name = "labelEM";
            this.labelEM.Size = new System.Drawing.Size(29, 13);
            this.labelEM.TabIndex = 0;
            this.labelEM.Text = "EM:";
            // 
            // tabOrcamentos
            // 
            this.tabOrcamentos.Controls.Add(this.dataGridViewOrcamentos);
            this.tabOrcamentos.Location = new System.Drawing.Point(4, 22);
            this.tabOrcamentos.Name = "tabOrcamentos";
            this.tabOrcamentos.Size = new System.Drawing.Size(972, 222);
            this.tabOrcamentos.TabIndex = 5;
            this.tabOrcamentos.Text = "Orçamentos";
            this.tabOrcamentos.UseVisualStyleBackColor = true;
            this.tabOrcamentos.Enter += new System.EventHandler(this.tabOrcamentos_Enter);
            // 
            // dataGridViewOrcamentos
            // 
            this.dataGridViewOrcamentos.AllowUserToAddRows = false;
            this.dataGridViewOrcamentos.AllowUserToDeleteRows = false;
            this.dataGridViewOrcamentos.AllowUserToOrderColumns = true;
            this.dataGridViewOrcamentos.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dataGridViewOrcamentos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewOrcamentos.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewOrcamentos.Name = "dataGridViewOrcamentos";
            this.dataGridViewOrcamentos.ReadOnly = true;
            this.dataGridViewOrcamentos.Size = new System.Drawing.Size(550, 222);
            this.dataGridViewOrcamentos.TabIndex = 1;
            this.dataGridViewOrcamentos.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewOrcamentos_CellDoubleClick);
            this.dataGridViewOrcamentos.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridViewOrcamentos_CellFormatting);
            this.dataGridViewOrcamentos.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dataGridViewOrcamentos_DataBindingComplete);
            // 
            // tabPedidos
            // 
            this.tabPedidos.Controls.Add(this.dataGridViewPedidos);
            this.tabPedidos.Location = new System.Drawing.Point(4, 22);
            this.tabPedidos.Name = "tabPedidos";
            this.tabPedidos.Size = new System.Drawing.Size(972, 222);
            this.tabPedidos.TabIndex = 6;
            this.tabPedidos.Text = "Pedidos";
            this.tabPedidos.UseVisualStyleBackColor = true;
            this.tabPedidos.Enter += new System.EventHandler(this.tabPedidos_Enter);
            // 
            // dataGridViewPedidos
            // 
            this.dataGridViewPedidos.AllowUserToAddRows = false;
            this.dataGridViewPedidos.AllowUserToDeleteRows = false;
            this.dataGridViewPedidos.AllowUserToOrderColumns = true;
            this.dataGridViewPedidos.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dataGridViewPedidos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPedidos.Location = new System.Drawing.Point(211, 0);
            this.dataGridViewPedidos.Name = "dataGridViewPedidos";
            this.dataGridViewPedidos.ReadOnly = true;
            this.dataGridViewPedidos.Size = new System.Drawing.Size(550, 222);
            this.dataGridViewPedidos.TabIndex = 2;
            this.dataGridViewPedidos.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewOrcamentos_CellDoubleClick);
            this.dataGridViewPedidos.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridViewOrcamentos_CellFormatting);
            this.dataGridViewPedidos.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dataGridViewOrcamentos_DataBindingComplete);
            // 
            // tabServicos
            // 
            this.tabServicos.Controls.Add(this.dataGridViewServicos);
            this.tabServicos.Location = new System.Drawing.Point(4, 22);
            this.tabServicos.Name = "tabServicos";
            this.tabServicos.Size = new System.Drawing.Size(972, 222);
            this.tabServicos.TabIndex = 7;
            this.tabServicos.Text = "Serviços";
            this.tabServicos.UseVisualStyleBackColor = true;
            this.tabServicos.Enter += new System.EventHandler(this.tabServicos_Enter);
            // 
            // dataGridViewServicos
            // 
            this.dataGridViewServicos.AllowUserToAddRows = false;
            this.dataGridViewServicos.AllowUserToDeleteRows = false;
            this.dataGridViewServicos.AllowUserToOrderColumns = true;
            this.dataGridViewServicos.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dataGridViewServicos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewServicos.Location = new System.Drawing.Point(211, 0);
            this.dataGridViewServicos.Name = "dataGridViewServicos";
            this.dataGridViewServicos.ReadOnly = true;
            this.dataGridViewServicos.Size = new System.Drawing.Size(550, 222);
            this.dataGridViewServicos.TabIndex = 2;
            this.dataGridViewServicos.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewOrcamentos_CellDoubleClick);
            this.dataGridViewServicos.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridViewOrcamentos_CellFormatting);
            this.dataGridViewServicos.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dataGridViewOrcamentos_DataBindingComplete);
            // 
            // tabInstrumentos
            // 
            this.tabInstrumentos.Controls.Add(this.rtbHelp2);
            this.tabInstrumentos.Controls.Add(this.rtbHelp0);
            this.tabInstrumentos.Controls.Add(this.btnLimparBomba);
            this.tabInstrumentos.Controls.Add(this.pnlEditarBomba);
            this.tabInstrumentos.Controls.Add(this.dgvBombas);
            this.tabInstrumentos.Location = new System.Drawing.Point(4, 22);
            this.tabInstrumentos.Name = "tabInstrumentos";
            this.tabInstrumentos.Size = new System.Drawing.Size(972, 222);
            this.tabInstrumentos.TabIndex = 8;
            this.tabInstrumentos.Text = "Instrumentos";
            this.tabInstrumentos.UseVisualStyleBackColor = true;
            this.tabInstrumentos.Enter += new System.EventHandler(this.tabInstrumentos_Enter);
            this.tabInstrumentos.Leave += new System.EventHandler(this.tabInstrumentos_Leave);
            // 
            // rtbHelp2
            // 
            this.rtbHelp2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.rtbHelp2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbHelp2.Location = new System.Drawing.Point(0, 32);
            this.rtbHelp2.Name = "rtbHelp2";
            this.rtbHelp2.Size = new System.Drawing.Size(35, 15);
            this.rtbHelp2.TabIndex = 12;
            this.rtbHelp2.Text = resources.GetString("rtbHelp2.Text");
            this.rtbHelp2.Visible = false;
            this.rtbHelp2.Click += new System.EventHandler(this.rtbHelp2_Click);
            // 
            // rtbHelp0
            // 
            this.rtbHelp0.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.rtbHelp0.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbHelp0.Cursor = System.Windows.Forms.Cursors.Default;
            this.rtbHelp0.Location = new System.Drawing.Point(18, 200);
            this.rtbHelp0.Name = "rtbHelp0";
            this.rtbHelp0.Size = new System.Drawing.Size(245, 120);
            this.rtbHelp0.TabIndex = 3;
            this.rtbHelp0.TabStop = false;
            this.rtbHelp0.Text = resources.GetString("rtbHelp0.Text");
            // 
            // btnLimparBomba
            // 
            this.btnLimparBomba.Location = new System.Drawing.Point(496, 121);
            this.btnLimparBomba.Name = "btnLimparBomba";
            this.btnLimparBomba.Size = new System.Drawing.Size(85, 23);
            this.btnLimparBomba.TabIndex = 11;
            this.btnLimparBomba.Text = "&Nova Bomba";
            this.btnLimparBomba.UseVisualStyleBackColor = true;
            this.btnLimparBomba.Click += new System.EventHandler(this.btnLimparBomba_Click);
            // 
            // pnlEditarBomba
            // 
            this.pnlEditarBomba.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlEditarBomba.Controls.Add(this.btnCancelar);
            this.pnlEditarBomba.Controls.Add(this.rtbHelp1);
            this.pnlEditarBomba.Controls.Add(this.btnGerarLacre);
            this.pnlEditarBomba.Controls.Add(this.btnDeletarBomba);
            this.pnlEditarBomba.Controls.Add(this.btnGravarBomba);
            this.pnlEditarBomba.Controls.Add(this.dgvLacre);
            this.pnlEditarBomba.Controls.Add(lblLacre);
            this.pnlEditarBomba.Controls.Add(this.tbLacre);
            this.pnlEditarBomba.Controls.Add(lblFabricante);
            this.pnlEditarBomba.Controls.Add(this.tbFabricante);
            this.pnlEditarBomba.Controls.Add(lblDescricao);
            this.pnlEditarBomba.Controls.Add(this.tbDescricao);
            this.pnlEditarBomba.Controls.Add(lblIlha);
            this.pnlEditarBomba.Controls.Add(this.tbIlha);
            this.pnlEditarBomba.Controls.Add(lblQBicos);
            this.pnlEditarBomba.Controls.Add(this.tbQBicos);
            this.pnlEditarBomba.Controls.Add(lblSerie);
            this.pnlEditarBomba.Controls.Add(this.tbSerie);
            this.pnlEditarBomba.Controls.Add(lblMarcaInstrumento);
            this.pnlEditarBomba.Controls.Add(this.cmbMarcaInstrumento);
            this.pnlEditarBomba.Location = new System.Drawing.Point(90, 10);
            this.pnlEditarBomba.Name = "pnlEditarBomba";
            this.pnlEditarBomba.Size = new System.Drawing.Size(600, 325);
            this.pnlEditarBomba.TabIndex = 1;
            this.pnlEditarBomba.Visible = false;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(400, 166);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(85, 23);
            this.btnCancelar.TabIndex = 56;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // rtbHelp1
            // 
            this.rtbHelp1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.rtbHelp1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbHelp1.Cursor = System.Windows.Forms.Cursors.Default;
            this.rtbHelp1.Location = new System.Drawing.Point(19, 170);
            this.rtbHelp1.Name = "rtbHelp1";
            this.rtbHelp1.Size = new System.Drawing.Size(245, 80);
            this.rtbHelp1.TabIndex = 2;
            this.rtbHelp1.TabStop = false;
            this.rtbHelp1.Text = "Os lacres gerados ficam editaveis no DataGridView.\nEditar e acionar <ENTER> para " +
    "salvar.\nPara deletar um lacre selecione a e acione  a tecla <DELETE>.";
            // 
            // btnGerarLacre
            // 
            this.btnGerarLacre.Location = new System.Drawing.Point(19, 142);
            this.btnGerarLacre.Name = "btnGerarLacre";
            this.btnGerarLacre.Size = new System.Drawing.Size(85, 23);
            this.btnGerarLacre.TabIndex = 55;
            this.btnGerarLacre.Text = "Inclui&r Lacre";
            this.btnGerarLacre.UseVisualStyleBackColor = true;
            this.btnGerarLacre.Click += new System.EventHandler(this.btnGerarLacre_Click);
            // 
            // btnDeletarBomba
            // 
            this.btnDeletarBomba.ForeColor = System.Drawing.Color.Red;
            this.btnDeletarBomba.Location = new System.Drawing.Point(493, 166);
            this.btnDeletarBomba.Name = "btnDeletarBomba";
            this.btnDeletarBomba.Size = new System.Drawing.Size(85, 23);
            this.btnDeletarBomba.TabIndex = 12;
            this.btnDeletarBomba.Text = "&Deletar Bomba";
            this.btnDeletarBomba.UseVisualStyleBackColor = true;
            this.btnDeletarBomba.Click += new System.EventHandler(this.btnDeletarBomba_Click);
            // 
            // btnGravarBomba
            // 
            this.btnGravarBomba.Location = new System.Drawing.Point(20, 175);
            this.btnGravarBomba.Name = "btnGravarBomba";
            this.btnGravarBomba.Size = new System.Drawing.Size(85, 23);
            this.btnGravarBomba.TabIndex = 10;
            this.btnGravarBomba.Text = "&Gravar Bomba";
            this.btnGravarBomba.UseVisualStyleBackColor = true;
            this.btnGravarBomba.Click += new System.EventHandler(this.btnGravarBomba_Click);
            // 
            // dgvLacre
            // 
            this.dgvLacre.AllowUserToAddRows = false;
            this.dgvLacre.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLacre.Location = new System.Drawing.Point(312, 100);
            this.dgvLacre.Name = "dgvLacre";
            this.dgvLacre.Size = new System.Drawing.Size(240, 190);
            this.dgvLacre.TabIndex = 8;
            this.dgvLacre.TabStop = false;
            this.dgvLacre.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvLacre_CellBeginEdit);
            this.dgvLacre.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvLacre_CellEndEdit);
            this.dgvLacre.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvLacre_DataBindingComplete);
            this.dgvLacre.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvLacre_KeyDown);
            // 
            // tbLacre
            // 
            this.tbLacre.Location = new System.Drawing.Point(19, 116);
            this.tbLacre.MaxLength = 15;
            this.tbLacre.Name = "tbLacre";
            this.tbLacre.Size = new System.Drawing.Size(180, 20);
            this.tbLacre.TabIndex = 7;
            // 
            // tbFabricante
            // 
            this.tbFabricante.Location = new System.Drawing.Point(211, 65);
            this.tbFabricante.Name = "tbFabricante";
            this.tbFabricante.Size = new System.Drawing.Size(180, 20);
            this.tbFabricante.TabIndex = 5;
            // 
            // tbDescricao
            // 
            this.tbDescricao.Location = new System.Drawing.Point(19, 65);
            this.tbDescricao.Name = "tbDescricao";
            this.tbDescricao.Size = new System.Drawing.Size(180, 20);
            this.tbDescricao.TabIndex = 4;
            // 
            // tbIlha
            // 
            this.tbIlha.Location = new System.Drawing.Point(453, 21);
            this.tbIlha.Name = "tbIlha";
            this.tbIlha.Size = new System.Drawing.Size(100, 20);
            this.tbIlha.TabIndex = 3;
            // 
            // tbQBicos
            // 
            this.tbQBicos.Location = new System.Drawing.Point(349, 21);
            this.tbQBicos.MaxLength = 1;
            this.tbQBicos.Name = "tbQBicos";
            this.tbQBicos.Size = new System.Drawing.Size(100, 20);
            this.tbQBicos.TabIndex = 2;
            this.tbQBicos.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbQBicos_KeyPress);
            // 
            // tbSerie
            // 
            this.tbSerie.Location = new System.Drawing.Point(19, 21);
            this.tbSerie.Name = "tbSerie";
            this.tbSerie.Size = new System.Drawing.Size(316, 20);
            this.tbSerie.TabIndex = 1;
            // 
            // cmbMarcaInstrumento
            // 
            this.cmbMarcaInstrumento.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbMarcaInstrumento.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbMarcaInstrumento.FormattingEnabled = true;
            this.cmbMarcaInstrumento.Items.AddRange(new object[] {
            "Daruma",
            "Extratema",
            "GBR Veeder Root",
            "Gilbarco Mecânica",
            "Gilbarco Pro",
            "Gilbarco Pulma Fit",
            "Tokheim",
            "Wayne 3G",
            "Wayne Duplex",
            "Wayne Mecânica",
            "Wayne Minnow"});
            this.cmbMarcaInstrumento.Location = new System.Drawing.Point(402, 64);
            this.cmbMarcaInstrumento.MaxLength = 30;
            this.cmbMarcaInstrumento.Name = "cmbMarcaInstrumento";
            this.cmbMarcaInstrumento.Size = new System.Drawing.Size(151, 21);
            this.cmbMarcaInstrumento.Sorted = true;
            this.cmbMarcaInstrumento.TabIndex = 6;
            // 
            // dgvBombas
            // 
            this.dgvBombas.AllowUserToAddRows = false;
            this.dgvBombas.AllowUserToDeleteRows = false;
            this.dgvBombas.AllowUserToOrderColumns = true;
            this.dgvBombas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBombas.Location = new System.Drawing.Point(15, 10);
            this.dgvBombas.Name = "dgvBombas";
            this.dgvBombas.ReadOnly = true;
            this.dgvBombas.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvBombas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvBombas.Size = new System.Drawing.Size(440, 150);
            this.dgvBombas.TabIndex = 0;
            this.dgvBombas.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvBombas_CellDoubleClick);
            this.dgvBombas.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvBombas_DataBindingComplete);
            this.dgvBombas.SelectionChanged += new System.EventHandler(this.dgvBombas_SelectionChanged);
            this.dgvBombas.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvBombas_KeyDown);
            // 
            // rAZAO_SOCIALTextBox
            // 
            this.rAZAO_SOCIALTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osClienteBindingSource, "RAZAO_SOCIAL", true));
            this.rAZAO_SOCIALTextBox.Location = new System.Drawing.Point(100, 115);
            this.rAZAO_SOCIALTextBox.MaxLength = 100;
            this.rAZAO_SOCIALTextBox.Name = "rAZAO_SOCIALTextBox";
            this.rAZAO_SOCIALTextBox.Size = new System.Drawing.Size(610, 20);
            this.rAZAO_SOCIALTextBox.TabIndex = 4;
            // 
            // nOME_FANTASIATextBox
            // 
            this.nOME_FANTASIATextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osClienteBindingSource, "NOME_FANTASIA", true));
            this.nOME_FANTASIATextBox.Location = new System.Drawing.Point(100, 155);
            this.nOME_FANTASIATextBox.MaxLength = 30;
            this.nOME_FANTASIATextBox.Name = "nOME_FANTASIATextBox";
            this.nOME_FANTASIATextBox.Size = new System.Drawing.Size(610, 20);
            this.nOME_FANTASIATextBox.TabIndex = 5;
            // 
            // mtbCPF
            // 
            this.mtbCPF.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osClienteBindingSource, "CPF", true));
            this.mtbCPF.Location = new System.Drawing.Point(110, 25);
            this.mtbCPF.Mask = "###.###.###-##";
            this.mtbCPF.Name = "mtbCPF";
            this.mtbCPF.Size = new System.Drawing.Size(220, 20);
            this.mtbCPF.TabIndex = 2;
            // 
            // mtbCNPJ
            // 
            this.mtbCNPJ.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osClienteBindingSource, "CNPJ", true));
            this.mtbCNPJ.Location = new System.Drawing.Point(110, 70);
            this.mtbCNPJ.Mask = "##.###.###/####-##";
            this.mtbCNPJ.Name = "mtbCNPJ";
            this.mtbCNPJ.Size = new System.Drawing.Size(220, 20);
            this.mtbCNPJ.TabIndex = 2;
            // 
            // comboBox2
            // 
            this.comboBox2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox2.DataBindings.Add(new System.Windows.Forms.Binding("SelectedItem", this.osClienteBindingSource, "TIPO_PESSOA", true));
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "F",
            "J"});
            this.comboBox2.Location = new System.Drawing.Point(25, 25);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(38, 21);
            this.comboBox2.TabIndex = 1;
            this.comboBox2.SelectionChangeCommitted += new System.EventHandler(this.comboBox2_SelectionChangeCommitted);
            // 
            // iDTextBox
            // 
            this.iDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osClienteBindingSource, "ID", true));
            this.iDTextBox.Enabled = false;
            this.iDTextBox.Location = new System.Drawing.Point(25, 115);
            this.iDTextBox.Name = "iDTextBox";
            this.iDTextBox.ReadOnly = true;
            this.iDTextBox.Size = new System.Drawing.Size(50, 20);
            this.iDTextBox.TabIndex = 1;
            // 
            // iETextBox
            // 
            this.iETextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osClienteBindingSource, "IE", true));
            this.iETextBox.Location = new System.Drawing.Point(360, 70);
            this.iETextBox.MaxLength = 15;
            this.iETextBox.Name = "iETextBox";
            this.iETextBox.Size = new System.Drawing.Size(243, 20);
            this.iETextBox.TabIndex = 3;
            this.iETextBox.TextChanged += new System.EventHandler(this.iETextBox_TextChanged);
            this.iETextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.iETextBox_KeyPress);
            // 
            // cPFTextBox
            // 
            this.cPFTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osClienteBindingSource, "CPF", true));
            this.cPFTextBox.Location = new System.Drawing.Point(7, 461);
            this.cPFTextBox.MaxLength = 11;
            this.cPFTextBox.Name = "cPFTextBox";
            this.cPFTextBox.Size = new System.Drawing.Size(220, 20);
            this.cPFTextBox.TabIndex = 0;
            this.cPFTextBox.TabStop = false;
            // 
            // cNPJTextBox
            // 
            this.cNPJTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osClienteBindingSource, "CNPJ", true));
            this.cNPJTextBox.Location = new System.Drawing.Point(240, 461);
            this.cNPJTextBox.MaxLength = 15;
            this.cNPJTextBox.Name = "cNPJTextBox";
            this.cNPJTextBox.Size = new System.Drawing.Size(220, 20);
            this.cNPJTextBox.TabIndex = 0;
            this.cNPJTextBox.TabStop = false;
            // 
            // cONTATOTextBox
            // 
            this.cONTATOTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osClienteBindingSource, "CONTATO", true));
            this.cONTATOTextBox.Location = new System.Drawing.Point(529, 12);
            this.cONTATOTextBox.MaxLength = 50;
            this.cONTATOTextBox.Name = "cONTATOTextBox";
            this.cONTATOTextBox.Size = new System.Drawing.Size(220, 20);
            this.cONTATOTextBox.TabIndex = 0;
            this.cONTATOTextBox.Visible = false;
            // 
            // bindingNavigator
            // 
            this.bindingNavigator.AddNewItem = this.tsbInserir;
            this.bindingNavigator.BindingSource = this.osClienteBindingSource;
            this.bindingNavigator.CountItem = this.toolStripLabel1;
            this.bindingNavigator.CountItemFormat = "de {0}";
            this.bindingNavigator.DeleteItem = null;
            this.bindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bindingNavigator.ImageScalingSize = new System.Drawing.Size(48, 48);
            this.bindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton3,
            this.toolStripSeparator3,
            this.tsbInserir,
            this.tsbGravar,
            this.tsbCancelar,
            this.toolStripSeparator4,
            this.toolStripButton1,
            this.toolStripButton5,
            this.toolStripSeparator1,
            this.toolStripTextBox1,
            this.toolStripLabel1,
            this.toolStripSeparator2,
            this.toolStripButton6,
            this.toolStripButton7,
            this.tsbDescartar});
            this.bindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator.MoveFirstItem = this.toolStripButton1;
            this.bindingNavigator.MoveLastItem = this.toolStripButton7;
            this.bindingNavigator.MoveNextItem = this.toolStripButton6;
            this.bindingNavigator.MovePreviousItem = this.toolStripButton5;
            this.bindingNavigator.Name = "bindingNavigator";
            this.bindingNavigator.PositionItem = this.toolStripTextBox1;
            this.bindingNavigator.Size = new System.Drawing.Size(1184, 70);
            this.bindingNavigator.TabIndex = 9;
            this.bindingNavigator.Text = "bindingNavigator1";
            // 
            // tsbInserir
            // 
            this.tsbInserir.Image = global::osExpress.Properties.Resources.insert;
            this.tsbInserir.Name = "tsbInserir";
            this.tsbInserir.RightToLeftAutoMirrorImage = true;
            this.tsbInserir.Size = new System.Drawing.Size(52, 67);
            this.tsbInserir.Text = "&Inserir";
            this.tsbInserir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbInserir.Click += new System.EventHandler(this.tsbInserir_Click);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(37, 67);
            this.toolStripLabel1.Text = "de {0}";
            this.toolStripLabel1.ToolTipText = "Total number of items";
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.Image = global::osExpress.Properties.Resources.search;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(62, 67);
            this.toolStripButton3.Text = "Consultar";
            this.toolStripButton3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 70);
            // 
            // tsbGravar
            // 
            this.tsbGravar.Image = global::osExpress.Properties.Resources.save;
            this.tsbGravar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbGravar.Name = "tsbGravar";
            this.tsbGravar.Size = new System.Drawing.Size(52, 67);
            this.tsbGravar.Text = "Grav&ar";
            this.tsbGravar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbGravar.Click += new System.EventHandler(this.tsbGravar_Click);
            // 
            // tsbCancelar
            // 
            this.tsbCancelar.Image = global::osExpress.Properties.Resources.clean;
            this.tsbCancelar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbCancelar.Name = "tsbCancelar";
            this.tsbCancelar.Size = new System.Drawing.Size(57, 67);
            this.tsbCancelar.Text = "Cancelar";
            this.tsbCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbCancelar.Click += new System.EventHandler(this.tsbCancelar_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 70);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = global::osExpress.Properties.Resources.first;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.RightToLeftAutoMirrorImage = true;
            this.toolStripButton1.Size = new System.Drawing.Size(56, 67);
            this.toolStripButton1.Text = "&Primeiro";
            this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.Image = global::osExpress.Properties.Resources.previous;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.RightToLeftAutoMirrorImage = true;
            this.toolStripButton5.Size = new System.Drawing.Size(54, 67);
            this.toolStripButton5.Text = "Anteri&or";
            this.toolStripButton5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton5.Click += new System.EventHandler(this.toolStripButton5_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 70);
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.AccessibleName = "Position";
            this.toolStripTextBox1.AutoSize = false;
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(50, 23);
            this.toolStripTextBox1.Text = "0";
            this.toolStripTextBox1.ToolTipText = "Current position";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 70);
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.Image = global::osExpress.Properties.Resources.next;
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.RightToLeftAutoMirrorImage = true;
            this.toolStripButton6.Size = new System.Drawing.Size(56, 67);
            this.toolStripButton6.Text = "Próxi&mo";
            this.toolStripButton6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton6.Click += new System.EventHandler(this.toolStripButton6_Click);
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.Image = global::osExpress.Properties.Resources.last;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.RightToLeftAutoMirrorImage = true;
            this.toolStripButton7.Size = new System.Drawing.Size(52, 67);
            this.toolStripButton7.Text = "&Último";
            this.toolStripButton7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton7.Click += new System.EventHandler(this.toolStripButton7_Click);
            // 
            // tsbDescartar
            // 
            this.tsbDescartar.Enabled = false;
            this.tsbDescartar.Image = global::osExpress.Properties.Resources.remove;
            this.tsbDescartar.Name = "tsbDescartar";
            this.tsbDescartar.RightToLeftAutoMirrorImage = true;
            this.tsbDescartar.Size = new System.Drawing.Size(60, 67);
            this.tsbDescartar.Text = "&Descartar";
            this.tsbDescartar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbDescartar.Click += new System.EventHandler(this.tsbDescartar_Click);
            // 
            // pnlConsultar
            // 
            this.pnlConsultar.BackColor = System.Drawing.SystemColors.Control;
            this.pnlConsultar.Controls.Add(this.tbLike);
            this.pnlConsultar.Controls.Add(this.labelLike);
            this.pnlConsultar.Controls.Add(this.flpManEventos);
            this.pnlConsultar.Controls.Add(this.label8);
            this.pnlConsultar.Controls.Add(this.pictureBox1);
            this.pnlConsultar.Controls.Add(this.label70);
            this.pnlConsultar.Controls.Add(this.lblTotalOS);
            this.pnlConsultar.Controls.Add(this.groupBox2);
            this.pnlConsultar.Controls.Add(this.osOrdemServicoDataGridView);
            this.pnlConsultar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnlConsultar.Location = new System.Drawing.Point(945, 87);
            this.pnlConsultar.Name = "pnlConsultar";
            this.pnlConsultar.Size = new System.Drawing.Size(717, 350);
            this.pnlConsultar.TabIndex = 11;
            // 
            // tbLike
            // 
            this.tbLike.Location = new System.Drawing.Point(6, 10);
            this.tbLike.Name = "tbLike";
            this.tbLike.Size = new System.Drawing.Size(100, 20);
            this.tbLike.TabIndex = 22;
            this.tbLike.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbLike_KeyDown);
            // 
            // labelLike
            // 
            this.labelLike.AutoSize = true;
            this.labelLike.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelLike.Location = new System.Drawing.Point(284, 169);
            this.labelLike.Name = "labelLike";
            this.labelLike.Size = new System.Drawing.Size(299, 13);
            this.labelLike.TabIndex = 19;
            this.labelLike.Text = "Filtro LIKE: %Razão Social%  - Acione <ENTER> para acionar";
            // 
            // flpManEventos
            // 
            this.flpManEventos.AutoScroll = true;
            this.flpManEventos.Location = new System.Drawing.Point(164, 13);
            this.flpManEventos.Margin = new System.Windows.Forms.Padding(2);
            this.flpManEventos.Name = "flpManEventos";
            this.flpManEventos.Size = new System.Drawing.Size(150, 32);
            this.flpManEventos.TabIndex = 18;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(3, 181);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(308, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Clique em \'S\' ou dois cliques do mouse para selecionar o Cliente";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::osExpress.Properties.Resources.search;
            this.pictureBox1.Location = new System.Drawing.Point(336, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(51, 50);
            this.pictureBox1.TabIndex = 16;
            this.pictureBox1.TabStop = false;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.Location = new System.Drawing.Point(385, 15);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(122, 31);
            this.label70.TabIndex = 15;
            this.label70.Text = "Consulta";
            // 
            // lblTotalOS
            // 
            this.lblTotalOS.AutoSize = true;
            this.lblTotalOS.Location = new System.Drawing.Point(3, 487);
            this.lblTotalOS.Name = "lblTotalOS";
            this.lblTotalOS.Size = new System.Drawing.Size(37, 13);
            this.lblTotalOS.TabIndex = 13;
            this.lblTotalOS.Text = "Total: ";
            this.lblTotalOS.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.labelSortear);
            this.groupBox2.Controls.Add(this.labelFantasia);
            this.groupBox2.Controls.Add(this.labelRazao);
            this.groupBox2.Controls.Add(this.chkFiltrarClienteFantasia);
            this.groupBox2.Controls.Add(this.cmbFiltroClienteFantasia);
            this.groupBox2.Controls.Add(this.labelFiltroCliente);
            this.groupBox2.Controls.Add(this.textboxFiltroCliente);
            this.groupBox2.Controls.Add(this.btnEventosPendentes);
            this.groupBox2.Controls.Add(this.btnEventosConcluidos);
            this.groupBox2.Controls.Add(this.btnEventosTodos);
            this.groupBox2.Controls.Add(this.cmbFiltroClienteCodigo);
            this.groupBox2.Controls.Add(this.chkFiltrarCliente);
            this.groupBox2.Controls.Add(this.chkFiltrarSituacao);
            this.groupBox2.Controls.Add(this.label65);
            this.groupBox2.Controls.Add(this.label64);
            this.groupBox2.Controls.Add(this.cmbFiltroSituacao);
            this.groupBox2.Controls.Add(this.cmbFiltroClienteNome);
            this.groupBox2.Location = new System.Drawing.Point(6, 51);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(696, 126);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Filtro";
            // 
            // labelSortear
            // 
            this.labelSortear.AutoSize = true;
            this.labelSortear.ForeColor = System.Drawing.Color.Red;
            this.labelSortear.Location = new System.Drawing.Point(278, 98);
            this.labelSortear.Name = "labelSortear";
            this.labelSortear.Size = new System.Drawing.Size(95, 13);
            this.labelSortear.TabIndex = 21;
            this.labelSortear.Text = "Clique para sortear";
            // 
            // labelFantasia
            // 
            this.labelFantasia.AutoSize = true;
            this.labelFantasia.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labelFantasia.ForeColor = System.Drawing.Color.Red;
            this.labelFantasia.Location = new System.Drawing.Point(226, 77);
            this.labelFantasia.Name = "labelFantasia";
            this.labelFantasia.Size = new System.Drawing.Size(50, 13);
            this.labelFantasia.TabIndex = 20;
            this.labelFantasia.Text = "Fantasia:";
            this.labelFantasia.Click += new System.EventHandler(this.labelFantasia_Click);
            // 
            // labelRazao
            // 
            this.labelRazao.AutoSize = true;
            this.labelRazao.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labelRazao.ForeColor = System.Drawing.Color.Red;
            this.labelRazao.Location = new System.Drawing.Point(235, 47);
            this.labelRazao.Name = "labelRazao";
            this.labelRazao.Size = new System.Drawing.Size(41, 13);
            this.labelRazao.TabIndex = 19;
            this.labelRazao.Text = "Razão:";
            this.labelRazao.Click += new System.EventHandler(this.labelRazao_Click);
            // 
            // chkFiltrarClienteFantasia
            // 
            this.chkFiltrarClienteFantasia.AutoSize = true;
            this.chkFiltrarClienteFantasia.Location = new System.Drawing.Point(639, 75);
            this.chkFiltrarClienteFantasia.Name = "chkFiltrarClienteFantasia";
            this.chkFiltrarClienteFantasia.Size = new System.Drawing.Size(51, 17);
            this.chkFiltrarClienteFantasia.TabIndex = 18;
            this.chkFiltrarClienteFantasia.Text = "Filtrar";
            this.chkFiltrarClienteFantasia.UseVisualStyleBackColor = true;
            this.chkFiltrarClienteFantasia.CheckedChanged += new System.EventHandler(this.chkFiltros_CheckedChanged);
            // 
            // cmbFiltroClienteFantasia
            // 
            this.cmbFiltroClienteFantasia.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbFiltroClienteFantasia.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbFiltroClienteFantasia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFiltroClienteFantasia.FormattingEnabled = true;
            this.cmbFiltroClienteFantasia.Location = new System.Drawing.Point(279, 72);
            this.cmbFiltroClienteFantasia.Name = "cmbFiltroClienteFantasia";
            this.cmbFiltroClienteFantasia.Size = new System.Drawing.Size(354, 21);
            this.cmbFiltroClienteFantasia.TabIndex = 17;
            // 
            // labelFiltroCliente
            // 
            this.labelFiltroCliente.AutoSize = true;
            this.labelFiltroCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFiltroCliente.Location = new System.Drawing.Point(502, 16);
            this.labelFiltroCliente.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelFiltroCliente.Name = "labelFiltroCliente";
            this.labelFiltroCliente.Size = new System.Drawing.Size(63, 9);
            this.labelFiltroCliente.TabIndex = 16;
            this.labelFiltroCliente.Text = "Filtro %Cliente%";
            // 
            // textboxFiltroCliente
            // 
            this.textboxFiltroCliente.Location = new System.Drawing.Point(502, 32);
            this.textboxFiltroCliente.Margin = new System.Windows.Forms.Padding(2);
            this.textboxFiltroCliente.Name = "textboxFiltroCliente";
            this.textboxFiltroCliente.Size = new System.Drawing.Size(151, 20);
            this.textboxFiltroCliente.TabIndex = 15;
            // 
            // btnEventosPendentes
            // 
            this.btnEventosPendentes.Location = new System.Drawing.Point(410, 18);
            this.btnEventosPendentes.Margin = new System.Windows.Forms.Padding(2);
            this.btnEventosPendentes.Name = "btnEventosPendentes";
            this.btnEventosPendentes.Size = new System.Drawing.Size(56, 19);
            this.btnEventosPendentes.TabIndex = 14;
            this.btnEventosPendentes.Text = "Eventos pendentes";
            this.btnEventosPendentes.UseVisualStyleBackColor = true;
            this.btnEventosPendentes.Click += new System.EventHandler(this.btnEventosPendentes_Click);
            // 
            // btnEventosConcluidos
            // 
            this.btnEventosConcluidos.Location = new System.Drawing.Point(345, 17);
            this.btnEventosConcluidos.Margin = new System.Windows.Forms.Padding(2);
            this.btnEventosConcluidos.Name = "btnEventosConcluidos";
            this.btnEventosConcluidos.Size = new System.Drawing.Size(56, 19);
            this.btnEventosConcluidos.TabIndex = 13;
            this.btnEventosConcluidos.Text = "Eventos concluidos";
            this.btnEventosConcluidos.UseVisualStyleBackColor = true;
            this.btnEventosConcluidos.Click += new System.EventHandler(this.btnEventosConcluidos_Click);
            // 
            // btnEventosTodos
            // 
            this.btnEventosTodos.Location = new System.Drawing.Point(285, 17);
            this.btnEventosTodos.Margin = new System.Windows.Forms.Padding(2);
            this.btnEventosTodos.Name = "btnEventosTodos";
            this.btnEventosTodos.Size = new System.Drawing.Size(56, 19);
            this.btnEventosTodos.TabIndex = 12;
            this.btnEventosTodos.Text = "Todos Eventos";
            this.btnEventosTodos.UseVisualStyleBackColor = true;
            this.btnEventosTodos.Click += new System.EventHandler(this.btnEventosTodos_Click);
            // 
            // cmbFiltroClienteCodigo
            // 
            this.cmbFiltroClienteCodigo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbFiltroClienteCodigo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbFiltroClienteCodigo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFiltroClienteCodigo.FormattingEnabled = true;
            this.cmbFiltroClienteCodigo.Location = new System.Drawing.Point(117, 43);
            this.cmbFiltroClienteCodigo.Name = "cmbFiltroClienteCodigo";
            this.cmbFiltroClienteCodigo.Size = new System.Drawing.Size(106, 21);
            this.cmbFiltroClienteCodigo.TabIndex = 11;
            // 
            // chkFiltrarCliente
            // 
            this.chkFiltrarCliente.AutoSize = true;
            this.chkFiltrarCliente.Location = new System.Drawing.Point(639, 45);
            this.chkFiltrarCliente.Name = "chkFiltrarCliente";
            this.chkFiltrarCliente.Size = new System.Drawing.Size(51, 17);
            this.chkFiltrarCliente.TabIndex = 9;
            this.chkFiltrarCliente.Text = "Filtrar";
            this.chkFiltrarCliente.UseVisualStyleBackColor = true;
            this.chkFiltrarCliente.CheckedChanged += new System.EventHandler(this.chkFiltros_CheckedChanged);
            // 
            // chkFiltrarSituacao
            // 
            this.chkFiltrarSituacao.AutoSize = true;
            this.chkFiltrarSituacao.Location = new System.Drawing.Point(229, 19);
            this.chkFiltrarSituacao.Name = "chkFiltrarSituacao";
            this.chkFiltrarSituacao.Size = new System.Drawing.Size(51, 17);
            this.chkFiltrarSituacao.TabIndex = 7;
            this.chkFiltrarSituacao.Text = "Filtrar";
            this.chkFiltrarSituacao.UseVisualStyleBackColor = true;
            this.chkFiltrarSituacao.CheckedChanged += new System.EventHandler(this.chkFiltros_CheckedChanged);
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(9, 23);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(102, 13);
            this.label65.TabIndex = 6;
            this.label65.Text = "Situação do Cliente:";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(69, 49);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(42, 13);
            this.label64.TabIndex = 5;
            this.label64.Text = "Cliente:";
            // 
            // cmbFiltroSituacao
            // 
            this.cmbFiltroSituacao.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbFiltroSituacao.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbFiltroSituacao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFiltroSituacao.FormattingEnabled = true;
            this.cmbFiltroSituacao.Items.AddRange(new object[] {
            "A - Ativo",
            "D - Desativado"});
            this.cmbFiltroSituacao.Location = new System.Drawing.Point(117, 17);
            this.cmbFiltroSituacao.Name = "cmbFiltroSituacao";
            this.cmbFiltroSituacao.Size = new System.Drawing.Size(106, 21);
            this.cmbFiltroSituacao.TabIndex = 3;
            // 
            // cmbFiltroClienteNome
            // 
            this.cmbFiltroClienteNome.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbFiltroClienteNome.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbFiltroClienteNome.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFiltroClienteNome.FormattingEnabled = true;
            this.cmbFiltroClienteNome.Location = new System.Drawing.Point(279, 42);
            this.cmbFiltroClienteNome.Name = "cmbFiltroClienteNome";
            this.cmbFiltroClienteNome.Size = new System.Drawing.Size(354, 21);
            this.cmbFiltroClienteNome.TabIndex = 1;
            this.cmbFiltroClienteNome.SelectedIndexChanged += new System.EventHandler(this.cmbFiltroClienteNome_SelectedIndexChanged);
            // 
            // osOrdemServicoDataGridView
            // 
            this.osOrdemServicoDataGridView.AllowUserToAddRows = false;
            this.osOrdemServicoDataGridView.AllowUserToDeleteRows = false;
            this.osOrdemServicoDataGridView.AllowUserToOrderColumns = true;
            this.osOrdemServicoDataGridView.AutoGenerateColumns = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.osOrdemServicoDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.osOrdemServicoDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.osOrdemServicoDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.rAZAOSOCIALDataGridViewTextBoxColumn,
            this.nOMEFANTASIADataGridViewTextBoxColumn,
            this.CNPJ});
            this.osOrdemServicoDataGridView.DataSource = this.osClienteBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.osOrdemServicoDataGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.osOrdemServicoDataGridView.Location = new System.Drawing.Point(6, 200);
            this.osOrdemServicoDataGridView.Name = "osOrdemServicoDataGridView";
            this.osOrdemServicoDataGridView.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.osOrdemServicoDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.osOrdemServicoDataGridView.Size = new System.Drawing.Size(700, 199);
            this.osOrdemServicoDataGridView.TabIndex = 11;
            this.osOrdemServicoDataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.osOrdemServicoDataGridView_CellDoubleClick);
            this.osOrdemServicoDataGridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.osOrdemServicoDataGridView_KeyDown);
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "Código";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // rAZAOSOCIALDataGridViewTextBoxColumn
            // 
            this.rAZAOSOCIALDataGridViewTextBoxColumn.DataPropertyName = "RAZAO_SOCIAL";
            this.rAZAOSOCIALDataGridViewTextBoxColumn.HeaderText = "Razão Social";
            this.rAZAOSOCIALDataGridViewTextBoxColumn.Name = "rAZAOSOCIALDataGridViewTextBoxColumn";
            this.rAZAOSOCIALDataGridViewTextBoxColumn.ReadOnly = true;
            this.rAZAOSOCIALDataGridViewTextBoxColumn.Width = 300;
            // 
            // nOMEFANTASIADataGridViewTextBoxColumn
            // 
            this.nOMEFANTASIADataGridViewTextBoxColumn.DataPropertyName = "NOME_FANTASIA";
            this.nOMEFANTASIADataGridViewTextBoxColumn.HeaderText = "Nome Fantasia";
            this.nOMEFANTASIADataGridViewTextBoxColumn.Name = "nOMEFANTASIADataGridViewTextBoxColumn";
            this.nOMEFANTASIADataGridViewTextBoxColumn.ReadOnly = true;
            this.nOMEFANTASIADataGridViewTextBoxColumn.Width = 140;
            // 
            // CNPJ
            // 
            this.CNPJ.DataPropertyName = "CNPJ";
            this.CNPJ.HeaderText = "CNPJ";
            this.CNPJ.Name = "CNPJ";
            this.CNPJ.ReadOnly = true;
            // 
            // labelCadastrarCliente
            // 
            this.labelCadastrarCliente.AutoSize = true;
            this.labelCadastrarCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCadastrarCliente.Location = new System.Drawing.Point(248, 63);
            this.labelCadastrarCliente.Name = "labelCadastrarCliente";
            this.labelCadastrarCliente.Size = new System.Drawing.Size(248, 31);
            this.labelCadastrarCliente.TabIndex = 12;
            this.labelCadastrarCliente.Text = "Cadastro Clientes";
            // 
            // Contatos
            // 
            this.Contatos.Controls.Add(labelContatosCliente);
            this.Contatos.Controls.Add(labelContatosHeader);
            this.Contatos.Controls.Add(this.cONTATOTextBox);
            this.Contatos.Controls.Add(cONTATOLabel);
            this.Contatos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Contatos.Location = new System.Drawing.Point(4, 22);
            this.Contatos.Name = "Contatos";
            this.Contatos.Size = new System.Drawing.Size(1192, 524);
            this.Contatos.TabIndex = 1;
            this.Contatos.Text = "Contatos";
            this.Contatos.UseVisualStyleBackColor = true;
            // 
            // Outros
            // 
            this.Outros.Location = new System.Drawing.Point(4, 22);
            this.Outros.Name = "Outros";
            this.Outros.Size = new System.Drawing.Size(1192, 524);
            this.Outros.TabIndex = 2;
            this.Outros.Text = "Outros";
            this.Outros.UseVisualStyleBackColor = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.A30CAIXA_PROG_EXTableAdapter = null;
            this.tableAdapterManager.A30CAIXA_PROGTableAdapter = null;
            this.tableAdapterManager.A30CLIENTESTableAdapter = null;
            this.tableAdapterManager.A30FUNCIONARIOTableAdapter = null;
            this.tableAdapterManager.A30PLANO_CONTATableAdapter = null;
            this.tableAdapterManager.A30POSTOSTableAdapter = null;
            this.tableAdapterManager.A30PRODUTOTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.OsClienteTableAdapter = this.osClienteTableAdapter;
            this.tableAdapterManager.OsEtiquetaOsClienteTableAdapter = null;
            this.tableAdapterManager.OsEtiquetaTableAdapter = null;
            this.tableAdapterManager.OsFlashDriveTableAdapter = null;
            this.tableAdapterManager.OsFormasPagamentoTableAdapter = null;
            this.tableAdapterManager.OsIbametroInmetroOsClienteTableAdapter = null;
            this.tableAdapterManager.OsIbametroInmetroTableAdapter = null;
            this.tableAdapterManager.OsIbametroOsCliente1TableAdapter = null;
            this.tableAdapterManager.OsIbametroOsClienteTableAdapter = null;
            this.tableAdapterManager.OsIbametroTableAdapter = null;
            this.tableAdapterManager.OsInstrumentoServicoOsClienteTableAdapter = null;
            this.tableAdapterManager.OsInstrumentoServicoTableAdapter = null;
            this.tableAdapterManager.OsItemServicoTableAdapter = null;
            this.tableAdapterManager.OsOrdemParcelaTableAdapter = null;
            this.tableAdapterManager.OsOrdemServicoTableAdapter = null;
            this.tableAdapterManager.OsParametrosTableAdapter = null;
            this.tableAdapterManager.OsParContasTableAdapter = null;
            this.tableAdapterManager.OsParSituacaoTableAdapter = null;
            this.tableAdapterManager.OsProdutosServicosTableAdapter = null;
            this.tableAdapterManager.OsReciboTableAdapter = null;
            this.tableAdapterManager.OsSituacoesOSTableAdapter = null;
            this.tableAdapterManager.OsUsuarioTableAdapter = null;
            this.tableAdapterManager.OsVersaoTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = osExpress.OsExpressDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.XOsFuncionarioTableAdapter = null;
            // 
            // osClienteTableAdapter
            // 
            this.osClienteTableAdapter.ClearBeforeFill = true;
            // 
            // ManCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1184, 711);
            this.Controls.Add(this.labelCadastrarCliente);
            this.Controls.Add(this.pnlConsultar);
            this.Controls.Add(this.bindingNavigator);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = true;
            this.MinimizeBox = true;
            this.Name = "ManCliente";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Manutenção de Clientes";
            this.Load += new System.EventHandler(this.ManCliente_Load);
            this.Controls.SetChildIndex(this.tabPrincipal, 0);
            this.Controls.SetChildIndex(this.bindingNavigator, 0);
            this.Controls.SetChildIndex(this.grpPrincipal, 0);
            this.Controls.SetChildIndex(this.pnlConsultar, 0);
            this.Controls.SetChildIndex(this.labelCadastrarCliente, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dtsPrincipal)).EndInit();
            this.tabPrincipal.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.osClienteBindingSource)).EndInit();
            this.tabChild.ResumeLayout(false);
            this.tabEndereco.ResumeLayout(false);
            this.tabEndereco.PerformLayout();
            this.tabContatos.ResumeLayout(false);
            this.tabContatos.PerformLayout();
            this.tabOutros.ResumeLayout(false);
            this.tabOutros.PerformLayout();
            this.tabObservacoes.ResumeLayout(false);
            this.tabObservacoes.PerformLayout();
            this.tabEventos.ResumeLayout(false);
            this.tabEventos.PerformLayout();
            this.tabOrcamentos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOrcamentos)).EndInit();
            this.tabPedidos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPedidos)).EndInit();
            this.tabServicos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewServicos)).EndInit();
            this.tabInstrumentos.ResumeLayout(false);
            this.pnlEditarBomba.ResumeLayout(false);
            this.pnlEditarBomba.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLacre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBombas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator)).EndInit();
            this.bindingNavigator.ResumeLayout(false);
            this.bindingNavigator.PerformLayout();
            this.pnlConsultar.ResumeLayout(false);
            this.pnlConsultar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.osOrdemServicoDataGridView)).EndInit();
            this.Contatos.ResumeLayout(false);
            this.Contatos.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage1;
        private OsExpressDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator bindingNavigator;
        private System.Windows.Forms.ToolStripButton tsbInserir;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripButton tsbGravar;
        private System.Windows.Forms.ToolStripButton tsbCancelar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButton6;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.Panel pnlConsultar;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label lblTotalOS;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cmbFiltroClienteCodigo;
        private System.Windows.Forms.CheckBox chkFiltrarCliente;
        private System.Windows.Forms.CheckBox chkFiltrarSituacao;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.ComboBox cmbFiltroSituacao;
        private System.Windows.Forms.ComboBox cmbFiltroClienteNome;
        private System.Windows.Forms.DataGridView osOrdemServicoDataGridView;
        private System.Windows.Forms.BindingSource osClienteBindingSource;
        private OsExpressDataSetTableAdapters.OsClienteTableAdapter osClienteTableAdapter;
        private System.Windows.Forms.TextBox iDTextBox;
        private System.Windows.Forms.TextBox cNPJTextBox;
        private System.Windows.Forms.TextBox iETextBox;
        private System.Windows.Forms.TextBox cONTATOTextBox;
        private System.Windows.Forms.TextBox cPFTextBox;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.MaskedTextBox mtbCNPJ;
        private System.Windows.Forms.MaskedTextBox mtbCPF;
        private System.Windows.Forms.Label labelCadastrarCliente;
        private System.Windows.Forms.TabPage Contatos;
        private System.Windows.Forms.TabPage Outros;
        private System.Windows.Forms.TextBox rAZAO_SOCIALTextBox;
        private System.Windows.Forms.TextBox nOME_FANTASIATextBox;
        private System.Windows.Forms.TabControl tabChild;
        private System.Windows.Forms.TabPage tabEndereco;
        private System.Windows.Forms.TabPage tabContatos;
        private System.Windows.Forms.TextBox cIDADETextBox;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox cEPTextBox;
        private System.Windows.Forms.TextBox bAIRROTextBox;
        private System.Windows.Forms.TextBox eNDERECOTextBox;
        private System.Windows.Forms.MaskedTextBox mtbTelefone;
        private System.Windows.Forms.TextBox tELEFONETextBox;
        private System.Windows.Forms.TextBox fAXTextBox;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.MaskedTextBox maskedTextboxContatosCelular;
        private System.Windows.Forms.TextBox textboxContatosEmail;
        private System.Windows.Forms.TextBox textboxContatosNome;
        private System.Windows.Forms.TextBox textboxHiddenId;
        private System.Windows.Forms.Button buttonContatosDeletar;
        private System.Windows.Forms.Button buttonContatosCancelar;
        private System.Windows.Forms.Button buttonContatosGravar;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TextBox textboxContatosObs;
        private System.Windows.Forms.MaskedTextBox maskedTextboxContatosCpf;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.TabPage tabOutros;
        private System.Windows.Forms.TabPage tabObservacoes;
        private System.Windows.Forms.TextBox oBSERVACAOTextBox;
        private System.Windows.Forms.TextBox cODIGO_SIGPOSTOTextBox;
        private System.Windows.Forms.TabPage tabEventos;
        private System.Windows.Forms.TextBox qTDE_BOMBASTextBox;
        private System.Windows.Forms.TextBox tIPO_BOMBATextBox;
        private System.Windows.Forms.TextBox qTDE_BICOSTextBox;
        private System.Windows.Forms.TextBox bANDEIRATextBox;
        private System.Windows.Forms.TextBox mODELO_CPUTextBox;
        private System.Windows.Forms.CheckBox checkboxRFID;
        private System.Windows.Forms.Label labelConsentrador;
        private System.Windows.Forms.Label labelQuantSumpers;
        private System.Windows.Forms.Label labelQuantTanques;
        private System.Windows.Forms.CheckBox checkboxAlertar;
        private System.Windows.Forms.TextBox textboxApontamento;
        private System.Windows.Forms.Label labelApontamento;
        private System.Windows.Forms.Label labelDataEHora;
        private System.Windows.Forms.Label labelEM;
        private System.Windows.Forms.ListView listviewEventos;
        private System.Windows.Forms.CheckBox checkboxPrincipal;
        private System.Windows.Forms.CheckBox checkboxPostoDeCombustivel;
        private System.Windows.Forms.ComboBox comboboxTipoNegocio;
        private System.Windows.Forms.TextBox textboxQuantSumpers;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textboxQuantTanques;
        private System.Windows.Forms.TextBox textboxCodigoSimp;
        private System.Windows.Forms.DateTimePicker datetimeRealizacao;
        private System.Windows.Forms.Button buttonEventosGravar;
        private System.Windows.Forms.Button buttonEventosCancelar;
        private System.Windows.Forms.Button buttonEventosDeletar;
        private System.Windows.Forms.TextBox textboxHiddenIdEvento;
        private System.Windows.Forms.CheckBox checkboxAtivo;
        private System.Windows.Forms.ToolStripButton tsbDescartar;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rAZAOSOCIALDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nOMEFANTASIADataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn CNPJ;
        private System.Windows.Forms.Button btnEventosPendentes;
        private System.Windows.Forms.Button btnEventosConcluidos;
        private System.Windows.Forms.Button btnEventosTodos;
        private System.Windows.Forms.FlowLayoutPanel flpManEventos;
        private System.Windows.Forms.Label labelFiltroCliente;
        private System.Windows.Forms.TextBox textboxFiltroCliente;
        private System.Windows.Forms.Button btnListEventos;
        private System.Windows.Forms.CheckBox chkFiltrarClienteFantasia;
        private System.Windows.Forms.ComboBox cmbFiltroClienteFantasia;
        private System.Windows.Forms.Label labelFantasia;
        private System.Windows.Forms.Label labelRazao;
        private System.Windows.Forms.Label labelSortear;
        private System.Windows.Forms.TabPage tabOrcamentos;
        private System.Windows.Forms.TabPage tabPedidos;
        private System.Windows.Forms.TabPage tabServicos;
        private System.Windows.Forms.DataGridView dataGridViewOrcamentos;
        private System.Windows.Forms.DataGridView dataGridViewPedidos;
        private System.Windows.Forms.DataGridView dataGridViewServicos;
        private System.Windows.Forms.Label labelLike;
        private System.Windows.Forms.TextBox tbLike;
        private System.Windows.Forms.TabPage tabInstrumentos;
        private System.Windows.Forms.DataGridView dgvBombas;
        private System.Windows.Forms.Panel pnlEditarBomba;
        private System.Windows.Forms.ComboBox cmbMarcaInstrumento;
        private System.Windows.Forms.TextBox tbQBicos;
        private System.Windows.Forms.TextBox tbSerie;
        private System.Windows.Forms.TextBox tbDescricao;
        private System.Windows.Forms.TextBox tbIlha;
        private System.Windows.Forms.DataGridView dgvLacre;
        private System.Windows.Forms.TextBox tbLacre;
        private System.Windows.Forms.TextBox tbFabricante;
        private System.Windows.Forms.Button btnGravarBomba;
        private System.Windows.Forms.Button btnDeletarBomba;
        private System.Windows.Forms.Button btnLimparBomba;
        private System.Windows.Forms.Button btnGerarLacre;
        private System.Windows.Forms.RichTextBox rtbHelp1;
        private System.Windows.Forms.RichTextBox rtbHelp0;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.RichTextBox rtbHelp2;
    }
}
