﻿namespace osExpress.Manutencoes
{
    partial class ManEtiqueta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label label11;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label tIPOLabel;
            System.Windows.Forms.Label nUM_ETIQUETALabel;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbSituacaoEtiquetas = new System.Windows.Forms.ComboBox();
            this.btnInserirMecanico = new System.Windows.Forms.Button();
            this.cmbTipoEtiqueta = new System.Windows.Forms.ComboBox();
            this.nUM_ETIQUETATextBox = new System.Windows.Forms.TextBox();
            this.osEtiquetaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cmbCodigoFuncionario = new System.Windows.Forms.ComboBox();
            this.osUsuarioBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cmbNomeFuncionario = new System.Windows.Forms.ComboBox();
            this.bindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.tsbInserir = new System.Windows.Forms.ToolStripButton();
            this.tsbGravar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbPrimeiro = new System.Windows.Forms.ToolStripButton();
            this.tsbAnterior = new System.Windows.Forms.ToolStripButton();
            this.tsbProximo = new System.Windows.Forms.ToolStripButton();
            this.tsbUltimo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbImprimir = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnSair = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.osEtiquetaDataGridView = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bdsUsuario = new System.Windows.Forms.BindingSource(this.components);
            this.tableAdapterManager = new osExpress.OsExpressDataSetTableAdapters.TableAdapterManager();
            this.osEtiquetaTableAdapter = new osExpress.OsExpressDataSetTableAdapters.OsEtiquetaTableAdapter();
            this.osUsuarioTableAdapter = new osExpress.OsExpressDataSetTableAdapters.OsUsuarioTableAdapter();
            this.pnlAtribuir = new System.Windows.Forms.Panel();
            this.btnAtribuicao = new System.Windows.Forms.Button();
            this.pnlCancelarAtribuicao = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbFuncionarioAtribuir = new System.Windows.Forms.ComboBox();
            this.OsXOSFuncionarioBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.xOsFuncionarioTableAdapter = new osExpress.OsExpressDataSetTableAdapters.XOsFuncionarioTableAdapter();
            this.bdsXOSFuncionario = new System.Windows.Forms.BindingSource(this.components);
            label11 = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            tIPOLabel = new System.Windows.Forms.Label();
            nUM_ETIQUETALabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dtsPrincipal)).BeginInit();
            this.tabPrincipal.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.osEtiquetaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.osUsuarioBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator)).BeginInit();
            this.bindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.osEtiquetaDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsUsuario)).BeginInit();
            this.pnlAtribuir.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OsXOSFuncionarioBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsXOSFuncionario)).BeginInit();
            this.SuspendLayout();
            // 
            // grpPrincipal
            // 
            this.grpPrincipal.Location = new System.Drawing.Point(12, 75);
            this.grpPrincipal.Size = new System.Drawing.Size(758, 10);
            this.grpPrincipal.Visible = false;
            // 
            // tabPrincipal
            // 
            this.tabPrincipal.Controls.Add(this.tabPage1);
            this.tabPrincipal.Location = new System.Drawing.Point(12, 91);
            this.tabPrincipal.Size = new System.Drawing.Size(758, 117);
            this.tabPrincipal.TabStop = false;
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label11.Location = new System.Drawing.Point(232, 9);
            label11.Name = "label11";
            label11.Size = new System.Drawing.Size(131, 13);
            label11.TabIndex = 45;
            label11.Text = "Nome do Funcionário:";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label1.Location = new System.Drawing.Point(11, 9);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(138, 13);
            label1.TabIndex = 43;
            label1.Text = "Código do Funcionário:";
            // 
            // tIPOLabel
            // 
            tIPOLabel.AutoSize = true;
            tIPOLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tIPOLabel.Location = new System.Drawing.Point(113, 36);
            tIPOLabel.Name = "tIPOLabel";
            tIPOLabel.Size = new System.Drawing.Size(36, 13);
            tIPOLabel.TabIndex = 49;
            tIPOLabel.Text = "Tipo:";
            // 
            // nUM_ETIQUETALabel
            // 
            nUM_ETIQUETALabel.AutoSize = true;
            nUM_ETIQUETALabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            nUM_ETIQUETALabel.Location = new System.Drawing.Point(26, 63);
            nUM_ETIQUETALabel.Name = "nUM_ETIQUETALabel";
            nUM_ETIQUETALabel.Size = new System.Drawing.Size(123, 13);
            nUM_ETIQUETALabel.TabIndex = 51;
            nUM_ETIQUETALabel.Text = "Número da Etiqueta:";
            // 
            // tabPage1
            // 
            this.tabPage1.AutoScroll = true;
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.cmbSituacaoEtiquetas);
            this.tabPage1.Controls.Add(this.btnInserirMecanico);
            this.tabPage1.Controls.Add(this.cmbTipoEtiqueta);
            this.tabPage1.Controls.Add(tIPOLabel);
            this.tabPage1.Controls.Add(nUM_ETIQUETALabel);
            this.tabPage1.Controls.Add(this.nUM_ETIQUETATextBox);
            this.tabPage1.Controls.Add(this.cmbCodigoFuncionario);
            this.tabPage1.Controls.Add(label11);
            this.tabPage1.Controls.Add(this.cmbNomeFuncionario);
            this.tabPage1.Controls.Add(label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(750, 91);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Cadastro";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(540, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 53;
            this.label2.Text = "Situação:";
            // 
            // cmbSituacaoEtiquetas
            // 
            this.cmbSituacaoEtiquetas.FormattingEnabled = true;
            this.cmbSituacaoEtiquetas.Items.AddRange(new object[] {
            "T - Todos",
            "U -Utilizados",
            "N - Não Utilizados"});
            this.cmbSituacaoEtiquetas.Location = new System.Drawing.Point(607, 60);
            this.cmbSituacaoEtiquetas.Name = "cmbSituacaoEtiquetas";
            this.cmbSituacaoEtiquetas.Size = new System.Drawing.Size(121, 21);
            this.cmbSituacaoEtiquetas.TabIndex = 52;
            this.cmbSituacaoEtiquetas.SelectedValueChanged += new System.EventHandler(this.comboBox6_SelectedValueChanged);
            // 
            // btnInserirMecanico
            // 
            this.btnInserirMecanico.Location = new System.Drawing.Point(369, 58);
            this.btnInserirMecanico.Name = "btnInserirMecanico";
            this.btnInserirMecanico.Size = new System.Drawing.Size(0, 23);
            this.btnInserirMecanico.TabIndex = 4;
            this.btnInserirMecanico.Text = "button1";
            this.btnInserirMecanico.UseVisualStyleBackColor = true;
            this.btnInserirMecanico.Enter += new System.EventHandler(this.btnInserirMecanico_Enter);
            // 
            // cmbTipoEtiqueta
            // 
            this.cmbTipoEtiqueta.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbTipoEtiqueta.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbTipoEtiqueta.FormattingEnabled = true;
            this.cmbTipoEtiqueta.Items.AddRange(new object[] {
            "L - Lacre",
            "S - Selo"});
            this.cmbTipoEtiqueta.Location = new System.Drawing.Point(155, 33);
            this.cmbTipoEtiqueta.Name = "cmbTipoEtiqueta";
            this.cmbTipoEtiqueta.Size = new System.Drawing.Size(101, 21);
            this.cmbTipoEtiqueta.TabIndex = 2;
            this.cmbTipoEtiqueta.SelectedIndexChanged += new System.EventHandler(this.cmbTipoEtiqueta_SelectedIndexChanged);
            // 
            // nUM_ETIQUETATextBox
            // 
            this.nUM_ETIQUETATextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osEtiquetaBindingSource, "NUM_ETIQUETA", true));
            this.nUM_ETIQUETATextBox.Location = new System.Drawing.Point(155, 60);
            this.nUM_ETIQUETATextBox.Name = "nUM_ETIQUETATextBox";
            this.nUM_ETIQUETATextBox.Size = new System.Drawing.Size(200, 20);
            this.nUM_ETIQUETATextBox.TabIndex = 3;
            // 
            // osEtiquetaBindingSource
            // 
            this.osEtiquetaBindingSource.DataMember = "OsEtiqueta";
            this.osEtiquetaBindingSource.DataSource = this.dtsPrincipal;
            // 
            // cmbCodigoFuncionario
            // 
            this.cmbCodigoFuncionario.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCodigoFuncionario.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCodigoFuncionario.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osEtiquetaBindingSource, "IDUSUARIO", true));
            this.cmbCodigoFuncionario.DataSource = this.OsXOSFuncionarioBindingSource;
            this.cmbCodigoFuncionario.DisplayMember = "idusuario";
            this.cmbCodigoFuncionario.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCodigoFuncionario.FormattingEnabled = true;
            this.cmbCodigoFuncionario.Location = new System.Drawing.Point(155, 6);
            this.cmbCodigoFuncionario.Name = "cmbCodigoFuncionario";
            this.cmbCodigoFuncionario.Size = new System.Drawing.Size(58, 21);
            this.cmbCodigoFuncionario.TabIndex = 0;
            this.cmbCodigoFuncionario.ValueMember = "idusuario";
            this.cmbCodigoFuncionario.SelectedValueChanged += new System.EventHandler(this.comboBox6_SelectedValueChanged);
            // 
            // osUsuarioBindingSource
            // 
            this.osUsuarioBindingSource.DataMember = "OsUsuario";
            this.osUsuarioBindingSource.DataSource = this.dtsPrincipal;
            this.osUsuarioBindingSource.Sort = "NOME ASC";
            // 
            // cmbNomeFuncionario
            // 
            this.cmbNomeFuncionario.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbNomeFuncionario.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbNomeFuncionario.DataSource = this.OsXOSFuncionarioBindingSource;
            this.cmbNomeFuncionario.DisplayMember = "uNome";
            this.cmbNomeFuncionario.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbNomeFuncionario.FormattingEnabled = true;
            this.cmbNomeFuncionario.Location = new System.Drawing.Point(369, 6);
            this.cmbNomeFuncionario.Name = "cmbNomeFuncionario";
            this.cmbNomeFuncionario.Size = new System.Drawing.Size(375, 21);
            this.cmbNomeFuncionario.TabIndex = 1;
            this.cmbNomeFuncionario.ValueMember = "idusuario";
            this.cmbNomeFuncionario.SelectedIndexChanged += new System.EventHandler(this.cmbNomeFuncionario_SelectedIndexChanged);
            this.cmbNomeFuncionario.SelectedValueChanged += new System.EventHandler(this.comboBox6_SelectedValueChanged);
            // 
            // bindingNavigator
            // 
            this.bindingNavigator.AddNewItem = this.tsbInserir;
            this.bindingNavigator.BindingSource = this.osEtiquetaBindingSource;
            this.bindingNavigator.CountItem = null;
            this.bindingNavigator.CountItemFormat = "de {0}";
            this.bindingNavigator.DeleteItem = null;
            this.bindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bindingNavigator.ImageScalingSize = new System.Drawing.Size(48, 48);
            this.bindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbInserir,
            this.tsbGravar,
            this.toolStripSeparator4,
            this.tsbPrimeiro,
            this.tsbAnterior,
            this.tsbProximo,
            this.tsbUltimo,
            this.toolStripSeparator1,
            this.tsbImprimir,
            this.toolStripSeparator2,
            this.btnSair,
            this.toolStripSeparator3,
            this.toolStripButton1});
            this.bindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator.MoveFirstItem = null;
            this.bindingNavigator.MoveLastItem = null;
            this.bindingNavigator.MoveNextItem = null;
            this.bindingNavigator.MovePreviousItem = null;
            this.bindingNavigator.Name = "bindingNavigator";
            this.bindingNavigator.PositionItem = null;
            this.bindingNavigator.Size = new System.Drawing.Size(790, 70);
            this.bindingNavigator.TabIndex = 0;
            this.bindingNavigator.Text = "bindingNavigator1";
            // 
            // tsbInserir
            // 
            this.tsbInserir.Image = global::osExpress.Properties.Resources.edit2;
            this.tsbInserir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbInserir.Name = "tsbInserir";
            this.tsbInserir.RightToLeftAutoMirrorImage = true;
            this.tsbInserir.Size = new System.Drawing.Size(52, 67);
            this.tsbInserir.Text = "&Incluir";
            this.tsbInserir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbInserir.Click += new System.EventHandler(this.tsbInserir_Click);
            // 
            // tsbGravar
            // 
            this.tsbGravar.Image = global::osExpress.Properties.Resources.save;
            this.tsbGravar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbGravar.Name = "tsbGravar";
            this.tsbGravar.Size = new System.Drawing.Size(52, 67);
            this.tsbGravar.Text = "Salv&ar";
            this.tsbGravar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbGravar.Click += new System.EventHandler(this.tsbGravar_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 70);
            // 
            // tsbPrimeiro
            // 
            this.tsbPrimeiro.Image = global::osExpress.Properties.Resources.first;
            this.tsbPrimeiro.Name = "tsbPrimeiro";
            this.tsbPrimeiro.RightToLeftAutoMirrorImage = true;
            this.tsbPrimeiro.Size = new System.Drawing.Size(56, 67);
            this.tsbPrimeiro.Text = "&Primeiro";
            this.tsbPrimeiro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbPrimeiro.Click += new System.EventHandler(this.tsbPrimeiro_Click);
            // 
            // tsbAnterior
            // 
            this.tsbAnterior.Image = global::osExpress.Properties.Resources.previous;
            this.tsbAnterior.Name = "tsbAnterior";
            this.tsbAnterior.RightToLeftAutoMirrorImage = true;
            this.tsbAnterior.Size = new System.Drawing.Size(54, 67);
            this.tsbAnterior.Text = "Anteri&or";
            this.tsbAnterior.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbAnterior.Click += new System.EventHandler(this.tsbAnterior_Click);
            // 
            // tsbProximo
            // 
            this.tsbProximo.Image = global::osExpress.Properties.Resources.next;
            this.tsbProximo.Name = "tsbProximo";
            this.tsbProximo.RightToLeftAutoMirrorImage = true;
            this.tsbProximo.Size = new System.Drawing.Size(56, 67);
            this.tsbProximo.Text = "Próxi&mo";
            this.tsbProximo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbProximo.Click += new System.EventHandler(this.tsbProximo_Click);
            // 
            // tsbUltimo
            // 
            this.tsbUltimo.Image = global::osExpress.Properties.Resources.last;
            this.tsbUltimo.Name = "tsbUltimo";
            this.tsbUltimo.RightToLeftAutoMirrorImage = true;
            this.tsbUltimo.Size = new System.Drawing.Size(52, 67);
            this.tsbUltimo.Text = "&Último";
            this.tsbUltimo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbUltimo.Click += new System.EventHandler(this.tsbUltimo_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 70);
            // 
            // tsbImprimir
            // 
            this.tsbImprimir.Image = global::osExpress.Properties.Resources.printer;
            this.tsbImprimir.ImageTransparentColor = System.Drawing.Color.Linen;
            this.tsbImprimir.Name = "tsbImprimir";
            this.tsbImprimir.Size = new System.Drawing.Size(58, 67);
            this.tsbImprimir.Text = "Relatório";
            this.tsbImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbImprimir.Click += new System.EventHandler(this.tsbImprimir_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 70);
            // 
            // btnSair
            // 
            this.btnSair.Image = global::osExpress.Properties.Resources.gohome;
            this.btnSair.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(52, 67);
            this.btnSair.Text = "&Sair";
            this.btnSair.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 70);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = global::osExpress.Properties.Resources.organize;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(52, 67);
            this.toolStripButton1.Text = "Atribuir";
            this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // osEtiquetaDataGridView
            // 
            this.osEtiquetaDataGridView.AllowUserToAddRows = false;
            this.osEtiquetaDataGridView.AutoGenerateColumns = false;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.osEtiquetaDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.osEtiquetaDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.osEtiquetaDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4});
            this.osEtiquetaDataGridView.DataSource = this.osEtiquetaBindingSource;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.osEtiquetaDataGridView.DefaultCellStyle = dataGridViewCellStyle5;
            this.osEtiquetaDataGridView.Location = new System.Drawing.Point(12, 214);
            this.osEtiquetaDataGridView.Name = "osEtiquetaDataGridView";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.osEtiquetaDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.osEtiquetaDataGridView.Size = new System.Drawing.Size(758, 220);
            this.osEtiquetaDataGridView.TabIndex = 7;
            this.osEtiquetaDataGridView.TabStop = false;
            this.osEtiquetaDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.osEtiquetaDataGridView_CellClick);
            this.osEtiquetaDataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.osEtiquetaDataGridView_CellEndEdit);
            this.osEtiquetaDataGridView.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.osEtiquetaDataGridView_UserDeletedRow);
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "Column1";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Visible = false;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "TIPO";
            this.dataGridViewTextBoxColumn3.HeaderText = "Tipo";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "NUM_ETIQUETA";
            this.dataGridViewTextBoxColumn4.HeaderText = "Número de Etiqueta";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 150;
            // 
            // bdsUsuario
            // 
            this.bdsUsuario.DataMember = "OsUsuario";
            this.bdsUsuario.DataSource = this.dtsPrincipal;
            this.bdsUsuario.Sort = "NOME ASC";
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.A30CAIXA_PROG_EXTableAdapter = null;
            this.tableAdapterManager.A30CAIXA_PROGTableAdapter = null;
            this.tableAdapterManager.A30CLIENTESTableAdapter = null;
            this.tableAdapterManager.A30FUNCIONARIOTableAdapter = null;
            this.tableAdapterManager.A30PLANO_CONTATableAdapter = null;
            this.tableAdapterManager.A30POSTOSTableAdapter = null;
            this.tableAdapterManager.A30PRODUTOTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.OsClienteTableAdapter = null;
            this.tableAdapterManager.OsEtiquetaOsClienteTableAdapter = null;
            this.tableAdapterManager.OsEtiquetaTableAdapter = this.osEtiquetaTableAdapter;
            this.tableAdapterManager.OsFlashDriveTableAdapter = null;
            this.tableAdapterManager.OsFormasPagamentoTableAdapter = null;
            this.tableAdapterManager.OsIbametroInmetroOsClienteTableAdapter = null;
            this.tableAdapterManager.OsIbametroInmetroTableAdapter = null;
            this.tableAdapterManager.OsIbametroOsCliente1TableAdapter = null;
            this.tableAdapterManager.OsIbametroOsClienteTableAdapter = null;
            this.tableAdapterManager.OsIbametroTableAdapter = null;
            this.tableAdapterManager.OsInstrumentoServicoOsClienteTableAdapter = null;
            this.tableAdapterManager.OsInstrumentoServicoTableAdapter = null;
            this.tableAdapterManager.OsItemServicoTableAdapter = null;
            this.tableAdapterManager.OsOrdemParcelaTableAdapter = null;
            this.tableAdapterManager.OsOrdemServicoTableAdapter = null;
            this.tableAdapterManager.OsParametrosTableAdapter = null;
            this.tableAdapterManager.OsParContasTableAdapter = null;
            this.tableAdapterManager.OsParSituacaoTableAdapter = null;
            this.tableAdapterManager.OsProdutosServicosTableAdapter = null;
            this.tableAdapterManager.OsReciboTableAdapter = null;
            this.tableAdapterManager.OsSituacoesOSTableAdapter = null;
            this.tableAdapterManager.OsUsuarioTableAdapter = null;
            this.tableAdapterManager.OsVersaoTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = osExpress.OsExpressDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.XOsFuncionarioTableAdapter = null;
            // 
            // osEtiquetaTableAdapter
            // 
            this.osEtiquetaTableAdapter.ClearBeforeFill = true;
            // 
            // osUsuarioTableAdapter
            // 
            this.osUsuarioTableAdapter.ClearBeforeFill = true;
            // 
            // pnlAtribuir
            // 
            this.pnlAtribuir.Controls.Add(this.btnAtribuicao);
            this.pnlAtribuir.Controls.Add(this.pnlCancelarAtribuicao);
            this.pnlAtribuir.Controls.Add(this.label3);
            this.pnlAtribuir.Controls.Add(this.cmbFuncionarioAtribuir);
            this.pnlAtribuir.Location = new System.Drawing.Point(365, 244);
            this.pnlAtribuir.Name = "pnlAtribuir";
            this.pnlAtribuir.Size = new System.Drawing.Size(227, 93);
            this.pnlAtribuir.TabIndex = 56;
            this.pnlAtribuir.Visible = false;
            // 
            // btnAtribuicao
            // 
            this.btnAtribuicao.Location = new System.Drawing.Point(12, 59);
            this.btnAtribuicao.Name = "btnAtribuicao";
            this.btnAtribuicao.Size = new System.Drawing.Size(75, 23);
            this.btnAtribuicao.TabIndex = 3;
            this.btnAtribuicao.Text = "Atribuir";
            this.btnAtribuicao.UseVisualStyleBackColor = true;
            this.btnAtribuicao.Click += new System.EventHandler(this.btnAtribuicao_Click);
            // 
            // pnlCancelarAtribuicao
            // 
            this.pnlCancelarAtribuicao.Location = new System.Drawing.Point(93, 59);
            this.pnlCancelarAtribuicao.Name = "pnlCancelarAtribuicao";
            this.pnlCancelarAtribuicao.Size = new System.Drawing.Size(75, 23);
            this.pnlCancelarAtribuicao.TabIndex = 2;
            this.pnlCancelarAtribuicao.Text = "Cancelar";
            this.pnlCancelarAtribuicao.UseVisualStyleBackColor = true;
            this.pnlCancelarAtribuicao.Click += new System.EventHandler(this.pnlCancelarAtribuicao_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(210, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Selecione o funcionario que deseja atribuir:";
            // 
            // cmbFuncionarioAtribuir
            // 
            this.cmbFuncionarioAtribuir.DataSource = this.bdsXOSFuncionario;
            this.cmbFuncionarioAtribuir.DisplayMember = "uNome";
            this.cmbFuncionarioAtribuir.FormattingEnabled = true;
            this.cmbFuncionarioAtribuir.Location = new System.Drawing.Point(12, 32);
            this.cmbFuncionarioAtribuir.Name = "cmbFuncionarioAtribuir";
            this.cmbFuncionarioAtribuir.Size = new System.Drawing.Size(156, 21);
            this.cmbFuncionarioAtribuir.TabIndex = 0;
            this.cmbFuncionarioAtribuir.ValueMember = "idusuario";
            // 
            // OsXOSFuncionarioBindingSource
            // 
            this.OsXOSFuncionarioBindingSource.DataMember = "XOsFuncionario";
            this.OsXOSFuncionarioBindingSource.DataSource = this.dtsPrincipal;
            this.OsXOSFuncionarioBindingSource.Filter = "isusuario = \'S\'";
            this.OsXOSFuncionarioBindingSource.Sort = "uNome ASC";
            // 
            // xOsFuncionarioTableAdapter
            // 
            this.xOsFuncionarioTableAdapter.ClearBeforeFill = true;
            // 
            // bdsXOSFuncionario
            // 
            this.bdsXOSFuncionario.DataMember = "XOsFuncionario";
            this.bdsXOSFuncionario.DataSource = this.dtsPrincipal;
            this.bdsXOSFuncionario.Filter = "isusuario = \'S\'";
            this.bdsXOSFuncionario.Sort = "uNome ASC";
            // 
            // ManEtiqueta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(790, 468);
            this.Controls.Add(this.pnlAtribuir);
            this.Controls.Add(this.osEtiquetaDataGridView);
            this.Controls.Add(this.bindingNavigator);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.MaximizeBox = true;
            this.MinimizeBox = true;
            this.Name = "ManEtiqueta";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Manutenção de Selos / Lacres";
            this.Load += new System.EventHandler(this.ManIbametro_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ManIbametro_KeyDown);
            this.Controls.SetChildIndex(this.bindingNavigator, 0);
            this.Controls.SetChildIndex(this.osEtiquetaDataGridView, 0);
            this.Controls.SetChildIndex(this.pnlAtribuir, 0);
            this.Controls.SetChildIndex(this.tabPrincipal, 0);
            this.Controls.SetChildIndex(this.grpPrincipal, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dtsPrincipal)).EndInit();
            this.tabPrincipal.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.osEtiquetaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.osUsuarioBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator)).EndInit();
            this.bindingNavigator.ResumeLayout(false);
            this.bindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.osEtiquetaDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsUsuario)).EndInit();
            this.pnlAtribuir.ResumeLayout(false);
            this.pnlAtribuir.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OsXOSFuncionarioBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsXOSFuncionario)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage1;
        private OsExpressDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator bindingNavigator;
        private System.Windows.Forms.ToolStripButton tsbInserir;
        private System.Windows.Forms.ToolStripButton tsbGravar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton tsbPrimeiro;
        private System.Windows.Forms.ToolStripButton tsbAnterior;
        private System.Windows.Forms.ToolStripButton tsbProximo;
        private System.Windows.Forms.ToolStripButton tsbUltimo;
        private System.Windows.Forms.ComboBox cmbCodigoFuncionario;
        private System.Windows.Forms.ComboBox cmbNomeFuncionario;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnSair;
        private System.Windows.Forms.ToolStripButton tsbImprimir;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.BindingSource osUsuarioBindingSource;
        private OsExpressDataSetTableAdapters.OsUsuarioTableAdapter osUsuarioTableAdapter;
        private System.Windows.Forms.BindingSource osEtiquetaBindingSource;
        private OsExpressDataSetTableAdapters.OsEtiquetaTableAdapter osEtiquetaTableAdapter;
        private System.Windows.Forms.TextBox nUM_ETIQUETATextBox;
        private System.Windows.Forms.DataGridView osEtiquetaDataGridView;
        private System.Windows.Forms.ComboBox cmbTipoEtiqueta;
        private System.Windows.Forms.Button btnInserirMecanico;
        private System.Windows.Forms.BindingSource bdsUsuario;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbSituacaoEtiquetas;
        private System.Windows.Forms.Panel pnlAtribuir;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ComboBox cmbFuncionarioAtribuir;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnAtribuicao;
        private System.Windows.Forms.Button pnlCancelarAtribuicao;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.BindingSource OsXOSFuncionarioBindingSource;
        private OsExpressDataSetTableAdapters.XOsFuncionarioTableAdapter xOsFuncionarioTableAdapter;
        private System.Windows.Forms.BindingSource bdsXOSFuncionario;
    }
}
