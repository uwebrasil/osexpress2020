﻿namespace osExpress.Manutencoes
{
    partial class ManFlashDrive
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label label11;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label segundaLabel;
            System.Windows.Forms.Label tercaLabel;
            System.Windows.Forms.Label quartaLabel;
            System.Windows.Forms.Label quintaLabel;
            System.Windows.Forms.Label sextaLabel;
            System.Windows.Forms.Label sabadoLabel;
            System.Windows.Forms.Label domingoLabel;
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.osFlashDriveBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dbgDrivers = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.segundaTextBox = new System.Windows.Forms.TextBox();
            this.tercaTextBox = new System.Windows.Forms.TextBox();
            this.quartaTextBox = new System.Windows.Forms.TextBox();
            this.quintaTextBox = new System.Windows.Forms.TextBox();
            this.sextaTextBox = new System.Windows.Forms.TextBox();
            this.sabadoTextBox = new System.Windows.Forms.TextBox();
            this.domingoTextBox = new System.Windows.Forms.TextBox();
            this.ativoCheckBox = new System.Windows.Forms.CheckBox();
            this.cmbCodigoCliente = new System.Windows.Forms.ComboBox();
            this.a30CLIENTESBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cmbNomeCliente = new System.Windows.Forms.ComboBox();
            this.txtCodigoIBAMETRO = new System.Windows.Forms.TextBox();
            this.emailSigPosto = new System.Windows.Forms.TextBox();
            this.tableAdapterManager = new osExpress.OsExpressDataSetTableAdapters.TableAdapterManager();
            this.osFlashDriveTableAdapter = new osExpress.OsExpressDataSetTableAdapters.OsFlashDriveTableAdapter();
            this.bindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.tsbInserir = new System.Windows.Forms.ToolStripButton();
            this.tsbGravar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbPrimeiro = new System.Windows.Forms.ToolStripButton();
            this.tsbAnterior = new System.Windows.Forms.ToolStripButton();
            this.tsbProximo = new System.Windows.Forms.ToolStripButton();
            this.tsbUltimo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.tsbImprimir = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnSair = new System.Windows.Forms.ToolStripButton();
            this.a30CLIENTESTableAdapter = new osExpress.OsExpressDataSetTableAdapters.A30CLIENTESTableAdapter();
            this.tmrPrincipal = new System.Windows.Forms.Timer(this.components);
            label11 = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            segundaLabel = new System.Windows.Forms.Label();
            tercaLabel = new System.Windows.Forms.Label();
            quartaLabel = new System.Windows.Forms.Label();
            quintaLabel = new System.Windows.Forms.Label();
            sextaLabel = new System.Windows.Forms.Label();
            sabadoLabel = new System.Windows.Forms.Label();
            domingoLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dtsPrincipal)).BeginInit();
            this.tabPrincipal.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.osFlashDriveBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbgDrivers)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.a30CLIENTESBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator)).BeginInit();
            this.bindingNavigator.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpPrincipal
            // 
            this.grpPrincipal.Location = new System.Drawing.Point(12, 75);
            this.grpPrincipal.Size = new System.Drawing.Size(758, 10);
            this.grpPrincipal.Visible = false;
            // 
            // tabPrincipal
            // 
            this.tabPrincipal.Controls.Add(this.tabPage1);
            this.tabPrincipal.Location = new System.Drawing.Point(12, 91);
            this.tabPrincipal.Size = new System.Drawing.Size(758, 230);
            this.tabPrincipal.TabStop = false;
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label11.Location = new System.Drawing.Point(238, 12);
            label11.Name = "label11";
            label11.Size = new System.Drawing.Size(104, 13);
            label11.TabIndex = 45;
            label11.Text = "Nome do Cliente:";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label1.Location = new System.Drawing.Point(29, 11);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(93, 13);
            label1.TabIndex = 43;
            label1.Text = "Código Cliente:";
            // 
            // segundaLabel
            // 
            segundaLabel.AutoSize = true;
            segundaLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            segundaLabel.Location = new System.Drawing.Point(13, 22);
            segundaLabel.Name = "segundaLabel";
            segundaLabel.Size = new System.Drawing.Size(61, 13);
            segundaLabel.TabIndex = 62;
            segundaLabel.Text = "Segunda:";
            // 
            // tercaLabel
            // 
            tercaLabel.AutoSize = true;
            tercaLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tercaLabel.Location = new System.Drawing.Point(13, 48);
            tercaLabel.Name = "tercaLabel";
            tercaLabel.Size = new System.Drawing.Size(44, 13);
            tercaLabel.TabIndex = 64;
            tercaLabel.Text = "Terca:";
            // 
            // quartaLabel
            // 
            quartaLabel.AutoSize = true;
            quartaLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            quartaLabel.Location = new System.Drawing.Point(13, 74);
            quartaLabel.Name = "quartaLabel";
            quartaLabel.Size = new System.Drawing.Size(49, 13);
            quartaLabel.TabIndex = 66;
            quartaLabel.Text = "Quarta:";
            // 
            // quintaLabel
            // 
            quintaLabel.AutoSize = true;
            quintaLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            quintaLabel.Location = new System.Drawing.Point(13, 100);
            quintaLabel.Name = "quintaLabel";
            quintaLabel.Size = new System.Drawing.Size(48, 13);
            quintaLabel.TabIndex = 68;
            quintaLabel.Text = "Quinta:";
            // 
            // sextaLabel
            // 
            sextaLabel.AutoSize = true;
            sextaLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            sextaLabel.Location = new System.Drawing.Point(285, 22);
            sextaLabel.Name = "sextaLabel";
            sextaLabel.Size = new System.Drawing.Size(43, 13);
            sextaLabel.TabIndex = 70;
            sextaLabel.Text = "Sexta:";
            // 
            // sabadoLabel
            // 
            sabadoLabel.AutoSize = true;
            sabadoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            sabadoLabel.Location = new System.Drawing.Point(285, 48);
            sabadoLabel.Name = "sabadoLabel";
            sabadoLabel.Size = new System.Drawing.Size(54, 13);
            sabadoLabel.TabIndex = 72;
            sabadoLabel.Text = "Sabado:";
            // 
            // domingoLabel
            // 
            domingoLabel.AutoSize = true;
            domingoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            domingoLabel.Location = new System.Drawing.Point(285, 74);
            domingoLabel.Name = "domingoLabel";
            domingoLabel.Size = new System.Drawing.Size(60, 13);
            domingoLabel.TabIndex = 74;
            domingoLabel.Text = "Domingo:";
            // 
            // tabPage1
            // 
            this.tabPage1.AutoScroll = true;
            this.tabPage1.Controls.Add(this.dbgDrivers);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.ativoCheckBox);
            this.tabPage1.Controls.Add(this.cmbCodigoCliente);
            this.tabPage1.Controls.Add(label11);
            this.tabPage1.Controls.Add(this.cmbNomeCliente);
            this.tabPage1.Controls.Add(label1);
            this.tabPage1.Controls.Add(this.txtCodigoIBAMETRO);
            this.tabPage1.Controls.Add(this.emailSigPosto);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(750, 204);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Cadastro";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // osFlashDriveBindingSource
            // 
            this.osFlashDriveBindingSource.DataMember = "OsFlashDrive";
            this.osFlashDriveBindingSource.DataSource = this.dtsPrincipal;
            // 
            // dbgDrivers
            // 
            this.dbgDrivers.AllowUserToAddRows = false;
            this.dbgDrivers.AllowUserToDeleteRows = false;
            this.dbgDrivers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dbgDrivers.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4});
            this.dbgDrivers.Location = new System.Drawing.Point(619, 41);
            this.dbgDrivers.Name = "dbgDrivers";
            this.dbgDrivers.ReadOnly = true;
            this.dbgDrivers.Size = new System.Drawing.Size(125, 126);
            this.dbgDrivers.TabIndex = 65;
            this.dbgDrivers.Visible = false;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Driver";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 50;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Rótulo";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Numero Serial";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Tipo";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(segundaLabel);
            this.groupBox1.Controls.Add(this.segundaTextBox);
            this.groupBox1.Controls.Add(tercaLabel);
            this.groupBox1.Controls.Add(this.tercaTextBox);
            this.groupBox1.Controls.Add(quartaLabel);
            this.groupBox1.Controls.Add(this.quartaTextBox);
            this.groupBox1.Controls.Add(quintaLabel);
            this.groupBox1.Controls.Add(this.quintaTextBox);
            this.groupBox1.Controls.Add(sextaLabel);
            this.groupBox1.Controls.Add(this.sextaTextBox);
            this.groupBox1.Controls.Add(sabadoLabel);
            this.groupBox1.Controls.Add(this.sabadoTextBox);
            this.groupBox1.Controls.Add(domingoLabel);
            this.groupBox1.Controls.Add(this.domingoTextBox);
            this.groupBox1.Location = new System.Drawing.Point(50, 37);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(563, 130);
            this.groupBox1.TabIndex = 64;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = " Dias da Semana ";
            // 
            // segundaTextBox
            // 
            this.segundaTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osFlashDriveBindingSource, "Segunda", true));
            this.segundaTextBox.Location = new System.Drawing.Point(77, 19);
            this.segundaTextBox.Name = "segundaTextBox";
            this.segundaTextBox.Size = new System.Drawing.Size(200, 20);
            this.segundaTextBox.TabIndex = 63;
            // 
            // tercaTextBox
            // 
            this.tercaTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osFlashDriveBindingSource, "Terca", true));
            this.tercaTextBox.Location = new System.Drawing.Point(77, 45);
            this.tercaTextBox.Name = "tercaTextBox";
            this.tercaTextBox.Size = new System.Drawing.Size(200, 20);
            this.tercaTextBox.TabIndex = 65;
            // 
            // quartaTextBox
            // 
            this.quartaTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osFlashDriveBindingSource, "Quarta", true));
            this.quartaTextBox.Location = new System.Drawing.Point(77, 71);
            this.quartaTextBox.Name = "quartaTextBox";
            this.quartaTextBox.Size = new System.Drawing.Size(200, 20);
            this.quartaTextBox.TabIndex = 67;
            // 
            // quintaTextBox
            // 
            this.quintaTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osFlashDriveBindingSource, "Quinta", true));
            this.quintaTextBox.Location = new System.Drawing.Point(77, 97);
            this.quintaTextBox.Name = "quintaTextBox";
            this.quintaTextBox.Size = new System.Drawing.Size(200, 20);
            this.quintaTextBox.TabIndex = 69;
            // 
            // sextaTextBox
            // 
            this.sextaTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osFlashDriveBindingSource, "Sexta", true));
            this.sextaTextBox.Location = new System.Drawing.Point(348, 19);
            this.sextaTextBox.Name = "sextaTextBox";
            this.sextaTextBox.Size = new System.Drawing.Size(200, 20);
            this.sextaTextBox.TabIndex = 71;
            // 
            // sabadoTextBox
            // 
            this.sabadoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osFlashDriveBindingSource, "Sabado", true));
            this.sabadoTextBox.Location = new System.Drawing.Point(348, 45);
            this.sabadoTextBox.Name = "sabadoTextBox";
            this.sabadoTextBox.Size = new System.Drawing.Size(200, 20);
            this.sabadoTextBox.TabIndex = 73;
            // 
            // domingoTextBox
            // 
            this.domingoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osFlashDriveBindingSource, "Domingo", true));
            this.domingoTextBox.Location = new System.Drawing.Point(348, 71);
            this.domingoTextBox.Name = "domingoTextBox";
            this.domingoTextBox.Size = new System.Drawing.Size(200, 20);
            this.domingoTextBox.TabIndex = 75;
            // 
            // ativoCheckBox
            // 
            this.ativoCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.osFlashDriveBindingSource, "Ativo", true));
            this.ativoCheckBox.Location = new System.Drawing.Point(50, 173);
            this.ativoCheckBox.Name = "ativoCheckBox";
            this.ativoCheckBox.Size = new System.Drawing.Size(104, 24);
            this.ativoCheckBox.TabIndex = 63;
            this.ativoCheckBox.Text = "Ativo?";
            this.ativoCheckBox.UseVisualStyleBackColor = true;
            // 
            // cmbCodigoCliente
            // 
            this.cmbCodigoCliente.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCodigoCliente.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCodigoCliente.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osFlashDriveBindingSource, "IDCLIENTE", true));
            this.cmbCodigoCliente.DataSource = this.a30CLIENTESBindingSource;
            this.cmbCodigoCliente.DisplayMember = "Cliente";
            this.cmbCodigoCliente.FormattingEnabled = true;
            this.cmbCodigoCliente.Location = new System.Drawing.Point(128, 8);
            this.cmbCodigoCliente.Name = "cmbCodigoCliente";
            this.cmbCodigoCliente.Size = new System.Drawing.Size(100, 21);
            this.cmbCodigoCliente.TabIndex = 2;
            this.cmbCodigoCliente.ValueMember = "ID";
            this.cmbCodigoCliente.SelectedValueChanged += new System.EventHandler(this.comboBox6_SelectedValueChanged);
            // 
            // a30CLIENTESBindingSource
            // 
            this.a30CLIENTESBindingSource.AllowNew = false;
            this.a30CLIENTESBindingSource.DataMember = "A30CLIENTES";
            this.a30CLIENTESBindingSource.DataSource = this.dtsPrincipal;
            this.a30CLIENTESBindingSource.Filter = "INATIVO = 0 AND TIPO_PESSOA = \'J\'";
            this.a30CLIENTESBindingSource.Sort = "NOME ASC";
            // 
            // cmbNomeCliente
            // 
            this.cmbNomeCliente.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbNomeCliente.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbNomeCliente.DataSource = this.a30CLIENTESBindingSource;
            this.cmbNomeCliente.DisplayMember = "Nome";
            this.cmbNomeCliente.FormattingEnabled = true;
            this.cmbNomeCliente.Location = new System.Drawing.Point(348, 8);
            this.cmbNomeCliente.Name = "cmbNomeCliente";
            this.cmbNomeCliente.Size = new System.Drawing.Size(396, 21);
            this.cmbNomeCliente.TabIndex = 3;
            this.cmbNomeCliente.ValueMember = "ID";
            this.cmbNomeCliente.SelectedValueChanged += new System.EventHandler(this.comboBox6_SelectedValueChanged);
            // 
            // txtCodigoIBAMETRO
            // 
            this.txtCodigoIBAMETRO.Enabled = false;
            this.txtCodigoIBAMETRO.Location = new System.Drawing.Point(433, 37);
            this.txtCodigoIBAMETRO.Name = "txtCodigoIBAMETRO";
            this.txtCodigoIBAMETRO.ReadOnly = true;
            this.txtCodigoIBAMETRO.Size = new System.Drawing.Size(0, 20);
            this.txtCodigoIBAMETRO.TabIndex = 0;
            this.txtCodigoIBAMETRO.TabStop = false;
            // 
            // emailSigPosto
            // 
            this.emailSigPosto.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.a30CLIENTESBindingSource, "obs01", true));
            this.emailSigPosto.Location = new System.Drawing.Point(202, 85);
            this.emailSigPosto.Name = "emailSigPosto";
            this.emailSigPosto.Size = new System.Drawing.Size(396, 20);
            this.emailSigPosto.TabIndex = 69;
            this.emailSigPosto.TabStop = false;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.A30CAIXA_PROG_EXTableAdapter = null;
            this.tableAdapterManager.A30CAIXA_PROGTableAdapter = null;
            this.tableAdapterManager.A30CLIENTESTableAdapter = null;
            this.tableAdapterManager.A30FUNCIONARIOTableAdapter = null;
            this.tableAdapterManager.A30PLANO_CONTATableAdapter = null;
            this.tableAdapterManager.A30POSTOSTableAdapter = null;
            this.tableAdapterManager.A30PRODUTOTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.OsClienteTableAdapter = null;
            this.tableAdapterManager.OsEtiquetaTableAdapter = null;
            this.tableAdapterManager.OsFlashDriveTableAdapter = this.osFlashDriveTableAdapter;
            this.tableAdapterManager.OsFormasPagamentoTableAdapter = null;
            this.tableAdapterManager.OsIbametroInmetroTableAdapter = null;
            this.tableAdapterManager.OsIbametroTableAdapter = null;
            this.tableAdapterManager.OsInstrumentoServicoTableAdapter = null;
            this.tableAdapterManager.OsItemServicoTableAdapter = null;
            this.tableAdapterManager.OsOrdemParcelaTableAdapter = null;
            this.tableAdapterManager.OsOrdemServicoTableAdapter = null;
            this.tableAdapterManager.OsParametrosTableAdapter = null;
            this.tableAdapterManager.OsParContasTableAdapter = null;
            this.tableAdapterManager.OsParSituacaoTableAdapter = null;
            this.tableAdapterManager.OsProdutosServicosTableAdapter = null;
            this.tableAdapterManager.OsReciboTableAdapter = null;
            this.tableAdapterManager.OsSituacoesOSTableAdapter = null;
            this.tableAdapterManager.OsUsuarioTableAdapter = null;
            this.tableAdapterManager.OsVersaoTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = osExpress.OsExpressDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // osFlashDriveTableAdapter
            // 
            this.osFlashDriveTableAdapter.ClearBeforeFill = true;
            // 
            // bindingNavigator
            // 
            this.bindingNavigator.AddNewItem = null;
            this.bindingNavigator.BindingSource = this.osFlashDriveBindingSource;
            this.bindingNavigator.CountItem = null;
            this.bindingNavigator.CountItemFormat = "de {0}";
            this.bindingNavigator.DeleteItem = null;
            this.bindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bindingNavigator.ImageScalingSize = new System.Drawing.Size(48, 48);
            this.bindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbInserir,
            this.tsbGravar,
            this.toolStripSeparator4,
            this.tsbPrimeiro,
            this.tsbAnterior,
            this.tsbProximo,
            this.tsbUltimo,
            this.toolStripSeparator1,
            this.toolStripButton1,
            this.tsbImprimir,
            this.toolStripSeparator2,
            this.btnSair});
            this.bindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator.MoveFirstItem = null;
            this.bindingNavigator.MoveLastItem = null;
            this.bindingNavigator.MoveNextItem = null;
            this.bindingNavigator.MovePreviousItem = null;
            this.bindingNavigator.Name = "bindingNavigator";
            this.bindingNavigator.PositionItem = null;
            this.bindingNavigator.Size = new System.Drawing.Size(784, 70);
            this.bindingNavigator.TabIndex = 0;
            this.bindingNavigator.Text = "bindingNavigator1";
            // 
            // tsbInserir
            // 
            this.tsbInserir.Image = global::osExpress.Properties.Resources.edit2;
            this.tsbInserir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbInserir.Name = "tsbInserir";
            this.tsbInserir.RightToLeftAutoMirrorImage = true;
            this.tsbInserir.Size = new System.Drawing.Size(52, 67);
            this.tsbInserir.Text = "&Incluir";
            this.tsbInserir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbInserir.Click += new System.EventHandler(this.tsbInserir_Click);
            // 
            // tsbGravar
            // 
            this.tsbGravar.Image = global::osExpress.Properties.Resources.save;
            this.tsbGravar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbGravar.Name = "tsbGravar";
            this.tsbGravar.Size = new System.Drawing.Size(52, 67);
            this.tsbGravar.Text = "Salv&ar";
            this.tsbGravar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbGravar.Click += new System.EventHandler(this.tsbGravar_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 70);
            // 
            // tsbPrimeiro
            // 
            this.tsbPrimeiro.Image = global::osExpress.Properties.Resources.first;
            this.tsbPrimeiro.Name = "tsbPrimeiro";
            this.tsbPrimeiro.RightToLeftAutoMirrorImage = true;
            this.tsbPrimeiro.Size = new System.Drawing.Size(56, 67);
            this.tsbPrimeiro.Text = "&Primeiro";
            this.tsbPrimeiro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbPrimeiro.Click += new System.EventHandler(this.tsbPrimeiro_Click);
            // 
            // tsbAnterior
            // 
            this.tsbAnterior.Image = global::osExpress.Properties.Resources.previous;
            this.tsbAnterior.Name = "tsbAnterior";
            this.tsbAnterior.RightToLeftAutoMirrorImage = true;
            this.tsbAnterior.Size = new System.Drawing.Size(54, 67);
            this.tsbAnterior.Text = "Anteri&or";
            this.tsbAnterior.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbAnterior.Click += new System.EventHandler(this.tsbAnterior_Click);
            // 
            // tsbProximo
            // 
            this.tsbProximo.Image = global::osExpress.Properties.Resources.next;
            this.tsbProximo.Name = "tsbProximo";
            this.tsbProximo.RightToLeftAutoMirrorImage = true;
            this.tsbProximo.Size = new System.Drawing.Size(55, 67);
            this.tsbProximo.Text = "Próxi&mo";
            this.tsbProximo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbProximo.Click += new System.EventHandler(this.tsbProximo_Click);
            // 
            // tsbUltimo
            // 
            this.tsbUltimo.Image = global::osExpress.Properties.Resources.last;
            this.tsbUltimo.Name = "tsbUltimo";
            this.tsbUltimo.RightToLeftAutoMirrorImage = true;
            this.tsbUltimo.Size = new System.Drawing.Size(52, 67);
            this.tsbUltimo.Text = "&Último";
            this.tsbUltimo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbUltimo.Click += new System.EventHandler(this.tsbUltimo_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 70);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = global::osExpress.Properties.Resources.down;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(81, 67);
            this.toolStripButton1.Text = "Exportar &XML";
            this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // tsbImprimir
            // 
            this.tsbImprimir.Enabled = false;
            this.tsbImprimir.Image = global::osExpress.Properties.Resources.printer;
            this.tsbImprimir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbImprimir.Name = "tsbImprimir";
            this.tsbImprimir.Size = new System.Drawing.Size(57, 67);
            this.tsbImprimir.Text = "Imprimi&r";
            this.tsbImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbImprimir.Click += new System.EventHandler(this.tsbImprimir_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 70);
            // 
            // btnSair
            // 
            this.btnSair.Image = global::osExpress.Properties.Resources.gohome;
            this.btnSair.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(52, 67);
            this.btnSair.Text = "&Sair";
            this.btnSair.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // a30CLIENTESTableAdapter
            // 
            this.a30CLIENTESTableAdapter.ClearBeforeFill = true;
            // 
            // tmrPrincipal
            // 
            this.tmrPrincipal.Interval = 1000;
            this.tmrPrincipal.Tick += new System.EventHandler(this.tmrPrincipal_Tick);
            // 
            // ManFlashDrive
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(784, 346);
            this.Controls.Add(this.bindingNavigator);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.MaximizeBox = true;
            this.MinimizeBox = true;
            this.Name = "ManFlashDrive";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Manutenção de FlashDrive por Cliente";
            this.Load += new System.EventHandler(this.ManIbametro_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ManIbametro_KeyDown);
            this.Controls.SetChildIndex(this.tabPrincipal, 0);
            this.Controls.SetChildIndex(this.bindingNavigator, 0);
            this.Controls.SetChildIndex(this.grpPrincipal, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dtsPrincipal)).EndInit();
            this.tabPrincipal.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.osFlashDriveBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbgDrivers)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.a30CLIENTESBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator)).EndInit();
            this.bindingNavigator.ResumeLayout(false);
            this.bindingNavigator.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage1;
        private OsExpressDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator bindingNavigator;
        private System.Windows.Forms.ToolStripButton tsbInserir;
        private System.Windows.Forms.ToolStripButton tsbGravar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton tsbPrimeiro;
        private System.Windows.Forms.ToolStripButton tsbAnterior;
        private System.Windows.Forms.ToolStripButton tsbProximo;
        private System.Windows.Forms.ToolStripButton tsbUltimo;
        private System.Windows.Forms.ComboBox cmbCodigoCliente;
        private System.Windows.Forms.ComboBox cmbNomeCliente;
        private System.Windows.Forms.TextBox txtCodigoIBAMETRO;
        private System.Windows.Forms.BindingSource a30CLIENTESBindingSource;
        private OsExpressDataSetTableAdapters.A30CLIENTESTableAdapter a30CLIENTESTableAdapter;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnSair;
        private System.Windows.Forms.ToolStripButton tsbImprimir;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.BindingSource osFlashDriveBindingSource;
        private OsExpressDataSetTableAdapters.OsFlashDriveTableAdapter osFlashDriveTableAdapter;
        private System.Windows.Forms.CheckBox ativoCheckBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox segundaTextBox;
        private System.Windows.Forms.TextBox tercaTextBox;
        private System.Windows.Forms.TextBox quartaTextBox;
        private System.Windows.Forms.TextBox quintaTextBox;
        private System.Windows.Forms.TextBox sextaTextBox;
        private System.Windows.Forms.TextBox sabadoTextBox;
        private System.Windows.Forms.TextBox domingoTextBox;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.DataGridView dbgDrivers;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.Timer tmrPrincipal;
        private System.Windows.Forms.TextBox emailSigPosto;
    }
}
