﻿namespace osExpress.Manutencoes
{
    partial class ManOrdemServico
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label iDLabel;
            System.Windows.Forms.Label iDUSUARIOLabel;
            System.Windows.Forms.Label iDCLIENTELabel;
            System.Windows.Forms.Label dT_CADASTROLabel;
            System.Windows.Forms.Label dT_FECHAMENTOLabel;
            System.Windows.Forms.Label oBSLabel;
            System.Windows.Forms.Label label9;
            System.Windows.Forms.Label label48;
            System.Windows.Forms.Label dATA_INICIALLabel;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bdsSelectEquipamento = new System.Windows.Forms.BindingSource(this.components);
            this.bindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.tsbInserir = new System.Windows.Forms.ToolStripButton();
            this.osOrdemServicoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.tsbCancelar = new System.Windows.Forms.ToolStripButton();
            this.tsbSalvar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbDescartar = new System.Windows.Forms.ToolStripButton();
            this.tsbFecharOS = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbImprimirRecibo = new System.Windows.Forms.ToolStripButton();
            this.tsbImprimirOS = new System.Windows.Forms.ToolStripButton();
            this.tsbGerarCaixa = new System.Windows.Forms.ToolStripButton();
            this.tabCabecalho = new System.Windows.Forms.TabPage();
            this.lblFiltroLike = new System.Windows.Forms.Label();
            this.tbFiltroLike = new System.Windows.Forms.TextBox();
            this.tbCodCliente = new System.Windows.Forms.TextBox();
            this.bdsClientes = new System.Windows.Forms.BindingSource(this.components);
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.txtAssinaturaCliente = new System.Windows.Forms.TextBox();
            this.cmbNomeUsuario = new System.Windows.Forms.ComboBox();
            this.OsXOSFuncionarioBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cmbCodigoUsuario = new System.Windows.Forms.ComboBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.cmbNomeCliente = new System.Windows.Forms.ComboBox();
            this.chkSuporte = new System.Windows.Forms.CheckBox();
            this.chkInstalacao = new System.Windows.Forms.CheckBox();
            this.cmbCodCliente = new System.Windows.Forms.ComboBox();
            this.txtCodOrdemServico = new System.Windows.Forms.TextBox();
            this.dtpDataCadastro = new System.Windows.Forms.DateTimePicker();
            this.dtpDataFechamento = new System.Windows.Forms.DateTimePicker();
            this.txtObservacao = new System.Windows.Forms.TextBox();
            this.osUsuarioBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tabSolicitacao = new System.Windows.Forms.TabPage();
            this.btnInserirTipoSolicitacao = new System.Windows.Forms.Button();
            this.cmbDescSolicitacao = new System.Windows.Forms.ComboBox();
            this.bdsSolicitacoes = new System.Windows.Forms.BindingSource(this.components);
            this.cmbCodigoSolicitacao = new System.Windows.Forms.ComboBox();
            this.grdSolicitacao = new System.Windows.Forms.DataGridView();
            this.iDSITUACAODataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DESCRICAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtsSelectSituacao = new System.Windows.Forms.BindingSource(this.components);
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.osSituacoesOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tabEncontrada = new System.Windows.Forms.TabPage();
            this.btnInserirSituacaoEncontrada = new System.Windows.Forms.Button();
            this.grdEncontrada = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn42 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn43 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmbDescSitEncontrada = new System.Windows.Forms.ComboBox();
            this.bdsEncontrada = new System.Windows.Forms.BindingSource(this.components);
            this.cmbCodigoSitEncontrada = new System.Windows.Forms.ComboBox();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.tabRealizada = new System.Windows.Forms.TabPage();
            this.dATA_INICIALDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.btnAtividadeRealizada = new System.Windows.Forms.Button();
            this.cmbDescricaoRealizada = new System.Windows.Forms.ComboBox();
            this.bdsRealizada = new System.Windows.Forms.BindingSource(this.components);
            this.cmbCodigoRealizada = new System.Windows.Forms.ComboBox();
            this.dtpHoraTotalRealizada = new System.Windows.Forms.DateTimePicker();
            this.dtpHoraFinalRealizada = new System.Windows.Forms.DateTimePicker();
            this.dtpHoraInicialRealizada = new System.Windows.Forms.DateTimePicker();
            this.grdRealizada = new System.Windows.Forms.DataGridView();
            this.iDSITUACAODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DATA_INICIAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hORAINICIODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hORAFIMDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hORATOTALDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descricao123 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.a30PRODUTOBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tabFinal = new System.Windows.Forms.TabPage();
            this.btnInserirSituacaoFinal = new System.Windows.Forms.Button();
            this.grsFinal = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn37 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn38 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmbDescSituacaoFinal = new System.Windows.Forms.ComboBox();
            this.bdsFinal = new System.Windows.Forms.BindingSource(this.components);
            this.cmbCodigoSituacaoFinal = new System.Windows.Forms.ComboBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.tabPecas = new System.Windows.Forms.TabPage();
            this.numQtdeProduto = new System.Windows.Forms.NumericUpDown();
            this.grdPecas = new System.Windows.Forms.DataGridView();
            this.qUANTIDADEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vALORDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tOTALDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Produto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descrição = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.osItemServicoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnAdicionarPecas = new System.Windows.Forms.Button();
            this.label37 = new System.Windows.Forms.Label();
            this.txtTotalProduto = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txtPrecoProduto = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.cmbCodigoIDProduto = new System.Windows.Forms.ComboBox();
            this.cmbDescricaoProduto = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cmbCodigoProduto = new System.Windows.Forms.ComboBox();
            this.tabServicos = new System.Windows.Forms.TabPage();
            this.chkPorContaGeral = new System.Windows.Forms.CheckBox();
            this.grpDespesas = new System.Windows.Forms.GroupBox();
            this.chkPorContaOutros = new System.Windows.Forms.CheckBox();
            this.chkPorContaTranslado = new System.Windows.Forms.CheckBox();
            this.chkPorContaHospedagem = new System.Windows.Forms.CheckBox();
            this.chkPorContaAlimentacao = new System.Windows.Forms.CheckBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txtTotalDesp = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.txtOutrasDespesas = new System.Windows.Forms.TextBox();
            this.txtTranslado = new System.Windows.Forms.TextBox();
            this.txtHospedagem = new System.Windows.Forms.TextBox();
            this.txtAlimentacao = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.label26 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.lb_media_de_consumo = new System.Windows.Forms.Label();
            this.lbl_total_de_kilometragem = new System.Windows.Forms.Label();
            this.tb_preco = new System.Windows.Forms.TextBox();
            this.tb_media = new System.Windows.Forms.TextBox();
            this.tb_kilometragem = new System.Windows.Forms.TextBox();
            this.txtHoraDeslocamentoQtde = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txtTotalServicosTotal = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.txtOutrosServicosTotal = new System.Windows.Forms.TextBox();
            this.txtDeslocamentoKmTotal = new System.Windows.Forms.TextBox();
            this.txtPontoRedeTotal = new System.Windows.Forms.TextBox();
            this.txtTaxaInstalacaoTotal = new System.Windows.Forms.TextBox();
            this.txtHoraExcedenteTotal = new System.Windows.Forms.TextBox();
            this.txtHoraDeslocamentoTotal = new System.Windows.Forms.TextBox();
            this.txtVisitaTecnicaTotal = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.txtOutrosServicosQtde = new System.Windows.Forms.TextBox();
            this.txtOutrosServicosVlr = new System.Windows.Forms.TextBox();
            this.txtDeslocamentoKmQtde = new System.Windows.Forms.TextBox();
            this.txtPontoRedeQtde = new System.Windows.Forms.TextBox();
            this.txtTaxaInstalacaoQtde = new System.Windows.Forms.TextBox();
            this.txtHoraExcedenteQtde = new System.Windows.Forms.TextBox();
            this.txtVisitaTecnicaQtde = new System.Windows.Forms.TextBox();
            this.txtDeslocamentoKmVlr = new System.Windows.Forms.TextBox();
            this.txtPontoRedeVlr = new System.Windows.Forms.TextBox();
            this.txtTaxaInstalacaoVlr = new System.Windows.Forms.TextBox();
            this.txtHoraExcedenteVlr = new System.Windows.Forms.TextBox();
            this.txtHoraDeslocamentoVlr = new System.Windows.Forms.TextBox();
            this.txtVisitaTecnicaVlr = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lblCabCliente = new System.Windows.Forms.Label();
            this.lblCabNCPJ = new System.Windows.Forms.Label();
            this.lblCabIE = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblCabTelefone = new System.Windows.Forms.Label();
            this.lblCabObservacao = new System.Windows.Forms.Label();
            this.tabEquipamentos = new System.Windows.Forms.TabPage();
            this.tbSeloAnterior = new System.Windows.Forms.TextBox();
            this.lblSeloAnterior = new System.Windows.Forms.Label();
            this.lblMarcaInitial = new System.Windows.Forms.Label();
            this.tbMarcaInicial = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.NUM_SERIE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Numero_IMETRO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numero_etiqueta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDETIQUETADataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDIBAMETRODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MARCA_INSTRUMENTO_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDORDEMDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.marca_inicial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.selo_anterior = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bdsSelectEquipamentoX = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cmbNumInmetroSelos = new System.Windows.Forms.ComboBox();
            this.bdsINMETROSelosX = new System.Windows.Forms.BindingSource(this.components);
            this.label55 = new System.Windows.Forms.Label();
            this.cmbCodigoINMETROSelos = new System.Windows.Forms.ComboBox();
            this.cmbCodigoEquipamentoSelos = new System.Windows.Forms.ComboBox();
            this.bdsEquipamentoSelosX = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.cmbNumeroEtiquetaSelos = new System.Windows.Forms.ComboBox();
            this.bdsSelos = new System.Windows.Forms.BindingSource(this.components);
            this.label52 = new System.Windows.Forms.Label();
            this.cmbCodigoSelos = new System.Windows.Forms.ComboBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.cmbNumSerieSelos = new System.Windows.Forms.ComboBox();
            this.label56 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.btnAdicionarEquipamento = new System.Windows.Forms.Button();
            this.bdsEquipamentoSelos = new System.Windows.Forms.BindingSource(this.components);
            this.bdsINMETROSelos = new System.Windows.Forms.BindingSource(this.components);
            this.osInstrumentoServicoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lblSituacao2 = new System.Windows.Forms.Label();
            this.lblCabSituacao = new System.Windows.Forms.Label();
            this.tabPagamento = new System.Windows.Forms.TabPage();
            this.grdPagamentos = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vALORDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.osFormasPagamentoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.osParContasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label60 = new System.Windows.Forms.Label();
            this.txtValorPago = new System.Windows.Forms.TextBox();
            this.btnAdicionarFormaPag = new System.Windows.Forms.Button();
            this.cmbCodigoForma = new System.Windows.Forms.ComboBox();
            this.label57 = new System.Windows.Forms.Label();
            this.osReciboBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tabLacres = new System.Windows.Forms.TabPage();
            this.tbLacreAnterior = new System.Windows.Forms.TextBox();
            this.tbMarcaInicialLacre = new System.Windows.Forms.TextBox();
            this.lbl_LacreAnterior = new System.Windows.Forms.Label();
            this.lblMarcaInicialLacre = new System.Windows.Forms.Label();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lacre_anterior = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.cmbNumInmetroLacres = new System.Windows.Forms.ComboBox();
            this.bdsINMETROLacresX = new System.Windows.Forms.BindingSource(this.components);
            this.label67 = new System.Windows.Forms.Label();
            this.cmbCodigoINMETROLacres = new System.Windows.Forms.ComboBox();
            this.cmbCodigoEquipamentoLacres = new System.Windows.Forms.ComboBox();
            this.bdsEquipamentoLacreX = new System.Windows.Forms.BindingSource(this.components);
            this.cmbCodigoLacres = new System.Windows.Forms.ComboBox();
            this.bdsLacre = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.cmbNumeroEtiquetaLacres = new System.Windows.Forms.ComboBox();
            this.label54 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.cmbNumSerieLacres = new System.Windows.Forms.ComboBox();
            this.label66 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.btnInserirLacres = new System.Windows.Forms.Button();
            this.bdsINMETROLacres = new System.Windows.Forms.BindingSource(this.components);
            this.bdsEquipamentoLacre = new System.Windows.Forms.BindingSource(this.components);
            this.txtTotalGeralPecas = new System.Windows.Forms.TextBox();
            this.txtTotalServicosOS = new System.Windows.Forms.TextBox();
            this.txtTotalFinalOS = new System.Windows.Forms.TextBox();
            this.txtTotalPago = new System.Windows.Forms.TextBox();
            this.txtValorAPagar = new System.Windows.Forms.TextBox();
            this.bdsFiltroCliente = new System.Windows.Forms.BindingSource(this.components);
            this.bdsFiltroUsuario = new System.Windows.Forms.BindingSource(this.components);
            this.pnlConsultar = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label70 = new System.Windows.Forms.Label();
            this.lblTotalOS = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkPostoOS = new System.Windows.Forms.CheckBox();
            this.labelPostoOS = new System.Windows.Forms.Label();
            this.cmbPostoOS = new System.Windows.Forms.ComboBox();
            this.label50 = new System.Windows.Forms.Label();
            this.lblFiltroPeriodoDe = new System.Windows.Forms.Label();
            this.dtp_ate = new System.Windows.Forms.DateTimePicker();
            this.dtp_de = new System.Windows.Forms.DateTimePicker();
            this.chkFiltrarPeriodo = new System.Windows.Forms.CheckBox();
            this.cmbFiltroClienteCodigo = new System.Windows.Forms.ComboBox();
            this.cmbFiltroUsuarioCodigo = new System.Windows.Forms.ComboBox();
            this.bdsFiltroXOSFuncionario = new System.Windows.Forms.BindingSource(this.components);
            this.chkFiltrarCliente = new System.Windows.Forms.CheckBox();
            this.chkFiltrarUsuario = new System.Windows.Forms.CheckBox();
            this.chkFiltrarSituacao = new System.Windows.Forms.CheckBox();
            this.label65 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.cmbFiltroSituacao = new System.Windows.Forms.ComboBox();
            this.cmbFiltroClienteNome = new System.Windows.Forms.ComboBox();
            this.cmbFiltroUsuarioNome = new System.Windows.Forms.ComboBox();
            this.osOrdemServicoDataGridView = new System.Windows.Forms.DataGridView();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IDPOSTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDUSUARIODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Usuario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDCLIENTEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dTCADASTRODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dTFECHAMENTODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.oBSDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.comboBox8 = new System.Windows.Forms.ComboBox();
            this.bdsViewOS = new System.Windows.Forms.BindingSource(this.components);
            this.osOrdemServicoTableAdapter = new osExpress.OsExpressDataSetTableAdapters.OsOrdemServicoTableAdapter();
            this.osItemServicoTableAdapter = new osExpress.OsExpressDataSetTableAdapters.OsItemServicoTableAdapter();
            this.osSituacoesOSTableAdapter = new osExpress.OsExpressDataSetTableAdapters.OsSituacoesOSTableAdapter();
            this.a30CLIENTESTableAdapter = new osExpress.OsExpressDataSetTableAdapters.A30CLIENTESTableAdapter();
            this.osParSituacaoTableAdapter = new osExpress.OsExpressDataSetTableAdapters.OsParSituacaoTableAdapter();
            this.a30POSTOSTableAdapter = new osExpress.OsExpressDataSetTableAdapters.A30POSTOSTableAdapter();
            this.a30PRODUTOTableAdapter = new osExpress.OsExpressDataSetTableAdapters.A30PRODUTOTableAdapter();
            this.tableAdapterManager = new osExpress.OsExpressDataSetTableAdapters.TableAdapterManager();
            this.osFormasPagamentoTableAdapter = new osExpress.OsExpressDataSetTableAdapters.OsFormasPagamentoTableAdapter();
            this.osInstrumentoServicoOsClienteTableAdapter = new osExpress.OsExpressDataSetTableAdapters.OsInstrumentoServicoOsClienteTableAdapter();
            this.osInstrumentoServicoTableAdapter = new osExpress.OsExpressDataSetTableAdapters.OsInstrumentoServicoTableAdapter();
            this.osReciboTableAdapter = new osExpress.OsExpressDataSetTableAdapters.OsReciboTableAdapter();
            this.osEtiquetaTableAdapter = new osExpress.OsExpressDataSetTableAdapters.OsEtiquetaTableAdapter();
            this.osIbametroTableAdapter = new osExpress.OsExpressDataSetTableAdapters.OsIbametroTableAdapter();
            this.osParContasTableAdapter = new osExpress.OsExpressDataSetTableAdapters.OsParContasTableAdapter();
            this.osUsuarioTableAdapter = new osExpress.OsExpressDataSetTableAdapters.OsUsuarioTableAdapter();
            this.osOrdemServicoTableAdapterView = new osExpress.OsExpressDataSetTableAdapters.OsOrdemServicoTableAdapter();
            this.lblCabFatura = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.lblNovoCodOS = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblCodigoOS = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.osIbametroInmetroTableAdapter = new osExpress.OsExpressDataSetTableAdapters.OsIbametroInmetroTableAdapter();
            this.osParametrosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.osParametrosTableAdapter = new osExpress.OsExpressDataSetTableAdapters.OsParametrosTableAdapter();
            this.lblCidade = new System.Windows.Forms.Label();
            this.lblUF = new System.Windows.Forms.Label();
            this.lblCidade2 = new System.Windows.Forms.Label();
            this.lblUF2 = new System.Windows.Forms.Label();
            this.cmbEmpresa = new System.Windows.Forms.ComboBox();
            this.lblEmpresa = new System.Windows.Forms.Label();
            this.chkSolicitante = new System.Windows.Forms.CheckBox();
            this.cmbSolicitante = new System.Windows.Forms.ComboBox();
            this.osIbametroOsClienteTableAdapter = new osExpress.OsExpressDataSetTableAdapters.OsIbametroOsClienteTableAdapter();
            this.osInstrumentoServicoBindingSourceX = new System.Windows.Forms.BindingSource(this.components);
            this.osIbametroInmetroOsClienteTableAdapter = new osExpress.OsExpressDataSetTableAdapters.OsIbametroInmetroOsClienteTableAdapter();
            this.xOsFuncionarioTableAdapter = new osExpress.OsExpressDataSetTableAdapters.XOsFuncionarioTableAdapter();
            this.fKOsInstrumentoServicoOsEtiquetaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            iDLabel = new System.Windows.Forms.Label();
            iDUSUARIOLabel = new System.Windows.Forms.Label();
            iDCLIENTELabel = new System.Windows.Forms.Label();
            dT_CADASTROLabel = new System.Windows.Forms.Label();
            dT_FECHAMENTOLabel = new System.Windows.Forms.Label();
            oBSLabel = new System.Windows.Forms.Label();
            label9 = new System.Windows.Forms.Label();
            label48 = new System.Windows.Forms.Label();
            dATA_INICIALLabel = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dtsPrincipal)).BeginInit();
            this.grpPrincipal.SuspendLayout();
            this.tabPrincipal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsSelectEquipamento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator)).BeginInit();
            this.bindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.osOrdemServicoBindingSource)).BeginInit();
            this.tabCabecalho.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsClientes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OsXOSFuncionarioBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.osUsuarioBindingSource)).BeginInit();
            this.tabSolicitacao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsSolicitacoes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdSolicitacao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtsSelectSituacao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.osSituacoesOSBindingSource)).BeginInit();
            this.tabEncontrada.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdEncontrada)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsEncontrada)).BeginInit();
            this.tabRealizada.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsRealizada)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdRealizada)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.a30PRODUTOBindingSource)).BeginInit();
            this.tabFinal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grsFinal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsFinal)).BeginInit();
            this.tabPecas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numQtdeProduto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdPecas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.osItemServicoBindingSource)).BeginInit();
            this.tabServicos.SuspendLayout();
            this.grpDespesas.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.tabEquipamentos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsSelectEquipamentoX)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsINMETROSelosX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsEquipamentoSelosX)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsSelos)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsEquipamentoSelos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsINMETROSelos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.osInstrumentoServicoBindingSource)).BeginInit();
            this.tabPagamento.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdPagamentos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.osFormasPagamentoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.osParContasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.osReciboBindingSource)).BeginInit();
            this.tabLacres.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsINMETROLacresX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsEquipamentoLacreX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsLacre)).BeginInit();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsINMETROLacres)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsEquipamentoLacre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsFiltroCliente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsFiltroUsuario)).BeginInit();
            this.pnlConsultar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsFiltroXOSFuncionario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.osOrdemServicoDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsViewOS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.osParametrosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.osInstrumentoServicoBindingSourceX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKOsInstrumentoServicoOsEtiquetaBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // grpPrincipal
            // 
            this.grpPrincipal.Controls.Add(this.lblUF2);
            this.grpPrincipal.Controls.Add(this.lblCidade2);
            this.grpPrincipal.Controls.Add(this.lblUF);
            this.grpPrincipal.Controls.Add(this.lblCidade);
            this.grpPrincipal.Controls.Add(this.label71);
            this.grpPrincipal.Controls.Add(this.label14);
            this.grpPrincipal.Controls.Add(this.label13);
            this.grpPrincipal.Controls.Add(this.label7);
            this.grpPrincipal.Controls.Add(this.label6);
            this.grpPrincipal.Controls.Add(this.txtTotalGeralPecas);
            this.grpPrincipal.Controls.Add(this.txtTotalServicosOS);
            this.grpPrincipal.Controls.Add(this.txtTotalFinalOS);
            this.grpPrincipal.Controls.Add(this.txtTotalPago);
            this.grpPrincipal.Controls.Add(this.txtValorAPagar);
            this.grpPrincipal.Controls.Add(this.lblNovoCodOS);
            this.grpPrincipal.Controls.Add(this.lblCabFatura);
            this.grpPrincipal.Controls.Add(this.label72);
            this.grpPrincipal.Controls.Add(this.lblCabSituacao);
            this.grpPrincipal.Controls.Add(this.lblSituacao2);
            this.grpPrincipal.Controls.Add(this.lblCabObservacao);
            this.grpPrincipal.Controls.Add(this.lblCabTelefone);
            this.grpPrincipal.Controls.Add(this.label11);
            this.grpPrincipal.Controls.Add(this.label10);
            this.grpPrincipal.Controls.Add(this.lblCabIE);
            this.grpPrincipal.Controls.Add(this.lblCabNCPJ);
            this.grpPrincipal.Controls.Add(this.lblCabCliente);
            this.grpPrincipal.Controls.Add(this.label4);
            this.grpPrincipal.Controls.Add(this.label3);
            this.grpPrincipal.Controls.Add(this.checkBox2);
            this.grpPrincipal.Controls.Add(this.checkBox1);
            this.grpPrincipal.Controls.Add(this.label2);
            this.grpPrincipal.Controls.Add(this.label1);
            this.grpPrincipal.Location = new System.Drawing.Point(12, 137);
            this.grpPrincipal.Margin = new System.Windows.Forms.Padding(4);
            this.grpPrincipal.Padding = new System.Windows.Forms.Padding(4);
            this.grpPrincipal.Size = new System.Drawing.Size(820, 156);
            this.grpPrincipal.Text = " Principal ";
            // 
            // tabPrincipal
            // 
            this.tabPrincipal.Controls.Add(this.tabCabecalho);
            this.tabPrincipal.Controls.Add(this.tabSolicitacao);
            this.tabPrincipal.Controls.Add(this.tabEncontrada);
            this.tabPrincipal.Controls.Add(this.tabRealizada);
            this.tabPrincipal.Controls.Add(this.tabFinal);
            this.tabPrincipal.Controls.Add(this.tabEquipamentos);
            this.tabPrincipal.Controls.Add(this.tabLacres);
            this.tabPrincipal.Controls.Add(this.tabPecas);
            this.tabPrincipal.Controls.Add(this.tabServicos);
            this.tabPrincipal.Controls.Add(this.tabPagamento);
            this.tabPrincipal.Location = new System.Drawing.Point(15, 293);
            this.tabPrincipal.Margin = new System.Windows.Forms.Padding(4);
            this.tabPrincipal.Size = new System.Drawing.Size(820, 378);
            this.tabPrincipal.TabIndex = 0;
            this.tabPrincipal.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tabPrincipal_Selecting);
            this.tabPrincipal.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabPrincipal_Selected);
            // 
            // iDLabel
            // 
            iDLabel.AutoSize = true;
            iDLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            iDLabel.ForeColor = System.Drawing.Color.Red;
            iDLabel.Location = new System.Drawing.Point(39, 14);
            iDLabel.Name = "iDLabel";
            iDLabel.Size = new System.Drawing.Size(89, 13);
            iDLabel.TabIndex = 0;
            iDLabel.Text = "Código da OS:";
            // 
            // iDUSUARIOLabel
            // 
            iDUSUARIOLabel.AutoSize = true;
            iDUSUARIOLabel.Location = new System.Drawing.Point(82, 40);
            iDUSUARIOLabel.Name = "iDUSUARIOLabel";
            iDUSUARIOLabel.Size = new System.Drawing.Size(46, 13);
            iDUSUARIOLabel.TabIndex = 2;
            iDUSUARIOLabel.Text = "Usuário:";
            // 
            // iDCLIENTELabel
            // 
            iDCLIENTELabel.AutoSize = true;
            iDCLIENTELabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            iDCLIENTELabel.Location = new System.Drawing.Point(35, 66);
            iDCLIENTELabel.Name = "iDCLIENTELabel";
            iDCLIENTELabel.Size = new System.Drawing.Size(93, 13);
            iDCLIENTELabel.TabIndex = 4;
            iDCLIENTELabel.Text = "Código do Cliente:";
            // 
            // dT_CADASTROLabel
            // 
            dT_CADASTROLabel.AutoSize = true;
            dT_CADASTROLabel.Location = new System.Drawing.Point(35, 328);
            dT_CADASTROLabel.Name = "dT_CADASTROLabel";
            dT_CADASTROLabel.Size = new System.Drawing.Size(93, 13);
            dT_CADASTROLabel.TabIndex = 10;
            dT_CADASTROLabel.Text = "Data de Cadastro:";
            // 
            // dT_FECHAMENTOLabel
            // 
            dT_FECHAMENTOLabel.AutoSize = true;
            dT_FECHAMENTOLabel.Location = new System.Drawing.Point(409, 327);
            dT_FECHAMENTOLabel.Name = "dT_FECHAMENTOLabel";
            dT_FECHAMENTOLabel.Size = new System.Drawing.Size(110, 13);
            dT_FECHAMENTOLabel.TabIndex = 12;
            dT_FECHAMENTOLabel.Text = "Data de Fechamento:";
            // 
            // oBSLabel
            // 
            oBSLabel.AutoSize = true;
            oBSLabel.Location = new System.Drawing.Point(4, 143);
            oBSLabel.Name = "oBSLabel";
            oBSLabel.Size = new System.Drawing.Size(125, 13);
            oBSLabel.TabIndex = 14;
            oBSLabel.Text = "Observações  p/ Cliente:";
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label9.Location = new System.Drawing.Point(236, 66);
            label9.Name = "label9";
            label9.Size = new System.Drawing.Size(88, 13);
            label9.TabIndex = 24;
            label9.Text = "Nome do Cliente:";
            // 
            // label48
            // 
            label48.AutoSize = true;
            label48.Location = new System.Drawing.Point(20, 238);
            label48.Name = "label48";
            label48.Size = new System.Drawing.Size(109, 13);
            label48.TabIndex = 28;
            label48.Text = "Observações Interna:";
            // 
            // dATA_INICIALLabel
            // 
            dATA_INICIALLabel.AutoSize = true;
            dATA_INICIALLabel.Location = new System.Drawing.Point(9, 64);
            dATA_INICIALLabel.Name = "dATA_INICIALLabel";
            dATA_INICIALLabel.Size = new System.Drawing.Size(153, 13);
            dATA_INICIALLabel.TabIndex = 31;
            dATA_INICIALLabel.Text = "Data da Realização da Tarefa:";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label5.Location = new System.Drawing.Point(53, 93);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(76, 13);
            label5.TabIndex = 35;
            label5.Text = "Email a enviar:";
            // 
            // bdsSelectEquipamento
            // 
            this.bdsSelectEquipamento.AllowNew = false;
            this.bdsSelectEquipamento.DataMember = "OsInstrumentoServico";
            this.bdsSelectEquipamento.DataSource = this.dtsPrincipal;
            // 
            // bindingNavigator
            // 
            this.bindingNavigator.AddNewItem = this.tsbInserir;
            this.bindingNavigator.BindingSource = this.osOrdemServicoBindingSource;
            this.bindingNavigator.CountItem = this.toolStripLabel1;
            this.bindingNavigator.CountItemFormat = "de {0}";
            this.bindingNavigator.DeleteItem = null;
            this.bindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bindingNavigator.ImageScalingSize = new System.Drawing.Size(48, 48);
            this.bindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton3,
            this.tsbInserir,
            this.tsbCancelar,
            this.tsbSalvar,
            this.toolStripSeparator4,
            this.toolStripButton1,
            this.toolStripButton5,
            this.toolStripSeparator1,
            this.toolStripTextBox1,
            this.toolStripLabel1,
            this.toolStripSeparator2,
            this.toolStripButton6,
            this.toolStripButton7,
            this.toolStripSeparator3,
            this.tsbDescartar,
            this.tsbFecharOS,
            this.toolStripSeparator5,
            this.tsbImprimirRecibo,
            this.tsbImprimirOS,
            this.tsbGerarCaixa});
            this.bindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator.MoveFirstItem = this.toolStripButton1;
            this.bindingNavigator.MoveLastItem = this.toolStripButton7;
            this.bindingNavigator.MoveNextItem = this.toolStripButton6;
            this.bindingNavigator.MovePreviousItem = this.toolStripButton5;
            this.bindingNavigator.Name = "bindingNavigator";
            this.bindingNavigator.PositionItem = this.toolStripTextBox1;
            this.bindingNavigator.Size = new System.Drawing.Size(855, 70);
            this.bindingNavigator.TabIndex = 9;
            this.bindingNavigator.Text = "bindingNavigator1";
            // 
            // tsbInserir
            // 
            this.tsbInserir.Image = global::osExpress.Properties.Resources.insert;
            this.tsbInserir.Name = "tsbInserir";
            this.tsbInserir.RightToLeftAutoMirrorImage = true;
            this.tsbInserir.Size = new System.Drawing.Size(52, 67);
            this.tsbInserir.Text = "&Inserir";
            this.tsbInserir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbInserir.Click += new System.EventHandler(this.tsbInserir_Click);
            // 
            // osOrdemServicoBindingSource
            // 
            this.osOrdemServicoBindingSource.DataMember = "OsOrdemServico";
            this.osOrdemServicoBindingSource.DataSource = this.dtsPrincipal;
            this.osOrdemServicoBindingSource.Sort = "ID ASC";
            this.osOrdemServicoBindingSource.CurrentChanged += new System.EventHandler(this.osOrdemServicoBindingSource_CurrentChanged);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(37, 67);
            this.toolStripLabel1.Text = "de {0}";
            this.toolStripLabel1.ToolTipText = "Total number of items";
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.Image = global::osExpress.Properties.Resources.search;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(62, 67);
            this.toolStripButton3.Text = "Consultar";
            this.toolStripButton3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click_1);
            // 
            // tsbCancelar
            // 
            this.tsbCancelar.Image = global::osExpress.Properties.Resources.clean;
            this.tsbCancelar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbCancelar.Name = "tsbCancelar";
            this.tsbCancelar.Size = new System.Drawing.Size(57, 67);
            this.tsbCancelar.Text = "Cancelar";
            this.tsbCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbCancelar.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // tsbSalvar
            // 
            this.tsbSalvar.Image = global::osExpress.Properties.Resources.save;
            this.tsbSalvar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSalvar.Name = "tsbSalvar";
            this.tsbSalvar.Size = new System.Drawing.Size(52, 67);
            this.tsbSalvar.Text = "Grav&ar";
            this.tsbSalvar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbSalvar.Click += new System.EventHandler(this.tsbSalvar_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 70);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = global::osExpress.Properties.Resources.first;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.RightToLeftAutoMirrorImage = true;
            this.toolStripButton1.Size = new System.Drawing.Size(56, 67);
            this.toolStripButton1.Text = "&Primeiro";
            this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.Image = global::osExpress.Properties.Resources.previous;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.RightToLeftAutoMirrorImage = true;
            this.toolStripButton5.Size = new System.Drawing.Size(54, 67);
            this.toolStripButton5.Text = "Anteri&or";
            this.toolStripButton5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton5.Click += new System.EventHandler(this.toolStripButton5_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 70);
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.AccessibleName = "Position";
            this.toolStripTextBox1.AutoSize = false;
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(50, 23);
            this.toolStripTextBox1.Text = "0";
            this.toolStripTextBox1.ToolTipText = "Current position";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 70);
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.Image = global::osExpress.Properties.Resources.next;
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.RightToLeftAutoMirrorImage = true;
            this.toolStripButton6.Size = new System.Drawing.Size(56, 67);
            this.toolStripButton6.Text = "Próxi&mo";
            this.toolStripButton6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton6.Click += new System.EventHandler(this.toolStripButton6_Click);
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.Image = global::osExpress.Properties.Resources.last;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.RightToLeftAutoMirrorImage = true;
            this.toolStripButton7.Size = new System.Drawing.Size(52, 67);
            this.toolStripButton7.Text = "&Último";
            this.toolStripButton7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton7.Click += new System.EventHandler(this.toolStripButton7_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 70);
            // 
            // tsbDescartar
            // 
            this.tsbDescartar.Image = global::osExpress.Properties.Resources.remove;
            this.tsbDescartar.Name = "tsbDescartar";
            this.tsbDescartar.RightToLeftAutoMirrorImage = true;
            this.tsbDescartar.Size = new System.Drawing.Size(60, 67);
            this.tsbDescartar.Text = "&Descartar";
            this.tsbDescartar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbDescartar.Click += new System.EventHandler(this.tsbExcluir_Click);
            // 
            // tsbFecharOS
            // 
            this.tsbFecharOS.Image = global::osExpress.Properties.Resources.key;
            this.tsbFecharOS.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbFecharOS.Name = "tsbFecharOS";
            this.tsbFecharOS.Size = new System.Drawing.Size(64, 67);
            this.tsbFecharOS.Text = "&Fechar OS";
            this.tsbFecharOS.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbFecharOS.Click += new System.EventHandler(this.tsbFecharOS_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 70);
            // 
            // tsbImprimirRecibo
            // 
            this.tsbImprimirRecibo.Image = global::osExpress.Properties.Resources.sale;
            this.tsbImprimirRecibo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbImprimirRecibo.Name = "tsbImprimirRecibo";
            this.tsbImprimirRecibo.Size = new System.Drawing.Size(78, 67);
            this.tsbImprimirRecibo.Text = "Gerar Recibo";
            this.tsbImprimirRecibo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbImprimirRecibo.Click += new System.EventHandler(this.tsbImprimirRecibo_Click);
            // 
            // tsbImprimirOS
            // 
            this.tsbImprimirOS.Image = global::osExpress.Properties.Resources.printer;
            this.tsbImprimirOS.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbImprimirOS.Name = "tsbImprimirOS";
            this.tsbImprimirOS.Size = new System.Drawing.Size(75, 67);
            this.tsbImprimirOS.Text = "Imprimir OS";
            this.tsbImprimirOS.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbImprimirOS.Click += new System.EventHandler(this.tsbImprimirOS_Click);
            // 
            // tsbGerarCaixa
            // 
            this.tsbGerarCaixa.Enabled = false;
            this.tsbGerarCaixa.Image = global::osExpress.Properties.Resources.creditcard;
            this.tsbGerarCaixa.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbGerarCaixa.Name = "tsbGerarCaixa";
            this.tsbGerarCaixa.Size = new System.Drawing.Size(71, 67);
            this.tsbGerarCaixa.Text = "Gerar Caixa";
            this.tsbGerarCaixa.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbGerarCaixa.Visible = false;
            this.tsbGerarCaixa.Click += new System.EventHandler(this.tsbGerarCaixa_Click);
            // 
            // tabCabecalho
            // 
            this.tabCabecalho.AutoScroll = true;
            this.tabCabecalho.Controls.Add(this.lblFiltroLike);
            this.tabCabecalho.Controls.Add(this.tbFiltroLike);
            this.tabCabecalho.Controls.Add(this.tbCodCliente);
            this.tabCabecalho.Controls.Add(label5);
            this.tabCabecalho.Controls.Add(this.txtEmail);
            this.tabCabecalho.Controls.Add(this.label61);
            this.tabCabecalho.Controls.Add(this.label59);
            this.tabCabecalho.Controls.Add(this.txtAssinaturaCliente);
            this.tabCabecalho.Controls.Add(this.cmbNomeUsuario);
            this.tabCabecalho.Controls.Add(this.cmbCodigoUsuario);
            this.tabCabecalho.Controls.Add(label48);
            this.tabCabecalho.Controls.Add(this.textBox1);
            this.tabCabecalho.Controls.Add(this.cmbNomeCliente);
            this.tabCabecalho.Controls.Add(label9);
            this.tabCabecalho.Controls.Add(this.chkSuporte);
            this.tabCabecalho.Controls.Add(this.chkInstalacao);
            this.tabCabecalho.Controls.Add(this.cmbCodCliente);
            this.tabCabecalho.Controls.Add(iDLabel);
            this.tabCabecalho.Controls.Add(this.txtCodOrdemServico);
            this.tabCabecalho.Controls.Add(iDUSUARIOLabel);
            this.tabCabecalho.Controls.Add(iDCLIENTELabel);
            this.tabCabecalho.Controls.Add(dT_CADASTROLabel);
            this.tabCabecalho.Controls.Add(this.dtpDataCadastro);
            this.tabCabecalho.Controls.Add(dT_FECHAMENTOLabel);
            this.tabCabecalho.Controls.Add(this.dtpDataFechamento);
            this.tabCabecalho.Controls.Add(oBSLabel);
            this.tabCabecalho.Controls.Add(this.txtObservacao);
            this.tabCabecalho.Location = new System.Drawing.Point(4, 22);
            this.tabCabecalho.Name = "tabCabecalho";
            this.tabCabecalho.Padding = new System.Windows.Forms.Padding(3);
            this.tabCabecalho.Size = new System.Drawing.Size(812, 352);
            this.tabCabecalho.TabIndex = 2;
            this.tabCabecalho.Text = "Cabeçalho";
            this.tabCabecalho.UseVisualStyleBackColor = true;
            // 
            // lblFiltroLike
            // 
            this.lblFiltroLike.AutoSize = true;
            this.lblFiltroLike.Location = new System.Drawing.Point(438, 18);
            this.lblFiltroLike.Name = "lblFiltroLike";
            this.lblFiltroLike.Size = new System.Drawing.Size(113, 13);
            this.lblFiltroLike.TabIndex = 39;
            this.lblFiltroLike.Text = "Filtrar Nome do Cliente";
            // 
            // tbFiltroLike
            // 
            this.tbFiltroLike.Location = new System.Drawing.Point(438, 37);
            this.tbFiltroLike.Name = "tbFiltroLike";
            this.tbFiltroLike.Size = new System.Drawing.Size(239, 20);
            this.tbFiltroLike.TabIndex = 38;
            this.tbFiltroLike.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbFiltroLike_KeyPress);
            // 
            // tbCodCliente
            // 
            this.tbCodCliente.AcceptsTab = true;
            this.tbCodCliente.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bdsClientes, "Cliente", true));
            this.tbCodCliente.Enabled = false;
            this.tbCodCliente.Location = new System.Drawing.Point(34, 203);
            this.tbCodCliente.Name = "tbCodCliente";
            this.tbCodCliente.Size = new System.Drawing.Size(100, 20);
            this.tbCodCliente.TabIndex = 3;
            this.tbCodCliente.Visible = false;
            this.tbCodCliente.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbCodCliente_KeyDown);
            this.tbCodCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbCodCliente_KeyPress);
            // 
            // bdsClientes
            // 
            this.bdsClientes.DataMember = "A30CLIENTES";
            this.bdsClientes.DataSource = this.dtsPrincipal;
            this.bdsClientes.Filter = "INATIVO = \'0\' AND TIPO_PESSOA =  \'J\'";
            this.bdsClientes.Sort = "NOME";
            // 
            // txtEmail
            // 
            this.txtEmail.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "EMAIL_ENVIADO", true));
            this.txtEmail.Location = new System.Drawing.Point(134, 90);
            this.txtEmail.MaxLength = 50;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(314, 20);
            this.txtEmail.TabIndex = 5;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.ForeColor = System.Drawing.Color.Red;
            this.label61.Location = new System.Drawing.Point(454, 120);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(223, 13);
            this.label61.TabIndex = 33;
            this.label61.Text = "Se esse campo for preenchido vai sair na OS.";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(32, 120);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(97, 13);
            this.label59.TabIndex = 32;
            this.label59.Text = "Cliente que Assina:";
            // 
            // txtAssinaturaCliente
            // 
            this.txtAssinaturaCliente.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "ASSINATURA", true));
            this.txtAssinaturaCliente.Location = new System.Drawing.Point(135, 117);
            this.txtAssinaturaCliente.MaxLength = 100;
            this.txtAssinaturaCliente.Name = "txtAssinaturaCliente";
            this.txtAssinaturaCliente.Size = new System.Drawing.Size(313, 20);
            this.txtAssinaturaCliente.TabIndex = 6;
            // 
            // cmbNomeUsuario
            // 
            this.cmbNomeUsuario.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbNomeUsuario.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbNomeUsuario.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osOrdemServicoBindingSource, "IDUSUARIO", true));
            this.cmbNomeUsuario.DataSource = this.OsXOSFuncionarioBindingSource;
            this.cmbNomeUsuario.DisplayMember = "uNome";
            this.cmbNomeUsuario.FormattingEnabled = true;
            this.cmbNomeUsuario.Location = new System.Drawing.Point(190, 37);
            this.cmbNomeUsuario.Name = "cmbNomeUsuario";
            this.cmbNomeUsuario.Size = new System.Drawing.Size(228, 21);
            this.cmbNomeUsuario.TabIndex = 2;
            this.cmbNomeUsuario.ValueMember = "idusuario";
            // 
            // OsXOSFuncionarioBindingSource
            // 
            this.OsXOSFuncionarioBindingSource.DataMember = "XOsFuncionario";
            this.OsXOSFuncionarioBindingSource.DataSource = this.dtsPrincipal;
            this.OsXOSFuncionarioBindingSource.Filter = "isusuario=\'S\'";
            // 
            // cmbCodigoUsuario
            // 
            this.cmbCodigoUsuario.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCodigoUsuario.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCodigoUsuario.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osOrdemServicoBindingSource, "IDUSUARIO", true));
            this.cmbCodigoUsuario.DataSource = this.OsXOSFuncionarioBindingSource;
            this.cmbCodigoUsuario.DisplayMember = "idusuario";
            this.cmbCodigoUsuario.FormattingEnabled = true;
            this.cmbCodigoUsuario.Location = new System.Drawing.Point(134, 37);
            this.cmbCodigoUsuario.Name = "cmbCodigoUsuario";
            this.cmbCodigoUsuario.Size = new System.Drawing.Size(50, 21);
            this.cmbCodigoUsuario.TabIndex = 1;
            this.cmbCodigoUsuario.ValueMember = "idusuario";
            // 
            // textBox1
            // 
            this.textBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "OBS_INTERNA", true));
            this.textBox1.Location = new System.Drawing.Point(135, 238);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox1.Size = new System.Drawing.Size(672, 79);
            this.textBox1.TabIndex = 8;
            // 
            // cmbNomeCliente
            // 
            this.cmbNomeCliente.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbNomeCliente.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbNomeCliente.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osOrdemServicoBindingSource, "IDCLIENTE", true));
            this.cmbNomeCliente.DataSource = this.bdsClientes;
            this.cmbNomeCliente.DisplayMember = "Nome";
            this.cmbNomeCliente.FormattingEnabled = true;
            this.cmbNomeCliente.Location = new System.Drawing.Point(330, 63);
            this.cmbNomeCliente.Name = "cmbNomeCliente";
            this.cmbNomeCliente.Size = new System.Drawing.Size(476, 21);
            this.cmbNomeCliente.TabIndex = 4;
            this.cmbNomeCliente.ValueMember = "ID";
            this.cmbNomeCliente.SelectionChangeCommitted += new System.EventHandler(this.cmbNomeCliente_SelectionChangeCommitted);
            this.cmbNomeCliente.Click += new System.EventHandler(this.cmbNomeCliente_Click);
            // 
            // chkSuporte
            // 
            this.chkSuporte.AutoSize = true;
            this.chkSuporte.Checked = true;
            this.chkSuporte.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSuporte.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.osOrdemServicoBindingSource, "SUPORTE_TECNICO", true));
            this.chkSuporte.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.osOrdemServicoBindingSource, "SUPORTE_TECNICO", true));
            this.chkSuporte.Enabled = false;
            this.chkSuporte.Location = new System.Drawing.Point(694, 37);
            this.chkSuporte.Name = "chkSuporte";
            this.chkSuporte.Size = new System.Drawing.Size(105, 17);
            this.chkSuporte.TabIndex = 7;
            this.chkSuporte.Text = "Suporte Técnico";
            this.chkSuporte.UseVisualStyleBackColor = true;
            this.chkSuporte.Visible = false;
            // 
            // chkInstalacao
            // 
            this.chkInstalacao.AutoSize = true;
            this.chkInstalacao.Checked = true;
            this.chkInstalacao.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkInstalacao.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.osOrdemServicoBindingSource, "INSTALACAO", true));
            this.chkInstalacao.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.osOrdemServicoBindingSource, "INSTALACAO", true));
            this.chkInstalacao.Enabled = false;
            this.chkInstalacao.Location = new System.Drawing.Point(694, 14);
            this.chkInstalacao.Name = "chkInstalacao";
            this.chkInstalacao.Size = new System.Drawing.Size(75, 17);
            this.chkInstalacao.TabIndex = 6;
            this.chkInstalacao.Text = "Instalação";
            this.chkInstalacao.UseVisualStyleBackColor = true;
            this.chkInstalacao.Visible = false;
            // 
            // cmbCodCliente
            // 
            this.cmbCodCliente.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCodCliente.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCodCliente.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osOrdemServicoBindingSource, "IDCLIENTE", true));
            this.cmbCodCliente.DataSource = this.bdsClientes;
            this.cmbCodCliente.DisplayMember = "Cliente";
            this.cmbCodCliente.FormattingEnabled = true;
            this.cmbCodCliente.Location = new System.Drawing.Point(134, 63);
            this.cmbCodCliente.Name = "cmbCodCliente";
            this.cmbCodCliente.Size = new System.Drawing.Size(91, 21);
            this.cmbCodCliente.TabIndex = 3;
            this.cmbCodCliente.ValueMember = "ID";
            this.cmbCodCliente.Click += new System.EventHandler(this.cmbCodCliente_Click);
            // 
            // txtCodOrdemServico
            // 
            this.txtCodOrdemServico.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "ID_FINAL", true));
            this.txtCodOrdemServico.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodOrdemServico.ForeColor = System.Drawing.Color.Red;
            this.txtCodOrdemServico.Location = new System.Drawing.Point(134, 11);
            this.txtCodOrdemServico.Name = "txtCodOrdemServico";
            this.txtCodOrdemServico.ReadOnly = true;
            this.txtCodOrdemServico.Size = new System.Drawing.Size(70, 20);
            this.txtCodOrdemServico.TabIndex = 0;
            // 
            // dtpDataCadastro
            // 
            this.dtpDataCadastro.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.osOrdemServicoBindingSource, "DT_CADASTRO", true));
            this.dtpDataCadastro.Enabled = false;
            this.dtpDataCadastro.Location = new System.Drawing.Point(134, 324);
            this.dtpDataCadastro.Name = "dtpDataCadastro";
            this.dtpDataCadastro.Size = new System.Drawing.Size(260, 20);
            this.dtpDataCadastro.TabIndex = 9;
            // 
            // dtpDataFechamento
            // 
            this.dtpDataFechamento.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.osOrdemServicoBindingSource, "DT_FECHAMENTO", true));
            this.dtpDataFechamento.Enabled = false;
            this.dtpDataFechamento.Location = new System.Drawing.Point(525, 323);
            this.dtpDataFechamento.Name = "dtpDataFechamento";
            this.dtpDataFechamento.Size = new System.Drawing.Size(260, 20);
            this.dtpDataFechamento.TabIndex = 10;
            // 
            // txtObservacao
            // 
            this.txtObservacao.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "OBS", true));
            this.txtObservacao.Location = new System.Drawing.Point(135, 143);
            this.txtObservacao.Multiline = true;
            this.txtObservacao.Name = "txtObservacao";
            this.txtObservacao.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtObservacao.Size = new System.Drawing.Size(672, 89);
            this.txtObservacao.TabIndex = 7;
            // 
            // osUsuarioBindingSource
            // 
            this.osUsuarioBindingSource.DataMember = "OsUsuario";
            this.osUsuarioBindingSource.DataSource = this.dtsPrincipal;
            this.osUsuarioBindingSource.Sort = "NOME";
            // 
            // tabSolicitacao
            // 
            this.tabSolicitacao.AutoScroll = true;
            this.tabSolicitacao.Controls.Add(this.btnInserirTipoSolicitacao);
            this.tabSolicitacao.Controls.Add(this.cmbDescSolicitacao);
            this.tabSolicitacao.Controls.Add(this.cmbCodigoSolicitacao);
            this.tabSolicitacao.Controls.Add(this.grdSolicitacao);
            this.tabSolicitacao.Controls.Add(this.label46);
            this.tabSolicitacao.Controls.Add(this.label47);
            this.tabSolicitacao.Location = new System.Drawing.Point(4, 22);
            this.tabSolicitacao.Name = "tabSolicitacao";
            this.tabSolicitacao.Padding = new System.Windows.Forms.Padding(3);
            this.tabSolicitacao.Size = new System.Drawing.Size(812, 352);
            this.tabSolicitacao.TabIndex = 3;
            this.tabSolicitacao.Text = "Tipo de Solicitação";
            this.tabSolicitacao.UseVisualStyleBackColor = true;
            // 
            // btnInserirTipoSolicitacao
            // 
            this.btnInserirTipoSolicitacao.Location = new System.Drawing.Point(319, 60);
            this.btnInserirTipoSolicitacao.Name = "btnInserirTipoSolicitacao";
            this.btnInserirTipoSolicitacao.Size = new System.Drawing.Size(75, 23);
            this.btnInserirTipoSolicitacao.TabIndex = 2;
            this.btnInserirTipoSolicitacao.Text = "Inserir";
            this.btnInserirTipoSolicitacao.UseVisualStyleBackColor = true;
            this.btnInserirTipoSolicitacao.Click += new System.EventHandler(this.btnInserirTipoSolicitacao_Click);
            // 
            // cmbDescSolicitacao
            // 
            this.cmbDescSolicitacao.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbDescSolicitacao.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbDescSolicitacao.DataSource = this.bdsSolicitacoes;
            this.cmbDescSolicitacao.DisplayMember = "NOME";
            this.cmbDescSolicitacao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDescSolicitacao.FormattingEnabled = true;
            this.cmbDescSolicitacao.Location = new System.Drawing.Point(141, 33);
            this.cmbDescSolicitacao.Name = "cmbDescSolicitacao";
            this.cmbDescSolicitacao.Size = new System.Drawing.Size(253, 21);
            this.cmbDescSolicitacao.TabIndex = 1;
            // 
            // bdsSolicitacoes
            // 
            this.bdsSolicitacoes.DataMember = "OsParSituacao";
            this.bdsSolicitacoes.DataSource = this.dtsPrincipal;
            this.bdsSolicitacoes.Filter = "TIPO_SITUACAO = \'S\'";
            this.bdsSolicitacoes.Sort = "NOME ASC";
            // 
            // cmbCodigoSolicitacao
            // 
            this.cmbCodigoSolicitacao.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCodigoSolicitacao.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCodigoSolicitacao.DataSource = this.bdsSolicitacoes;
            this.cmbCodigoSolicitacao.DisplayMember = "ID";
            this.cmbCodigoSolicitacao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCodigoSolicitacao.FormattingEnabled = true;
            this.cmbCodigoSolicitacao.Location = new System.Drawing.Point(141, 6);
            this.cmbCodigoSolicitacao.Name = "cmbCodigoSolicitacao";
            this.cmbCodigoSolicitacao.Size = new System.Drawing.Size(62, 21);
            this.cmbCodigoSolicitacao.TabIndex = 0;
            // 
            // grdSolicitacao
            // 
            this.grdSolicitacao.AllowUserToAddRows = false;
            this.grdSolicitacao.AllowUserToOrderColumns = true;
            this.grdSolicitacao.AutoGenerateColumns = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdSolicitacao.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.grdSolicitacao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdSolicitacao.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDSITUACAODataGridViewTextBoxColumn1,
            this.DESCRICAO});
            this.grdSolicitacao.DataSource = this.dtsSelectSituacao;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdSolicitacao.DefaultCellStyle = dataGridViewCellStyle2;
            this.grdSolicitacao.Location = new System.Drawing.Point(6, 89);
            this.grdSolicitacao.Name = "grdSolicitacao";
            this.grdSolicitacao.ReadOnly = true;
            this.grdSolicitacao.Size = new System.Drawing.Size(800, 257);
            this.grdSolicitacao.TabIndex = 3;
            this.grdSolicitacao.TabStop = false;
            // 
            // iDSITUACAODataGridViewTextBoxColumn1
            // 
            this.iDSITUACAODataGridViewTextBoxColumn1.DataPropertyName = "IDSITUACAO";
            this.iDSITUACAODataGridViewTextBoxColumn1.HeaderText = "Solcitação";
            this.iDSITUACAODataGridViewTextBoxColumn1.Name = "iDSITUACAODataGridViewTextBoxColumn1";
            this.iDSITUACAODataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // DESCRICAO
            // 
            this.DESCRICAO.DataPropertyName = "DESCRICAO";
            this.DESCRICAO.HeaderText = "Descrição";
            this.DESCRICAO.Name = "DESCRICAO";
            this.DESCRICAO.ReadOnly = true;
            this.DESCRICAO.Width = 500;
            // 
            // dtsSelectSituacao
            // 
            this.dtsSelectSituacao.DataMember = "OsSituacoesOS";
            this.dtsSelectSituacao.DataSource = this.dtsPrincipal;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(7, 36);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(128, 13);
            this.label46.TabIndex = 29;
            this.label46.Text = "Descrição da Solicitação:";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(22, 9);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(113, 13);
            this.label47.TabIndex = 28;
            this.label47.Text = "Código da Solicitação:";
            // 
            // osSituacoesOSBindingSource
            // 
            this.osSituacoesOSBindingSource.AllowNew = true;
            this.osSituacoesOSBindingSource.DataMember = "OsSituacoesOS";
            this.osSituacoesOSBindingSource.DataSource = this.dtsPrincipal;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Cliente.......................:";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.osOrdemServicoBindingSource, "INSTALACAO", true));
            this.checkBox1.Enabled = false;
            this.checkBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.Location = new System.Drawing.Point(361, 113);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(85, 17);
            this.checkBox1.TabIndex = 2;
            this.checkBox1.Text = "Instalação";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.Visible = false;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.osOrdemServicoBindingSource, "SUPORTE_TECNICO", true));
            this.checkBox2.Enabled = false;
            this.checkBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox2.Location = new System.Drawing.Point(361, 131);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(120, 17);
            this.checkBox2.TabIndex = 3;
            this.checkBox2.Text = "Suporte Técnico";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "CNPJ........................:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 60);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(110, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "I.E.............................:";
            // 
            // tabEncontrada
            // 
            this.tabEncontrada.Controls.Add(this.btnInserirSituacaoEncontrada);
            this.tabEncontrada.Controls.Add(this.grdEncontrada);
            this.tabEncontrada.Controls.Add(this.cmbDescSitEncontrada);
            this.tabEncontrada.Controls.Add(this.cmbCodigoSitEncontrada);
            this.tabEncontrada.Controls.Add(this.label44);
            this.tabEncontrada.Controls.Add(this.label45);
            this.tabEncontrada.Location = new System.Drawing.Point(4, 22);
            this.tabEncontrada.Name = "tabEncontrada";
            this.tabEncontrada.Padding = new System.Windows.Forms.Padding(3);
            this.tabEncontrada.Size = new System.Drawing.Size(812, 352);
            this.tabEncontrada.TabIndex = 4;
            this.tabEncontrada.Text = "Situação Encontrada";
            this.tabEncontrada.UseVisualStyleBackColor = true;
            // 
            // btnInserirSituacaoEncontrada
            // 
            this.btnInserirSituacaoEncontrada.Location = new System.Drawing.Point(367, 60);
            this.btnInserirSituacaoEncontrada.Name = "btnInserirSituacaoEncontrada";
            this.btnInserirSituacaoEncontrada.Size = new System.Drawing.Size(75, 23);
            this.btnInserirSituacaoEncontrada.TabIndex = 2;
            this.btnInserirSituacaoEncontrada.Text = "Inserir";
            this.btnInserirSituacaoEncontrada.UseVisualStyleBackColor = true;
            this.btnInserirSituacaoEncontrada.Click += new System.EventHandler(this.btnInserirSituacaoEncontrada_Click);
            // 
            // grdEncontrada
            // 
            this.grdEncontrada.AllowUserToAddRows = false;
            this.grdEncontrada.AllowUserToOrderColumns = true;
            this.grdEncontrada.AutoGenerateColumns = false;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdEncontrada.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.grdEncontrada.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdEncontrada.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn42,
            this.dataGridViewTextBoxColumn43});
            this.grdEncontrada.DataSource = this.dtsSelectSituacao;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdEncontrada.DefaultCellStyle = dataGridViewCellStyle4;
            this.grdEncontrada.Location = new System.Drawing.Point(6, 89);
            this.grdEncontrada.Name = "grdEncontrada";
            this.grdEncontrada.ReadOnly = true;
            this.grdEncontrada.Size = new System.Drawing.Size(800, 257);
            this.grdEncontrada.TabIndex = 3;
            this.grdEncontrada.TabStop = false;
            // 
            // dataGridViewTextBoxColumn42
            // 
            this.dataGridViewTextBoxColumn42.DataPropertyName = "IDSITUACAO";
            this.dataGridViewTextBoxColumn42.HeaderText = "Situação";
            this.dataGridViewTextBoxColumn42.Name = "dataGridViewTextBoxColumn42";
            this.dataGridViewTextBoxColumn42.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn43
            // 
            this.dataGridViewTextBoxColumn43.DataPropertyName = "DESCRICAO";
            this.dataGridViewTextBoxColumn43.HeaderText = "Descrição";
            this.dataGridViewTextBoxColumn43.Name = "dataGridViewTextBoxColumn43";
            this.dataGridViewTextBoxColumn43.ReadOnly = true;
            this.dataGridViewTextBoxColumn43.Width = 500;
            // 
            // cmbDescSitEncontrada
            // 
            this.cmbDescSitEncontrada.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbDescSitEncontrada.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbDescSitEncontrada.DataSource = this.bdsEncontrada;
            this.cmbDescSitEncontrada.DisplayMember = "NOME";
            this.cmbDescSitEncontrada.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDescSitEncontrada.FormattingEnabled = true;
            this.cmbDescSitEncontrada.Location = new System.Drawing.Point(189, 33);
            this.cmbDescSitEncontrada.Name = "cmbDescSitEncontrada";
            this.cmbDescSitEncontrada.Size = new System.Drawing.Size(253, 21);
            this.cmbDescSitEncontrada.TabIndex = 1;
            // 
            // bdsEncontrada
            // 
            this.bdsEncontrada.AllowNew = true;
            this.bdsEncontrada.DataMember = "OsParSituacao";
            this.bdsEncontrada.DataSource = this.dtsPrincipal;
            this.bdsEncontrada.Filter = "TIPO_SITUACAO = \'E\'";
            this.bdsEncontrada.Sort = "NOME ASC";
            // 
            // cmbCodigoSitEncontrada
            // 
            this.cmbCodigoSitEncontrada.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCodigoSitEncontrada.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCodigoSitEncontrada.DataSource = this.bdsEncontrada;
            this.cmbCodigoSitEncontrada.DisplayMember = "ID";
            this.cmbCodigoSitEncontrada.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCodigoSitEncontrada.FormattingEnabled = true;
            this.cmbCodigoSitEncontrada.Location = new System.Drawing.Point(189, 6);
            this.cmbCodigoSitEncontrada.Name = "cmbCodigoSitEncontrada";
            this.cmbCodigoSitEncontrada.Size = new System.Drawing.Size(62, 21);
            this.cmbCodigoSitEncontrada.TabIndex = 0;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(7, 36);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(176, 13);
            this.label44.TabIndex = 29;
            this.label44.Text = "Descrição da Situação Encontrada:";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(22, 9);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(161, 13);
            this.label45.TabIndex = 28;
            this.label45.Text = "Código da Situação Encontrada:";
            // 
            // tabRealizada
            // 
            this.tabRealizada.AutoScroll = true;
            this.tabRealizada.Controls.Add(dATA_INICIALLabel);
            this.tabRealizada.Controls.Add(this.dATA_INICIALDateTimePicker);
            this.tabRealizada.Controls.Add(this.btnAtividadeRealizada);
            this.tabRealizada.Controls.Add(this.cmbDescricaoRealizada);
            this.tabRealizada.Controls.Add(this.cmbCodigoRealizada);
            this.tabRealizada.Controls.Add(this.dtpHoraTotalRealizada);
            this.tabRealizada.Controls.Add(this.dtpHoraFinalRealizada);
            this.tabRealizada.Controls.Add(this.dtpHoraInicialRealizada);
            this.tabRealizada.Controls.Add(this.grdRealizada);
            this.tabRealizada.Controls.Add(this.label41);
            this.tabRealizada.Controls.Add(this.label40);
            this.tabRealizada.Controls.Add(this.label39);
            this.tabRealizada.Controls.Add(this.label36);
            this.tabRealizada.Controls.Add(this.label38);
            this.tabRealizada.Location = new System.Drawing.Point(4, 22);
            this.tabRealizada.Name = "tabRealizada";
            this.tabRealizada.Padding = new System.Windows.Forms.Padding(3);
            this.tabRealizada.Size = new System.Drawing.Size(812, 352);
            this.tabRealizada.TabIndex = 5;
            this.tabRealizada.Text = "Atividade Realizada";
            this.tabRealizada.UseVisualStyleBackColor = true;
            // 
            // dATA_INICIALDateTimePicker
            // 
            this.dATA_INICIALDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dATA_INICIALDateTimePicker.Location = new System.Drawing.Point(168, 60);
            this.dATA_INICIALDateTimePicker.Name = "dATA_INICIALDateTimePicker";
            this.dATA_INICIALDateTimePicker.Size = new System.Drawing.Size(85, 20);
            this.dATA_INICIALDateTimePicker.TabIndex = 2;
            // 
            // btnAtividadeRealizada
            // 
            this.btnAtividadeRealizada.Location = new System.Drawing.Point(178, 164);
            this.btnAtividadeRealizada.Name = "btnAtividadeRealizada";
            this.btnAtividadeRealizada.Size = new System.Drawing.Size(75, 23);
            this.btnAtividadeRealizada.TabIndex = 6;
            this.btnAtividadeRealizada.Text = "Inserir";
            this.btnAtividadeRealizada.UseVisualStyleBackColor = true;
            this.btnAtividadeRealizada.Click += new System.EventHandler(this.btnAtividadeRealizada_Click);
            // 
            // cmbDescricaoRealizada
            // 
            this.cmbDescricaoRealizada.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbDescricaoRealizada.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbDescricaoRealizada.DataSource = this.bdsRealizada;
            this.cmbDescricaoRealizada.DisplayMember = "NOME";
            this.cmbDescricaoRealizada.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDescricaoRealizada.FormattingEnabled = true;
            this.cmbDescricaoRealizada.Location = new System.Drawing.Point(168, 33);
            this.cmbDescricaoRealizada.Name = "cmbDescricaoRealizada";
            this.cmbDescricaoRealizada.Size = new System.Drawing.Size(253, 21);
            this.cmbDescricaoRealizada.TabIndex = 1;
            // 
            // bdsRealizada
            // 
            this.bdsRealizada.DataMember = "OsParSituacao";
            this.bdsRealizada.DataSource = this.dtsPrincipal;
            this.bdsRealizada.Filter = "TIPO_SITUACAO = \'R\'";
            this.bdsRealizada.Sort = "NOME ASC";
            // 
            // cmbCodigoRealizada
            // 
            this.cmbCodigoRealizada.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCodigoRealizada.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCodigoRealizada.DataSource = this.bdsRealizada;
            this.cmbCodigoRealizada.DisplayMember = "ID";
            this.cmbCodigoRealizada.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCodigoRealizada.FormattingEnabled = true;
            this.cmbCodigoRealizada.Location = new System.Drawing.Point(168, 6);
            this.cmbCodigoRealizada.Name = "cmbCodigoRealizada";
            this.cmbCodigoRealizada.Size = new System.Drawing.Size(62, 21);
            this.cmbCodigoRealizada.TabIndex = 0;
            // 
            // dtpHoraTotalRealizada
            // 
            this.dtpHoraTotalRealizada.Enabled = false;
            this.dtpHoraTotalRealizada.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpHoraTotalRealizada.Location = new System.Drawing.Point(168, 138);
            this.dtpHoraTotalRealizada.Name = "dtpHoraTotalRealizada";
            this.dtpHoraTotalRealizada.ShowUpDown = true;
            this.dtpHoraTotalRealizada.Size = new System.Drawing.Size(88, 20);
            this.dtpHoraTotalRealizada.TabIndex = 5;
            // 
            // dtpHoraFinalRealizada
            // 
            this.dtpHoraFinalRealizada.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpHoraFinalRealizada.Location = new System.Drawing.Point(168, 112);
            this.dtpHoraFinalRealizada.Name = "dtpHoraFinalRealizada";
            this.dtpHoraFinalRealizada.ShowUpDown = true;
            this.dtpHoraFinalRealizada.Size = new System.Drawing.Size(88, 20);
            this.dtpHoraFinalRealizada.TabIndex = 4;
            this.dtpHoraFinalRealizada.ValueChanged += new System.EventHandler(this.calculaHora);
            // 
            // dtpHoraInicialRealizada
            // 
            this.dtpHoraInicialRealizada.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpHoraInicialRealizada.Location = new System.Drawing.Point(168, 86);
            this.dtpHoraInicialRealizada.Name = "dtpHoraInicialRealizada";
            this.dtpHoraInicialRealizada.ShowUpDown = true;
            this.dtpHoraInicialRealizada.Size = new System.Drawing.Size(88, 20);
            this.dtpHoraInicialRealizada.TabIndex = 3;
            this.dtpHoraInicialRealizada.ValueChanged += new System.EventHandler(this.calculaHora);
            // 
            // grdRealizada
            // 
            this.grdRealizada.AllowUserToAddRows = false;
            this.grdRealizada.AutoGenerateColumns = false;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdRealizada.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.grdRealizada.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdRealizada.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDSITUACAODataGridViewTextBoxColumn,
            this.DATA_INICIAL,
            this.hORAINICIODataGridViewTextBoxColumn,
            this.hORAFIMDataGridViewTextBoxColumn,
            this.hORATOTALDataGridViewTextBoxColumn,
            this.Descricao123});
            this.grdRealizada.DataSource = this.dtsSelectSituacao;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdRealizada.DefaultCellStyle = dataGridViewCellStyle6;
            this.grdRealizada.Location = new System.Drawing.Point(6, 193);
            this.grdRealizada.Name = "grdRealizada";
            this.grdRealizada.Size = new System.Drawing.Size(800, 153);
            this.grdRealizada.TabIndex = 7;
            this.grdRealizada.TabStop = false;
            this.grdRealizada.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.grdRealizada_UserDeletingRow);
            // 
            // iDSITUACAODataGridViewTextBoxColumn
            // 
            this.iDSITUACAODataGridViewTextBoxColumn.DataPropertyName = "IDSITUACAO";
            this.iDSITUACAODataGridViewTextBoxColumn.HeaderText = "Atividade";
            this.iDSITUACAODataGridViewTextBoxColumn.Name = "iDSITUACAODataGridViewTextBoxColumn";
            // 
            // DATA_INICIAL
            // 
            this.DATA_INICIAL.DataPropertyName = "DATA_INICIAL";
            this.DATA_INICIAL.HeaderText = "Data da Realização";
            this.DATA_INICIAL.Name = "DATA_INICIAL";
            this.DATA_INICIAL.Width = 130;
            // 
            // hORAINICIODataGridViewTextBoxColumn
            // 
            this.hORAINICIODataGridViewTextBoxColumn.DataPropertyName = "HORA_INICIO";
            this.hORAINICIODataGridViewTextBoxColumn.HeaderText = "Horário Inicial";
            this.hORAINICIODataGridViewTextBoxColumn.Name = "hORAINICIODataGridViewTextBoxColumn";
            // 
            // hORAFIMDataGridViewTextBoxColumn
            // 
            this.hORAFIMDataGridViewTextBoxColumn.DataPropertyName = "HORA_FIM";
            this.hORAFIMDataGridViewTextBoxColumn.HeaderText = "Horário Final";
            this.hORAFIMDataGridViewTextBoxColumn.Name = "hORAFIMDataGridViewTextBoxColumn";
            // 
            // hORATOTALDataGridViewTextBoxColumn
            // 
            this.hORATOTALDataGridViewTextBoxColumn.DataPropertyName = "HORA_TOTAL";
            this.hORATOTALDataGridViewTextBoxColumn.HeaderText = "Horário Total";
            this.hORATOTALDataGridViewTextBoxColumn.Name = "hORATOTALDataGridViewTextBoxColumn";
            // 
            // Descricao123
            // 
            this.Descricao123.DataPropertyName = "DESCRICAO";
            this.Descricao123.HeaderText = "Descrição";
            this.Descricao123.Name = "Descricao123";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(91, 144);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(71, 13);
            this.label41.TabIndex = 22;
            this.label41.Text = "Horário Total:";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(93, 118);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(69, 13);
            this.label40.TabIndex = 21;
            this.label40.Text = "Horário Final:";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(88, 92);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(74, 13);
            this.label39.TabIndex = 20;
            this.label39.Text = "Horário Inicial:";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(42, 35);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(120, 13);
            this.label36.TabIndex = 14;
            this.label36.Text = "Descrição da Atividade:";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(57, 8);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(105, 13);
            this.label38.TabIndex = 13;
            this.label38.Text = "Código da Atividade:";
            // 
            // a30PRODUTOBindingSource
            // 
            this.a30PRODUTOBindingSource.AllowNew = false;
            this.a30PRODUTOBindingSource.DataMember = "A30PRODUTO";
            this.a30PRODUTOBindingSource.DataSource = this.dtsPrincipal;
            this.a30PRODUTOBindingSource.Filter = "TIPO <> 1 AND INATIVO = 0";
            this.a30PRODUTOBindingSource.Sort = "DESCRICAO";
            // 
            // tabFinal
            // 
            this.tabFinal.Controls.Add(this.btnInserirSituacaoFinal);
            this.tabFinal.Controls.Add(this.grsFinal);
            this.tabFinal.Controls.Add(this.cmbDescSituacaoFinal);
            this.tabFinal.Controls.Add(this.cmbCodigoSituacaoFinal);
            this.tabFinal.Controls.Add(this.label42);
            this.tabFinal.Controls.Add(this.label43);
            this.tabFinal.Location = new System.Drawing.Point(4, 22);
            this.tabFinal.Name = "tabFinal";
            this.tabFinal.Padding = new System.Windows.Forms.Padding(3);
            this.tabFinal.Size = new System.Drawing.Size(812, 352);
            this.tabFinal.TabIndex = 6;
            this.tabFinal.Text = "Situação Final";
            this.tabFinal.UseVisualStyleBackColor = true;
            // 
            // btnInserirSituacaoFinal
            // 
            this.btnInserirSituacaoFinal.Location = new System.Drawing.Point(309, 60);
            this.btnInserirSituacaoFinal.Name = "btnInserirSituacaoFinal";
            this.btnInserirSituacaoFinal.Size = new System.Drawing.Size(75, 23);
            this.btnInserirSituacaoFinal.TabIndex = 2;
            this.btnInserirSituacaoFinal.Text = "Inserir";
            this.btnInserirSituacaoFinal.UseVisualStyleBackColor = true;
            this.btnInserirSituacaoFinal.Click += new System.EventHandler(this.btnInserirSituacaoFinal_Click);
            // 
            // grsFinal
            // 
            this.grsFinal.AllowUserToAddRows = false;
            this.grsFinal.AllowUserToOrderColumns = true;
            this.grsFinal.AutoGenerateColumns = false;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grsFinal.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.grsFinal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grsFinal.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn37,
            this.dataGridViewTextBoxColumn38});
            this.grsFinal.DataSource = this.dtsSelectSituacao;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grsFinal.DefaultCellStyle = dataGridViewCellStyle8;
            this.grsFinal.Location = new System.Drawing.Point(5, 89);
            this.grsFinal.Name = "grsFinal";
            this.grsFinal.ReadOnly = true;
            this.grsFinal.Size = new System.Drawing.Size(801, 257);
            this.grsFinal.TabIndex = 3;
            this.grsFinal.TabStop = false;
            // 
            // dataGridViewTextBoxColumn37
            // 
            this.dataGridViewTextBoxColumn37.DataPropertyName = "IDSITUACAO";
            this.dataGridViewTextBoxColumn37.HeaderText = "Solcitação";
            this.dataGridViewTextBoxColumn37.Name = "dataGridViewTextBoxColumn37";
            this.dataGridViewTextBoxColumn37.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn38
            // 
            this.dataGridViewTextBoxColumn38.DataPropertyName = "DESCRICAO";
            this.dataGridViewTextBoxColumn38.HeaderText = "Descrição";
            this.dataGridViewTextBoxColumn38.Name = "dataGridViewTextBoxColumn38";
            this.dataGridViewTextBoxColumn38.ReadOnly = true;
            this.dataGridViewTextBoxColumn38.Width = 500;
            // 
            // cmbDescSituacaoFinal
            // 
            this.cmbDescSituacaoFinal.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbDescSituacaoFinal.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbDescSituacaoFinal.DataSource = this.bdsFinal;
            this.cmbDescSituacaoFinal.DisplayMember = "NOME";
            this.cmbDescSituacaoFinal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDescSituacaoFinal.FormattingEnabled = true;
            this.cmbDescSituacaoFinal.Location = new System.Drawing.Point(131, 33);
            this.cmbDescSituacaoFinal.Name = "cmbDescSituacaoFinal";
            this.cmbDescSituacaoFinal.Size = new System.Drawing.Size(253, 21);
            this.cmbDescSituacaoFinal.TabIndex = 1;
            // 
            // bdsFinal
            // 
            this.bdsFinal.DataMember = "OsParSituacao";
            this.bdsFinal.DataSource = this.dtsPrincipal;
            this.bdsFinal.Filter = "TIPO_SITUACAO = \'F\'";
            this.bdsFinal.Sort = "NOME ASC";
            // 
            // cmbCodigoSituacaoFinal
            // 
            this.cmbCodigoSituacaoFinal.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCodigoSituacaoFinal.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCodigoSituacaoFinal.DataSource = this.bdsFinal;
            this.cmbCodigoSituacaoFinal.DisplayMember = "ID";
            this.cmbCodigoSituacaoFinal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCodigoSituacaoFinal.FormattingEnabled = true;
            this.cmbCodigoSituacaoFinal.Location = new System.Drawing.Point(131, 6);
            this.cmbCodigoSituacaoFinal.Name = "cmbCodigoSituacaoFinal";
            this.cmbCodigoSituacaoFinal.Size = new System.Drawing.Size(62, 21);
            this.cmbCodigoSituacaoFinal.TabIndex = 0;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(7, 36);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(118, 13);
            this.label42.TabIndex = 29;
            this.label42.Text = "Descrição da Situação:";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(22, 9);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(103, 13);
            this.label43.TabIndex = 28;
            this.label43.Text = "Código da Situação:";
            // 
            // tabPecas
            // 
            this.tabPecas.AutoScroll = true;
            this.tabPecas.Controls.Add(this.numQtdeProduto);
            this.tabPecas.Controls.Add(this.grdPecas);
            this.tabPecas.Controls.Add(this.btnAdicionarPecas);
            this.tabPecas.Controls.Add(this.label37);
            this.tabPecas.Controls.Add(this.txtTotalProduto);
            this.tabPecas.Controls.Add(this.label35);
            this.tabPecas.Controls.Add(this.txtPrecoProduto);
            this.tabPecas.Controls.Add(this.label29);
            this.tabPecas.Controls.Add(this.label27);
            this.tabPecas.Controls.Add(this.cmbCodigoIDProduto);
            this.tabPecas.Controls.Add(this.cmbDescricaoProduto);
            this.tabPecas.Controls.Add(this.label12);
            this.tabPecas.Controls.Add(this.cmbCodigoProduto);
            this.tabPecas.Location = new System.Drawing.Point(4, 22);
            this.tabPecas.Name = "tabPecas";
            this.tabPecas.Padding = new System.Windows.Forms.Padding(3);
            this.tabPecas.Size = new System.Drawing.Size(812, 352);
            this.tabPecas.TabIndex = 7;
            this.tabPecas.Text = "Produtos e Serviços";
            this.tabPecas.UseVisualStyleBackColor = true;
            // 
            // numQtdeProduto
            // 
            this.numQtdeProduto.Location = new System.Drawing.Point(126, 65);
            this.numQtdeProduto.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numQtdeProduto.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numQtdeProduto.Name = "numQtdeProduto";
            this.numQtdeProduto.Size = new System.Drawing.Size(61, 20);
            this.numQtdeProduto.TabIndex = 2;
            this.numQtdeProduto.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numQtdeProduto.ValueChanged += new System.EventHandler(this.calculaItemPeca);
            // 
            // grdPecas
            // 
            this.grdPecas.AllowUserToAddRows = false;
            this.grdPecas.AllowUserToOrderColumns = true;
            this.grdPecas.AutoGenerateColumns = false;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdPecas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.grdPecas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdPecas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.qUANTIDADEDataGridViewTextBoxColumn,
            this.vALORDataGridViewTextBoxColumn,
            this.tOTALDataGridViewTextBoxColumn,
            this.Produto,
            this.Descrição});
            this.grdPecas.DataSource = this.osItemServicoBindingSource;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdPecas.DefaultCellStyle = dataGridViewCellStyle14;
            this.grdPecas.Location = new System.Drawing.Point(6, 144);
            this.grdPecas.Name = "grdPecas";
            this.grdPecas.ReadOnly = true;
            this.grdPecas.Size = new System.Drawing.Size(800, 202);
            this.grdPecas.TabIndex = 37;
            this.grdPecas.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.grdPecas_UserDeletedRow);
            this.grdPecas.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.grdPecas_UserDeletingRow);
            // 
            // qUANTIDADEDataGridViewTextBoxColumn
            // 
            this.qUANTIDADEDataGridViewTextBoxColumn.DataPropertyName = "QUANTIDADE";
            this.qUANTIDADEDataGridViewTextBoxColumn.HeaderText = "Quantidade";
            this.qUANTIDADEDataGridViewTextBoxColumn.Name = "qUANTIDADEDataGridViewTextBoxColumn";
            this.qUANTIDADEDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // vALORDataGridViewTextBoxColumn
            // 
            this.vALORDataGridViewTextBoxColumn.DataPropertyName = "VALOR";
            this.vALORDataGridViewTextBoxColumn.HeaderText = "Valor";
            this.vALORDataGridViewTextBoxColumn.Name = "vALORDataGridViewTextBoxColumn";
            this.vALORDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tOTALDataGridViewTextBoxColumn
            // 
            this.tOTALDataGridViewTextBoxColumn.DataPropertyName = "TOTAL";
            this.tOTALDataGridViewTextBoxColumn.HeaderText = "Total";
            this.tOTALDataGridViewTextBoxColumn.Name = "tOTALDataGridViewTextBoxColumn";
            this.tOTALDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // Produto
            // 
            this.Produto.DataPropertyName = "Produto";
            this.Produto.HeaderText = "Produto";
            this.Produto.Name = "Produto";
            this.Produto.ReadOnly = true;
            // 
            // Descrição
            // 
            this.Descrição.DataPropertyName = "DESCRICAO";
            this.Descrição.HeaderText = "Descrição";
            this.Descrição.Name = "Descrição";
            this.Descrição.ReadOnly = true;
            this.Descrição.Width = 300;
            // 
            // osItemServicoBindingSource
            // 
            this.osItemServicoBindingSource.AllowNew = true;
            this.osItemServicoBindingSource.DataMember = "OsItemServico";
            this.osItemServicoBindingSource.DataSource = this.dtsPrincipal;
            // 
            // btnAdicionarPecas
            // 
            this.btnAdicionarPecas.Location = new System.Drawing.Point(398, 115);
            this.btnAdicionarPecas.Name = "btnAdicionarPecas";
            this.btnAdicionarPecas.Size = new System.Drawing.Size(75, 23);
            this.btnAdicionarPecas.TabIndex = 5;
            this.btnAdicionarPecas.Text = "Inserir";
            this.btnAdicionarPecas.UseVisualStyleBackColor = true;
            this.btnAdicionarPecas.Click += new System.EventHandler(this.btnAdicionarPecas_Click);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Red;
            this.label37.Location = new System.Drawing.Point(70, 120);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(51, 13);
            this.label37.TabIndex = 35;
            this.label37.Text = "TOTAL:";
            // 
            // txtTotalProduto
            // 
            this.txtTotalProduto.Enabled = false;
            this.txtTotalProduto.Location = new System.Drawing.Point(126, 117);
            this.txtTotalProduto.Name = "txtTotalProduto";
            this.txtTotalProduto.ReadOnly = true;
            this.txtTotalProduto.Size = new System.Drawing.Size(85, 20);
            this.txtTotalProduto.TabIndex = 4;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(83, 94);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(38, 13);
            this.label35.TabIndex = 13;
            this.label35.Text = "Preço:";
            // 
            // txtPrecoProduto
            // 
            this.txtPrecoProduto.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.a30PRODUTOBindingSource, "Preco", true));
            this.txtPrecoProduto.Enabled = false;
            this.txtPrecoProduto.Location = new System.Drawing.Point(126, 91);
            this.txtPrecoProduto.Name = "txtPrecoProduto";
            this.txtPrecoProduto.Size = new System.Drawing.Size(85, 20);
            this.txtPrecoProduto.TabIndex = 3;
            this.txtPrecoProduto.TextChanged += new System.EventHandler(this.calculaItemPeca);
            this.txtPrecoProduto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TplPermiteSoValor);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(56, 68);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(65, 13);
            this.label29.TabIndex = 11;
            this.label29.Text = "Quantidade:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(23, 14);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(98, 13);
            this.label27.TabIndex = 10;
            this.label27.Text = "Código do Produto:";
            // 
            // cmbCodigoIDProduto
            // 
            this.cmbCodigoIDProduto.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCodigoIDProduto.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCodigoIDProduto.DataSource = this.a30PRODUTOBindingSource;
            this.cmbCodigoIDProduto.DisplayMember = "ID";
            this.cmbCodigoIDProduto.Enabled = false;
            this.cmbCodigoIDProduto.FormattingEnabled = true;
            this.cmbCodigoIDProduto.Location = new System.Drawing.Point(246, 11);
            this.cmbCodigoIDProduto.Name = "cmbCodigoIDProduto";
            this.cmbCodigoIDProduto.Size = new System.Drawing.Size(67, 21);
            this.cmbCodigoIDProduto.TabIndex = 0;
            this.cmbCodigoIDProduto.Visible = false;
            // 
            // cmbDescricaoProduto
            // 
            this.cmbDescricaoProduto.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbDescricaoProduto.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbDescricaoProduto.DataSource = this.a30PRODUTOBindingSource;
            this.cmbDescricaoProduto.DisplayMember = "Descricao";
            this.cmbDescricaoProduto.FormattingEnabled = true;
            this.cmbDescricaoProduto.Location = new System.Drawing.Point(126, 38);
            this.cmbDescricaoProduto.Name = "cmbDescricaoProduto";
            this.cmbDescricaoProduto.Size = new System.Drawing.Size(381, 21);
            this.cmbDescricaoProduto.TabIndex = 1;
            this.cmbDescricaoProduto.SelectedIndexChanged += new System.EventHandler(this.calculaItemPeca);
            this.cmbDescricaoProduto.SelectedValueChanged += new System.EventHandler(this.calculaItemPeca);
            this.cmbDescricaoProduto.TextChanged += new System.EventHandler(this.calculaItemPeca);
            this.cmbDescricaoProduto.Validated += new System.EventHandler(this.calculaItemPeca);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(8, 41);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(113, 13);
            this.label12.TabIndex = 5;
            this.label12.Text = "Descrição do Produto:";
            // 
            // cmbCodigoProduto
            // 
            this.cmbCodigoProduto.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCodigoProduto.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCodigoProduto.DataSource = this.a30PRODUTOBindingSource;
            this.cmbCodigoProduto.DisplayMember = "Produto";
            this.cmbCodigoProduto.FormattingEnabled = true;
            this.cmbCodigoProduto.Location = new System.Drawing.Point(126, 11);
            this.cmbCodigoProduto.Name = "cmbCodigoProduto";
            this.cmbCodigoProduto.Size = new System.Drawing.Size(114, 21);
            this.cmbCodigoProduto.TabIndex = 0;
            this.cmbCodigoProduto.Validated += new System.EventHandler(this.calculaItemPeca);
            // 
            // tabServicos
            // 
            this.tabServicos.Controls.Add(this.chkPorContaGeral);
            this.tabServicos.Controls.Add(this.grpDespesas);
            this.tabServicos.Controls.Add(this.groupBox1);
            this.tabServicos.Location = new System.Drawing.Point(4, 22);
            this.tabServicos.Name = "tabServicos";
            this.tabServicos.Padding = new System.Windows.Forms.Padding(3);
            this.tabServicos.Size = new System.Drawing.Size(812, 352);
            this.tabServicos.TabIndex = 8;
            this.tabServicos.Text = "Serviços";
            this.tabServicos.UseVisualStyleBackColor = true;
            // 
            // chkPorContaGeral
            // 
            this.chkPorContaGeral.AutoSize = true;
            this.chkPorContaGeral.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.osOrdemServicoBindingSource, "POR_CONTA_GERAL", true));
            this.chkPorContaGeral.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.osOrdemServicoBindingSource, "POR_CONTA_GERAL", true));
            this.chkPorContaGeral.Location = new System.Drawing.Point(460, 5);
            this.chkPorContaGeral.Name = "chkPorContaGeral";
            this.chkPorContaGeral.Size = new System.Drawing.Size(78, 17);
            this.chkPorContaGeral.TabIndex = 36;
            this.chkPorContaGeral.Text = "Por conta?";
            this.chkPorContaGeral.UseVisualStyleBackColor = true;
            this.chkPorContaGeral.CheckedChanged += new System.EventHandler(this.chkPorConta_CheckedChanged);
            // 
            // grpDespesas
            // 
            this.grpDespesas.Controls.Add(this.chkPorContaOutros);
            this.grpDespesas.Controls.Add(this.chkPorContaTranslado);
            this.grpDespesas.Controls.Add(this.chkPorContaHospedagem);
            this.grpDespesas.Controls.Add(this.chkPorContaAlimentacao);
            this.grpDespesas.Controls.Add(this.label34);
            this.grpDespesas.Controls.Add(this.txtTotalDesp);
            this.grpDespesas.Controls.Add(this.label32);
            this.grpDespesas.Controls.Add(this.txtOutrasDespesas);
            this.grpDespesas.Controls.Add(this.txtTranslado);
            this.grpDespesas.Controls.Add(this.txtHospedagem);
            this.grpDespesas.Controls.Add(this.txtAlimentacao);
            this.grpDespesas.Controls.Add(this.label22);
            this.grpDespesas.Controls.Add(this.label23);
            this.grpDespesas.Controls.Add(this.label24);
            this.grpDespesas.Controls.Add(this.label25);
            this.grpDespesas.Location = new System.Drawing.Point(460, 28);
            this.grpDespesas.Name = "grpDespesas";
            this.grpDespesas.Size = new System.Drawing.Size(324, 174);
            this.grpDespesas.TabIndex = 1;
            this.grpDespesas.TabStop = false;
            this.grpDespesas.Text = " Discriminação das Despesas ";
            // 
            // chkPorContaOutros
            // 
            this.chkPorContaOutros.AutoSize = true;
            this.chkPorContaOutros.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.osOrdemServicoBindingSource, "POR_CONTA_OUTROS", true));
            this.chkPorContaOutros.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.osOrdemServicoBindingSource, "POR_CONTA_OUTROS", true));
            this.chkPorContaOutros.Location = new System.Drawing.Point(93, 118);
            this.chkPorContaOutros.Name = "chkPorContaOutros";
            this.chkPorContaOutros.Size = new System.Drawing.Size(78, 17);
            this.chkPorContaOutros.TabIndex = 7;
            this.chkPorContaOutros.Text = "Por conta?";
            this.chkPorContaOutros.UseVisualStyleBackColor = true;
            this.chkPorContaOutros.CheckedChanged += new System.EventHandler(this.chkPorContaOutros_CheckedChanged);
            // 
            // chkPorContaTranslado
            // 
            this.chkPorContaTranslado.AutoSize = true;
            this.chkPorContaTranslado.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.osOrdemServicoBindingSource, "POR_CONTA_TRANSLADO", true));
            this.chkPorContaTranslado.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.osOrdemServicoBindingSource, "POR_CONTA_TRANSLADO", true));
            this.chkPorContaTranslado.Location = new System.Drawing.Point(93, 92);
            this.chkPorContaTranslado.Name = "chkPorContaTranslado";
            this.chkPorContaTranslado.Size = new System.Drawing.Size(78, 17);
            this.chkPorContaTranslado.TabIndex = 5;
            this.chkPorContaTranslado.Text = "Por conta?";
            this.chkPorContaTranslado.UseVisualStyleBackColor = true;
            this.chkPorContaTranslado.CheckedChanged += new System.EventHandler(this.chkPorContaTranslado_CheckedChanged);
            // 
            // chkPorContaHospedagem
            // 
            this.chkPorContaHospedagem.AutoSize = true;
            this.chkPorContaHospedagem.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.osOrdemServicoBindingSource, "POR_CONTA_HOSPEDAGEM", true));
            this.chkPorContaHospedagem.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.osOrdemServicoBindingSource, "POR_CONTA_HOSPEDAGEM", true));
            this.chkPorContaHospedagem.Location = new System.Drawing.Point(93, 66);
            this.chkPorContaHospedagem.Name = "chkPorContaHospedagem";
            this.chkPorContaHospedagem.Size = new System.Drawing.Size(78, 17);
            this.chkPorContaHospedagem.TabIndex = 3;
            this.chkPorContaHospedagem.Text = "Por conta?";
            this.chkPorContaHospedagem.UseVisualStyleBackColor = true;
            this.chkPorContaHospedagem.CheckedChanged += new System.EventHandler(this.chkPorContaHospedagem_CheckedChanged);
            // 
            // chkPorContaAlimentacao
            // 
            this.chkPorContaAlimentacao.AutoSize = true;
            this.chkPorContaAlimentacao.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.osOrdemServicoBindingSource, "POR_CONTA_ALIMENTACAO", true));
            this.chkPorContaAlimentacao.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.osOrdemServicoBindingSource, "POR_CONTA_ALIMENTACAO", true));
            this.chkPorContaAlimentacao.Location = new System.Drawing.Point(93, 40);
            this.chkPorContaAlimentacao.Name = "chkPorContaAlimentacao";
            this.chkPorContaAlimentacao.Size = new System.Drawing.Size(78, 17);
            this.chkPorContaAlimentacao.TabIndex = 0;
            this.chkPorContaAlimentacao.Text = "Por conta?";
            this.chkPorContaAlimentacao.UseVisualStyleBackColor = true;
            this.chkPorContaAlimentacao.CheckedChanged += new System.EventHandler(this.chkPorContaAlimentacao_CheckedChanged);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Red;
            this.label34.Location = new System.Drawing.Point(6, 145);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(92, 13);
            this.label34.TabIndex = 34;
            this.label34.Text = "TOTAL DESP.:";
            // 
            // txtTotalDesp
            // 
            this.txtTotalDesp.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "TOTAL_DESPESAS_OS", true));
            this.txtTotalDesp.Enabled = false;
            this.txtTotalDesp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalDesp.Location = new System.Drawing.Point(108, 142);
            this.txtTotalDesp.Name = "txtTotalDesp";
            this.txtTotalDesp.ReadOnly = true;
            this.txtTotalDesp.Size = new System.Drawing.Size(94, 20);
            this.txtTotalDesp.TabIndex = 9;
            this.txtTotalDesp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTotalDesp.TextChanged += new System.EventHandler(this.calculaVlrTotalOS);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(217, 22);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(31, 13);
            this.label32.TabIndex = 30;
            this.label32.Text = "Valor";
            // 
            // txtOutrasDespesas
            // 
            this.txtOutrasDespesas.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "OUTRAS_DESPESAS", true));
            this.txtOutrasDespesas.Location = new System.Drawing.Point(185, 116);
            this.txtOutrasDespesas.Name = "txtOutrasDespesas";
            this.txtOutrasDespesas.Size = new System.Drawing.Size(94, 20);
            this.txtOutrasDespesas.TabIndex = 8;
            this.txtOutrasDespesas.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtOutrasDespesas.TextChanged += new System.EventHandler(this.calculaVlrDespesas);
            this.txtOutrasDespesas.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TplPermiteSoValor);
            // 
            // txtTranslado
            // 
            this.txtTranslado.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "TRANSLADO", true));
            this.txtTranslado.Location = new System.Drawing.Point(185, 90);
            this.txtTranslado.Name = "txtTranslado";
            this.txtTranslado.Size = new System.Drawing.Size(94, 20);
            this.txtTranslado.TabIndex = 6;
            this.txtTranslado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTranslado.TextChanged += new System.EventHandler(this.calculaVlrDespesas);
            this.txtTranslado.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TplPermiteSoValor);
            // 
            // txtHospedagem
            // 
            this.txtHospedagem.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "HOSPEDAGEM", true));
            this.txtHospedagem.Location = new System.Drawing.Point(185, 64);
            this.txtHospedagem.Name = "txtHospedagem";
            this.txtHospedagem.Size = new System.Drawing.Size(94, 20);
            this.txtHospedagem.TabIndex = 4;
            this.txtHospedagem.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtHospedagem.TextChanged += new System.EventHandler(this.calculaVlrDespesas);
            this.txtHospedagem.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TplPermiteSoValor);
            // 
            // txtAlimentacao
            // 
            this.txtAlimentacao.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "ALIMENTACAO", true));
            this.txtAlimentacao.Location = new System.Drawing.Point(185, 38);
            this.txtAlimentacao.Name = "txtAlimentacao";
            this.txtAlimentacao.Size = new System.Drawing.Size(94, 20);
            this.txtAlimentacao.TabIndex = 1;
            this.txtAlimentacao.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAlimentacao.TextChanged += new System.EventHandler(this.calculaVlrDespesas);
            this.txtAlimentacao.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TplPermiteSoValor);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(38, 119);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(41, 13);
            this.label22.TabIndex = 7;
            this.label22.Text = "Outros:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(22, 93);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(57, 13);
            this.label23.TabIndex = 6;
            this.label23.Text = "Translado:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(6, 67);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(73, 13);
            this.label24.TabIndex = 5;
            this.label24.Text = "Hospedagem:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(11, 41);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(68, 13);
            this.label25.TabIndex = 4;
            this.label25.Text = "Alimentação:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox10);
            this.groupBox1.Controls.Add(this.txtHoraDeslocamentoQtde);
            this.groupBox1.Controls.Add(this.label33);
            this.groupBox1.Controls.Add(this.txtTotalServicosTotal);
            this.groupBox1.Controls.Add(this.label31);
            this.groupBox1.Controls.Add(this.txtOutrosServicosTotal);
            this.groupBox1.Controls.Add(this.txtDeslocamentoKmTotal);
            this.groupBox1.Controls.Add(this.txtPontoRedeTotal);
            this.groupBox1.Controls.Add(this.txtTaxaInstalacaoTotal);
            this.groupBox1.Controls.Add(this.txtHoraExcedenteTotal);
            this.groupBox1.Controls.Add(this.txtHoraDeslocamentoTotal);
            this.groupBox1.Controls.Add(this.txtVisitaTecnicaTotal);
            this.groupBox1.Controls.Add(this.label30);
            this.groupBox1.Controls.Add(this.label28);
            this.groupBox1.Controls.Add(this.txtOutrosServicosQtde);
            this.groupBox1.Controls.Add(this.txtOutrosServicosVlr);
            this.groupBox1.Controls.Add(this.txtDeslocamentoKmQtde);
            this.groupBox1.Controls.Add(this.txtPontoRedeQtde);
            this.groupBox1.Controls.Add(this.txtTaxaInstalacaoQtde);
            this.groupBox1.Controls.Add(this.txtHoraExcedenteQtde);
            this.groupBox1.Controls.Add(this.txtVisitaTecnicaQtde);
            this.groupBox1.Controls.Add(this.txtDeslocamentoKmVlr);
            this.groupBox1.Controls.Add(this.txtPontoRedeVlr);
            this.groupBox1.Controls.Add(this.txtTaxaInstalacaoVlr);
            this.groupBox1.Controls.Add(this.txtHoraExcedenteVlr);
            this.groupBox1.Controls.Add(this.txtHoraDeslocamentoVlr);
            this.groupBox1.Controls.Add(this.txtVisitaTecnicaVlr);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(445, 340);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = " Discriminação dos Serviços ";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.label26);
            this.groupBox10.Controls.Add(this.button1);
            this.groupBox10.Controls.Add(this.lb_media_de_consumo);
            this.groupBox10.Controls.Add(this.lbl_total_de_kilometragem);
            this.groupBox10.Controls.Add(this.tb_preco);
            this.groupBox10.Controls.Add(this.tb_media);
            this.groupBox10.Controls.Add(this.tb_kilometragem);
            this.groupBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox10.Location = new System.Drawing.Point(6, 170);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(428, 74);
            this.groupBox10.TabIndex = 34;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Calculador - Preço Unitário";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(260, 26);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(94, 13);
            this.label26.TabIndex = 6;
            this.label26.Text = "Preço Combustível";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(379, 43);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(40, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lb_media_de_consumo
            // 
            this.lb_media_de_consumo.AutoSize = true;
            this.lb_media_de_consumo.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_media_de_consumo.Location = new System.Drawing.Point(136, 26);
            this.lb_media_de_consumo.Name = "lb_media_de_consumo";
            this.lb_media_de_consumo.Size = new System.Drawing.Size(98, 13);
            this.lb_media_de_consumo.TabIndex = 4;
            this.lb_media_de_consumo.Text = "Media do Consumo";
            // 
            // lbl_total_de_kilometragem
            // 
            this.lbl_total_de_kilometragem.AutoSize = true;
            this.lbl_total_de_kilometragem.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_total_de_kilometragem.Location = new System.Drawing.Point(11, 26);
            this.lbl_total_de_kilometragem.Name = "lbl_total_de_kilometragem";
            this.lbl_total_de_kilometragem.Size = new System.Drawing.Size(110, 13);
            this.lbl_total_de_kilometragem.TabIndex = 3;
            this.lbl_total_de_kilometragem.Text = "Total de Kilometragem";
            // 
            // tb_preco
            // 
            this.tb_preco.Location = new System.Drawing.Point(253, 45);
            this.tb_preco.Name = "tb_preco";
            this.tb_preco.Size = new System.Drawing.Size(120, 18);
            this.tb_preco.TabIndex = 2;
            this.tb_preco.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_preco_KeyPress);
            // 
            // tb_media
            // 
            this.tb_media.Location = new System.Drawing.Point(129, 45);
            this.tb_media.Name = "tb_media";
            this.tb_media.Size = new System.Drawing.Size(120, 18);
            this.tb_media.TabIndex = 1;
            this.tb_media.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_media_KeyPress);
            // 
            // tb_kilometragem
            // 
            this.tb_kilometragem.Location = new System.Drawing.Point(5, 45);
            this.tb_kilometragem.Name = "tb_kilometragem";
            this.tb_kilometragem.Size = new System.Drawing.Size(120, 18);
            this.tb_kilometragem.TabIndex = 0;
            this.tb_kilometragem.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_kilometragem_KeyPress);
            // 
            // txtHoraDeslocamentoQtde
            // 
            this.txtHoraDeslocamentoQtde.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "HORA_DESLOCAMENTO_QTDE", true));
            this.txtHoraDeslocamentoQtde.Location = new System.Drawing.Point(286, 64);
            this.txtHoraDeslocamentoQtde.Name = "txtHoraDeslocamentoQtde";
            this.txtHoraDeslocamentoQtde.Size = new System.Drawing.Size(48, 20);
            this.txtHoraDeslocamentoQtde.TabIndex = 4;
            this.txtHoraDeslocamentoQtde.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtHoraDeslocamentoQtde.TextChanged += new System.EventHandler(this.calculaDeslocamento);
            this.txtHoraDeslocamentoQtde.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TplPermiteSoNumero);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Red;
            this.label33.Location = new System.Drawing.Point(124, 312);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(56, 13);
            this.label33.TabIndex = 33;
            this.label33.Text = "TOTAIS:";
            // 
            // txtTotalServicosTotal
            // 
            this.txtTotalServicosTotal.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "TOTAL_SERVICOS_OS", true));
            this.txtTotalServicosTotal.Enabled = false;
            this.txtTotalServicosTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalServicosTotal.Location = new System.Drawing.Point(286, 309);
            this.txtTotalServicosTotal.Name = "txtTotalServicosTotal";
            this.txtTotalServicosTotal.Size = new System.Drawing.Size(148, 20);
            this.txtTotalServicosTotal.TabIndex = 21;
            this.txtTotalServicosTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTotalServicosTotal.TextChanged += new System.EventHandler(this.calculaVlrTotalOS);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(374, 22);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(31, 13);
            this.label31.TabIndex = 29;
            this.label31.Text = "Total";
            // 
            // txtOutrosServicosTotal
            // 
            this.txtOutrosServicosTotal.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "OUTROS_SERVICOS_TOTAL", true));
            this.txtOutrosServicosTotal.Enabled = false;
            this.txtOutrosServicosTotal.Location = new System.Drawing.Point(342, 283);
            this.txtOutrosServicosTotal.Name = "txtOutrosServicosTotal";
            this.txtOutrosServicosTotal.Size = new System.Drawing.Size(92, 20);
            this.txtOutrosServicosTotal.TabIndex = 20;
            this.txtOutrosServicosTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtOutrosServicosTotal.TextChanged += new System.EventHandler(this.calculaVlrTotalServ);
            // 
            // txtDeslocamentoKmTotal
            // 
            this.txtDeslocamentoKmTotal.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "DESLOCAMENTO_KM_TOTAL", true));
            this.txtDeslocamentoKmTotal.Enabled = false;
            this.txtDeslocamentoKmTotal.Location = new System.Drawing.Point(342, 256);
            this.txtDeslocamentoKmTotal.Name = "txtDeslocamentoKmTotal";
            this.txtDeslocamentoKmTotal.Size = new System.Drawing.Size(92, 20);
            this.txtDeslocamentoKmTotal.TabIndex = 17;
            this.txtDeslocamentoKmTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDeslocamentoKmTotal.TextChanged += new System.EventHandler(this.calculaVlrTotalServ);
            // 
            // txtPontoRedeTotal
            // 
            this.txtPontoRedeTotal.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "PONTO_REDE_TOTAL", true));
            this.txtPontoRedeTotal.Enabled = false;
            this.txtPontoRedeTotal.Location = new System.Drawing.Point(342, 142);
            this.txtPontoRedeTotal.Name = "txtPontoRedeTotal";
            this.txtPontoRedeTotal.Size = new System.Drawing.Size(92, 20);
            this.txtPontoRedeTotal.TabIndex = 14;
            this.txtPontoRedeTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPontoRedeTotal.TextChanged += new System.EventHandler(this.calculaVlrTotalServ);
            // 
            // txtTaxaInstalacaoTotal
            // 
            this.txtTaxaInstalacaoTotal.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "TAXA_INSTALACAO_TOTAL", true));
            this.txtTaxaInstalacaoTotal.Enabled = false;
            this.txtTaxaInstalacaoTotal.Location = new System.Drawing.Point(342, 116);
            this.txtTaxaInstalacaoTotal.Name = "txtTaxaInstalacaoTotal";
            this.txtTaxaInstalacaoTotal.Size = new System.Drawing.Size(92, 20);
            this.txtTaxaInstalacaoTotal.TabIndex = 11;
            this.txtTaxaInstalacaoTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTaxaInstalacaoTotal.TextChanged += new System.EventHandler(this.calculaVlrTotalServ);
            // 
            // txtHoraExcedenteTotal
            // 
            this.txtHoraExcedenteTotal.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "HORA_EXCEDENTE_TOTAL", true));
            this.txtHoraExcedenteTotal.Enabled = false;
            this.txtHoraExcedenteTotal.Location = new System.Drawing.Point(342, 90);
            this.txtHoraExcedenteTotal.Name = "txtHoraExcedenteTotal";
            this.txtHoraExcedenteTotal.Size = new System.Drawing.Size(92, 20);
            this.txtHoraExcedenteTotal.TabIndex = 8;
            this.txtHoraExcedenteTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtHoraExcedenteTotal.TextChanged += new System.EventHandler(this.calculaVlrTotalServ);
            // 
            // txtHoraDeslocamentoTotal
            // 
            this.txtHoraDeslocamentoTotal.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "HORA_DESLOCAMENTO_TOTAL", true));
            this.txtHoraDeslocamentoTotal.Enabled = false;
            this.txtHoraDeslocamentoTotal.Location = new System.Drawing.Point(342, 64);
            this.txtHoraDeslocamentoTotal.Name = "txtHoraDeslocamentoTotal";
            this.txtHoraDeslocamentoTotal.Size = new System.Drawing.Size(92, 20);
            this.txtHoraDeslocamentoTotal.TabIndex = 5;
            this.txtHoraDeslocamentoTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtHoraDeslocamentoTotal.TextChanged += new System.EventHandler(this.calculaVlrTotalServ);
            // 
            // txtVisitaTecnicaTotal
            // 
            this.txtVisitaTecnicaTotal.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "VISITA_TECNICA_TOTAL", true));
            this.txtVisitaTecnicaTotal.Enabled = false;
            this.txtVisitaTecnicaTotal.Location = new System.Drawing.Point(342, 38);
            this.txtVisitaTecnicaTotal.Name = "txtVisitaTecnicaTotal";
            this.txtVisitaTecnicaTotal.Size = new System.Drawing.Size(92, 20);
            this.txtVisitaTecnicaTotal.TabIndex = 2;
            this.txtVisitaTecnicaTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtVisitaTecnicaTotal.TextChanged += new System.EventHandler(this.calculaVlrTotalServ);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(280, 22);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(62, 13);
            this.label30.TabIndex = 21;
            this.label30.Text = "Quantidade";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(203, 22);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(61, 13);
            this.label28.TabIndex = 20;
            this.label28.Text = "Vlr. Unitário";
            // 
            // txtOutrosServicosQtde
            // 
            this.txtOutrosServicosQtde.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "OUTROS_SERVICOS_QTDE", true));
            this.txtOutrosServicosQtde.Location = new System.Drawing.Point(286, 283);
            this.txtOutrosServicosQtde.Name = "txtOutrosServicosQtde";
            this.txtOutrosServicosQtde.Size = new System.Drawing.Size(48, 20);
            this.txtOutrosServicosQtde.TabIndex = 19;
            this.txtOutrosServicosQtde.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtOutrosServicosQtde.TextChanged += new System.EventHandler(this.calculaOutros);
            this.txtOutrosServicosQtde.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TplPermiteSoNumero);
            // 
            // txtOutrosServicosVlr
            // 
            this.txtOutrosServicosVlr.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "OUTROS_SERVICOS_VLR", true));
            this.txtOutrosServicosVlr.Location = new System.Drawing.Point(186, 283);
            this.txtOutrosServicosVlr.Name = "txtOutrosServicosVlr";
            this.txtOutrosServicosVlr.Size = new System.Drawing.Size(92, 20);
            this.txtOutrosServicosVlr.TabIndex = 18;
            this.txtOutrosServicosVlr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtOutrosServicosVlr.TextChanged += new System.EventHandler(this.calculaOutros);
            this.txtOutrosServicosVlr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TplPermiteSoValor);
            // 
            // txtDeslocamentoKmQtde
            // 
            this.txtDeslocamentoKmQtde.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "DESLOCAMENTO_KM_QTDE", true));
            this.txtDeslocamentoKmQtde.Location = new System.Drawing.Point(286, 256);
            this.txtDeslocamentoKmQtde.Name = "txtDeslocamentoKmQtde";
            this.txtDeslocamentoKmQtde.Size = new System.Drawing.Size(48, 20);
            this.txtDeslocamentoKmQtde.TabIndex = 16;
            this.txtDeslocamentoKmQtde.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDeslocamentoKmQtde.TextChanged += new System.EventHandler(this.calculaDeslocamentoKM);
            this.txtDeslocamentoKmQtde.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TplPermiteSoNumero);
            // 
            // txtPontoRedeQtde
            // 
            this.txtPontoRedeQtde.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "PONTO_REDE_QTDE", true));
            this.txtPontoRedeQtde.Location = new System.Drawing.Point(286, 142);
            this.txtPontoRedeQtde.Name = "txtPontoRedeQtde";
            this.txtPontoRedeQtde.Size = new System.Drawing.Size(48, 20);
            this.txtPontoRedeQtde.TabIndex = 13;
            this.txtPontoRedeQtde.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPontoRedeQtde.TextChanged += new System.EventHandler(this.calculaRede);
            this.txtPontoRedeQtde.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TplPermiteSoNumero);
            // 
            // txtTaxaInstalacaoQtde
            // 
            this.txtTaxaInstalacaoQtde.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "TAXA_INSTALACAO_QTDE", true));
            this.txtTaxaInstalacaoQtde.Location = new System.Drawing.Point(286, 116);
            this.txtTaxaInstalacaoQtde.Name = "txtTaxaInstalacaoQtde";
            this.txtTaxaInstalacaoQtde.Size = new System.Drawing.Size(48, 20);
            this.txtTaxaInstalacaoQtde.TabIndex = 10;
            this.txtTaxaInstalacaoQtde.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTaxaInstalacaoQtde.TextChanged += new System.EventHandler(this.calculaInstalacao);
            this.txtTaxaInstalacaoQtde.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TplPermiteSoNumero);
            // 
            // txtHoraExcedenteQtde
            // 
            this.txtHoraExcedenteQtde.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "HORA_EXCEDENTE_QTDE", true));
            this.txtHoraExcedenteQtde.Location = new System.Drawing.Point(286, 90);
            this.txtHoraExcedenteQtde.Name = "txtHoraExcedenteQtde";
            this.txtHoraExcedenteQtde.Size = new System.Drawing.Size(48, 20);
            this.txtHoraExcedenteQtde.TabIndex = 7;
            this.txtHoraExcedenteQtde.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtHoraExcedenteQtde.TextChanged += new System.EventHandler(this.calculaExcedente);
            this.txtHoraExcedenteQtde.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TplPermiteSoNumero);
            // 
            // txtVisitaTecnicaQtde
            // 
            this.txtVisitaTecnicaQtde.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "VISITA_TECNICA_QTDE", true));
            this.txtVisitaTecnicaQtde.Location = new System.Drawing.Point(286, 38);
            this.txtVisitaTecnicaQtde.Name = "txtVisitaTecnicaQtde";
            this.txtVisitaTecnicaQtde.Size = new System.Drawing.Size(48, 20);
            this.txtVisitaTecnicaQtde.TabIndex = 1;
            this.txtVisitaTecnicaQtde.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtVisitaTecnicaQtde.TextChanged += new System.EventHandler(this.calculaVisitaTecnica);
            this.txtVisitaTecnicaQtde.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TplPermiteSoNumero);
            // 
            // txtDeslocamentoKmVlr
            // 
            this.txtDeslocamentoKmVlr.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "DESLOCAMENTO_KM_VLR", true));
            this.txtDeslocamentoKmVlr.Location = new System.Drawing.Point(186, 256);
            this.txtDeslocamentoKmVlr.Name = "txtDeslocamentoKmVlr";
            this.txtDeslocamentoKmVlr.Size = new System.Drawing.Size(92, 20);
            this.txtDeslocamentoKmVlr.TabIndex = 15;
            this.txtDeslocamentoKmVlr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDeslocamentoKmVlr.TextChanged += new System.EventHandler(this.calculaDeslocamentoKM);
            this.txtDeslocamentoKmVlr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TplPermiteSoValor);
            // 
            // txtPontoRedeVlr
            // 
            this.txtPontoRedeVlr.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "PONTO_REDE_VLR", true));
            this.txtPontoRedeVlr.Location = new System.Drawing.Point(186, 142);
            this.txtPontoRedeVlr.Name = "txtPontoRedeVlr";
            this.txtPontoRedeVlr.Size = new System.Drawing.Size(92, 20);
            this.txtPontoRedeVlr.TabIndex = 12;
            this.txtPontoRedeVlr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPontoRedeVlr.TextChanged += new System.EventHandler(this.calculaRede);
            this.txtPontoRedeVlr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TplPermiteSoValor);
            // 
            // txtTaxaInstalacaoVlr
            // 
            this.txtTaxaInstalacaoVlr.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "TAXA_INSTALACAO_VLR", true));
            this.txtTaxaInstalacaoVlr.Location = new System.Drawing.Point(186, 116);
            this.txtTaxaInstalacaoVlr.Name = "txtTaxaInstalacaoVlr";
            this.txtTaxaInstalacaoVlr.Size = new System.Drawing.Size(92, 20);
            this.txtTaxaInstalacaoVlr.TabIndex = 9;
            this.txtTaxaInstalacaoVlr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTaxaInstalacaoVlr.TextChanged += new System.EventHandler(this.calculaInstalacao);
            this.txtTaxaInstalacaoVlr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TplPermiteSoValor);
            // 
            // txtHoraExcedenteVlr
            // 
            this.txtHoraExcedenteVlr.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "HORA_EXCEDENTE_VLR", true));
            this.txtHoraExcedenteVlr.Location = new System.Drawing.Point(186, 90);
            this.txtHoraExcedenteVlr.Name = "txtHoraExcedenteVlr";
            this.txtHoraExcedenteVlr.Size = new System.Drawing.Size(92, 20);
            this.txtHoraExcedenteVlr.TabIndex = 6;
            this.txtHoraExcedenteVlr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtHoraExcedenteVlr.TextChanged += new System.EventHandler(this.calculaExcedente);
            this.txtHoraExcedenteVlr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TplPermiteSoValor);
            // 
            // txtHoraDeslocamentoVlr
            // 
            this.txtHoraDeslocamentoVlr.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "HORA_DESLOCAMENTO_VLR", true));
            this.txtHoraDeslocamentoVlr.Location = new System.Drawing.Point(186, 64);
            this.txtHoraDeslocamentoVlr.Name = "txtHoraDeslocamentoVlr";
            this.txtHoraDeslocamentoVlr.Size = new System.Drawing.Size(92, 20);
            this.txtHoraDeslocamentoVlr.TabIndex = 3;
            this.txtHoraDeslocamentoVlr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtHoraDeslocamentoVlr.TextChanged += new System.EventHandler(this.calculaDeslocamento);
            this.txtHoraDeslocamentoVlr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TplPermiteSoValor);
            // 
            // txtVisitaTecnicaVlr
            // 
            this.txtVisitaTecnicaVlr.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "VISITA_TECNICA_VLR", true));
            this.txtVisitaTecnicaVlr.Location = new System.Drawing.Point(186, 38);
            this.txtVisitaTecnicaVlr.Name = "txtVisitaTecnicaVlr";
            this.txtVisitaTecnicaVlr.Size = new System.Drawing.Size(92, 20);
            this.txtVisitaTecnicaVlr.TabIndex = 0;
            this.txtVisitaTecnicaVlr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtVisitaTecnicaVlr.TextChanged += new System.EventHandler(this.calculaVisitaTecnica);
            this.txtVisitaTecnicaVlr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TplPermiteSoValor);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(32, 259);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(148, 13);
            this.label19.TabIndex = 5;
            this.label19.Text = "Deslocamento p/Km Rodado:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(17, 145);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(163, 13);
            this.label20.TabIndex = 4;
            this.label20.Text = "Configuração de Ponto de Rede:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(11, 119);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(169, 13);
            this.label21.TabIndex = 3;
            this.label21.Text = "Taxa de Instalação de Programas:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(7, 93);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(173, 13);
            this.label18.TabIndex = 2;
            this.label18.Text = "Hora Técnica Excedente no Local:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(17, 67);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(163, 13);
            this.label17.TabIndex = 1;
            this.label17.Text = "Hora Técnica em Deslocamento:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(139, 286);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(41, 13);
            this.label16.TabIndex = 1;
            this.label16.Text = "Outros:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(103, 41);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(77, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "Visita Técnica:";
            // 
            // lblCabCliente
            // 
            this.lblCabCliente.AutoSize = true;
            this.lblCabCliente.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bdsClientes, "Nome", true));
            this.lblCabCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCabCliente.Location = new System.Drawing.Point(116, 41);
            this.lblCabCliente.Name = "lblCabCliente";
            this.lblCabCliente.Size = new System.Drawing.Size(27, 13);
            this.lblCabCliente.TabIndex = 6;
            this.lblCabCliente.Text = "(...)";
            // 
            // lblCabNCPJ
            // 
            this.lblCabNCPJ.AutoSize = true;
            this.lblCabNCPJ.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bdsClientes, "Cgc", true));
            this.lblCabNCPJ.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCabNCPJ.Location = new System.Drawing.Point(116, 79);
            this.lblCabNCPJ.Name = "lblCabNCPJ";
            this.lblCabNCPJ.Size = new System.Drawing.Size(27, 13);
            this.lblCabNCPJ.TabIndex = 7;
            this.lblCabNCPJ.Text = "(...)";
            // 
            // lblCabIE
            // 
            this.lblCabIE.AutoSize = true;
            this.lblCabIE.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bdsClientes, "Inscricao", true));
            this.lblCabIE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCabIE.Location = new System.Drawing.Point(116, 60);
            this.lblCabIE.Name = "lblCabIE";
            this.lblCabIE.Size = new System.Drawing.Size(27, 13);
            this.lblCabIE.TabIndex = 8;
            this.lblCabIE.Text = "(...)";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 96);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(109, 13);
            this.label10.TabIndex = 10;
            this.label10.Text = "Telefone...................:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 112);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(110, 13);
            this.label11.TabIndex = 11;
            this.label11.Text = "Endereço..................:";
            // 
            // lblCabTelefone
            // 
            this.lblCabTelefone.AutoSize = true;
            this.lblCabTelefone.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bdsClientes, "Telefone1", true));
            this.lblCabTelefone.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCabTelefone.Location = new System.Drawing.Point(116, 96);
            this.lblCabTelefone.Name = "lblCabTelefone";
            this.lblCabTelefone.Size = new System.Drawing.Size(27, 13);
            this.lblCabTelefone.TabIndex = 12;
            this.lblCabTelefone.Text = "(...)";
            // 
            // lblCabObservacao
            // 
            this.lblCabObservacao.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bdsClientes, "Endereco", true));
            this.lblCabObservacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCabObservacao.Location = new System.Drawing.Point(116, 112);
            this.lblCabObservacao.Name = "lblCabObservacao";
            this.lblCabObservacao.Size = new System.Drawing.Size(237, 16);
            this.lblCabObservacao.TabIndex = 13;
            this.lblCabObservacao.Text = "(...)";
            // 
            // tabEquipamentos
            // 
            this.tabEquipamentos.AutoScroll = true;
            this.tabEquipamentos.Controls.Add(this.tbSeloAnterior);
            this.tabEquipamentos.Controls.Add(this.lblSeloAnterior);
            this.tabEquipamentos.Controls.Add(this.lblMarcaInitial);
            this.tabEquipamentos.Controls.Add(this.tbMarcaInicial);
            this.tabEquipamentos.Controls.Add(this.dataGridView1);
            this.tabEquipamentos.Controls.Add(this.groupBox3);
            this.tabEquipamentos.Controls.Add(this.cmbCodigoINMETROSelos);
            this.tabEquipamentos.Controls.Add(this.cmbCodigoEquipamentoSelos);
            this.tabEquipamentos.Controls.Add(this.groupBox6);
            this.tabEquipamentos.Controls.Add(this.cmbCodigoSelos);
            this.tabEquipamentos.Controls.Add(this.groupBox5);
            this.tabEquipamentos.Controls.Add(this.btnAdicionarEquipamento);
            this.tabEquipamentos.Location = new System.Drawing.Point(4, 22);
            this.tabEquipamentos.Name = "tabEquipamentos";
            this.tabEquipamentos.Padding = new System.Windows.Forms.Padding(3);
            this.tabEquipamentos.Size = new System.Drawing.Size(812, 352);
            this.tabEquipamentos.TabIndex = 9;
            this.tabEquipamentos.Text = "Selos";
            this.tabEquipamentos.UseVisualStyleBackColor = true;
            // 
            // tbSeloAnterior
            // 
            this.tbSeloAnterior.Location = new System.Drawing.Point(579, 55);
            this.tbSeloAnterior.MaxLength = 10;
            this.tbSeloAnterior.Name = "tbSeloAnterior";
            this.tbSeloAnterior.Size = new System.Drawing.Size(100, 20);
            this.tbSeloAnterior.TabIndex = 61;
            // 
            // lblSeloAnterior
            // 
            this.lblSeloAnterior.AutoSize = true;
            this.lblSeloAnterior.Location = new System.Drawing.Point(501, 58);
            this.lblSeloAnterior.Name = "lblSeloAnterior";
            this.lblSeloAnterior.Size = new System.Drawing.Size(67, 13);
            this.lblSeloAnterior.TabIndex = 60;
            this.lblSeloAnterior.Text = "Selo Anterior";
            // 
            // lblMarcaInitial
            // 
            this.lblMarcaInitial.AutoSize = true;
            this.lblMarcaInitial.Location = new System.Drawing.Point(300, 58);
            this.lblMarcaInitial.Name = "lblMarcaInitial";
            this.lblMarcaInitial.Size = new System.Drawing.Size(73, 13);
            this.lblMarcaInitial.TabIndex = 59;
            this.lblMarcaInitial.Text = "Marca Inicial: ";
            // 
            // tbMarcaInicial
            // 
            this.tbMarcaInicial.Location = new System.Drawing.Point(380, 55);
            this.tbMarcaInicial.MaxLength = 10;
            this.tbMarcaInicial.Name = "tbMarcaInicial";
            this.tbMarcaInicial.Size = new System.Drawing.Size(100, 20);
            this.tbMarcaInicial.TabIndex = 58;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AutoGenerateColumns = false;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NUM_SERIE,
            this.Numero_IMETRO,
            this.numero_etiqueta,
            this.iDETIQUETADataGridViewTextBoxColumn,
            this.iDIBAMETRODataGridViewTextBoxColumn,
            this.iDDataGridViewTextBoxColumn2,
            this.MARCA_INSTRUMENTO_1,
            this.iDORDEMDataGridViewTextBoxColumn,
            this.marca_inicial,
            this.selo_anterior});
            this.dataGridView1.DataSource = this.bdsSelectEquipamentoX;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridView1.Location = new System.Drawing.Point(6, 81);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(800, 265);
            this.dataGridView1.TabIndex = 55;
            // 
            // NUM_SERIE
            // 
            this.NUM_SERIE.DataPropertyName = "NUM_SERIE";
            this.NUM_SERIE.HeaderText = "Número de Série";
            this.NUM_SERIE.Name = "NUM_SERIE";
            this.NUM_SERIE.ReadOnly = true;
            this.NUM_SERIE.Width = 120;
            // 
            // Numero_IMETRO
            // 
            this.Numero_IMETRO.DataPropertyName = "NUM_INMETRO";
            this.Numero_IMETRO.HeaderText = "Número INMETRO";
            this.Numero_IMETRO.Name = "Numero_IMETRO";
            this.Numero_IMETRO.ReadOnly = true;
            this.Numero_IMETRO.Width = 120;
            // 
            // numero_etiqueta
            // 
            this.numero_etiqueta.DataPropertyName = "NUM_ETIQUETA";
            this.numero_etiqueta.HeaderText = "Número Etiqueta";
            this.numero_etiqueta.Name = "numero_etiqueta";
            this.numero_etiqueta.ReadOnly = true;
            this.numero_etiqueta.Width = 120;
            // 
            // iDETIQUETADataGridViewTextBoxColumn
            // 
            this.iDETIQUETADataGridViewTextBoxColumn.DataPropertyName = "IDETIQUETA";
            this.iDETIQUETADataGridViewTextBoxColumn.HeaderText = "IDETIQUETA";
            this.iDETIQUETADataGridViewTextBoxColumn.Name = "iDETIQUETADataGridViewTextBoxColumn";
            this.iDETIQUETADataGridViewTextBoxColumn.ReadOnly = true;
            this.iDETIQUETADataGridViewTextBoxColumn.Visible = false;
            // 
            // iDIBAMETRODataGridViewTextBoxColumn
            // 
            this.iDIBAMETRODataGridViewTextBoxColumn.DataPropertyName = "IDIBAMETRO";
            this.iDIBAMETRODataGridViewTextBoxColumn.HeaderText = "IDIBAMETRO";
            this.iDIBAMETRODataGridViewTextBoxColumn.Name = "iDIBAMETRODataGridViewTextBoxColumn";
            this.iDIBAMETRODataGridViewTextBoxColumn.ReadOnly = true;
            this.iDIBAMETRODataGridViewTextBoxColumn.Visible = false;
            // 
            // iDDataGridViewTextBoxColumn2
            // 
            this.iDDataGridViewTextBoxColumn2.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn2.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn2.Name = "iDDataGridViewTextBoxColumn2";
            this.iDDataGridViewTextBoxColumn2.ReadOnly = true;
            this.iDDataGridViewTextBoxColumn2.Visible = false;
            // 
            // MARCA_INSTRUMENTO_1
            // 
            this.MARCA_INSTRUMENTO_1.DataPropertyName = "MARCA_INSTRUMENTO";
            this.MARCA_INSTRUMENTO_1.HeaderText = "Marca";
            this.MARCA_INSTRUMENTO_1.Name = "MARCA_INSTRUMENTO_1";
            this.MARCA_INSTRUMENTO_1.ReadOnly = true;
            this.MARCA_INSTRUMENTO_1.Width = 120;
            // 
            // iDORDEMDataGridViewTextBoxColumn
            // 
            this.iDORDEMDataGridViewTextBoxColumn.DataPropertyName = "IDORDEM";
            this.iDORDEMDataGridViewTextBoxColumn.HeaderText = "IDORDEM";
            this.iDORDEMDataGridViewTextBoxColumn.Name = "iDORDEMDataGridViewTextBoxColumn";
            this.iDORDEMDataGridViewTextBoxColumn.ReadOnly = true;
            this.iDORDEMDataGridViewTextBoxColumn.Visible = false;
            // 
            // marca_inicial
            // 
            this.marca_inicial.DataPropertyName = "marca_inicial";
            this.marca_inicial.HeaderText = "Marca Inicial";
            this.marca_inicial.Name = "marca_inicial";
            this.marca_inicial.ReadOnly = true;
            // 
            // selo_anterior
            // 
            this.selo_anterior.DataPropertyName = "selo_anterior";
            this.selo_anterior.HeaderText = "Selo Anterior";
            this.selo_anterior.Name = "selo_anterior";
            this.selo_anterior.ReadOnly = true;
            // 
            // bdsSelectEquipamentoX
            // 
            this.bdsSelectEquipamentoX.AllowNew = false;
            this.bdsSelectEquipamentoX.DataMember = "OsInstrumentoServicoOsCliente";
            this.bdsSelectEquipamentoX.DataSource = this.dtsPrincipal;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cmbNumInmetroSelos);
            this.groupBox3.Controls.Add(this.label55);
            this.groupBox3.Location = new System.Drawing.Point(299, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(240, 45);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = " INMETRO ";
            // 
            // cmbNumInmetroSelos
            // 
            this.cmbNumInmetroSelos.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbNumInmetroSelos.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbNumInmetroSelos.DataSource = this.bdsINMETROSelosX;
            this.cmbNumInmetroSelos.DisplayMember = "NUM_INMETRO";
            this.cmbNumInmetroSelos.FormattingEnabled = true;
            this.cmbNumInmetroSelos.Location = new System.Drawing.Point(124, 14);
            this.cmbNumInmetroSelos.Name = "cmbNumInmetroSelos";
            this.cmbNumInmetroSelos.Size = new System.Drawing.Size(110, 21);
            this.cmbNumInmetroSelos.TabIndex = 0;
            this.cmbNumInmetroSelos.ValueMember = "ID";
            // 
            // bdsINMETROSelosX
            // 
            this.bdsINMETROSelosX.AllowNew = false;
            this.bdsINMETROSelosX.DataMember = "OsIbametroInmetroOsCliente";
            this.bdsINMETROSelosX.DataSource = this.dtsPrincipal;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(6, 17);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(115, 13);
            this.label55.TabIndex = 56;
            this.label55.Text = "Número do INMETRO:";
            // 
            // cmbCodigoINMETROSelos
            // 
            this.cmbCodigoINMETROSelos.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCodigoINMETROSelos.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCodigoINMETROSelos.DataSource = this.bdsINMETROSelosX;
            this.cmbCodigoINMETROSelos.DisplayMember = "ID";
            this.cmbCodigoINMETROSelos.FormattingEnabled = true;
            this.cmbCodigoINMETROSelos.Location = new System.Drawing.Point(364, 94);
            this.cmbCodigoINMETROSelos.Name = "cmbCodigoINMETROSelos";
            this.cmbCodigoINMETROSelos.Size = new System.Drawing.Size(191, 21);
            this.cmbCodigoINMETROSelos.TabIndex = 57;
            this.cmbCodigoINMETROSelos.ValueMember = "ID";
            // 
            // cmbCodigoEquipamentoSelos
            // 
            this.cmbCodigoEquipamentoSelos.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCodigoEquipamentoSelos.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCodigoEquipamentoSelos.DataSource = this.bdsEquipamentoSelosX;
            this.cmbCodigoEquipamentoSelos.DisplayMember = "ID";
            this.cmbCodigoEquipamentoSelos.FormattingEnabled = true;
            this.cmbCodigoEquipamentoSelos.Location = new System.Drawing.Point(139, 94);
            this.cmbCodigoEquipamentoSelos.Name = "cmbCodigoEquipamentoSelos";
            this.cmbCodigoEquipamentoSelos.Size = new System.Drawing.Size(156, 21);
            this.cmbCodigoEquipamentoSelos.TabIndex = 56;
            this.cmbCodigoEquipamentoSelos.ValueMember = "ID";
            this.cmbCodigoEquipamentoSelos.SelectedIndexChanged += new System.EventHandler(this.cmbCodigoEquipamentoSelos_SelectedIndexChanged);
            // 
            // bdsEquipamentoSelosX
            // 
            this.bdsEquipamentoSelosX.DataMember = "OsIbametroOsCliente";
            this.bdsEquipamentoSelosX.DataSource = this.dtsPrincipal;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.cmbNumeroEtiquetaSelos);
            this.groupBox6.Controls.Add(this.label52);
            this.groupBox6.Location = new System.Drawing.Point(576, 6);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(230, 45);
            this.groupBox6.TabIndex = 2;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = " Selos ";
            // 
            // cmbNumeroEtiquetaSelos
            // 
            this.cmbNumeroEtiquetaSelos.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbNumeroEtiquetaSelos.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbNumeroEtiquetaSelos.DataSource = this.bdsSelos;
            this.cmbNumeroEtiquetaSelos.DisplayMember = "NUM_ETIQUETA";
            this.cmbNumeroEtiquetaSelos.FormattingEnabled = true;
            this.cmbNumeroEtiquetaSelos.Location = new System.Drawing.Point(111, 13);
            this.cmbNumeroEtiquetaSelos.Name = "cmbNumeroEtiquetaSelos";
            this.cmbNumeroEtiquetaSelos.Size = new System.Drawing.Size(110, 21);
            this.cmbNumeroEtiquetaSelos.TabIndex = 0;
            this.cmbNumeroEtiquetaSelos.ValueMember = "ID";
            // 
            // bdsSelos
            // 
            this.bdsSelos.DataMember = "OsEtiqueta";
            this.bdsSelos.DataSource = this.dtsPrincipal;
            this.bdsSelos.Filter = "UTILIZADO = 0 AND TIPO = \'S\'";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(5, 16);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(104, 13);
            this.label52.TabIndex = 54;
            this.label52.Text = "Número da Etiqueta:";
            // 
            // cmbCodigoSelos
            // 
            this.cmbCodigoSelos.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCodigoSelos.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCodigoSelos.DataSource = this.bdsSelos;
            this.cmbCodigoSelos.DisplayMember = "ID";
            this.cmbCodigoSelos.FormattingEnabled = true;
            this.cmbCodigoSelos.Location = new System.Drawing.Point(584, 94);
            this.cmbCodigoSelos.Name = "cmbCodigoSelos";
            this.cmbCodigoSelos.Size = new System.Drawing.Size(196, 21);
            this.cmbCodigoSelos.TabIndex = 53;
            this.cmbCodigoSelos.ValueMember = "ID";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.textBox2);
            this.groupBox5.Controls.Add(this.cmbNumSerieSelos);
            this.groupBox5.Controls.Add(this.label56);
            this.groupBox5.Controls.Add(this.label49);
            this.groupBox5.Location = new System.Drawing.Point(6, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(250, 69);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = " Bombas ";
            // 
            // textBox2
            // 
            this.textBox2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bdsEquipamentoSelosX, "MARCA_INSTRUMENTO", true));
            this.textBox2.Location = new System.Drawing.Point(133, 41);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(110, 20);
            this.textBox2.TabIndex = 1;
            // 
            // cmbNumSerieSelos
            // 
            this.cmbNumSerieSelos.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbNumSerieSelos.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbNumSerieSelos.DataSource = this.bdsEquipamentoSelosX;
            this.cmbNumSerieSelos.DisplayMember = "NUM_SERIE";
            this.cmbNumSerieSelos.FormattingEnabled = true;
            this.cmbNumSerieSelos.Location = new System.Drawing.Point(133, 14);
            this.cmbNumSerieSelos.Name = "cmbNumSerieSelos";
            this.cmbNumSerieSelos.Size = new System.Drawing.Size(110, 21);
            this.cmbNumSerieSelos.TabIndex = 0;
            this.cmbNumSerieSelos.ValueMember = "ID";
            this.cmbNumSerieSelos.SelectedIndexChanged += new System.EventHandler(this.cmbNumSerieSelos_SelectedIndexChanged);
            this.cmbNumSerieSelos.SelectionChangeCommitted += new System.EventHandler(this.cmbNumSerieSelos_SelectionChangeCommitted);
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(42, 17);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(89, 13);
            this.label56.TabIndex = 56;
            this.label56.Text = "Número de Série:";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(11, 44);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(120, 13);
            this.label49.TabIndex = 52;
            this.label49.Text = "Marca do Equipamento:";
            // 
            // btnAdicionarEquipamento
            // 
            this.btnAdicionarEquipamento.Location = new System.Drawing.Point(730, 53);
            this.btnAdicionarEquipamento.Name = "btnAdicionarEquipamento";
            this.btnAdicionarEquipamento.Size = new System.Drawing.Size(75, 23);
            this.btnAdicionarEquipamento.TabIndex = 3;
            this.btnAdicionarEquipamento.Text = "Inserir";
            this.btnAdicionarEquipamento.UseVisualStyleBackColor = true;
            this.btnAdicionarEquipamento.Click += new System.EventHandler(this.button6_Click);
            // 
            // bdsEquipamentoSelos
            // 
            this.bdsEquipamentoSelos.DataMember = "OsIbametro";
            this.bdsEquipamentoSelos.DataSource = this.dtsPrincipal;
            // 
            // bdsINMETROSelos
            // 
            this.bdsINMETROSelos.AllowNew = false;
            this.bdsINMETROSelos.DataMember = "OsIbametroInmetro";
            this.bdsINMETROSelos.DataSource = this.dtsPrincipal;
            // 
            // osInstrumentoServicoBindingSource
            // 
            this.osInstrumentoServicoBindingSource.DataMember = "OsInstrumentoServico";
            this.osInstrumentoServicoBindingSource.DataSource = this.dtsPrincipal;
            // 
            // lblSituacao2
            // 
            this.lblSituacao2.AutoSize = true;
            this.lblSituacao2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSituacao2.ForeColor = System.Drawing.Color.Red;
            this.lblSituacao2.Location = new System.Drawing.Point(9, 130);
            this.lblSituacao2.Name = "lblSituacao2";
            this.lblSituacao2.Size = new System.Drawing.Size(111, 17);
            this.lblSituacao2.TabIndex = 33;
            this.lblSituacao2.Text = "Situação.......:";
            // 
            // lblCabSituacao
            // 
            this.lblCabSituacao.AutoSize = true;
            this.lblCabSituacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCabSituacao.ForeColor = System.Drawing.Color.Red;
            this.lblCabSituacao.Location = new System.Drawing.Point(116, 130);
            this.lblCabSituacao.Name = "lblCabSituacao";
            this.lblCabSituacao.Size = new System.Drawing.Size(35, 17);
            this.lblCabSituacao.TabIndex = 34;
            this.lblCabSituacao.Text = "(...)";
            // 
            // tabPagamento
            // 
            this.tabPagamento.AutoScroll = true;
            this.tabPagamento.Controls.Add(this.grdPagamentos);
            this.tabPagamento.Controls.Add(this.comboBox2);
            this.tabPagamento.Controls.Add(this.label60);
            this.tabPagamento.Controls.Add(this.txtValorPago);
            this.tabPagamento.Controls.Add(this.btnAdicionarFormaPag);
            this.tabPagamento.Controls.Add(this.cmbCodigoForma);
            this.tabPagamento.Controls.Add(this.label57);
            this.tabPagamento.Location = new System.Drawing.Point(4, 22);
            this.tabPagamento.Name = "tabPagamento";
            this.tabPagamento.Padding = new System.Windows.Forms.Padding(3);
            this.tabPagamento.Size = new System.Drawing.Size(812, 352);
            this.tabPagamento.TabIndex = 10;
            this.tabPagamento.Text = "Pagamento";
            this.tabPagamento.UseVisualStyleBackColor = true;
            // 
            // grdPagamentos
            // 
            this.grdPagamentos.AllowUserToAddRows = false;
            this.grdPagamentos.AllowUserToOrderColumns = true;
            this.grdPagamentos.AutoGenerateColumns = false;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdPagamentos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.grdPagamentos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdPagamentos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.vALORDataGridViewTextBoxColumn1,
            this.nome});
            this.grdPagamentos.DataSource = this.osFormasPagamentoBindingSource;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdPagamentos.DefaultCellStyle = dataGridViewCellStyle16;
            this.grdPagamentos.Location = new System.Drawing.Point(6, 93);
            this.grdPagamentos.Name = "grdPagamentos";
            this.grdPagamentos.ReadOnly = true;
            this.grdPagamentos.Size = new System.Drawing.Size(800, 253);
            this.grdPagamentos.TabIndex = 3;
            this.grdPagamentos.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.grdPagamentos_UserDeletingRow);
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "Código";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            // 
            // vALORDataGridViewTextBoxColumn1
            // 
            this.vALORDataGridViewTextBoxColumn1.DataPropertyName = "VALOR";
            this.vALORDataGridViewTextBoxColumn1.HeaderText = "Valor";
            this.vALORDataGridViewTextBoxColumn1.Name = "vALORDataGridViewTextBoxColumn1";
            this.vALORDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // nome
            // 
            this.nome.DataPropertyName = "NOME";
            this.nome.HeaderText = "Formas de Pagamento";
            this.nome.Name = "nome";
            this.nome.ReadOnly = true;
            this.nome.Width = 140;
            // 
            // osFormasPagamentoBindingSource
            // 
            this.osFormasPagamentoBindingSource.DataMember = "OsFormasPagamento";
            this.osFormasPagamentoBindingSource.DataSource = this.dtsPrincipal;
            this.osFormasPagamentoBindingSource.CurrentChanged += new System.EventHandler(this.osFormasPagamentoBindingSource_CurrentChanged);
            // 
            // comboBox2
            // 
            this.comboBox2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox2.DataSource = this.osParContasBindingSource;
            this.comboBox2.DisplayMember = "NOME";
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(196, 11);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(318, 21);
            this.comboBox2.TabIndex = 0;
            this.comboBox2.ValueMember = "ID";
            // 
            // osParContasBindingSource
            // 
            this.osParContasBindingSource.DataMember = "OsParContas";
            this.osParContasBindingSource.DataSource = this.dtsPrincipal;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(130, 41);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(62, 13);
            this.label60.TabIndex = 42;
            this.label60.Text = "Valor Pago:";
            // 
            // txtValorPago
            // 
            this.txtValorPago.Location = new System.Drawing.Point(196, 38);
            this.txtValorPago.Name = "txtValorPago";
            this.txtValorPago.Size = new System.Drawing.Size(94, 20);
            this.txtValorPago.TabIndex = 1;
            this.txtValorPago.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtValorPago.TextChanged += new System.EventHandler(this.calculaFormaPagto);
            this.txtValorPago.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TplPermiteSoValor);
            // 
            // btnAdicionarFormaPag
            // 
            this.btnAdicionarFormaPag.Location = new System.Drawing.Point(215, 64);
            this.btnAdicionarFormaPag.Name = "btnAdicionarFormaPag";
            this.btnAdicionarFormaPag.Size = new System.Drawing.Size(75, 23);
            this.btnAdicionarFormaPag.TabIndex = 2;
            this.btnAdicionarFormaPag.Text = "Inserir";
            this.btnAdicionarFormaPag.UseVisualStyleBackColor = true;
            this.btnAdicionarFormaPag.Click += new System.EventHandler(this.btnAdicionarFormaPag_Click);
            // 
            // cmbCodigoForma
            // 
            this.cmbCodigoForma.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCodigoForma.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCodigoForma.DataSource = this.osParContasBindingSource;
            this.cmbCodigoForma.DisplayMember = "ID";
            this.cmbCodigoForma.FormattingEnabled = true;
            this.cmbCodigoForma.Location = new System.Drawing.Point(627, 115);
            this.cmbCodigoForma.Name = "cmbCodigoForma";
            this.cmbCodigoForma.Size = new System.Drawing.Size(107, 21);
            this.cmbCodigoForma.TabIndex = 0;
            this.cmbCodigoForma.ValueMember = "ID";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(15, 14);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(177, 13);
            this.label57.TabIndex = 36;
            this.label57.Text = "Descrição da Forma de Pagamento:";
            // 
            // osReciboBindingSource
            // 
            this.osReciboBindingSource.DataMember = "OsRecibo";
            this.osReciboBindingSource.DataSource = this.dtsPrincipal;
            // 
            // tabLacres
            // 
            this.tabLacres.Controls.Add(this.tbLacreAnterior);
            this.tabLacres.Controls.Add(this.tbMarcaInicialLacre);
            this.tabLacres.Controls.Add(this.lbl_LacreAnterior);
            this.tabLacres.Controls.Add(this.lblMarcaInicialLacre);
            this.tabLacres.Controls.Add(this.dataGridView2);
            this.tabLacres.Controls.Add(this.groupBox9);
            this.tabLacres.Controls.Add(this.cmbCodigoINMETROLacres);
            this.tabLacres.Controls.Add(this.cmbCodigoEquipamentoLacres);
            this.tabLacres.Controls.Add(this.cmbCodigoLacres);
            this.tabLacres.Controls.Add(this.groupBox7);
            this.tabLacres.Controls.Add(this.groupBox8);
            this.tabLacres.Controls.Add(this.btnInserirLacres);
            this.tabLacres.Location = new System.Drawing.Point(4, 22);
            this.tabLacres.Name = "tabLacres";
            this.tabLacres.Padding = new System.Windows.Forms.Padding(3);
            this.tabLacres.Size = new System.Drawing.Size(812, 352);
            this.tabLacres.TabIndex = 11;
            this.tabLacres.Text = "Lacres";
            this.tabLacres.UseVisualStyleBackColor = true;
            // 
            // tbLacreAnterior
            // 
            this.tbLacreAnterior.Location = new System.Drawing.Point(579, 55);
            this.tbLacreAnterior.Name = "tbLacreAnterior";
            this.tbLacreAnterior.Size = new System.Drawing.Size(100, 20);
            this.tbLacreAnterior.TabIndex = 66;
            // 
            // tbMarcaInicialLacre
            // 
            this.tbMarcaInicialLacre.Location = new System.Drawing.Point(380, 55);
            this.tbMarcaInicialLacre.Name = "tbMarcaInicialLacre";
            this.tbMarcaInicialLacre.Size = new System.Drawing.Size(100, 20);
            this.tbMarcaInicialLacre.TabIndex = 65;
            // 
            // lbl_LacreAnterior
            // 
            this.lbl_LacreAnterior.AutoSize = true;
            this.lbl_LacreAnterior.Location = new System.Drawing.Point(501, 58);
            this.lbl_LacreAnterior.Name = "lbl_LacreAnterior";
            this.lbl_LacreAnterior.Size = new System.Drawing.Size(76, 13);
            this.lbl_LacreAnterior.TabIndex = 64;
            this.lbl_LacreAnterior.Text = "Lacre Anterior:";
            // 
            // lblMarcaInicialLacre
            // 
            this.lblMarcaInicialLacre.AutoSize = true;
            this.lblMarcaInicialLacre.Location = new System.Drawing.Point(300, 58);
            this.lblMarcaInicialLacre.Name = "lblMarcaInicialLacre";
            this.lblMarcaInicialLacre.Size = new System.Drawing.Size(70, 13);
            this.lblMarcaInicialLacre.TabIndex = 63;
            this.lblMarcaInicialLacre.Text = "Marca Inicial:";
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToOrderColumns = true;
            this.dataGridView2.AutoGenerateColumns = false;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.lacre_anterior});
            this.dataGridView2.DataSource = this.bdsSelectEquipamentoX;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView2.DefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridView2.Location = new System.Drawing.Point(6, 81);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.Size = new System.Drawing.Size(800, 265);
            this.dataGridView2.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "NUM_SERIE";
            this.dataGridViewTextBoxColumn1.HeaderText = "Número de Série";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 120;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "NUM_INMETRO";
            this.dataGridViewTextBoxColumn2.HeaderText = "Número INMETRO";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 120;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "NUM_ETIQUETA";
            this.dataGridViewTextBoxColumn3.HeaderText = "Número Etiqueta";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 120;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "ID";
            this.dataGridViewTextBoxColumn6.HeaderText = "ID";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Visible = false;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "MARCA_INSTRUMENTO";
            this.dataGridViewTextBoxColumn7.HeaderText = "Marca";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 120;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "marca_inicial";
            this.dataGridViewTextBoxColumn8.HeaderText = "Marca Inicial";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // lacre_anterior
            // 
            this.lacre_anterior.DataPropertyName = "lacre_anterior";
            this.lacre_anterior.HeaderText = "Lacre Anterior";
            this.lacre_anterior.Name = "lacre_anterior";
            this.lacre_anterior.ReadOnly = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.cmbNumInmetroLacres);
            this.groupBox9.Controls.Add(this.label67);
            this.groupBox9.Location = new System.Drawing.Point(299, 6);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(240, 45);
            this.groupBox9.TabIndex = 1;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = " INMETRO ";
            // 
            // cmbNumInmetroLacres
            // 
            this.cmbNumInmetroLacres.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbNumInmetroLacres.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbNumInmetroLacres.DataSource = this.bdsINMETROLacresX;
            this.cmbNumInmetroLacres.DisplayMember = "NUM_INMETRO";
            this.cmbNumInmetroLacres.FormattingEnabled = true;
            this.cmbNumInmetroLacres.Location = new System.Drawing.Point(122, 16);
            this.cmbNumInmetroLacres.Name = "cmbNumInmetroLacres";
            this.cmbNumInmetroLacres.Size = new System.Drawing.Size(110, 21);
            this.cmbNumInmetroLacres.TabIndex = 0;
            this.cmbNumInmetroLacres.ValueMember = "ID";
            // 
            // bdsINMETROLacresX
            // 
            this.bdsINMETROLacresX.DataMember = "OsIbametroInmetroOsCliente";
            this.bdsINMETROLacresX.DataSource = this.dtsPrincipal;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(6, 20);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(115, 13);
            this.label67.TabIndex = 65;
            this.label67.Text = "Número do INMETRO:";
            // 
            // cmbCodigoINMETROLacres
            // 
            this.cmbCodigoINMETROLacres.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCodigoINMETROLacres.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCodigoINMETROLacres.DataSource = this.bdsINMETROLacresX;
            this.cmbCodigoINMETROLacres.DisplayMember = "ID";
            this.cmbCodigoINMETROLacres.FormattingEnabled = true;
            this.cmbCodigoINMETROLacres.Location = new System.Drawing.Point(462, 103);
            this.cmbCodigoINMETROLacres.Name = "cmbCodigoINMETROLacres";
            this.cmbCodigoINMETROLacres.Size = new System.Drawing.Size(194, 21);
            this.cmbCodigoINMETROLacres.TabIndex = 62;
            this.cmbCodigoINMETROLacres.ValueMember = "ID";
            // 
            // cmbCodigoEquipamentoLacres
            // 
            this.cmbCodigoEquipamentoLacres.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCodigoEquipamentoLacres.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCodigoEquipamentoLacres.DataSource = this.bdsEquipamentoLacreX;
            this.cmbCodigoEquipamentoLacres.DisplayMember = "ID";
            this.cmbCodigoEquipamentoLacres.FormattingEnabled = true;
            this.cmbCodigoEquipamentoLacres.Location = new System.Drawing.Point(282, 103);
            this.cmbCodigoEquipamentoLacres.Name = "cmbCodigoEquipamentoLacres";
            this.cmbCodigoEquipamentoLacres.Size = new System.Drawing.Size(174, 21);
            this.cmbCodigoEquipamentoLacres.TabIndex = 61;
            this.cmbCodigoEquipamentoLacres.ValueMember = "ID";
            this.cmbCodigoEquipamentoLacres.SelectedIndexChanged += new System.EventHandler(this.cmbCodigoEquipamentoLacres_SelectedIndexChanged);
            // 
            // bdsEquipamentoLacreX
            // 
            this.bdsEquipamentoLacreX.DataMember = "OsIbametroOsCliente";
            this.bdsEquipamentoLacreX.DataSource = this.dtsPrincipal;
            // 
            // cmbCodigoLacres
            // 
            this.cmbCodigoLacres.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCodigoLacres.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCodigoLacres.DataSource = this.bdsLacre;
            this.cmbCodigoLacres.DisplayMember = "ID";
            this.cmbCodigoLacres.FormattingEnabled = true;
            this.cmbCodigoLacres.Location = new System.Drawing.Point(97, 103);
            this.cmbCodigoLacres.Name = "cmbCodigoLacres";
            this.cmbCodigoLacres.Size = new System.Drawing.Size(179, 21);
            this.cmbCodigoLacres.TabIndex = 47;
            this.cmbCodigoLacres.ValueMember = "ID";
            // 
            // bdsLacre
            // 
            this.bdsLacre.DataMember = "OsEtiqueta";
            this.bdsLacre.DataSource = this.dtsPrincipal;
            this.bdsLacre.Filter = "UTILIZADO = 0 AND TIPO = \'L\'";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.cmbNumeroEtiquetaLacres);
            this.groupBox7.Controls.Add(this.label54);
            this.groupBox7.Location = new System.Drawing.Point(576, 6);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(230, 45);
            this.groupBox7.TabIndex = 2;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = " Lacres ";
            // 
            // cmbNumeroEtiquetaLacres
            // 
            this.cmbNumeroEtiquetaLacres.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbNumeroEtiquetaLacres.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbNumeroEtiquetaLacres.DataSource = this.bdsLacre;
            this.cmbNumeroEtiquetaLacres.DisplayMember = "NUM_ETIQUETA";
            this.cmbNumeroEtiquetaLacres.FormattingEnabled = true;
            this.cmbNumeroEtiquetaLacres.Location = new System.Drawing.Point(115, 16);
            this.cmbNumeroEtiquetaLacres.Name = "cmbNumeroEtiquetaLacres";
            this.cmbNumeroEtiquetaLacres.Size = new System.Drawing.Size(110, 21);
            this.cmbNumeroEtiquetaLacres.TabIndex = 0;
            this.cmbNumeroEtiquetaLacres.ValueMember = "ID";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(7, 19);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(104, 13);
            this.label54.TabIndex = 48;
            this.label54.Text = "Número da Etiqueta:";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.groupBox4);
            this.groupBox8.Controls.Add(this.textBox3);
            this.groupBox8.Controls.Add(this.cmbNumSerieLacres);
            this.groupBox8.Controls.Add(this.label66);
            this.groupBox8.Controls.Add(this.label68);
            this.groupBox8.Location = new System.Drawing.Point(6, 6);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(250, 69);
            this.groupBox8.TabIndex = 0;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = " Bombas ";
            // 
            // groupBox4
            // 
            this.groupBox4.Location = new System.Drawing.Point(250, 5);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(200, 100);
            this.groupBox4.TabIndex = 66;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "groupBox4";
            // 
            // textBox3
            // 
            this.textBox3.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bdsEquipamentoLacreX, "MARCA_INSTRUMENTO", true));
            this.textBox3.Location = new System.Drawing.Point(134, 43);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(110, 20);
            this.textBox3.TabIndex = 1;
            // 
            // cmbNumSerieLacres
            // 
            this.cmbNumSerieLacres.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbNumSerieLacres.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbNumSerieLacres.DataSource = this.bdsEquipamentoLacreX;
            this.cmbNumSerieLacres.DisplayMember = "NUM_SERIE";
            this.cmbNumSerieLacres.FormattingEnabled = true;
            this.cmbNumSerieLacres.Location = new System.Drawing.Point(134, 16);
            this.cmbNumSerieLacres.Name = "cmbNumSerieLacres";
            this.cmbNumSerieLacres.Size = new System.Drawing.Size(110, 21);
            this.cmbNumSerieLacres.TabIndex = 0;
            this.cmbNumSerieLacres.ValueMember = "ID";
            this.cmbNumSerieLacres.SelectionChangeCommitted += new System.EventHandler(this.cmbNumSerieLacres_SelectionChangeCommitted);
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(42, 19);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(89, 13);
            this.label66.TabIndex = 62;
            this.label66.Text = "Número de Série:";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(11, 46);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(120, 13);
            this.label68.TabIndex = 60;
            this.label68.Text = "Marca do Equipamento:";
            // 
            // btnInserirLacres
            // 
            this.btnInserirLacres.Location = new System.Drawing.Point(730, 53);
            this.btnInserirLacres.Name = "btnInserirLacres";
            this.btnInserirLacres.Size = new System.Drawing.Size(75, 23);
            this.btnInserirLacres.TabIndex = 3;
            this.btnInserirLacres.Text = "Inserir";
            this.btnInserirLacres.UseVisualStyleBackColor = true;
            this.btnInserirLacres.Click += new System.EventHandler(this.btnInserirLacres_Click);
            // 
            // bdsINMETROLacres
            // 
            this.bdsINMETROLacres.DataMember = "OsIbametroInmetro";
            this.bdsINMETROLacres.DataSource = this.dtsPrincipal;
            // 
            // bdsEquipamentoLacre
            // 
            this.bdsEquipamentoLacre.DataMember = "OsIbametro";
            this.bdsEquipamentoLacre.DataSource = this.dtsPrincipal;
            // 
            // txtTotalGeralPecas
            // 
            this.txtTotalGeralPecas.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "TOTAL_PECAS_OS", true));
            this.txtTotalGeralPecas.Enabled = false;
            this.txtTotalGeralPecas.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalGeralPecas.Location = new System.Drawing.Point(711, 14);
            this.txtTotalGeralPecas.Name = "txtTotalGeralPecas";
            this.txtTotalGeralPecas.ReadOnly = true;
            this.txtTotalGeralPecas.Size = new System.Drawing.Size(100, 23);
            this.txtTotalGeralPecas.TabIndex = 31;
            this.txtTotalGeralPecas.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTotalServicosOS
            // 
            this.txtTotalServicosOS.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "TOTAL_GERAL_SERVICOS", true));
            this.txtTotalServicosOS.Enabled = false;
            this.txtTotalServicosOS.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalServicosOS.Location = new System.Drawing.Point(710, 43);
            this.txtTotalServicosOS.Name = "txtTotalServicosOS";
            this.txtTotalServicosOS.ReadOnly = true;
            this.txtTotalServicosOS.Size = new System.Drawing.Size(100, 23);
            this.txtTotalServicosOS.TabIndex = 0;
            this.txtTotalServicosOS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTotalFinalOS
            // 
            this.txtTotalFinalOS.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "TOTAL_OS", true));
            this.txtTotalFinalOS.Enabled = false;
            this.txtTotalFinalOS.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalFinalOS.Location = new System.Drawing.Point(710, 71);
            this.txtTotalFinalOS.Name = "txtTotalFinalOS";
            this.txtTotalFinalOS.ReadOnly = true;
            this.txtTotalFinalOS.Size = new System.Drawing.Size(100, 23);
            this.txtTotalFinalOS.TabIndex = 33;
            this.txtTotalFinalOS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTotalPago
            // 
            this.txtTotalPago.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "VALOR_PAGO", true));
            this.txtTotalPago.Enabled = false;
            this.txtTotalPago.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalPago.Location = new System.Drawing.Point(710, 100);
            this.txtTotalPago.Name = "txtTotalPago";
            this.txtTotalPago.ReadOnly = true;
            this.txtTotalPago.Size = new System.Drawing.Size(100, 21);
            this.txtTotalPago.TabIndex = 49;
            this.txtTotalPago.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtValorAPagar
            // 
            this.txtValorAPagar.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "VALOR_RESTANTE", true));
            this.txtValorAPagar.Enabled = false;
            this.txtValorAPagar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorAPagar.Location = new System.Drawing.Point(710, 127);
            this.txtValorAPagar.Name = "txtValorAPagar";
            this.txtValorAPagar.ReadOnly = true;
            this.txtValorAPagar.Size = new System.Drawing.Size(100, 21);
            this.txtValorAPagar.TabIndex = 47;
            this.txtValorAPagar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // bdsFiltroCliente
            // 
            this.bdsFiltroCliente.DataMember = "A30CLIENTES";
            this.bdsFiltroCliente.DataSource = this.dtsPrincipal;
            this.bdsFiltroCliente.Sort = "NOME";
            // 
            // bdsFiltroUsuario
            // 
            this.bdsFiltroUsuario.DataMember = "OsUsuario";
            this.bdsFiltroUsuario.DataSource = this.dtsPrincipal;
            this.bdsFiltroUsuario.Sort = "NOME";
            // 
            // pnlConsultar
            // 
            this.pnlConsultar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlConsultar.Controls.Add(this.label8);
            this.pnlConsultar.Controls.Add(this.pictureBox1);
            this.pnlConsultar.Controls.Add(this.label70);
            this.pnlConsultar.Controls.Add(this.lblTotalOS);
            this.pnlConsultar.Controls.Add(this.groupBox2);
            this.pnlConsultar.Controls.Add(this.osOrdemServicoDataGridView);
            this.pnlConsultar.Controls.Add(this.comboBox8);
            this.pnlConsultar.Location = new System.Drawing.Point(780, 150);
            this.pnlConsultar.Name = "pnlConsultar";
            this.pnlConsultar.Size = new System.Drawing.Size(813, 510);
            this.pnlConsultar.TabIndex = 10;
            this.pnlConsultar.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(3, 205);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(361, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "Clique em \'S\' ou dois cliques do mouse para selecionar a Ordem de Serviço";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::osExpress.Properties.Resources.search;
            this.pictureBox1.Location = new System.Drawing.Point(336, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(51, 50);
            this.pictureBox1.TabIndex = 16;
            this.pictureBox1.TabStop = false;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.Location = new System.Drawing.Point(385, 15);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(122, 31);
            this.label70.TabIndex = 15;
            this.label70.Text = "Consulta";
            // 
            // lblTotalOS
            // 
            this.lblTotalOS.AutoSize = true;
            this.lblTotalOS.Location = new System.Drawing.Point(3, 487);
            this.lblTotalOS.Name = "lblTotalOS";
            this.lblTotalOS.Size = new System.Drawing.Size(37, 13);
            this.lblTotalOS.TabIndex = 13;
            this.lblTotalOS.Text = "Total: ";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkPostoOS);
            this.groupBox2.Controls.Add(this.labelPostoOS);
            this.groupBox2.Controls.Add(this.cmbPostoOS);
            this.groupBox2.Controls.Add(this.label50);
            this.groupBox2.Controls.Add(this.lblFiltroPeriodoDe);
            this.groupBox2.Controls.Add(this.dtp_ate);
            this.groupBox2.Controls.Add(this.dtp_de);
            this.groupBox2.Controls.Add(this.chkFiltrarPeriodo);
            this.groupBox2.Controls.Add(this.cmbFiltroClienteCodigo);
            this.groupBox2.Controls.Add(this.cmbFiltroUsuarioCodigo);
            this.groupBox2.Controls.Add(this.chkFiltrarCliente);
            this.groupBox2.Controls.Add(this.chkFiltrarUsuario);
            this.groupBox2.Controls.Add(this.chkFiltrarSituacao);
            this.groupBox2.Controls.Add(this.label65);
            this.groupBox2.Controls.Add(this.label64);
            this.groupBox2.Controls.Add(this.label62);
            this.groupBox2.Controls.Add(this.cmbFiltroSituacao);
            this.groupBox2.Controls.Add(this.cmbFiltroClienteNome);
            this.groupBox2.Controls.Add(this.cmbFiltroUsuarioNome);
            this.groupBox2.Location = new System.Drawing.Point(6, 52);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(784, 150);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = " Filtros ";
            // 
            // chkPostoOS
            // 
            this.chkPostoOS.AutoSize = true;
            this.chkPostoOS.Location = new System.Drawing.Point(390, 113);
            this.chkPostoOS.Name = "chkPostoOS";
            this.chkPostoOS.Size = new System.Drawing.Size(51, 17);
            this.chkPostoOS.TabIndex = 49;
            this.chkPostoOS.Text = "Filtrar";
            this.chkPostoOS.UseVisualStyleBackColor = true;
            this.chkPostoOS.CheckStateChanged += new System.EventHandler(this.chkFiltrarSelecoes_CheckedChanged);
            // 
            // labelPostoOS
            // 
            this.labelPostoOS.AutoSize = true;
            this.labelPostoOS.Location = new System.Drawing.Point(43, 115);
            this.labelPostoOS.Name = "labelPostoOS";
            this.labelPostoOS.Size = new System.Drawing.Size(51, 13);
            this.labelPostoOS.TabIndex = 48;
            this.labelPostoOS.Text = "Empresa:";
            // 
            // cmbPostoOS
            // 
            this.cmbPostoOS.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbPostoOS.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbPostoOS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPostoOS.FormattingEnabled = true;
            this.cmbPostoOS.Location = new System.Drawing.Point(100, 111);
            this.cmbPostoOS.Name = "cmbPostoOS";
            this.cmbPostoOS.Size = new System.Drawing.Size(280, 21);
            this.cmbPostoOS.TabIndex = 47;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(640, 50);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(31, 13);
            this.label50.TabIndex = 18;
            this.label50.Text = "ATÉ";
            // 
            // lblFiltroPeriodoDe
            // 
            this.lblFiltroPeriodoDe.AutoSize = true;
            this.lblFiltroPeriodoDe.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFiltroPeriodoDe.Location = new System.Drawing.Point(640, 20);
            this.lblFiltroPeriodoDe.Name = "lblFiltroPeriodoDe";
            this.lblFiltroPeriodoDe.Size = new System.Drawing.Size(24, 13);
            this.lblFiltroPeriodoDe.TabIndex = 17;
            this.lblFiltroPeriodoDe.Text = "DE";
            // 
            // dtp_ate
            // 
            this.dtp_ate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_ate.Location = new System.Drawing.Point(673, 46);
            this.dtp_ate.Name = "dtp_ate";
            this.dtp_ate.Size = new System.Drawing.Size(100, 20);
            this.dtp_ate.TabIndex = 14;
            // 
            // dtp_de
            // 
            this.dtp_de.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_de.Location = new System.Drawing.Point(673, 17);
            this.dtp_de.Name = "dtp_de";
            this.dtp_de.Size = new System.Drawing.Size(100, 20);
            this.dtp_de.TabIndex = 13;
            // 
            // chkFiltrarPeriodo
            // 
            this.chkFiltrarPeriodo.AutoSize = true;
            this.chkFiltrarPeriodo.Location = new System.Drawing.Point(572, 18);
            this.chkFiltrarPeriodo.Name = "chkFiltrarPeriodo";
            this.chkFiltrarPeriodo.Size = new System.Drawing.Size(51, 17);
            this.chkFiltrarPeriodo.TabIndex = 12;
            this.chkFiltrarPeriodo.Text = "Filtrar";
            this.chkFiltrarPeriodo.UseVisualStyleBackColor = true;
            this.chkFiltrarPeriodo.CheckedChanged += new System.EventHandler(this.chkFiltrarSelecoes_CheckedChanged);
            // 
            // cmbFiltroClienteCodigo
            // 
            this.cmbFiltroClienteCodigo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbFiltroClienteCodigo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbFiltroClienteCodigo.DataSource = this.bdsFiltroCliente;
            this.cmbFiltroClienteCodigo.DisplayMember = "Cliente";
            this.cmbFiltroClienteCodigo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFiltroClienteCodigo.FormattingEnabled = true;
            this.cmbFiltroClienteCodigo.Location = new System.Drawing.Point(100, 71);
            this.cmbFiltroClienteCodigo.Name = "cmbFiltroClienteCodigo";
            this.cmbFiltroClienteCodigo.Size = new System.Drawing.Size(106, 21);
            this.cmbFiltroClienteCodigo.TabIndex = 11;
            this.cmbFiltroClienteCodigo.ValueMember = "ID";
            // 
            // cmbFiltroUsuarioCodigo
            // 
            this.cmbFiltroUsuarioCodigo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbFiltroUsuarioCodigo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbFiltroUsuarioCodigo.DataSource = this.bdsFiltroXOSFuncionario;
            this.cmbFiltroUsuarioCodigo.DisplayMember = "idusuario";
            this.cmbFiltroUsuarioCodigo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFiltroUsuarioCodigo.FormattingEnabled = true;
            this.cmbFiltroUsuarioCodigo.Location = new System.Drawing.Point(100, 44);
            this.cmbFiltroUsuarioCodigo.Name = "cmbFiltroUsuarioCodigo";
            this.cmbFiltroUsuarioCodigo.Size = new System.Drawing.Size(106, 21);
            this.cmbFiltroUsuarioCodigo.TabIndex = 10;
            this.cmbFiltroUsuarioCodigo.ValueMember = "idusuario";
            // 
            // bdsFiltroXOSFuncionario
            // 
            this.bdsFiltroXOSFuncionario.DataMember = "XOsFuncionario";
            this.bdsFiltroXOSFuncionario.DataSource = this.dtsPrincipal;
            this.bdsFiltroXOSFuncionario.Filter = "isusuario = \'S\'";
            this.bdsFiltroXOSFuncionario.Sort = "uNome";
            // 
            // chkFiltrarCliente
            // 
            this.chkFiltrarCliente.AutoSize = true;
            this.chkFiltrarCliente.Location = new System.Drawing.Point(572, 73);
            this.chkFiltrarCliente.Name = "chkFiltrarCliente";
            this.chkFiltrarCliente.Size = new System.Drawing.Size(51, 17);
            this.chkFiltrarCliente.TabIndex = 9;
            this.chkFiltrarCliente.Text = "Filtrar";
            this.chkFiltrarCliente.UseVisualStyleBackColor = true;
            this.chkFiltrarCliente.CheckedChanged += new System.EventHandler(this.chkFiltrarSelecoes_CheckedChanged);
            // 
            // chkFiltrarUsuario
            // 
            this.chkFiltrarUsuario.AutoSize = true;
            this.chkFiltrarUsuario.Location = new System.Drawing.Point(572, 46);
            this.chkFiltrarUsuario.Name = "chkFiltrarUsuario";
            this.chkFiltrarUsuario.Size = new System.Drawing.Size(51, 17);
            this.chkFiltrarUsuario.TabIndex = 8;
            this.chkFiltrarUsuario.Text = "Filtrar";
            this.chkFiltrarUsuario.UseVisualStyleBackColor = true;
            this.chkFiltrarUsuario.CheckedChanged += new System.EventHandler(this.chkFiltrarSelecoes_CheckedChanged);
            // 
            // chkFiltrarSituacao
            // 
            this.chkFiltrarSituacao.AutoSize = true;
            this.chkFiltrarSituacao.Location = new System.Drawing.Point(212, 22);
            this.chkFiltrarSituacao.Name = "chkFiltrarSituacao";
            this.chkFiltrarSituacao.Size = new System.Drawing.Size(51, 17);
            this.chkFiltrarSituacao.TabIndex = 7;
            this.chkFiltrarSituacao.Text = "Filtrar";
            this.chkFiltrarSituacao.UseVisualStyleBackColor = true;
            this.chkFiltrarSituacao.CheckedChanged += new System.EventHandler(this.chkFiltrarSelecoes_CheckedChanged);
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(9, 23);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(85, 13);
            this.label65.TabIndex = 6;
            this.label65.Text = "Situação da OS:";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(52, 75);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(42, 13);
            this.label64.TabIndex = 5;
            this.label64.Text = "Cliente:";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(48, 52);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(46, 13);
            this.label62.TabIndex = 4;
            this.label62.Text = "Usuário:";
            // 
            // cmbFiltroSituacao
            // 
            this.cmbFiltroSituacao.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbFiltroSituacao.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbFiltroSituacao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFiltroSituacao.FormattingEnabled = true;
            this.cmbFiltroSituacao.Items.AddRange(new object[] {
            "A - Aberta",
            "F - Fechada",
            "D - Descartada"});
            this.cmbFiltroSituacao.Location = new System.Drawing.Point(100, 17);
            this.cmbFiltroSituacao.Name = "cmbFiltroSituacao";
            this.cmbFiltroSituacao.Size = new System.Drawing.Size(106, 21);
            this.cmbFiltroSituacao.TabIndex = 3;
            // 
            // cmbFiltroClienteNome
            // 
            this.cmbFiltroClienteNome.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbFiltroClienteNome.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbFiltroClienteNome.DataSource = this.bdsFiltroCliente;
            this.cmbFiltroClienteNome.DisplayMember = "Nome";
            this.cmbFiltroClienteNome.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFiltroClienteNome.FormattingEnabled = true;
            this.cmbFiltroClienteNome.Location = new System.Drawing.Point(212, 71);
            this.cmbFiltroClienteNome.Name = "cmbFiltroClienteNome";
            this.cmbFiltroClienteNome.Size = new System.Drawing.Size(354, 21);
            this.cmbFiltroClienteNome.TabIndex = 1;
            this.cmbFiltroClienteNome.ValueMember = "ID";
            // 
            // cmbFiltroUsuarioNome
            // 
            this.cmbFiltroUsuarioNome.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbFiltroUsuarioNome.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbFiltroUsuarioNome.DataSource = this.bdsFiltroXOSFuncionario;
            this.cmbFiltroUsuarioNome.DisplayMember = "uNome";
            this.cmbFiltroUsuarioNome.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFiltroUsuarioNome.FormattingEnabled = true;
            this.cmbFiltroUsuarioNome.Location = new System.Drawing.Point(212, 44);
            this.cmbFiltroUsuarioNome.Name = "cmbFiltroUsuarioNome";
            this.cmbFiltroUsuarioNome.Size = new System.Drawing.Size(354, 21);
            this.cmbFiltroUsuarioNome.TabIndex = 2;
            this.cmbFiltroUsuarioNome.ValueMember = "idusuario";
            // 
            // osOrdemServicoDataGridView
            // 
            this.osOrdemServicoDataGridView.AllowUserToAddRows = false;
            this.osOrdemServicoDataGridView.AllowUserToDeleteRows = false;
            this.osOrdemServicoDataGridView.AllowUserToOrderColumns = true;
            this.osOrdemServicoDataGridView.AutoGenerateColumns = false;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.osOrdemServicoDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.osOrdemServicoDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.osOrdemServicoDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.IDPOSTO,
            this.iDUSUARIODataGridViewTextBoxColumn,
            this.Usuario,
            this.iDCLIENTEDataGridViewTextBoxColumn,
            this.Cliente,
            this.dTCADASTRODataGridViewTextBoxColumn,
            this.dTFECHAMENTODataGridViewTextBoxColumn,
            this.oBSDataGridViewTextBoxColumn});
            this.osOrdemServicoDataGridView.DataSource = this.osOrdemServicoBindingSource;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.osOrdemServicoDataGridView.DefaultCellStyle = dataGridViewCellStyle18;
            this.osOrdemServicoDataGridView.Location = new System.Drawing.Point(6, 224);
            this.osOrdemServicoDataGridView.Name = "osOrdemServicoDataGridView";
            this.osOrdemServicoDataGridView.ReadOnly = true;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.osOrdemServicoDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle19;
            this.osOrdemServicoDataGridView.Size = new System.Drawing.Size(785, 306);
            this.osOrdemServicoDataGridView.TabIndex = 1;
            this.osOrdemServicoDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.osOrdemServicoDataGridView_CellClick);
            this.osOrdemServicoDataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.osOrdemServicoDataGridView_CellDoubleClick);
            this.osOrdemServicoDataGridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.osOrdemServicoDataGridView_KeyDown);
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID_FINAL";
            this.iDDataGridViewTextBoxColumn.HeaderText = "Os";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.ReadOnly = true;
            this.iDDataGridViewTextBoxColumn.Width = 50;
            // 
            // IDPOSTO
            // 
            this.IDPOSTO.DataPropertyName = "IDPOSTO";
            this.IDPOSTO.HeaderText = "Empresa";
            this.IDPOSTO.Name = "IDPOSTO";
            this.IDPOSTO.ReadOnly = true;
            this.IDPOSTO.Width = 50;
            // 
            // iDUSUARIODataGridViewTextBoxColumn
            // 
            this.iDUSUARIODataGridViewTextBoxColumn.DataPropertyName = "IDUSUARIO";
            this.iDUSUARIODataGridViewTextBoxColumn.HeaderText = "Usuário";
            this.iDUSUARIODataGridViewTextBoxColumn.Name = "iDUSUARIODataGridViewTextBoxColumn";
            this.iDUSUARIODataGridViewTextBoxColumn.ReadOnly = true;
            this.iDUSUARIODataGridViewTextBoxColumn.Width = 60;
            // 
            // Usuario
            // 
            this.Usuario.DataPropertyName = "NOME_USUARIO";
            this.Usuario.HeaderText = "Nome Usuário";
            this.Usuario.Name = "Usuario";
            this.Usuario.ReadOnly = true;
            // 
            // iDCLIENTEDataGridViewTextBoxColumn
            // 
            this.iDCLIENTEDataGridViewTextBoxColumn.DataPropertyName = "IDCLIENTE";
            this.iDCLIENTEDataGridViewTextBoxColumn.HeaderText = "Cliente";
            this.iDCLIENTEDataGridViewTextBoxColumn.Name = "iDCLIENTEDataGridViewTextBoxColumn";
            this.iDCLIENTEDataGridViewTextBoxColumn.ReadOnly = true;
            this.iDCLIENTEDataGridViewTextBoxColumn.Width = 60;
            // 
            // Cliente
            // 
            this.Cliente.DataPropertyName = "Nome";
            this.Cliente.HeaderText = "Nome Cliente";
            this.Cliente.Name = "Cliente";
            this.Cliente.ReadOnly = true;
            // 
            // dTCADASTRODataGridViewTextBoxColumn
            // 
            this.dTCADASTRODataGridViewTextBoxColumn.DataPropertyName = "DT_CADASTRO";
            this.dTCADASTRODataGridViewTextBoxColumn.HeaderText = "Dt. Cadastro";
            this.dTCADASTRODataGridViewTextBoxColumn.Name = "dTCADASTRODataGridViewTextBoxColumn";
            this.dTCADASTRODataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dTFECHAMENTODataGridViewTextBoxColumn
            // 
            this.dTFECHAMENTODataGridViewTextBoxColumn.DataPropertyName = "DT_FECHAMENTO";
            this.dTFECHAMENTODataGridViewTextBoxColumn.HeaderText = "Dt. Fechamento";
            this.dTFECHAMENTODataGridViewTextBoxColumn.Name = "dTFECHAMENTODataGridViewTextBoxColumn";
            this.dTFECHAMENTODataGridViewTextBoxColumn.ReadOnly = true;
            this.dTFECHAMENTODataGridViewTextBoxColumn.Width = 110;
            // 
            // oBSDataGridViewTextBoxColumn
            // 
            this.oBSDataGridViewTextBoxColumn.DataPropertyName = "OBS";
            this.oBSDataGridViewTextBoxColumn.HeaderText = "Observação";
            this.oBSDataGridViewTextBoxColumn.Name = "oBSDataGridViewTextBoxColumn";
            this.oBSDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // comboBox8
            // 
            this.comboBox8.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox8.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox8.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osOrdemServicoBindingSource, "IDCLIENTE", true));
            this.comboBox8.DataSource = this.bdsClientes;
            this.comboBox8.DisplayMember = "Cliente";
            this.comboBox8.FormattingEnabled = true;
            this.comboBox8.Location = new System.Drawing.Point(1662, 203);
            this.comboBox8.Name = "comboBox8";
            this.comboBox8.Size = new System.Drawing.Size(91, 21);
            this.comboBox8.TabIndex = 14;
            this.comboBox8.ValueMember = "ID";
            // 
            // bdsViewOS
            // 
            this.bdsViewOS.AllowNew = false;
            this.bdsViewOS.DataMember = "OsOrdemServico";
            this.bdsViewOS.DataSource = this.dtsPrincipal;
            // 
            // osOrdemServicoTableAdapter
            // 
            this.osOrdemServicoTableAdapter.ClearBeforeFill = true;
            // 
            // osItemServicoTableAdapter
            // 
            this.osItemServicoTableAdapter.ClearBeforeFill = true;
            // 
            // osSituacoesOSTableAdapter
            // 
            this.osSituacoesOSTableAdapter.ClearBeforeFill = true;
            // 
            // a30CLIENTESTableAdapter
            // 
            this.a30CLIENTESTableAdapter.ClearBeforeFill = true;
            // 
            // osParSituacaoTableAdapter
            // 
            this.osParSituacaoTableAdapter.ClearBeforeFill = true;
            // 
            // a30POSTOSTableAdapter
            // 
            this.a30POSTOSTableAdapter.ClearBeforeFill = true;
            // 
            // a30PRODUTOTableAdapter
            // 
            this.a30PRODUTOTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.A30CAIXA_PROG_EXTableAdapter = null;
            this.tableAdapterManager.A30CAIXA_PROGTableAdapter = null;
            this.tableAdapterManager.A30CLIENTESTableAdapter = null;
            this.tableAdapterManager.A30FUNCIONARIOTableAdapter = null;
            this.tableAdapterManager.A30PLANO_CONTATableAdapter = null;
            this.tableAdapterManager.A30POSTOSTableAdapter = null;
            this.tableAdapterManager.A30PRODUTOTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.OsClienteTableAdapter = null;
            this.tableAdapterManager.OsEtiquetaOsClienteTableAdapter = null;
            this.tableAdapterManager.OsEtiquetaTableAdapter = null;
            this.tableAdapterManager.OsFlashDriveTableAdapter = null;
            this.tableAdapterManager.OsFormasPagamentoTableAdapter = this.osFormasPagamentoTableAdapter;
            this.tableAdapterManager.OsIbametroInmetroOsClienteTableAdapter = null;
            this.tableAdapterManager.OsIbametroInmetroTableAdapter = null;
            this.tableAdapterManager.OsIbametroOsCliente1TableAdapter = null;
            this.tableAdapterManager.OsIbametroOsClienteTableAdapter = null;
            this.tableAdapterManager.OsIbametroTableAdapter = null;
            this.tableAdapterManager.OsInstrumentoServicoOsClienteTableAdapter = this.osInstrumentoServicoOsClienteTableAdapter;
            this.tableAdapterManager.OsInstrumentoServicoTableAdapter = this.osInstrumentoServicoTableAdapter;
            this.tableAdapterManager.OsItemServicoTableAdapter = this.osItemServicoTableAdapter;
            this.tableAdapterManager.OsOrdemParcelaTableAdapter = null;
            this.tableAdapterManager.OsOrdemServicoTableAdapter = this.osOrdemServicoTableAdapter;
            this.tableAdapterManager.OsParametrosTableAdapter = null;
            this.tableAdapterManager.OsParContasTableAdapter = null;
            this.tableAdapterManager.OsParSituacaoTableAdapter = null;
            this.tableAdapterManager.OsProdutosServicosTableAdapter = null;
            this.tableAdapterManager.OsReciboTableAdapter = this.osReciboTableAdapter;
            this.tableAdapterManager.OsSituacoesOSTableAdapter = this.osSituacoesOSTableAdapter;
            this.tableAdapterManager.OsUsuarioTableAdapter = null;
            this.tableAdapterManager.OsVersaoTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = osExpress.OsExpressDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.UpdateInsertDelete;
            this.tableAdapterManager.XOsFuncionarioTableAdapter = null;
            // 
            // osFormasPagamentoTableAdapter
            // 
            this.osFormasPagamentoTableAdapter.ClearBeforeFill = true;
            // 
            // osInstrumentoServicoOsClienteTableAdapter
            // 
            this.osInstrumentoServicoOsClienteTableAdapter.ClearBeforeFill = true;
            // 
            // osInstrumentoServicoTableAdapter
            // 
            this.osInstrumentoServicoTableAdapter.ClearBeforeFill = true;
            // 
            // osReciboTableAdapter
            // 
            this.osReciboTableAdapter.ClearBeforeFill = true;
            // 
            // osEtiquetaTableAdapter
            // 
            this.osEtiquetaTableAdapter.ClearBeforeFill = true;
            // 
            // osIbametroTableAdapter
            // 
            this.osIbametroTableAdapter.ClearBeforeFill = true;
            // 
            // osParContasTableAdapter
            // 
            this.osParContasTableAdapter.ClearBeforeFill = true;
            // 
            // osUsuarioTableAdapter
            // 
            this.osUsuarioTableAdapter.ClearBeforeFill = true;
            // 
            // osOrdemServicoTableAdapterView
            // 
            this.osOrdemServicoTableAdapterView.ClearBeforeFill = true;
            // 
            // lblCabFatura
            // 
            this.lblCabFatura.AutoSize = true;
            this.lblCabFatura.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "NUMERO_FATURA", true));
            this.lblCabFatura.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCabFatura.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblCabFatura.Location = new System.Drawing.Point(256, 130);
            this.lblCabFatura.Name = "lblCabFatura";
            this.lblCabFatura.Size = new System.Drawing.Size(35, 17);
            this.lblCabFatura.TabIndex = 41;
            this.lblCabFatura.Text = "(...)";
            this.lblCabFatura.Visible = false;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label72.Location = new System.Drawing.Point(196, 130);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(60, 17);
            this.label72.TabIndex = 40;
            this.label72.Text = "Fatura:";
            this.label72.Visible = false;
            // 
            // lblNovoCodOS
            // 
            this.lblNovoCodOS.AutoSize = true;
            this.lblNovoCodOS.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "ID_FINAL", true));
            this.lblNovoCodOS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNovoCodOS.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblNovoCodOS.Location = new System.Drawing.Point(116, 22);
            this.lblNovoCodOS.Name = "lblNovoCodOS";
            this.lblNovoCodOS.Size = new System.Drawing.Size(27, 13);
            this.lblNovoCodOS.TabIndex = 43;
            this.lblNovoCodOS.Text = "(...)";
            this.lblNovoCodOS.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(9, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Ordem de Serviço:";
            // 
            // lblCodigoOS
            // 
            this.lblCodigoOS.AutoSize = true;
            this.lblCodigoOS.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osOrdemServicoBindingSource, "ID", true));
            this.lblCodigoOS.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigoOS.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblCodigoOS.Location = new System.Drawing.Point(273, 219);
            this.lblCodigoOS.Name = "lblCodigoOS";
            this.lblCodigoOS.Size = new System.Drawing.Size(97, 17);
            this.lblCodigoOS.TabIndex = 44;
            this.lblCodigoOS.Text = "lblCodigoOS";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label6.Location = new System.Drawing.Point(504, 18);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(201, 17);
            this.label6.TabIndex = 50;
            this.label6.Text = "Total Geral das Peças (+):";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label7.Location = new System.Drawing.Point(485, 47);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(219, 17);
            this.label7.TabIndex = 51;
            this.label7.Text = "Total Geral dos Serviços (+):";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label13.Location = new System.Drawing.Point(628, 75);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(76, 17);
            this.label13.TabIndex = 52;
            this.label13.Text = "Total (=):";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label14.Location = new System.Drawing.Point(586, 105);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(115, 17);
            this.label14.TabIndex = 53;
            this.label14.Text = "Total Pago (-):";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label71.Location = new System.Drawing.Point(508, 131);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(196, 17);
            this.label71.TabIndex = 54;
            this.label71.Text = "Valor a Pagar (Fiado) (=):";
            // 
            // osIbametroInmetroTableAdapter
            // 
            this.osIbametroInmetroTableAdapter.ClearBeforeFill = true;
            // 
            // osParametrosBindingSource
            // 
            this.osParametrosBindingSource.DataMember = "OsParametros";
            this.osParametrosBindingSource.DataSource = this.dtsPrincipal;
            // 
            // osParametrosTableAdapter
            // 
            this.osParametrosTableAdapter.ClearBeforeFill = true;
            // 
            // lblCidade
            // 
            this.lblCidade.AutoSize = true;
            this.lblCidade.Location = new System.Drawing.Point(170, 19);
            this.lblCidade.Name = "lblCidade";
            this.lblCidade.Size = new System.Drawing.Size(103, 13);
            this.lblCidade.TabIndex = 55;
            this.lblCidade.Text = "Cidade....................:";
            // 
            // lblUF
            // 
            this.lblUF.AutoSize = true;
            this.lblUF.Location = new System.Drawing.Point(173, 43);
            this.lblUF.Name = "lblUF";
            this.lblUF.Size = new System.Drawing.Size(103, 13);
            this.lblUF.TabIndex = 56;
            this.lblUF.Text = "Estado....................:";
            // 
            // lblCidade2
            // 
            this.lblCidade2.AutoSize = true;
            this.lblCidade2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bdsClientes, "Cidade", true));
            this.lblCidade2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCidade2.Location = new System.Drawing.Point(259, 18);
            this.lblCidade2.Name = "lblCidade2";
            this.lblCidade2.Size = new System.Drawing.Size(66, 13);
            this.lblCidade2.TabIndex = 57;
            this.lblCidade2.Text = "lblCidade2";
            // 
            // lblUF2
            // 
            this.lblUF2.AutoSize = true;
            this.lblUF2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bdsClientes, "Estado", true));
            this.lblUF2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUF2.Location = new System.Drawing.Point(264, 43);
            this.lblUF2.Name = "lblUF2";
            this.lblUF2.Size = new System.Drawing.Size(43, 13);
            this.lblUF2.TabIndex = 58;
            this.lblUF2.Text = "lblUF2";
            // 
            // cmbEmpresa
            // 
            this.cmbEmpresa.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbEmpresa.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbEmpresa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEmpresa.FormattingEnabled = true;
            this.cmbEmpresa.Items.AddRange(new object[] {
            "A - Aberta",
            "F - Fechada",
            "D - Descartada"});
            this.cmbEmpresa.Location = new System.Drawing.Point(90, 89);
            this.cmbEmpresa.Name = "cmbEmpresa";
            this.cmbEmpresa.Size = new System.Drawing.Size(280, 21);
            this.cmbEmpresa.TabIndex = 45;
            this.cmbEmpresa.SelectionChangeCommitted += new System.EventHandler(this.cmbEmpresa_SelectionChangeCommitted);
            // 
            // lblEmpresa
            // 
            this.lblEmpresa.AutoSize = true;
            this.lblEmpresa.Location = new System.Drawing.Point(30, 95);
            this.lblEmpresa.Name = "lblEmpresa";
            this.lblEmpresa.Size = new System.Drawing.Size(51, 13);
            this.lblEmpresa.TabIndex = 46;
            this.lblEmpresa.Text = "Empresa:";
            // 
            // chkSolicitante
            // 
            this.chkSolicitante.AutoSize = true;
            this.chkSolicitante.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkSolicitante.Checked = true;
            this.chkSolicitante.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSolicitante.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSolicitante.Location = new System.Drawing.Point(387, 91);
            this.chkSolicitante.Name = "chkSolicitante";
            this.chkSolicitante.Size = new System.Drawing.Size(145, 17);
            this.chkSolicitante.TabIndex = 47;
            this.chkSolicitante.Text = "Solicitante: O mesmo";
            this.chkSolicitante.UseVisualStyleBackColor = true;
            this.chkSolicitante.CheckedChanged += new System.EventHandler(this.chkSolicitante_CheckedChanged);
            // 
            // cmbSolicitante
            // 
            this.cmbSolicitante.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbSolicitante.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbSolicitante.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSolicitante.Enabled = false;
            this.cmbSolicitante.FormattingEnabled = true;
            this.cmbSolicitante.Items.AddRange(new object[] {
            "A - Aberta",
            "F - Fechada",
            "D - Descartada"});
            this.cmbSolicitante.Location = new System.Drawing.Point(550, 90);
            this.cmbSolicitante.Name = "cmbSolicitante";
            this.cmbSolicitante.Size = new System.Drawing.Size(280, 21);
            this.cmbSolicitante.TabIndex = 48;
            this.cmbSolicitante.SelectionChangeCommitted += new System.EventHandler(this.cmbSolicitante_SelectionChangeCommitted);
            // 
            // osIbametroOsClienteTableAdapter
            // 
            this.osIbametroOsClienteTableAdapter.ClearBeforeFill = true;
            // 
            // osInstrumentoServicoBindingSourceX
            // 
            this.osInstrumentoServicoBindingSourceX.DataMember = "OsInstrumentoServicoOsCliente";
            this.osInstrumentoServicoBindingSourceX.DataSource = this.dtsPrincipal;
            // 
            // osIbametroInmetroOsClienteTableAdapter
            // 
            this.osIbametroInmetroOsClienteTableAdapter.ClearBeforeFill = true;
            // 
            // xOsFuncionarioTableAdapter
            // 
            this.xOsFuncionarioTableAdapter.ClearBeforeFill = true;
            // 
            // fKOsInstrumentoServicoOsEtiquetaBindingSource
            // 
            this.fKOsInstrumentoServicoOsEtiquetaBindingSource.DataMember = "FK_OsInstrumentoServico_OsEtiqueta";
            this.fKOsInstrumentoServicoOsEtiquetaBindingSource.DataSource = this.bdsSelos;
            // 
            // ManOrdemServico
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(855, 673);
            this.Controls.Add(this.cmbSolicitante);
            this.Controls.Add(this.chkSolicitante);
            this.Controls.Add(this.lblEmpresa);
            this.Controls.Add(this.cmbEmpresa);
            this.Controls.Add(this.pnlConsultar);
            this.Controls.Add(this.bindingNavigator);
            this.Controls.Add(this.lblCodigoOS);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = true;
            this.MinimizeBox = true;
            this.Name = "ManOrdemServico";
            this.Text = "Manutenção de Ordem de Serviço";
            this.Load += new System.EventHandler(this.ManOrdemServico_Load);
            this.Shown += new System.EventHandler(this.ManOrdemServico_Shown);
            this.Controls.SetChildIndex(this.tabPrincipal, 0);
            this.Controls.SetChildIndex(this.grpPrincipal, 0);
            this.Controls.SetChildIndex(this.lblCodigoOS, 0);
            this.Controls.SetChildIndex(this.bindingNavigator, 0);
            this.Controls.SetChildIndex(this.pnlConsultar, 0);
            this.Controls.SetChildIndex(this.cmbEmpresa, 0);
            this.Controls.SetChildIndex(this.lblEmpresa, 0);
            this.Controls.SetChildIndex(this.chkSolicitante, 0);
            this.Controls.SetChildIndex(this.cmbSolicitante, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dtsPrincipal)).EndInit();
            this.grpPrincipal.ResumeLayout(false);
            this.grpPrincipal.PerformLayout();
            this.tabPrincipal.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bdsSelectEquipamento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator)).EndInit();
            this.bindingNavigator.ResumeLayout(false);
            this.bindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.osOrdemServicoBindingSource)).EndInit();
            this.tabCabecalho.ResumeLayout(false);
            this.tabCabecalho.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsClientes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OsXOSFuncionarioBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.osUsuarioBindingSource)).EndInit();
            this.tabSolicitacao.ResumeLayout(false);
            this.tabSolicitacao.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsSolicitacoes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdSolicitacao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtsSelectSituacao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.osSituacoesOSBindingSource)).EndInit();
            this.tabEncontrada.ResumeLayout(false);
            this.tabEncontrada.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdEncontrada)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsEncontrada)).EndInit();
            this.tabRealizada.ResumeLayout(false);
            this.tabRealizada.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsRealizada)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdRealizada)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.a30PRODUTOBindingSource)).EndInit();
            this.tabFinal.ResumeLayout(false);
            this.tabFinal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grsFinal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsFinal)).EndInit();
            this.tabPecas.ResumeLayout(false);
            this.tabPecas.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numQtdeProduto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdPecas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.osItemServicoBindingSource)).EndInit();
            this.tabServicos.ResumeLayout(false);
            this.tabServicos.PerformLayout();
            this.grpDespesas.ResumeLayout(false);
            this.grpDespesas.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.tabEquipamentos.ResumeLayout(false);
            this.tabEquipamentos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsSelectEquipamentoX)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsINMETROSelosX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsEquipamentoSelosX)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsSelos)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsEquipamentoSelos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsINMETROSelos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.osInstrumentoServicoBindingSource)).EndInit();
            this.tabPagamento.ResumeLayout(false);
            this.tabPagamento.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdPagamentos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.osFormasPagamentoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.osParContasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.osReciboBindingSource)).EndInit();
            this.tabLacres.ResumeLayout(false);
            this.tabLacres.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsINMETROLacresX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsEquipamentoLacreX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsLacre)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsINMETROLacres)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsEquipamentoLacre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsFiltroCliente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsFiltroUsuario)).EndInit();
            this.pnlConsultar.ResumeLayout(false);
            this.pnlConsultar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsFiltroXOSFuncionario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.osOrdemServicoDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsViewOS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.osParametrosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.osInstrumentoServicoBindingSourceX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKOsInstrumentoServicoOsEtiquetaBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingNavigator bindingNavigator;
        private System.Windows.Forms.ToolStripButton tsbInserir;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripButton tsbDescartar;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButton6;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton tsbSalvar;
        private System.Windows.Forms.ToolStripButton tsbImprimirOS;
        private System.Windows.Forms.TabPage tabCabecalho;
        private System.Windows.Forms.TabPage tabSolicitacao;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.BindingSource osOrdemServicoBindingSource;
        private OsExpressDataSetTableAdapters.OsOrdemServicoTableAdapter osOrdemServicoTableAdapter;
        //private OsExpressDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.TextBox txtCodOrdemServico;
        private System.Windows.Forms.DateTimePicker dtpDataCadastro;
        private System.Windows.Forms.DateTimePicker dtpDataFechamento;
        private System.Windows.Forms.TextBox txtObservacao;
        private System.Windows.Forms.ToolStripButton tsbFecharOS;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbCodCliente;
        private System.Windows.Forms.TabPage tabEncontrada;
        private System.Windows.Forms.TabPage tabRealizada;
        private System.Windows.Forms.TabPage tabFinal;
        private System.Windows.Forms.TabPage tabPecas;
        private System.Windows.Forms.TabPage tabServicos;
        private System.Windows.Forms.Label lblCabIE;
        private System.Windows.Forms.Label lblCabNCPJ;
        private System.Windows.Forms.Label lblCabCliente;
        private System.Windows.Forms.BindingSource bdsClientes;
        private OsExpressDataSetTableAdapters.A30CLIENTESTableAdapter a30CLIENTESTableAdapter;
        private OsExpressDataSetTableAdapters.OsParSituacaoTableAdapter osParSituacaoTableAdapter;
        private System.Windows.Forms.CheckBox chkSuporte;
        private System.Windows.Forms.CheckBox chkInstalacao;
        private System.Windows.Forms.ComboBox cmbNomeCliente;
        private System.Windows.Forms.BindingSource osSituacoesOSBindingSource;
        private OsExpressDataSetTableAdapters.OsSituacoesOSTableAdapter osSituacoesOSTableAdapter;
        private System.Windows.Forms.BindingSource bdsSolicitacoes;
        private System.Windows.Forms.BindingSource bdsEncontrada;
        private System.Windows.Forms.BindingSource bdsRealizada;
        private System.Windows.Forms.BindingSource bdsFinal;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cmbCodigoProduto;
        private System.Windows.Forms.Label lblCabObservacao;
        private System.Windows.Forms.Label lblCabTelefone;
        private System.Windows.Forms.GroupBox grpDespesas;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtOutrasDespesas;
        private System.Windows.Forms.TextBox txtTranslado;
        private System.Windows.Forms.TextBox txtHospedagem;
        private System.Windows.Forms.TextBox txtAlimentacao;
        private System.Windows.Forms.TextBox txtOutrosServicosQtde;
        private System.Windows.Forms.TextBox txtOutrosServicosVlr;
        private System.Windows.Forms.TextBox txtDeslocamentoKmQtde;
        private System.Windows.Forms.TextBox txtPontoRedeQtde;
        private System.Windows.Forms.TextBox txtTaxaInstalacaoQtde;
        private System.Windows.Forms.TextBox txtHoraExcedenteQtde;
        private System.Windows.Forms.TextBox txtVisitaTecnicaQtde;
        private System.Windows.Forms.TextBox txtDeslocamentoKmVlr;
        private System.Windows.Forms.TextBox txtPontoRedeVlr;
        private System.Windows.Forms.TextBox txtTaxaInstalacaoVlr;
        private System.Windows.Forms.TextBox txtHoraExcedenteVlr;
        private System.Windows.Forms.TextBox txtHoraDeslocamentoVlr;
        private System.Windows.Forms.TextBox txtVisitaTecnicaVlr;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtOutrosServicosTotal;
        private System.Windows.Forms.TextBox txtDeslocamentoKmTotal;
        private System.Windows.Forms.TextBox txtPontoRedeTotal;
        private System.Windows.Forms.TextBox txtTaxaInstalacaoTotal;
        private System.Windows.Forms.TextBox txtHoraExcedenteTotal;
        private System.Windows.Forms.TextBox txtHoraDeslocamentoTotal;
        private System.Windows.Forms.TextBox txtVisitaTecnicaTotal;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtTotalServicosTotal;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txtTotalDesp;
        private OsExpressDataSetTableAdapters.A30POSTOSTableAdapter a30POSTOSTableAdapter;
        private System.Windows.Forms.BindingSource a30PRODUTOBindingSource;
        private OsExpressDataSetTableAdapters.A30PRODUTOTableAdapter a30PRODUTOTableAdapter;
        private System.Windows.Forms.ComboBox cmbCodigoIDProduto;
        private System.Windows.Forms.ComboBox cmbDescricaoProduto;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtPrecoProduto;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtTotalProduto;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.BindingSource osItemServicoBindingSource;
        private OsExpressDataSetTableAdapters.OsItemServicoTableAdapter osItemServicoTableAdapter;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.DataGridView grdRealizada;
        private System.Windows.Forms.ComboBox cmbDescricaoRealizada;
        private System.Windows.Forms.ComboBox cmbCodigoRealizada;
        private System.Windows.Forms.DateTimePicker dtpHoraTotalRealizada;
        private System.Windows.Forms.DateTimePicker dtpHoraFinalRealizada;
        private System.Windows.Forms.DateTimePicker dtpHoraInicialRealizada;
        private System.Windows.Forms.BindingSource dtsSelectSituacao;
        private System.Windows.Forms.ComboBox cmbDescSolicitacao;
        private System.Windows.Forms.ComboBox cmbCodigoSolicitacao;
        private System.Windows.Forms.DataGridView grdSolicitacao;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.ComboBox cmbDescSitEncontrada;
        private System.Windows.Forms.ComboBox cmbCodigoSitEncontrada;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.ComboBox cmbDescSituacaoFinal;
        private System.Windows.Forms.ComboBox cmbCodigoSituacaoFinal;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        //private System.Windows.Forms.DataGridViewTextBoxColumn hORATOTALDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridView grdEncontrada;
        private System.Windows.Forms.DataGridView grsFinal;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn37;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn38;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn42;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn43;
        private System.Windows.Forms.TextBox txtHoraDeslocamentoQtde;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private OsExpressDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.Button btnInserirTipoSolicitacao;
        private System.Windows.Forms.Button btnInserirSituacaoEncontrada;
        private System.Windows.Forms.Button btnAtividadeRealizada;
        private System.Windows.Forms.Button btnInserirSituacaoFinal;
        private System.Windows.Forms.Button btnAdicionarPecas;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.TabPage tabEquipamentos;
        private System.Windows.Forms.Button btnAdicionarEquipamento;
        private System.Windows.Forms.BindingSource bdsEquipamentoSelos;
        private System.Windows.Forms.BindingSource bdsSelos;
        private OsExpressDataSetTableAdapters.OsEtiquetaTableAdapter osEtiquetaTableAdapter;
        private OsExpressDataSetTableAdapters.OsIbametroTableAdapter osIbametroTableAdapter;
        private System.Windows.Forms.BindingSource osInstrumentoServicoBindingSource;
        private OsExpressDataSetTableAdapters.OsInstrumentoServicoTableAdapter osInstrumentoServicoTableAdapter;
        private System.Windows.Forms.Label lblCabSituacao;
        private System.Windows.Forms.Label lblSituacao2;
        private System.Windows.Forms.BindingSource bdsSelectEquipamento;
        private System.Windows.Forms.TabPage tabPagamento;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.TextBox txtValorPago;
        private System.Windows.Forms.Button btnAdicionarFormaPag;
        private System.Windows.Forms.ComboBox cmbCodigoForma;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.ToolStripButton tsbImprimirRecibo;
        private System.Windows.Forms.BindingSource osFormasPagamentoBindingSource;
        private OsExpressDataSetTableAdapters.OsFormasPagamentoTableAdapter osFormasPagamentoTableAdapter;
        private System.Windows.Forms.BindingSource osParContasBindingSource;
        private OsExpressDataSetTableAdapters.OsParContasTableAdapter osParContasTableAdapter;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.DataGridView grdPagamentos;
        private System.Windows.Forms.DataGridView grdPecas;
        private System.Windows.Forms.CheckBox chkPorContaGeral;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDSITUACAODataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn DESCRICAO;
        private System.Windows.Forms.CheckBox chkPorContaOutros;
        private System.Windows.Forms.CheckBox chkPorContaTranslado;
        private System.Windows.Forms.CheckBox chkPorContaHospedagem;
        private System.Windows.Forms.CheckBox chkPorContaAlimentacao;
        private System.Windows.Forms.BindingSource osReciboBindingSource;
        private OsExpressDataSetTableAdapters.OsReciboTableAdapter osReciboTableAdapter;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TabPage tabLacres;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button btnInserirLacres;
        private System.Windows.Forms.ComboBox cmbNumeroEtiquetaLacres;
        private System.Windows.Forms.ComboBox cmbCodigoLacres;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.ComboBox cmbNumSerieLacres;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.ComboBox cmbNumeroEtiquetaSelos;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.ComboBox cmbNumSerieSelos;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.BindingSource bdsLacre;
        private System.Windows.Forms.BindingSource bdsEquipamentoLacre;
        private System.Windows.Forms.ComboBox cmbCodigoUsuario;
        private System.Windows.Forms.ComboBox cmbNomeUsuario;
        private System.Windows.Forms.BindingSource osUsuarioBindingSource;
        private OsExpressDataSetTableAdapters.OsUsuarioTableAdapter osUsuarioTableAdapter;
        private System.Windows.Forms.TextBox txtTotalGeralPecas;
        private System.Windows.Forms.TextBox txtTotalServicosOS;
        private System.Windows.Forms.TextBox txtTotalFinalOS;
        private System.Windows.Forms.TextBox txtValorAPagar;
        private System.Windows.Forms.TextBox txtTotalPago;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDFORMADataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn vALORDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn nome;
        private System.Windows.Forms.DateTimePicker dATA_INICIALDateTimePicker;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TextBox txtAssinaturaCliente;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDSITUACAODataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descricao123;
        private System.Windows.Forms.DataGridViewTextBoxColumn DATA_INICIAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn hORAINICIODataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn hORAFIMDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn hORATOTALDataGridViewTextBoxColumn;
        private System.Windows.Forms.ToolStripButton tsbCancelar;
        private System.Windows.Forms.BindingSource bdsFiltroUsuario;
        private System.Windows.Forms.BindingSource bdsFiltroCliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDPRODUTODataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Produto;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descrição;
        private System.Windows.Forms.DataGridViewTextBoxColumn qUANTIDADEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vALORDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tOTALDataGridViewTextBoxColumn;
        private System.Windows.Forms.ComboBox cmbNumInmetroLacres;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Panel pnlConsultar;
        private System.Windows.Forms.Label lblTotalOS;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cmbFiltroClienteCodigo;
        private System.Windows.Forms.ComboBox cmbFiltroUsuarioCodigo;
        private System.Windows.Forms.CheckBox chkFiltrarCliente;
        private System.Windows.Forms.CheckBox chkFiltrarUsuario;
        private System.Windows.Forms.CheckBox chkFiltrarSituacao;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.ComboBox cmbFiltroSituacao;
        private System.Windows.Forms.ComboBox cmbFiltroClienteNome;
        private System.Windows.Forms.ComboBox cmbFiltroUsuarioNome;
        private System.Windows.Forms.DataGridView osOrdemServicoDataGridView;
        private System.Windows.Forms.ComboBox comboBox8;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.BindingSource bdsViewOS;
        private OsExpressDataSetTableAdapters.OsOrdemServicoTableAdapter osOrdemServicoTableAdapterView;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Label lblCabFatura;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label lblNovoCodOS;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblCodigoOS;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ToolStripButton tsbGerarCaixa;
        private System.Windows.Forms.NumericUpDown numQtdeProduto;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.ComboBox cmbCodigoEquipamentoLacres;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.ComboBox cmbCodigoSelos;
        private System.Windows.Forms.BindingSource bdsINMETROSelos;
        private System.Windows.Forms.BindingSource bdsINMETROLacres;
        private OsExpressDataSetTableAdapters.OsIbametroInmetroTableAdapter osIbametroInmetroTableAdapter;
        private System.Windows.Forms.ComboBox cmbCodigoINMETROSelos;
        private System.Windows.Forms.ComboBox cmbCodigoEquipamentoSelos;
        private System.Windows.Forms.ComboBox cmbCodigoINMETROLacres;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox cmbNumInmetroSelos;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.BindingSource osParametrosBindingSource;
        private OsExpressDataSetTableAdapters.OsParametrosTableAdapter osParametrosTableAdapter;
        private System.Windows.Forms.TextBox tbCodCliente;
        private System.Windows.Forms.TextBox tbSeloAnterior;
        private System.Windows.Forms.Label lblSeloAnterior;
        private System.Windows.Forms.Label lblMarcaInitial;
        private System.Windows.Forms.TextBox tbMarcaInicial;
        private System.Windows.Forms.DataGridViewTextBoxColumn NUM_SERIE;
        private System.Windows.Forms.DataGridViewTextBoxColumn Numero_IMETRO;
        private System.Windows.Forms.DataGridViewTextBoxColumn numero_etiqueta;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDETIQUETADataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDIBAMETRODataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn MARCA_INSTRUMENTO_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDORDEMDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn marca_inicial;
        private System.Windows.Forms.DataGridViewTextBoxColumn selo_anterior;
        private System.Windows.Forms.TextBox tbLacreAnterior;
        private System.Windows.Forms.TextBox tbMarcaInicialLacre;
        private System.Windows.Forms.Label lbl_LacreAnterior;
        private System.Windows.Forms.Label lblMarcaInicialLacre;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn lacre_anterior;
        private System.Windows.Forms.Label lblFiltroLike;
        private System.Windows.Forms.TextBox tbFiltroLike;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label lb_media_de_consumo;
        private System.Windows.Forms.Label lbl_total_de_kilometragem;
        private System.Windows.Forms.TextBox tb_preco;
        private System.Windows.Forms.TextBox tb_media;
        private System.Windows.Forms.TextBox tb_kilometragem;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DateTimePicker dtp_de;
        private System.Windows.Forms.CheckBox chkFiltrarPeriodo;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label lblFiltroPeriodoDe;
        private System.Windows.Forms.DateTimePicker dtp_ate;
        private System.Windows.Forms.Label lblUF2;
        private System.Windows.Forms.Label lblCidade2;
        private System.Windows.Forms.Label lblUF;
        private System.Windows.Forms.Label lblCidade;
        private System.Windows.Forms.ComboBox cmbEmpresa;
        private System.Windows.Forms.Label lblEmpresa;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDPOSTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn Usuario;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cliente;
        private System.Windows.Forms.ComboBox cmbPostoOS;
        private System.Windows.Forms.Label labelPostoOS;
        private System.Windows.Forms.CheckBox chkPostoOS;
        private System.Windows.Forms.CheckBox chkSolicitante;
        private System.Windows.Forms.ComboBox cmbSolicitante;
        private System.Windows.Forms.BindingSource bdsEquipamentoSelosX;
        private OsExpressDataSetTableAdapters.OsIbametroOsClienteTableAdapter osIbametroOsClienteTableAdapter;
        private System.Windows.Forms.BindingSource osInstrumentoServicoBindingSourceX;
        private OsExpressDataSetTableAdapters.OsInstrumentoServicoOsClienteTableAdapter osInstrumentoServicoOsClienteTableAdapter;
        private System.Windows.Forms.BindingSource bdsINMETROSelosX;
        private OsExpressDataSetTableAdapters.OsIbametroInmetroOsClienteTableAdapter osIbametroInmetroOsClienteTableAdapter;
        private System.Windows.Forms.BindingSource bdsSelectEquipamentoX;
        private System.Windows.Forms.BindingSource bdsEquipamentoLacreX;
        private System.Windows.Forms.BindingSource bdsINMETROLacresX;
        private System.Windows.Forms.BindingSource OsXOSFuncionarioBindingSource;
        private OsExpressDataSetTableAdapters.XOsFuncionarioTableAdapter xOsFuncionarioTableAdapter;
        private System.Windows.Forms.BindingSource bdsFiltroXOSFuncionario;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDUSUARIODataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDCLIENTEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dTCADASTRODataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dTFECHAMENTODataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn oBSDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource fKOsInstrumentoServicoOsEtiquetaBindingSource;
    }
}
