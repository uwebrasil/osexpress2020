﻿using System;
using System.Data;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using USBDriveSerialNumber;
using System.Xml;
using System.Text;

namespace osExpress.Manutencoes
{
    public partial class ManFlashDrive : osExpress.FrmPrincipal
    {
        int ultimoCliente = -1;

        private Relatorios.RelIbametroCliente relIbametroCliente { get; set; }

        public ManFlashDrive()
        {
            InitializeComponent();
            string f = a30CLIENTESBindingSource.Filter;
            a30CLIENTESBindingSource.Filter = f + " and " + string.Format("Posto = '{0}'", OsConfiguracoes.codigoPosto);
        }

        private void estadoInicial()
        {
            // *** Controles *** //

            tsbInserir.Enabled = true;
            tsbGravar.Enabled = false; 

            cmbCodigoCliente.SelectedIndex = -1;
            cmbNomeCliente.SelectedIndex = -1;
            //cmbMarcaInstrumento.SelectedIndex = -1;
            osFlashDriveBindingSource.Filter = "ID = 0";
            pesquisar();
            cmbNomeCliente.Focus();
        }

        private void ManIbametro_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dtsPrincipal.OsFlashDrive' table. You can move, or remove it, as needed.
            this.osFlashDriveTableAdapter.Fill(this.dtsPrincipal.OsFlashDrive);
            // TODO: This line of code loads data into the 'dtsPrincipal.A30CLIENTES' table. You can move, or remove it, as needed.
            this.a30CLIENTESTableAdapter.Fill(this.dtsPrincipal.A30CLIENTES);
            //
            this.WindowState = FormWindowState.Maximized;

            estadoInicial();

            //pesquisar();
        }

        private void tsbGravar_Click(object sender, EventArgs e)
        {

            // *** Controles *** //
            tsbInserir.Enabled = true;
            tsbGravar.Enabled = false;
            tsbPrimeiro.Enabled = true;
            tsbAnterior.Enabled = true;
            tsbProximo.Enabled = true;
            tsbUltimo.Enabled = true;
            // ***************** //

            cmbCodigoCliente.SelectedIndex = ultimoCliente;
            cmbNomeCliente.SelectedIndex = ultimoCliente;
            estadoCorrente = EstadoManutencao.smPesquisar;
            try
            {
                this.Validate();
                this.osFlashDriveBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(this.dtsPrincipal);
                cmbNomeCliente.Enabled = true;
                cmbCodigoCliente.Enabled = true;
            }
            catch (Exception)
            {
                MessageBox.Show("ERRO");
                cancelar();
            }

            tmrPrincipal.Enabled = false;
        }

        private void tsbInserir_Click(object sender, EventArgs e)
        {
            inserir();

            tsbGravar.Enabled = true;

            segundaTextBox.Focus();

            cmbCodigoCliente.SelectedIndex = ultimoCliente;
            cmbNomeCliente.SelectedIndex = ultimoCliente;
            cmbNomeCliente.Enabled = false;
            cmbCodigoCliente.Enabled = false;

            tsbInserir.Enabled = false;
            tmrPrincipal.Enabled = true;
        }

        private void ManIbametro_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }

        private void btnInserirMecanico_Enter(object sender, EventArgs e)
        {
            tsbInserir.Enabled = true;
            //tsbGravar.PerformClick();
            tsbInserir.PerformClick();
        }

        private void comboBox6_SelectedValueChanged(object sender, EventArgs e)
        {
            if (/*cmbNomeCliente.Text.Length > 0 ||*/ cmbCodigoCliente.Text.Length > 0)
            {
                ultimoCliente = cmbCodigoCliente.SelectedIndex;
                ultimoCliente = cmbNomeCliente.SelectedIndex;
                try
                {
                        osFlashDriveBindingSource.Filter = "IDCLIENTE = " + cmbCodigoCliente.SelectedValue.ToString();
                }
                catch (Exception)
                {

                }

                if (ultimoCliente >= 0)
                {
                    cmbCodigoCliente.SelectedIndex = ultimoCliente;
                    cmbNomeCliente.SelectedIndex = ultimoCliente;

                }
            }
         }

        private void tsbPrimeiro_Click(object sender, EventArgs e)
        {
            cancelarAlteracoes();
            a30CLIENTESBindingSource.MoveFirst();
        }

        private void tsbAnterior_Click(object sender, EventArgs e)
        {
            cancelarAlteracoes();
            a30CLIENTESBindingSource.MovePrevious();
        }

        private void tsbProximo_Click(object sender, EventArgs e)
        {
            cancelarAlteracoes();
            a30CLIENTESBindingSource.MoveNext();
        }

        private void tsbUltimo_Click(object sender, EventArgs e)
        {
            cancelarAlteracoes();
            a30CLIENTESBindingSource.MoveLast();
        }

        private void osIbametroDataGridView_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            tsbGravar.Enabled = true;
            excluir();
        }

        private void tsbExcluir_Click(object sender, EventArgs e)
        {
            excluir();
        }

        private void cancelar()
        {
            cmbNomeCliente.Enabled = true;
            cmbCodigoCliente.Enabled = true;
            osFlashDriveBindingSource.CancelEdit();
            dtsPrincipal.RejectChanges();
            pesquisar();
            tsbInserir.Enabled = true;
            tsbGravar.Enabled = false;
        }

        private void cancelarAlteracoes()
        {
            if (estadoCorrente == EstadoManutencao.smInserir ||
                estadoCorrente == EstadoManutencao.smExcluir ||
                estadoCorrente == EstadoManutencao.smEditar)
            {
                if (MessageBox.Show("Deseja abandonar sem gravar?",
                    null,
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    cancelar();
                }
            }
        }

        private void osIbametroDataGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            cancelar();
        }

        private void osIbametroDataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            tsbInserir.Enabled = false;
            tsbGravar.Enabled = true;
            editar();
        }

        private void osIbametroInmetroDataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            tsbInserir.Enabled = false;
            tsbGravar.Enabled = true;
            editar();
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void tsbImprimir_Click(object sender, EventArgs e)
        {
            /*relIbametroCliente = new Relatorios.RelIbametroCliente();

            relIbametroCliente.configuraOS(cmbCodigoCliente.SelectedValue.ToString());

            relIbametroCliente.Show();*/
        }

        private void tmrPrincipal_Tick(object sender, EventArgs e)
        {
            try
            {
                dbgDrivers.DataSource = null;
                dbgDrivers.Rows.Clear();

                DriveInfo[] ListDrives = DriveInfo.GetDrives();

                foreach (DriveInfo Drive in ListDrives)
                {
                    if (Drive.DriveType == DriveType.Removable)
                    {
                        if (Drive.IsReady)
                        {
                            USBSerialNumber usb = new USBSerialNumber();
                            string serial = usb.getSerialNumberFromDriveLetter(Drive.RootDirectory.ToString().Substring(0, 2));
                            dbgDrivers.Rows.Add(Drive.RootDirectory, Drive.VolumeLabel, serial, Drive.DriveType.ToString());

                            if (segundaTextBox.Focused)
                            {
                                segundaTextBox.Text = serial;
                            }
                            else
                            if (tercaTextBox.Focused)
                            {
                                tercaTextBox.Text = serial;
                            }
                            else
                            if (quartaTextBox.Focused)
                            {
                                quartaTextBox.Text = serial;
                            }
                            else
                            if (quintaTextBox.Focused)
                            {
                                quintaTextBox.Text = serial;
                            }
                            else
                            if (sextaTextBox.Focused)
                            {
                                sextaTextBox.Text = serial;
                            }
                            else
                            if (sabadoTextBox.Focused)
                            {
                                sabadoTextBox.Text = serial;
                            }
                            else
                            if (domingoTextBox.Focused)
                            {
                                domingoTextBox.Text = serial;
                            }

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                stsPrincipal.Text = "Status: " + ex.Message;
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            string xml = "";

            xml = "<Numeroserie>\r\n" +
                      "<segunda>" + segundaTextBox.Text + "</segunda>\r\n" +
                      "<terca>" + tercaTextBox.Text + "</terca>\r\n" +
                      "<quarta>" + quartaTextBox.Text + "</quarta>\r\n" +
                      "<quinta>" + quintaTextBox.Text + "</quinta>\r\n" +
                      "<sexta>" + sextaTextBox.Text + "</sexta>\r\n" +
                      "<sabado>" + sabadoTextBox.Text + "</sabado>\r\n" +
                      "<domingo>" + domingoTextBox.Text + "</domingo>\r\n" +
                  "</Numeroserie>";

            string nomeArquivo = cmbCodigoCliente.Text + ".xml";

            StreamWriter file = new System.IO.StreamWriter(nomeArquivo);
            file.WriteLine(xml);
            file.Close();

            MessageBox.Show("Arquivo: " + nomeArquivo + " Criado com Sucesso", "osExpress");
        }

    }
} 
