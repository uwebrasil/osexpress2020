﻿namespace osExpress.Manutencoes
{
    partial class ManFuncionario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label label_rg;
            System.Windows.Forms.Label fAXLabel;
            System.Windows.Forms.Label tELEFONELabel;
            System.Windows.Forms.Label bAIRROLabel;
            System.Windows.Forms.Label eNDERECOLabel;
            System.Windows.Forms.Label cIDADELabel;
            System.Windows.Forms.Label uFLabel;
            System.Windows.Forms.Label labelNome;
            System.Windows.Forms.Label nOME_FANTASIALabel;
            System.Windows.Forms.Label tIPO_PESSOALabel;
            System.Windows.Forms.Label iDLabel;
            System.Windows.Forms.Label cNPJLabel;
            System.Windows.Forms.Label labelEmail;
            System.Windows.Forms.Label cPFLabel;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.labelCep = new System.Windows.Forms.Label();
            this.labelCadastrarFuncionario = new System.Windows.Forms.Label();
            this.pnlConsultar = new System.Windows.Forms.Panel();
            this.tbLike = new System.Windows.Forms.TextBox();
            this.labelLike = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label70 = new System.Windows.Forms.Label();
            this.lblTotalOS = new System.Windows.Forms.Label();
            this.dgvFuncionario = new System.Windows.Forms.DataGridView();
            this.CNPJ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.postoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.funcionarioDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.turnoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipoPessoaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cgcDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.inscricaoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.enderecoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bairroDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cidadeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.estadoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cepDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs01DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs02DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs03DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs04DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs05DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs06DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs07DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs08DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs09DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs10DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.noturnoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.inativoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtAlterDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs11DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs12DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs13DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs14DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs15DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs16DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs17DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs18DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs19DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs20DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rFIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.biometriaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cTACaixaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipoVIPDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uNomeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uSenhaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipoacessoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtcadastroDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ultimoacessoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.planocontaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.acessapedidoDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.digitaselolacreDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.idusuarioDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isusuarioDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bsXOSFuncionario = new System.Windows.Forms.BindingSource(this.components);
            this.dtsXOSFuncionario = new osExpress.XOSFuncionario();
            this.xOsFuncionarioTableAdapter = new osExpress.XOSFuncionarioTableAdapters.XOsFuncionarioTableAdapter();
            this.FuncionarioBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bnInserir = new System.Windows.Forms.ToolStripButton();
            this.bnCount = new System.Windows.Forms.ToolStripLabel();
            this.bnConsultar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.bnGravar = new System.Windows.Forms.ToolStripButton();
            this.bnCancelar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.bnPrimeiro = new System.Windows.Forms.ToolStripButton();
            this.bnAnterior = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bnPosition = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bnProximo = new System.Windows.Forms.ToolStripButton();
            this.bnUltimo = new System.Windows.Forms.ToolStripButton();
            this.bnDescartar = new System.Windows.Forms.ToolStripButton();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.labelSenha = new System.Windows.Forms.Label();
            this.labelLogin = new System.Windows.Forms.Label();
            this.tbSenha = new System.Windows.Forms.TextBox();
            this.tbLogin = new System.Windows.Forms.TextBox();
            this.checkboxIsUsuario = new System.Windows.Forms.CheckBox();
            this.tb_rg = new System.Windows.Forms.TextBox();
            this.tabChild = new System.Windows.Forms.TabControl();
            this.tabEndereco = new System.Windows.Forms.TabPage();
            this.tELEFONETextBox = new System.Windows.Forms.TextBox();
            this.fAXTextBox = new System.Windows.Forms.TextBox();
            this.mtbTelefone = new System.Windows.Forms.MaskedTextBox();
            this.tbBairro = new System.Windows.Forms.TextBox();
            this.tbEndereco = new System.Windows.Forms.TextBox();
            this.tbCidade = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.tbCep = new System.Windows.Forms.TextBox();
            this.tabOrcamentos = new System.Windows.Forms.TabPage();
            this.pnlOrcamentos = new System.Windows.Forms.Panel();
            this.pnlOrcamentosGrid = new System.Windows.Forms.Panel();
            this.dgvOrcamentos = new System.Windows.Forms.DataGridView();
            this.pnlOrcamentosFilter = new System.Windows.Forms.Panel();
            this.tabPedidos = new System.Windows.Forms.TabPage();
            this.tabServicos = new System.Windows.Forms.TabPage();
            this.tabInstrumentos = new System.Windows.Forms.TabPage();
            this.pnlInstrumentos = new System.Windows.Forms.Panel();
            this.pnlInstrumentosGrid = new System.Windows.Forms.Panel();
            this.dgvInstrumentos = new System.Windows.Forms.DataGridView();
            this.pnlInstrumentosFilter = new System.Windows.Forms.Panel();
            this.tbNome = new System.Windows.Forms.TextBox();
            this.nOME_FANTASIATextBox = new System.Windows.Forms.TextBox();
            this.mtbCPF = new System.Windows.Forms.MaskedTextBox();
            this.mtbCNPJ = new System.Windows.Forms.MaskedTextBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.tbIdusuario = new System.Windows.Forms.TextBox();
            this.tb_email = new System.Windows.Forms.TextBox();
            this.cPFTextBox = new System.Windows.Forms.TextBox();
            this.cNPJTextBox = new System.Windows.Forms.TextBox();
            this.pnlPedidos = new System.Windows.Forms.Panel();
            this.pnlPedidosGrid = new System.Windows.Forms.Panel();
            this.dgvPedidos = new System.Windows.Forms.DataGridView();
            this.pnlPedidosFilter = new System.Windows.Forms.Panel();
            this.pnlServicos = new System.Windows.Forms.Panel();
            this.pnlServicosGrid = new System.Windows.Forms.Panel();
            this.dgvServicos = new System.Windows.Forms.DataGridView();
            this.pnlServicosFilter = new System.Windows.Forms.Panel();
            label_rg = new System.Windows.Forms.Label();
            fAXLabel = new System.Windows.Forms.Label();
            tELEFONELabel = new System.Windows.Forms.Label();
            bAIRROLabel = new System.Windows.Forms.Label();
            eNDERECOLabel = new System.Windows.Forms.Label();
            cIDADELabel = new System.Windows.Forms.Label();
            uFLabel = new System.Windows.Forms.Label();
            labelNome = new System.Windows.Forms.Label();
            nOME_FANTASIALabel = new System.Windows.Forms.Label();
            tIPO_PESSOALabel = new System.Windows.Forms.Label();
            iDLabel = new System.Windows.Forms.Label();
            cNPJLabel = new System.Windows.Forms.Label();
            labelEmail = new System.Windows.Forms.Label();
            cPFLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dtsPrincipal)).BeginInit();
            this.tabPrincipal.SuspendLayout();
            this.pnlConsultar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFuncionario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsXOSFuncionario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtsXOSFuncionario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FuncionarioBindingNavigator)).BeginInit();
            this.FuncionarioBindingNavigator.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabChild.SuspendLayout();
            this.tabEndereco.SuspendLayout();
            this.tabOrcamentos.SuspendLayout();
            this.pnlOrcamentos.SuspendLayout();
            this.pnlOrcamentosGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOrcamentos)).BeginInit();
            this.tabPedidos.SuspendLayout();
            this.tabServicos.SuspendLayout();
            this.tabInstrumentos.SuspendLayout();
            this.pnlInstrumentos.SuspendLayout();
            this.pnlInstrumentosGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInstrumentos)).BeginInit();
            this.pnlPedidos.SuspendLayout();
            this.pnlPedidosGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPedidos)).BeginInit();
            this.pnlServicos.SuspendLayout();
            this.pnlServicosGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvServicos)).BeginInit();
            this.SuspendLayout();
            // 
            // grpPrincipal
            // 
            this.grpPrincipal.Visible = false;
            // 
            // tabPrincipal
            // 
            this.tabPrincipal.Controls.Add(this.tabPage1);
            this.tabPrincipal.Size = new System.Drawing.Size(960, 580);
            // 
            // label_rg
            // 
            label_rg.AutoSize = true;
            label_rg.Location = new System.Drawing.Point(360, 10);
            label_rg.Name = "label_rg";
            label_rg.Size = new System.Drawing.Size(29, 13);
            label_rg.TabIndex = 52;
            label_rg.Text = "R.G.";
            // 
            // fAXLabel
            // 
            fAXLabel.AutoSize = true;
            fAXLabel.Location = new System.Drawing.Point(300, 95);
            fAXLabel.Name = "fAXLabel";
            fAXLabel.Size = new System.Drawing.Size(24, 13);
            fAXLabel.TabIndex = 66;
            fAXLabel.Text = "Fax";
            fAXLabel.Visible = false;
            // 
            // tELEFONELabel
            // 
            tELEFONELabel.AutoSize = true;
            tELEFONELabel.Location = new System.Drawing.Point(10, 95);
            tELEFONELabel.Name = "tELEFONELabel";
            tELEFONELabel.Size = new System.Drawing.Size(49, 13);
            tELEFONELabel.TabIndex = 61;
            tELEFONELabel.Text = "Telefone";
            // 
            // bAIRROLabel
            // 
            bAIRROLabel.AutoSize = true;
            bAIRROLabel.Location = new System.Drawing.Point(450, 55);
            bAIRROLabel.Name = "bAIRROLabel";
            bAIRROLabel.Size = new System.Drawing.Size(34, 13);
            bAIRROLabel.TabIndex = 60;
            bAIRROLabel.Text = "Bairro";
            // 
            // eNDERECOLabel
            // 
            eNDERECOLabel.AutoSize = true;
            eNDERECOLabel.Location = new System.Drawing.Point(10, 55);
            eNDERECOLabel.Name = "eNDERECOLabel";
            eNDERECOLabel.Size = new System.Drawing.Size(53, 13);
            eNDERECOLabel.TabIndex = 58;
            eNDERECOLabel.Text = "Endereço";
            // 
            // cIDADELabel
            // 
            cIDADELabel.AutoSize = true;
            cIDADELabel.Location = new System.Drawing.Point(240, 10);
            cIDADELabel.Name = "cIDADELabel";
            cIDADELabel.Size = new System.Drawing.Size(40, 13);
            cIDADELabel.TabIndex = 57;
            cIDADELabel.Text = "Cidade";
            // 
            // uFLabel
            // 
            uFLabel.AutoSize = true;
            uFLabel.Location = new System.Drawing.Point(170, 10);
            uFLabel.Name = "uFLabel";
            uFLabel.Size = new System.Drawing.Size(40, 13);
            uFLabel.TabIndex = 55;
            uFLabel.Text = "Estado";
            // 
            // labelNome
            // 
            labelNome.AutoSize = true;
            labelNome.Location = new System.Drawing.Point(100, 100);
            labelNome.Name = "labelNome";
            labelNome.Size = new System.Drawing.Size(38, 13);
            labelNome.TabIndex = 47;
            labelNome.Text = "Nome ";
            // 
            // nOME_FANTASIALabel
            // 
            nOME_FANTASIALabel.AutoSize = true;
            nOME_FANTASIALabel.Location = new System.Drawing.Point(100, 140);
            nOME_FANTASIALabel.Name = "nOME_FANTASIALabel";
            nOME_FANTASIALabel.Size = new System.Drawing.Size(78, 13);
            nOME_FANTASIALabel.TabIndex = 48;
            nOME_FANTASIALabel.Text = "Nome Fantasia";
            nOME_FANTASIALabel.Visible = false;
            // 
            // tIPO_PESSOALabel
            // 
            tIPO_PESSOALabel.AutoSize = true;
            tIPO_PESSOALabel.Location = new System.Drawing.Point(25, 10);
            tIPO_PESSOALabel.Name = "tIPO_PESSOALabel";
            tIPO_PESSOALabel.Size = new System.Drawing.Size(66, 13);
            tIPO_PESSOALabel.TabIndex = 0;
            tIPO_PESSOALabel.Text = "Tipo Pessoa";
            tIPO_PESSOALabel.Visible = false;
            // 
            // iDLabel
            // 
            iDLabel.AutoSize = true;
            iDLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            iDLabel.Location = new System.Drawing.Point(25, 100);
            iDLabel.Name = "iDLabel";
            iDLabel.Size = new System.Drawing.Size(46, 13);
            iDLabel.TabIndex = 0;
            iDLabel.Text = "Código";
            // 
            // cNPJLabel
            // 
            cNPJLabel.AutoSize = true;
            cNPJLabel.Location = new System.Drawing.Point(110, 55);
            cNPJLabel.Name = "cNPJLabel";
            cNPJLabel.Size = new System.Drawing.Size(34, 13);
            cNPJLabel.TabIndex = 0;
            cNPJLabel.Text = "CNPJ";
            cNPJLabel.Visible = false;
            // 
            // labelEmail
            // 
            labelEmail.AutoSize = true;
            labelEmail.Location = new System.Drawing.Point(360, 55);
            labelEmail.Name = "labelEmail";
            labelEmail.Size = new System.Drawing.Size(32, 13);
            labelEmail.TabIndex = 0;
            labelEmail.Text = "Email";
            // 
            // cPFLabel
            // 
            cPFLabel.AutoSize = true;
            cPFLabel.Location = new System.Drawing.Point(110, 10);
            cPFLabel.Name = "cPFLabel";
            cPFLabel.Size = new System.Drawing.Size(27, 13);
            cPFLabel.TabIndex = 0;
            cPFLabel.Text = "CPF";
            // 
            // labelCep
            // 
            this.labelCep.AutoSize = true;
            this.labelCep.Location = new System.Drawing.Point(10, 10);
            this.labelCep.Name = "labelCep";
            this.labelCep.Size = new System.Drawing.Size(28, 13);
            this.labelCep.TabIndex = 50;
            this.labelCep.Text = "CEP";
            // 
            // labelCadastrarFuncionario
            // 
            this.labelCadastrarFuncionario.AutoSize = true;
            this.labelCadastrarFuncionario.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCadastrarFuncionario.Location = new System.Drawing.Point(217, 9);
            this.labelCadastrarFuncionario.Name = "labelCadastrarFuncionario";
            this.labelCadastrarFuncionario.Size = new System.Drawing.Size(309, 31);
            this.labelCadastrarFuncionario.TabIndex = 13;
            this.labelCadastrarFuncionario.Text = "Cadastro Funcionários";
            this.labelCadastrarFuncionario.Visible = false;
            // 
            // pnlConsultar
            // 
            this.pnlConsultar.BackColor = System.Drawing.SystemColors.Control;
            this.pnlConsultar.Controls.Add(this.tbLike);
            this.pnlConsultar.Controls.Add(this.labelLike);
            this.pnlConsultar.Controls.Add(this.label8);
            this.pnlConsultar.Controls.Add(this.pictureBox1);
            this.pnlConsultar.Controls.Add(this.label70);
            this.pnlConsultar.Controls.Add(this.lblTotalOS);
            this.pnlConsultar.Controls.Add(this.dgvFuncionario);
            this.pnlConsultar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnlConsultar.Location = new System.Drawing.Point(934, 121);
            this.pnlConsultar.Name = "pnlConsultar";
            this.pnlConsultar.Size = new System.Drawing.Size(717, 350);
            this.pnlConsultar.TabIndex = 14;
            // 
            // tbLike
            // 
            this.tbLike.Location = new System.Drawing.Point(6, 10);
            this.tbLike.Name = "tbLike";
            this.tbLike.Size = new System.Drawing.Size(100, 20);
            this.tbLike.TabIndex = 22;
            this.tbLike.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbLike_KeyDown);
            // 
            // labelLike
            // 
            this.labelLike.AutoSize = true;
            this.labelLike.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelLike.Location = new System.Drawing.Point(284, 169);
            this.labelLike.Name = "labelLike";
            this.labelLike.Size = new System.Drawing.Size(264, 13);
            this.labelLike.TabIndex = 19;
            this.labelLike.Text = "Filtro LIKE: %Nome%  - Acione <ENTER> para acionar";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(3, 181);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(331, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Clique em \'S\' ou dois cliques do mouse para selecionar o Funcionário";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::osExpress.Properties.Resources.search;
            this.pictureBox1.Location = new System.Drawing.Point(336, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(51, 50);
            this.pictureBox1.TabIndex = 16;
            this.pictureBox1.TabStop = false;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.Location = new System.Drawing.Point(385, 15);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(122, 31);
            this.label70.TabIndex = 15;
            this.label70.Text = "Consulta";
            // 
            // lblTotalOS
            // 
            this.lblTotalOS.AutoSize = true;
            this.lblTotalOS.Location = new System.Drawing.Point(3, 487);
            this.lblTotalOS.Name = "lblTotalOS";
            this.lblTotalOS.Size = new System.Drawing.Size(37, 13);
            this.lblTotalOS.TabIndex = 13;
            this.lblTotalOS.Text = "Total: ";
            this.lblTotalOS.Visible = false;
            // 
            // dgvFuncionario
            // 
            this.dgvFuncionario.AllowUserToAddRows = false;
            this.dgvFuncionario.AllowUserToDeleteRows = false;
            this.dgvFuncionario.AutoGenerateColumns = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvFuncionario.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvFuncionario.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFuncionario.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CNPJ,
            this.postoDataGridViewTextBoxColumn,
            this.funcionarioDataGridViewTextBoxColumn,
            this.nomeDataGridViewTextBoxColumn,
            this.turnoDataGridViewTextBoxColumn,
            this.tipoPessoaDataGridViewTextBoxColumn,
            this.cgcDataGridViewTextBoxColumn,
            this.inscricaoDataGridViewTextBoxColumn,
            this.enderecoDataGridViewTextBoxColumn,
            this.bairroDataGridViewTextBoxColumn,
            this.cidadeDataGridViewTextBoxColumn,
            this.estadoDataGridViewTextBoxColumn,
            this.cepDataGridViewTextBoxColumn,
            this.obs01DataGridViewTextBoxColumn,
            this.obs02DataGridViewTextBoxColumn,
            this.obs03DataGridViewTextBoxColumn,
            this.obs04DataGridViewTextBoxColumn,
            this.obs05DataGridViewTextBoxColumn,
            this.obs06DataGridViewTextBoxColumn,
            this.obs07DataGridViewTextBoxColumn,
            this.obs08DataGridViewTextBoxColumn,
            this.obs09DataGridViewTextBoxColumn,
            this.obs10DataGridViewTextBoxColumn,
            this.noturnoDataGridViewTextBoxColumn,
            this.iDDataGridViewTextBoxColumn,
            this.inativoDataGridViewTextBoxColumn,
            this.dtAlterDataGridViewTextBoxColumn,
            this.obs11DataGridViewTextBoxColumn,
            this.obs12DataGridViewTextBoxColumn,
            this.obs13DataGridViewTextBoxColumn,
            this.obs14DataGridViewTextBoxColumn,
            this.obs15DataGridViewTextBoxColumn,
            this.obs16DataGridViewTextBoxColumn,
            this.obs17DataGridViewTextBoxColumn,
            this.obs18DataGridViewTextBoxColumn,
            this.obs19DataGridViewTextBoxColumn,
            this.obs20DataGridViewTextBoxColumn,
            this.rFIDDataGridViewTextBoxColumn,
            this.biometriaDataGridViewTextBoxColumn,
            this.cTACaixaDataGridViewTextBoxColumn,
            this.tipoVIPDataGridViewTextBoxColumn,
            this.uNomeDataGridViewTextBoxColumn,
            this.uSenhaDataGridViewTextBoxColumn,
            this.tipoacessoDataGridViewTextBoxColumn,
            this.dtcadastroDataGridViewTextBoxColumn,
            this.ultimoacessoDataGridViewTextBoxColumn,
            this.planocontaDataGridViewTextBoxColumn,
            this.acessapedidoDataGridViewCheckBoxColumn,
            this.digitaselolacreDataGridViewCheckBoxColumn,
            this.idusuarioDataGridViewTextBoxColumn,
            this.isusuarioDataGridViewTextBoxColumn});
            this.dgvFuncionario.DataSource = this.bsXOSFuncionario;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvFuncionario.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvFuncionario.Location = new System.Drawing.Point(6, 200);
            this.dgvFuncionario.Name = "dgvFuncionario";
            this.dgvFuncionario.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvFuncionario.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvFuncionario.Size = new System.Drawing.Size(700, 199);
            this.dgvFuncionario.TabIndex = 11;
            this.dgvFuncionario.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFuncionario_CellDoubleClick);
            this.dgvFuncionario.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvFuncionario_KeyDown);
            // 
            // CNPJ
            // 
            this.CNPJ.DataPropertyName = "CNPJ";
            this.CNPJ.HeaderText = "CNPJ";
            this.CNPJ.Name = "CNPJ";
            this.CNPJ.ReadOnly = true;
            // 
            // postoDataGridViewTextBoxColumn
            // 
            this.postoDataGridViewTextBoxColumn.DataPropertyName = "Posto";
            this.postoDataGridViewTextBoxColumn.HeaderText = "Posto";
            this.postoDataGridViewTextBoxColumn.Name = "postoDataGridViewTextBoxColumn";
            this.postoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // funcionarioDataGridViewTextBoxColumn
            // 
            this.funcionarioDataGridViewTextBoxColumn.DataPropertyName = "Funcionario";
            this.funcionarioDataGridViewTextBoxColumn.HeaderText = "Funcionario";
            this.funcionarioDataGridViewTextBoxColumn.Name = "funcionarioDataGridViewTextBoxColumn";
            this.funcionarioDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nomeDataGridViewTextBoxColumn
            // 
            this.nomeDataGridViewTextBoxColumn.DataPropertyName = "Nome";
            this.nomeDataGridViewTextBoxColumn.HeaderText = "Nome";
            this.nomeDataGridViewTextBoxColumn.Name = "nomeDataGridViewTextBoxColumn";
            this.nomeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // turnoDataGridViewTextBoxColumn
            // 
            this.turnoDataGridViewTextBoxColumn.DataPropertyName = "Turno";
            this.turnoDataGridViewTextBoxColumn.HeaderText = "Turno";
            this.turnoDataGridViewTextBoxColumn.Name = "turnoDataGridViewTextBoxColumn";
            this.turnoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tipoPessoaDataGridViewTextBoxColumn
            // 
            this.tipoPessoaDataGridViewTextBoxColumn.DataPropertyName = "Tipo_Pessoa";
            this.tipoPessoaDataGridViewTextBoxColumn.HeaderText = "Tipo_Pessoa";
            this.tipoPessoaDataGridViewTextBoxColumn.Name = "tipoPessoaDataGridViewTextBoxColumn";
            this.tipoPessoaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cgcDataGridViewTextBoxColumn
            // 
            this.cgcDataGridViewTextBoxColumn.DataPropertyName = "Cgc";
            this.cgcDataGridViewTextBoxColumn.HeaderText = "Cgc";
            this.cgcDataGridViewTextBoxColumn.Name = "cgcDataGridViewTextBoxColumn";
            this.cgcDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // inscricaoDataGridViewTextBoxColumn
            // 
            this.inscricaoDataGridViewTextBoxColumn.DataPropertyName = "Inscricao";
            this.inscricaoDataGridViewTextBoxColumn.HeaderText = "Inscricao";
            this.inscricaoDataGridViewTextBoxColumn.Name = "inscricaoDataGridViewTextBoxColumn";
            this.inscricaoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // enderecoDataGridViewTextBoxColumn
            // 
            this.enderecoDataGridViewTextBoxColumn.DataPropertyName = "Endereco";
            this.enderecoDataGridViewTextBoxColumn.HeaderText = "Endereco";
            this.enderecoDataGridViewTextBoxColumn.Name = "enderecoDataGridViewTextBoxColumn";
            this.enderecoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // bairroDataGridViewTextBoxColumn
            // 
            this.bairroDataGridViewTextBoxColumn.DataPropertyName = "Bairro";
            this.bairroDataGridViewTextBoxColumn.HeaderText = "Bairro";
            this.bairroDataGridViewTextBoxColumn.Name = "bairroDataGridViewTextBoxColumn";
            this.bairroDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cidadeDataGridViewTextBoxColumn
            // 
            this.cidadeDataGridViewTextBoxColumn.DataPropertyName = "Cidade";
            this.cidadeDataGridViewTextBoxColumn.HeaderText = "Cidade";
            this.cidadeDataGridViewTextBoxColumn.Name = "cidadeDataGridViewTextBoxColumn";
            this.cidadeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // estadoDataGridViewTextBoxColumn
            // 
            this.estadoDataGridViewTextBoxColumn.DataPropertyName = "Estado";
            this.estadoDataGridViewTextBoxColumn.HeaderText = "Estado";
            this.estadoDataGridViewTextBoxColumn.Name = "estadoDataGridViewTextBoxColumn";
            this.estadoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cepDataGridViewTextBoxColumn
            // 
            this.cepDataGridViewTextBoxColumn.DataPropertyName = "Cep";
            this.cepDataGridViewTextBoxColumn.HeaderText = "Cep";
            this.cepDataGridViewTextBoxColumn.Name = "cepDataGridViewTextBoxColumn";
            this.cepDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs01DataGridViewTextBoxColumn
            // 
            this.obs01DataGridViewTextBoxColumn.DataPropertyName = "obs01";
            this.obs01DataGridViewTextBoxColumn.HeaderText = "obs01";
            this.obs01DataGridViewTextBoxColumn.Name = "obs01DataGridViewTextBoxColumn";
            this.obs01DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs02DataGridViewTextBoxColumn
            // 
            this.obs02DataGridViewTextBoxColumn.DataPropertyName = "obs02";
            this.obs02DataGridViewTextBoxColumn.HeaderText = "obs02";
            this.obs02DataGridViewTextBoxColumn.Name = "obs02DataGridViewTextBoxColumn";
            this.obs02DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs03DataGridViewTextBoxColumn
            // 
            this.obs03DataGridViewTextBoxColumn.DataPropertyName = "obs03";
            this.obs03DataGridViewTextBoxColumn.HeaderText = "obs03";
            this.obs03DataGridViewTextBoxColumn.Name = "obs03DataGridViewTextBoxColumn";
            this.obs03DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs04DataGridViewTextBoxColumn
            // 
            this.obs04DataGridViewTextBoxColumn.DataPropertyName = "obs04";
            this.obs04DataGridViewTextBoxColumn.HeaderText = "obs04";
            this.obs04DataGridViewTextBoxColumn.Name = "obs04DataGridViewTextBoxColumn";
            this.obs04DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs05DataGridViewTextBoxColumn
            // 
            this.obs05DataGridViewTextBoxColumn.DataPropertyName = "obs05";
            this.obs05DataGridViewTextBoxColumn.HeaderText = "obs05";
            this.obs05DataGridViewTextBoxColumn.Name = "obs05DataGridViewTextBoxColumn";
            this.obs05DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs06DataGridViewTextBoxColumn
            // 
            this.obs06DataGridViewTextBoxColumn.DataPropertyName = "obs06";
            this.obs06DataGridViewTextBoxColumn.HeaderText = "obs06";
            this.obs06DataGridViewTextBoxColumn.Name = "obs06DataGridViewTextBoxColumn";
            this.obs06DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs07DataGridViewTextBoxColumn
            // 
            this.obs07DataGridViewTextBoxColumn.DataPropertyName = "obs07";
            this.obs07DataGridViewTextBoxColumn.HeaderText = "obs07";
            this.obs07DataGridViewTextBoxColumn.Name = "obs07DataGridViewTextBoxColumn";
            this.obs07DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs08DataGridViewTextBoxColumn
            // 
            this.obs08DataGridViewTextBoxColumn.DataPropertyName = "obs08";
            this.obs08DataGridViewTextBoxColumn.HeaderText = "obs08";
            this.obs08DataGridViewTextBoxColumn.Name = "obs08DataGridViewTextBoxColumn";
            this.obs08DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs09DataGridViewTextBoxColumn
            // 
            this.obs09DataGridViewTextBoxColumn.DataPropertyName = "obs09";
            this.obs09DataGridViewTextBoxColumn.HeaderText = "obs09";
            this.obs09DataGridViewTextBoxColumn.Name = "obs09DataGridViewTextBoxColumn";
            this.obs09DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs10DataGridViewTextBoxColumn
            // 
            this.obs10DataGridViewTextBoxColumn.DataPropertyName = "obs10";
            this.obs10DataGridViewTextBoxColumn.HeaderText = "obs10";
            this.obs10DataGridViewTextBoxColumn.Name = "obs10DataGridViewTextBoxColumn";
            this.obs10DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // noturnoDataGridViewTextBoxColumn
            // 
            this.noturnoDataGridViewTextBoxColumn.DataPropertyName = "Noturno";
            this.noturnoDataGridViewTextBoxColumn.HeaderText = "Noturno";
            this.noturnoDataGridViewTextBoxColumn.Name = "noturnoDataGridViewTextBoxColumn";
            this.noturnoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // inativoDataGridViewTextBoxColumn
            // 
            this.inativoDataGridViewTextBoxColumn.DataPropertyName = "Inativo";
            this.inativoDataGridViewTextBoxColumn.HeaderText = "Inativo";
            this.inativoDataGridViewTextBoxColumn.Name = "inativoDataGridViewTextBoxColumn";
            this.inativoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dtAlterDataGridViewTextBoxColumn
            // 
            this.dtAlterDataGridViewTextBoxColumn.DataPropertyName = "Dt_Alter";
            this.dtAlterDataGridViewTextBoxColumn.HeaderText = "Dt_Alter";
            this.dtAlterDataGridViewTextBoxColumn.Name = "dtAlterDataGridViewTextBoxColumn";
            this.dtAlterDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs11DataGridViewTextBoxColumn
            // 
            this.obs11DataGridViewTextBoxColumn.DataPropertyName = "Obs11";
            this.obs11DataGridViewTextBoxColumn.HeaderText = "Obs11";
            this.obs11DataGridViewTextBoxColumn.Name = "obs11DataGridViewTextBoxColumn";
            this.obs11DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs12DataGridViewTextBoxColumn
            // 
            this.obs12DataGridViewTextBoxColumn.DataPropertyName = "Obs12";
            this.obs12DataGridViewTextBoxColumn.HeaderText = "Obs12";
            this.obs12DataGridViewTextBoxColumn.Name = "obs12DataGridViewTextBoxColumn";
            this.obs12DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs13DataGridViewTextBoxColumn
            // 
            this.obs13DataGridViewTextBoxColumn.DataPropertyName = "Obs13";
            this.obs13DataGridViewTextBoxColumn.HeaderText = "Obs13";
            this.obs13DataGridViewTextBoxColumn.Name = "obs13DataGridViewTextBoxColumn";
            this.obs13DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs14DataGridViewTextBoxColumn
            // 
            this.obs14DataGridViewTextBoxColumn.DataPropertyName = "Obs14";
            this.obs14DataGridViewTextBoxColumn.HeaderText = "Obs14";
            this.obs14DataGridViewTextBoxColumn.Name = "obs14DataGridViewTextBoxColumn";
            this.obs14DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs15DataGridViewTextBoxColumn
            // 
            this.obs15DataGridViewTextBoxColumn.DataPropertyName = "Obs15";
            this.obs15DataGridViewTextBoxColumn.HeaderText = "Obs15";
            this.obs15DataGridViewTextBoxColumn.Name = "obs15DataGridViewTextBoxColumn";
            this.obs15DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs16DataGridViewTextBoxColumn
            // 
            this.obs16DataGridViewTextBoxColumn.DataPropertyName = "Obs16";
            this.obs16DataGridViewTextBoxColumn.HeaderText = "Obs16";
            this.obs16DataGridViewTextBoxColumn.Name = "obs16DataGridViewTextBoxColumn";
            this.obs16DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs17DataGridViewTextBoxColumn
            // 
            this.obs17DataGridViewTextBoxColumn.DataPropertyName = "Obs17";
            this.obs17DataGridViewTextBoxColumn.HeaderText = "Obs17";
            this.obs17DataGridViewTextBoxColumn.Name = "obs17DataGridViewTextBoxColumn";
            this.obs17DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs18DataGridViewTextBoxColumn
            // 
            this.obs18DataGridViewTextBoxColumn.DataPropertyName = "Obs18";
            this.obs18DataGridViewTextBoxColumn.HeaderText = "Obs18";
            this.obs18DataGridViewTextBoxColumn.Name = "obs18DataGridViewTextBoxColumn";
            this.obs18DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs19DataGridViewTextBoxColumn
            // 
            this.obs19DataGridViewTextBoxColumn.DataPropertyName = "Obs19";
            this.obs19DataGridViewTextBoxColumn.HeaderText = "Obs19";
            this.obs19DataGridViewTextBoxColumn.Name = "obs19DataGridViewTextBoxColumn";
            this.obs19DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs20DataGridViewTextBoxColumn
            // 
            this.obs20DataGridViewTextBoxColumn.DataPropertyName = "Obs20";
            this.obs20DataGridViewTextBoxColumn.HeaderText = "Obs20";
            this.obs20DataGridViewTextBoxColumn.Name = "obs20DataGridViewTextBoxColumn";
            this.obs20DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // rFIDDataGridViewTextBoxColumn
            // 
            this.rFIDDataGridViewTextBoxColumn.DataPropertyName = "RFID";
            this.rFIDDataGridViewTextBoxColumn.HeaderText = "RFID";
            this.rFIDDataGridViewTextBoxColumn.Name = "rFIDDataGridViewTextBoxColumn";
            this.rFIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // biometriaDataGridViewTextBoxColumn
            // 
            this.biometriaDataGridViewTextBoxColumn.DataPropertyName = "Biometria";
            this.biometriaDataGridViewTextBoxColumn.HeaderText = "Biometria";
            this.biometriaDataGridViewTextBoxColumn.Name = "biometriaDataGridViewTextBoxColumn";
            this.biometriaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cTACaixaDataGridViewTextBoxColumn
            // 
            this.cTACaixaDataGridViewTextBoxColumn.DataPropertyName = "CTA_Caixa";
            this.cTACaixaDataGridViewTextBoxColumn.HeaderText = "CTA_Caixa";
            this.cTACaixaDataGridViewTextBoxColumn.Name = "cTACaixaDataGridViewTextBoxColumn";
            this.cTACaixaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tipoVIPDataGridViewTextBoxColumn
            // 
            this.tipoVIPDataGridViewTextBoxColumn.DataPropertyName = "Tipo_VIP";
            this.tipoVIPDataGridViewTextBoxColumn.HeaderText = "Tipo_VIP";
            this.tipoVIPDataGridViewTextBoxColumn.Name = "tipoVIPDataGridViewTextBoxColumn";
            this.tipoVIPDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // uNomeDataGridViewTextBoxColumn
            // 
            this.uNomeDataGridViewTextBoxColumn.DataPropertyName = "uNome";
            this.uNomeDataGridViewTextBoxColumn.HeaderText = "uNome";
            this.uNomeDataGridViewTextBoxColumn.Name = "uNomeDataGridViewTextBoxColumn";
            this.uNomeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // uSenhaDataGridViewTextBoxColumn
            // 
            this.uSenhaDataGridViewTextBoxColumn.DataPropertyName = "uSenha";
            this.uSenhaDataGridViewTextBoxColumn.HeaderText = "uSenha";
            this.uSenhaDataGridViewTextBoxColumn.Name = "uSenhaDataGridViewTextBoxColumn";
            this.uSenhaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tipoacessoDataGridViewTextBoxColumn
            // 
            this.tipoacessoDataGridViewTextBoxColumn.DataPropertyName = "tipo_acesso";
            this.tipoacessoDataGridViewTextBoxColumn.HeaderText = "tipo_acesso";
            this.tipoacessoDataGridViewTextBoxColumn.Name = "tipoacessoDataGridViewTextBoxColumn";
            this.tipoacessoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dtcadastroDataGridViewTextBoxColumn
            // 
            this.dtcadastroDataGridViewTextBoxColumn.DataPropertyName = "dt_cadastro";
            this.dtcadastroDataGridViewTextBoxColumn.HeaderText = "dt_cadastro";
            this.dtcadastroDataGridViewTextBoxColumn.Name = "dtcadastroDataGridViewTextBoxColumn";
            this.dtcadastroDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // ultimoacessoDataGridViewTextBoxColumn
            // 
            this.ultimoacessoDataGridViewTextBoxColumn.DataPropertyName = "ultimo_acesso";
            this.ultimoacessoDataGridViewTextBoxColumn.HeaderText = "ultimo_acesso";
            this.ultimoacessoDataGridViewTextBoxColumn.Name = "ultimoacessoDataGridViewTextBoxColumn";
            this.ultimoacessoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // planocontaDataGridViewTextBoxColumn
            // 
            this.planocontaDataGridViewTextBoxColumn.DataPropertyName = "plano_conta";
            this.planocontaDataGridViewTextBoxColumn.HeaderText = "plano_conta";
            this.planocontaDataGridViewTextBoxColumn.Name = "planocontaDataGridViewTextBoxColumn";
            this.planocontaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // acessapedidoDataGridViewCheckBoxColumn
            // 
            this.acessapedidoDataGridViewCheckBoxColumn.DataPropertyName = "acessa_pedido";
            this.acessapedidoDataGridViewCheckBoxColumn.HeaderText = "acessa_pedido";
            this.acessapedidoDataGridViewCheckBoxColumn.Name = "acessapedidoDataGridViewCheckBoxColumn";
            this.acessapedidoDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // digitaselolacreDataGridViewCheckBoxColumn
            // 
            this.digitaselolacreDataGridViewCheckBoxColumn.DataPropertyName = "digita_selo_lacre";
            this.digitaselolacreDataGridViewCheckBoxColumn.HeaderText = "digita_selo_lacre";
            this.digitaselolacreDataGridViewCheckBoxColumn.Name = "digitaselolacreDataGridViewCheckBoxColumn";
            this.digitaselolacreDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // idusuarioDataGridViewTextBoxColumn
            // 
            this.idusuarioDataGridViewTextBoxColumn.DataPropertyName = "idusuario";
            this.idusuarioDataGridViewTextBoxColumn.HeaderText = "idusuario";
            this.idusuarioDataGridViewTextBoxColumn.Name = "idusuarioDataGridViewTextBoxColumn";
            this.idusuarioDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // isusuarioDataGridViewTextBoxColumn
            // 
            this.isusuarioDataGridViewTextBoxColumn.DataPropertyName = "isusuario";
            this.isusuarioDataGridViewTextBoxColumn.HeaderText = "isusuario";
            this.isusuarioDataGridViewTextBoxColumn.Name = "isusuarioDataGridViewTextBoxColumn";
            this.isusuarioDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // bsXOSFuncionario
            // 
            this.bsXOSFuncionario.DataMember = "XOsFuncionario";
            this.bsXOSFuncionario.DataSource = this.dtsXOSFuncionario;
            // 
            // dtsXOSFuncionario
            // 
            this.dtsXOSFuncionario.DataSetName = "dtsXOSFuncionario";
            this.dtsXOSFuncionario.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // xOsFuncionarioTableAdapter
            // 
            this.xOsFuncionarioTableAdapter.ClearBeforeFill = true;
            // 
            // FuncionarioBindingNavigator
            // 
            this.FuncionarioBindingNavigator.AddNewItem = this.bnInserir;
            this.FuncionarioBindingNavigator.BindingSource = this.bsXOSFuncionario;
            this.FuncionarioBindingNavigator.CountItem = this.bnCount;
            this.FuncionarioBindingNavigator.CountItemFormat = "de {0}";
            this.FuncionarioBindingNavigator.DeleteItem = null;
            this.FuncionarioBindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.FuncionarioBindingNavigator.ImageScalingSize = new System.Drawing.Size(48, 48);
            this.FuncionarioBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bnConsultar,
            this.toolStripSeparator3,
            this.bnInserir,
            this.bnGravar,
            this.bnCancelar,
            this.toolStripSeparator4,
            this.bnPrimeiro,
            this.bnAnterior,
            this.toolStripSeparator1,
            this.bnPosition,
            this.bnCount,
            this.toolStripSeparator2,
            this.bnProximo,
            this.bnUltimo,
            this.bnDescartar});
            this.FuncionarioBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.FuncionarioBindingNavigator.MoveFirstItem = this.bnPrimeiro;
            this.FuncionarioBindingNavigator.MoveLastItem = this.bnUltimo;
            this.FuncionarioBindingNavigator.MoveNextItem = this.bnProximo;
            this.FuncionarioBindingNavigator.MovePreviousItem = this.bnAnterior;
            this.FuncionarioBindingNavigator.Name = "FuncionarioBindingNavigator";
            this.FuncionarioBindingNavigator.PositionItem = this.bnPosition;
            this.FuncionarioBindingNavigator.Size = new System.Drawing.Size(1184, 70);
            this.FuncionarioBindingNavigator.TabIndex = 15;
            this.FuncionarioBindingNavigator.Text = "FuncionarioBindingNavigator";
            // 
            // bnInserir
            // 
            this.bnInserir.Image = global::osExpress.Properties.Resources.insert;
            this.bnInserir.Name = "bnInserir";
            this.bnInserir.RightToLeftAutoMirrorImage = true;
            this.bnInserir.Size = new System.Drawing.Size(52, 67);
            this.bnInserir.Text = "&Inserir";
            this.bnInserir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnInserir.Click += new System.EventHandler(this.bnInserir_Click);
            // 
            // bnCount
            // 
            this.bnCount.Name = "bnCount";
            this.bnCount.Size = new System.Drawing.Size(37, 67);
            this.bnCount.Text = "de {0}";
            this.bnCount.ToolTipText = "Total number of items";
            // 
            // bnConsultar
            // 
            this.bnConsultar.Image = global::osExpress.Properties.Resources.search;
            this.bnConsultar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnConsultar.Name = "bnConsultar";
            this.bnConsultar.Size = new System.Drawing.Size(62, 67);
            this.bnConsultar.Text = "Consultar";
            this.bnConsultar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnConsultar.Click += new System.EventHandler(this.bnConsultar_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 70);
            // 
            // bnGravar
            // 
            this.bnGravar.Image = global::osExpress.Properties.Resources.save;
            this.bnGravar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnGravar.Name = "bnGravar";
            this.bnGravar.Size = new System.Drawing.Size(52, 67);
            this.bnGravar.Text = "Grav&ar";
            this.bnGravar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnGravar.Click += new System.EventHandler(this.bnGravar_Click);
            // 
            // bnCancelar
            // 
            this.bnCancelar.Enabled = false;
            this.bnCancelar.Image = global::osExpress.Properties.Resources.clean;
            this.bnCancelar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnCancelar.Name = "bnCancelar";
            this.bnCancelar.Size = new System.Drawing.Size(57, 67);
            this.bnCancelar.Text = "Cancelar";
            this.bnCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnCancelar.Click += new System.EventHandler(this.bnCancelar_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 70);
            // 
            // bnPrimeiro
            // 
            this.bnPrimeiro.Image = global::osExpress.Properties.Resources.first;
            this.bnPrimeiro.Name = "bnPrimeiro";
            this.bnPrimeiro.RightToLeftAutoMirrorImage = true;
            this.bnPrimeiro.Size = new System.Drawing.Size(56, 67);
            this.bnPrimeiro.Text = "&Primeiro";
            this.bnPrimeiro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnPrimeiro.Click += new System.EventHandler(this.bnPrimeiro_Click);
            // 
            // bnAnterior
            // 
            this.bnAnterior.Image = global::osExpress.Properties.Resources.previous;
            this.bnAnterior.Name = "bnAnterior";
            this.bnAnterior.RightToLeftAutoMirrorImage = true;
            this.bnAnterior.Size = new System.Drawing.Size(54, 67);
            this.bnAnterior.Text = "Anteri&or";
            this.bnAnterior.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnAnterior.Click += new System.EventHandler(this.bnAnterior_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 70);
            // 
            // bnPosition
            // 
            this.bnPosition.AccessibleName = "Position";
            this.bnPosition.AutoSize = false;
            this.bnPosition.Name = "bnPosition";
            this.bnPosition.Size = new System.Drawing.Size(50, 23);
            this.bnPosition.Text = "0";
            this.bnPosition.ToolTipText = "Current position";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 70);
            // 
            // bnProximo
            // 
            this.bnProximo.Image = global::osExpress.Properties.Resources.next;
            this.bnProximo.Name = "bnProximo";
            this.bnProximo.RightToLeftAutoMirrorImage = true;
            this.bnProximo.Size = new System.Drawing.Size(56, 67);
            this.bnProximo.Text = "Próxi&mo";
            this.bnProximo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnProximo.Click += new System.EventHandler(this.bnProximo_Click);
            // 
            // bnUltimo
            // 
            this.bnUltimo.Image = global::osExpress.Properties.Resources.last;
            this.bnUltimo.Name = "bnUltimo";
            this.bnUltimo.RightToLeftAutoMirrorImage = true;
            this.bnUltimo.Size = new System.Drawing.Size(52, 67);
            this.bnUltimo.Text = "&Último";
            this.bnUltimo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnUltimo.Click += new System.EventHandler(this.bnUltimo_Click);
            // 
            // bnDescartar
            // 
            this.bnDescartar.Enabled = false;
            this.bnDescartar.Image = global::osExpress.Properties.Resources.remove;
            this.bnDescartar.Name = "bnDescartar";
            this.bnDescartar.RightToLeftAutoMirrorImage = true;
            this.bnDescartar.Size = new System.Drawing.Size(60, 67);
            this.bnDescartar.Text = "&Descartar";
            this.bnDescartar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnDescartar.Click += new System.EventHandler(this.bnDescartar_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.AutoScroll = true;
            this.tabPage1.Controls.Add(this.labelSenha);
            this.tabPage1.Controls.Add(this.labelLogin);
            this.tabPage1.Controls.Add(this.tbSenha);
            this.tabPage1.Controls.Add(this.tbLogin);
            this.tabPage1.Controls.Add(this.checkboxIsUsuario);
            this.tabPage1.Controls.Add(label_rg);
            this.tabPage1.Controls.Add(this.tb_rg);
            this.tabPage1.Controls.Add(this.tabChild);
            this.tabPage1.Controls.Add(labelNome);
            this.tabPage1.Controls.Add(this.tbNome);
            this.tabPage1.Controls.Add(nOME_FANTASIALabel);
            this.tabPage1.Controls.Add(this.nOME_FANTASIATextBox);
            this.tabPage1.Controls.Add(this.mtbCPF);
            this.tabPage1.Controls.Add(this.mtbCNPJ);
            this.tabPage1.Controls.Add(this.comboBox2);
            this.tabPage1.Controls.Add(tIPO_PESSOALabel);
            this.tabPage1.Controls.Add(iDLabel);
            this.tabPage1.Controls.Add(this.tbIdusuario);
            this.tabPage1.Controls.Add(cNPJLabel);
            this.tabPage1.Controls.Add(labelEmail);
            this.tabPage1.Controls.Add(this.tb_email);
            this.tabPage1.Controls.Add(cPFLabel);
            this.tabPage1.Controls.Add(this.cPFTextBox);
            this.tabPage1.Controls.Add(this.cNPJTextBox);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(952, 554);
            this.tabPage1.TabIndex = 1;
            this.tabPage1.Text = " Dados";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // labelSenha
            // 
            this.labelSenha.AutoSize = true;
            this.labelSenha.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSenha.Location = new System.Drawing.Point(732, 139);
            this.labelSenha.Name = "labelSenha";
            this.labelSenha.Size = new System.Drawing.Size(47, 13);
            this.labelSenha.TabIndex = 61;
            this.labelSenha.Text = "Senha:";
            // 
            // labelLogin
            // 
            this.labelLogin.AutoSize = true;
            this.labelLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLogin.Location = new System.Drawing.Point(732, 98);
            this.labelLogin.Name = "labelLogin";
            this.labelLogin.Size = new System.Drawing.Size(42, 13);
            this.labelLogin.TabIndex = 60;
            this.labelLogin.Text = "Login:";
            // 
            // tbSenha
            // 
            this.tbSenha.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsXOSFuncionario, "uSenha", true));
            this.tbSenha.Location = new System.Drawing.Point(732, 155);
            this.tbSenha.Name = "tbSenha";
            this.tbSenha.Size = new System.Drawing.Size(142, 20);
            this.tbSenha.TabIndex = 59;
            // 
            // tbLogin
            // 
            this.tbLogin.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsXOSFuncionario, "uNome", true));
            this.tbLogin.Location = new System.Drawing.Point(732, 114);
            this.tbLogin.Name = "tbLogin";
            this.tbLogin.Size = new System.Drawing.Size(142, 20);
            this.tbLogin.TabIndex = 58;
            // 
            // checkboxIsUsuario
            // 
            this.checkboxIsUsuario.AutoSize = true;
            this.checkboxIsUsuario.Checked = true;
            this.checkboxIsUsuario.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkboxIsUsuario.Location = new System.Drawing.Point(732, 70);
            this.checkboxIsUsuario.Name = "checkboxIsUsuario";
            this.checkboxIsUsuario.Size = new System.Drawing.Size(78, 17);
            this.checkboxIsUsuario.TabIndex = 57;
            this.checkboxIsUsuario.Text = "É Usuário?";
            this.checkboxIsUsuario.UseVisualStyleBackColor = true;
            this.checkboxIsUsuario.CheckedChanged += new System.EventHandler(this.checkboxIsUsuario_CheckedChanged);
            // 
            // tb_rg
            // 
            this.tb_rg.Location = new System.Drawing.Point(360, 25);
            this.tb_rg.MaxLength = 50;
            this.tb_rg.Name = "tb_rg";
            this.tb_rg.Size = new System.Drawing.Size(320, 20);
            this.tb_rg.TabIndex = 53;
            this.tb_rg.Text = " ";
            // 
            // tabChild
            // 
            this.tabChild.Controls.Add(this.tabEndereco);
            this.tabChild.Controls.Add(this.tabOrcamentos);
            this.tabChild.Controls.Add(this.tabPedidos);
            this.tabChild.Controls.Add(this.tabServicos);
            this.tabChild.Controls.Add(this.tabInstrumentos);
            this.tabChild.Location = new System.Drawing.Point(10, 207);
            this.tabChild.Name = "tabChild";
            this.tabChild.SelectedIndex = 0;
            this.tabChild.Size = new System.Drawing.Size(980, 248);
            this.tabChild.TabIndex = 51;
            // 
            // tabEndereco
            // 
            this.tabEndereco.Controls.Add(this.tELEFONETextBox);
            this.tabEndereco.Controls.Add(fAXLabel);
            this.tabEndereco.Controls.Add(this.fAXTextBox);
            this.tabEndereco.Controls.Add(this.mtbTelefone);
            this.tabEndereco.Controls.Add(tELEFONELabel);
            this.tabEndereco.Controls.Add(bAIRROLabel);
            this.tabEndereco.Controls.Add(this.tbBairro);
            this.tabEndereco.Controls.Add(eNDERECOLabel);
            this.tabEndereco.Controls.Add(this.tbEndereco);
            this.tabEndereco.Controls.Add(cIDADELabel);
            this.tabEndereco.Controls.Add(this.tbCidade);
            this.tabEndereco.Controls.Add(this.comboBox1);
            this.tabEndereco.Controls.Add(uFLabel);
            this.tabEndereco.Controls.Add(this.labelCep);
            this.tabEndereco.Controls.Add(this.tbCep);
            this.tabEndereco.Location = new System.Drawing.Point(4, 22);
            this.tabEndereco.Name = "tabEndereco";
            this.tabEndereco.Padding = new System.Windows.Forms.Padding(3);
            this.tabEndereco.Size = new System.Drawing.Size(972, 222);
            this.tabEndereco.TabIndex = 0;
            this.tabEndereco.Text = "Endereço";
            this.tabEndereco.UseVisualStyleBackColor = true;
            // 
            // tELEFONETextBox
            // 
            this.tELEFONETextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsXOSFuncionario, "obs02", true));
            this.tELEFONETextBox.Location = new System.Drawing.Point(6, 167);
            this.tELEFONETextBox.MaxLength = 20;
            this.tELEFONETextBox.Name = "tELEFONETextBox";
            this.tELEFONETextBox.Size = new System.Drawing.Size(220, 20);
            this.tELEFONETextBox.TabIndex = 64;
            this.tELEFONETextBox.TabStop = false;
            // 
            // fAXTextBox
            // 
            this.fAXTextBox.Location = new System.Drawing.Point(300, 110);
            this.fAXTextBox.MaxLength = 20;
            this.fAXTextBox.Name = "fAXTextBox";
            this.fAXTextBox.Size = new System.Drawing.Size(243, 20);
            this.fAXTextBox.TabIndex = 65;
            this.fAXTextBox.Visible = false;
            // 
            // mtbTelefone
            // 
            this.mtbTelefone.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsXOSFuncionario, "obs02", true));
            this.mtbTelefone.Location = new System.Drawing.Point(10, 110);
            this.mtbTelefone.Mask = "(##)####-####";
            this.mtbTelefone.Name = "mtbTelefone";
            this.mtbTelefone.Size = new System.Drawing.Size(243, 20);
            this.mtbTelefone.TabIndex = 62;
            // 
            // tbBairro
            // 
            this.tbBairro.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsXOSFuncionario, "Bairro", true));
            this.tbBairro.Location = new System.Drawing.Point(450, 70);
            this.tbBairro.MaxLength = 30;
            this.tbBairro.Name = "tbBairro";
            this.tbBairro.Size = new System.Drawing.Size(246, 20);
            this.tbBairro.TabIndex = 5;
            // 
            // tbEndereco
            // 
            this.tbEndereco.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsXOSFuncionario, "Endereco", true));
            this.tbEndereco.Location = new System.Drawing.Point(10, 70);
            this.tbEndereco.MaxLength = 50;
            this.tbEndereco.Name = "tbEndereco";
            this.tbEndereco.Size = new System.Drawing.Size(408, 20);
            this.tbEndereco.TabIndex = 4;
            // 
            // tbCidade
            // 
            this.tbCidade.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsXOSFuncionario, "Cidade", true));
            this.tbCidade.Location = new System.Drawing.Point(240, 25);
            this.tbCidade.MaxLength = 30;
            this.tbCidade.Name = "tbCidade";
            this.tbCidade.Size = new System.Drawing.Size(456, 20);
            this.tbCidade.TabIndex = 3;
            // 
            // comboBox1
            // 
            this.comboBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox1.DataBindings.Add(new System.Windows.Forms.Binding("SelectedItem", this.bsXOSFuncionario, "Estado", true));
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.ItemHeight = 13;
            this.comboBox1.Items.AddRange(new object[] {
            "AC",
            "AL",
            "AP",
            "AM",
            "BA",
            "CE",
            "DF",
            "ES",
            "GO",
            "MA",
            "MT",
            "MS",
            "MG",
            "PA",
            "PB",
            "PR",
            "PE",
            "PI",
            "RJ",
            "RN",
            "RS",
            "RO",
            "RR",
            "SC",
            "SP",
            "SE",
            "TO"});
            this.comboBox1.Location = new System.Drawing.Point(170, 25);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(50, 21);
            this.comboBox1.TabIndex = 2;
            // 
            // tbCep
            // 
            this.tbCep.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsXOSFuncionario, "Cep", true));
            this.tbCep.Location = new System.Drawing.Point(10, 25);
            this.tbCep.MaxLength = 8;
            this.tbCep.Name = "tbCep";
            this.tbCep.Size = new System.Drawing.Size(134, 20);
            this.tbCep.TabIndex = 1;
            // 
            // tabOrcamentos
            // 
            this.tabOrcamentos.Controls.Add(this.pnlOrcamentos);
            this.tabOrcamentos.Location = new System.Drawing.Point(4, 22);
            this.tabOrcamentos.Name = "tabOrcamentos";
            this.tabOrcamentos.Size = new System.Drawing.Size(972, 222);
            this.tabOrcamentos.TabIndex = 5;
            this.tabOrcamentos.Text = "Orçamentos";
            this.tabOrcamentos.UseVisualStyleBackColor = true;
            // 
            // pnlOrcamentos
            // 
            this.pnlOrcamentos.Controls.Add(this.pnlOrcamentosGrid);
            this.pnlOrcamentos.Controls.Add(this.pnlOrcamentosFilter);
            this.pnlOrcamentos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlOrcamentos.Location = new System.Drawing.Point(0, 0);
            this.pnlOrcamentos.Name = "pnlOrcamentos";
            this.pnlOrcamentos.Size = new System.Drawing.Size(972, 222);
            this.pnlOrcamentos.TabIndex = 1;
            // 
            // pnlOrcamentosGrid
            // 
            this.pnlOrcamentosGrid.Controls.Add(this.dgvOrcamentos);
            this.pnlOrcamentosGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlOrcamentosGrid.Location = new System.Drawing.Point(0, 82);
            this.pnlOrcamentosGrid.Name = "pnlOrcamentosGrid";
            this.pnlOrcamentosGrid.Size = new System.Drawing.Size(972, 140);
            this.pnlOrcamentosGrid.TabIndex = 1;
            // 
            // dgvOrcamentos
            // 
            this.dgvOrcamentos.AllowUserToAddRows = false;
            this.dgvOrcamentos.AllowUserToDeleteRows = false;
            this.dgvOrcamentos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvOrcamentos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvOrcamentos.Location = new System.Drawing.Point(0, 0);
            this.dgvOrcamentos.Name = "dgvOrcamentos";
            this.dgvOrcamentos.Size = new System.Drawing.Size(972, 140);
            this.dgvOrcamentos.TabIndex = 0;
            // 
            // pnlOrcamentosFilter
            // 
            this.pnlOrcamentosFilter.BackColor = System.Drawing.Color.BurlyWood;
            this.pnlOrcamentosFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlOrcamentosFilter.Location = new System.Drawing.Point(0, 0);
            this.pnlOrcamentosFilter.Name = "pnlOrcamentosFilter";
            this.pnlOrcamentosFilter.Size = new System.Drawing.Size(972, 82);
            this.pnlOrcamentosFilter.TabIndex = 0;
            // 
            // tabPedidos
            // 
            this.tabPedidos.Controls.Add(this.pnlPedidos);
            this.tabPedidos.Location = new System.Drawing.Point(4, 22);
            this.tabPedidos.Name = "tabPedidos";
            this.tabPedidos.Size = new System.Drawing.Size(972, 222);
            this.tabPedidos.TabIndex = 6;
            this.tabPedidos.Text = "Pedidos";
            this.tabPedidos.UseVisualStyleBackColor = true;
            // 
            // tabServicos
            // 
            this.tabServicos.Controls.Add(this.pnlServicos);
            this.tabServicos.Location = new System.Drawing.Point(4, 22);
            this.tabServicos.Name = "tabServicos";
            this.tabServicos.Size = new System.Drawing.Size(972, 222);
            this.tabServicos.TabIndex = 7;
            this.tabServicos.Text = "Serviços";
            this.tabServicos.UseVisualStyleBackColor = true;
            // 
            // tabInstrumentos
            // 
            this.tabInstrumentos.Controls.Add(this.pnlInstrumentos);
            this.tabInstrumentos.Location = new System.Drawing.Point(4, 22);
            this.tabInstrumentos.Name = "tabInstrumentos";
            this.tabInstrumentos.Size = new System.Drawing.Size(972, 222);
            this.tabInstrumentos.TabIndex = 8;
            this.tabInstrumentos.Text = "Instrumentos";
            this.tabInstrumentos.UseVisualStyleBackColor = true;
            // 
            // pnlInstrumentos
            // 
            this.pnlInstrumentos.Controls.Add(this.pnlInstrumentosGrid);
            this.pnlInstrumentos.Controls.Add(this.pnlInstrumentosFilter);
            this.pnlInstrumentos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlInstrumentos.Location = new System.Drawing.Point(0, 0);
            this.pnlInstrumentos.Name = "pnlInstrumentos";
            this.pnlInstrumentos.Size = new System.Drawing.Size(972, 222);
            this.pnlInstrumentos.TabIndex = 0;
            // 
            // pnlInstrumentosGrid
            // 
            this.pnlInstrumentosGrid.Controls.Add(this.dgvInstrumentos);
            this.pnlInstrumentosGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlInstrumentosGrid.Location = new System.Drawing.Point(0, 82);
            this.pnlInstrumentosGrid.Name = "pnlInstrumentosGrid";
            this.pnlInstrumentosGrid.Size = new System.Drawing.Size(972, 140);
            this.pnlInstrumentosGrid.TabIndex = 1;
            // 
            // dgvInstrumentos
            // 
            this.dgvInstrumentos.AllowUserToAddRows = false;
            this.dgvInstrumentos.AllowUserToDeleteRows = false;
            this.dgvInstrumentos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvInstrumentos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvInstrumentos.Location = new System.Drawing.Point(0, 0);
            this.dgvInstrumentos.Name = "dgvInstrumentos";
            this.dgvInstrumentos.Size = new System.Drawing.Size(972, 140);
            this.dgvInstrumentos.TabIndex = 0;
            // 
            // pnlInstrumentosFilter
            // 
            this.pnlInstrumentosFilter.BackColor = System.Drawing.Color.BurlyWood;
            this.pnlInstrumentosFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlInstrumentosFilter.Location = new System.Drawing.Point(0, 0);
            this.pnlInstrumentosFilter.Name = "pnlInstrumentosFilter";
            this.pnlInstrumentosFilter.Size = new System.Drawing.Size(972, 82);
            this.pnlInstrumentosFilter.TabIndex = 0;
            // 
            // tbNome
            // 
            this.tbNome.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsXOSFuncionario, "Nome", true));
            this.tbNome.Location = new System.Drawing.Point(100, 115);
            this.tbNome.MaxLength = 50;
            this.tbNome.Name = "tbNome";
            this.tbNome.Size = new System.Drawing.Size(610, 20);
            this.tbNome.TabIndex = 4;
            // 
            // nOME_FANTASIATextBox
            // 
            this.nOME_FANTASIATextBox.Location = new System.Drawing.Point(100, 155);
            this.nOME_FANTASIATextBox.MaxLength = 20;
            this.nOME_FANTASIATextBox.Name = "nOME_FANTASIATextBox";
            this.nOME_FANTASIATextBox.Size = new System.Drawing.Size(610, 20);
            this.nOME_FANTASIATextBox.TabIndex = 5;
            this.nOME_FANTASIATextBox.Visible = false;
            // 
            // mtbCPF
            // 
            this.mtbCPF.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsXOSFuncionario, "Cgc", true));
            this.mtbCPF.Location = new System.Drawing.Point(110, 25);
            this.mtbCPF.Mask = "###.###.###-##";
            this.mtbCPF.Name = "mtbCPF";
            this.mtbCPF.Size = new System.Drawing.Size(220, 20);
            this.mtbCPF.TabIndex = 2;
            // 
            // mtbCNPJ
            // 
            this.mtbCNPJ.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsXOSFuncionario, "Cgc", true));
            this.mtbCNPJ.Location = new System.Drawing.Point(110, 70);
            this.mtbCNPJ.Mask = "##.###.###/####-##";
            this.mtbCNPJ.Name = "mtbCNPJ";
            this.mtbCNPJ.Size = new System.Drawing.Size(220, 20);
            this.mtbCNPJ.TabIndex = 2;
            this.mtbCNPJ.Visible = false;
            // 
            // comboBox2
            // 
            this.comboBox2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox2.DataBindings.Add(new System.Windows.Forms.Binding("SelectedItem", this.bsXOSFuncionario, "Tipo_Pessoa", true));
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "F",
            "J"});
            this.comboBox2.Location = new System.Drawing.Point(25, 25);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(38, 21);
            this.comboBox2.TabIndex = 1;
            this.comboBox2.Visible = false;
            this.comboBox2.SelectionChangeCommitted += new System.EventHandler(this.comboBox2_SelectionChangeCommitted);
            // 
            // tbIdusuario
            // 
            this.tbIdusuario.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsXOSFuncionario, "idusuario", true));
            this.tbIdusuario.Enabled = false;
            this.tbIdusuario.Location = new System.Drawing.Point(25, 115);
            this.tbIdusuario.Name = "tbIdusuario";
            this.tbIdusuario.ReadOnly = true;
            this.tbIdusuario.Size = new System.Drawing.Size(50, 20);
            this.tbIdusuario.TabIndex = 1;
            // 
            // tb_email
            // 
            this.tb_email.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsXOSFuncionario, "obs01", true));
            this.tb_email.Location = new System.Drawing.Point(360, 70);
            this.tb_email.MaxLength = 100;
            this.tb_email.Name = "tb_email";
            this.tb_email.Size = new System.Drawing.Size(320, 20);
            this.tb_email.TabIndex = 3;
            // 
            // cPFTextBox
            // 
            this.cPFTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsXOSFuncionario, "Cgc", true));
            this.cPFTextBox.Location = new System.Drawing.Point(10, 461);
            this.cPFTextBox.MaxLength = 11;
            this.cPFTextBox.Name = "cPFTextBox";
            this.cPFTextBox.Size = new System.Drawing.Size(220, 20);
            this.cPFTextBox.TabIndex = 0;
            this.cPFTextBox.TabStop = false;
            // 
            // cNPJTextBox
            // 
            this.cNPJTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsXOSFuncionario, "Cgc", true));
            this.cNPJTextBox.Location = new System.Drawing.Point(240, 461);
            this.cNPJTextBox.MaxLength = 15;
            this.cNPJTextBox.Name = "cNPJTextBox";
            this.cNPJTextBox.Size = new System.Drawing.Size(220, 20);
            this.cNPJTextBox.TabIndex = 0;
            this.cNPJTextBox.TabStop = false;
            this.cNPJTextBox.Visible = false;
            // 
            // pnlPedidos
            // 
            this.pnlPedidos.Controls.Add(this.pnlPedidosGrid);
            this.pnlPedidos.Controls.Add(this.pnlPedidosFilter);
            this.pnlPedidos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPedidos.Location = new System.Drawing.Point(0, 0);
            this.pnlPedidos.Name = "pnlPedidos";
            this.pnlPedidos.Size = new System.Drawing.Size(972, 222);
            this.pnlPedidos.TabIndex = 1;
            // 
            // pnlPedidosGrid
            // 
            this.pnlPedidosGrid.Controls.Add(this.dgvPedidos);
            this.pnlPedidosGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPedidosGrid.Location = new System.Drawing.Point(0, 82);
            this.pnlPedidosGrid.Name = "pnlPedidosGrid";
            this.pnlPedidosGrid.Size = new System.Drawing.Size(972, 140);
            this.pnlPedidosGrid.TabIndex = 1;
            // 
            // dgvPedidos
            // 
            this.dgvPedidos.AllowUserToAddRows = false;
            this.dgvPedidos.AllowUserToDeleteRows = false;
            this.dgvPedidos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPedidos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPedidos.Location = new System.Drawing.Point(0, 0);
            this.dgvPedidos.Name = "dgvPedidos";
            this.dgvPedidos.Size = new System.Drawing.Size(972, 140);
            this.dgvPedidos.TabIndex = 0;
            // 
            // pnlPedidosFilter
            // 
            this.pnlPedidosFilter.BackColor = System.Drawing.Color.BurlyWood;
            this.pnlPedidosFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlPedidosFilter.Location = new System.Drawing.Point(0, 0);
            this.pnlPedidosFilter.Name = "pnlPedidosFilter";
            this.pnlPedidosFilter.Size = new System.Drawing.Size(972, 82);
            this.pnlPedidosFilter.TabIndex = 0;
            // 
            // pnlServicos
            // 
            this.pnlServicos.Controls.Add(this.pnlServicosGrid);
            this.pnlServicos.Controls.Add(this.pnlServicosFilter);
            this.pnlServicos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlServicos.Location = new System.Drawing.Point(0, 0);
            this.pnlServicos.Name = "pnlServicos";
            this.pnlServicos.Size = new System.Drawing.Size(972, 222);
            this.pnlServicos.TabIndex = 1;
            // 
            // pnlServicosGrid
            // 
            this.pnlServicosGrid.Controls.Add(this.dgvServicos);
            this.pnlServicosGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlServicosGrid.Location = new System.Drawing.Point(0, 82);
            this.pnlServicosGrid.Name = "pnlServicosGrid";
            this.pnlServicosGrid.Size = new System.Drawing.Size(972, 140);
            this.pnlServicosGrid.TabIndex = 1;
            // 
            // dgvServicos
            // 
            this.dgvServicos.AllowUserToAddRows = false;
            this.dgvServicos.AllowUserToDeleteRows = false;
            this.dgvServicos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvServicos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvServicos.Location = new System.Drawing.Point(0, 0);
            this.dgvServicos.Name = "dgvServicos";
            this.dgvServicos.Size = new System.Drawing.Size(972, 140);
            this.dgvServicos.TabIndex = 0;
            // 
            // pnlServicosFilter
            // 
            this.pnlServicosFilter.BackColor = System.Drawing.Color.BurlyWood;
            this.pnlServicosFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlServicosFilter.Location = new System.Drawing.Point(0, 0);
            this.pnlServicosFilter.Name = "pnlServicosFilter";
            this.pnlServicosFilter.Size = new System.Drawing.Size(972, 82);
            this.pnlServicosFilter.TabIndex = 0;
            // 
            // ManFuncionario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1184, 711);
            this.Controls.Add(this.FuncionarioBindingNavigator);
            this.Controls.Add(this.pnlConsultar);
            this.Controls.Add(this.labelCadastrarFuncionario);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.MaximizeBox = true;
            this.MinimizeBox = true;
            this.Name = "ManFuncionario";
            this.Text = "ManFuncionario";
            this.Load += new System.EventHandler(this.ManFuncionario_Load);
            this.Controls.SetChildIndex(this.grpPrincipal, 0);
            this.Controls.SetChildIndex(this.tabPrincipal, 0);
            this.Controls.SetChildIndex(this.labelCadastrarFuncionario, 0);
            this.Controls.SetChildIndex(this.pnlConsultar, 0);
            this.Controls.SetChildIndex(this.FuncionarioBindingNavigator, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dtsPrincipal)).EndInit();
            this.tabPrincipal.ResumeLayout(false);
            this.pnlConsultar.ResumeLayout(false);
            this.pnlConsultar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFuncionario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsXOSFuncionario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtsXOSFuncionario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FuncionarioBindingNavigator)).EndInit();
            this.FuncionarioBindingNavigator.ResumeLayout(false);
            this.FuncionarioBindingNavigator.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabChild.ResumeLayout(false);
            this.tabEndereco.ResumeLayout(false);
            this.tabEndereco.PerformLayout();
            this.tabOrcamentos.ResumeLayout(false);
            this.pnlOrcamentos.ResumeLayout(false);
            this.pnlOrcamentosGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvOrcamentos)).EndInit();
            this.tabPedidos.ResumeLayout(false);
            this.tabServicos.ResumeLayout(false);
            this.tabInstrumentos.ResumeLayout(false);
            this.pnlInstrumentos.ResumeLayout(false);
            this.pnlInstrumentosGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvInstrumentos)).EndInit();
            this.pnlPedidos.ResumeLayout(false);
            this.pnlPedidosGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPedidos)).EndInit();
            this.pnlServicos.ResumeLayout(false);
            this.pnlServicosGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvServicos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelCadastrarFuncionario;
        private System.Windows.Forms.Panel pnlConsultar;
        private System.Windows.Forms.TextBox tbLike;
        private System.Windows.Forms.Label labelLike;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label lblTotalOS;
        private System.Windows.Forms.DataGridView dgvFuncionario;
        private System.Windows.Forms.DataGridViewTextBoxColumn CNPJ;
        private System.Windows.Forms.BindingSource bsXOSFuncionario;
        private XOSFuncionario dtsXOSFuncionario;
        private XOSFuncionarioTableAdapters.XOsFuncionarioTableAdapter xOsFuncionarioTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn postoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn funcionarioDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn turnoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipoPessoaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cgcDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn inscricaoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn enderecoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bairroDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cidadeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn estadoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cepDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs01DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs02DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs03DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs04DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs05DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs06DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs07DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs08DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs09DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs10DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn noturnoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn inativoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtAlterDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs11DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs12DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs13DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs14DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs15DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs16DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs17DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs18DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs19DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs20DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rFIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn biometriaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cTACaixaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipoVIPDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn uNomeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn uSenhaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipoacessoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtcadastroDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ultimoacessoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn planocontaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn acessapedidoDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn digitaselolacreDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idusuarioDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn isusuarioDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingNavigator FuncionarioBindingNavigator;
        private System.Windows.Forms.ToolStripButton bnInserir;
        private System.Windows.Forms.ToolStripLabel bnCount;
        private System.Windows.Forms.ToolStripButton bnConsultar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton bnGravar;
        private System.Windows.Forms.ToolStripButton bnCancelar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton bnPrimeiro;
        private System.Windows.Forms.ToolStripButton bnAnterior;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripTextBox bnPosition;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton bnProximo;
        private System.Windows.Forms.ToolStripButton bnUltimo;
        private System.Windows.Forms.ToolStripButton bnDescartar;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.CheckBox checkboxIsUsuario;
        private System.Windows.Forms.TextBox tb_rg;
        private System.Windows.Forms.TabControl tabChild;
        private System.Windows.Forms.TabPage tabEndereco;
        private System.Windows.Forms.TextBox fAXTextBox;
        private System.Windows.Forms.TextBox tELEFONETextBox;
        private System.Windows.Forms.MaskedTextBox mtbTelefone;
        private System.Windows.Forms.TextBox tbBairro;
        private System.Windows.Forms.TextBox tbEndereco;
        private System.Windows.Forms.TextBox tbCidade;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox tbCep;
        private System.Windows.Forms.TabPage tabOrcamentos;
        private System.Windows.Forms.TabPage tabPedidos;
        private System.Windows.Forms.TabPage tabServicos;
        private System.Windows.Forms.TabPage tabInstrumentos;
        private System.Windows.Forms.TextBox tbNome;
        private System.Windows.Forms.TextBox nOME_FANTASIATextBox;
        private System.Windows.Forms.MaskedTextBox mtbCPF;
        private System.Windows.Forms.MaskedTextBox mtbCNPJ;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.TextBox tbIdusuario;
        private System.Windows.Forms.TextBox tb_email;
        private System.Windows.Forms.TextBox cPFTextBox;
        private System.Windows.Forms.TextBox cNPJTextBox;
        private System.Windows.Forms.TextBox tbSenha;
        private System.Windows.Forms.TextBox tbLogin;
        private System.Windows.Forms.Label labelSenha;
        private System.Windows.Forms.Label labelLogin;
        private System.Windows.Forms.Label labelCep;
        private System.Windows.Forms.Panel pnlInstrumentos;
        private System.Windows.Forms.Panel pnlInstrumentosGrid;
        private System.Windows.Forms.Panel pnlInstrumentosFilter;
        private System.Windows.Forms.DataGridView dgvInstrumentos;
        private System.Windows.Forms.Panel pnlOrcamentos;
        private System.Windows.Forms.Panel pnlOrcamentosGrid;
        private System.Windows.Forms.DataGridView dgvOrcamentos;
        private System.Windows.Forms.Panel pnlOrcamentosFilter;
        private System.Windows.Forms.Panel pnlPedidos;
        private System.Windows.Forms.Panel pnlPedidosGrid;
        private System.Windows.Forms.DataGridView dgvPedidos;
        private System.Windows.Forms.Panel pnlPedidosFilter;
        private System.Windows.Forms.Panel pnlServicos;
        private System.Windows.Forms.Panel pnlServicosGrid;
        private System.Windows.Forms.DataGridView dgvServicos;
        private System.Windows.Forms.Panel pnlServicosFilter;

    }
}