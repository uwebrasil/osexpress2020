﻿using System;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using CrystalDecisions.Shared;
using System.Data.SqlClient;
using System.Configuration;
using TExpress;
using System.Collections.Generic;

namespace osExpress.Manutencoes
{
    public partial class ManOrdemServico : osExpress.FrmPrincipal
    {      
        // 15.12.2020 uwe
        // comboBox8 - databound IDCliente 
        // set x 1662 "invisivel", fumção ??
        //
        // tempo maximal que o funcionário pode trabalhar
        // sem intervalo de 1 hora.
        private int MAXTEMPO_SEM_INTERVALO = 3600 * 4;

        private BindingSource bs_cabecalho;
        private BindingSource bs_empresa; // ... para administrar a variável global OsComfiguracoes.POSTO
        private BindingSource bs_postoos; // (KOPIE bs_empresa) ... para filtrar em OsOrdemServico por IDPOSTO 
        private BindingSource bs_solicitante;

        private RelOrdem relOrdemServico;
        EnviaEmail enviaEmail;
        string corpoOS;

        public ManOrdemServico()
        {
            InitializeComponent();
            corrigirLayout();

            pnlConsultar.Visible = true;
            // 29.05.2014 uwe           
            //tsbGerarCaixa.Enabled = false;
            DB.ConnectionName = "osExpress.Properties.Settings.OsExpressConnectionString";

            //bs.Filter = string.Format("Name LIKE '%{0}%'", textBox1.Text);
            //uwex
            //bdsFiltroCliente.Filter = string.Format("Posto = '{0}'", OsConfiguracoes.codigoPosto);
            bdsClientes.Filter = string.Format("Posto = '{0}'", OsConfiguracoes.codigoPosto);
            bdsClientes.Sort = "LOGOTIPO";
            
            #region bindingsource.filter não funciona com subselect
            //bdsFiltroCliente.Filter = " Posto ='" + OsConfiguracoes.codigoPosto + "'";
            // o seguinte não funciona
            //string filtroCliente = " Posto ='" + OsConfiguracoes.codigoPosto + "'";
            //filtroCliente +=
            //    @" AND id = (select min(id) from a30clientes where cliente = a.cliente and posto = '" + OsConfiguracoes.codigoPosto + "')";               
            //bdsFiltroCliente.Filter = filtroCliente;
            #endregion 

            #region CONSULTA
            // Page inicial Consulta
            string xsql =
                @"select id,nome from A30Clientes A where posto='" + OsConfiguracoes.codigoPosto;  
                xsql +="' and A.ID = (select MIN(id) from A30Clientes where cliente=a.cliente) ";
            BindingSource bs = new BindingSource();
            var mysource = DB.GetTableFromSQL(xsql);
            bs.DataSource = mysource;
            bs.Sort = "nome";

            // Nome
            cmbFiltroClienteNome.DataSource = null;
            cmbFiltroClienteNome.DataSource = bs;
            cmbFiltroClienteNome.DisplayMember = "nome";

            // Id do funcionario
            cmbFiltroClienteCodigo.DataSource = null;
            cmbFiltroClienteCodigo.DataSource = bs;
            cmbFiltroClienteCodigo.DisplayMember = "id";
            #endregion

            #region Cabeçalho
            // Tabpage CABEÇALHO
            string sql_cabecalho =
                @"select id,nome,logotipo,cliente,inscricao,cgc,telefone1,endereco,cidade,estado from A30Clientes A where posto='" 
                + OsConfiguracoes.codigoPosto;
            sql_cabecalho += "' and inativo = '0' and tipo_pessoa = 'J";                                        
            sql_cabecalho += "' and A.ID = (select MIN(id) from A30Clientes where cliente=a.cliente and posto='"
                               + OsConfiguracoes.codigoPosto + "') ";

            /*
             select * from (
                select id,nome,logotipo,cliente,inscricao,cgc,telefone1,endereco from A30Clientes A 
                 where posto='001' and inativo = '0' and tipo_pessoa = 'J' 
                   and A.ID = (select MIN(id) from A30Clientes where cliente=a.cliente and posto='001')

                UNION 
                select id,razao_social nome ,nome_fantasia logotipo,id cliente,ie inscricao,cnpj cgc,telefone telefone1,endereco from OsCliente
                ) abc
            */

            string sql_cabecalho_union = @"select * from (";
            sql_cabecalho_union += sql_cabecalho;
            sql_cabecalho_union += @" UNION select id,razao_social nome ,nome_fantasia logotipo,
                                            id cliente,ie inscricao,cnpj cgc,telefone telefone1,endereco,cidade,uf estado from OsCliente ) abc";


            //27.02.2021 uwe
            // tirar vinculação com A30Clientes
            sql_cabecalho_union = @" select id,razao_social nome ,nome_fantasia logotipo,
                                            id cliente,ie inscricao,cnpj cgc,telefone telefone1,endereco,cidade,uf estado from OsCliente";

            bs_cabecalho = new BindingSource();
           
            var mysource_cabecalho = DB.GetTableFromSQL(sql_cabecalho_union);
            bs_cabecalho.DataSource = mysource_cabecalho;
            bs_cabecalho.Sort = "nome";

            //// Logotipo
            cmbNomeCliente.DataSource = null;
            cmbNomeCliente.DataSource = bs_cabecalho;
            //cmbNomeCliente.DisplayMember = "logotipo";
            // change 09.04.
            cmbNomeCliente.DisplayMember = "nome";

            bs_cabecalho.Sort = " cliente";
            cmbCodCliente.DataSource = null;
            cmbCodCliente.DataSource = bs_cabecalho;         
            cmbCodCliente.DisplayMember = "cliente";


            tbCodCliente.DataBindings.Clear();
            tbCodCliente.DataBindings.Add(
                new Binding("Text", bs_cabecalho, "cliente", true));


            lblCabCliente.DataBindings.Clear();
            lblCabCliente.DataBindings.Add(
                new Binding("Text", bs_cabecalho, "nome", true));

            /* 28.11.2020 */
            lblCabIE.DataBindings.Clear();
            lblCabIE.DataBindings.Add(
                new Binding("Text", bs_cabecalho, "inscricao", true));

            lblCabNCPJ.DataBindings.Clear();
            lblCabNCPJ.DataBindings.Add(
                new Binding("Text", bs_cabecalho, "cgc", true));

            lblCabTelefone.DataBindings.Clear();
            lblCabTelefone.DataBindings.Add(
                new Binding("Text", bs_cabecalho, "telefone1", true));

            lblCabObservacao.DataBindings.Clear();
            lblCabObservacao.DataBindings.Add(
                new Binding("Text", bs_cabecalho, "endereco", true));
            /* 28.11.2020 */

            lblCidade2.DataBindings.Clear();
            lblCidade2.DataBindings.Add(
                new Binding("Text", bs_cabecalho, "cidade", true));

            lblUF2.DataBindings.Clear();
            lblUF2.DataBindings.Add(
                new Binding("Text", bs_cabecalho, "estado", true));
            #endregion
            #region Empresa
            // BindingSource bs_empresa = new BindingSource();
            var sql_empresa = @"SELECT id, razao, codigo FROM A30POSTOS";
            var empresa = DB.GetTableFromSQL(sql_empresa);
            bs_empresa = new BindingSource();
            bs_empresa.DataSource = empresa;
            bs_empresa.Sort = "codigo";


            cmbEmpresa.DataSource = null;
            cmbEmpresa.DataSource = bs_empresa;
            cmbEmpresa.DisplayMember = "razao";
            cmbEmpresa.ValueMember = "codigo";

            //cmbEmpresa.DataBindings.Add(
            //    new Binding("SelectedValue", bs_empresa, "codigo", true));
            cmbEmpresa.SelectedIndex = OsConfiguracoes.idPosto - 1;
            #endregion
            #region Posto OS ... filtrar empresa(posto) na consulta
            var sql_postoos = @"SELECT id, razao, codigo FROM A30POSTOS";
            var postoos = DB.GetTableFromSQL(sql_postoos);
            bs_postoos = new BindingSource();
            bs_postoos.DataSource = empresa;
            bs_postoos.Sort = "codigo";

            cmbPostoOS.DataSource = null;
            cmbPostoOS.DataSource = bs_postoos;
            cmbPostoOS.DisplayMember = "razao";
            cmbPostoOS.ValueMember = "codigo";

            cmbPostoOS.SelectedIndex = -1;
            #endregion

            #region Solicitante
            var sql_solicitante = @"SELECT id, razao_social FROM OsCliente";
            var solicitante = DB.GetTableFromSQL(sql_solicitante);
            bs_solicitante = new BindingSource();
            bs_solicitante.DataSource = solicitante;
            bs_solicitante.Sort = "razao_social";

            cmbSolicitante.DataSource = null;
            cmbSolicitante.DataSource = bs_solicitante;
            cmbSolicitante.DisplayMember = "razao_social";
            cmbSolicitante.ValueMember = "id";

            cmbSolicitante.SelectedIndex = -1;

            DataRowView row = (DataRowView)bs_solicitante.Current;

            var id = row["id"];
            var rs = row["razao_social"];

             //if(<yourCombobox>.SelectedIndex==-1)
             //   {
             //       CustomerBindingSource.SuspendBinding();
             //   }
             //   else
             //   {
             //       CustomerBindingSource.ResumeBinding();
             //   }
            #endregion

            #region selos
            // OsConfiguracoes.codigoPosto ==  "001"|"002", IBAMETRO.POSTO == 1|NULL ...            
            //bdsEquipamentoSelos.Filter = " POSTO ='" + Convert.ToInt32(OsConfiguracoes.codigoPosto)  + "'";

            #endregion
            
            #region Produtos
            // Tabpage Produtos
            string psql =
                @"select posto,id,produto,descricao,preco from A30Produto A where posto='" + OsConfiguracoes.codigoPosto;
            psql += "' and A.ID = (select MIN(id) from A30Produto where produto=a.produto) ";

            //select inativo,posto,id,produto,descricao,preco from A30Produto A 
            //where posto='002' 
            //and inativo='0'
            //and A.ID = (select MIN(id) from A30Produto where produto=a.produto and posto='002') 

            psql =  @"select posto,id,produto,descricao,preco from A30Produto A where posto='" + OsConfiguracoes.codigoPosto;
            psql += @"' and inativo='0'";
            psql += @" and A.ID = (select MIN(id) from A30Produto where produto=a.produto and posto='" + OsConfiguracoes.codigoPosto + "') ";


            BindingSource bs_produto = new BindingSource();
            var mysource_produto = DB.GetTableFromSQL(psql);
            bs_produto.DataSource = mysource_produto;
            bs_produto.Sort = "descricao";

            // Descricao
            cmbDescricaoProduto.DataSource = null;
            cmbDescricaoProduto.DataSource = bs_produto;
            cmbDescricaoProduto.DisplayMember = "descricao";

            // Produto
            cmbCodigoProduto.DataSource = null;
            cmbCodigoProduto.DataSource = bs_produto;
            cmbCodigoProduto.DisplayMember = "produto";

            // ID
            cmbCodigoIDProduto.DataSource = null;
            cmbCodigoIDProduto.DataSource = bs_produto;
            cmbCodigoIDProduto.DisplayMember = "id";

            // Preco
            txtPrecoProduto.DataBindings.Clear();
            txtPrecoProduto.DataBindings.Add(
                new Binding("Text", bs_produto, "preco", true));
            #endregion

            lblCodigoOS.SendToBack();
        }

        private void corrigirLayout()
        {
            cmbEmpresa.SendToBack();
            lblEmpresa.SendToBack();

            cmbSolicitante.SendToBack();
            chkSolicitante.SendToBack();

            // Location
            pnlConsultar.Location = new Point(10, 90); //Helper.Layout.LocationOrdemPanel;
            grpPrincipal.Location = new Point(10, 90+30); //Helper.Layout.LocationOrdemGroupBox;
            tabPrincipal.Location = new Point(10, 300+30); // Helper.Layout.LocationOrdemTabControl;
            // Size
            pnlConsultar.Size = Helper.Layout.SizeOrdemPanel;
            grpPrincipal.Size = Helper.Layout.SizeOrdemGroupBox;
            tabPrincipal.Size = Helper.Layout.SizeOrdemTabControl;
           
            // DataGridView
            //osOrdemServicoDataGridView.Size = new Size(815,375);
            //osOrdemServicoDataGridView.Size = new Size(815, 415);
            osOrdemServicoDataGridView.Size = new Size(815, 365);
            lblTotalOS.Location = new Point(3, 595);
            osOrdemServicoDataGridView.Columns[0].Width = 73;
            osOrdemServicoDataGridView.Columns[1].Width = 72;
           
            lblSituacao2.Location = new Point(lblSituacao2.Location.X, lblSituacao2.Location.Y + 45);
            lblCabSituacao.Location = new Point(lblCabSituacao.Location.X, lblCabSituacao.Location.Y + 45);
            label72.Location = new Point(label72.Location.X, label72.Location.Y + 45);
            lblCabFatura.Location = new Point(lblCabFatura.Location.X, lblCabFatura.Location.Y + 45);

            lblCodigoOS.Location = new Point(lblCodigoOS.Location.X, lblCodigoOS.Location.Y + 45);

            
        }

        private void ManOrdemServico_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dtsPrincipal.XOsFuncionario' table. You can move, or remove it, as needed.
            this.xOsFuncionarioTableAdapter.Fill(this.dtsPrincipal.XOsFuncionario);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsIbametroInmetroOsCliente' table. You can move, or remove it, as needed.
            this.osIbametroInmetroOsClienteTableAdapter.Fill(this.dtsPrincipal.OsIbametroInmetroOsCliente);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsInstrumentoServicoOsCliente' table. You can move, or remove it, as needed.
            this.osInstrumentoServicoOsClienteTableAdapter.Fill(this.dtsPrincipal.OsInstrumentoServicoOsCliente);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsIbametroOsCliente' table. You can move, or remove it, as needed.
            this.osIbametroOsClienteTableAdapter.Fill(this.dtsPrincipal.OsIbametroOsCliente);
            int w = (int)(this.Parent.ClientSize.Width * 0.9);
            int h = (int)(this.Parent.ClientSize.Height * 0.9);
            this.Size = new Size(w, h);
            int x = (int)(this.Parent.ClientSize.Width - w) / 2;
            int y = (int)(this.Parent.ClientSize.Height - h) / 2;
            this.Location = new Point(x, y);

            // TODO: This line of code loads data into the 'dtsPrincipal.OsParametros' table. You can move, or remove it, as needed.
            this.osParametrosTableAdapter.Fill(this.dtsPrincipal.OsParametros);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsIbametroInmetro' table. You can move, or remove it, as needed.
            this.osIbametroInmetroTableAdapter.Fill(this.dtsPrincipal.OsIbametroInmetro);
            //this.WindowState = FormWindowState.Maximized;
            //
            this.SuspendLayout();
            //
            // TODO: This line of code loads data into the 'dtsPrincipal.OsUsuario' table. You can move, or remove it, as needed.
            this.osUsuarioTableAdapter.Fill(this.dtsPrincipal.OsUsuario);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsRecibo' table. You can move, or remove it, as needed.
            this.osReciboTableAdapter.Fill(this.dtsPrincipal.OsRecibo);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsRecibo' table. You can move, or remove it, as needed.
            this.osReciboTableAdapter.Fill(this.dtsPrincipal.OsRecibo);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsParContas' table. You can move, or remove it, as needed.
            this.osParContasTableAdapter.Fill(this.dtsPrincipal.OsParContas);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsFormasPagamento' table. You can move, or remove it, as needed.
            this.osFormasPagamentoTableAdapter.Fill(this.dtsPrincipal.OsFormasPagamento);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsInstrumentoServico' table. You can move, or remove it, as needed.
            this.osInstrumentoServicoTableAdapter.Fill(this.dtsPrincipal.OsInstrumentoServico);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsIbametro' table. You can move, or remove it, as needed.
            this.osIbametroTableAdapter.Fill(this.dtsPrincipal.OsIbametro);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsEtiqueta' table. You can move, or remove it, as needed.
            this.osEtiquetaTableAdapter.Fill(this.dtsPrincipal.OsEtiqueta);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsItemServico' table. You can move, or remove it, as needed.
            this.osItemServicoTableAdapter.Fill(this.dtsPrincipal.OsItemServico);
            // TODO: This line of code loads data into the 'dtsPrincipal.A30PRODUTO' table. You can move, or remove it, as needed.
            this.a30PRODUTOTableAdapter.Fill(this.dtsPrincipal.A30PRODUTO);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsSituacoesOS' table. You can move, or remove it, as needed.
            this.osSituacoesOSTableAdapter.Fill(this.dtsPrincipal.OsSituacoesOS);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsParSituacao' table. You can move, or remove it, as needed.
            this.osParSituacaoTableAdapter.Fill(this.dtsPrincipal.OsParSituacao);
            // TODO: This line of code loads data into the 'dtsPrincipal.A30CLIENTES' table. You can move, or remove it, as needed.
            this.a30CLIENTESTableAdapter.Fill(this.dtsPrincipal.A30CLIENTES);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsOrdemServico' table. You can move, or remove it, as needed.
            //
            var debugstop=1;
            try
            {
                //this.osOrdemServicoTableAdapter.SqlOrdensServico(this.dtsPrincipal.OsOrdemServico);
                this.osOrdemServicoTableAdapter.SqlOrdensServicoXOSFuncionario(this.dtsPrincipal.OsOrdemServico);
               
            }
            catch (Exception ee) {
                Debug.WriteLine(ee.Message);
            }

            
            //
            this.ResumeLayout();

            estadoCorrente = EstadoManutencao.smPesquisar;
            cmbFiltroSituacao.SelectedIndex = 0;

            if (OsConfiguracoes.tipoAcesso == 'A')
            {
                osOrdemServicoBindingSource.Filter = "ID_USR_OFFLINE = " + "'" + OsConfiguracoes.codigoUsuario + "' AND STATUS_OS = 'A' AND TIPO_DOC = 'S' ";
            }
            else
            {
                osOrdemServicoBindingSource.Filter = "ID_USR_OFFLINE = " + "'" + OsConfiguracoes.codigoUsuario + "' AND STATUS_OS = 'A' AND TIPO_DOC = 'S' ";
                cmbFiltroUsuarioCodigo.Enabled = false;
                cmbFiltroUsuarioNome.Enabled = false;
                chkFiltrarUsuario.Enabled = false;
            }

            tsbCancelar.Enabled = false;
            tsbInserir.Enabled = true;
            lblTotalOS.Text = "Total: " + osOrdemServicoBindingSource.Count.ToString();


            //
            enviaEmail = new EnviaEmail();

            DataRowView template = (DataRowView)osParametrosBindingSource.Current;
            corpoOS = template["EMAIL_ORDEM"].ToString();

            tbCodCliente.Text = "";

            lblCidade.Location = new Point(9, 130);
            lblCidade2.Location = new Point(116, 130);
            lblUF.Location = new Point(9, 146);
            lblUF2.Location = new Point(116, 146);

        }


        private void tsbInserir_Click(object sender, EventArgs e)
        {
            int existeOsAberta = Int32.Parse((string)osOrdemServicoTableAdapter.SqlExisteOsAberta(OsConfiguracoes.codigoUsuario, OsConfiguracoes.idPosto));

            if (existeOsAberta > 0)
            {
                MessageBox.Show("Não é possível criar uma nova ordem de serviço sem fechar a(s) anterior(es): " + existeOsAberta);
                osOrdemServicoBindingSource.CancelEdit();
                return;
            }
            //foreach (TabPage tab in tabPrincipal.TabPages)
            //{
            //    if (!tab.Name.Equals("tabCabecalho"))
            //        //tab.Enabled = false;
            //        tab.Hide();
            //    //Debug.WriteLine(tab.Name);
            //}

            // 04.01.2021 uwe
            tbCodCliente.Text = "";
            resetSolicitante();

            lblCabCliente.Text = "";
            lblCabIE.Text = "";
            lblCabNCPJ.Text = "";
            lblCabTelefone.Text = "";
            lblCabObservacao.Text = "";
            lblCabSituacao.Text = "";

            // 17.11.2020 uwe
            lblCidade2.Text = "";
            lblUF2.Text = "";
            //lblCidade.Text = "";
            //lblUF.Text = "";
            //

            tsbCancelar.Enabled = true;
            tsbInserir.Enabled = false;

            // mod 23.03.2015 uwe
            toolStripButton3.Enabled = false; // botão CONSULTAR
            toolStripButton1.Enabled = false; // botão PRIMEIRO
            toolStripButton5.Enabled = false; // botão ANTERIOR

            toolStripButton6.Enabled = false; // botão PROXIMO , ja deshabilitado?
            toolStripButton7.Enabled = false; // botão ULTIMO  , ja deshabilitado?
            // end mod 23.03.2015

            pnlConsultar.Visible = false;

            inserir();
            //uwex            
            if (estadoCorrente == EstadoManutencao.smInserir)
                lblCodigoOS.Visible = false;
            else           
                lblCodigoOS.Visible = true;
            

            if (OsConfiguracoes.tipoAcesso == 'A')
            {
                cmbCodigoUsuario.Enabled = true;
                cmbNomeUsuario.Enabled = true;
                // test uwex
                //lblCodigoOS.Visible = false;
            }
            else
            {
                // test uwex
                //lblCodigoOS.Visible = true;

                cmbCodigoUsuario.Text = OsConfiguracoes.codigoUsuario.ToString();
                cmbNomeUsuario.Text = OsConfiguracoes.nomeUsuario;
                cmbCodigoUsuario.Enabled = false;
                cmbNomeUsuario.Enabled = false;
            }
           
            // preencher com usuario de login
            int index = osUsuarioBindingSource.Find(@"Id", OsConfiguracoes.codigoUsuario);
            if (index >= 0)
                osUsuarioBindingSource.Position = index;



        }

        //private void salvarOrdemServico()
        private void salvarOrdemServico_OLD()
        {
            tsbCancelar.Enabled = false;
            tsbInserir.Enabled = true;
            
                                    
            int proximoCodigo = (int)osOrdemServicoTableAdapter.SqlProximaOS(OsConfiguracoes.codigoUsuario, OsConfiguracoes.idPosto);


            //SELECT        { fn IFNULL(MAX(ID_OFFLINE), 0) }+1 AS CODIGO_OFFLINE
        //    OsOrdemServico o where o.ID_USR_OFFLINE = @USUARIO AND o.IDPOSTO = @IDPOSTO
            if (cmbNomeCliente.Text.Length == 0 || cmbNomeCliente.Text.Length == 0)
            {
                MessageBox.Show("Informe o Código do Cliente Antes de Gravar a OS!");
                return;
            }
            else
            {
                if (estadoCorrente == EstadoManutencao.smInserir)
                {
                    DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;
                    currentOS["IDPOSTO"] = OsConfiguracoes.idPosto;
                    currentOS["DT_CADASTRO"] = DateTime.Now;
                    currentOS["ID_OFFLINE"] = proximoCodigo;

                    // codigo de posto em vez de id do posto ??

                    //currentOS["ID_FINAL"] = OsConfiguracoes.codigoUsuario.ToString() 
                    //                      + OsConfiguracoes.idPosto.ToString() 
                    //                      + proximoCodigo;
                    currentOS["ID_FINAL"] = OsConfiguracoes.codigoUsuario
                                          + OsConfiguracoes.codigoPosto
                                          + proximoCodigo;
                    
                    // 13.03.2015 uwe
                    //lblCodigoOS.Text = currentOS["ID_FINAL"].ToString();
                    //new_idfinal = Convert.ToInt32(currentOS["ID_FINAL"]);

                    currentOS["ID_USR_OFFLINE"] = OsConfiguracoes.codigoUsuario;
                    currentOS["TIPO_DOC"] = 'S';
                }

                if (txtAssinaturaCliente.Text.Length == 0)
                {
                    //txtAssinaturaCliente.Text = cmbNomeCliente.Text;
                    // cmbNomeCliente - Displaymember alterado por LOGOTIPO
                    try {
                        txtAssinaturaCliente.Text =
                        DB.GetSingleValue("select max(nome) from a30clientes where logotipo='" + cmbNomeCliente.Text + "'").ToString();
                    }
                    // Exexão já tratado na biblioteca DB
                    catch (Exception ee) { }
                }

                if (txtEmail.Text.Length == 0)
                {
                    DataRowView currentCliente = (DataRowView)bdsClientes.Current;
                    txtEmail.Text = currentCliente["obs01"].ToString();
                }

                //currentOS["TOTAL_OS"] = (decimal)currentOS["TOTAL_PECAS_OS"] + (decimal)currentOS["TOTAL_GERAL_SERVICOS"];
                txtTotalFinalOS.Text = (decimal.Parse(txtTotalServicosOS.Text) + decimal.Parse(txtTotalGeralPecas.Text)).ToString();
                txtValorAPagar.Text = (decimal.Parse(txtTotalFinalOS.Text) - decimal.Parse(txtTotalPago.Text)).ToString();
                this.Validate();
                this.osOrdemServicoBindingSource.EndEdit();
                this.osOrdemServicoTableAdapter.Update(this.dtsPrincipal);
                this.osSituacoesOSBindingSource.EndEdit();
                this.osItemServicoBindingSource.EndEdit();
                this.osInstrumentoServicoBindingSource.EndEdit();
                this.osFormasPagamentoBindingSource.EndEdit();

                //this.bdsLacre.EndEdit();
                //this.bdsSelos.EndEdit();
                //this.osEtiquetaTableAdapter.Update(this.dtsPrincipal);


                this.tableAdapterManager.UpdateAll(this.dtsPrincipal);
                estadoCorrente = EstadoManutencao.smPesquisar;
                //bitValue = reader["MyBitColumn"] as bool? ?? null;
            }
        }

        // private void salvarOrdemServicoX()
        private void salvarOrdemServico()
        {
            tsbCancelar.Enabled = false;
            tsbInserir.Enabled = true;

            int proximoCodigo = (int)osOrdemServicoTableAdapter.SqlProximaOS(OsConfiguracoes.codigoUsuario, OsConfiguracoes.idPosto);

            if (cmbNomeCliente.Text.Length == 0 || cmbNomeCliente.Text.Length == 0)
            {
                MessageBox.Show("Informe o Código do Cliente Antes de Gravar a OS!");
                return;
            }
            else
            {
                if (estadoCorrente == EstadoManutencao.smInserir)
                {
                    DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;
                    currentOS["IDPOSTO"] = OsConfiguracoes.idPosto;
                    currentOS["DT_CADASTRO"] = DateTime.Now;
                    currentOS["ID_OFFLINE"] = proximoCodigo;
                  
                    currentOS["ID_FINAL"] = OsConfiguracoes.codigoUsuario
                                          + OsConfiguracoes.codigoPosto
                                          + proximoCodigo;

                    currentOS["ID_USR_OFFLINE"] = OsConfiguracoes.codigoUsuario;
                    currentOS["TIPO_DOC"] = 'S';
                }

                if (txtAssinaturaCliente.Text.Length == 0)
                {                    
                    try
                    {
                        txtAssinaturaCliente.Text =
                        //DB.GetSingleValue("select max(nome) from a30clientes where logotipo='" + cmbNomeCliente.Text + "'").ToString();
                        DB.GetSingleValue("select max(nome) from OSCliente where nome_fantasia='" + cmbNomeCliente.Text + "'").ToString();
                    }
                    // Exexão já tratado na biblioteca DB
                    catch (Exception ee) { }
                }

                if (txtEmail.Text.Length == 0)
                {
                    DataRowView currentCliente = (DataRowView)bdsClientes.Current;
                    txtEmail.Text = currentCliente["obs01"].ToString();
                }

                //currentOS["TOTAL_OS"] = (decimal)currentOS["TOTAL_PECAS_OS"] + (decimal)currentOS["TOTAL_GERAL_SERVICOS"];
                txtTotalFinalOS.Text = (decimal.Parse(txtTotalServicosOS.Text) + decimal.Parse(txtTotalGeralPecas.Text)).ToString();
                txtValorAPagar.Text = (decimal.Parse(txtTotalFinalOS.Text) - decimal.Parse(txtTotalPago.Text)).ToString();
                this.Validate();
                this.osOrdemServicoBindingSource.EndEdit();
                this.osOrdemServicoTableAdapter.Update(this.dtsPrincipal);
                this.osSituacoesOSBindingSource.EndEdit();
                this.osItemServicoBindingSource.EndEdit();

                //this.osInstrumentoServicoBindingSource.EndEdit();
                this.osInstrumentoServicoBindingSourceX.EndEdit();

                this.osFormasPagamentoBindingSource.EndEdit();

                //this.bdsLacre.EndEdit();
                //this.bdsSelos.EndEdit();
                //this.osEtiquetaTableAdapter.Update(this.dtsPrincipal);


                this.tableAdapterManager.UpdateAll(this.dtsPrincipal);
                estadoCorrente = EstadoManutencao.smPesquisar;
                //bitValue = reader["MyBitColumn"] as bool? ?? null;
            }
        }
        private void tsbSalvar_Click(object sender, EventArgs e)
        {
            salvarOrdemServico();
            MessageBox.Show("Registro gravado com sucesso!");
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.osSituacoesOSBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dtsPrincipal);
        }


        private decimal calculavalores(int qtde, decimal valor)
        {
            decimal total;

            total = qtde * valor;

            return total;
        }


        private void calculaVisitaTecnica(object sender, EventArgs e)
        {
            if (txtVisitaTecnicaQtde.Text.Length > 0 && txtVisitaTecnicaVlr.Text.Length > 0)
            txtVisitaTecnicaTotal.Text = calculavalores(int.Parse(txtVisitaTecnicaQtde.Text), decimal.Parse(txtVisitaTecnicaVlr.Text)).ToString();
        }


        private void calculaDeslocamento(object sender, EventArgs e)
        {
            if (txtHoraDeslocamentoQtde.Text.Length > 0 && txtHoraDeslocamentoVlr.Text.Length > 0)
                txtHoraDeslocamentoTotal.Text = calculavalores(int.Parse(txtHoraDeslocamentoQtde.Text), decimal.Parse(txtHoraDeslocamentoVlr.Text)).ToString();
        }


        private void calculaExcedente(object sender, EventArgs e)
        {
            if (txtHoraExcedenteQtde.Text.Length > 0 && txtHoraExcedenteVlr.Text.Length > 0)
                txtHoraExcedenteTotal.Text = calculavalores(int.Parse(txtHoraExcedenteQtde.Text), decimal.Parse(txtHoraExcedenteVlr.Text)).ToString();
        }


        private void calculaInstalacao(object sender, EventArgs e)
        {
            if (txtTaxaInstalacaoQtde.Text.Length > 0 && txtTaxaInstalacaoVlr.Text.Length > 0)
                txtTaxaInstalacaoTotal.Text = calculavalores(int.Parse(txtTaxaInstalacaoQtde.Text), decimal.Parse(txtTaxaInstalacaoVlr.Text)).ToString();
        }


        private void calculaRede(object sender, EventArgs e)
        {
            if (txtPontoRedeQtde.Text.Length > 0 && txtPontoRedeVlr.Text.Length > 0)
                txtPontoRedeTotal.Text = calculavalores(int.Parse(txtPontoRedeQtde.Text), decimal.Parse(txtPontoRedeVlr.Text)).ToString();
        }


        private void calculaDeslocamentoKM(object sender, EventArgs e)
        {
            if (txtDeslocamentoKmQtde.Text.Length > 0 && txtDeslocamentoKmVlr.Text.Length > 0)
                txtDeslocamentoKmTotal.Text = calculavalores(int.Parse(txtDeslocamentoKmQtde.Text), decimal.Parse(txtDeslocamentoKmVlr.Text)).ToString();
        }


        private void calculaOutros(object sender, EventArgs e)
        {
            if (txtOutrosServicosQtde.Text.Length > 0 && txtOutrosServicosVlr.Text.Length > 0)
                txtOutrosServicosTotal.Text = calculavalores(int.Parse(txtOutrosServicosQtde.Text), decimal.Parse(txtOutrosServicosVlr.Text)).ToString();
        }


        private void calculaVlrDespesas(object sender, EventArgs e)
        {
            decimal totalDesp = 0;

            if (txtAlimentacao.Text.Length > 0)
                totalDesp = totalDesp + decimal.Parse(txtAlimentacao.Text);
            if (txtHospedagem.Text.Length > 0)
                totalDesp = totalDesp + decimal.Parse(txtHospedagem.Text);
            if (txtTranslado.Text.Length > 0)
                totalDesp = totalDesp + decimal.Parse(txtTranslado.Text);
            if (txtOutrasDespesas.Text.Length > 0)
                totalDesp = totalDesp + decimal.Parse(txtOutrasDespesas.Text);

            txtTotalDesp.Text = totalDesp.ToString();
        }


        private void calculaVlrTotalServ(object sender, EventArgs e)
        {
            decimal totalDesp = 0;

            if (txtVisitaTecnicaTotal.Text.Length > 0)
                totalDesp = totalDesp + decimal.Parse(txtVisitaTecnicaTotal.Text);
            if (txtHoraDeslocamentoTotal.Text.Length > 0)
                totalDesp = totalDesp + decimal.Parse(txtHoraDeslocamentoTotal.Text);
            if (txtHoraExcedenteTotal.Text.Length > 0)
                totalDesp = totalDesp + decimal.Parse(txtHoraExcedenteTotal.Text);
            if (txtTaxaInstalacaoTotal.Text.Length > 0)
                totalDesp = totalDesp + decimal.Parse(txtTaxaInstalacaoTotal.Text);
            if (txtPontoRedeTotal.Text.Length > 0)
                totalDesp = totalDesp + decimal.Parse(txtPontoRedeTotal.Text);
            if (txtDeslocamentoKmTotal.Text.Length > 0)
                totalDesp = totalDesp + decimal.Parse(txtDeslocamentoKmTotal.Text);
            if (txtOutrosServicosTotal.Text.Length > 0)
                totalDesp = totalDesp + decimal.Parse(txtOutrosServicosTotal.Text);

            txtTotalServicosTotal.Text = totalDesp.ToString();
        }


        private void calculaVlrTotalOS(object sender, EventArgs e)
        {
            decimal totalDesp = 0;

            if (txtTotalServicosTotal.Text.Length > 0)
                totalDesp = totalDesp + decimal.Parse(txtTotalServicosTotal.Text);
            if (txtTotalDesp.Text.Length > 0)
                totalDesp = totalDesp + decimal.Parse(txtTotalDesp.Text);
              
            txtTotalServicosOS.Text = totalDesp.ToString();
        }


        private void TplPermiteSoValor(object sender, KeyPressEventArgs e) 
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != ',')
            {
                e.Handled = true;
            }

            // Só permite apenas número um caracter de vírgula (decimal).
            if (e.KeyChar == ',' && (sender as TextBox).Text.IndexOf(',') > -1)
            {
                e.Handled = true;
            }
        }


        private void TplPermiteSoNumero(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }


        private void ManOrdemServico_Shown(object sender, EventArgs e)
        {
            if (OsConfiguracoes.tipoAcesso == 'A')
            {
                cmbCodigoUsuario.Enabled = true;
                cmbNomeUsuario.Enabled = true;
            }
            else
            {
                cmbCodigoUsuario.Enabled = false;
                cmbNomeUsuario.Enabled = false;
            }

            pnlConsultar.Visible = true;
        }


        private void calculaItemPeca(object sender, EventArgs e)
        {
            if (txtPrecoProduto.Text.Length < 1)
            {
                txtPrecoProduto.Text = "0";
            }      

            if (numQtdeProduto.Value != 0)
                txtTotalProduto.Text = (numQtdeProduto.Value * 
                                        decimal.Parse(txtPrecoProduto.Text)).ToString();
        }


        private void osOrdemServicoBindingSource_CurrentChanged(object sender, EventArgs e)
        {
            /*if ((cmbNomeCliente.Text.Length == 0 || cmbNomeCliente.Text.Length == 0) &&  (estadoCorrente == EstadoManutencao.smInserir))
            {
                MessageBox.Show("Informe o Código do Cliente Antes de Gravar a OS!");
                return;
            }*/
            // 2018
             DataRowView current = (DataRowView)osOrdemServicoBindingSource.Current;

            
             
            //UXX           
            // Se existir registro...
            if (osOrdemServicoBindingSource.Count > 0)
            {
                //setEmpresa();

                if (current["STATUS_OS"].ToString() == "F")
                {
                    //tsbGerarCaixa.Enabled = true;
                    // 29.05.2014 Uwe
                    // BUG: tsbGerarCaixa.Enabled = true;

                    // 26.03.2018 Uwe
                    //var currentUsuario = (DataRowView)osUsuarioBindingSource.Current;
                    //var tipo_acesso = (char)currentUsuario["TIPO_ACESSO"];

                    var tipo_acesso = OsConfiguracoes.tipoAcesso;
                    if (((bool)current["FATURA_GERADA"] == false) && (tipo_acesso == 'A'))
                    {
                        //tsbGerarCaixa.Enabled = true;

                        // if usuario == admin
                        tsbFecharOS.Enabled = true;
                        tsbFecharOS.Text = "Abri&r OS";
                        tsbFecharOS.ToolTipText = @"Abrir OS";
                    }
                    else
                    {
                        tsbFecharOS.Enabled = false;
                        tsbFecharOS.Text = "&Fechar OS";
                        tsbFecharOS.ToolTipText = @"Fechar OS";
                    }
                }

                //var id_final = current["ID_FINAL"];

                //if (tsbFecharOS.Text.Contains("Abrir"))
                //{
                //    //DB.ExecuteNonQuery("Update OsOrdemServico set status_os = 'A' Where ID_FINAL='" + id_final + "'"); 
                //    return;
                //}

                if (OsConfiguracoes.tipoAcesso == 'A')
                {
                    txtPrecoProduto.ReadOnly = false;
                    txtPrecoProduto.Enabled = true;
                }
                else
                {
                    txtPrecoProduto.ReadOnly = true;
                    txtPrecoProduto.Enabled = false;
                }

                // deu muito problema
                //osItemServicoBindingSource.Filter = "IDORDEM = " + current["ID"];
                //osSituacoesOSBindingSource.Filter = "IDORDEM = " + current["ID"];

                 if ((string)current["STATUS_OS"] != "A")
                  {
                        tabCabecalho.Enabled = false;
                        tabSolicitacao.Enabled = false;
                        tabEncontrada.Enabled = false;
                        tabRealizada.Enabled = false;
                        tabFinal.Enabled = false;
                        tabPecas.Enabled = false;
                        tabServicos.Enabled = false;
                        tabEquipamentos.Enabled = false;
                        tabLacres.Enabled = false;
                        tabPagamento.Enabled = false;
                  }
                  else
                  {
                      tabCabecalho.Enabled = true;
                      tabSolicitacao.Enabled = true;
                      tabEncontrada.Enabled = true;
                      tabRealizada.Enabled = true;
                      tabFinal.Enabled = true;
                      tabPecas.Enabled = true;
                      tabServicos.Enabled = true;

                      if (OsConfiguracoes.digitaSeloELacre == true)
                      {
                          tabEquipamentos.Enabled = true;
                          tabLacres.Enabled = true;
                      }
                      else
                      {
                          tabEquipamentos.Enabled = false;
                          tabLacres.Enabled = false;
                      }

                      tabPagamento.Enabled = true;
                  }


                  if ((string)current["STATUS_OS"] == "A")
                  {
                      lblCabSituacao.Text = "Aberta";
                      lblCabSituacao.ForeColor = Color.Green;
                      lblSituacao2.ForeColor = Color.Green;
                      tsbDescartar.Enabled = true;
                      tsbFecharOS.Enabled = true;
                  }
                  else
                  if ((string)current["STATUS_OS"] == "D")
                  {
                      lblCabSituacao.Text = "Descartada";
                      lblCabSituacao.ForeColor = Color.Red;
                      lblSituacao2.ForeColor = Color.Red;
                      tsbDescartar.Enabled = false;
                      tsbFecharOS.Enabled = false;
                  }
                  else
                  if ((string)current["STATUS_OS"] == "F")
                  {
                      // 29.05.2014 Uwe
                      // BUG: tsbGerarCaixa.Enabled = true;
                      // 26.03.2018 Uwe
                      if ((bool) current["FATURA_GERADA"] == false)
                      {
                          //tsbGerarCaixa.Enabled = true;
                      }
                      // end 28.03.2018
                      lblCabSituacao.Text = "Fechada";
                      lblCabSituacao.ForeColor = Color.Blue;
                      lblSituacao2.ForeColor = Color.Blue;
                      tsbDescartar.Enabled = false;
                      tsbFecharOS.Enabled = false;
                  }
                  
                if (tabPrincipal.SelectedIndex == 1)
                    this.osSituacoesOSTableAdapter.SituacaoRealizada(this.dtsPrincipal.OsSituacoesOS, (int)current["ID"], "S");
                if (tabPrincipal.SelectedIndex == 2)
                    this.osSituacoesOSTableAdapter.SituacaoRealizada(this.dtsPrincipal.OsSituacoesOS, (int)current["ID"], "E");
                if (tabPrincipal.SelectedIndex == 3)
                    this.osSituacoesOSTableAdapter.SituacaoRealizada(this.dtsPrincipal.OsSituacoesOS, (int)current["ID"], "R");
                if (tabPrincipal.SelectedIndex == 4)
                    this.osSituacoesOSTableAdapter.SituacaoRealizada(this.dtsPrincipal.OsSituacoesOS, (int)current["ID"], "F");
                if (tabPrincipal.SelectedIndex == 5)
                {
                    //this.osInstrumentoServicoTableAdapter.SqlEquipamentos(this.dtsPrincipal.OsInstrumentoServico, (int)current["ID"]);
                    this.osInstrumentoServicoOsClienteTableAdapter.SqlEquipamentos(this.dtsPrincipal.OsInstrumentoServicoOsCliente, (int)current["ID"]);

                    //bdsEquipamentoSelos.Filter = "IDCLIENTE = " + current["IDCLIENTE"].ToString();
                    //bdsEquipamentoSelos.Filter = "IDCLIENTE = " + current["IDCLIENTE"];
                    //bdsEquipamentoSelos.Filter += " AND POSTO ='" + Convert.ToInt32(OsConfiguracoes.codigoPosto) + "'";

                    bdsEquipamentoSelosX.Filter = "IDCLIENTE = " + current["IDCLIENTE"];
                    bdsEquipamentoSelosX.Filter += " AND POSTO ='" + Convert.ToInt32(OsConfiguracoes.codigoPosto) + "'";

                    //" POSTO ='" + Convert.ToInt32(OsConfiguracoes.codigoPosto)  + "'";
                }
                if (tabPrincipal.SelectedIndex == 6)
                {
                   // this.osInstrumentoServicoTableAdapter.SqlLacre(this.dtsPrincipal.OsInstrumentoServico, (int)current["ID"]);
                   // bdsEquipamentoLacre.Filter = "IDCLIENTE = " + current["IDCLIENTE"].ToString();
                   // bdsEquipamentoLacre.Filter += " AND POSTO ='" + Convert.ToInt32(OsConfiguracoes.codigoPosto) + "'";
                    this.osInstrumentoServicoOsClienteTableAdapter.SqlLacre(this.dtsPrincipal.OsInstrumentoServicoOsCliente, (int)current["ID"]);
                    bdsEquipamentoLacreX.Filter = "IDCLIENTE = " + current["IDCLIENTE"].ToString();
                    bdsEquipamentoLacreX.Filter += " AND POSTO ='" + Convert.ToInt32(OsConfiguracoes.codigoPosto) + "'";
                }
                if (tabPrincipal.SelectedIndex == 7)
                    this.osItemServicoTableAdapter.SqlItensDaOrdem(this.dtsPrincipal.OsItemServico, (int)current["ID"]);
                if (tabPrincipal.SelectedIndex == 9)
                    this.osFormasPagamentoTableAdapter.SqlFormasDePagamento(this.dtsPrincipal.OsFormasPagamento, (int)current["ID"]);

                if ((Decimal)current["VALOR_PAGO"] > 0)
                    tsbImprimirRecibo.Enabled = true;
                else
                    tsbImprimirRecibo.Enabled = false;
            }
        }

        private void limpaAbaSelos()
        {
            cmbNumInmetroSelos.SelectedIndex = -1;
            cmbNumSerieSelos.SelectedIndex = -1;
            //cmbCodigoSelos.SelectedIndex = -1;
            cmbNumeroEtiquetaSelos.SelectedIndex = -1;
        }

        private void limpaAbaLacres()
        {
            cmbNumSerieLacres.SelectedIndex = -1;
            cmbNumInmetroLacres.SelectedIndex = -1;
            //cmbCodigoLacres.SelectedIndex = -1;
            cmbNumeroEtiquetaLacres.SelectedIndex = -1;
        }

        private void limpaAbaProdutos()
        {
            cmbCodigoProduto.SelectedIndex = -1;
            cmbDescricaoProduto.SelectedIndex = -1;
        }
        /// <summary>
        /// 23.04.2014 Uwe
        /// Retorna as atividades referente á ordem de serviço
        /// </summary>
        /// <param name="id_ordem">Id de Ordem Serviço</param>
        /// <returns>Tabela OsSituacoesOs</returns>
        private DataTable linhasSituacao(int id_ordem)
        {
            return (DataTable)DB.GetTableFromSQL(
                     @"select * from OsSituacoesOS where HORA_INICIO is not null and idordem=" 
                       + id_ordem.ToString() 
                       + @" order by hora_inicio");
        }
        /// <summary>
        /// 23.04.2014 Uwe
        /// Retorna tempo passado desde 01.01.1970.
        /// </summary>
        /// <param name="dt">data</param>
        /// <returns>número de segundos</returns>
        private int dateTime2Seconds(DateTime dt)
        {
            TimeSpan span = dt.Subtract(new DateTime(1970, 1, 1, 0, 0, 0));
            return span.Hours*3600 + span.Minutes*60 + span.Seconds;
        }
        /// <summary>
        /// 23.04.2014 Uwe
        /// Retorna número de segundos já passados
        /// sem intervalo.
        /// </summary>
        /// <param name="dt">data</param>
        /// <returns>segundos</returns>
        private int getSecondsUsed(DataTable dt) 
        {
            DateTime timeZero  = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            DateTime timeSoFar = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            bool found_interval = false;
            int s = 0;
            int cnt = 0;
            foreach ( DataRow row in dt.Rows )
            {
                cnt += 1;
                timeSoFar = timeSoFar.AddHours(  ((DateTime)row["hora_total"]).Hour);
                timeSoFar = timeSoFar.AddMinutes(((DateTime)row["hora_total"]).Minute);
                timeSoFar = timeSoFar.AddSeconds(((DateTime)row["hora_total"]).Second);
               
                s = dateTime2Seconds(timeSoFar);
                if (s == MAXTEMPO_SEM_INTERVALO)
                {
                    found_interval = true;
                    timeSoFar = timeZero;
                    cnt = 0;
                }
            }
            // corrigir tempo
            for (int i=0;i<cnt;i++)
                timeSoFar = timeSoFar.AddMinutes(-1);
            return s;          
        }
        /// <summary>
        /// 23.04.2014 Uwe
        /// Retorna última hora_fim da tabela OsSituacoesOs
        /// </summary>
        /// <param name="dt">Table</param>
        /// <returns>Data</returns>
        private DateTime getLastHoraFim(DataTable dt)
        {
          return (DateTime)dt.Rows[dt.Rows.Count-1]["hora_fim"];
        }
        /// <summary>
        /// 23.04.2014 Uwe
        /// Calcular proxima hora_fim no máximo.
        /// </summary>
        /// <param name="dt">Data</param>
        /// <param name="sec_used">Segundos</param>
        /// <returns>Data</returns>
        private DateTime getNextHoraFim(DateTime dt, int sec_used)
        {
            DateTime timeSoFar = dt;
            int available = MAXTEMPO_SEM_INTERVALO - sec_used;
            int h = available / 3600;
            timeSoFar = timeSoFar.AddHours(h);
            int m = (available % 3600) / 60;
            timeSoFar = timeSoFar.AddMinutes(m);
            int s = (available % 3600) % 60;
            timeSoFar = timeSoFar.AddSeconds(s);
           
            // não pode ultrapassar meia-noite
            DateTime timeZero = new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0);
            DateTime midnight = new DateTime(dt.Year, dt.Month, dt.Day, 23, 59, 59);
            if (timeSoFar > midnight)
                timeSoFar = midnight;

            return timeSoFar;
        }
        /// <summary>
        /// inicializar respectivamente atualizar os controles
        /// DateTimePicker
        /// </summary>
        /// <returns>errorCode(não usado até agora)</returns>
        private int updateHorarioRealizado()
        {
            DB.ConnectionName = "osExpress.Properties.Settings.OsExpressConnectionString";
            
            //var constr = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            int error_code = 0;
            if (lblNovoCodOS.Text.Length == 0)
            {
                //MessageBox.Show("Informe o Codiogo de Cliente Antes de Gravar","E R R O");
                return -1;
            }

            string id_final = lblNovoCodOS.Text;
            int id_ordem = 0;
            //erik inicio


            string connectionString = ConfigurationManager.ConnectionStrings["osExpress.Properties.Settings.OsExpressConnectionString"].ConnectionString;
            
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                //using (SqlCommand command = new SqlCommand("select id from osordemservico where ID_final=" + id_final.ToString(), connection))
                // uwe 26.03.2018 - possivel integer overflow
                using (SqlCommand command = new SqlCommand("select id from osordemservico where ID_final='" + id_final + "'", connection))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        // Check is the reader has any rows at all before starting to read.
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                id_ordem = reader.GetInt32(0);
                            }
                        }
                    }
                }
            }

            //erik fim
          
              //int id_ordemx =  (int) DB.GetSingleValue("select * from osordemservico where ID_final=" + id_final.ToString());

                DateTime dtInicio = dtpHoraInicialRealizada.Value;
                DateTime dtFinal  = dtpHoraFinalRealizada.Value;           

                DataTable dt = linhasSituacao(id_ordem);
              
                int rcount = dt.Rows.Count;
                // primeira atividade
                if (rcount == 0)
                {
                    dtpHoraInicialRealizada.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 8, 0, 0);
                    dtpHoraFinalRealizada.Value = dtpHoraInicialRealizada.Value.AddHours(4);
                    
                    dtInicio = dtpHoraInicialRealizada.Value;
                    dtFinal = dtpHoraFinalRealizada.Value;
                    TimeSpan horaTotal = new TimeSpan(dtFinal.Ticks - dtInicio.Ticks);
                    dtpHoraTotalRealizada.Text = horaTotal.ToString();
                }
                else 
                {
                    int s = getSecondsUsed(dt);
                    DateTime nextInicialDate = getLastHoraFim(dt);

                    // proxima atividade - hora inicial                    
                    int secondsUsed = getSecondsUsed(dt);
                    if (secondsUsed == MAXTEMPO_SEM_INTERVALO)
                        nextInicialDate = nextInicialDate.AddHours(1);
                    dtpHoraInicialRealizada.Value = nextInicialDate;
                    // proxima atividade - hora fim                                      
                    DateTime nextFinalDate = nextInicialDate;

                    DateTime timeZero = new DateTime(nextFinalDate.Year,
                                                    nextFinalDate.Month,
                                                    nextFinalDate.Day, 0, 0, 0);
                    DateTime midnight = new DateTime(nextFinalDate.Year,
                                                     nextFinalDate.Month,
                                                     nextFinalDate.Day, 23, 59, 59);

                    if (secondsUsed == MAXTEMPO_SEM_INTERVALO)
                        nextFinalDate = nextFinalDate.AddHours(4);
                    else
                        nextFinalDate = getNextHoraFim(nextFinalDate, getSecondsUsed(dt));


                    if (nextFinalDate > midnight)
                    {
                        dtpHoraFinalRealizada.Value = midnight;
                        nextFinalDate = midnight;
                    }
                    dtpHoraFinalRealizada.Value = nextFinalDate;

                    dtInicio = dtpHoraInicialRealizada.Value;
                    dtFinal  = dtpHoraFinalRealizada.Value;

                    TimeSpan horaTotal = new TimeSpan((dtFinal.Ticks - dtInicio.Ticks));
                    try
                    {
                        dtpHoraTotalRealizada.Text = horaTotal.ToString();
                    }
                    catch(Exception ex)
                    {
                        horaTotal = new TimeSpan(0);
                        dtpHoraTotalRealizada.Text = horaTotal.ToString();
                    }
                }
                return error_code;
        }
            
        private void tabPrincipal_Selected(object sender, TabControlEventArgs e)
        {
            try
            {
                 DataRowView current = (DataRowView)osOrdemServicoBindingSource.Current;
                //if (current["IDCLIENTE"].ToString().Length == 0)
                //    return;
                //
                if (tabPrincipal.SelectedIndex == 1)
                    this.osSituacoesOSTableAdapter.SituacaoRealizada(this.dtsPrincipal.OsSituacoesOS, (int)current["ID"], "S");
                if (tabPrincipal.SelectedIndex == 2)
                    this.osSituacoesOSTableAdapter.SituacaoRealizada(this.dtsPrincipal.OsSituacoesOS, (int)current["ID"], "E");
                if (tabPrincipal.SelectedIndex == 3)
                {
                    this.osSituacoesOSTableAdapter.SituacaoRealizada(this.dtsPrincipal.OsSituacoesOS, (int)current["ID"], "R");
                    updateHorarioRealizado();
                }
                if (tabPrincipal.SelectedIndex == 4)
                    this.osSituacoesOSTableAdapter.SituacaoRealizada(this.dtsPrincipal.OsSituacoesOS, (int)current["ID"], "F");
                if (tabPrincipal.SelectedIndex == 5)
                {
                    //if (current["IDCLIENTE"].ToString().Length == 0)
                    //    return;

                    //this.osInstrumentoServicoTableAdapter.Fill(dtsPrincipal.OsInstrumentoServico);
                    //this.osInstrumentoServicoTableAdapter.SqlEquipamentos(this.dtsPrincipal.OsInstrumentoServico, (int)current["ID"]);

                    this.osInstrumentoServicoOsClienteTableAdapter.SqlEquipamentos(this.dtsPrincipal.OsInstrumentoServicoOsCliente, (int)current["ID"]);
                    //bdsEquipamentoSelos.Filter = "IDCLIENTE = " + current["IDCLIENTE"].ToString();
                    //bdsEquipamentoSelos.Filter = "IDCLIENTE = " + current["IDCLIENTE"];
                    //bdsEquipamentoSelos.Filter += " AND POSTO ='" + Convert.ToInt32(OsConfiguracoes.codigoPosto) + "'";
                    bdsEquipamentoSelosX.Filter = "IDCLIENTE = " + current["IDCLIENTE"];
                   
                    //bdsEquipamentoSelosX.Filter += " AND POSTO ='" + Convert.ToInt32(OsConfiguracoes.codigoPosto) + "'";

                    DataRowView currentIDIBA = (DataRowView)bdsEquipamentoSelosX.Current;
                    if (currentIDIBA != null)
                    {
                        string idibametro = "";
                        idibametro = currentIDIBA["ID"].ToString();
                        bdsINMETROSelosX.Filter = "IDIBAMETRO = " + idibametro;
                    }
                        // ponte safana 09/09/2021 para evitar "efeitos colaterais"
                    string pssql = @"select coalesce(idusuario,id) idusuario from xosfuncionario where id=" + OsConfiguracoes.codigoUsuario;
                    string ps_idusuario = DB.GetSingleValue(pssql).ToString();
                    bdsSelos.Filter = "UTILIZADO = 0 AND TIPO = 'S' AND IDUSUARIO = " + ps_idusuario;

                    //bdsSelos.Filter = "UTILIZADO = 0 AND TIPO = 'S' AND IDUSUARIO = " + OsConfiguracoes.codigoUsuario;
                    bdsSelos.Filter += " AND POSTO ='" + OsConfiguracoes.codigoPosto + "'";

                    if (estadoCorrente == EstadoManutencao.smInserir)
                    {
                        limpaAbaSelos();
                    }

                }

                if (tabPrincipal.SelectedIndex == 6)
                {
                    //this.osInstrumentoServicoTableAdapter.SqlLacre(this.dtsPrincipal.OsInstrumentoServico, (int)current["ID"]);
                    //bdsEquipamentoLacre.Filter = "IDCLIENTE = " + current["IDCLIENTE"].ToString();
                    //bdsEquipamentoLacre.Filter += " AND POSTO ='" + Convert.ToInt32(OsConfiguracoes.codigoPosto) + "'";
                    this.osInstrumentoServicoOsClienteTableAdapter.SqlLacre(this.dtsPrincipal.OsInstrumentoServicoOsCliente, (int)current["ID"]);
                 
                    //bdsEquipamentoLacreX.Filter = "IDCLIENTE = " + current["IDCLIENTE"].ToString();                
                    //bdsEquipamentoLacreX.Filter += " AND POSTO ='" + Convert.ToInt32(OsConfiguracoes.codigoPosto) + "'";

                    bdsEquipamentoLacreX.Filter = "IDCLIENTE = " + current["IDCLIENTE"];

                    DataRowView currentIDIBA_Lacre = (DataRowView)bdsEquipamentoLacreX.Current;
                    if (currentIDIBA_Lacre != null)
                    {
                        string idibametro_lacre = "";
                        idibametro_lacre = currentIDIBA_Lacre["ID"].ToString();
                        bdsINMETROLacresX.Filter = "IDIBAMETRO = " + idibametro_lacre;
                    }

                    // bdsLacre.Filter = "UTILIZADO = 0 AND TIPO = 'L' AND IDUSUARIO = " + OsConfiguracoes.codigoUsuario;
                    // ponte safana 09/09/2021 para evitar "efeitos colaterais"
                    string pssql = @"select coalesce(idusuario,id) idusuario from xosfuncionario where id=" + OsConfiguracoes.codigoUsuario;
                    string ps_idusuario = DB.GetSingleValue(pssql).ToString();
                    bdsLacre.Filter = "UTILIZADO = 0 AND TIPO = 'L' AND IDUSUARIO = " + ps_idusuario;

                    bdsLacre.Filter += " AND POSTO ='" + OsConfiguracoes.codigoPosto + "'";

                    if (estadoCorrente == EstadoManutencao.smInserir)
                    {
                        limpaAbaLacres();
                    }
                }
                if (tabPrincipal.SelectedIndex == 7)
                {
                    this.osItemServicoTableAdapter.SqlItensDaOrdem(this.dtsPrincipal.OsItemServico, (int)current["ID"]);
                    limpaAbaProdutos();
                }
                if (tabPrincipal.SelectedIndex == 9)
                    this.osFormasPagamentoTableAdapter.SqlFormasDePagamento(this.dtsPrincipal.OsFormasPagamento, (int)current["ID"]);

            }
            catch (Exception et)
            {
                string m = @"TAB "+tabPrincipal.SelectedIndex.ToString()+": \r\n" + et.Message;
                MessageBox.Show(m, @"Erro selecionar tab",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Stop);
            }
        }


        private void tsbExcluir_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Deseja realmente descartar essa OS? ", 
                                null, 
                                MessageBoxButtons.YesNo, 
                                MessageBoxIcon.Warning) == DialogResult.Yes)
            {   
                DataRowView current = (DataRowView)osOrdemServicoBindingSource.Current;

                current["STATUS_OS"] = "D";

                osOrdemServicoBindingSource.EndEdit();
                osOrdemServicoTableAdapter.Update(this.dtsPrincipal);
            }
        }


        private void toolStripButton8_Click(object sender, EventArgs e)
        {
            Close();
        }

        // Button btnAdicionarEquipamentos
        // Selos - inserir
        private void button6_Click(object sender, EventArgs e)
        {
            //salvarOrdemServico();
            //salvarOrdemServicoX();
            // julho 2021 uwe
            if (cmbNumSerieSelos.SelectedIndex == -1) {
                MessageBox.Show("Favor, informe o número de série da bomba.");
                return;
            }
            if (cmbNumInmetroSelos.SelectedIndex == -1)
            {
                MessageBox.Show("Favor, informe o número de INMETRO utilizado.");
                return;
            }

            if (cmbNumeroEtiquetaSelos.SelectedIndex == -1)
            {
                MessageBox.Show("Favor, informe o número do selo utilizado.");
                return;
            }

            salvarOrdemServico();

            if (cmbCodigoEquipamentoSelos.Text.Length == 0)
            {
                MessageBox.Show("Informe o número de série da bomba.");
                return;
            }

            if (cmbCodigoINMETROSelos.Text.Length == 0)
            {
                MessageBox.Show("Informe o número de INMETRO utilizado.");
                return;
            }

            if (cmbCodigoSelos.Text.Length == 0)
            {
                MessageBox.Show("Informe o número do selo utilizado.");
                return;
            }

           // DataRowView current = (DataRowView) osInstrumentoServicoBindingSource.AddNew();
            DataRowView current = (DataRowView)osInstrumentoServicoBindingSourceX.AddNew();

            if (lblCodigoOS.Text == "")
            {
                //lblCodigoOS.Text = lblNovoCodOS.Text;
                lblCodigoOS.Text =
                    DB.GetSingleValue("select id from osordemservico where id_final=" 
                    + lblNovoCodOS.Text).ToString();
             }

            current["IDORDEM"] = lblCodigoOS.Text;
            current["IDIBAMETRO"] = cmbCodigoEquipamentoSelos.Text;
            current["IDINMETRO"] = cmbCodigoINMETROSelos.Text;
            current["IDETIQUETA"] = cmbCodigoSelos.Text;

            //string idEtiqueta = cmbCodigoSelos.Text;
            //string idordem = lblCodigoOS.Text;

            // uwe 25.02.2015
            current["MARCA_INICIAL"] = tbMarcaInicial.Text;
            current["SELO_ANTERIOR"] = tbSeloAnterior.Text;

            DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;

            if (estadoCorrente == EstadoManutencao.smInserir)
            {
                //currentOS["IDUSUARIO"] = OsConfiguracoes.codigoUsuario;
                currentOS["IDPOSTO"] = OsConfiguracoes.idPosto;
                currentOS["DT_CADASTRO"] = DateTime.Now;
            }
            osOrdemServicoBindingSource.EndEdit();
            osOrdemServicoTableAdapter.Update(dtsPrincipal);

            //osInstrumentoServicoBindingSource.EndEdit();
            //osInstrumentoServicoTableAdapter.Update(dtsPrincipal);
            osInstrumentoServicoBindingSourceX.EndEdit();
            osInstrumentoServicoOsClienteTableAdapter.Update(dtsPrincipal);
            //
            //this.osInstrumentoServicoTableAdapter.SqlEquipamentos(this.dtsPrincipal.OsInstrumentoServico, (int)currentOS["ID"]);
            this.osInstrumentoServicoOsClienteTableAdapter.SqlEquipamentos(this.dtsPrincipal.OsInstrumentoServicoOsCliente, (int)currentOS["ID"]);
          
            limpaAbaSelos();
        }

        private void calculaHora(object sender, EventArgs e)
        {
            try
            {
                DateTime dtInicio = DateTime.Parse(dtpHoraInicialRealizada.Text);
                DateTime dtFinal = DateTime.Parse(dtpHoraFinalRealizada.Text);

                TimeSpan horaTotal = new TimeSpan(dtFinal.Ticks - dtInicio.Ticks);

                dtpHoraTotalRealizada.Text = horaTotal.ToString();
            }
            catch (Exception)
            {

            }

        }


        private void calculaFormaPagto(object sender, EventArgs e)
        {
            decimal totalDesp = 0;

            if (txtTotalFinalOS.Text.Length > 0)
                totalDesp = totalDesp + decimal.Parse(txtTotalFinalOS.Text);
            if (txtValorPago.Text.Length > 0)
                totalDesp = totalDesp - decimal.Parse(txtValorPago.Text);
        }


        private void chkPorConta_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPorContaGeral.Checked == true)
            {
                txtAlimentacao.Text = "";
                txtHospedagem.Text = "";
                txtTranslado.Text = "";
                txtOutrasDespesas.Text = "";
                grpDespesas.Enabled = false;
            }
            else
                grpDespesas.Enabled = true;
        }


        private void btnAdicionarPecas_Click(object sender, EventArgs e)
        {
            salvarOrdemServico();
            
            DataRowView current = (DataRowView)osItemServicoBindingSource.AddNew();

            //if (lblCodigoOS.Text == "")
            //{
            //    //lblCodigoOS.Text = lblNovoCodOS.Text;
            //    lblCodigoOS.Text =
            //        DB.GetSingleValue("select id from osordemservico where id_final="
            //        + lblNovoCodOS.Text).ToString();
            //}

            current["IDORDEM"] = lblCodigoOS.Text;
            current["IDPRODUTO"] = cmbCodigoIDProduto.Text;
            current["QUANTIDADE"] = numQtdeProduto.Value;
            current["VALOR"] = txtPrecoProduto.Text;

            if (txtTotalProduto.Text.Length > 0)
                current["TOTAL"] = decimal.Parse(txtTotalProduto.Text);
            else
                current["TOTAL"] = 0;

            if (txtTotalGeralPecas.Text.Length < 1)
            {
                txtTotalGeralPecas.Text = "0";
            }

            DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;

            if (estadoCorrente == EstadoManutencao.smInserir)
            {
                //currentOS["IDUSUARIO"] = OsConfiguracoes.codigoUsuario;
                currentOS["IDPOSTO"] = OsConfiguracoes.idPosto;
                currentOS["DT_CADASTRO"] = DateTime.Now;
            }

            /*currentOS["TOTAL_PECAS_OS"] = (decimal)currentOS["TOTAL_PECAS_OS"] + decimal.Parse(txtTotalProduto.Text);

            osOrdemServicoBindingSource.EndEdit();
            osOrdemServicoTableAdapter.Update(dtsPrincipal);

            osItemServicoBindingSource.EndEdit();
            osItemServicoTableAdapter.Update(dtsPrincipal);
            */

            osItemServicoBindingSource.EndEdit();
            osItemServicoTableAdapter.Update(dtsPrincipal);

            currentOS["TOTAL_PECAS_OS"] = osItemServicoTableAdapter.SqlTotalProdutos((int)currentOS["ID"]);//(decimal)currentOS["TOTAL_PECAS_OS"] + decimal.Parse(txtTotalProduto.Text);

            osOrdemServicoBindingSource.EndEdit();
            osOrdemServicoTableAdapter.Update(dtsPrincipal);

            numQtdeProduto.Value = 1;
            numQtdeProduto.Focus();

            this.osItemServicoTableAdapter.SqlItensDaOrdem(this.dtsPrincipal.OsItemServico, (int)currentOS["ID"]);
            limpaAbaProdutos();
        }


        private void btnAdicionarFormaPag_Click(object sender, EventArgs e)
        {
            salvarOrdemServico();

            if (txtTotalPago.Text.Length == 0 )
            {
                txtTotalPago.Text = "0";
            }
            if (decimal.Parse(txtTotalPago.Text) + decimal.Parse(txtValorPago.Text) > decimal.Parse(txtTotalFinalOS.Text))
            {
                    MessageBox.Show("Atenção: O valor do pagamento não pode ultrapassar o valor total da OS.");
                    return;
            }
            else
            {
                DataRowView current = (DataRowView)osFormasPagamentoBindingSource.AddNew();

                //if (lblCodigoOS.Text == "")
                //{
                //    //lblCodigoOS.Text = lblNovoCodOS.Text;
                //    lblCodigoOS.Text =
                //        DB.GetSingleValue("select id from osordemservico where id_final="
                //        + lblNovoCodOS.Text).ToString();
                //}

                current["IDORDEM"] = lblCodigoOS.Text;
                current["IDFORMA"] = cmbCodigoForma.Text;
                if (txtValorPago.Text.Length > 0)
                    current["VALOR"] = decimal.Parse(txtValorPago.Text);

                this.osFormasPagamentoBindingSource.EndEdit();
                this.osFormasPagamentoTableAdapter.Update(dtsPrincipal);

                if (txtTotalPago.Text.Length < 1)
                {
                    txtTotalPago.Text = "0";
                }

                DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;
                currentOS["VALOR_PAGO"] = (decimal.Parse(txtTotalPago.Text) + decimal.Parse(txtValorPago.Text)).ToString();
                currentOS["VALOR_RESTANTE"] = (decimal.Parse(txtTotalFinalOS.Text) - decimal.Parse(txtTotalPago.Text)).ToString();

                //txtTotalPago.Text = (decimal.Parse(txtTotalPago.Text) + decimal.Parse(txtValorPago.Text)).ToString();/*osFormasPagamentoTableAdapter.SqlSomaPagamentos(int.Parse(label8.Text)).ToString();*/
                //txtValorAPagar.Text = (decimal.Parse(txtTotalFinalOS.Text) - decimal.Parse(txtTotalPago.Text)).ToString();
                //txtValorAPagar.Text = osOrdemServicoTableAdapter.SqlDiferenca(int.Parse(label8.Text)).ToString();
                if (estadoCorrente == EstadoManutencao.smInserir)
                {
                    //currentOS["IDUSUARIO"] = OsConfiguracoes.codigoUsuario;
                    currentOS["IDPOSTO"] = OsConfiguracoes.idPosto;
                    currentOS["DT_CADASTRO"] = DateTime.Now;
                }

                osOrdemServicoBindingSource.EndEdit();
                osOrdemServicoTableAdapter.Update(dtsPrincipal);

                osFormasPagamentoBindingSource.EndEdit();
                osFormasPagamentoTableAdapter.Update(dtsPrincipal);

                txtValorPago.Clear();
                txtValorPago.Focus();
            }
        }


        private void imprimirRecibo(string id_final)
        {
            RelRecibo relRecibo = new RelRecibo();

            ParameterFields paramFields = new ParameterFields();
            ParameterField pfItemYr = new ParameterField();
            pfItemYr.ParameterFieldName = "Ordem";
            ParameterDiscreteValue dcItemYr = new ParameterDiscreteValue();
            // 28.04. Uwe
            // Ponte Safana ...
            if (lblCodigoOS.Text.Equals(""))
            {
                lblCodigoOS.Text = DB.GetSingleValue("select id from osordemservico where id_final=" + id_final).ToString();
            }
            dcItemYr.Value = lblCodigoOS.Text;
            pfItemYr.CurrentValues.Add(dcItemYr);
            paramFields.Add(pfItemYr);
            relRecibo.crystalReportViewer1.ParameterFieldInfo = paramFields;

            // Se houve pagamento...
            if (txtTotalFinalOS.Text.Length > 0)
            {
                NumeroPorExtenso extenso = new NumeroPorExtenso();
                DataRowView current = (DataRowView)osOrdemServicoBindingSource.Current;
                extenso.SetNumero((decimal) current["VALOR_PAGO"]);

                OsConfiguracoes.literalExtenso = extenso.ToString();

                if ((decimal)(current["TOTAL_OS"]) == (decimal)(current["VALOR_PAGO"]))
                    OsConfiguracoes.literalRecibo = "Referente ao pagamento da Ordem de Serviço, nº " + lblNovoCodOS.Text;
                else
                    OsConfiguracoes.literalRecibo = "Referente ao pagamento PARCIAL da Ordem de Serviço, nº " + lblNovoCodOS.Text;
            }
            relRecibo.Show();
        }


        private void imprimirOS(String codigoOS, String arquivo)
        {
            string solicitante = "solicitante";
            DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;
            int id_cliente = (int)currentOS["IDCLIENTE"];
            
            // BUG 06082021var id_solicitante = currentOS["SOLICITANTE"];
            
            var id_os = (int)currentOS["ID"];
            int id_solicitante;
            try
            {
                id_solicitante = (int)DB.GetSingleValue(@"select solicitante from OsOrdemServico where id = " + id_os);
            }
            catch (Exception exsol) { id_solicitante = -1; }
            //string cliente = "";
            bool isA30Clientes = true;
            try
            {               
                int cnt = (int)DB.GetSingleValue(@"SELECT count(1) FROM OsCliente WHERE id=" + id_cliente);
                isA30Clientes = cnt == 0;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return;
            }
            // change id_antigo to id
            if (isA30Clientes) solicitante = DB.GetSingleValue(@"SELECT razao_social from OsCliente where id_antigo = " + id_cliente).ToString();          
            // 
            else
            // OsCliente
            {
                //if (id_solicitante == DBNull.Value)
                if (id_solicitante == -1)
                {
                    solicitante = DB.GetSingleValue(@"select razao_social from OsCliente where id = " + id_cliente).ToString();
                }
                else 
                {
                    solicitante = DB.GetSingleValue(@"select razao_social from OsCliente where id = " + id_solicitante).ToString();
                }
            }
            


            string fileName = "Ordem de Servico nº " + arquivo + ".pdf";

            relOrdemServico = new RelOrdem();
            // 28.04. Uwe
            // Ponte Safana ...
            if (codigoOS.Equals(""))
            {
                codigoOS = DB.GetSingleValue("select id from osordemservico where id_final=" + arquivo).ToString();
            }
            relOrdemServico.configuraOS(codigoOS, fileName, solicitante);           

            relOrdemServico.Show();
        }


        private void tsbImprimirRecibo_Click(object sender, EventArgs e)
        {
            DataRowView current = (DataRowView)osOrdemServicoBindingSource.Current;
            if ((Boolean)current["RECIBO_IMPRESSO"] == false)
            {
                if (MessageBox.Show("O recibo ainda não foi impresso deseja imprimir agora?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    current["RECIBO_IMPRESSO"] = true;
                    osOrdemServicoBindingSource.EndEdit();
                    osOrdemServicoTableAdapter.Update(dtsPrincipal);

                    DataRowView currentRecibo = (DataRowView)osReciboBindingSource.AddNew();

                    // 28.04. Uwe
                    // Ponte Safana ...
                    if (lblCodigoOS.Text.Equals(""))
                    {
                        lblCodigoOS.Text = current["id_final"].ToString();
                    }

                    currentRecibo["IDORDEM"] = lblCodigoOS.Text;

                    osReciboBindingSource.EndEdit();
                    osReciboTableAdapter.Update(dtsPrincipal);

                    imprimirRecibo(current["id_final"].ToString());
                }
            }
            else
                imprimirRecibo(current["id_final"].ToString());
        }


        private void tsbImprimirOS_Click(object sender, EventArgs e)
        {
            imprimirOS(lblCodigoOS.Text, lblNovoCodOS.Text);
        }


        private void osFormasPagamentoBindingSource_CurrentChanged(object sender, EventArgs e)
        {
            if (osFormasPagamentoBindingSource.Count > 0)
                tsbImprimirRecibo.Enabled = true;
            else
                tsbImprimirRecibo.Enabled = false;
        }


        private void chkPorContaAlimentacao_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPorContaAlimentacao.Checked == true)
            {
                txtAlimentacao.Text = "0";
                txtAlimentacao.Enabled = false;
            }
            else
                txtAlimentacao.Enabled = true;
        }


        private void chkPorContaHospedagem_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPorContaHospedagem.Checked == true)
            {
                txtHospedagem.Text = "0";
                txtHospedagem.Enabled = false;
            }
            else
                txtHospedagem.Enabled = true;
        }


        private void chkPorContaTranslado_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPorContaTranslado.Checked == true)
           { 
                txtTranslado.Text = "0";
                txtTranslado.Enabled = false;
            }
            else
                txtTranslado.Enabled = true;
        }

        private void chkPorContaOutros_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPorContaOutros.Checked == true)
            {
                txtOutrasDespesas.Text = "0";
                txtOutrasDespesas.Enabled = false;
            }
            else
                txtOutrasDespesas.Enabled = true;
        }
        /// <summary>
        /// Modificação Selo/Lacre 
        /// Uwe Kristmann 
        /// 17.02.2014
        /// </summary>
        /// <param name="id_ordemServico">id de ordem de serviço
        /// referente aos quais os selos ou lacres serão marcados como ultilizados.
        /// </param>
        private void updateSeloLacre(int id_ordemServico)
        {
            System.Diagnostics.Debug.WriteLine("id_ordemServico = " + id_ordemServico.ToString());
            List<int> selo_e_lacre = 
             DB.GetList<int>("Select idetiqueta From OsInstrumentoServico where idordem=" + id_ordemServico.ToString());
            foreach ( int etiqueta in selo_e_lacre )
            {
              System.Diagnostics.Debug.WriteLine("etiqueta = " + etiqueta.ToString());            
              DB.ExecuteNonQuery("Update OsEtiqueta set UTILIZADO = 1 Where id=" + etiqueta.ToString()); 
            }
            //
            List<int> selo_e_lacreX =
            DB.GetList<int>("Select idetiqueta From OsInstrumentoServicoOsCliente where idordem=" + id_ordemServico.ToString());
            //foreach (int etiqueta in selo_e_lacre)
            //ERRO Sabado,13.02.2021
            foreach (int etiqueta in selo_e_lacreX)
            {
                System.Diagnostics.Debug.WriteLine("etiqueta = " + etiqueta.ToString());
                DB.ExecuteNonQuery("Update OsEtiqueta set UTILIZADO = 1 Where id=" + etiqueta.ToString());
            }
        }

        private void tsbFecharOS_Click(object sender, EventArgs e)
        {
            //UXX
            DataRowView currentOSAbrirOS = (DataRowView)osOrdemServicoBindingSource.Current;
            var id_final = currentOSAbrirOS["ID_FINAL"];
            var idusuario = currentOSAbrirOS["IDUSUARIO"];

            // reabrir OS fechado uwe 26.03.2018
            if (tsbFecharOS.Text.Contains("Abri"))
            {
                string sql = "Update OsOrdemServico set status_os = 'A' Where ID_FINAL='" + id_final + "'";
                DB.ExecuteNonQuery(sql);
                
                #region voltar a CONSULTA depois ter aberto o OS
                // acionar botão CONSULTA
                toolStripButton3.PerformClick();
                // set Filtro Fechada
                cmbFiltroSituacao.SelectedIndex = 0; //1;
                /*
                int idx = bdsFiltroUsuario.Find(@"Id", idusuario);
                if (idx >= 0)
                    bdsFiltroUsuario.Position = idx;
                 */
                int idx = bdsFiltroXOSFuncionario.Find(@"Idusuario", idusuario);
                if (idx >= 0)
                    bdsFiltroXOSFuncionario.Position = idx;
                // set Filtro Situação Fechada
                chkFiltrarSituacao.Checked = false;
                chkFiltrarSituacao.Checked = true;
                // set Filtro OsConfiguracoes.codigoUsuario
                chkFiltrarUsuario.Checked = false;
                chkFiltrarUsuario.Checked = true;
                // achar o OS recem fechado no DataGridView
                idx = osOrdemServicoBindingSource.Find(@"Id_Final", id_final);
                if (idx >= 0)
                {
                    osOrdemServicoBindingSource.Position = idx;
                    //osOrdemServicoDataGridView.Columns[idx].Selected = true;
                    osOrdemServicoDataGridView.Rows[idx].Selected = true;
                    // acionar DoubleClick 
                    osOrdemServicoDataGridView_CellDoubleClick(
                        osOrdemServicoDataGridView, new DataGridViewCellEventArgs(0, idx));

                    tsbFecharOS.Enabled = true;
                    tsbFecharOS.Text = "&Fechar OS";
                    tsbFecharOS.ToolTipText = @"Fechar OS";
                }
                cmbSolicitante.SelectedIndex = bs_solicitante.Position;
                #endregion
                return;
            }

            string email = txtEmail.Text;
           
            String codigoOS = lblCodigoOS.Text;
            String codigoOSFinal = lblNovoCodOS.Text;
            relOrdemServico = new RelOrdem();
          
            // 06/08/2021 uwe /aqui se perde o campo "solicitante" que não faz parte do binding ...
            int id_os = (int)currentOSAbrirOS["ID"];
            int id_solicitante = (int)DB.GetSingleValue("SELECT COALESCE(solicitante,-1) from osordemservico where id = " + id_os.ToString());

            salvarOrdemServico();           


            DataRowView current = (DataRowView)osOrdemServicoBindingSource.Current;
            
            // 17.02.2014 Modificação Uwe Kristmann
            // marcar Selo/Lacre utilizado  
            updateSeloLacre((int)current["ID"]);

            if (MessageBox.Show("Deseja Realmente Fechar essa OS?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                NumeroPorExtenso extenso = new NumeroPorExtenso();
                extenso.SetNumero(decimal.Parse(txtValorAPagar.Text));
                OsConfiguracoes.literalExtenso = extenso.ToString();

                current["STATUS_OS"] = "F";
                current["DT_FECHAMENTO"] = DateTime.Now;
                current["VALOR_RESTANTE_EXTENSO"] = OsConfiguracoes.literalExtenso;
                
                osOrdemServicoBindingSource.EndEdit();
                osOrdemServicoTableAdapter.Update(this.dtsPrincipal);

                // EMAIL PATRICIA desativado temporariamente 
                // enviaEmail.enviarTecnoExpress(codigoOSFinal, OsConfiguracoes.nomeUsuario, "patricia@tecnoexpress.com.br", "O");

                // 29.05.2014 Uwe
                //tsbGerarCaixa.Enabled = true;

                // corrigir solicitante
                if (id_solicitante != -1)
                {
                    DB.ExecuteNonQuery("UPDATE OsOrdemservico SET solicitante = " + id_solicitante.ToString() + " WHERE id = " + id_os.ToString());
                }

                MessageBox.Show("OS fechada com sucesso!");
                estadoCorrente = EstadoManutencao.smPesquisar;

                #region Impressora, Email ao cliente
                // 17.02.2014 Modificação Uwe Kristmann
                // "DebugMode"
                string cnn = ConfigurationManager.ConnectionStrings["osExpress.Properties.Settings.OsExpressConnectionString"].ToString();
                if (cnn.Substring(12, 19).Equals(@"192.168.186.1\posto"))
                {
                    MessageBox.Show("DEBUGMODE - Impressora não funciona.");
                    //estadoCorrente = EstadoManutencao.smPesquisar;
                }
                else
                {
                    try
                    {
                        var aaa = 1;
                        imprimirOS(codigoOS, codigoOSFinal);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Verifique a disponibilidade da impressora!", "ERRO - impressão");
                    }
                }

                if (MessageBox.Show("Deseja enviar o email para o cliente?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    enviaEmail.enviarCliente(email, "OS", "Ordem de Servico nº " + codigoOSFinal + ".pdf", corpoOS);
                    MessageBox.Show("Email enviado com sucesso!");
                }
                #endregion

                #region voltar a CONSULTA depois ter fechado o OS
                // acionar botão CONSULTA
                toolStripButton3.PerformClick();
                // set Filtro Fechada
                cmbFiltroSituacao.SelectedIndex = 1;
                /*
                int idx = bdsFiltroUsuario.Find(@"Id", OsConfiguracoes.codigoUsuario);
                if (idx >= 0)
                    bdsFiltroUsuario.Position = idx;*/
                int idx = bdsFiltroXOSFuncionario.Find(@"Idusuario", OsConfiguracoes.codigoUsuario);
                if (idx >= 0)
                    bdsFiltroXOSFuncionario.Position = idx;
                // set Filtro Situação Fechada
                chkFiltrarSituacao.Checked = false;
                chkFiltrarSituacao.Checked = true;
                // set Filtro OsConfiguracoes.codigoUsuario
                chkFiltrarUsuario.Checked = false;
                chkFiltrarUsuario.Checked = true;
                // achar o OS recem fechado no DataGridView
                idx = osOrdemServicoBindingSource.Find(@"Id_Final", codigoOSFinal);
                if (idx >= 0)
                {
                    osOrdemServicoBindingSource.Position = idx;
                       //osOrdemServicoDataGridView.Columns[idx].Selected = true;
                    osOrdemServicoDataGridView.Rows[idx].Selected = true;
                    // acionar DoubleClick 
                    osOrdemServicoDataGridView_CellDoubleClick(
                        osOrdemServicoDataGridView, new DataGridViewCellEventArgs(0, idx));

                    tsbFecharOS.Enabled = true;
                }
                #endregion

                cmbSolicitante.SelectedIndex = bs_solicitante.Position;

            }
        }


        private void grdPagamentos_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            DataRowView currentPagamentos = (DataRowView)osFormasPagamentoBindingSource.Current;
            DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;

            currentOS["VALOR_PAGO"] = (decimal)currentOS["VALOR_PAGO"] - (decimal)currentPagamentos["VALOR"];
            currentOS["VALOR_RESTANTE"] = (decimal)currentOS["VALOR_RESTANTE"] + (decimal)currentPagamentos["VALOR"];

            osOrdemServicoBindingSource.EndEdit();
            osOrdemServicoTableAdapter.Update(dtsPrincipal);
        }

        private void grdPecas_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;
            osItemServicoBindingSource.EndEdit();
            osItemServicoTableAdapter.Update(dtsPrincipal);

            currentOS["TOTAL_PECAS_OS"] = osItemServicoTableAdapter.SqlTotalProdutos((int)currentOS["ID"]);//(decimal)currentOS["TOTAL_PECAS_OS"] - (decimal)currentPecas["TOTAL"];
            currentOS["TOTAL_OS"] = osItemServicoTableAdapter.SqlTotalProdutos((int)currentOS["ID"]);//(decimal)currentOS["TOTAL_OS"] - (decimal)currentPecas["TOTAL"];

            osOrdemServicoBindingSource.EndEdit();
            osOrdemServicoTableAdapter.Update(dtsPrincipal);
        }

        private void grdPecas_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            /*DataRowView currentPecas = (DataRowView)osItemServicoBindingSource.Current;
            DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;

            currentOS["TOTAL_PECAS_OS"] = (decimal)currentOS["TOTAL_PECAS_OS"] - (decimal)currentPecas["TOTAL"];
            currentOS["TOTAL_OS"] = (decimal)currentOS["TOTAL_OS"] - (decimal)currentPecas["VALOR"];

            osOrdemServicoBindingSource.EndEdit();
            osOrdemServicoTableAdapter.Update(dtsPrincipal);*/

        }


        /// <summary>
        /// CANCELAR
        /// </summary>    
        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            resetSolicitante();
            tsbCancelar.Enabled = false;
            tsbInserir.Enabled = true;
            osOrdemServicoBindingSource.CancelEdit();
            pnlConsultar.Visible = true;
            // mod 23.03.2015 uwe
            toolStripButton3.Enabled = true;
            pesquisar();
            // end mod 23.03.2015
        }

        private int getPosto()
        {
            return (int) DB.GetSingleValue(@"select id from a30postos where codigo='"+OsConfiguracoes.codigoPosto+"'");
        }

        private void chkFiltrarSelecoes_CheckedChanged(object sender, EventArgs e)
        {
            string sAnd = " AND ";
            string sFilter = "";

            // idx = osOrdemServicoBindingSource.Find(@"Id_Final", codigoOSFinal);
            // Limpa o filtro antes.
            osOrdemServicoBindingSource.Filter = "";

            //return;

            if ( OsConfiguracoes.tipoAcesso == 'A')
            {
                sFilter = sFilter = " TIPO_DOC = 'S' "+sAnd;    
            }
            else
            {
                sFilter = sFilter = "TIPO_DOC = 'S' AND ID_USR_OFFLINE = " + "'" + OsConfiguracoes.codigoUsuario + "'" + sAnd;
                cmbFiltroUsuarioCodigo.Enabled = false;
                cmbFiltroUsuarioNome.Enabled = false;
                chkFiltrarUsuario.Enabled = false;
            }

            if (chkFiltrarSituacao.Checked)
            {
                sFilter = sFilter + " STATUS_OS = " + "'" + cmbFiltroSituacao.Text.Substring(0, 1) + "'" + sAnd;
            }
            if (chkFiltrarUsuario.Checked)
            {
                sFilter = sFilter + " ID_USR_OFFLINE = " + cmbFiltroUsuarioNome.SelectedValue + sAnd;
            }
            if (chkFiltrarCliente.Checked)
            {
                sFilter = sFilter + " IDCLIENTE = " + cmbFiltroClienteNome.SelectedValue + sAnd;
            }

            //sFilter = sFilter + " IDPOSTO = " + getPosto() + sAnd;

            if (chkPostoOS.Checked)
            {
                int selected = cmbPostoOS.SelectedIndex;
                if (selected >= 0)
                {
                    int posto = (int)((DataRowView)cmbPostoOS.SelectedItem)[0];
                    sFilter = sFilter + " IDPOSTO=" + posto.ToString() + sAnd;
                }

            }

            if (chkFiltrarPeriodo.Checked)
            {
                sFilter = sFilter + " dt_fechamento >= '" + dtp_de.Value.Date
                                  + "' and dt_fechamento <= '" + dtp_ate.Value.Date +"'";
                sFilter = sFilter + sAnd;
            }
            
            // Se existir filtro...
            string ssFilter = sFilter.Substring(0, sFilter.Length - 5);
            if (sFilter.Length > 0)
                osOrdemServicoBindingSource.Filter = sFilter.Substring(0, sFilter.Length - 5);
            // 29.05.2014 Uwe
            //tsbGerarCaixa.Enabled = false;
            lblTotalOS.Text = "Total: " + osOrdemServicoBindingSource.Count.ToString();
        }


        private void toolStripButton3_Click_1(object sender, EventArgs e)
        {
            resetSolicitante();
            osOrdemServicoBindingSource.CancelEdit();
            pnlConsultar.Visible = true;            
            //this.osOrdemServicoTableAdapter.SqlOrdensServico(this.dtsPrincipal.OsOrdemServico);
            this.osOrdemServicoTableAdapter.SqlOrdensServicoXOSFuncionario(this.dtsPrincipal.OsOrdemServico);
            // 29.05.2014 Uwe
            //tsbGerarCaixa.Enabled = false;
            //
            pesquisar();
        }

        private void setSolicitante() 
        {
            DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;
            // preselect SOLICITANTE                
            if (currentOS["SOLICITANTE"] != DBNull.Value)
            {                
                // diferente do cliente 
                int index = bs_solicitante.Find(@"Id", currentOS["SOLICITANTE"]);
                if (index >= 0)
                {
                    // muss hier gesetzt werden, sonst wird die checbox weider resettet... 
                    chkSolicitante.Checked = false;

                    cmbSolicitante.Enabled = true;
                    //cmbSolicitante.SelectedIndex = index;
                    //cmbSolicitante.SelectedValue = currentOS["SOLICITANTE"];
                    bs_solicitante.Position = index;
                   
                }
            }

            /// 15.02.2021 uwe
            //DataRowView current = (DataRowView)osOrdemServicoBindingSource.Current;
            //var currentUsuario = (DataRowView)osUsuarioBindingSource.Current;

            //var tipo_acesso = (char)currentUsuario["TIPO_ACESSO"];

            if (currentOS["STATUS_OS"].ToString() == "F")
            {
                chkSolicitante.Enabled = false;
                cmbSolicitante.Enabled = false;
            }

            if (currentOS["STATUS_OS"].ToString() == "A")
            {
                chkSolicitante.Enabled = true;
                cmbSolicitante.Enabled = true;
            }
        }

        private void osOrdemServicoDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            //return;
            
            //pnlConsultar.Visible = false;
            // 29.05.2014 Uwe
            //tsbGerarCaixa.Enabled = false;

            //pnlConsultar.Visible = false;
            //// 29.05.2014 Uwe
            //tsbGerarCaixa.Enabled = false;
            DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;
            ////if (cmbFiltroSituacao.SelectedItem.ToString()[0] == 'F')
            //if (currentOS["STATUS_OS"].ToString() == "F")
            //{
            //    //tsbGerarCaixa.Enabled = true;
            //    // 29.05.2014 Uwe
            //    // BUG: tsbGerarCaixa.Enabled = true;
            //    // 26.03.2018 Uwe
            //    if ((bool)currentOS["FATURA_GERADA"] == false)
            //    {
            //        tsbGerarCaixa.Enabled = true;
            //    }
            //}

            //
            // Juni 2018 - Sort on click dataGridViewHeader
            var bs = osOrdemServicoBindingSource;

            //var colNames = currentOS.DataView.Table.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToList();

            var ri = e.RowIndex;
            var ci = e.ColumnIndex;

            // observacao
            //if (ci == 7) return;
           
            //var sortorder = bs.Sort.Contains("ASC") ? "DESC" : "ASC";

            //var col = "";
            //if (ci == 0) col = "ID ";
            //if (ci == 1) col = "IDUSUARIO ";
            //if (ci == 2) col = "NOME_USUARIO ";
            //if (ci == 3) col = "IDCLIENTE ";
            //if (ci == 4) col = "nome ";
            //if (ci == 5) col = "DT_CADASTRO ";
            //if (ci == 6) col = "DT_FECHAMENTO ";

            if (ri == -1)
            {
                //Debug.WriteLine(col + sortorder);
                //bs.Sort = col + sortorder;
                //osOrdemServicoDataGridView.Update();
            }
            else
                //enableDisableCaixaGerar(currentOS);
                abrirFecharOS();
            if (e.ColumnIndex == 0)
            {
                setEmpresa();
            }
            setSolicitante();
            
        }
        private void setEmpresa() { 
            DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;
            // 
            string cliente = currentOS["IDCLIENTE"].ToString();
            string csql = @"Select count(*) anz from OsIbametroOsCliente where idcliente = " + cliente;
            int cc = Convert.ToInt32(DB.GetSingleValue(csql).ToString());
            if (cc > 0)
            {
                string sql = @"Select max(posto) from OsIbametroOsCliente where idcliente = " + cliente;
                int idx = Convert.ToInt32(DB.GetSingleValue(sql).ToString());

                cmbEmpresa.SelectedIndex = idx-1;

            }
        }
        
        private void abrirFecharOS() {

            pnlConsultar.Visible = false;

            DataRowView current = (DataRowView)osOrdemServicoBindingSource.Current;
            var currentUsuario = (DataRowView)osUsuarioBindingSource.Current;

            var tipo_acesso = (char)currentUsuario["TIPO_ACESSO"];

            if (current["STATUS_OS"].ToString() == "F")
            {
                if (((bool)current["FATURA_GERADA"] == false) && (tipo_acesso == 'A'))
                {
                    tsbFecharOS.Enabled = true;
                    tsbFecharOS.Text = "Abri&r OS";
                    tsbFecharOS.ToolTipText = @"Abrir OS";
                }
                else
                {
                    //tsbFecharOS.Enabled = false;
                    //tsbFecharOS.Text = "&Fechar OS";
                    //tsbFecharOS.ToolTipText = @"Fechar OS";
                    tsbFecharOS.Enabled = false;
                    tsbFecharOS.Text = "Abri&r OS";
                    tsbFecharOS.ToolTipText = @"Abrir OS";
                }
            }

            if (current["STATUS_OS"].ToString() == "A")
            {
                if (((bool)current["FATURA_GERADA"] == false) && (tipo_acesso == 'A'))
                {
                    tsbFecharOS.Enabled = true;
                    tsbFecharOS.Text = "&Fechar OS";
                    tsbFecharOS.ToolTipText = @"Fechar OS";                    
                }
                else
                {
                    tsbFecharOS.Enabled = false;
                    tsbFecharOS.Text = "&Fechar OS";
                    tsbFecharOS.ToolTipText = @"Fechar OS";      
                }
            }


        }
        private void enableDisableCaixaGerar(DataRowView current)
        {
            pnlConsultar.Visible = false;
            // 29.05.2014 Uwe
            //tsbGerarCaixa.Enabled = false;
            //DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;
            //if (cmbFiltroSituacao.SelectedItem.ToString()[0] == 'F')

            if (current["STATUS_OS"].ToString() == "F")
            {
                //tsbGerarCaixa.Enabled = true;
                //
                // 29.05.2014 Uwe
                // BUG: tsbGerarCaixa.Enabled = true;
                // 26.03.2018 Uwe
                var currentUsuario = (DataRowView)osUsuarioBindingSource.Current;
                
                var tipo_acesso = (char) currentUsuario["TIPO_ACESSO"];
                if (   ((bool) current["FATURA_GERADA"] == false) && (tipo_acesso == 'A'))
                {
                    //tsbGerarCaixa.Enabled = true;
                    //
                    // if usuario == admin
                    tsbFecharOS.Enabled = true;
                    tsbFecharOS.Text = "Abri&r OS";
                    tsbFecharOS.ToolTipText = @"Abrir OS";
                }
                else
                {
                    tsbFecharOS.Enabled = false;
                    tsbFecharOS.Text = "&Fechar OS";
                    tsbFecharOS.ToolTipText = @"Fechar OS";
                }
            }
            //else
            //{
            //    tsbFecharOS.Enabled = false;
            //    tsbFecharOS.Text = "&Fechar OS";
            //    tsbFecharOS.ToolTipText = @"Fechar OS";
            //}

            updateCabecalho(Convert.ToInt32(current["IDCLIENTE"].ToString()));
        }

        private void osOrdemServicoDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //pnlConsultar.Visible = false;
            //// 29.05.2014 Uwe
            //tsbGerarCaixa.Enabled = false;
            DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;
            ////if (cmbFiltroSituacao.SelectedItem.ToString()[0] == 'F')
            //if (currentOS["STATUS_OS"].ToString() == "F")
            //{
            //    //tsbGerarCaixa.Enabled = true;
            //    // 29.05.2014 Uwe
            //    // BUG: tsbGerarCaixa.Enabled = true;
            //    // 26.03.2018 Uwe
            //    if ((bool) currentOS["FATURA_GERADA"] == false)
            //    {
            //        tsbGerarCaixa.Enabled = true;
            //    }
            //}
            //// end 28.03.2018
            //// NAO FUNCIONA por causa das linhas duplas/triplas ...
            //// osOrdemServicoBindingSource nao considera isso !
            ////int index = bs_cabecalho.Find(@"id", currentOS["IDCLIENTE"]);
            ////if (index >= 0)
            ////{
            ////    bs_cabecalho.Position = index;
            ////}
           
            //updateCabecalho(Convert.ToInt32(currentOS["IDCLIENTE"].ToString()));

            var ri = e.RowIndex;
            var ci = e.ColumnIndex;

            if (ri > -1)
                //enableDisableCaixaGerar(currentOS);
                abrirFecharOS();

            setSolicitante();

        }

        private void updateCabecalho(int id)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();

            DataTable dt
                =
                DB.GetTableFromSQL(
                    @"select id,nome,logotipo,cliente,inscricao,cgc,telefone1,endereco, cidade, estado 
                      from a30clientes where 1=1 and id="+id);

            if (dt.Rows.Count == 0) { 
               dt =
                DB.GetTableFromSQL(
                    @"select id,razao_social nome, nome_fantasia logotipo,id cliente,ie inscricao,cnpj cgc,telefone telefone1,endereco, cidade, uf estado 
                      from osCliente where 1=1 and id=" +id);
            }



            dic["nome"] = dt.Rows[0]["Nome"].ToString();
            dic["inscricao"] = dt.Rows[0]["Inscricao"].ToString();
            dic["cgc"] = dt.Rows[0]["cgc"].ToString();
            dic["telefone1"] = dt.Rows[0]["telefone1"].ToString();
            dic["endereco"] = dt.Rows[0]["endereco"].ToString();
            dic["cidade"] = dt.Rows[0]["cidade"].ToString();
            dic["estado"] = dt.Rows[0]["estado"].ToString();


            lblCabCliente.Text = dic["nome"];
            lblCabIE.Text = dic["inscricao"];
            lblCabNCPJ.Text = dic["cgc"];
            lblCabTelefone.Text = dic["telefone1"];
            lblCabObservacao.Text = dic["endereco"];
            lblCidade2.Text = dic["cidade"];
            lblUF2.Text = dic["estado"];

            lblCidade.Location = new Point(9,130);
            lblCidade2.Location = new Point(116, 130);
            lblUF.Location = new Point(9, 146);
            lblUF2.Location = new Point(116, 146);

        }

        private void osOrdemServicoDataGridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.S)
            {
                pnlConsultar.Visible = false;
                DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;
                //if (cmbFiltroSituacao.SelectedItem.ToString()[0] == 'F')
                //if (currentOS["STATUS_OS"].ToString() == "F")
                //    tsbGerarCaixa.Enabled = true;
               abrirFecharOS();
               setSolicitante();

            }
        }


        private void tsbGerarCaixa_Click(object sender, EventArgs e)
        {
            
            // Essa rotina é responsável faz a chamada da procedure de fechamento do caixa,
            if (decimal.Parse(txtTotalFinalOS.Text) > 0)
            {
                var connectionString = ConfigurationManager.ConnectionStrings["osExpress.Properties.Settings.OsExpressConnectionString"].ConnectionString;
                SqlConnection conn = new SqlConnection(connectionString);
                SqlCommand cmd = new SqlCommand("dbo.osPrcGeraCaixa", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter param = new SqlParameter("@CODIGO_ORDEM", int.Parse(lblCodigoOS.Text));
                param.Direction = ParameterDirection.Input;
                param.DbType = DbType.Int32;
                
                cmd.Parameters.Add(param);
                cmd.Connection.Open();
                if (cmd.ExecuteNonQuery() > 0)
                    MessageBox.Show("Caixa gerado com sucesso!");
                cmd.Connection.Close();
            }
        }

        private void btnInserirSituacaoEncontrada_Click(object sender, EventArgs e)
        {
            salvarOrdemServico();

            DataRowView current = (DataRowView)osSituacoesOSBindingSource.AddNew();

            current["IDORDEM"] = lblCodigoOS.Text;
            current["IDSITUACAO"] = cmbCodigoSitEncontrada.Text;

            DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;

            if (estadoCorrente == EstadoManutencao.smInserir)
            {
                //currentOS["IDUSUARIO"] = OsConfiguracoes.codigoUsuario;
                currentOS["IDPOSTO"] = OsConfiguracoes.idPosto;
                currentOS["DT_CADASTRO"] = DateTime.Now;
            }
            osOrdemServicoBindingSource.EndEdit();
            osOrdemServicoTableAdapter.Update(dtsPrincipal);

            osSituacoesOSBindingSource.EndEdit();
            osSituacoesOSTableAdapter.Update(dtsPrincipal);

            this.osSituacoesOSTableAdapter.SituacaoRealizada(this.dtsPrincipal.OsSituacoesOS, (int)currentOS["ID"], "E");
        }

        private void btnInserirTipoSolicitacao_Click(object sender, EventArgs e)
        {
            salvarOrdemServico();

            DataRowView current = (DataRowView)osSituacoesOSBindingSource.AddNew();

            current["IDORDEM"] = lblCodigoOS.Text;
            current["IDSITUACAO"] = cmbCodigoSolicitacao.Text;

            DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;

            if (estadoCorrente == EstadoManutencao.smInserir)
            {
                //currentOS["IDUSUARIO"] = OsConfiguracoes.codigoUsuario;
                currentOS["IDPOSTO"] = OsConfiguracoes.idPosto;
                currentOS["DT_CADASTRO"] = DateTime.Now;
            }
           
            // column idusuario does not allow NULL
            osOrdemServicoBindingSource.EndEdit();
            osOrdemServicoTableAdapter.Update(dtsPrincipal);

            osSituacoesOSBindingSource.EndEdit();
            osSituacoesOSTableAdapter.Update(dtsPrincipal);

            this.osSituacoesOSTableAdapter.SituacaoRealizada(this.dtsPrincipal.OsSituacoesOS, (int)currentOS["ID"], "S");
        }

        /// <summary>
        /// 22.04.2014 Uwe
        /// Verificar se o novo horário inicial não interfera
        /// com o último horário fim.
        /// </summary>
        /// <returns>se novo horario inicial seja válido ou não</returns>
        private bool horarioInvalido()
        {
            DateTime dtMaxFim = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
           
            string id_final = lblNovoCodOS.Text;
            //int id_ordem = (int) DB.GetSingleValue("select * from osordemservico where ID_final=" + id_final.ToString());

            //erik inicio
            int id_ordem = 0;

            string connectionString = ConfigurationManager.ConnectionStrings["osExpress.Properties.Settings.OsExpressConnectionString"].ConnectionString;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand("select id from osordemservico where ID_final=" + id_final.ToString(), connection))
                {
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        // Check is the reader has any rows at all before starting to read.
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                id_ordem = reader.GetInt32(0);
                            }
                        }
                    }
                }
            }

            //erik fim

            DateTime dtInicio = DateTime.Parse(dtpHoraInicialRealizada.Text);
            DateTime dtFinal  = DateTime.Parse(dtpHoraFinalRealizada.Text);
           
            DataTable dt = linhasSituacao(id_ordem);

            int x = getSecondsUsed(dt);           

            int cnt = (int)DB.GetSingleValue(@"select count(1)  from ossituacoesos where 1=1
                                        and not HORA_INICIO is null 
                                        and IDordem=" + id_ordem.ToString());
            if (cnt > 0)
            {
                dtMaxFim = (DateTime)DB.GetSingleValue(
                                              @"select max(hora_fim) from ossituacoesos where 1=1
                                               and not HORA_INICIO is null 
                                               and IDordem=" + id_ordem.ToString());
                if ((new TimeSpan(dtInicio.Ticks - dtMaxFim.Ticks).TotalSeconds) < 0.0)
                {
                    MessageBox.Show("Favor escolher horário inicial maior que último horário fim!", "A V I S O",
                                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return true;
                }
            }

            if ((new TimeSpan(dtInicio.Ticks - dtFinal.Ticks).TotalSeconds) >= 0.0)
            {
                MessageBox.Show("Favor escolher horário fim maior que horário inicial!", "A V I S O",
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return true;
            }

            if ((new TimeSpan(dtFinal.Ticks - dtInicio.Ticks).TotalSeconds) + getSecondsUsed(dt) > MAXTEMPO_SEM_INTERVALO)
            {
                 int intervalo = 0;
                 if (cnt > 0)
                 {
                     DateTime lastFim = getLastHoraFim(dt);
                     if (lastFim.AddHours(1) > dtInicio)
                     {
                         MessageBox.Show("Favor cumpra intervalo depois de 4 horas de trabalho.", "A V I S O",
                                       MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                       return true;
                     }
                 }
                 else
                 {
                     MessageBox.Show("Favor cumpra intervalo depois de 4 horas de trabalho!", "A V I S O",
                                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                     return true;
                 }
            }

            
            return false;
        }
        
        private void btnAtividadeRealizada_Click(object sender, EventArgs e)
        {
            salvarOrdemServico();
            // verificar horários válidos
            // 08.04.2014 Uwe
            if (horarioInvalido())
            {               
                return;
            }            

            //salvarOrdemServico();

            DataRowView current = (DataRowView)osSituacoesOSBindingSource.AddNew();
          
            DateTime dtInicio = dtpHoraInicialRealizada.Value;
            DateTime dtFinal = dtpHoraFinalRealizada.Value;

            TimeSpan horaTotal = new TimeSpan(dtFinal.Ticks - dtInicio.Ticks);
           
            dtpHoraTotalRealizada.Text = horaTotal.ToString();

            current["IDORDEM"] = lblCodigoOS.Text;
            current["IDSITUACAO"] = cmbCodigoRealizada.Text;
            current["HORA_INICIO"] = dtpHoraInicialRealizada.Text;
            current["HORA_FIM"] = dtpHoraFinalRealizada.Text;
            current["HORA_TOTAL"] = dtpHoraTotalRealizada.Text;
            current["DATA_INICIAL"] = dATA_INICIALDateTimePicker.Text;

            DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;
                       
            if (estadoCorrente == EstadoManutencao.smInserir)
            {
                //currentOS["IDUSUARIO"] = OsConfiguracoes.codigoUsuario;
                currentOS["IDPOSTO"] = OsConfiguracoes.idPosto;
                currentOS["DT_CADASTRO"] = DateTime.Now;
            }
            osOrdemServicoBindingSource.EndEdit();
            osOrdemServicoTableAdapter.Update(dtsPrincipal);

            osSituacoesOSBindingSource.EndEdit();
            osSituacoesOSTableAdapter.Update(dtsPrincipal);

            this.osSituacoesOSTableAdapter.SituacaoRealizada(this.dtsPrincipal.OsSituacoesOS, (int)currentOS["ID"], "R");

            updateHorarioRealizado();
        }

        private void btnInserirSituacaoFinal_Click(object sender, EventArgs e)
        {
            salvarOrdemServico();

            DataRowView current = (DataRowView)osSituacoesOSBindingSource.AddNew();

            current["IDORDEM"] = lblCodigoOS.Text;
            current["IDSITUACAO"] = cmbCodigoSituacaoFinal.Text;

            DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;

            if (estadoCorrente == EstadoManutencao.smInserir)
            {
                //currentOS["IDUSUARIO"] = OsConfiguracoes.codigoUsuario;
                currentOS["IDPOSTO"] = OsConfiguracoes.idPosto;
                currentOS["DT_CADASTRO"] = DateTime.Now;
            }
            osOrdemServicoBindingSource.EndEdit();
            osOrdemServicoTableAdapter.Update(dtsPrincipal);

            osSituacoesOSBindingSource.EndEdit();
            osSituacoesOSTableAdapter.Update(dtsPrincipal);

            this.osSituacoesOSTableAdapter.SituacaoRealizada(this.dtsPrincipal.OsSituacoesOS, (int)currentOS["ID"], "F");
        }

        private void btnInserirLacres_Click(object sender, EventArgs e)
        {

            if (cmbNumSerieLacres.SelectedIndex == -1)
            {
                MessageBox.Show("Favor, informe o número de série da bomba.");
                return;
            }
            if (cmbNumInmetroLacres.SelectedIndex == -1)
            {
                MessageBox.Show("Favor, informe o número de INMETRO utilizado.");
                return;
            }

            if (cmbNumeroEtiquetaLacres.SelectedIndex == -1)
            {
                MessageBox.Show("Favor, informe o número do lacre utilizado.");
                return;
            }

            salvarOrdemServico();

            if (cmbCodigoEquipamentoLacres.Text.Length == 0)
            {
                MessageBox.Show("Informe o número de série da bomba.");
                return;
            }

            if (cmbCodigoINMETROLacres.Text.Length == 0)
            {
                MessageBox.Show("Informe o número de INMETRO utilizado.");
                return;
            }

            if (cmbCodigoLacres.Text.Length == 0)
            {
                MessageBox.Show("Informe o número do lacre utilizado.");
                return;
            }

           // DataRowView current = (DataRowView)osInstrumentoServicoBindingSource.AddNew();
            DataRowView current = (DataRowView)osInstrumentoServicoBindingSourceX.AddNew();
            if (lblCodigoOS.Text == "")
            {
                //lblCodigoOS.Text = lblNovoCodOS.Text;
                lblCodigoOS.Text = 
                    DB.GetSingleValue("select id from osordemservico where id_final="
                    + lblNovoCodOS.Text).ToString();
            }

            current["IDORDEM"] = lblCodigoOS.Text;

            current["IDIBAMETRO"] = cmbCodigoEquipamentoLacres.Text;
            current["IDINMETRO"] = cmbCodigoINMETROLacres.Text;
            current["IDETIQUETA"] = cmbCodigoLacres.Text;
            // uwe 25.02.2015
            current["MARCA_INICIAL"] = tbMarcaInicialLacre.Text;
            current["LACRE_ANTERIOR"] = tbLacreAnterior.Text;

            DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;

            if (estadoCorrente == EstadoManutencao.smInserir)
            {
                //currentOS["IDUSUARIO"] = OsConfiguracoes.codigoUsuario;
                currentOS["IDPOSTO"] = OsConfiguracoes.idPosto;
                currentOS["DT_CADASTRO"] = DateTime.Now;
            }
            osOrdemServicoBindingSource.EndEdit();
            osOrdemServicoTableAdapter.Update(dtsPrincipal);

            //osInstrumentoServicoBindingSource.EndEdit();
            //osInstrumentoServicoTableAdapter.Update(dtsPrincipal);
            //this.osInstrumentoServicoTableAdapter.SqlLacre(this.dtsPrincipal.OsInstrumentoServico, (int)currentOS["ID"]);
            osInstrumentoServicoBindingSourceX.EndEdit();
            osInstrumentoServicoOsClienteTableAdapter.Update(dtsPrincipal);
            this.osInstrumentoServicoOsClienteTableAdapter.SqlLacre(this.dtsPrincipal.OsInstrumentoServicoOsCliente, (int)currentOS["ID"]);
         
            limpaAbaLacres();
        }

        private void cmbCodigoEquipamentoSelos_SelectedIndexChanged(object sender, EventArgs e)
        {
            // BindingSource = bdsEquipamentoSelosX para
            // A) cmbCodigoEquipamentosSelos
            // B) cmbNumSerieSelos
            // AQUI:
            // A.Text           indica o valor NOVO (depois da seleção)
            // B.SelectedValue  indica o valor NOVO (depois da seleção)
            if (cmbNumSerieSelos.Text.Length > 0 && cmbCodigoEquipamentoSelos.Text != "")
            {
                //osIbametroInmetroTableAdapter.SqlFiltraINMETRO(this.dtsPrincipal.OsIbametroInmetro, Int32.Parse(cmbCodigoEquipamentoSelos.Text));
                osIbametroInmetroOsClienteTableAdapter.SqlFiltraINMETRO(this.dtsPrincipal.OsIbametroInmetroOsCliente, Int32.Parse(cmbCodigoEquipamentoSelos.Text));
            }
        }
        private void cmbNumSerieSelos_SelectionChangeCommitted(object sender, EventArgs e)
        {
            // BindingSource = bdsEquipamentoSelosX para
            // A) cmbCodigoEquipamentosSelos
            // B) cmbNumSerieSelos
            // AQUI:
            // A.Text           indica o valor VELHO (antes da seleção)
            // B.SelectedValue  indica o valor NOVO  (depois da seleção)
            if (cmbNumSerieSelos.SelectedValue != null)
            {
               // osIbametroInmetroTableAdapter.SqlFiltraINMETRO(this.dtsPrincipal.OsIbametroInmetro, Int32.Parse(cmbCodigoEquipamentoSelos.Text));
                osIbametroInmetroOsClienteTableAdapter.SqlFiltraINMETRO(this.dtsPrincipal.OsIbametroInmetroOsCliente, Int32.Parse(cmbCodigoEquipamentoSelos.Text));
            }
        }
        private void cmbNumSerieLacres_SelectionChangeCommitted(object sender, EventArgs e)
        {
            //if (cmbNumSerieLacres.Text.Length > 0)
            if (cmbNumSerieLacres.SelectedValue != null)
            {
               // osIbametroInmetroTableAdapter.SqlFiltraINMETRO(this.dtsPrincipal.OsIbametroInmetro, Int32.Parse(cmbCodigoEquipamentoLacres.Text));
                osIbametroInmetroOsClienteTableAdapter.SqlFiltraINMETRO(this.dtsPrincipal.OsIbametroInmetroOsCliente, Int32.Parse(cmbCodigoEquipamentoLacres.Text));
            }
        }
        private void cmbCodigoEquipamentoLacres_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            if (cmbNumSerieLacres.Text.Length > 0)
            {
                //osIbametroInmetroTableAdapter.SqlFiltraINMETRO(this.dtsPrincipal.OsIbametroInmetro, Int32.Parse(cmbCodigoEquipamentoLacres.Text));
                try
                {
                    // erro sabado 13.02.2021 Int32.Parse(cmbCodigoEquipamentoLacres.Text) == "" ??
                    osIbametroInmetroOsClienteTableAdapter.SqlFiltraINMETRO(this.dtsPrincipal.OsIbametroInmetroOsCliente, Int32.Parse(cmbCodigoEquipamentoLacres.Text));
                }
                catch (Exception ex) { }

            }
        }
        private void cmbNumSerieSelos_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
       
        private void cmbNomeCliente_Click(object sender, EventArgs e)
        {
            //bdsClientes.Sort = "NOME";
            /// 22.05.2014 - Uwe
            //bdsClientes.Sort = "LOGOTIPO";
        }

        private void cmbCodCliente_Click(object sender, EventArgs e)
        {
            bdsClientes.Sort = "CLiente";
        }

        
        

        /// <summary>
        /// 22.05.2014 - Uwe
        /// habilitar a communicação entre Textbox e Combobox
        /// </summary>       
        private void tbCodCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
           

            //Control ctrl = (Control)sender; 
            //if (e.KeyChar == (char)Keys.Return)
            //{
            //    if (tbCodCliente.Text.Equals(""))
            //    {
            //        this.SelectNextControl(ctrl, true, true, true, true);
            //        e.Handled = true;
            //        return;
            //    }
            //    int valido 
            //      = (int)DB.GetSingleValue("select count(*) from a30clientes where cliente='"+tbCodCliente.Text+"'");
            //    int valido_oscliente
            //     = (int)DB.GetSingleValue("select count(*) from osCliente where id='" + tbCodCliente.Text + "'");
            //    if (valido > 0 || valido_oscliente > 0)
            //        cmbCodCliente.SelectedIndex = cmbCodCliente.FindStringExact(tbCodCliente.Text);
            //    //cmbNomeCliente.SelectedIndex = cmbNomeCliente.FindStringExact(tbCodCliente.Text);
            //    else
            //    {
            //        MessageBox.Show("Não achei o cliente <"
            //                        + tbCodCliente.Text
            //                        + "> na tabela A30Clientes nem na tabela OsCliente.", "A V I S O",
            //                        MessageBoxButtons.OK, MessageBoxIcon.Information);
            //        //e.Handled = true;
            //        tbCodCliente.Text = "";
            //        this.SelectNextControl(ctrl, true, true, true, true);
            //        //cmbNomeCliente.Focus();
            //        //cmbNomeCliente.Select();
            //    }
            //}
        }

        private void cmbNomeCliente_SelectionChangeCommitted(object sender, EventArgs e)
        {
            //MessageBox.Show("asd");
            //var tab = (from System.Windows.Forms.TabPage tab in tabControl1.TabPages
            //           where tab.Name == "tabName"
            //           select tab).First();
            //((Control)this.tabPage).Enabled = false;
            foreach (TabPage tab in tabPrincipal.TabPages)
            {
                tab.Enabled = true;
                //Debug.WriteLine(tab.Name);
            }
        }

        private void tabPrincipal_Selecting(object sender, TabControlCancelEventArgs e)
        {
            // primeiro fornecer o Cliente
            if (cmbNomeCliente.Text.Length == 0 && estadoCorrente == EstadoManutencao.smInserir)
            {
                MessageBox.Show(@"Informe o Codigo de Cliente Antes de Graver a OS!", @"E R R O  tabPrincipal_Selecting");
                e.Cancel = true;
            }
        }

        /// <summary>
        /// Handles the UserDeletingRow event of the grdRealizada control.
        /// 
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DataGridViewRowCancelEventArgs"/> instance containing the event data.</param>
        private void grdRealizada_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            DialogResult response = MessageBox.Show(@"Certeza que quer deletar a linha?", 
                @"Deletar linha?", 
                MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if ((response == DialogResult.No))
            {
                e.Cancel = true;
            }

            DateTime hora_inicio = (DateTime)e.Row.Cells[2].Value;
            DateTime hora_fim = (DateTime)e.Row.Cells[3].Value;
            int idordem = Convert.ToInt32(lblCodigoOS.Text);

            string delActivity = @"delete from OsSituacoesOs where 1=1 ";
            delActivity += @" AND idordem=" + idordem;
            delActivity += @" AND hora_inicio='" + hora_inicio + "'";
            delActivity += @" AND hora_fim='" + hora_fim + "'";
            try
            {
                // atualizar as propostas horário 
                dtpHoraInicialRealizada.Value = hora_inicio;
                dtpHoraFinalRealizada.Value = hora_fim;
                //DB.ExecuteNonQuery(delActivity); // ConcurrencyError !!?

                //grdRealizada.DataSource = null;                               
                //grdRealizada.DataSource = dtsSelectSituacao;
                //dtsSelectSituacao.ResetBindings(false);
                //this.osSituacoesOSTableAdapter.SituacaoRealizada(idordem);

                //this.osSituacoesOSTableAdapter.Fill(this.dtsPrincipal.OsSituacoesOS);
                //osSituacoesOSTableAdapter.Adapter.A //Adapter.AcceptChangesDuringUpdate = 

                //osSituacoesOSBindingSource.ResetBindings(false);
                //osSituacoesOSTableAdapter.Delete(idordem);

            }
            catch (Exception abc)
            {
                MessageBox.Show(delActivity, @"Erro deletar atividade realizada",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Stop);
            }

        }

        private void tbFiltroLike_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                
                string filtro = "NOME LIKE '%" + tbFiltroLike.Text + "%' ";
                //string filtro = "LOGOTIPO LIKE '%" + tbFiltroLike.Text + "%' ";
                bs_cabecalho.Filter = filtro;
                //cmbNomeCliente.DataSource = null;
                //cmbNomeCliente.DataSource = bdsClientes;
            }
        }

        private void tb_kilometragem_KeyPress(object sender, KeyPressEventArgs e)
        {
            handleKeyPress(sender,e);
        }

        private void tb_media_KeyPress(object sender, KeyPressEventArgs e)
        {
            handleKeyPress(sender, e);
        }

        private void tb_preco_KeyPress(object sender, KeyPressEventArgs e)
        {
            handleKeyPress(sender, e);
        }

        private void handleKeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != ','))
            {
                e.Handled = true;
            }

            // only allow one comma (decimal point)
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') > -1))
            {
                e.Handled = true;
            }
        }

        private double calculaPrecoUnitario()
        {
            // System.Globalization.CultureInfo.InvariantCulture
            double kilo = Convert.ToDouble(tb_kilometragem.Text);
            double media = Convert.ToDouble(tb_media.Text);
            double preco = Convert.ToDouble(tb_preco.Text);
            //PREÇO UNITÁIRO = ((TOTAL DA KILOMETRAGEM / 8 ) * PREÇO DO COMUBUSTIVEL)/ TOTAL DA KILOMETRAGEM
            double result = ( (kilo/(media*1.0)) * preco) / (kilo*1.0);
            return result;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double preco_unitario = calculaPrecoUnitario();
            txtDeslocamentoKmVlr.Text = preco_unitario.ToString();
            txtDeslocamentoKmQtde.Text = tb_kilometragem.Text;
            txtDeslocamentoKmTotal.Text =
                (preco_unitario*
                 Convert.ToDouble(tb_kilometragem.Text)).ToString();
        }        
        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;
            updateCabecalho(Convert.ToInt32(currentOS["IDCLIENTE"].ToString()));
            abrirFecharOS();
            setSolicitante();
        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;
            updateCabecalho(Convert.ToInt32(currentOS["IDCLIENTE"].ToString()));
            abrirFecharOS();
            setSolicitante();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;
            updateCabecalho(Convert.ToInt32(currentOS["IDCLIENTE"].ToString()));
            abrirFecharOS();
            setSolicitante();
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;
            updateCabecalho(Convert.ToInt32(currentOS["IDCLIENTE"].ToString()));
            abrirFecharOS();
            setSolicitante();
            //currentOS["SOLICITANTE"] ==  DBNull.Value
        }

        private void cmbEmpresa_SelectionChangeCommitted(object sender, EventArgs e)
        {
            DataRowView currentEmp = (DataRowView)bs_empresa.Current;

            OsConfiguracoes.idPosto = (int)((DataRowView)cmbEmpresa.SelectedItem)["id"];

            OsConfiguracoes.nomePosto = ((DataRowView)cmbEmpresa.SelectedItem)["razao"].ToString();

            OsConfiguracoes.codigoPosto = ((DataRowView)cmbEmpresa.SelectedItem)["codigo"].ToString();
        }

        private void resetSolicitante() {
            chkSolicitante.Checked = true;
            cmbSolicitante.SelectedIndex = -1;
            cmbSolicitante.Enabled = false;
            try
            {
                ((DataRowView)osOrdemServicoBindingSource.Current)["SOLICITANTE"] = DBNull.Value;
            }
            catch (Exception ex) { }

        }

        private void chkSolicitante_CheckedChanged(object sender, EventArgs e)
        {
            // if "O mesmo" == false, queremos selecionar o Solicitante (quem paga)
            //cmbSolicitante.Enabled = false;
            if (((CheckBox)sender).Checked == false) 
            {
                cmbSolicitante.Enabled = true;
                // pra testar
                cmbSolicitante.SelectedIndex = -1;
                try
                {
                    ((DataRowView)osOrdemServicoBindingSource.Current)["SOLICITANTE"] = DBNull.Value;
                }
                catch (Exception ex) { }
            }
            if (((CheckBox)sender).Checked == true)
            {
                cmbSolicitante.Enabled = false;
                cmbSolicitante.SelectedIndex = -1;
                try
                {
                    ((DataRowView)osOrdemServicoBindingSource.Current)["SOLICITANTE"] = DBNull.Value;
                }
                catch (Exception ex) { }
            }

            //if(<yourCombobox>.SelectedIndex==-1)
            //   {
            //       CustomerBindingSource.SuspendBinding();
            //   }
            //   else
            //   {
            //       CustomerBindingSource.ResumeBinding();
            //   }
        }

        private void cmbSolicitante_SelectionChangeCommitted(object sender, EventArgs e)
        {
            DataRowView row = (DataRowView)bs_solicitante.Current;

            int id = (int)((DataRowView)cmbSolicitante.SelectedItem)["id"];
            string razao = ((DataRowView)cmbSolicitante.SelectedItem)["razao_social"].ToString();

            ((DataRowView)osOrdemServicoBindingSource.Current)["SOLICITANTE"] = id;
        }

        private void tbCodCliente_KeyDown(object sender, KeyEventArgs e)
        {

           // Control ctrl = (Control)sender;
            
           if (e.KeyCode == Keys.Enter)
            {
                if (tbCodCliente.Text.Equals(""))
                {
                    //this.SelectNextControl(ctrl, true, true, true, true);
                    //e.Handled = true;
                    //return;
                }
                //int valido
                //  = (int)DB.GetSingleValue("select count(*) from a30clientes where cliente='" + tbCodCliente.Text + "'");
                int valido = 0;
                int valido_oscliente
                 = (int)DB.GetSingleValue("select count(*) from osCliente where id='" + tbCodCliente.Text + "'");
                if (valido > 0 || valido_oscliente > 0)
                //cmbCodCliente.SelectedIndex = cmbCodCliente.FindStringExact(tbCodCliente.Text);
                ////cmbNomeCliente.SelectedIndex = cmbNomeCliente.FindStringExact(tbCodCliente.Text);
                {
                    //FindStringExact returns -1 if not found
                    //FindStringExact returns  0 if parameter is empty
                    var x = cmbCodCliente.FindStringExact(tbCodCliente.Text);

                    cmbCodCliente.SelectedIndex = x;
                }
                else
                {
                    MessageBox.Show("Não achei o cliente <"
                                    + tbCodCliente.Text
                                    + "> na tabela OsCliente.", "A V I S O",
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //e.Handled = true;
                    //tbCodCliente.Text = "";
                    //this.SelectNextControl(ctrl, true, true, true, true);
                    //cmbNomeCliente.Focus();
                    //cmbNomeCliente.Select();
                }
            }
        }
                                               
    }
}
