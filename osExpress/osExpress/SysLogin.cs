﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using TExpress;
using System.Configuration;

namespace osExpress
{
    public partial class SysLogin : Form
    {
        // Maio 2021
        public BindingSource bs_funcionario;

        public SysLogin()
        {
            InitializeComponent();
            DB.ConnectionName = "osExpress.Properties.Settings.OsExpressConnectionString";
        }

        private void btnLogar_Click(object sender, EventArgs e)
        {           
            try
            {
                if (logar() == true)
                {
                    Hide();
                    /// Obtém as configurações do usuário corrente.
                    //DataRowView currentUsr = (DataRowView) bdsLogin.Current;
                    DataRowView currentUsr = (DataRowView)bs_funcionario.Current;
                    DataRowView currentEmp = (DataRowView) bdsPosto.Current;

                    OsConfiguracoes.codigoUsuario = (int) currentUsr["ID"];
                    //OsConfiguracoes.codigoUsuario = (int)currentUsr["IDUSUARIO"];


                    OsConfiguracoes.nomeUsuario = (String) currentUsr["NOME"];

                    OsConfiguracoes.idPosto = (int) currentEmp["ID"];

                    OsConfiguracoes.nomePosto = (String) currentEmp["RAZAO"];

                    OsConfiguracoes.codigoPosto = (String) currentEmp["Codigo"];
                    

                    //OsConfiguracoes.tipoAcesso = (char) currentUsr["TIPO_ACESSO"];
                    //OsConfiguracoes.PlanoConta = (String) currentUsr["PLANO_CONTA"].ToString();
                    //OsConfiguracoes.acessaPedido = (Boolean) currentUsr["ACESSA_PEDIDO"];
                    OsConfiguracoes.digitaSeloELacre = (Boolean) currentUsr["DIGITA_SELO_LACRE"];
                    // maio 2021 XOSFuncionario.Tipo_Acesso = null ...
                    // faltam definições ...ainda precisamos plano_conta, acessa_pedido, digita_selo_lacre ..?
                    OsConfiguracoes.tipoAcesso = 'A';
                    OsConfiguracoes.acessaPedido = true;
                   
                }
                else
                {
                    lblInvalido.Visible = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace, ex.Message);
            }
        }

        /*
        public Boolean logar()
        {
            Boolean result = false;
          
            // Valida o usuário e a senha. 
            // Importante mesmo com logica nova !! (veja bdsLogin em cima)
            bdsLogin.Filter = "NOME = '"+txtUsuario.Text+"' AND SENHA = '"+txtSenha.Text+"'";           

            // logica nova
            string sql = @"select count(1) from osusuario where nome='" + txtUsuario.Text 
                           + @"' and senha='"+txtSenha.Text+@"'
                           and id in (select idusuario from osusuarioposto where idposto =  
                           (select id from a30postos where codigo='" + cmbPosto.Text + "'))";
            try
            {
                if (DB.ConnectionName.Equals(""))
                    DB.ConnectionName = "osExpress.Properties.Settings.OsExpressConnectionString";
                result = (int) DB.GetSingleValue(sql) > 0;
               
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message,@"ERRO",MessageBoxButtons.OK,MessageBoxIcon.Stop);
                return false;
            }
            return result;
        }
        */
        public Boolean logar()
        {
            Boolean result = false;

            // Valida o usuário e a senha. 
            // Importante mesmo com logica nova !! (veja bdsLogin em cima)
            bs_funcionario.Filter = "UNOME = '" + txtUsuario.Text + "' AND USENHA = '" + txtSenha.Text + "'";

            // logica nova
            /*string sql = @"select count(1) from osusuario where nome='" + txtUsuario.Text
                           + @"' and senha='" + txtSenha.Text + @"'
                           and id in (select idusuario from osusuarioposto where idposto =  
                           (select id from a30postos where codigo='" + cmbPosto.Text + "'))";
             */ 
            string sql = @"SELECT count(*) FROM XOSFuncionario where unome='" + txtUsuario.Text
                           + @"' and usenha='" + txtSenha.Text + @"' and isusuario='S'";
            try
            {
                //var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
               // var xx = Properties.Settings.Default["OsExpressConnectionString"];

                if (DB.ConnectionName.Equals(""))
                    DB.ConnectionName = "osExpress.Properties.Settings.OsExpressConnectionString";
                result = (int)DB.GetSingleValue(sql) > 0;

            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message, @"ERRO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
            return result;
        }

        private void SysLogin_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void SysLogin_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dtsLogin.A30POSTOS' table. You can move, or remove it, as needed.
            this.a30POSTOSTableAdapter.Fill(this.dtsLogin.A30POSTOS);
            // TODO: This line of code loads data into the 'dtsLogin.OsUsuario' table. You can move, or remove it, as needed.
            //this.osUsuarioTableAdapter.Fill(this.dtsLogin.OsUsuario);

            bs_funcionario = new BindingSource();
            bs_funcionario.DataSource = GetFuncionarios();           
            bs_funcionario.Sort = "nome";
        }
        /*
        private void ReadAllSettings()
        {
            try
            {
                var appSettings = ConfigurationManager.AppSettings;

                if (appSettings.Count == 0)
                {
                    //Console.WriteLine("AppSettings is empty.");
                    System.Diagnostics.Debug.WriteLine("AppSettings is empty.");
                }
                else
                {
                    foreach (var key in appSettings.AllKeys)
                    {
                        System.Diagnostics.Debug.WriteLine("Key: {0} Value: {1}", key, appSettings[key]);
                    }
                }
            }
            catch (ConfigurationErrorsException)
            {
                System.Diagnostics.Debug.WriteLine("Error reading app settings");
            }
        }  
        */
        private DataTable GetFuncionarios()
        {
           // ReadAllSettings();
           // var debug = 1;
            string sql = @"select 
                                  --Posto, Funcionario, 
                                  id,Nome, idusuario, Tipo_Pessoa, Cgc, RG, isusuario, uNome, uSenha,
                                  tipo_acesso, plano_conta, acessa_pedido, digita_selo_lacre
                                  --cep, estado, endereco, bairro, cidade,
                                  --obs01,obs02
                                  --Turno, Tipo_Pessoa, Cgc, Inscricao, Endereco, Bairro, Cidade, 
                                  --Estado, Cep, obs01, obs02, obs03, obs04, obs05, obs06, obs07, obs08, 
                                  --obs09, obs10, Noturno, ID, Inativo, Dt_Alter, Obs11, Obs12, Obs13, Obs14, 
                                  --Obs15, Obs16, Obs17, Obs18, Obs19, Obs20, RFID, Biometria, CTA_Caixa, Tipo_VIP,
                                  --uNome, uSenha, tipo_acesso, dt_cadastro, ultimo_acesso, plano_conta, acessa_pedido, digita_selo_lacre, 
                                  --idusuario, isusuario 
                                 FROM XOSFUNCIONARIO
                                     WHERE 1=1 and isusuario = 'S'";
           
            sql += @" ORDER BY nome";

            //var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            //var xx = Properties.Settings.Default["OsExpressConnectionString"];
            //var yy = ConfigurationManager.ConnectionStrings[1].ConnectionString;
            //var x = DB.ConnectionName;

            //var aa = ConfigurationManager.ConnectionStrings[1].ConnectionString;
            //var bb = ConfigurationManager.ConnectionStrings[x].ConnectionString;
            DataTable data = DB.GetTableFromSQL(sql);
            return data;
        }
        
        private void SysLogin_KeyDown(object sender, KeyEventArgs e)
        {
            // Se for pressionado a tecla ESC fecha a tela.
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }

            if (e.KeyCode == Keys.Enter)
            {
                btnLogar.PerformClick();
            }
        }

        private static string GetConnectionString()
        {
            //Data Source=192.168.25.254\sigtecno;Initial Catalog=OsExpress;Persist Security Info=True;User ID=sa;Password=*****
            return @"Data Source="+OsConfiguracoes.IpServidor+@"\"+OsConfiguracoes.Instancia
                   + @";Initial Catalog=OsExpress;Persist Security Info=True;User ID=sa;Password=" + OsConfiguracoes.Senha;
        }
        /// <summary>
        ///     Consulta que retorna um único valor.
        ///     Use "top 1" no sql entregue caso quiser o primeiro registro
        /// </summary>
        /// <param name="sql">SQL-Querystring</param>
        /// <returns>O objecto resultado.</returns>
        private static object GetSingleValue(string sql)
        {
            object retval = null;
            using (var sconn = new SqlConnection(GetConnectionString()))
            {
                using (var scmd = new SqlCommand(sql, sconn))
                {
                    sconn.Open();
                    SqlDataReader adar = scmd.ExecuteReader();
                    if (adar.HasRows)
                    {
                        while (adar.Read())
                        {
                            retval = adar[0];
                        }
                    }                   
                }
            }
            return retval;
        }

    }
}
