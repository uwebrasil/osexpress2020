﻿namespace osExpress
{
    partial class SysConfiguracao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.grbConfiguracao = new System.Windows.Forms.GroupBox();
            this.btnFechar = new System.Windows.Forms.Button();
            this.btnTestar = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtEndereco = new System.Windows.Forms.TextBox();
            this.txtUsuarioConexao = new System.Windows.Forms.TextBox();
            this.txtSenhaConexao = new System.Windows.Forms.TextBox();
            this.btnAplicar = new System.Windows.Forms.Button();
            this.stpStatus = new System.Windows.Forms.StatusStrip();
            this.tsbStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.bdsVersao = new System.Windows.Forms.BindingSource(this.components);
            this.osExpressDataSet = new osExpress.OsExpressDataSet();
            this.osVersaoTableAdapter = new osExpress.OsExpressDataSetTableAdapters.OsVersaoTableAdapter();
            this.grbConfiguracao.SuspendLayout();
            this.stpStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsVersao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.osExpressDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // grbConfiguracao
            // 
            this.grbConfiguracao.Controls.Add(this.btnFechar);
            this.grbConfiguracao.Controls.Add(this.btnTestar);
            this.grbConfiguracao.Controls.Add(this.label8);
            this.grbConfiguracao.Controls.Add(this.label5);
            this.grbConfiguracao.Controls.Add(this.label6);
            this.grbConfiguracao.Controls.Add(this.label7);
            this.grbConfiguracao.Controls.Add(this.txtEndereco);
            this.grbConfiguracao.Controls.Add(this.txtUsuarioConexao);
            this.grbConfiguracao.Controls.Add(this.txtSenhaConexao);
            this.grbConfiguracao.Controls.Add(this.btnAplicar);
            this.grbConfiguracao.Location = new System.Drawing.Point(12, 12);
            this.grbConfiguracao.Name = "grbConfiguracao";
            this.grbConfiguracao.Size = new System.Drawing.Size(323, 125);
            this.grbConfiguracao.TabIndex = 4;
            this.grbConfiguracao.TabStop = false;
            this.grbConfiguracao.Text = " Configuração de Conexão ";
            // 
            // btnFechar
            // 
            this.btnFechar.Location = new System.Drawing.Point(222, 97);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(48, 23);
            this.btnFechar.TabIndex = 21;
            this.btnFechar.Text = "&Fechar";
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // btnTestar
            // 
            this.btnTestar.Location = new System.Drawing.Point(168, 97);
            this.btnTestar.Name = "btnTestar";
            this.btnTestar.Size = new System.Drawing.Size(48, 23);
            this.btnTestar.TabIndex = 20;
            this.btnTestar.Text = "&Testar";
            this.btnTestar.UseVisualStyleBackColor = true;
            this.btnTestar.Click += new System.EventHandler(this.btnTestar_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(234, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "\\sigtecno";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Nome do Servidor:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(56, 48);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Usuário:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(61, 74);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Senha:";
            // 
            // txtEndereco
            // 
            this.txtEndereco.Location = new System.Drawing.Point(108, 19);
            this.txtEndereco.Name = "txtEndereco";
            this.txtEndereco.Size = new System.Drawing.Size(120, 20);
            this.txtEndereco.TabIndex = 10;
            // 
            // txtUsuarioConexao
            // 
            this.txtUsuarioConexao.Location = new System.Drawing.Point(108, 45);
            this.txtUsuarioConexao.Name = "txtUsuarioConexao";
            this.txtUsuarioConexao.Size = new System.Drawing.Size(162, 20);
            this.txtUsuarioConexao.TabIndex = 11;
            // 
            // txtSenhaConexao
            // 
            this.txtSenhaConexao.Location = new System.Drawing.Point(108, 71);
            this.txtSenhaConexao.Name = "txtSenhaConexao";
            this.txtSenhaConexao.PasswordChar = '*';
            this.txtSenhaConexao.Size = new System.Drawing.Size(162, 20);
            this.txtSenhaConexao.TabIndex = 12;
            // 
            // btnAplicar
            // 
            this.btnAplicar.Location = new System.Drawing.Point(107, 97);
            this.btnAplicar.Name = "btnAplicar";
            this.btnAplicar.Size = new System.Drawing.Size(54, 23);
            this.btnAplicar.TabIndex = 13;
            this.btnAplicar.Text = "&Aplicar";
            this.btnAplicar.UseVisualStyleBackColor = true;
            this.btnAplicar.Click += new System.EventHandler(this.btnAplicar_Click);
            // 
            // stpStatus
            // 
            this.stpStatus.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.stpStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbStatus});
            this.stpStatus.Location = new System.Drawing.Point(0, 147);
            this.stpStatus.Name = "stpStatus";
            this.stpStatus.Size = new System.Drawing.Size(353, 22);
            this.stpStatus.TabIndex = 5;
            this.stpStatus.Text = "statusStrip1";
            // 
            // tsbStatus
            // 
            this.tsbStatus.ForeColor = System.Drawing.Color.Red;
            this.tsbStatus.Name = "tsbStatus";
            this.tsbStatus.Size = new System.Drawing.Size(127, 17);
            this.tsbStatus.Text = "Status: SEM CONEXÃO";
            // 
            // bdsVersao
            // 
            this.bdsVersao.DataMember = "OsVersao";
            this.bdsVersao.DataSource = this.osExpressDataSet;
            // 
            // osExpressDataSet
            // 
            this.osExpressDataSet.DataSetName = "OsExpressDataSet";
            this.osExpressDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // osVersaoTableAdapter
            // 
            this.osVersaoTableAdapter.ClearBeforeFill = true;
            // 
            // SysConfiguracao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(353, 169);
            this.ControlBox = false;
            this.Controls.Add(this.stpStatus);
            this.Controls.Add(this.grbConfiguracao);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.KeyPreview = true;
            this.Name = "SysConfiguracao";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "osExpress (...)";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.SysConfiguracao_FormClosed);
            this.Load += new System.EventHandler(this.SysConfiguracao_Load);
            this.Shown += new System.EventHandler(this.SysConfiguracao_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SysConfiguracao_KeyDown);
            this.grbConfiguracao.ResumeLayout(false);
            this.grbConfiguracao.PerformLayout();
            this.stpStatus.ResumeLayout(false);
            this.stpStatus.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsVersao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.osExpressDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grbConfiguracao;
        private System.Windows.Forms.Button btnTestar;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtEndereco;
        private System.Windows.Forms.TextBox txtUsuarioConexao;
        private System.Windows.Forms.TextBox txtSenhaConexao;
        private System.Windows.Forms.Button btnAplicar;
        private System.Windows.Forms.StatusStrip stpStatus;
        private System.Windows.Forms.ToolStripStatusLabel tsbStatus;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.BindingSource bdsVersao;
        private OsExpressDataSet osExpressDataSet;
        private OsExpressDataSetTableAdapters.OsVersaoTableAdapter osVersaoTableAdapter;
        private System.Windows.Forms.Label label8;
    }
}