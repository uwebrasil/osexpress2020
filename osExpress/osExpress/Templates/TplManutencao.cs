﻿/*

 Apesar de ser uma boa prática a ideia de se criar um Framework 
 para o sistema e para outros que estão por vir, por conta das várias mudanças
 esse "Template" padrão não foi pra frente, algumas telas utilizam ele, 
 a alteração desse template pode impactar no funcionamento de outras telas.
 
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace osExpress
{
    public partial class FrmPrincipal : Form
    {
        
        // Verifica o tipo de ação a ser executada pelos cadastros.
        // ccCreate, ccUpdate referente ao CRUD OsClienteContatos
        // ceCreate, ceUpdate referente ao CRUD OsClienteEventos
        // cbCreate, cbUpdate referente ao CRUD IsIbametroOsCliente // Manutenção Bombas
        public enum EstadoManutencao
        {
            smInserir, smGravar, smExcluir, smEditar, smPesquisar
            ,ccCreate, ccUpdate, ceCreate, ceUpdate,
            cbCreate, cbUpdate
        }

        protected EstadoManutencao estadoCorrente;

        public FrmPrincipal()
        {
            InitializeComponent();
            pesquisar();
        }

        // Limpa todos os campos da tela do tipo TextBox (não utilizado).
        //private void limpaCampos() 
        //{
        //    foreach(Control ctl in this.Controls) 
        //    {
        //        if (ctl is TextBox)
        //            (ctl as TextBox).Text = "";
        //        if (ctl is DateTimePicker)
        //            (ctl as TextBox).Text = "";
        //    }
        //}
        protected void create()
        {
            tssStatus.Text = "Status: Adicionando Contato...";
            estadoCorrente = EstadoManutencao.ccCreate;
        }
        protected void update()
        {
            tssStatus.Text = "Status: Alterando Contato...";
            estadoCorrente = EstadoManutencao.ccUpdate;
        }

        protected void createEvento()
        {
            tssStatus.Text = "Status: Adicionando Evento...";
            estadoCorrente = EstadoManutencao.ceCreate;
        }
        protected void updateEvento()
        {
            tssStatus.Text = "Status: Alterando Evento...";
            estadoCorrente = EstadoManutencao.ceUpdate;
        }
        protected void createBomba()
        {
            tssStatus.Text = "Status: Adicionando Bomba...";
            estadoCorrente = EstadoManutencao.cbCreate;
        }
        protected void updateBomba()
        {
            tssStatus.Text = "Status: Alterando Bomba...";
            estadoCorrente = EstadoManutencao.cbUpdate;
        }
        protected void inserir()
        {
            tssStatus.Text = "Status: Inserindo...";
            estadoCorrente = EstadoManutencao.smInserir;
        }

        protected void gravar()
        {
            tssStatus.Text = "Status: Gravado com sucesso!";
            estadoCorrente = EstadoManutencao.smGravar;
        }

        protected void excluir()
        {
            tssStatus.Text = "Status: Excluído com sucesso, clique em gravar.";
            estadoCorrente = EstadoManutencao.smExcluir;
        }

        protected void editar()
        {
            tssStatus.Text = "Status: Editando... no final clique em gravar";
            estadoCorrente = EstadoManutencao.smEditar;
        }

        protected void pesquisar()
        {
            tssStatus.Text = @"Status: Consultando";
            estadoCorrente = EstadoManutencao.smPesquisar;
        }

        // Verifica se o estado em que se encontra a tela tem uma transação corrente acontecendo.
        private Boolean validaEstadoCorrente()
        {
            if (estadoCorrente == EstadoManutencao.smInserir || estadoCorrente == EstadoManutencao.smEditar || estadoCorrente == EstadoManutencao.smExcluir)
            {
                if (MessageBox.Show("Deseja abandonar sem gravar?",
                null,
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Warning) == DialogResult.Yes)
                    return true;
                else
                    return false;
            }
            else
                return true;

            // return false;
        }

        private void FrmPrincipal_KeyDown(object sender, KeyEventArgs e)
        {
            // Se for pressionado a tecla ESC fecha a tela.
            if (e.KeyCode == Keys.Escape)
            {
                if (validaEstadoCorrente())
                    this.Close();
            }
        }

        private void FrmPrincipal_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (validaEstadoCorrente())
                e.Cancel = false;
            else
                e.Cancel = true;
        }

    }
}
