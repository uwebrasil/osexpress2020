﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace osExpress
{

    public static class OsConfiguracoes
    {
        public static int codigoUsuario;
        public static String nomeUsuario;
        public static int idPosto;
        public static String codigoPosto;
        public static String nomePosto;
        public static Char tipoAcesso;
        public static int codigoFuncionario;
        public static String nomeFuncionario;
        public static string IpServidor;
        public static string Instancia;
        public static string Senha;
        public static string literalRecibo;
        public static string literalExtenso;
        public static String PlanoConta;
        public static Boolean acessaPedido;
        public static Boolean digitaSeloELacre;
    }

}
