﻿namespace osExpress.Manutencoes
{
    partial class ManEvento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ManEvento));
            this.OsClienteEventoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dtsClienteEvento = new osExpress.dtsClienteEvento();
            this.osClienteEventoTableAdapter = new osExpress.dtsClienteEventoTableAdapters.OsClienteEventoTableAdapter();
            this.ClienteEventoBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bnInserir = new System.Windows.Forms.ToolStripButton();
            this.bnCount = new System.Windows.Forms.ToolStripLabel();
            this.bnConsultar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.bnGravar = new System.Windows.Forms.ToolStripButton();
            this.bnCancelar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.bnPrimeiro = new System.Windows.Forms.ToolStripButton();
            this.bnAnterior = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bnPosition = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bnProximo = new System.Windows.Forms.ToolStripButton();
            this.bnUltimo = new System.Windows.Forms.ToolStripButton();
            this.bnDescartar = new System.Windows.Forms.ToolStripButton();
            this.osEventosDataGridView = new System.Windows.Forms.DataGridView();
            this.alertarDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datahorarealizacaoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.apontamentoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idclienteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panelEditar = new System.Windows.Forms.Panel();
            this.labelClienteCombobox = new System.Windows.Forms.Label();
            this.cmbCliente = new System.Windows.Forms.ComboBox();
            this.labelCliente = new System.Windows.Forms.Label();
            this.textboxCliente = new System.Windows.Forms.TextBox();
            this.labelApontamento = new System.Windows.Forms.Label();
            this.datetimeRealizacao = new System.Windows.Forms.DateTimePicker();
            this.checkboxAlertar = new System.Windows.Forms.CheckBox();
            this.textboxApontamento = new System.Windows.Forms.TextBox();
            this.labelDataEHora = new System.Windows.Forms.Label();
            this.labelEM = new System.Windows.Forms.Label();
            this.labelCadastrarEventos = new System.Windows.Forms.Label();
            this.labelCountEventos = new System.Windows.Forms.Label();
            this.buttonEventosTodos = new System.Windows.Forms.Button();
            this.buttonEventosConcluidos = new System.Windows.Forms.Button();
            this.buttonEventosPendentes = new System.Windows.Forms.Button();
            this.textboxFiltroCliente = new System.Windows.Forms.TextBox();
            this.labelFiltroCliente = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dtsPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OsClienteEventoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtsClienteEvento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClienteEventoBindingNavigator)).BeginInit();
            this.ClienteEventoBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.osEventosDataGridView)).BeginInit();
            this.panelEditar.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpPrincipal
            // 
            this.grpPrincipal.Location = new System.Drawing.Point(1200, 158);
            this.grpPrincipal.Margin = new System.Windows.Forms.Padding(2);
            this.grpPrincipal.Padding = new System.Windows.Forms.Padding(2);
            this.grpPrincipal.Size = new System.Drawing.Size(760, 23);
            this.grpPrincipal.Visible = false;
            // 
            // tabPrincipal
            // 
            this.tabPrincipal.Location = new System.Drawing.Point(12, 999);
            this.tabPrincipal.Margin = new System.Windows.Forms.Padding(2);
            this.tabPrincipal.Visible = false;
            // 
            // OsClienteEventoBindingSource
            // 
            this.OsClienteEventoBindingSource.DataMember = "OsClienteEvento";
            this.OsClienteEventoBindingSource.DataSource = this.dtsClienteEvento;
            // 
            // dtsClienteEvento
            // 
            this.dtsClienteEvento.DataSetName = "dtsClienteEvento";
            this.dtsClienteEvento.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // osClienteEventoTableAdapter
            // 
            this.osClienteEventoTableAdapter.ClearBeforeFill = true;
            // 
            // ClienteEventoBindingNavigator
            // 
            this.ClienteEventoBindingNavigator.AddNewItem = this.bnInserir;
            this.ClienteEventoBindingNavigator.BindingSource = this.OsClienteEventoBindingSource;
            this.ClienteEventoBindingNavigator.CountItem = this.bnCount;
            this.ClienteEventoBindingNavigator.CountItemFormat = "de {0}";
            this.ClienteEventoBindingNavigator.DeleteItem = null;
            this.ClienteEventoBindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.ClienteEventoBindingNavigator.ImageScalingSize = new System.Drawing.Size(48, 48);
            this.ClienteEventoBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bnConsultar,
            this.toolStripSeparator3,
            this.bnInserir,
            this.bnGravar,
            this.bnCancelar,
            this.toolStripSeparator4,
            this.bnPrimeiro,
            this.bnAnterior,
            this.toolStripSeparator1,
            this.bnPosition,
            this.bnCount,
            this.toolStripSeparator2,
            this.bnProximo,
            this.bnUltimo,
            this.bnDescartar});
            this.ClienteEventoBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.ClienteEventoBindingNavigator.MoveFirstItem = this.bnPrimeiro;
            this.ClienteEventoBindingNavigator.MoveLastItem = this.bnUltimo;
            this.ClienteEventoBindingNavigator.MoveNextItem = this.bnProximo;
            this.ClienteEventoBindingNavigator.MovePreviousItem = this.bnAnterior;
            this.ClienteEventoBindingNavigator.Name = "ClienteEventoBindingNavigator";
            this.ClienteEventoBindingNavigator.PositionItem = this.bnPosition;
            this.ClienteEventoBindingNavigator.Size = new System.Drawing.Size(959, 70);
            this.ClienteEventoBindingNavigator.TabIndex = 13;
            this.ClienteEventoBindingNavigator.Text = "ClienteEventoBindingNavigator";
            // 
            // bnInserir
            // 
            this.bnInserir.Image = global::osExpress.Properties.Resources.insert;
            this.bnInserir.Name = "bnInserir";
            this.bnInserir.RightToLeftAutoMirrorImage = true;
            this.bnInserir.Size = new System.Drawing.Size(52, 67);
            this.bnInserir.Text = "&Inserir";
            this.bnInserir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnInserir.Click += new System.EventHandler(this.bnInserir_Click);
            // 
            // bnCount
            // 
            this.bnCount.Name = "bnCount";
            this.bnCount.Size = new System.Drawing.Size(37, 67);
            this.bnCount.Text = "de {0}";
            this.bnCount.ToolTipText = "Total number of items";
            // 
            // bnConsultar
            // 
            this.bnConsultar.Image = global::osExpress.Properties.Resources.search;
            this.bnConsultar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnConsultar.Name = "bnConsultar";
            this.bnConsultar.Size = new System.Drawing.Size(62, 67);
            this.bnConsultar.Text = "Consultar";
            this.bnConsultar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnConsultar.Click += new System.EventHandler(this.bnConsultar_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 70);
            // 
            // bnGravar
            // 
            this.bnGravar.Image = global::osExpress.Properties.Resources.save;
            this.bnGravar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnGravar.Name = "bnGravar";
            this.bnGravar.Size = new System.Drawing.Size(52, 67);
            this.bnGravar.Text = "Grav&ar";
            this.bnGravar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnGravar.Click += new System.EventHandler(this.bnGravar_Click);
            // 
            // bnCancelar
            // 
            this.bnCancelar.Enabled = false;
            this.bnCancelar.Image = global::osExpress.Properties.Resources.clean;
            this.bnCancelar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnCancelar.Name = "bnCancelar";
            this.bnCancelar.Size = new System.Drawing.Size(57, 67);
            this.bnCancelar.Text = "Cancelar";
            this.bnCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnCancelar.Click += new System.EventHandler(this.bnCancelar_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 70);
            // 
            // bnPrimeiro
            // 
            this.bnPrimeiro.Image = global::osExpress.Properties.Resources.first;
            this.bnPrimeiro.Name = "bnPrimeiro";
            this.bnPrimeiro.RightToLeftAutoMirrorImage = true;
            this.bnPrimeiro.Size = new System.Drawing.Size(56, 67);
            this.bnPrimeiro.Text = "&Primeiro";
            this.bnPrimeiro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // bnAnterior
            // 
            this.bnAnterior.Image = global::osExpress.Properties.Resources.previous;
            this.bnAnterior.Name = "bnAnterior";
            this.bnAnterior.RightToLeftAutoMirrorImage = true;
            this.bnAnterior.Size = new System.Drawing.Size(54, 67);
            this.bnAnterior.Text = "Anteri&or";
            this.bnAnterior.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnAnterior.Click += new System.EventHandler(this.bnAnterior_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 70);
            // 
            // bnPosition
            // 
            this.bnPosition.AccessibleName = "Position";
            this.bnPosition.AutoSize = false;
            this.bnPosition.Name = "bnPosition";
            this.bnPosition.Size = new System.Drawing.Size(50, 23);
            this.bnPosition.Text = "0";
            this.bnPosition.ToolTipText = "Current position";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 70);
            // 
            // bnProximo
            // 
            this.bnProximo.Image = global::osExpress.Properties.Resources.next;
            this.bnProximo.Name = "bnProximo";
            this.bnProximo.RightToLeftAutoMirrorImage = true;
            this.bnProximo.Size = new System.Drawing.Size(56, 67);
            this.bnProximo.Text = "Próxi&mo";
            this.bnProximo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnProximo.Click += new System.EventHandler(this.bnProximo_Click);
            // 
            // bnUltimo
            // 
            this.bnUltimo.Image = global::osExpress.Properties.Resources.last;
            this.bnUltimo.Name = "bnUltimo";
            this.bnUltimo.RightToLeftAutoMirrorImage = true;
            this.bnUltimo.Size = new System.Drawing.Size(52, 67);
            this.bnUltimo.Text = "&Último";
            this.bnUltimo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // bnDescartar
            // 
            this.bnDescartar.Enabled = false;
            this.bnDescartar.Image = global::osExpress.Properties.Resources.remove;
            this.bnDescartar.Name = "bnDescartar";
            this.bnDescartar.RightToLeftAutoMirrorImage = true;
            this.bnDescartar.Size = new System.Drawing.Size(60, 67);
            this.bnDescartar.Text = "&Descartar";
            this.bnDescartar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnDescartar.Click += new System.EventHandler(this.bnDescartar_Click);
            // 
            // osEventosDataGridView
            // 
            this.osEventosDataGridView.AllowUserToAddRows = false;
            this.osEventosDataGridView.AllowUserToDeleteRows = false;
            this.osEventosDataGridView.AllowUserToOrderColumns = true;
            this.osEventosDataGridView.AutoGenerateColumns = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.osEventosDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.osEventosDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.osEventosDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.alertarDataGridViewTextBoxColumn,
            this.datahorarealizacaoDataGridViewTextBoxColumn,
            this.apontamentoDataGridViewTextBoxColumn,
            this.idclienteDataGridViewTextBoxColumn});
            this.osEventosDataGridView.DataSource = this.OsClienteEventoBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.osEventosDataGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.osEventosDataGridView.Location = new System.Drawing.Point(100, 203);
            this.osEventosDataGridView.Name = "osEventosDataGridView";
            this.osEventosDataGridView.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.osEventosDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.osEventosDataGridView.Size = new System.Drawing.Size(760, 325);
            this.osEventosDataGridView.TabIndex = 16;
            this.osEventosDataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.osEventosDataGridView_CellDoubleClick);
            this.osEventosDataGridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.osEventosDataGridView_KeyDown);
            // 
            // alertarDataGridViewTextBoxColumn
            // 
            this.alertarDataGridViewTextBoxColumn.DataPropertyName = "alertar";
            this.alertarDataGridViewTextBoxColumn.FillWeight = 50F;
            this.alertarDataGridViewTextBoxColumn.HeaderText = "alertar";
            this.alertarDataGridViewTextBoxColumn.Name = "alertarDataGridViewTextBoxColumn";
            this.alertarDataGridViewTextBoxColumn.ReadOnly = true;
            this.alertarDataGridViewTextBoxColumn.Width = 56;
            // 
            // datahorarealizacaoDataGridViewTextBoxColumn
            // 
            this.datahorarealizacaoDataGridViewTextBoxColumn.DataPropertyName = "datahora_realizacao";
            this.datahorarealizacaoDataGridViewTextBoxColumn.FillWeight = 150F;
            this.datahorarealizacaoDataGridViewTextBoxColumn.HeaderText = "realização";
            this.datahorarealizacaoDataGridViewTextBoxColumn.Name = "datahorarealizacaoDataGridViewTextBoxColumn";
            this.datahorarealizacaoDataGridViewTextBoxColumn.ReadOnly = true;
            this.datahorarealizacaoDataGridViewTextBoxColumn.Width = 167;
            // 
            // apontamentoDataGridViewTextBoxColumn
            // 
            this.apontamentoDataGridViewTextBoxColumn.DataPropertyName = "apontamento";
            this.apontamentoDataGridViewTextBoxColumn.FillWeight = 200F;
            this.apontamentoDataGridViewTextBoxColumn.HeaderText = "apontamento";
            this.apontamentoDataGridViewTextBoxColumn.Name = "apontamentoDataGridViewTextBoxColumn";
            this.apontamentoDataGridViewTextBoxColumn.ReadOnly = true;
            this.apontamentoDataGridViewTextBoxColumn.Width = 223;
            // 
            // idclienteDataGridViewTextBoxColumn
            // 
            this.idclienteDataGridViewTextBoxColumn.DataPropertyName = "id_cliente";
            this.idclienteDataGridViewTextBoxColumn.HeaderText = "id_cliente";
            this.idclienteDataGridViewTextBoxColumn.Name = "idclienteDataGridViewTextBoxColumn";
            this.idclienteDataGridViewTextBoxColumn.ReadOnly = true;
            this.idclienteDataGridViewTextBoxColumn.Width = 111;
            // 
            // panelEditar
            // 
            this.panelEditar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelEditar.Controls.Add(this.labelClienteCombobox);
            this.panelEditar.Controls.Add(this.cmbCliente);
            this.panelEditar.Controls.Add(this.labelCliente);
            this.panelEditar.Controls.Add(this.textboxCliente);
            this.panelEditar.Controls.Add(this.labelApontamento);
            this.panelEditar.Controls.Add(this.datetimeRealizacao);
            this.panelEditar.Controls.Add(this.checkboxAlertar);
            this.panelEditar.Controls.Add(this.textboxApontamento);
            this.panelEditar.Controls.Add(this.labelDataEHora);
            this.panelEditar.Controls.Add(this.labelEM);
            this.panelEditar.Location = new System.Drawing.Point(150, 250);
            this.panelEditar.Margin = new System.Windows.Forms.Padding(2);
            this.panelEditar.Name = "panelEditar";
            this.panelEditar.Size = new System.Drawing.Size(600, 325);
            this.panelEditar.TabIndex = 17;
            this.panelEditar.Visible = false;
            // 
            // labelClienteCombobox
            // 
            this.labelClienteCombobox.AutoSize = true;
            this.labelClienteCombobox.Location = new System.Drawing.Point(306, 19);
            this.labelClienteCombobox.Name = "labelClienteCombobox";
            this.labelClienteCombobox.Size = new System.Drawing.Size(39, 13);
            this.labelClienteCombobox.TabIndex = 13;
            this.labelClienteCombobox.Text = "Cliente";
            // 
            // cmbCliente
            // 
            this.cmbCliente.FormattingEnabled = true;
            this.cmbCliente.Location = new System.Drawing.Point(298, 39);
            this.cmbCliente.Name = "cmbCliente";
            this.cmbCliente.Size = new System.Drawing.Size(251, 21);
            this.cmbCliente.TabIndex = 12;
            this.cmbCliente.Visible = false;
            this.cmbCliente.SelectionChangeCommitted += new System.EventHandler(this.cmbCliente_SelectionChangeCommitted);
            // 
            // labelCliente
            // 
            this.labelCliente.AutoSize = true;
            this.labelCliente.Location = new System.Drawing.Point(124, 72);
            this.labelCliente.Name = "labelCliente";
            this.labelCliente.Size = new System.Drawing.Size(39, 13);
            this.labelCliente.TabIndex = 11;
            this.labelCliente.Text = "Cliente";
            // 
            // textboxCliente
            // 
            this.textboxCliente.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.OsClienteEventoBindingSource, "id_cliente", true));
            this.textboxCliente.Enabled = false;
            this.textboxCliente.Location = new System.Drawing.Point(112, 89);
            this.textboxCliente.Margin = new System.Windows.Forms.Padding(2);
            this.textboxCliente.Name = "textboxCliente";
            this.textboxCliente.Size = new System.Drawing.Size(437, 20);
            this.textboxCliente.TabIndex = 10;
            // 
            // labelApontamento
            // 
            this.labelApontamento.AutoSize = true;
            this.labelApontamento.Location = new System.Drawing.Point(61, 117);
            this.labelApontamento.Name = "labelApontamento";
            this.labelApontamento.Size = new System.Drawing.Size(70, 13);
            this.labelApontamento.TabIndex = 9;
            this.labelApontamento.Text = "Apontamento";
            // 
            // datetimeRealizacao
            // 
            this.datetimeRealizacao.CustomFormat = "dd/MM/yyyy HH:mm";
            this.datetimeRealizacao.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.OsClienteEventoBindingSource, "datahora_realizacao", true));
            this.datetimeRealizacao.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datetimeRealizacao.Location = new System.Drawing.Point(58, 39);
            this.datetimeRealizacao.Name = "datetimeRealizacao";
            this.datetimeRealizacao.Size = new System.Drawing.Size(220, 20);
            this.datetimeRealizacao.TabIndex = 5;
            this.datetimeRealizacao.ValueChanged += new System.EventHandler(this.datetimeRealizacao_ValueChanged);
            // 
            // checkboxAlertar
            // 
            this.checkboxAlertar.AutoSize = true;
            this.checkboxAlertar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkboxAlertar.Location = new System.Drawing.Point(28, 79);
            this.checkboxAlertar.Name = "checkboxAlertar";
            this.checkboxAlertar.Size = new System.Drawing.Size(74, 17);
            this.checkboxAlertar.TabIndex = 8;
            this.checkboxAlertar.Text = "Alertar ?";
            this.checkboxAlertar.UseVisualStyleBackColor = true;
            // 
            // textboxApontamento
            // 
            this.textboxApontamento.AcceptsReturn = true;
            this.textboxApontamento.AcceptsTab = true;
            this.textboxApontamento.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.OsClienteEventoBindingSource, "apontamento", true));
            this.textboxApontamento.Location = new System.Drawing.Point(50, 137);
            this.textboxApontamento.Multiline = true;
            this.textboxApontamento.Name = "textboxApontamento";
            this.textboxApontamento.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textboxApontamento.Size = new System.Drawing.Size(500, 139);
            this.textboxApontamento.TabIndex = 7;
            // 
            // labelDataEHora
            // 
            this.labelDataEHora.AutoSize = true;
            this.labelDataEHora.Location = new System.Drawing.Point(68, 19);
            this.labelDataEHora.Name = "labelDataEHora";
            this.labelDataEHora.Size = new System.Drawing.Size(65, 13);
            this.labelDataEHora.TabIndex = 6;
            this.labelDataEHora.Text = "Data e Hora";
            // 
            // labelEM
            // 
            this.labelEM.AutoSize = true;
            this.labelEM.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEM.Location = new System.Drawing.Point(23, 44);
            this.labelEM.Name = "labelEM";
            this.labelEM.Size = new System.Drawing.Size(29, 13);
            this.labelEM.TabIndex = 4;
            this.labelEM.Text = "EM:";
            // 
            // labelCadastrarEventos
            // 
            this.labelCadastrarEventos.AutoSize = true;
            this.labelCadastrarEventos.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCadastrarEventos.Location = new System.Drawing.Point(324, 104);
            this.labelCadastrarEventos.Name = "labelCadastrarEventos";
            this.labelCadastrarEventos.Size = new System.Drawing.Size(287, 31);
            this.labelCadastrarEventos.TabIndex = 18;
            this.labelCadastrarEventos.Text = "Manutenção Eventos";
            // 
            // labelCountEventos
            // 
            this.labelCountEventos.AutoSize = true;
            this.labelCountEventos.Location = new System.Drawing.Point(257, 235);
            this.labelCountEventos.Name = "labelCountEventos";
            this.labelCountEventos.Size = new System.Drawing.Size(109, 13);
            this.labelCountEventos.TabIndex = 19;
            this.labelCountEventos.Text = "Número dos Eventos ";
            // 
            // buttonEventosTodos
            // 
            this.buttonEventosTodos.Location = new System.Drawing.Point(123, 162);
            this.buttonEventosTodos.Margin = new System.Windows.Forms.Padding(2);
            this.buttonEventosTodos.Name = "buttonEventosTodos";
            this.buttonEventosTodos.Size = new System.Drawing.Size(150, 25);
            this.buttonEventosTodos.TabIndex = 20;
            this.buttonEventosTodos.Text = "Todos Eventos";
            this.buttonEventosTodos.UseVisualStyleBackColor = true;
            this.buttonEventosTodos.Click += new System.EventHandler(this.buttonEventosTodos_Click);
            // 
            // buttonEventosConcluidos
            // 
            this.buttonEventosConcluidos.Location = new System.Drawing.Point(288, 162);
            this.buttonEventosConcluidos.Margin = new System.Windows.Forms.Padding(2);
            this.buttonEventosConcluidos.Name = "buttonEventosConcluidos";
            this.buttonEventosConcluidos.Size = new System.Drawing.Size(150, 25);
            this.buttonEventosConcluidos.TabIndex = 21;
            this.buttonEventosConcluidos.Text = "Eventos concluidos";
            this.buttonEventosConcluidos.UseVisualStyleBackColor = true;
            this.buttonEventosConcluidos.Click += new System.EventHandler(this.buttonEventosConcluidos_Click);
            // 
            // buttonEventosPendentes
            // 
            this.buttonEventosPendentes.Location = new System.Drawing.Point(461, 162);
            this.buttonEventosPendentes.Margin = new System.Windows.Forms.Padding(2);
            this.buttonEventosPendentes.Name = "buttonEventosPendentes";
            this.buttonEventosPendentes.Size = new System.Drawing.Size(150, 25);
            this.buttonEventosPendentes.TabIndex = 22;
            this.buttonEventosPendentes.Text = "Eventos pendentes";
            this.buttonEventosPendentes.UseVisualStyleBackColor = true;
            this.buttonEventosPendentes.Click += new System.EventHandler(this.buttonEventosPendentes_Click);
            // 
            // textboxFiltroCliente
            // 
            this.textboxFiltroCliente.Location = new System.Drawing.Point(641, 166);
            this.textboxFiltroCliente.Name = "textboxFiltroCliente";
            this.textboxFiltroCliente.Size = new System.Drawing.Size(200, 20);
            this.textboxFiltroCliente.TabIndex = 23;
            // 
            // labelFiltroCliente
            // 
            this.labelFiltroCliente.AutoSize = true;
            this.labelFiltroCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFiltroCliente.Location = new System.Drawing.Point(645, 153);
            this.labelFiltroCliente.Name = "labelFiltroCliente";
            this.labelFiltroCliente.Size = new System.Drawing.Size(71, 12);
            this.labelFiltroCliente.TabIndex = 24;
            this.labelFiltroCliente.Text = "%FiltroCliente%";
            // 
            // ManEvento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(959, 711);
            this.Controls.Add(this.labelFiltroCliente);
            this.Controls.Add(this.textboxFiltroCliente);
            this.Controls.Add(this.buttonEventosPendentes);
            this.Controls.Add(this.buttonEventosConcluidos);
            this.Controls.Add(this.buttonEventosTodos);
            this.Controls.Add(this.labelCountEventos);
            this.Controls.Add(this.labelCadastrarEventos);
            this.Controls.Add(this.panelEditar);
            this.Controls.Add(this.ClienteEventoBindingNavigator);
            this.Controls.Add(this.osEventosDataGridView);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = true;
            this.MinimizeBox = true;
            this.Name = "ManEvento";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "  Manutenção de Eventos";
            this.Load += new System.EventHandler(this.ManEvento_Load);
            this.Controls.SetChildIndex(this.grpPrincipal, 0);
            this.Controls.SetChildIndex(this.tabPrincipal, 0);
            this.Controls.SetChildIndex(this.osEventosDataGridView, 0);
            this.Controls.SetChildIndex(this.ClienteEventoBindingNavigator, 0);
            this.Controls.SetChildIndex(this.panelEditar, 0);
            this.Controls.SetChildIndex(this.labelCadastrarEventos, 0);
            this.Controls.SetChildIndex(this.labelCountEventos, 0);
            this.Controls.SetChildIndex(this.buttonEventosTodos, 0);
            this.Controls.SetChildIndex(this.buttonEventosConcluidos, 0);
            this.Controls.SetChildIndex(this.buttonEventosPendentes, 0);
            this.Controls.SetChildIndex(this.textboxFiltroCliente, 0);
            this.Controls.SetChildIndex(this.labelFiltroCliente, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dtsPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OsClienteEventoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtsClienteEvento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClienteEventoBindingNavigator)).EndInit();
            this.ClienteEventoBindingNavigator.ResumeLayout(false);
            this.ClienteEventoBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.osEventosDataGridView)).EndInit();
            this.panelEditar.ResumeLayout(false);
            this.panelEditar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource OsClienteEventoBindingSource;
        private dtsClienteEvento dtsClienteEvento;
        private dtsClienteEventoTableAdapters.OsClienteEventoTableAdapter osClienteEventoTableAdapter;
        private System.Windows.Forms.BindingNavigator ClienteEventoBindingNavigator;
        private System.Windows.Forms.ToolStripButton bnInserir;
        private System.Windows.Forms.ToolStripLabel bnCount;
        private System.Windows.Forms.ToolStripButton bnConsultar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton bnGravar;
        private System.Windows.Forms.ToolStripButton bnCancelar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton bnPrimeiro;
        private System.Windows.Forms.ToolStripButton bnAnterior;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripTextBox bnPosition;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton bnProximo;
        private System.Windows.Forms.ToolStripButton bnUltimo;
        private System.Windows.Forms.ToolStripButton bnDescartar;
        private System.Windows.Forms.DataGridView osEventosDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn alertarDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn datahorarealizacaoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn apontamentoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idclienteDataGridViewTextBoxColumn;
        private System.Windows.Forms.Panel panelEditar;
        private System.Windows.Forms.DateTimePicker datetimeRealizacao;
        private System.Windows.Forms.CheckBox checkboxAlertar;
        private System.Windows.Forms.TextBox textboxApontamento;
        private System.Windows.Forms.Label labelDataEHora;
        private System.Windows.Forms.Label labelEM;
        private System.Windows.Forms.TextBox textboxCliente;
        private System.Windows.Forms.Label labelApontamento;
        private System.Windows.Forms.Label labelCliente;
        private System.Windows.Forms.Label labelClienteCombobox;
        private System.Windows.Forms.ComboBox cmbCliente;
        private System.Windows.Forms.Label labelCadastrarEventos;
        private System.Windows.Forms.Label labelCountEventos;
        private System.Windows.Forms.Button buttonEventosTodos;
        private System.Windows.Forms.Button buttonEventosConcluidos;
        private System.Windows.Forms.Button buttonEventosPendentes;
        private System.Windows.Forms.TextBox textboxFiltroCliente;
        private System.Windows.Forms.Label labelFiltroCliente;
    }
}