﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using CrystalDecisions.Shared;
using System.Data.SqlClient;
using System.Configuration;
using TExpress;
using System.Collections.Generic;

namespace osExpress.Manutencoes
{
    public partial class ManOrdemServico : osExpress.FrmPrincipal
    {
        // tempo maximal que o funcionário pode trabalhar
        // sem intervalo de 1 hora.
        private int MAXTEMPO_SEM_INTERVALO = 3600 * 4;

        private RelOrdem relOrdemServico;
        EnviaEmail enviaEmail;
        string corpoOS;

        public ManOrdemServico()
        {
            InitializeComponent();
            pnlConsultar.Visible = true;
            // 29.05.2014 by Uwe Kristmann
            // Test TeamExplorer 2
            tsbGerarCaixa.Enabled = false;
            DB.ConnectionName = "osExpress.Properties.Settings.OsExpressConnectionString";

            //bs.Filter = string.Format("Name LIKE '%{0}%'", textBox1.Text);
            //uwex
            //bdsFiltroCliente.Filter = string.Format("Posto = '{0}'", OsConfiguracoes.codigoPosto);
            //bdsClientes.Filter = string.Format("Posto = '{0}'", OsConfiguracoes.codigoPosto);
            //bdsFiltroUsuario
        }


        private void ManOrdemServico_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dtsPrincipal.OsParametros' table. You can move, or remove it, as needed.
            this.osParametrosTableAdapter.Fill(this.dtsPrincipal.OsParametros);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsIbametroInmetro' table. You can move, or remove it, as needed.
            this.osIbametroInmetroTableAdapter.Fill(this.dtsPrincipal.OsIbametroInmetro);
            this.WindowState = FormWindowState.Maximized;
            //
            this.SuspendLayout();
            //
            // TODO: This line of code loads data into the 'dtsPrincipal.OsUsuario' table. You can move, or remove it, as needed.
            this.osUsuarioTableAdapter.Fill(this.dtsPrincipal.OsUsuario);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsRecibo' table. You can move, or remove it, as needed.
            this.osReciboTableAdapter.Fill(this.dtsPrincipal.OsRecibo);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsRecibo' table. You can move, or remove it, as needed.
            this.osReciboTableAdapter.Fill(this.dtsPrincipal.OsRecibo);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsParContas' table. You can move, or remove it, as needed.
            this.osParContasTableAdapter.Fill(this.dtsPrincipal.OsParContas);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsFormasPagamento' table. You can move, or remove it, as needed.
            this.osFormasPagamentoTableAdapter.Fill(this.dtsPrincipal.OsFormasPagamento);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsInstrumentoServico' table. You can move, or remove it, as needed.
            this.osInstrumentoServicoTableAdapter.Fill(this.dtsPrincipal.OsInstrumentoServico);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsIbametro' table. You can move, or remove it, as needed.
            this.osIbametroTableAdapter.Fill(this.dtsPrincipal.OsIbametro);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsEtiqueta' table. You can move, or remove it, as needed.
            this.osEtiquetaTableAdapter.Fill(this.dtsPrincipal.OsEtiqueta);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsItemServico' table. You can move, or remove it, as needed.
            this.osItemServicoTableAdapter.Fill(this.dtsPrincipal.OsItemServico);
            // TODO: This line of code loads data into the 'dtsPrincipal.A30PRODUTO' table. You can move, or remove it, as needed.
            this.a30PRODUTOTableAdapter.Fill(this.dtsPrincipal.A30PRODUTO);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsSituacoesOS' table. You can move, or remove it, as needed.
            this.osSituacoesOSTableAdapter.Fill(this.dtsPrincipal.OsSituacoesOS);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsParSituacao' table. You can move, or remove it, as needed.
            this.osParSituacaoTableAdapter.Fill(this.dtsPrincipal.OsParSituacao);
            // TODO: This line of code loads data into the 'dtsPrincipal.A30CLIENTES' table. You can move, or remove it, as needed.
            this.a30CLIENTESTableAdapter.Fill(this.dtsPrincipal.A30CLIENTES);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsOrdemServico' table. You can move, or remove it, as needed.
            //
            this.osOrdemServicoTableAdapter.SqlOrdensServico(this.dtsPrincipal.OsOrdemServico);
            //
            this.ResumeLayout();

            estadoCorrente = EstadoManutencao.smPesquisar;
            cmbFiltroSituacao.SelectedIndex = 0;

            if (OsConfiguracoes.tipoAcesso == 'A')
            {
                osOrdemServicoBindingSource.Filter = "ID_USR_OFFLINE = " + "'" + OsConfiguracoes.codigoUsuario + "' AND STATUS_OS = 'A' AND TIPO_DOC = 'S' ";
            }
            else
            {
                osOrdemServicoBindingSource.Filter = "ID_USR_OFFLINE = " + "'" + OsConfiguracoes.codigoUsuario + "' AND STATUS_OS = 'A' AND TIPO_DOC = 'S' ";
                cmbFiltroUsuarioCodigo.Enabled = false;
                cmbFiltroUsuarioNome.Enabled = false;
                chkFiltrarUsuario.Enabled = false;
            }

            tsbCancelar.Enabled = false;
            tsbInserir.Enabled = true;
            lblTotalOS.Text = "Total: " + osOrdemServicoBindingSource.Count.ToString();


            //
            enviaEmail = new EnviaEmail();

            DataRowView template = (DataRowView)osParametrosBindingSource.Current;
            corpoOS = template["EMAIL_ORDEM"].ToString();

            tbCodCliente.Text = "";
        }


        private void tsbInserir_Click(object sender, EventArgs e)
        {
            int existeOsAberta = Int32.Parse((string)osOrdemServicoTableAdapter.SqlExisteOsAberta(OsConfiguracoes.codigoUsuario, OsConfiguracoes.idPosto));

            if (existeOsAberta > 0)
            {
                MessageBox.Show("Não é possível criar uma nova ordem de serviço sem fechar a(s) anterior(es): " + existeOsAberta);
                osOrdemServicoBindingSource.CancelEdit();
                return;
            }

            lblCabCliente.Text = "";
            lblCabIE.Text = "";
            lblCabNCPJ.Text = "";
            lblCabTelefone.Text = "";
            lblCabObservacao.Text = "";
            lblCabSituacao.Text = "";

            tsbCancelar.Enabled = true;
            tsbInserir.Enabled = false;

            pnlConsultar.Visible = false;

            inserir();
            //uwex            
            if (estadoCorrente == EstadoManutencao.smInserir)
                lblCodigoOS.Visible = false;
            else           
                lblCodigoOS.Visible = true;
            

            if (OsConfiguracoes.tipoAcesso == 'A')
            {
                cmbCodigoUsuario.Enabled = true;
                cmbNomeUsuario.Enabled = true;
                // test uwex
                //lblCodigoOS.Visible = false;
            }
            else
            {
                // test uwex
                //lblCodigoOS.Visible = true;

                cmbCodigoUsuario.Text = OsConfiguracoes.codigoUsuario.ToString();
                cmbNomeUsuario.Text = OsConfiguracoes.nomeUsuario;
                cmbCodigoUsuario.Enabled = false;
                cmbNomeUsuario.Enabled = false;
            }
        }

        private void salvarOrdemServico()
        {
            tsbCancelar.Enabled = false;
            tsbInserir.Enabled = true;
            int proximoCodigo = (int)osOrdemServicoTableAdapter.SqlProximaOS(OsConfiguracoes.codigoUsuario, OsConfiguracoes.idPosto);
            //SELECT        { fn IFNULL(MAX(ID_OFFLINE), 0) }+1 AS CODIGO_OFFLINE
        //    OsOrdemServico o where o.ID_USR_OFFLINE = @USUARIO AND o.IDPOSTO = @IDPOSTO
            //uwex
            if (cmbNomeCliente.Text.Length == 0 || cmbNomeCliente.Text.Length == 0)
            {
                MessageBox.Show("Informe o Código do Cliente Antes de Gravar a OS!");
                return;
            }
            else
            {
                if (estadoCorrente == EstadoManutencao.smInserir)
                {
                    DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;
                    currentOS["IDPOSTO"] = OsConfiguracoes.idPosto;
                    currentOS["DT_CADASTRO"] = DateTime.Now;
                    currentOS["ID_OFFLINE"] = proximoCodigo;
                    currentOS["ID_FINAL"] = OsConfiguracoes.codigoUsuario.ToString() + OsConfiguracoes.idPosto.ToString() + proximoCodigo;
                    currentOS["ID_USR_OFFLINE"] = OsConfiguracoes.codigoUsuario;
                    currentOS["TIPO_DOC"] = 'S';
                }

                if (txtAssinaturaCliente.Text.Length == 0)
                {
                    //txtAssinaturaCliente.Text = cmbNomeCliente.Text;
                    // cmbNomeCliente - Displaymember alterado por LOGOTIPO
                    try {
                        txtAssinaturaCliente.Text =
                        DB.GetSingleValue("select (max) nome from a30clientes where logotipo='" + cmbNomeCliente.Text + "'").ToString();
                    }
                    // Exexão já tratado na biblioteca DB
                    catch (Exception ee) { }
                }

                if (txtEmail.Text.Length == 0)
                {
                    DataRowView currentCliente = (DataRowView)bdsClientes.Current;
                    txtEmail.Text = currentCliente["obs01"].ToString();
                }

                //currentOS["TOTAL_OS"] = (decimal)currentOS["TOTAL_PECAS_OS"] + (decimal)currentOS["TOTAL_GERAL_SERVICOS"];
                txtTotalFinalOS.Text = (decimal.Parse(txtTotalServicosOS.Text) + decimal.Parse(txtTotalGeralPecas.Text)).ToString();
                txtValorAPagar.Text = (decimal.Parse(txtTotalFinalOS.Text) - decimal.Parse(txtTotalPago.Text)).ToString();
                this.Validate();
                this.osOrdemServicoBindingSource.EndEdit();
                this.osOrdemServicoTableAdapter.Update(this.dtsPrincipal);
                this.osSituacoesOSBindingSource.EndEdit();
                this.osItemServicoBindingSource.EndEdit();
                this.osInstrumentoServicoBindingSource.EndEdit();
                this.osFormasPagamentoBindingSource.EndEdit();

                //this.bdsLacre.EndEdit();
                //this.bdsSelos.EndEdit();
                //this.osEtiquetaTableAdapter.Update(this.dtsPrincipal);


                this.tableAdapterManager.UpdateAll(this.dtsPrincipal);
                estadoCorrente = EstadoManutencao.smPesquisar;
                //bitValue = reader["MyBitColumn"] as bool? ?? null;
            }
            return;
        }

        private void tsbSalvar_Click(object sender, EventArgs e)
        {
            salvarOrdemServico();
            MessageBox.Show("Registro gravado com sucesso!");
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.osSituacoesOSBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dtsPrincipal);
        }


        private decimal calculavalores(int qtde, decimal valor)
        {
            decimal total;

            total = qtde * valor;

            return total;
        }


        private void calculaVisitaTecnica(object sender, EventArgs e)
        {
            if (txtVisitaTecnicaQtde.Text.Length > 0 && txtVisitaTecnicaVlr.Text.Length > 0)
            txtVisitaTecnicaTotal.Text = calculavalores(int.Parse(txtVisitaTecnicaQtde.Text), decimal.Parse(txtVisitaTecnicaVlr.Text)).ToString();
        }


        private void calculaDeslocamento(object sender, EventArgs e)
        {
            if (txtHoraDeslocamentoQtde.Text.Length > 0 && txtHoraDeslocamentoVlr.Text.Length > 0)
                txtHoraDeslocamentoTotal.Text = calculavalores(int.Parse(txtHoraDeslocamentoQtde.Text), decimal.Parse(txtHoraDeslocamentoVlr.Text)).ToString();
        }


        private void calculaExcedente(object sender, EventArgs e)
        {
            if (txtHoraExcedenteQtde.Text.Length > 0 && txtHoraExcedenteVlr.Text.Length > 0)
                txtHoraExcedenteTotal.Text = calculavalores(int.Parse(txtHoraExcedenteQtde.Text), decimal.Parse(txtHoraExcedenteVlr.Text)).ToString();
        }


        private void calculaInstalacao(object sender, EventArgs e)
        {
            if (txtTaxaInstalacaoQtde.Text.Length > 0 && txtTaxaInstalacaoVlr.Text.Length > 0)
                txtTaxaInstalacaoTotal.Text = calculavalores(int.Parse(txtTaxaInstalacaoQtde.Text), decimal.Parse(txtTaxaInstalacaoVlr.Text)).ToString();
        }


        private void calculaRede(object sender, EventArgs e)
        {
            if (txtPontoRedeQtde.Text.Length > 0 && txtPontoRedeVlr.Text.Length > 0)
                txtPontoRedeTotal.Text = calculavalores(int.Parse(txtPontoRedeQtde.Text), decimal.Parse(txtPontoRedeVlr.Text)).ToString();
        }


        private void calculaDeslocamentoKM(object sender, EventArgs e)
        {
            if (txtDeslocamentoKmQtde.Text.Length > 0 && txtDeslocamentoKmVlr.Text.Length > 0)
                txtDeslocamentoKmTotal.Text = calculavalores(int.Parse(txtDeslocamentoKmQtde.Text), decimal.Parse(txtDeslocamentoKmVlr.Text)).ToString();
        }


        private void calculaOutros(object sender, EventArgs e)
        {
            if (txtOutrosServicosQtde.Text.Length > 0 && txtOutrosServicosVlr.Text.Length > 0)
                txtOutrosServicosTotal.Text = calculavalores(int.Parse(txtOutrosServicosQtde.Text), decimal.Parse(txtOutrosServicosVlr.Text)).ToString();
        }


        private void calculaVlrDespesas(object sender, EventArgs e)
        {
            decimal totalDesp = 0;

            if (txtAlimentacao.Text.Length > 0)
                totalDesp = totalDesp + decimal.Parse(txtAlimentacao.Text);
            if (txtHospedagem.Text.Length > 0)
                totalDesp = totalDesp + decimal.Parse(txtHospedagem.Text);
            if (txtTranslado.Text.Length > 0)
                totalDesp = totalDesp + decimal.Parse(txtTranslado.Text);
            if (txtOutrasDespesas.Text.Length > 0)
                totalDesp = totalDesp + decimal.Parse(txtOutrasDespesas.Text);

            txtTotalDesp.Text = totalDesp.ToString();
        }


        private void calculaVlrTotalServ(object sender, EventArgs e)
        {
            decimal totalDesp = 0;

            if (txtVisitaTecnicaTotal.Text.Length > 0)
                totalDesp = totalDesp + decimal.Parse(txtVisitaTecnicaTotal.Text);
            if (txtHoraDeslocamentoTotal.Text.Length > 0)
                totalDesp = totalDesp + decimal.Parse(txtHoraDeslocamentoTotal.Text);
            if (txtHoraExcedenteTotal.Text.Length > 0)
                totalDesp = totalDesp + decimal.Parse(txtHoraExcedenteTotal.Text);
            if (txtTaxaInstalacaoTotal.Text.Length > 0)
                totalDesp = totalDesp + decimal.Parse(txtTaxaInstalacaoTotal.Text);
            if (txtPontoRedeTotal.Text.Length > 0)
                totalDesp = totalDesp + decimal.Parse(txtPontoRedeTotal.Text);
            if (txtDeslocamentoKmTotal.Text.Length > 0)
                totalDesp = totalDesp + decimal.Parse(txtDeslocamentoKmTotal.Text);
            if (txtOutrosServicosTotal.Text.Length > 0)
                totalDesp = totalDesp + decimal.Parse(txtOutrosServicosTotal.Text);

            txtTotalServicosTotal.Text = totalDesp.ToString();
        }


        private void calculaVlrTotalOS(object sender, EventArgs e)
        {
            decimal totalDesp = 0;

            if (txtTotalServicosTotal.Text.Length > 0)
                totalDesp = totalDesp + decimal.Parse(txtTotalServicosTotal.Text);
            if (txtTotalDesp.Text.Length > 0)
                totalDesp = totalDesp + decimal.Parse(txtTotalDesp.Text);
              
            txtTotalServicosOS.Text = totalDesp.ToString();
        }


        private void TplPermiteSoValor(object sender, KeyPressEventArgs e) 
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != ',')
            {
                e.Handled = true;
            }

            // Só permite apenas número um caracter de vírgula (decimal).
            if (e.KeyChar == ',' && (sender as TextBox).Text.IndexOf(',') > -1)
            {
                e.Handled = true;
            }
        }


        private void TplPermiteSoNumero(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }


        private void ManOrdemServico_Shown(object sender, EventArgs e)
        {
            if (OsConfiguracoes.tipoAcesso == 'A')
            {
                cmbCodigoUsuario.Enabled = true;
                cmbNomeUsuario.Enabled = true;
            }
            else
            {
                cmbCodigoUsuario.Enabled = false;
                cmbNomeUsuario.Enabled = false;
            }

            pnlConsultar.Visible = true;
        }


        private void calculaItemPeca(object sender, EventArgs e)
        {
            if (txtPrecoProduto.Text.Length < 1)
            {
                txtPrecoProduto.Text = "0";
            }      

            if (numQtdeProduto.Value != 0)
                txtTotalProduto.Text = (numQtdeProduto.Value * 
                                        decimal.Parse(txtPrecoProduto.Text)).ToString();
        }


        private void osOrdemServicoBindingSource_CurrentChanged(object sender, EventArgs e)
        {
            /*if ((cmbNomeCliente.Text.Length == 0 || cmbNomeCliente.Text.Length == 0) &&  (estadoCorrente == EstadoManutencao.smInserir))
            {
                MessageBox.Show("Informe o Código do Cliente Antes de Gravar a OS!");
                return;
            }*/

            DataRowView current = (DataRowView)osOrdemServicoBindingSource.Current;

            // Se existir registro...
            if (osOrdemServicoBindingSource.Count > 0)
            {
                if (OsConfiguracoes.tipoAcesso == 'A')
                {
                    txtPrecoProduto.ReadOnly = false;
                    txtPrecoProduto.Enabled = true;
                }
                else
                {
                    txtPrecoProduto.ReadOnly = true;
                    txtPrecoProduto.Enabled = false;
                }

                // deu muito problema
                //osItemServicoBindingSource.Filter = "IDORDEM = " + current["ID"];
                //osSituacoesOSBindingSource.Filter = "IDORDEM = " + current["ID"];

                 if ((string)current["STATUS_OS"] != "A")
                  {
                        tabCabecalho.Enabled = false;
                        tabSolicitacao.Enabled = false;
                        tabEncontrada.Enabled = false;
                        tabRealizada.Enabled = false;
                        tabFinal.Enabled = false;
                        tabPecas.Enabled = false;
                        tabServicos.Enabled = false;
                        tabEquipamentos.Enabled = false;
                        tabLacres.Enabled = false;
                        tabPagamento.Enabled = false;
                  }
                  else
                  {
                      tabCabecalho.Enabled = true;
                      tabSolicitacao.Enabled = true;
                      tabEncontrada.Enabled = true;
                      tabRealizada.Enabled = true;
                      tabFinal.Enabled = true;
                      tabPecas.Enabled = true;
                      tabServicos.Enabled = true;

                      if (OsConfiguracoes.digitaSeloELacre == true)
                      {
                          tabEquipamentos.Enabled = true;
                          tabLacres.Enabled = true;
                      }
                      else
                      {
                          tabEquipamentos.Enabled = false;
                          tabLacres.Enabled = false;
                      }

                      tabPagamento.Enabled = true;
                  }


                  if ((string)current["STATUS_OS"] == "A")
                  {
                      lblCabSituacao.Text = "Aberta";
                      lblCabSituacao.ForeColor = Color.Green;
                      lblSituacao2.ForeColor = Color.Green;
                      tsbDescartar.Enabled = true;
                      tsbFecharOS.Enabled = true;
                  }
                  else
                  if ((string)current["STATUS_OS"] == "D")
                  {
                      lblCabSituacao.Text = "Descartada";
                      lblCabSituacao.ForeColor = Color.Red;
                      lblSituacao2.ForeColor = Color.Red;
                      tsbDescartar.Enabled = false;
                      tsbFecharOS.Enabled = false;
                  }
                  else
                  if ((string)current["STATUS_OS"] == "F")
                  {
                      // 29.05.2014 Uwe
                      tsbGerarCaixa.Enabled = true;

                      lblCabSituacao.Text = "Fechada";
                      lblCabSituacao.ForeColor = Color.Blue;
                      lblSituacao2.ForeColor = Color.Blue;
                      tsbDescartar.Enabled = false;
                      tsbFecharOS.Enabled = false;
                  }
                  
                if (tabPrincipal.SelectedIndex == 1)
                    this.osSituacoesOSTableAdapter.SituacaoRealizada(this.dtsPrincipal.OsSituacoesOS, (int)current["ID"], "S");
                if (tabPrincipal.SelectedIndex == 2)
                    this.osSituacoesOSTableAdapter.SituacaoRealizada(this.dtsPrincipal.OsSituacoesOS, (int)current["ID"], "E");
                if (tabPrincipal.SelectedIndex == 3)
                    this.osSituacoesOSTableAdapter.SituacaoRealizada(this.dtsPrincipal.OsSituacoesOS, (int)current["ID"], "R");
                if (tabPrincipal.SelectedIndex == 4)
                    this.osSituacoesOSTableAdapter.SituacaoRealizada(this.dtsPrincipal.OsSituacoesOS, (int)current["ID"], "F");
                if (tabPrincipal.SelectedIndex == 5)
                {
                    this.osInstrumentoServicoTableAdapter.SqlEquipamentos(this.dtsPrincipal.OsInstrumentoServico, (int)current["ID"]);
                    bdsEquipamentoSelos.Filter = "IDCLIENTE = " + current["IDCLIENTE"].ToString();
                }
                if (tabPrincipal.SelectedIndex == 6)
                {
                    this.osInstrumentoServicoTableAdapter.SqlLacre(this.dtsPrincipal.OsInstrumentoServico, (int)current["ID"]);
                    bdsEquipamentoLacre.Filter = "IDCLIENTE = " + current["IDCLIENTE"].ToString();
                }
                if (tabPrincipal.SelectedIndex == 7)
                    this.osItemServicoTableAdapter.SqlItensDaOrdem(this.dtsPrincipal.OsItemServico, (int)current["ID"]);
                if (tabPrincipal.SelectedIndex == 9)
                    this.osFormasPagamentoTableAdapter.SqlFormasDePagamento(this.dtsPrincipal.OsFormasPagamento, (int)current["ID"]);

                if ((Decimal)current["VALOR_PAGO"] > 0)
                    tsbImprimirRecibo.Enabled = true;
                else
                    tsbImprimirRecibo.Enabled = false;
            }
        }

        private void limpaAbaSelos()
        {
            cmbNumInmetroSelos.SelectedIndex = -1;
            cmbNumSerieSelos.SelectedIndex = -1;
            //cmbCodigoSelos.SelectedIndex = -1;
            cmbNumeroEtiquetaSelos.SelectedIndex = -1;
        }

        private void limpaAbaLacres()
        {
            cmbNumSerieLacres.SelectedIndex = -1;
            cmbNumInmetroLacres.SelectedIndex = -1;
            //cmbCodigoLacres.SelectedIndex = -1;
            cmbNumeroEtiquetaLacres.SelectedIndex = -1;
        }

        private void limpaAbaProdutos()
        {
            cmbCodigoProduto.SelectedIndex = -1;
            cmbDescricaoProduto.SelectedIndex = -1;
        }
        /// <summary>
        /// 23.04.2014 Uwe
        /// Retorna as atividades referente á ordem de serviço
        /// </summary>
        /// <param name="id_ordem">Id de Ordem Serviço</param>
        /// <returns>Tabela OsSituacoesOs</returns>
        private DataTable linhasSituacao(int id_ordem)
        {
            return (DataTable)DB.GetTableFromSQL(
                     @"select * from OsSituacoesOS where HORA_INICIO is not null and idordem=" 
                       + id_ordem.ToString() 
                       + @" order by hora_inicio");
        }
        /// <summary>
        /// 23.04.2014 Uwe
        /// Retorna tempo passado desde 01.01.1970.
        /// </summary>
        /// <param name="dt">data</param>
        /// <returns>número de segundos</returns>
        private int dateTime2Seconds(DateTime dt)
        {
            TimeSpan span = dt.Subtract(new DateTime(1970, 1, 1, 0, 0, 0));
            return span.Hours*3600 + span.Minutes*60 + span.Seconds;
        }
        /// <summary>
        /// 23.04.2014 Uwe
        /// Retorna número de segundos já passados
        /// sem intervalo.
        /// </summary>
        /// <param name="dt">data</param>
        /// <returns>segundos</returns>
        private int getSecondsUsed(DataTable dt) 
        {
            DateTime timeZero  = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            DateTime timeSoFar = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            bool found_interval = false;
            int s = 0;
            int cnt = 0;
            foreach ( DataRow row in dt.Rows )
            {
                cnt += 1;
                timeSoFar = timeSoFar.AddHours(  ((DateTime)row["hora_total"]).Hour);
                timeSoFar = timeSoFar.AddMinutes(((DateTime)row["hora_total"]).Minute);
                timeSoFar = timeSoFar.AddSeconds(((DateTime)row["hora_total"]).Second);
               
                s = dateTime2Seconds(timeSoFar);
                if (s == MAXTEMPO_SEM_INTERVALO)
                {
                    found_interval = true;
                    timeSoFar = timeZero;
                    cnt = 0;
                }
            }
            // corrigir tempo
            for (int i=0;i<cnt;i++)
                timeSoFar = timeSoFar.AddMinutes(-1);
            return s;          
        }
        /// <summary>
        /// 23.04.2014 Uwe
        /// Retorna última hora_fim da tabela OsSituacoesOs
        /// </summary>
        /// <param name="dt">Table</param>
        /// <returns>Data</returns>
        private DateTime getLastHoraFim(DataTable dt)
        {
          return (DateTime)dt.Rows[dt.Rows.Count-1]["hora_fim"];
        }
        /// <summary>
        /// 23.04.2014 Uwe
        /// Calcular proxima hora_fim no máximo.
        /// </summary>
        /// <param name="dt">Data</param>
        /// <param name="sec_used">Segundos</param>
        /// <returns>Data</returns>
        private DateTime getNextHoraFim(DateTime dt, int sec_used)
        {
            DateTime timeSoFar = dt;
            int available = MAXTEMPO_SEM_INTERVALO - sec_used;
            int h = available / 3600;
            timeSoFar = timeSoFar.AddHours(h);
            int m = (available % 3600) / 60;
            timeSoFar = timeSoFar.AddMinutes(m);
            int s = (available % 3600) % 60;
            timeSoFar = timeSoFar.AddSeconds(s);
           
            // não pode ultrapassar meia-noite
            DateTime timeZero = new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0);
            DateTime midnight = new DateTime(dt.Year, dt.Month, dt.Day, 23, 59, 59);
            if (timeSoFar > midnight)
                timeSoFar = midnight;

            return timeSoFar;
        }
        /// <summary>
        /// inicializar respectivamente atualizar os controles
        /// DateTimePicker
        /// </summary>
        /// <returns>errorCode(não usado até agora)</returns>
        private int updateHorarioRealizado()
        {
            DB.ConnectionName = "osExpress.Properties.Settings.OsExpressConnectionString";
            //var constr = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            int error_code = 0;
            if (lblNovoCodOS.Text.Length == 0) return -1;
                int id_final = Convert.ToInt32(lblNovoCodOS.Text);
                int id_ordem = (int) DB.GetSingleValue("select * from osordemservico where ID_final=" + id_final.ToString());

                DateTime dtInicio = dtpHoraInicialRealizada.Value;
                DateTime dtFinal  = dtpHoraFinalRealizada.Value;           

                DataTable dt = linhasSituacao(id_ordem);
              
                int rcount = dt.Rows.Count;
                // primeira atividade
                if (rcount == 0)
                {
                    dtpHoraInicialRealizada.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 8, 0, 0);
                    dtpHoraFinalRealizada.Value = dtpHoraInicialRealizada.Value.AddHours(4);
                    
                    dtInicio = dtpHoraInicialRealizada.Value;
                    dtFinal = dtpHoraFinalRealizada.Value;
                    TimeSpan horaTotal = new TimeSpan(dtFinal.Ticks - dtInicio.Ticks);
                    dtpHoraTotalRealizada.Text = horaTotal.ToString();
                }
                else 
                {
                    int s = getSecondsUsed(dt);
                    DateTime nextInicialDate = getLastHoraFim(dt);

                    // proxima atividade - hora inicial                    
                    int secondsUsed = getSecondsUsed(dt);
                    if (secondsUsed == MAXTEMPO_SEM_INTERVALO)
                        nextInicialDate = nextInicialDate.AddHours(1);
                    dtpHoraInicialRealizada.Value = nextInicialDate;
                    // proxima atividade - hora fim                                      
                    DateTime nextFinalDate = nextInicialDate;

                    DateTime timeZero = new DateTime(nextFinalDate.Year,
                                                    nextFinalDate.Month,
                                                    nextFinalDate.Day, 0, 0, 0);
                    DateTime midnight = new DateTime(nextFinalDate.Year,
                                                     nextFinalDate.Month,
                                                     nextFinalDate.Day, 23, 59, 59);

                    if (secondsUsed == MAXTEMPO_SEM_INTERVALO)
                        nextFinalDate = nextFinalDate.AddHours(4);
                    else
                        nextFinalDate = getNextHoraFim(nextFinalDate, getSecondsUsed(dt));


                    if (nextFinalDate > midnight)
                    {
                        dtpHoraFinalRealizada.Value = midnight;
                        nextFinalDate = midnight;
                    }
                    dtpHoraFinalRealizada.Value = nextFinalDate;

                    dtInicio = dtpHoraInicialRealizada.Value;
                    dtFinal  = dtpHoraFinalRealizada.Value;

                    TimeSpan horaTotal = new TimeSpan((dtFinal.Ticks - dtInicio.Ticks));
                    try
                    {
                        dtpHoraTotalRealizada.Text = horaTotal.ToString();
                    }
                    catch(Exception ex)
                    {
                        horaTotal = new TimeSpan(0);
                        dtpHoraTotalRealizada.Text = horaTotal.ToString();
                    }
                }
                return error_code;
        }
            
        private void tabPrincipal_Selected(object sender, TabControlEventArgs e)
        {   
            DataRowView current = (DataRowView) osOrdemServicoBindingSource.Current;
            if (current["IDCLIENTE"].ToString().Length == 0)
            {
                MessageBox.Show("Informe o Codigo de Cliente Antes de Gravar a OS!", "E R R O ");
                return;
            }

            if (tabPrincipal.SelectedIndex == 1)
                this.osSituacoesOSTableAdapter.SituacaoRealizada(this.dtsPrincipal.OsSituacoesOS, (int)current["ID"], "S");
            if (tabPrincipal.SelectedIndex == 2)
                this.osSituacoesOSTableAdapter.SituacaoRealizada(this.dtsPrincipal.OsSituacoesOS, (int)current["ID"], "E");
            if (tabPrincipal.SelectedIndex == 3)
            {
                this.osSituacoesOSTableAdapter.SituacaoRealizada(this.dtsPrincipal.OsSituacoesOS, (int)current["ID"], "R");
                updateHorarioRealizado();               
            }
            if (tabPrincipal.SelectedIndex == 4)
                this.osSituacoesOSTableAdapter.SituacaoRealizada(this.dtsPrincipal.OsSituacoesOS, (int)current["ID"], "F");
            if (tabPrincipal.SelectedIndex == 5)
            {
                //this.osInstrumentoServicoTableAdapter.Fill(dtsPrincipal.OsInstrumentoServico);
                this.osInstrumentoServicoTableAdapter.SqlEquipamentos(this.dtsPrincipal.OsInstrumentoServico, (int)current["ID"]);
                bdsEquipamentoSelos.Filter = "IDCLIENTE = " + current["IDCLIENTE"].ToString();
                bdsSelos.Filter = "UTILIZADO = 0 AND TIPO = 'S' AND IDUSUARIO = " + OsConfiguracoes.codigoUsuario;
                limpaAbaSelos();
            }

            if (tabPrincipal.SelectedIndex == 6)
            {
                this.osInstrumentoServicoTableAdapter.SqlLacre(this.dtsPrincipal.OsInstrumentoServico, (int)current["ID"]);
                bdsEquipamentoLacre.Filter = "IDCLIENTE = " + current["IDCLIENTE"].ToString();
                bdsLacre.Filter = "UTILIZADO = 0 AND TIPO = 'L' AND IDUSUARIO = " + OsConfiguracoes.codigoUsuario;
                limpaAbaLacres();
            }
            if (tabPrincipal.SelectedIndex == 7)
            {
                this.osItemServicoTableAdapter.SqlItensDaOrdem(this.dtsPrincipal.OsItemServico, (int)current["ID"]);
                limpaAbaProdutos();
            }
            if (tabPrincipal.SelectedIndex == 9)
                this.osFormasPagamentoTableAdapter.SqlFormasDePagamento(this.dtsPrincipal.OsFormasPagamento, (int)current["ID"]);
        }


        private void tsbExcluir_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Deseja realmente descartar essa OS?", 
                                null, 
                                MessageBoxButtons.YesNo, 
                                MessageBoxIcon.Warning) == DialogResult.Yes)
            {   
                DataRowView current = (DataRowView)osOrdemServicoBindingSource.Current;

                current["STATUS_OS"] = "D";

                osOrdemServicoBindingSource.EndEdit();
                osOrdemServicoTableAdapter.Update(this.dtsPrincipal);
            }
        }


        private void toolStripButton8_Click(object sender, EventArgs e)
        {
            Close();
        }


        private void button6_Click(object sender, EventArgs e)
        {
            //if (!salvarOrdemServico()) return;
            salvarOrdemServico();
            if (cmbCodigoEquipamentoSelos.Text.Length == 0)
            {
                MessageBox.Show("Informe o número de série da bomba.");
                return;
            }

            if (cmbCodigoINMETROSelos.Text.Length == 0)
            {
                MessageBox.Show("Informe o número de INMETRO utilizado.");
                return;
            }

            if (cmbCodigoSelos.Text.Length == 0)
            {
                MessageBox.Show("Informe o número do selo utilizado.");
                return;
            }

            DataRowView current = (DataRowView)osInstrumentoServicoBindingSource.AddNew();

            current["IDORDEM"] = lblCodigoOS.Text;
            current["IDIBAMETRO"] = cmbCodigoEquipamentoSelos.Text;
            current["IDINMETRO"] = cmbCodigoINMETROSelos.Text;
            current["IDETIQUETA"] = cmbCodigoSelos.Text;

            DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;

            if (estadoCorrente == EstadoManutencao.smInserir)
            {
                //currentOS["IDUSUARIO"] = OsConfiguracoes.codigoUsuario;
                currentOS["IDPOSTO"] = OsConfiguracoes.idPosto;
                currentOS["DT_CADASTRO"] = DateTime.Now;
            }
            osOrdemServicoBindingSource.EndEdit();
            osOrdemServicoTableAdapter.Update(dtsPrincipal);

            osInstrumentoServicoBindingSource.EndEdit();
            osInstrumentoServicoTableAdapter.Update(dtsPrincipal);

            this.osInstrumentoServicoTableAdapter.SqlEquipamentos(this.dtsPrincipal.OsInstrumentoServico, (int)currentOS["ID"]);
            limpaAbaSelos();
        }

        private void calculaHora(object sender, EventArgs e)
        {
            try
            {
                DateTime dtInicio = DateTime.Parse(dtpHoraInicialRealizada.Text);
                DateTime dtFinal = DateTime.Parse(dtpHoraFinalRealizada.Text);

                TimeSpan horaTotal = new TimeSpan(dtFinal.Ticks - dtInicio.Ticks);

                dtpHoraTotalRealizada.Text = horaTotal.ToString();
            }
            catch (Exception)
            {

            }

        }


        private void calculaFormaPagto(object sender, EventArgs e)
        {
            decimal totalDesp = 0;

            if (txtTotalFinalOS.Text.Length > 0)
                totalDesp = totalDesp + decimal.Parse(txtTotalFinalOS.Text);
            if (txtValorPago.Text.Length > 0)
                totalDesp = totalDesp - decimal.Parse(txtValorPago.Text);
        }


        private void chkPorConta_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPorContaGeral.Checked == true)
            {
                txtAlimentacao.Text = "";
                txtHospedagem.Text = "";
                txtTranslado.Text = "";
                txtOutrasDespesas.Text = "";
                grpDespesas.Enabled = false;
            }
            else
                grpDespesas.Enabled = true;
        }


        private void btnAdicionarPecas_Click(object sender, EventArgs e)
        {
            //if (!salvarOrdemServico()) return;
            salvarOrdemServico();
            DataRowView current = (DataRowView)osItemServicoBindingSource.AddNew();

            current["IDORDEM"] = lblCodigoOS.Text;
            current["IDPRODUTO"] = cmbCodigoIDProduto.Text;
            current["QUANTIDADE"] = numQtdeProduto.Value;
            current["VALOR"] = txtPrecoProduto.Text;

            if (txtTotalProduto.Text.Length > 0)
                current["TOTAL"] = decimal.Parse(txtTotalProduto.Text);
            else
                current["TOTAL"] = 0;

            if (txtTotalGeralPecas.Text.Length < 1)
            {
                txtTotalGeralPecas.Text = "0";
            }

            DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;

            if (estadoCorrente == EstadoManutencao.smInserir)
            {
                //currentOS["IDUSUARIO"] = OsConfiguracoes.codigoUsuario;
                currentOS["IDPOSTO"] = OsConfiguracoes.idPosto;
                currentOS["DT_CADASTRO"] = DateTime.Now;
            }

            /*currentOS["TOTAL_PECAS_OS"] = (decimal)currentOS["TOTAL_PECAS_OS"] + decimal.Parse(txtTotalProduto.Text);

            osOrdemServicoBindingSource.EndEdit();
            osOrdemServicoTableAdapter.Update(dtsPrincipal);

            osItemServicoBindingSource.EndEdit();
            osItemServicoTableAdapter.Update(dtsPrincipal);
            */

            osItemServicoBindingSource.EndEdit();
            osItemServicoTableAdapter.Update(dtsPrincipal);

            currentOS["TOTAL_PECAS_OS"] = osItemServicoTableAdapter.SqlTotalProdutos((int)currentOS["ID"]);//(decimal)currentOS["TOTAL_PECAS_OS"] + decimal.Parse(txtTotalProduto.Text);

            osOrdemServicoBindingSource.EndEdit();
            osOrdemServicoTableAdapter.Update(dtsPrincipal);

            numQtdeProduto.Value = 1;
            numQtdeProduto.Focus();

            this.osItemServicoTableAdapter.SqlItensDaOrdem(this.dtsPrincipal.OsItemServico, (int)currentOS["ID"]);
            limpaAbaProdutos();
        }


        private void btnAdicionarFormaPag_Click(object sender, EventArgs e)
        {
            //if (!salvarOrdemServico()) return;
            salvarOrdemServico();
            if (txtTotalPago.Text.Length == 0 )
            {
                txtTotalPago.Text = "0";
            }
            if (decimal.Parse(txtTotalPago.Text) + decimal.Parse(txtValorPago.Text) > decimal.Parse(txtTotalFinalOS.Text))
            {
                    MessageBox.Show("Atenção: O valor do pagamento não pode ultrapassar o valor total da OS.");
                    return;
            }
            else
            {
                DataRowView current = (DataRowView)osFormasPagamentoBindingSource.AddNew();

                current["IDORDEM"] = lblCodigoOS.Text;
                current["IDFORMA"] = cmbCodigoForma.Text;
                if (txtValorPago.Text.Length > 0)
                    current["VALOR"] = decimal.Parse(txtValorPago.Text);

                this.osFormasPagamentoBindingSource.EndEdit();
                this.osFormasPagamentoTableAdapter.Update(dtsPrincipal);

                if (txtTotalPago.Text.Length < 1)
                {
                    txtTotalPago.Text = "0";
                }

                DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;
                currentOS["VALOR_PAGO"] = (decimal.Parse(txtTotalPago.Text) + decimal.Parse(txtValorPago.Text)).ToString();
                currentOS["VALOR_RESTANTE"] = (decimal.Parse(txtTotalFinalOS.Text) - decimal.Parse(txtTotalPago.Text)).ToString();

                //txtTotalPago.Text = (decimal.Parse(txtTotalPago.Text) + decimal.Parse(txtValorPago.Text)).ToString();/*osFormasPagamentoTableAdapter.SqlSomaPagamentos(int.Parse(label8.Text)).ToString();*/
                //txtValorAPagar.Text = (decimal.Parse(txtTotalFinalOS.Text) - decimal.Parse(txtTotalPago.Text)).ToString();
                //txtValorAPagar.Text = osOrdemServicoTableAdapter.SqlDiferenca(int.Parse(label8.Text)).ToString();
                if (estadoCorrente == EstadoManutencao.smInserir)
                {
                    //currentOS["IDUSUARIO"] = OsConfiguracoes.codigoUsuario;
                    currentOS["IDPOSTO"] = OsConfiguracoes.idPosto;
                    currentOS["DT_CADASTRO"] = DateTime.Now;
                }

                osOrdemServicoBindingSource.EndEdit();
                osOrdemServicoTableAdapter.Update(dtsPrincipal);

                osFormasPagamentoBindingSource.EndEdit();
                osFormasPagamentoTableAdapter.Update(dtsPrincipal);

                txtValorPago.Clear();
                txtValorPago.Focus();
            }
        }


        private void imprimirRecibo(string id_final)
        {
            RelRecibo relRecibo = new RelRecibo();

            ParameterFields paramFields = new ParameterFields();
            ParameterField pfItemYr = new ParameterField();
            pfItemYr.ParameterFieldName = "Ordem";
            ParameterDiscreteValue dcItemYr = new ParameterDiscreteValue();
            // 28.04. Uwe
            // Ponte Safana ...
            if (lblCodigoOS.Text.Equals(""))
            {
                lblCodigoOS.Text = DB.GetSingleValue("select id from osordemservico where id_final=" + id_final).ToString();
            }
            dcItemYr.Value = lblCodigoOS.Text;
            pfItemYr.CurrentValues.Add(dcItemYr);
            paramFields.Add(pfItemYr);
            relRecibo.crystalReportViewer1.ParameterFieldInfo = paramFields;

            // Se houve pagamento...
            if (txtTotalFinalOS.Text.Length > 0)
            {
                NumeroPorExtenso extenso = new NumeroPorExtenso();
                DataRowView current = (DataRowView)osOrdemServicoBindingSource.Current;
                extenso.SetNumero((decimal) current["VALOR_PAGO"]);

                OsConfiguracoes.literalExtenso = extenso.ToString();

                if ((decimal)(current["TOTAL_OS"]) == (decimal)(current["VALOR_PAGO"]))
                    OsConfiguracoes.literalRecibo = "Referente ao pagamento da Ordem de Serviço, nº " + lblNovoCodOS.Text;
                else
                    OsConfiguracoes.literalRecibo = "Referente ao pagamento PARCIAL da Ordem de Serviço, nº " + lblNovoCodOS.Text;
            }
            relRecibo.Show();
        }


        private void imprimirOS(String codigoOS, String arquivo)
        {
            string fileName = "Ordem de Servico nº " + arquivo + ".pdf";

            relOrdemServico = new RelOrdem();
            // 28.04. Uwe
            // Ponte Safana ...
            if (codigoOS.Equals(""))
            {
                codigoOS = DB.GetSingleValue("select id from osordemservico where id_final=" + arquivo).ToString();
            }
            relOrdemServico.configuraOS(codigoOS, fileName);           

            relOrdemServico.Show();
        }


        private void tsbImprimirRecibo_Click(object sender, EventArgs e)
        {
            DataRowView current = (DataRowView)osOrdemServicoBindingSource.Current;
            if ((Boolean)current["RECIBO_IMPRESSO"] == false)
            {
                if (MessageBox.Show("O recibo ainda não foi impresso deseja imprimir agora?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    current["RECIBO_IMPRESSO"] = true;
                    osOrdemServicoBindingSource.EndEdit();
                    osOrdemServicoTableAdapter.Update(dtsPrincipal);

                    DataRowView currentRecibo = (DataRowView)osReciboBindingSource.AddNew();

                    // 28.04. Uwe
                    // Ponte Safana ...
                    if (lblCodigoOS.Text.Equals(""))
                    {
                        lblCodigoOS.Text = current["id_final"].ToString();
                    }

                    currentRecibo["IDORDEM"] = lblCodigoOS.Text;

                    osReciboBindingSource.EndEdit();
                    osReciboTableAdapter.Update(dtsPrincipal);

                    imprimirRecibo(current["id_final"].ToString());
                }
            }
            else
                imprimirRecibo(current["id_final"].ToString());
        }


        private void tsbImprimirOS_Click(object sender, EventArgs e)
        {
            imprimirOS(lblCodigoOS.Text, lblNovoCodOS.Text);
        }


        private void osFormasPagamentoBindingSource_CurrentChanged(object sender, EventArgs e)
        {
            if (osFormasPagamentoBindingSource.Count > 0)
                tsbImprimirRecibo.Enabled = true;
            else
                tsbImprimirRecibo.Enabled = false;
        }


        private void chkPorContaAlimentacao_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPorContaAlimentacao.Checked == true)
            {
                txtAlimentacao.Text = "0";
                txtAlimentacao.Enabled = false;
            }
            else
                txtAlimentacao.Enabled = true;
        }


        private void chkPorContaHospedagem_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPorContaHospedagem.Checked == true)
            {
                txtHospedagem.Text = "0";
                txtHospedagem.Enabled = false;
            }
            else
                txtHospedagem.Enabled = true;
        }


        private void chkPorContaTranslado_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPorContaTranslado.Checked == true)
           { 
                txtTranslado.Text = "0";
                txtTranslado.Enabled = false;
            }
            else
                txtTranslado.Enabled = true;
        }

        private void chkPorContaOutros_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPorContaOutros.Checked == true)
            {
                txtOutrasDespesas.Text = "0";
                txtOutrasDespesas.Enabled = false;
            }
            else
                txtOutrasDespesas.Enabled = true;
        }
        /// <summary>
        /// Modificação Selo/Lacre 
        /// Uwe Kristmann 
        /// 17.02.2014
        /// </summary>
        /// <param name="id_ordemServico">id de ordem de serviço
        /// referente aos quais os selos ou lacres serão marcados como ultilizados.
        /// </param>
        private void updateSeloLacre(int id_ordemServico)
        {
            System.Diagnostics.Debug.WriteLine("id_ordemServico = " + id_ordemServico.ToString());
            List<int> selo_e_lacre = 
             DB.GetList<int>("Select idetiqueta From OsInstrumentoServico where idordem=" + id_ordemServico.ToString());
            foreach ( int etiqueta in selo_e_lacre )
            {
              System.Diagnostics.Debug.WriteLine("etiqueta = " + etiqueta.ToString());            
              DB.ExecuteNonQuery("Update OsEtiqueta set UTILIZADO = 1 Where id=" + etiqueta.ToString()); 
            }
        }

        private void tsbFecharOS_Click(object sender, EventArgs e)
        {
            string email = txtEmail.Text;
           
            String codigoOS = lblCodigoOS.Text;
            String codigoOSFinal = lblNovoCodOS.Text;
            relOrdemServico = new RelOrdem();

            //if (!salvarOrdemServico()) return;
            salvarOrdemServico();
            DataRowView current = (DataRowView)osOrdemServicoBindingSource.Current;
            
            // 17.02.2014 Modificação Uwe Kristmann
            // marcar Selo/Lacre utilizado  
            updateSeloLacre((int)current["ID"]);

            if (MessageBox.Show("Deseja Realmente Fechar essa OS?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                NumeroPorExtenso extenso = new NumeroPorExtenso();
                extenso.SetNumero(decimal.Parse(txtValorAPagar.Text));
                OsConfiguracoes.literalExtenso = extenso.ToString();

                current["STATUS_OS"] = "F";
                current["DT_FECHAMENTO"] = DateTime.Now;
                current["VALOR_RESTANTE_EXTENSO"] = OsConfiguracoes.literalExtenso;
                
                osOrdemServicoBindingSource.EndEdit();
                osOrdemServicoTableAdapter.Update(this.dtsPrincipal);
                enviaEmail.enviarTecnoExpress(codigoOSFinal, OsConfiguracoes.nomeUsuario, "patricia@tecnoexpress.com.br", "O");
                // 29.05.2014 Uwe
                tsbGerarCaixa.Enabled = true;
                MessageBox.Show("OS fechada com sucesso!");

                // 17.02.2014 Modificação Uwe Kristmann
                // "DebugMode"
                string cnn = ConfigurationManager.ConnectionStrings["osExpress.Properties.Settings.OsExpressConnectionString"].ToString();
                if (cnn.Substring(12,19).Equals(@"192.168.25.48\posto"))
                    MessageBox.Show("DEBUGMODE - Impressora não funciona.");
                else 
                {
                  try
                  {
                    imprimirOS(codigoOS, codigoOSFinal);
                  }
                  catch (Exception ex)
                  {
                    MessageBox.Show("Verifique a disponibilidade da impressora!","ERRO - impressão");
                  }
                }

                if (MessageBox.Show("Deseja enviar o email para o cliente?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    enviaEmail.enviarCliente(email, "OS", "Ordem de Servico nº " + codigoOSFinal + ".pdf", corpoOS);
                    MessageBox.Show("Emai enviado com sucesso!");
                }
            }
        }


        private void grdPagamentos_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            DataRowView currentPagamentos = (DataRowView)osFormasPagamentoBindingSource.Current;
            DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;

            currentOS["VALOR_PAGO"] = (decimal)currentOS["VALOR_PAGO"] - (decimal)currentPagamentos["VALOR"];
            currentOS["VALOR_RESTANTE"] = (decimal)currentOS["VALOR_RESTANTE"] + (decimal)currentPagamentos["VALOR"];

            osOrdemServicoBindingSource.EndEdit();
            osOrdemServicoTableAdapter.Update(dtsPrincipal);
        }

        private void grdPecas_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;
            osItemServicoBindingSource.EndEdit();
            osItemServicoTableAdapter.Update(dtsPrincipal);

            currentOS["TOTAL_PECAS_OS"] = osItemServicoTableAdapter.SqlTotalProdutos((int)currentOS["ID"]);//(decimal)currentOS["TOTAL_PECAS_OS"] - (decimal)currentPecas["TOTAL"];
            currentOS["TOTAL_OS"] = osItemServicoTableAdapter.SqlTotalProdutos((int)currentOS["ID"]);//(decimal)currentOS["TOTAL_OS"] - (decimal)currentPecas["TOTAL"];

            osOrdemServicoBindingSource.EndEdit();
            osOrdemServicoTableAdapter.Update(dtsPrincipal);
        }

        private void grdPecas_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            /*DataRowView currentPecas = (DataRowView)osItemServicoBindingSource.Current;
            DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;

            currentOS["TOTAL_PECAS_OS"] = (decimal)currentOS["TOTAL_PECAS_OS"] - (decimal)currentPecas["TOTAL"];
            currentOS["TOTAL_OS"] = (decimal)currentOS["TOTAL_OS"] - (decimal)currentPecas["VALOR"];

            osOrdemServicoBindingSource.EndEdit();
            osOrdemServicoTableAdapter.Update(dtsPrincipal);*/

        }


        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            tsbCancelar.Enabled = false;
            tsbInserir.Enabled = true;
            osOrdemServicoBindingSource.CancelEdit();
            pnlConsultar.Visible = true;
        }


        private void chkFiltrarSelecoes_CheckedChanged(object sender, EventArgs e)
        {
            string sAnd = " AND ";
            string sFilter = "";

            // Limpa o filtro antes.
            osOrdemServicoBindingSource.Filter = "";

            if ( OsConfiguracoes.tipoAcesso == 'A')
            {
                sFilter = sFilter = " TIPO_DOC = 'S' "+sAnd;    
            }
            else
            {
                sFilter = sFilter = "TIPO_DOC = 'S' AND ID_USR_OFFLINE = " + "'" + OsConfiguracoes.codigoUsuario + "'" + sAnd;
                cmbFiltroUsuarioCodigo.Enabled = false;
                cmbFiltroUsuarioNome.Enabled = false;
                chkFiltrarUsuario.Enabled = false;
            }

            if (chkFiltrarSituacao.Checked)
            {
                sFilter = sFilter + " STATUS_OS = " + "'" + cmbFiltroSituacao.Text.Substring(0, 1) + "'" + sAnd;
            }
            if (chkFiltrarUsuario.Checked)
            {
                sFilter = sFilter + " ID_USR_OFFLINE = " + cmbFiltroUsuarioNome.SelectedValue + sAnd;
            }
            if (chkFiltrarCliente.Checked)
            {
                sFilter = sFilter + " IDCLIENTE = " + cmbFiltroClienteNome.SelectedValue + sAnd;
            }

            // Se existir filtro...
            if (sFilter.Length > 0)
                osOrdemServicoBindingSource.Filter = sFilter.Substring(0, sFilter.Length - 5);
            // 29.05.2014 Uwe
            tsbGerarCaixa.Enabled = false;
            lblTotalOS.Text = "Total: " + osOrdemServicoBindingSource.Count.ToString();
        }


        private void toolStripButton3_Click_1(object sender, EventArgs e)
        {           
            osOrdemServicoBindingSource.CancelEdit();
            pnlConsultar.Visible = true;            
            this.osOrdemServicoTableAdapter.SqlOrdensServico(this.dtsPrincipal.OsOrdemServico);
            // 29.05.2014 Uwe
            tsbGerarCaixa.Enabled = false;
            pesquisar();
        }


        private void osOrdemServicoDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //pnlConsultar.Visible = false;
            // 29.05.2014 Uwe
            tsbGerarCaixa.Enabled = false;
            
        }
        private void osOrdemServicoDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            pnlConsultar.Visible = false;
            // 29.05.2014 Uwe
            tsbGerarCaixa.Enabled = false;
            DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;
            //if (cmbFiltroSituacao.SelectedItem.ToString()[0] == 'F')
            if (currentOS["STATUS_OS"].ToString() == "F")
                tsbGerarCaixa.Enabled = true;           
        }


        private void osOrdemServicoDataGridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.S)
            {
                pnlConsultar.Visible = false;
                DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;
                //if (cmbFiltroSituacao.SelectedItem.ToString()[0] == 'F')
                if (currentOS["STATUS_OS"].ToString() == "F")
                    tsbGerarCaixa.Enabled = true;
            }
        }


        private void tsbGerarCaixa_Click(object sender, EventArgs e)
        {
            
            // Essa rotina é responsável faz a chamada da procedure de fechamento do caixa,
            if (decimal.Parse(txtTotalFinalOS.Text) > 0)
            {
                var connectionString = ConfigurationManager.ConnectionStrings["osExpress.Properties.Settings.OsExpressConnectionString"].ConnectionString;
                SqlConnection conn = new SqlConnection(connectionString);
                SqlCommand cmd = new SqlCommand("dbo.osPrcGeraCaixa", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter param = new SqlParameter("@CODIGO_ORDEM", int.Parse(lblCodigoOS.Text));
                param.Direction = ParameterDirection.Input;
                param.DbType = DbType.Int32;
                
                cmd.Parameters.Add(param);
                cmd.Connection.Open();
                if (cmd.ExecuteNonQuery() > 0)
                    MessageBox.Show("Caixa gerado com sucesso!");
                cmd.Connection.Close();
            }
        }

        private void btnInserirSituacaoEncontrada_Click(object sender, EventArgs e)
        {
            //if (!salvarOrdemServico()) return;
            salvarOrdemServico();
            DataRowView current = (DataRowView)osSituacoesOSBindingSource.AddNew();

            current["IDORDEM"] = lblCodigoOS.Text;
            current["IDSITUACAO"] = cmbCodigoSitEncontrada.Text;

            DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;

            if (estadoCorrente == EstadoManutencao.smInserir)
            {
                //currentOS["IDUSUARIO"] = OsConfiguracoes.codigoUsuario;
                currentOS["IDPOSTO"] = OsConfiguracoes.idPosto;
                currentOS["DT_CADASTRO"] = DateTime.Now;
            }
            osOrdemServicoBindingSource.EndEdit();
            osOrdemServicoTableAdapter.Update(dtsPrincipal);

            osSituacoesOSBindingSource.EndEdit();
            osSituacoesOSTableAdapter.Update(dtsPrincipal);

            this.osSituacoesOSTableAdapter.SituacaoRealizada(this.dtsPrincipal.OsSituacoesOS, (int)currentOS["ID"], "E");
        }

        private void btnInserirTipoSolicitacao_Click(object sender, EventArgs e)
        {
            //if (!salvarOrdemServico()) return;
            salvarOrdemServico();
            // Mensagem de Erro já em "salvarOrdemServico"
            //if (cmbNomeCliente.Text.Length == 0)
            //    return;
            DataRowView current = (DataRowView)osSituacoesOSBindingSource.AddNew();

            current["IDORDEM"] = lblCodigoOS.Text;
            current["IDSITUACAO"] = cmbCodigoSolicitacao.Text;

            DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;

            if (estadoCorrente == EstadoManutencao.smInserir)
            {
                //currentOS["IDUSUARIO"] = OsConfiguracoes.codigoUsuario;
                currentOS["IDPOSTO"] = OsConfiguracoes.idPosto;
                currentOS["DT_CADASTRO"] = DateTime.Now;
            }
            // todo errorhandling uwe
            // column idusuario doesnot allow NULL
            osOrdemServicoBindingSource.EndEdit();
            osOrdemServicoTableAdapter.Update(dtsPrincipal);

            osSituacoesOSBindingSource.EndEdit();
            osSituacoesOSTableAdapter.Update(dtsPrincipal);

            this.osSituacoesOSTableAdapter.SituacaoRealizada(this.dtsPrincipal.OsSituacoesOS, (int)currentOS["ID"], "S");
        }

        /// <summary>
        /// 22.04.2014 Uwe
        /// Verificar se o novo horário inicial não interfera
        /// com o último horário fim.
        /// </summary>
        /// <returns>se novo horario inicial seja válido ou não</returns>
        private bool horarioInvalido()
        {
            DateTime dtMaxFim = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
            // todo: error handling uwe
            if (lblNovoCodOS.Text.Length == 0)
            {
                MessageBox.Show("Informe o Codigo do Cliente Antes de Gravar a OS","E R R O");
                return true;
            }
            int id_final = Convert.ToInt32(lblNovoCodOS.Text);
            int id_ordem = (int) DB.GetSingleValue("select * from osordemservico where ID_final=" + id_final.ToString());

            DateTime dtInicio = DateTime.Parse(dtpHoraInicialRealizada.Text);
            DateTime dtFinal  = DateTime.Parse(dtpHoraFinalRealizada.Text);
           
            DataTable dt = linhasSituacao(id_ordem);

            int x = getSecondsUsed(dt);           

            int cnt = (int)DB.GetSingleValue(@"select count(1)  from ossituacoesos where 1=1
                                        and not HORA_INICIO is null 
                                        and IDordem=" + id_ordem.ToString());
            if (cnt > 0)
            {
                dtMaxFim = (DateTime)DB.GetSingleValue(
                                              @"select max(hora_fim) from ossituacoesos where 1=1
                                               and not HORA_INICIO is null 
                                               and IDordem=" + id_ordem.ToString());
                if ((new TimeSpan(dtInicio.Ticks - dtMaxFim.Ticks).TotalSeconds) < 0.0)
                {
                    MessageBox.Show("Favor escolher horário inicial maior que último horário fim!", "A V I S O",
                                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return true;
                }
            }

            if ((new TimeSpan(dtInicio.Ticks - dtFinal.Ticks).TotalSeconds) >= 0.0)
            {
                MessageBox.Show("Favor escolher horário fim maior que horário inicial!", "A V I S O",
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return true;
            }

            if ((new TimeSpan(dtFinal.Ticks - dtInicio.Ticks).TotalSeconds) + getSecondsUsed(dt) > MAXTEMPO_SEM_INTERVALO)
            {
                 int intervalo = 0;
                 if (cnt > 0)
                 {
                     DateTime lastFim = getLastHoraFim(dt);
                     if (lastFim.AddHours(1) > dtInicio)
                     {
                         MessageBox.Show("Favor cumpra intervalo depois de 4 horas de trabalho.", "A V I S O",
                                       MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                       return true;
                     }
                 }
                 else
                 {
                     MessageBox.Show("Favor cumpra intervalo depois de 4 horas de trabalho!", "A V I S O",
                                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                     return true;
                 }
            }

            
            return false;
        }
        
        private void btnAtividadeRealizada_Click(object sender, EventArgs e)
        {      
            // verificar horários válidos
            // 08.04.2014 Uwe
            if (horarioInvalido())
            {               
                return;
            }

            //if (!salvarOrdemServico()) return;
            salvarOrdemServico();
            DataRowView current = (DataRowView)osSituacoesOSBindingSource.AddNew();
          
            DateTime dtInicio = dtpHoraInicialRealizada.Value;
            DateTime dtFinal = dtpHoraFinalRealizada.Value;

            TimeSpan horaTotal = new TimeSpan(dtFinal.Ticks - dtInicio.Ticks);
           
            dtpHoraTotalRealizada.Text = horaTotal.ToString();

            current["IDORDEM"] = lblCodigoOS.Text;
            current["IDSITUACAO"] = cmbCodigoRealizada.Text;
            current["HORA_INICIO"] = dtpHoraInicialRealizada.Text;
            current["HORA_FIM"] = dtpHoraFinalRealizada.Text;
            current["HORA_TOTAL"] = dtpHoraTotalRealizada.Text;
            current["DATA_INICIAL"] = dATA_INICIALDateTimePicker.Text;

            DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;
                       
            if (estadoCorrente == EstadoManutencao.smInserir)
            {
                //currentOS["IDUSUARIO"] = OsConfiguracoes.codigoUsuario;
                currentOS["IDPOSTO"] = OsConfiguracoes.idPosto;
                currentOS["DT_CADASTRO"] = DateTime.Now;
            }
            osOrdemServicoBindingSource.EndEdit();
            osOrdemServicoTableAdapter.Update(dtsPrincipal);

            osSituacoesOSBindingSource.EndEdit();
            osSituacoesOSTableAdapter.Update(dtsPrincipal);

            this.osSituacoesOSTableAdapter.SituacaoRealizada(this.dtsPrincipal.OsSituacoesOS, (int)currentOS["ID"], "R");

            updateHorarioRealizado();
        }

        private void btnInserirSituacaoFinal_Click(object sender, EventArgs e)
        {
            //if (!salvarOrdemServico()) return;
            salvarOrdemServico();
            DataRowView current = (DataRowView)osSituacoesOSBindingSource.AddNew();

            current["IDORDEM"] = lblCodigoOS.Text;
            current["IDSITUACAO"] = cmbCodigoSituacaoFinal.Text;

            DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;

            if (estadoCorrente == EstadoManutencao.smInserir)
            {
                //currentOS["IDUSUARIO"] = OsConfiguracoes.codigoUsuario;
                currentOS["IDPOSTO"] = OsConfiguracoes.idPosto;
                currentOS["DT_CADASTRO"] = DateTime.Now;
            }
            osOrdemServicoBindingSource.EndEdit();
            osOrdemServicoTableAdapter.Update(dtsPrincipal);

            osSituacoesOSBindingSource.EndEdit();
            osSituacoesOSTableAdapter.Update(dtsPrincipal);

            this.osSituacoesOSTableAdapter.SituacaoRealizada(this.dtsPrincipal.OsSituacoesOS, (int)currentOS["ID"], "F");
        }

        private void btnInserirLacres_Click(object sender, EventArgs e)
        {
            //if (!salvarOrdemServico()) return;
            salvarOrdemServico();
            if (cmbCodigoEquipamentoLacres.Text.Length == 0)
            {
                MessageBox.Show("Informe o número de série da bomba.");
                return;
            }

            if (cmbCodigoINMETROLacres.Text.Length == 0)
            {
                MessageBox.Show("Informe o número de INMETRO utilizado.");
                return;
            }

            if (cmbCodigoLacres.Text.Length == 0)
            {
                MessageBox.Show("Informe o número do lacre utilizado.");
                return;
            }

            DataRowView current = (DataRowView)osInstrumentoServicoBindingSource.AddNew();

            current["IDORDEM"] = lblCodigoOS.Text;
            current["IDIBAMETRO"] = cmbCodigoEquipamentoLacres.Text;
            current["IDINMETRO"] = cmbCodigoINMETROLacres.Text;
            current["IDETIQUETA"] = cmbCodigoLacres.Text;

            DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;

            if (estadoCorrente == EstadoManutencao.smInserir)
            {
                //currentOS["IDUSUARIO"] = OsConfiguracoes.codigoUsuario;
                currentOS["IDPOSTO"] = OsConfiguracoes.idPosto;
                currentOS["DT_CADASTRO"] = DateTime.Now;
            }
            osOrdemServicoBindingSource.EndEdit();
            osOrdemServicoTableAdapter.Update(dtsPrincipal);

            osInstrumentoServicoBindingSource.EndEdit();
            osInstrumentoServicoTableAdapter.Update(dtsPrincipal);

            this.osInstrumentoServicoTableAdapter.SqlLacre(this.dtsPrincipal.OsInstrumentoServico, (int)currentOS["ID"]);
            limpaAbaLacres();
        }

        private void cmbCodigoEquipamentoSelos_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbNumSerieSelos.Text.Length > 0 && cmbCodigoEquipamentoSelos.Text != "")
            {
                osIbametroInmetroTableAdapter.SqlFiltraINMETRO(this.dtsPrincipal.OsIbametroInmetro, Int32.Parse(cmbCodigoEquipamentoSelos.Text));
            }
        }

        private void cmbCodigoEquipamentoLacres_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbNumSerieLacres.Text.Length > 0)
            {
                osIbametroInmetroTableAdapter.SqlFiltraINMETRO(this.dtsPrincipal.OsIbametroInmetro, Int32.Parse(cmbCodigoEquipamentoLacres.Text));
            }
        }

        private void cmbNomeCliente_Click(object sender, EventArgs e)
        {
            //bdsClientes.Sort = "NOME";
            /// 22.05.2014 - Uwe
            bdsClientes.Sort = "LOGOTIPO";
        }

        private void cmbCodCliente_Click(object sender, EventArgs e)
        {
            bdsClientes.Sort = "CLiente";
        }

        private void cmbNumSerieSelos_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        

        /// <summary>
        /// 22.05.2014 - Uwe
        /// habilitar a communicação entre Textbox e Combobox
        /// </summary>       
        private void tbCodCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                int valido 
                  = (int)DB.GetSingleValue("select count(*) from a30clientes where cliente='"+tbCodCliente.Text+"'"); 
                if ( valido > 0)
                  cmbCodCliente.SelectedIndex = cmbCodCliente.FindStringExact(tbCodCliente.Text);
                else
                    MessageBox.Show("Não achei o cliente <"
                                    + tbCodCliente.Text
                                    + "> na tabela A30Clientes.","A V I S O",
                                    MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
        }

       
       
      
        
    }
}
