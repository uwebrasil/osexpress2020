﻿namespace osExpress.Manutencoes
{
    partial class ManIbametro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label label11;
            System.Windows.Forms.Label dT_CADASTROLabel3;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label3;
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnInserirMecanico = new System.Windows.Forms.Button();
            this.cmbMarcaInstrumento = new System.Windows.Forms.ComboBox();
            this.cmbCodigoCliente = new System.Windows.Forms.ComboBox();
            this.osIbametroBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.a30CLIENTESBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cmbNomeCliente = new System.Windows.Forms.ComboBox();
            this.dtpDataCadastro = new System.Windows.Forms.DateTimePicker();
            this.txtCodigoIBAMETRO = new System.Windows.Forms.TextBox();
            this.txtNumeroSerie = new System.Windows.Forms.TextBox();
            this.tableAdapterManager = new osExpress.OsExpressDataSetTableAdapters.TableAdapterManager();
            this.osIbametroInmetroTableAdapter = new osExpress.OsExpressDataSetTableAdapters.OsIbametroInmetroTableAdapter();
            this.osIbametroTableAdapter = new osExpress.OsExpressDataSetTableAdapters.OsIbametroTableAdapter();
            this.bindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.tsbInserir = new System.Windows.Forms.ToolStripButton();
            this.tsbGravar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbPrimeiro = new System.Windows.Forms.ToolStripButton();
            this.tsbAnterior = new System.Windows.Forms.ToolStripButton();
            this.tsbProximo = new System.Windows.Forms.ToolStripButton();
            this.tsbUltimo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbImprimir = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnSair = new System.Windows.Forms.ToolStripButton();
            this.bdsConsultaIbametro = new System.Windows.Forms.BindingSource(this.components);
            this.osIbametroInmetroBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.a30CLIENTESTableAdapter = new osExpress.OsExpressDataSetTableAdapters.A30CLIENTESTableAdapter();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtNumINMETRO = new System.Windows.Forms.TextBox();
            this.osIbametroInmetroDataGridView = new System.Windows.Forms.DataGridView();
            this.nUMINMETRODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnInserirINMETRO = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.osIbametroDataGridView = new System.Windows.Forms.DataGridView();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmbEmpresa = new System.Windows.Forms.ComboBox();
            this.lblEmpresa = new System.Windows.Forms.Label();
            this.fKOsInstrumentoServicoOsIbametroBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.osInstrumentoServicoTableAdapter = new osExpress.OsExpressDataSetTableAdapters.OsInstrumentoServicoTableAdapter();
            label11 = new System.Windows.Forms.Label();
            dT_CADASTROLabel3 = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dtsPrincipal)).BeginInit();
            this.tabPrincipal.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.osIbametroBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.a30CLIENTESBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator)).BeginInit();
            this.bindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsConsultaIbametro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.osIbametroInmetroBindingSource)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.osIbametroInmetroDataGridView)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.osIbametroDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKOsInstrumentoServicoOsIbametroBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // grpPrincipal
            // 
            this.grpPrincipal.Location = new System.Drawing.Point(12, 75);
            this.grpPrincipal.Size = new System.Drawing.Size(758, 10);
            this.grpPrincipal.Visible = false;
            // 
            // tabPrincipal
            // 
            this.tabPrincipal.Controls.Add(this.tabPage1);
            this.tabPrincipal.Location = new System.Drawing.Point(12, 141);
            this.tabPrincipal.Size = new System.Drawing.Size(758, 117);
            this.tabPrincipal.TabStop = false;
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label11.Location = new System.Drawing.Point(238, 12);
            label11.Name = "label11";
            label11.Size = new System.Drawing.Size(104, 13);
            label11.TabIndex = 45;
            label11.Text = "Nome do Cliente:";
            // 
            // dT_CADASTROLabel3
            // 
            dT_CADASTROLabel3.AutoSize = true;
            dT_CADASTROLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dT_CADASTROLabel3.Location = new System.Drawing.Point(317, 63);
            dT_CADASTROLabel3.Name = "dT_CADASTROLabel3";
            dT_CADASTROLabel3.Size = new System.Drawing.Size(110, 13);
            dT_CADASTROLabel3.TabIndex = 44;
            dT_CADASTROLabel3.Text = "Data de Cadastro:";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label1.Location = new System.Drawing.Point(29, 11);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(93, 13);
            label1.TabIndex = 43;
            label1.Text = "Código Cliente:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label2.Location = new System.Drawing.Point(6, 37);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(116, 13);
            label2.TabIndex = 41;
            label2.Text = "Marca Instrumento:";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label3.Location = new System.Drawing.Point(27, 64);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(95, 13);
            label3.TabIndex = 42;
            label3.Text = "Número SÉRIE:";
            // 
            // tabPage1
            // 
            this.tabPage1.AutoScroll = true;
            this.tabPage1.Controls.Add(this.btnInserirMecanico);
            this.tabPage1.Controls.Add(this.cmbMarcaInstrumento);
            this.tabPage1.Controls.Add(this.cmbCodigoCliente);
            this.tabPage1.Controls.Add(label11);
            this.tabPage1.Controls.Add(this.cmbNomeCliente);
            this.tabPage1.Controls.Add(dT_CADASTROLabel3);
            this.tabPage1.Controls.Add(this.dtpDataCadastro);
            this.tabPage1.Controls.Add(label1);
            this.tabPage1.Controls.Add(this.txtCodigoIBAMETRO);
            this.tabPage1.Controls.Add(label2);
            this.tabPage1.Controls.Add(label3);
            this.tabPage1.Controls.Add(this.txtNumeroSerie);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(750, 91);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Cadastro";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // btnInserirMecanico
            // 
            this.btnInserirMecanico.Location = new System.Drawing.Point(259, 60);
            this.btnInserirMecanico.Name = "btnInserirMecanico";
            this.btnInserirMecanico.Size = new System.Drawing.Size(0, 22);
            this.btnInserirMecanico.TabIndex = 46;
            this.btnInserirMecanico.Text = "btnInserirMecanico";
            this.btnInserirMecanico.UseVisualStyleBackColor = true;
            this.btnInserirMecanico.Enter += new System.EventHandler(this.btnInserirMecanico_Enter);
            // 
            // cmbMarcaInstrumento
            // 
            this.cmbMarcaInstrumento.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbMarcaInstrumento.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbMarcaInstrumento.FormattingEnabled = true;
            this.cmbMarcaInstrumento.Items.AddRange(new object[] {
            "Daruma",
            "Extratema",
            "GBR Veeder Root",
            "Gilbarco Mecânica",
            "Gilbarco Pro",
            "Gilbarco Pulma Fit",
            "Tokheim",
            "Wayne 3G",
            "Wayne Duplex",
            "Wayne Mecânica",
            "Wayne Minnow"});
            this.cmbMarcaInstrumento.Location = new System.Drawing.Point(128, 34);
            this.cmbMarcaInstrumento.MaxLength = 30;
            this.cmbMarcaInstrumento.Name = "cmbMarcaInstrumento";
            this.cmbMarcaInstrumento.Size = new System.Drawing.Size(151, 21);
            this.cmbMarcaInstrumento.Sorted = true;
            this.cmbMarcaInstrumento.TabIndex = 4;
            // 
            // cmbCodigoCliente
            // 
            this.cmbCodigoCliente.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCodigoCliente.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCodigoCliente.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osIbametroBindingSource, "IDCLIENTE", true));
            this.cmbCodigoCliente.DataSource = this.a30CLIENTESBindingSource;
            this.cmbCodigoCliente.DisplayMember = "Cliente";
            this.cmbCodigoCliente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCodigoCliente.FormattingEnabled = true;
            this.cmbCodigoCliente.Location = new System.Drawing.Point(128, 8);
            this.cmbCodigoCliente.Name = "cmbCodigoCliente";
            this.cmbCodigoCliente.Size = new System.Drawing.Size(100, 21);
            this.cmbCodigoCliente.TabIndex = 2;
            this.cmbCodigoCliente.ValueMember = "ID";
            this.cmbCodigoCliente.SelectedValueChanged += new System.EventHandler(this.comboBox6_SelectedValueChanged);
            // 
            // osIbametroBindingSource
            // 
            this.osIbametroBindingSource.DataMember = "OsIbametro";
            this.osIbametroBindingSource.DataSource = this.dtsPrincipal;
            this.osIbametroBindingSource.Sort = "ID";
            this.osIbametroBindingSource.BindingComplete += new System.Windows.Forms.BindingCompleteEventHandler(this.osIbametroBindingSource_BindingComplete);
            // 
            // a30CLIENTESBindingSource
            // 
            this.a30CLIENTESBindingSource.AllowNew = false;
            this.a30CLIENTESBindingSource.DataMember = "A30CLIENTES";
            this.a30CLIENTESBindingSource.DataSource = this.dtsPrincipal;
            this.a30CLIENTESBindingSource.Filter = "INATIVO = 0 AND TIPO_PESSOA = \'J\'";
            this.a30CLIENTESBindingSource.Sort = "NOME ASC";
            // 
            // cmbNomeCliente
            // 
            this.cmbNomeCliente.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbNomeCliente.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbNomeCliente.DataSource = this.a30CLIENTESBindingSource;
            this.cmbNomeCliente.DisplayMember = "Nome";
            this.cmbNomeCliente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbNomeCliente.FormattingEnabled = true;
            this.cmbNomeCliente.Location = new System.Drawing.Point(348, 8);
            this.cmbNomeCliente.Name = "cmbNomeCliente";
            this.cmbNomeCliente.Size = new System.Drawing.Size(396, 21);
            this.cmbNomeCliente.TabIndex = 3;
            this.cmbNomeCliente.ValueMember = "ID";
            this.cmbNomeCliente.SelectedValueChanged += new System.EventHandler(this.comboBox6_SelectedValueChanged);
            // 
            // dtpDataCadastro
            // 
            this.dtpDataCadastro.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.osIbametroBindingSource, "DT_CADASTRO", true));
            this.dtpDataCadastro.Enabled = false;
            this.dtpDataCadastro.Location = new System.Drawing.Point(433, 61);
            this.dtpDataCadastro.Name = "dtpDataCadastro";
            this.dtpDataCadastro.Size = new System.Drawing.Size(279, 20);
            this.dtpDataCadastro.TabIndex = 1;
            // 
            // txtCodigoIBAMETRO
            // 
            this.txtCodigoIBAMETRO.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osIbametroBindingSource, "ID", true));
            this.txtCodigoIBAMETRO.Enabled = false;
            this.txtCodigoIBAMETRO.Location = new System.Drawing.Point(433, 37);
            this.txtCodigoIBAMETRO.Name = "txtCodigoIBAMETRO";
            this.txtCodigoIBAMETRO.ReadOnly = true;
            this.txtCodigoIBAMETRO.Size = new System.Drawing.Size(0, 20);
            this.txtCodigoIBAMETRO.TabIndex = 0;
            this.txtCodigoIBAMETRO.TabStop = false;
            // 
            // txtNumeroSerie
            // 
            this.txtNumeroSerie.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osIbametroBindingSource, "NUM_SERIE", true));
            this.txtNumeroSerie.Location = new System.Drawing.Point(128, 61);
            this.txtNumeroSerie.MaxLength = 15;
            this.txtNumeroSerie.Name = "txtNumeroSerie";
            this.txtNumeroSerie.Size = new System.Drawing.Size(100, 20);
            this.txtNumeroSerie.TabIndex = 5;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.A30CAIXA_PROG_EXTableAdapter = null;
            this.tableAdapterManager.A30CAIXA_PROGTableAdapter = null;
            this.tableAdapterManager.A30CLIENTESTableAdapter = null;
            this.tableAdapterManager.A30FUNCIONARIOTableAdapter = null;
            this.tableAdapterManager.A30PLANO_CONTATableAdapter = null;
            this.tableAdapterManager.A30POSTOSTableAdapter = null;
            this.tableAdapterManager.A30PRODUTOTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.OsClienteTableAdapter = null;
            this.tableAdapterManager.OsEtiquetaOsClienteTableAdapter = null;
            this.tableAdapterManager.OsEtiquetaTableAdapter = null;
            this.tableAdapterManager.OsFlashDriveTableAdapter = null;
            this.tableAdapterManager.OsFormasPagamentoTableAdapter = null;
            this.tableAdapterManager.OsIbametroInmetroOsClienteTableAdapter = null;
            this.tableAdapterManager.OsIbametroInmetroTableAdapter = this.osIbametroInmetroTableAdapter;
            this.tableAdapterManager.OsIbametroOsClienteTableAdapter = null;
            this.tableAdapterManager.OsIbametroTableAdapter = this.osIbametroTableAdapter;
            this.tableAdapterManager.OsInstrumentoServicoOsClienteTableAdapter = null;
            this.tableAdapterManager.OsInstrumentoServicoTableAdapter = null;
            this.tableAdapterManager.OsItemServicoTableAdapter = null;
            this.tableAdapterManager.OsOrdemParcelaTableAdapter = null;
            this.tableAdapterManager.OsOrdemServicoTableAdapter = null;
            this.tableAdapterManager.OsParametrosTableAdapter = null;
            this.tableAdapterManager.OsParContasTableAdapter = null;
            this.tableAdapterManager.OsParSituacaoTableAdapter = null;
            this.tableAdapterManager.OsProdutosServicosTableAdapter = null;
            this.tableAdapterManager.OsReciboTableAdapter = null;
            this.tableAdapterManager.OsSituacoesOSTableAdapter = null;
            this.tableAdapterManager.OsUsuarioTableAdapter = null;
            this.tableAdapterManager.OsVersaoTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = osExpress.OsExpressDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // osIbametroInmetroTableAdapter
            // 
            this.osIbametroInmetroTableAdapter.ClearBeforeFill = true;
            // 
            // osIbametroTableAdapter
            // 
            this.osIbametroTableAdapter.ClearBeforeFill = true;
            // 
            // bindingNavigator
            // 
            this.bindingNavigator.AddNewItem = this.tsbInserir;
            this.bindingNavigator.BindingSource = this.osIbametroBindingSource;
            this.bindingNavigator.CountItem = null;
            this.bindingNavigator.CountItemFormat = "de {0}";
            this.bindingNavigator.DeleteItem = null;
            this.bindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bindingNavigator.ImageScalingSize = new System.Drawing.Size(48, 48);
            this.bindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbInserir,
            this.tsbGravar,
            this.toolStripSeparator4,
            this.tsbPrimeiro,
            this.tsbAnterior,
            this.tsbProximo,
            this.tsbUltimo,
            this.toolStripSeparator1,
            this.tsbImprimir,
            this.toolStripSeparator2,
            this.btnSair});
            this.bindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator.MoveFirstItem = null;
            this.bindingNavigator.MoveLastItem = null;
            this.bindingNavigator.MoveNextItem = null;
            this.bindingNavigator.MovePreviousItem = null;
            this.bindingNavigator.Name = "bindingNavigator";
            this.bindingNavigator.PositionItem = null;
            this.bindingNavigator.Size = new System.Drawing.Size(784, 70);
            this.bindingNavigator.TabIndex = 0;
            this.bindingNavigator.Text = "bindingNavigator1";
            this.bindingNavigator.RefreshItems += new System.EventHandler(this.bindingNavigator_RefreshItems);
            // 
            // tsbInserir
            // 
            this.tsbInserir.Image = global::osExpress.Properties.Resources.edit2;
            this.tsbInserir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbInserir.Name = "tsbInserir";
            this.tsbInserir.RightToLeftAutoMirrorImage = true;
            this.tsbInserir.Size = new System.Drawing.Size(52, 67);
            this.tsbInserir.Text = "&Incluir";
            this.tsbInserir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbInserir.Click += new System.EventHandler(this.tsbInserir_Click);
            // 
            // tsbGravar
            // 
            this.tsbGravar.Image = global::osExpress.Properties.Resources.save;
            this.tsbGravar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbGravar.Name = "tsbGravar";
            this.tsbGravar.Size = new System.Drawing.Size(52, 67);
            this.tsbGravar.Text = "Salv&ar";
            this.tsbGravar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbGravar.Click += new System.EventHandler(this.tsbGravar_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 70);
            // 
            // tsbPrimeiro
            // 
            this.tsbPrimeiro.Image = global::osExpress.Properties.Resources.first;
            this.tsbPrimeiro.Name = "tsbPrimeiro";
            this.tsbPrimeiro.RightToLeftAutoMirrorImage = true;
            this.tsbPrimeiro.Size = new System.Drawing.Size(56, 67);
            this.tsbPrimeiro.Text = "&Primeiro";
            this.tsbPrimeiro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbPrimeiro.Click += new System.EventHandler(this.tsbPrimeiro_Click);
            // 
            // tsbAnterior
            // 
            this.tsbAnterior.Image = global::osExpress.Properties.Resources.previous;
            this.tsbAnterior.Name = "tsbAnterior";
            this.tsbAnterior.RightToLeftAutoMirrorImage = true;
            this.tsbAnterior.Size = new System.Drawing.Size(54, 67);
            this.tsbAnterior.Text = "Anteri&or";
            this.tsbAnterior.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbAnterior.Click += new System.EventHandler(this.tsbAnterior_Click);
            // 
            // tsbProximo
            // 
            this.tsbProximo.Image = global::osExpress.Properties.Resources.next;
            this.tsbProximo.Name = "tsbProximo";
            this.tsbProximo.RightToLeftAutoMirrorImage = true;
            this.tsbProximo.Size = new System.Drawing.Size(56, 67);
            this.tsbProximo.Text = "Próxi&mo";
            this.tsbProximo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbProximo.Click += new System.EventHandler(this.tsbProximo_Click);
            // 
            // tsbUltimo
            // 
            this.tsbUltimo.Image = global::osExpress.Properties.Resources.last;
            this.tsbUltimo.Name = "tsbUltimo";
            this.tsbUltimo.RightToLeftAutoMirrorImage = true;
            this.tsbUltimo.Size = new System.Drawing.Size(52, 67);
            this.tsbUltimo.Text = "&Último";
            this.tsbUltimo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbUltimo.Click += new System.EventHandler(this.tsbUltimo_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 70);
            // 
            // tsbImprimir
            // 
            this.tsbImprimir.Image = global::osExpress.Properties.Resources.printer;
            this.tsbImprimir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbImprimir.Name = "tsbImprimir";
            this.tsbImprimir.Size = new System.Drawing.Size(57, 67);
            this.tsbImprimir.Text = "Imprimi&r";
            this.tsbImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbImprimir.Click += new System.EventHandler(this.tsbImprimir_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 70);
            // 
            // btnSair
            // 
            this.btnSair.Image = global::osExpress.Properties.Resources.gohome;
            this.btnSair.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(52, 67);
            this.btnSair.Text = "&Sair";
            this.btnSair.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // bdsConsultaIbametro
            // 
            this.bdsConsultaIbametro.DataMember = "OsIbametro";
            this.bdsConsultaIbametro.DataSource = this.dtsPrincipal;
            // 
            // osIbametroInmetroBindingSource
            // 
            this.osIbametroInmetroBindingSource.DataMember = "OsIbametroInmetro";
            this.osIbametroInmetroBindingSource.DataSource = this.dtsPrincipal;
            // 
            // a30CLIENTESTableAdapter
            // 
            this.a30CLIENTESTableAdapter.ClearBeforeFill = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtNumINMETRO);
            this.groupBox1.Controls.Add(this.osIbametroInmetroDataGridView);
            this.groupBox1.Controls.Add(this.btnInserirINMETRO);
            this.groupBox1.Location = new System.Drawing.Point(13, 468);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(169, 164);
            this.groupBox1.TabIndex = 61;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = " INMETRO das Bombas ";
            // 
            // txtNumINMETRO
            // 
            this.txtNumINMETRO.Location = new System.Drawing.Point(7, 19);
            this.txtNumINMETRO.MaxLength = 15;
            this.txtNumINMETRO.Name = "txtNumINMETRO";
            this.txtNumINMETRO.Size = new System.Drawing.Size(156, 20);
            this.txtNumINMETRO.TabIndex = 0;
            // 
            // osIbametroInmetroDataGridView
            // 
            this.osIbametroInmetroDataGridView.AllowUserToAddRows = false;
            this.osIbametroInmetroDataGridView.AutoGenerateColumns = false;
            this.osIbametroInmetroDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.osIbametroInmetroDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nUMINMETRODataGridViewTextBoxColumn});
            this.osIbametroInmetroDataGridView.DataSource = this.osIbametroInmetroBindingSource;
            this.osIbametroInmetroDataGridView.Location = new System.Drawing.Point(6, 45);
            this.osIbametroInmetroDataGridView.Name = "osIbametroInmetroDataGridView";
            this.osIbametroInmetroDataGridView.Size = new System.Drawing.Size(157, 113);
            this.osIbametroInmetroDataGridView.TabIndex = 61;
            this.osIbametroInmetroDataGridView.TabStop = false;
            this.osIbametroInmetroDataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.osIbametroDataGridView_CellEndEdit);
            this.osIbametroInmetroDataGridView.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.osIbametroDataGridView_UserDeletedRow);
            // 
            // nUMINMETRODataGridViewTextBoxColumn
            // 
            this.nUMINMETRODataGridViewTextBoxColumn.DataPropertyName = "NUM_INMETRO";
            this.nUMINMETRODataGridViewTextBoxColumn.HeaderText = "Nº INMETRO";
            this.nUMINMETRODataGridViewTextBoxColumn.Name = "nUMINMETRODataGridViewTextBoxColumn";
            // 
            // btnInserirINMETRO
            // 
            this.btnInserirINMETRO.Location = new System.Drawing.Point(59, 77);
            this.btnInserirINMETRO.Name = "btnInserirINMETRO";
            this.btnInserirINMETRO.Size = new System.Drawing.Size(101, 23);
            this.btnInserirINMETRO.TabIndex = 1;
            this.btnInserirINMETRO.Text = "Inserir INMETRO";
            this.btnInserirINMETRO.UseVisualStyleBackColor = true;
            this.btnInserirINMETRO.Enter += new System.EventHandler(this.btnInserirINMETRO_Enter);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.osIbametroDataGridView);
            this.groupBox3.Location = new System.Drawing.Point(12, 264);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(758, 198);
            this.groupBox3.TabIndex = 62;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = " Bombas do Cliente ";
            // 
            // osIbametroDataGridView
            // 
            this.osIbametroDataGridView.AllowUserToAddRows = false;
            this.osIbametroDataGridView.AutoGenerateColumns = false;
            this.osIbametroDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.osIbametroDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn});
            this.osIbametroDataGridView.DataSource = this.osIbametroBindingSource;
            this.osIbametroDataGridView.Location = new System.Drawing.Point(6, 19);
            this.osIbametroDataGridView.Name = "osIbametroDataGridView";
            this.osIbametroDataGridView.Size = new System.Drawing.Size(746, 173);
            this.osIbametroDataGridView.TabIndex = 13;
            this.osIbametroDataGridView.TabStop = false;
            this.osIbametroDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.osIbametroDataGridView_CellClick);
            this.osIbametroDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.osIbametroDataGridView_CellContentClick);
            this.osIbametroDataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.osIbametroDataGridView_CellEndEdit);
            this.osIbametroDataGridView.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.osIbametroDataGridView_DataError);
            this.osIbametroDataGridView.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.osIbametroDataGridView_UserDeletedRow);
            this.osIbametroDataGridView.KeyUp += new System.Windows.Forms.KeyEventHandler(this.osIbametroDataGridView_KeyUp);
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cmbEmpresa
            // 
            this.cmbEmpresa.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbEmpresa.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbEmpresa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEmpresa.FormattingEnabled = true;
            this.cmbEmpresa.Items.AddRange(new object[] {
            "A - Aberta",
            "F - Fechada",
            "D - Descartada"});
            this.cmbEmpresa.Location = new System.Drawing.Point(90, 115);
            this.cmbEmpresa.Name = "cmbEmpresa";
            this.cmbEmpresa.Size = new System.Drawing.Size(280, 21);
            this.cmbEmpresa.TabIndex = 63;
            this.cmbEmpresa.SelectionChangeCommitted += new System.EventHandler(this.cmbEmpresa_SelectionChangeCommitted);
            // 
            // lblEmpresa
            // 
            this.lblEmpresa.AutoSize = true;
            this.lblEmpresa.Location = new System.Drawing.Point(30, 118);
            this.lblEmpresa.Name = "lblEmpresa";
            this.lblEmpresa.Size = new System.Drawing.Size(51, 13);
            this.lblEmpresa.TabIndex = 64;
            this.lblEmpresa.Text = "Empresa:";
            // 
            // fKOsInstrumentoServicoOsIbametroBindingSource
            // 
            this.fKOsInstrumentoServicoOsIbametroBindingSource.DataMember = "FK_OsInstrumentoServico_OsIbametro";
            this.fKOsInstrumentoServicoOsIbametroBindingSource.DataSource = this.osIbametroBindingSource;
            // 
            // osInstrumentoServicoTableAdapter
            // 
            this.osInstrumentoServicoTableAdapter.ClearBeforeFill = true;
            // 
            // ManIbametro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(784, 607);
            this.Controls.Add(this.lblEmpresa);
            this.Controls.Add(this.cmbEmpresa);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.bindingNavigator);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.MaximizeBox = true;
            this.MinimizeBox = true;
            this.Name = "ManIbametro";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Manutenção de Bombas por Cliente";
            this.Load += new System.EventHandler(this.ManIbametro_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ManIbametro_KeyDown);
            this.Controls.SetChildIndex(this.tabPrincipal, 0);
            this.Controls.SetChildIndex(this.bindingNavigator, 0);
            this.Controls.SetChildIndex(this.grpPrincipal, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.groupBox3, 0);
            this.Controls.SetChildIndex(this.cmbEmpresa, 0);
            this.Controls.SetChildIndex(this.lblEmpresa, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dtsPrincipal)).EndInit();
            this.tabPrincipal.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.osIbametroBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.a30CLIENTESBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator)).EndInit();
            this.bindingNavigator.ResumeLayout(false);
            this.bindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsConsultaIbametro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.osIbametroInmetroBindingSource)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.osIbametroInmetroDataGridView)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.osIbametroDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKOsInstrumentoServicoOsIbametroBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage1;
        private OsExpressDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator bindingNavigator;
        private System.Windows.Forms.ToolStripButton tsbInserir;
        private System.Windows.Forms.ToolStripButton tsbGravar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton tsbPrimeiro;
        private System.Windows.Forms.ToolStripButton tsbAnterior;
        private System.Windows.Forms.ToolStripButton tsbProximo;
        private System.Windows.Forms.ToolStripButton tsbUltimo;
        private System.Windows.Forms.BindingSource osIbametroBindingSource;
        private OsExpressDataSetTableAdapters.OsIbametroTableAdapter osIbametroTableAdapter;
        private System.Windows.Forms.BindingSource osIbametroInmetroBindingSource;
        private OsExpressDataSetTableAdapters.OsIbametroInmetroTableAdapter osIbametroInmetroTableAdapter;
        private System.Windows.Forms.ComboBox cmbCodigoCliente;
        private System.Windows.Forms.ComboBox cmbNomeCliente;
        private System.Windows.Forms.DateTimePicker dtpDataCadastro;
        private System.Windows.Forms.TextBox txtCodigoIBAMETRO;
        private System.Windows.Forms.TextBox txtNumeroSerie;
        private System.Windows.Forms.ComboBox cmbMarcaInstrumento;
        private System.Windows.Forms.BindingSource a30CLIENTESBindingSource;
        private OsExpressDataSetTableAdapters.A30CLIENTESTableAdapter a30CLIENTESTableAdapter;
        private System.Windows.Forms.Button btnInserirMecanico;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtNumINMETRO;
        private System.Windows.Forms.Button btnInserirINMETRO;
        private System.Windows.Forms.DataGridView osIbametroInmetroDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn nUMINMETRODataGridViewTextBoxColumn;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView osIbametroDataGridView;
        private System.Windows.Forms.BindingSource bdsConsultaIbametro;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnSair;
        private System.Windows.Forms.ToolStripButton tsbImprimir;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ComboBox cmbEmpresa;
        private System.Windows.Forms.Label lblEmpresa;
        private System.Windows.Forms.BindingSource fKOsInstrumentoServicoOsIbametroBindingSource;
        private OsExpressDataSetTableAdapters.OsInstrumentoServicoTableAdapter osInstrumentoServicoTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
    }
}
