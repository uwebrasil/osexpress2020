﻿using System;
using System.Data;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using USBDriveSerialNumber;
using System.Xml;
using System.Text;
using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using CREtiqueta;

namespace osExpress.Manutencoes
{
    public partial class ManEtiqueta : osExpress.FrmPrincipal
    {
        int ultimoCliente = -1;

        bool cellClicked = false;

        public ManEtiqueta()
        {
            InitializeComponent();         
        }

        private void estadoInicial()
        {
            // *** Controles *** //

            tsbInserir.Enabled = true;
            tsbGravar.Enabled = false; 

            cmbCodigoFuncionario.SelectedIndex = -1;
            cmbNomeFuncionario.SelectedIndex = -1;
            //cmbMarcaInstrumento.SelectedIndex = -1;
            osEtiquetaBindingSource.Filter = "ID = 0";
            pesquisar();
            cmbNomeFuncionario.Focus();
            // Begin Modificação 
            // Uwe Kristmann 13.02.2014
            // desativar os controles sequintes 
            // até tiver escolhido funcionário
            //MessageBox.Show("debug 1");
            cmbTipoEtiqueta.Enabled = false;
            cmbSituacaoEtiquetas.Enabled = false;
            nUM_ETIQUETATextBox.Enabled = false;
            tsbInserir.Enabled = false;
            // End Modificação
        }

        private void ManIbametro_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dtsPrincipal.OsEtiqueta' table. You can move, or remove it, as needed.
            this.osEtiquetaTableAdapter.Fill(this.dtsPrincipal.OsEtiqueta);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsUsuario' table. You can move, or remove it, as needed.
            this.osUsuarioTableAdapter.Fill(this.dtsPrincipal.OsUsuario);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsUsuario' table. You can move, or remove it, as needed.
            this.osUsuarioTableAdapter.Fill(this.dtsPrincipal.OsUsuario);
            //
            this.WindowState = FormWindowState.Maximized;

            estadoInicial();

            //pesquisar();
        }

        private void tsbGravar_Click(object sender, EventArgs e)
        {
            osEtiquetaBindingSource.CancelEdit();
            // *** Controles *** //
            tsbInserir.Enabled = true;
            tsbGravar.Enabled = false;
            tsbPrimeiro.Enabled = true;
            tsbAnterior.Enabled = true;
            tsbProximo.Enabled = true;
            tsbUltimo.Enabled = true;
            // ***************** //

            cmbCodigoFuncionario.SelectedIndex = ultimoCliente;
            cmbNomeFuncionario.SelectedIndex = ultimoCliente;
            estadoCorrente = EstadoManutencao.smPesquisar;
            try
            {
                this.Validate();
                this.osEtiquetaBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(this.dtsPrincipal);
                cmbNomeFuncionario.Enabled = true;
                cmbCodigoFuncionario.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                cancelar();
                // Modificação 
                // Uwe Kristmann 13.02.2014            
                // possibiltar a gravação de novo depois ter excluido a linha dupla
                // *** Controles *** //
                /*
                if (osEtiquetaDataGridView.RowCount > 0)
                {
                    tsbInserir.Enabled = false;
                    tsbGravar.Enabled = true;
                    tsbPrimeiro.Enabled = false;
                    tsbAnterior.Enabled = false;
                    tsbProximo.Enabled = false;
                    tsbUltimo.Enabled = false;
                }
                else
                    cancelar();
                 * */
                // ***************** //
            }
        }

        private void tsbInserir_Click(object sender, EventArgs e)
        {
            inserir();

            cmbTipoEtiqueta.Focus();

            tsbGravar.Enabled = true;

            cmbCodigoFuncionario.SelectedIndex = ultimoCliente;
            cmbNomeFuncionario.SelectedIndex = ultimoCliente;
            cmbNomeFuncionario.Enabled = false;
            cmbCodigoFuncionario.Enabled = false;

            tsbInserir.Enabled = false;
        }

        private void ManIbametro_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }

        private void btnInserirMecanico_Enter(object sender, EventArgs e)
        {
            tsbInserir.Enabled = true; 
            //tsbGravar.PerformClick();
            try
            {
                DataRowView current = (DataRowView)osEtiquetaBindingSource.Current;
                if (estadoCorrente == EstadoManutencao.smInserir)
                {
                    current["TIPO"] = cmbTipoEtiqueta.Text.Substring(0, 1);
                    current["IDUSUARIO"] = "15";
                }
                tsbInserir.PerformClick();
            }
            // Begin Modificação 
            // Uwe Kristmann 14.02.2014 
            catch (Exception ex) 
            {
                MessageBox.Show(ex.Message,
               "A V I S O",
               MessageBoxButtons.OK,
               MessageBoxIcon.Exclamation);
            }
            // End Modificação
        }

        private void comboBox6_SelectedValueChanged(object sender, EventArgs e)
        {
            string situacao = "";
            switch (cmbSituacaoEtiquetas.SelectedIndex)
            {
                case 0:
                {
                    situacao = "";
                    break;
                }
                case 1:
                {
                    situacao = " AND UTILIZADO = 1 ";
                    break;
                }
                case 2:
                {
                    situacao = " AND UTILIZADO = 0";
                    break;
                }
                default: break;
            }

            if (/*cmbNomeCliente.Text.Length > 0 ||*/ cmbCodigoFuncionario.Text.Length > 0)
            {
                ultimoCliente = cmbCodigoFuncionario.SelectedIndex;
                ultimoCliente = cmbNomeFuncionario.SelectedIndex;
                try
                {
                    if (estadoCorrente == EstadoManutencao.smInserir)
                    {
                        osEtiquetaBindingSource.Filter = "IDUSUARIO = " + cmbCodigoFuncionario.SelectedValue.ToString() + " AND ID > " + osEtiquetaTableAdapter.SqlUltimaEtiqueta(Int32.Parse(cmbNomeFuncionario.SelectedValue.ToString())) + situacao;
                    }
                    else
                    {
                        osEtiquetaBindingSource.Filter = "IDUSUARIO = " + cmbCodigoFuncionario.SelectedValue.ToString() + situacao;
                    }
                }
                catch (Exception)
                {

                }

                if (ultimoCliente >= 0)
                {
                    cmbCodigoFuncionario.SelectedIndex = ultimoCliente;
                    cmbNomeFuncionario.SelectedIndex = ultimoCliente;
                }
            }
         }

        private void tsbPrimeiro_Click(object sender, EventArgs e)
        {
            cancelarAlteracoes();
            osUsuarioBindingSource.MoveFirst();
        }

        private void tsbAnterior_Click(object sender, EventArgs e)
        {
            cancelarAlteracoes();
            osUsuarioBindingSource.MovePrevious();
        }

        private void tsbProximo_Click(object sender, EventArgs e)
        {
            cancelarAlteracoes();
            osUsuarioBindingSource.MoveNext();
        }

        private void tsbUltimo_Click(object sender, EventArgs e)
        {
            cancelarAlteracoes();
            osUsuarioBindingSource.MoveLast();
        }

        private void osIbametroDataGridView_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            tsbGravar.Enabled = true;
            excluir();
        }

        private void tsbExcluir_Click(object sender, EventArgs e)
        {
            excluir();
        }

        private void cancelar()
        {
            cmbNomeFuncionario.Enabled = true;
            cmbCodigoFuncionario.Enabled = true;
            osEtiquetaBindingSource.CancelEdit();
            dtsPrincipal.RejectChanges();
            pesquisar();
            tsbInserir.Enabled = true;
            tsbGravar.Enabled = false;
        }

        private void cancelarAlteracoes()
        {
            if (estadoCorrente == EstadoManutencao.smInserir ||
                estadoCorrente == EstadoManutencao.smExcluir ||
                estadoCorrente == EstadoManutencao.smEditar)
            {
                if (MessageBox.Show("Deseja abandonar sem gravar?",
                    null,
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    cancelar();
                }
            }
        }

        private void osIbametroDataGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            cancelar();
        }

        private void osIbametroDataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            tsbInserir.Enabled = false;
            tsbGravar.Enabled = true;
            editar();
        }

        private void osIbametroInmetroDataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            tsbInserir.Enabled = false;
            tsbGravar.Enabled = true;
            editar();
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void tsbImprimir_Click(object sender, EventArgs e)
        {
           // RelEtiquetas relEtiquetas = new RelEtiquetas();
           // relEtiquetas.Show();
           RelEtiqueta2 relEtiquetas = new RelEtiqueta2();
           relEtiquetas.Show();
        }

        private void osEtiquetaDataGridView_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            tsbGravar.Enabled = true;
            excluir();
        }

        private void osEtiquetaDataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            tsbInserir.Enabled = false;
            tsbGravar.Enabled = true;
            editar();
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
        }

        private void pnlCancelarAtribuicao_Click(object sender, EventArgs e)
        {
            pnlAtribuir.Visible = false;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            pnlAtribuir.Visible = true;
        }

        private void btnAtribuicao_Click(object sender, EventArgs e)
        {
            int erros = 0;
            if (osEtiquetaDataGridView.SelectedCells.Count > 0)
            {
                // Essa rotina é responsável faz a chamada da procedure de fechamento do caixa,
                for (int i = 0; i < osEtiquetaDataGridView.SelectedCells.Count; i++)
                {
                    var connectionString = ConfigurationManager.ConnectionStrings["osExpress.Properties.Settings.OsExpressConnectionString"].ConnectionString;
                    SqlConnection conn = new SqlConnection(connectionString);
                    SqlCommand cmd = new SqlCommand("dbo.osPrcAtribuiSeloLacre", conn);

                    cmd.CommandType = CommandType.StoredProcedure;

                    int selectedrowindex = osEtiquetaDataGridView.SelectedCells[i].RowIndex;

                    DataGridViewRow selectedRow = osEtiquetaDataGridView.Rows[selectedrowindex];

                    string idEtiqueta = Convert.ToString(selectedRow.Cells["ID"].Value);

                    SqlParameter param1 = new SqlParameter("@USUARIO", int.Parse(cmbFuncionarioAtribuir.SelectedValue.ToString()));
                    SqlParameter param2 = new SqlParameter("@ETIQUETA", int.Parse(idEtiqueta));

                    param1.Direction = ParameterDirection.Input;
                    param1.DbType = DbType.Int32;
                    param2.Direction = ParameterDirection.Input;
                    param2.DbType = DbType.Int32;

                    cmd.Parameters.Add(param1);
                    cmd.Parameters.Add(param2);

                    cmd.Connection.Open();

                    if (cmd.ExecuteNonQuery() > 0)
                        erros = erros + 1;

                    cmd.Connection.Close();
                }

                if (erros == osEtiquetaDataGridView.SelectedCells.Count)
                {
                    MessageBox.Show("Selos / Lacres foram atribuidos com sucesso!");
                    this.osEtiquetaTableAdapter.Fill(this.dtsPrincipal.OsEtiqueta);
                }

                pnlAtribuir.Visible = false;
            }
            // Begin Modificação 
            // Uwe Kristmann 13.02.2014 
            else 
            {
                MessageBox.Show("Não tem nada a atribuir!",
                "A V I S O",
                MessageBoxButtons.OK,
                MessageBoxIcon.Exclamation); 
            }
            // End Modificação
                
        }
        // Modificação 
        // Uwe Kristmann 13.02.2014            
        // ativar os controles seguintes depois tiver escolhido um funcionário
        private void cmbNomeFuncionario_SelectedIndexChanged(object sender, EventArgs e)
        {           
            cmbTipoEtiqueta.Enabled = true;
            cmbSituacaoEtiquetas.Enabled = true;
            nUM_ETIQUETATextBox.Enabled = true;
            tsbInserir.Enabled = true;
        }
        // Modificação 
        // Uwe Kristmann 14.02.2014 
        // cria vinculo ao comboBox cmbTipoEtiqueta
        private void osEtiquetaDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {            
            // coluna "Tipo"
            //string val = osEtiquetaDataGridView.Rows[e.RowIndex].Cells[1].Value.ToString();
            //cellClicked = true;
            //cmbTipoEtiqueta.SelectedIndex = cmbTipoEtiqueta.FindString(val);            
        }
        // Modificação 
        // Uwe Kristmann 14.02.2014 
        // sincronizar o ComboBox cmbTipoEtiqueta com o ClickEvent 
        // ( mostrar o selo/lacre da linha selecionada )
        private void cmbTipoEtiqueta_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*
            if (cellClicked) 
            {
                cellClicked = !cellClicked;
                return;
            }
            string funcionario = cmbCodigoFuncionario.Text;
            string tipo = ((ComboBox)sender).SelectedItem.ToString().Substring(0,1);
            string filter = "IDUSUARIO = "+ funcionario;
            if (tipo.Length > 0)
                filter += " AND TIPO='" +tipo+"'";
            osEtiquetaBindingSource.Filter = filter;           
             */
        }

       

    }
} 
