﻿namespace osExpress.Manutencoes
{
    partial class ManParOS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label iDLabel2;
            System.Windows.Forms.Label nOMELabel1;
            System.Windows.Forms.Label sENHALabel;
            System.Windows.Forms.Label dT_CADASTROLabel2;
            System.Windows.Forms.Label label7;
            System.Windows.Forms.Label iDLabel5;
            System.Windows.Forms.Label nOMELabel3;
            System.Windows.Forms.Label iDCONTALabel;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label iDLabel;
            System.Windows.Forms.Label nOMELabel;
            System.Windows.Forms.Label dESCRICAOLabel;
            System.Windows.Forms.Label tIPO_SITUACAOLabel;
            System.Windows.Forms.Label dT_CADASTROLabel;
            System.Windows.Forms.Label tIPO_ACESSOLabel;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label9;
            System.Windows.Forms.Label label8;
            System.Windows.Forms.Label iDFUNCIONARIOLabel;
            this.a30CLIENTESBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bdsFuncionario = new System.Windows.Forms.BindingSource(this.components);
            this.bindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.tsbInserir = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.tsbExcluir = new System.Windows.Forms.ToolStripButton();
            this.tsbGravar = new System.Windows.Forms.ToolStripButton();
            this.tsbCancelar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbPrimeiro = new System.Windows.Forms.ToolStripButton();
            this.tsbAnterior = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbProximo = new System.Windows.Forms.ToolStripButton();
            this.tsbUltimo = new System.Windows.Forms.ToolStripButton();
            this.bdsFiltroUsuario = new System.Windows.Forms.BindingSource(this.components);
            this.FiltroUsuario = new System.Windows.Forms.BindingSource(this.components);
            this.bdsFiltroCliente = new System.Windows.Forms.BindingSource(this.components);
            this.osUsuarioBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tbpUsuarios = new System.Windows.Forms.TabPage();
            this.cbPosto002 = new System.Windows.Forms.CheckBox();
            this.cbPosto001 = new System.Windows.Forms.CheckBox();
            this.osUsuarioDataGridView = new System.Windows.Forms.DataGridView();
            this.pnlAdmnistrador1 = new System.Windows.Forms.Panel();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.a30PLANO_CONTABindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.contaComboBox = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.pnlAdmnistrador2 = new System.Windows.Forms.Panel();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.aCESSA_PEDIDOCheckBox = new System.Windows.Forms.CheckBox();
            this.cmbTipoAcesso = new System.Windows.Forms.ComboBox();
            this.lblSenhaDigitos = new System.Windows.Forms.Label();
            this.lblSenhaNaoConfere = new System.Windows.Forms.Label();
            this.txtSenha = new System.Windows.Forms.TextBox();
            this.iDTextBox5 = new System.Windows.Forms.TextBox();
            this.nOMETextBox3 = new System.Windows.Forms.TextBox();
            this.txtConfirmaSenha = new System.Windows.Forms.TextBox();
            this.dT_CADASTRODateTimePicker5 = new System.Windows.Forms.DateTimePicker();
            this.osParSituacaoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.osParSituacaoTableAdapter = new osExpress.OsExpressDataSetTableAdapters.OsParSituacaoTableAdapter();
            this.a30CLIENTESTableAdapter = new osExpress.OsExpressDataSetTableAdapters.A30CLIENTESTableAdapter();
            this.osUsuarioTableAdapter = new osExpress.OsExpressDataSetTableAdapters.OsUsuarioTableAdapter();
            this.a30FUNCIONARIOTableAdapter = new osExpress.OsExpressDataSetTableAdapters.A30FUNCIONARIOTableAdapter();
            this.tableAdapterManager = new osExpress.OsExpressDataSetTableAdapters.TableAdapterManager();
            this.osParametrosTableAdapter = new osExpress.OsExpressDataSetTableAdapters.OsParametrosTableAdapter();
            this.osParContasTableAdapter = new osExpress.OsExpressDataSetTableAdapters.OsParContasTableAdapter();
            this.osProdutosServicosTableAdapter = new osExpress.OsExpressDataSetTableAdapters.OsProdutosServicosTableAdapter();
            this.a30PLANO_CONTATableAdapter = new osExpress.OsExpressDataSetTableAdapters.A30PLANO_CONTATableAdapter();
            this.tbpFormPag = new System.Windows.Forms.TabPage();
            this.label26 = new System.Windows.Forms.Label();
            this.comboBox7 = new System.Windows.Forms.ComboBox();
            this.osParContasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bdsPlanoContaFormaPag = new System.Windows.Forms.BindingSource(this.components);
            this.osParContasDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.iDTextBox1 = new System.Windows.Forms.TextBox();
            this.nOMETextBox1 = new System.Windows.Forms.TextBox();
            this.tabParametrizacao = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.comboBox34 = new System.Windows.Forms.ComboBox();
            this.osProdutosServicosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bdsPlanoFiado = new System.Windows.Forms.BindingSource(this.components);
            this.comboBox35 = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.comboBox44 = new System.Windows.Forms.ComboBox();
            this.bdsPlanoVendas = new System.Windows.Forms.BindingSource(this.components);
            this.comboBox45 = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.comboBox24 = new System.Windows.Forms.ComboBox();
            this.bdsPlanoPedido = new System.Windows.Forms.BindingSource(this.components);
            this.comboBox25 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox26 = new System.Windows.Forms.ComboBox();
            this.bdsPlanoTranslado = new System.Windows.Forms.BindingSource(this.components);
            this.comboBox27 = new System.Windows.Forms.ComboBox();
            this.comboBox28 = new System.Windows.Forms.ComboBox();
            this.bdsPlanoHospedagem = new System.Windows.Forms.BindingSource(this.components);
            this.comboBox29 = new System.Windows.Forms.ComboBox();
            this.comboBox30 = new System.Windows.Forms.ComboBox();
            this.bdsPlanoAlimentacao = new System.Windows.Forms.BindingSource(this.components);
            this.comboBox31 = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.comboBox15 = new System.Windows.Forms.ComboBox();
            this.bdsPlanoOutros = new System.Windows.Forms.BindingSource(this.components);
            this.comboBox14 = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.comboBox22 = new System.Windows.Forms.ComboBox();
            this.bdsProdutoReembolso = new System.Windows.Forms.BindingSource(this.components);
            this.comboBox23 = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.comboBox32 = new System.Windows.Forms.ComboBox();
            this.bdsProdutoOutros = new System.Windows.Forms.BindingSource(this.components);
            this.comboBox33 = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.comboBox16 = new System.Windows.Forms.ComboBox();
            this.bdsProdutoKM = new System.Windows.Forms.BindingSource(this.components);
            this.comboBox17 = new System.Windows.Forms.ComboBox();
            this.bdsProdutoRede = new System.Windows.Forms.BindingSource(this.components);
            this.comboBox18 = new System.Windows.Forms.ComboBox();
            this.bdsProdutoTaxa = new System.Windows.Forms.BindingSource(this.components);
            this.comboBox19 = new System.Windows.Forms.ComboBox();
            this.bdsProdutoExcedente = new System.Windows.Forms.BindingSource(this.components);
            this.comboBox20 = new System.Windows.Forms.ComboBox();
            this.bdsProdutoDeslocamento = new System.Windows.Forms.BindingSource(this.components);
            this.comboBox21 = new System.Windows.Forms.ComboBox();
            this.bdsProdutoVisita = new System.Windows.Forms.BindingSource(this.components);
            this.comboBox11 = new System.Windows.Forms.ComboBox();
            this.comboBox12 = new System.Windows.Forms.ComboBox();
            this.comboBox13 = new System.Windows.Forms.ComboBox();
            this.comboBox10 = new System.Windows.Forms.ComboBox();
            this.comboBox9 = new System.Windows.Forms.ComboBox();
            this.comboBox8 = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.bdsPlanoContaRec = new System.Windows.Forms.BindingSource(this.components);
            this.a30PRODUTOTableAdapter = new osExpress.OsExpressDataSetTableAdapters.A30PRODUTOTableAdapter();
            this.bdsPlanoOutrosServicos = new System.Windows.Forms.BindingSource(this.components);
            this.tbpSituacoes = new System.Windows.Forms.TabPage();
            this.cmbTipoSituacao = new System.Windows.Forms.ComboBox();
            this.txtCodigoSituacao = new System.Windows.Forms.TextBox();
            this.txtNomeSituacao = new System.Windows.Forms.TextBox();
            this.txtDescricaoSituacao = new System.Windows.Forms.TextBox();
            this.dtpDataCadastroSituacao = new System.Windows.Forms.DateTimePicker();
            this.grdSituacao = new System.Windows.Forms.DataGridView();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nOMEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dESCRICAODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tIPOSITUACAODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dTCADASTRODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.eMAIL_PEDIDOTextBox = new System.Windows.Forms.TextBox();
            this.osParametrosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.eMAIL_ORDEMTextBox = new System.Windows.Forms.TextBox();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            iDLabel2 = new System.Windows.Forms.Label();
            nOMELabel1 = new System.Windows.Forms.Label();
            sENHALabel = new System.Windows.Forms.Label();
            dT_CADASTROLabel2 = new System.Windows.Forms.Label();
            label7 = new System.Windows.Forms.Label();
            iDLabel5 = new System.Windows.Forms.Label();
            nOMELabel3 = new System.Windows.Forms.Label();
            iDCONTALabel = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            iDLabel = new System.Windows.Forms.Label();
            nOMELabel = new System.Windows.Forms.Label();
            dESCRICAOLabel = new System.Windows.Forms.Label();
            tIPO_SITUACAOLabel = new System.Windows.Forms.Label();
            dT_CADASTROLabel = new System.Windows.Forms.Label();
            tIPO_ACESSOLabel = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label9 = new System.Windows.Forms.Label();
            label8 = new System.Windows.Forms.Label();
            iDFUNCIONARIOLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dtsPrincipal)).BeginInit();
            this.grpPrincipal.SuspendLayout();
            this.tabPrincipal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.a30CLIENTESBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsFuncionario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator)).BeginInit();
            this.bindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsFiltroUsuario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FiltroUsuario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsFiltroCliente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.osUsuarioBindingSource)).BeginInit();
            this.tbpUsuarios.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.osUsuarioDataGridView)).BeginInit();
            this.pnlAdmnistrador1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.a30PLANO_CONTABindingSource)).BeginInit();
            this.pnlAdmnistrador2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.osParSituacaoBindingSource)).BeginInit();
            this.tbpFormPag.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.osParContasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPlanoContaFormaPag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.osParContasDataGridView)).BeginInit();
            this.tabParametrizacao.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.osProdutosServicosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPlanoFiado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPlanoVendas)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPlanoPedido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPlanoTranslado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPlanoHospedagem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPlanoAlimentacao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPlanoOutros)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsProdutoReembolso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsProdutoOutros)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsProdutoKM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsProdutoRede)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsProdutoTaxa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsProdutoExcedente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsProdutoDeslocamento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsProdutoVisita)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPlanoContaRec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPlanoOutrosServicos)).BeginInit();
            this.tbpSituacoes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdSituacao)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.osParametrosBindingSource)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpPrincipal
            // 
            this.grpPrincipal.Controls.Add(this.label3);
            this.grpPrincipal.Controls.Add(this.label2);
            this.grpPrincipal.Controls.Add(this.label1);
            this.grpPrincipal.Visible = false;
            // 
            // tabPrincipal
            // 
            this.tabPrincipal.Controls.Add(this.tbpSituacoes);
            this.tabPrincipal.Controls.Add(this.tbpUsuarios);
            this.tabPrincipal.Controls.Add(this.tbpFormPag);
            this.tabPrincipal.Controls.Add(this.tabParametrizacao);
            this.tabPrincipal.Controls.Add(this.tabPage2);
            this.tabPrincipal.Location = new System.Drawing.Point(12, 87);
            this.tabPrincipal.Size = new System.Drawing.Size(760, 522);
            this.tabPrincipal.TabIndex = 0;
            this.tabPrincipal.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabPrincipal_Selected);
            // 
            // iDLabel2
            // 
            iDLabel2.AutoSize = true;
            iDLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            iDLabel2.Location = new System.Drawing.Point(63, 13);
            iDLabel2.Name = "iDLabel2";
            iDLabel2.Size = new System.Drawing.Size(50, 13);
            iDLabel2.TabIndex = 16;
            iDLabel2.Text = "Código:";
            // 
            // nOMELabel1
            // 
            nOMELabel1.AutoSize = true;
            nOMELabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            nOMELabel1.Location = new System.Drawing.Point(5, 92);
            nOMELabel1.Name = "nOMELabel1";
            nOMELabel1.Size = new System.Drawing.Size(108, 13);
            nOMELabel1.TabIndex = 22;
            nOMELabel1.Text = "Nome do Usuário:";
            // 
            // sENHALabel
            // 
            sENHALabel.AutoSize = true;
            sENHALabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            sENHALabel.Location = new System.Drawing.Point(8, 142);
            sENHALabel.Name = "sENHALabel";
            sENHALabel.Size = new System.Drawing.Size(104, 13);
            sENHALabel.TabIndex = 24;
            sENHALabel.Text = "Confirmar Senha:";
            // 
            // dT_CADASTROLabel2
            // 
            dT_CADASTROLabel2.AutoSize = true;
            dT_CADASTROLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dT_CADASTROLabel2.Location = new System.Drawing.Point(307, 13);
            dT_CADASTROLabel2.Name = "dT_CADASTROLabel2";
            dT_CADASTROLabel2.Size = new System.Drawing.Size(110, 13);
            dT_CADASTROLabel2.TabIndex = 28;
            dT_CADASTROLabel2.Text = "Data de Cadastro:";
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label7.Location = new System.Drawing.Point(66, 118);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(47, 13);
            label7.TabIndex = 38;
            label7.Text = "Senha:";
            // 
            // iDLabel5
            // 
            iDLabel5.AutoSize = true;
            iDLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            iDLabel5.Location = new System.Drawing.Point(196, 20);
            iDLabel5.Name = "iDLabel5";
            iDLabel5.Size = new System.Drawing.Size(50, 13);
            iDLabel5.TabIndex = 8;
            iDLabel5.Text = "Código:";
            // 
            // nOMELabel3
            // 
            nOMELabel3.AutoSize = true;
            nOMELabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            nOMELabel3.Location = new System.Drawing.Point(203, 46);
            nOMELabel3.Name = "nOMELabel3";
            nOMELabel3.Size = new System.Drawing.Size(43, 13);
            nOMELabel3.TabIndex = 10;
            nOMELabel3.Text = "Nome:";
            // 
            // iDCONTALabel
            // 
            iDCONTALabel.AutoSize = true;
            iDCONTALabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            iDCONTALabel.Location = new System.Drawing.Point(10, 68);
            iDCONTALabel.Name = "iDCONTALabel";
            iDCONTALabel.Size = new System.Drawing.Size(236, 13);
            iDCONTALabel.TabIndex = 12;
            iDCONTALabel.Text = "Conta Referente ao Tipo de Pagamento:";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label6.Location = new System.Drawing.Point(358, 68);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(123, 13);
            label6.TabIndex = 14;
            label6.Text = "Descrição da Conta:";
            // 
            // iDLabel
            // 
            iDLabel.AutoSize = true;
            iDLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            iDLabel.Location = new System.Drawing.Point(68, 11);
            iDLabel.Name = "iDLabel";
            iDLabel.Size = new System.Drawing.Size(50, 13);
            iDLabel.TabIndex = 25;
            iDLabel.Text = "Código:";
            // 
            // nOMELabel
            // 
            nOMELabel.AutoSize = true;
            nOMELabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            nOMELabel.Location = new System.Drawing.Point(75, 37);
            nOMELabel.Name = "nOMELabel";
            nOMELabel.Size = new System.Drawing.Size(43, 13);
            nOMELabel.TabIndex = 26;
            nOMELabel.Text = "Nome:";
            // 
            // dESCRICAOLabel
            // 
            dESCRICAOLabel.AutoSize = true;
            dESCRICAOLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dESCRICAOLabel.Location = new System.Drawing.Point(50, 63);
            dESCRICAOLabel.Name = "dESCRICAOLabel";
            dESCRICAOLabel.Size = new System.Drawing.Size(68, 13);
            dESCRICAOLabel.TabIndex = 27;
            dESCRICAOLabel.Text = "Descrição:";
            // 
            // tIPO_SITUACAOLabel
            // 
            tIPO_SITUACAOLabel.AutoSize = true;
            tIPO_SITUACAOLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tIPO_SITUACAOLabel.Location = new System.Drawing.Point(10, 89);
            tIPO_SITUACAOLabel.Name = "tIPO_SITUACAOLabel";
            tIPO_SITUACAOLabel.Size = new System.Drawing.Size(108, 13);
            tIPO_SITUACAOLabel.TabIndex = 28;
            tIPO_SITUACAOLabel.Text = "Tipo da Situação:";
            // 
            // dT_CADASTROLabel
            // 
            dT_CADASTROLabel.AutoSize = true;
            dT_CADASTROLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dT_CADASTROLabel.Location = new System.Drawing.Point(332, 12);
            dT_CADASTROLabel.Name = "dT_CADASTROLabel";
            dT_CADASTROLabel.Size = new System.Drawing.Size(110, 13);
            dT_CADASTROLabel.TabIndex = 29;
            dT_CADASTROLabel.Text = "Data de Cadastro:";
            // 
            // tIPO_ACESSOLabel
            // 
            tIPO_ACESSOLabel.AutoSize = true;
            tIPO_ACESSOLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tIPO_ACESSOLabel.Location = new System.Drawing.Point(10, 7);
            tIPO_ACESSOLabel.Name = "tIPO_ACESSOLabel";
            tIPO_ACESSOLabel.Size = new System.Drawing.Size(99, 13);
            tIPO_ACESSOLabel.TabIndex = 54;
            tIPO_ACESSOLabel.Text = "Tipo de Acesso:";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label4.Location = new System.Drawing.Point(217, 5);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(131, 13);
            label4.TabIndex = 56;
            label4.Text = "Nome do Funcionário:";
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label9.Location = new System.Drawing.Point(226, 32);
            label9.Name = "label9";
            label9.Size = new System.Drawing.Size(122, 13);
            label9.TabIndex = 55;
            label9.Text = "Descrição do Plano:";
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label8.Location = new System.Drawing.Point(11, 32);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(98, 13);
            label8.TabIndex = 54;
            label8.Text = "Plano de Conta:";
            // 
            // iDFUNCIONARIOLabel
            // 
            iDFUNCIONARIOLabel.AutoSize = true;
            iDFUNCIONARIOLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            iDFUNCIONARIOLabel.Location = new System.Drawing.Point(1, 5);
            iDFUNCIONARIOLabel.Name = "iDFUNCIONARIOLabel";
            iDFUNCIONARIOLabel.Size = new System.Drawing.Size(110, 13);
            iDFUNCIONARIOLabel.TabIndex = 53;
            iDFUNCIONARIOLabel.Text = "Id do Funcionario:";
            // 
            // a30CLIENTESBindingSource
            // 
            this.a30CLIENTESBindingSource.DataMember = "A30CLIENTES";
            this.a30CLIENTESBindingSource.DataSource = this.dtsPrincipal;
            this.a30CLIENTESBindingSource.Filter = "INATIVO = \'0\'";
            this.a30CLIENTESBindingSource.Sort = "NOME";
            // 
            // bdsFuncionario
            // 
            this.bdsFuncionario.DataMember = "A30FUNCIONARIO";
            this.bdsFuncionario.DataSource = this.dtsPrincipal;
            this.bdsFuncionario.Sort = "Nome";
            // 
            // bindingNavigator
            // 
            this.bindingNavigator.AddNewItem = this.tsbInserir;
            this.bindingNavigator.CountItem = this.toolStripLabel1;
            this.bindingNavigator.CountItemFormat = "de {0}";
            this.bindingNavigator.DeleteItem = this.tsbExcluir;
            this.bindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bindingNavigator.ImageScalingSize = new System.Drawing.Size(48, 48);
            this.bindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbInserir,
            this.tsbExcluir,
            this.tsbGravar,
            this.tsbCancelar,
            this.toolStripSeparator4,
            this.tsbPrimeiro,
            this.tsbAnterior,
            this.toolStripSeparator1,
            this.toolStripTextBox1,
            this.toolStripLabel1,
            this.toolStripSeparator2,
            this.tsbProximo,
            this.tsbUltimo});
            this.bindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator.MoveFirstItem = this.tsbPrimeiro;
            this.bindingNavigator.MoveLastItem = this.tsbUltimo;
            this.bindingNavigator.MoveNextItem = this.tsbProximo;
            this.bindingNavigator.MovePreviousItem = this.tsbAnterior;
            this.bindingNavigator.Name = "bindingNavigator";
            this.bindingNavigator.PositionItem = this.toolStripTextBox1;
            this.bindingNavigator.Size = new System.Drawing.Size(784, 70);
            this.bindingNavigator.TabIndex = 8;
            this.bindingNavigator.Text = "bindingNavigator1";
            // 
            // tsbInserir
            // 
            this.tsbInserir.Image = global::osExpress.Properties.Resources.insert;
            this.tsbInserir.Name = "tsbInserir";
            this.tsbInserir.RightToLeftAutoMirrorImage = true;
            this.tsbInserir.Size = new System.Drawing.Size(52, 67);
            this.tsbInserir.Text = "&Inserir";
            this.tsbInserir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbInserir.Click += new System.EventHandler(this.tsbInserir_Click);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(37, 67);
            this.toolStripLabel1.Text = "de {0}";
            this.toolStripLabel1.ToolTipText = "Total number of items";
            // 
            // tsbExcluir
            // 
            this.tsbExcluir.Image = global::osExpress.Properties.Resources.remove;
            this.tsbExcluir.Name = "tsbExcluir";
            this.tsbExcluir.RightToLeftAutoMirrorImage = true;
            this.tsbExcluir.Size = new System.Drawing.Size(52, 67);
            this.tsbExcluir.Text = "E&xcluir";
            this.tsbExcluir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbExcluir.Click += new System.EventHandler(this.tsbExcluir_Click);
            // 
            // tsbGravar
            // 
            this.tsbGravar.Image = global::osExpress.Properties.Resources.save;
            this.tsbGravar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbGravar.Name = "tsbGravar";
            this.tsbGravar.Size = new System.Drawing.Size(52, 67);
            this.tsbGravar.Text = "Grav&ar";
            this.tsbGravar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbGravar.Click += new System.EventHandler(this.tsbGravar_Click);
            // 
            // tsbCancelar
            // 
            this.tsbCancelar.Image = global::osExpress.Properties.Resources.clean;
            this.tsbCancelar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbCancelar.Name = "tsbCancelar";
            this.tsbCancelar.Size = new System.Drawing.Size(57, 67);
            this.tsbCancelar.Text = "Cancelar";
            this.tsbCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbCancelar.Click += new System.EventHandler(this.tsbCancelar_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 70);
            // 
            // tsbPrimeiro
            // 
            this.tsbPrimeiro.Image = global::osExpress.Properties.Resources.first;
            this.tsbPrimeiro.Name = "tsbPrimeiro";
            this.tsbPrimeiro.RightToLeftAutoMirrorImage = true;
            this.tsbPrimeiro.Size = new System.Drawing.Size(56, 67);
            this.tsbPrimeiro.Text = "&Primeiro";
            this.tsbPrimeiro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // tsbAnterior
            // 
            this.tsbAnterior.Image = global::osExpress.Properties.Resources.previous;
            this.tsbAnterior.Name = "tsbAnterior";
            this.tsbAnterior.RightToLeftAutoMirrorImage = true;
            this.tsbAnterior.Size = new System.Drawing.Size(54, 67);
            this.tsbAnterior.Text = "Anteri&or";
            this.tsbAnterior.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 70);
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.AccessibleName = "Position";
            this.toolStripTextBox1.AutoSize = false;
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(50, 23);
            this.toolStripTextBox1.Text = "0";
            this.toolStripTextBox1.ToolTipText = "Current position";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 70);
            // 
            // tsbProximo
            // 
            this.tsbProximo.Image = global::osExpress.Properties.Resources.next;
            this.tsbProximo.Name = "tsbProximo";
            this.tsbProximo.RightToLeftAutoMirrorImage = true;
            this.tsbProximo.Size = new System.Drawing.Size(55, 67);
            this.tsbProximo.Text = "Próxi&mo";
            this.tsbProximo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // tsbUltimo
            // 
            this.tsbUltimo.Image = global::osExpress.Properties.Resources.last;
            this.tsbUltimo.Name = "tsbUltimo";
            this.tsbUltimo.RightToLeftAutoMirrorImage = true;
            this.tsbUltimo.Size = new System.Drawing.Size(52, 67);
            this.tsbUltimo.Text = "&Último";
            this.tsbUltimo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // bdsFiltroUsuario
            // 
            this.bdsFiltroUsuario.DataMember = "OsUsuario";
            this.bdsFiltroUsuario.DataSource = this.dtsPrincipal;
            this.bdsFiltroUsuario.Sort = "NOME";
            // 
            // FiltroUsuario
            // 
            this.FiltroUsuario.DataMember = "OsUsuario";
            this.FiltroUsuario.DataSource = this.dtsPrincipal;
            this.FiltroUsuario.Sort = "NOME";
            // 
            // bdsFiltroCliente
            // 
            this.bdsFiltroCliente.DataMember = "A30CLIENTES";
            this.bdsFiltroCliente.DataSource = this.dtsPrincipal;
            this.bdsFiltroCliente.Sort = "NOME";
            // 
            // osUsuarioBindingSource
            // 
            this.osUsuarioBindingSource.DataMember = "OsUsuario";
            this.osUsuarioBindingSource.DataSource = this.dtsPrincipal;
            this.osUsuarioBindingSource.Sort = "NOME";
            this.osUsuarioBindingSource.CurrentChanged += new System.EventHandler(this.osUsuarioBindingSource_CurrentChanged);
            // 
            // tbpUsuarios
            // 
            this.tbpUsuarios.AutoScroll = true;
            this.tbpUsuarios.Controls.Add(this.cbPosto002);
            this.tbpUsuarios.Controls.Add(this.cbPosto001);
            this.tbpUsuarios.Controls.Add(this.osUsuarioDataGridView);
            this.tbpUsuarios.Controls.Add(this.pnlAdmnistrador1);
            this.tbpUsuarios.Controls.Add(this.pnlAdmnistrador2);
            this.tbpUsuarios.Controls.Add(this.lblSenhaDigitos);
            this.tbpUsuarios.Controls.Add(this.lblSenhaNaoConfere);
            this.tbpUsuarios.Controls.Add(label7);
            this.tbpUsuarios.Controls.Add(this.txtSenha);
            this.tbpUsuarios.Controls.Add(iDLabel2);
            this.tbpUsuarios.Controls.Add(this.iDTextBox5);
            this.tbpUsuarios.Controls.Add(nOMELabel1);
            this.tbpUsuarios.Controls.Add(this.nOMETextBox3);
            this.tbpUsuarios.Controls.Add(sENHALabel);
            this.tbpUsuarios.Controls.Add(this.txtConfirmaSenha);
            this.tbpUsuarios.Controls.Add(dT_CADASTROLabel2);
            this.tbpUsuarios.Controls.Add(this.dT_CADASTRODateTimePicker5);
            this.tbpUsuarios.Location = new System.Drawing.Point(4, 22);
            this.tbpUsuarios.Name = "tbpUsuarios";
            this.tbpUsuarios.Padding = new System.Windows.Forms.Padding(3);
            this.tbpUsuarios.Size = new System.Drawing.Size(752, 496);
            this.tbpUsuarios.TabIndex = 3;
            this.tbpUsuarios.Text = "Usuários";
            this.tbpUsuarios.UseVisualStyleBackColor = true;
            // 
            // cbPosto002
            // 
            this.cbPosto002.AutoSize = true;
            this.cbPosto002.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPosto002.Location = new System.Drawing.Point(436, 142);
            this.cbPosto002.Name = "cbPosto002";
            this.cbPosto002.Size = new System.Drawing.Size(69, 17);
            this.cbPosto002.TabIndex = 50;
            this.cbPosto002.Text = "Posto 2";
            this.cbPosto002.UseVisualStyleBackColor = true;
            // 
            // cbPosto001
            // 
            this.cbPosto001.AutoSize = true;
            this.cbPosto001.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPosto001.Location = new System.Drawing.Point(436, 118);
            this.cbPosto001.Name = "cbPosto001";
            this.cbPosto001.Size = new System.Drawing.Size(69, 17);
            this.cbPosto001.TabIndex = 49;
            this.cbPosto001.Text = "Posto 1";
            this.cbPosto001.UseVisualStyleBackColor = true;
            // 
            // osUsuarioDataGridView
            // 
            this.osUsuarioDataGridView.AllowUserToAddRows = false;
            this.osUsuarioDataGridView.AutoGenerateColumns = false;
            this.osUsuarioDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.osUsuarioDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewCheckBoxColumn1,
            this.dataGridViewCheckBoxColumn2});
            this.osUsuarioDataGridView.DataSource = this.osUsuarioBindingSource;
            this.osUsuarioDataGridView.Location = new System.Drawing.Point(3, 224);
            this.osUsuarioDataGridView.Name = "osUsuarioDataGridView";
            this.osUsuarioDataGridView.Size = new System.Drawing.Size(743, 266);
            this.osUsuarioDataGridView.TabIndex = 48;
            this.osUsuarioDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.osUsuarioDataGridView_CellClick);
            // 
            // pnlAdmnistrador1
            // 
            this.pnlAdmnistrador1.Controls.Add(this.comboBox4);
            this.pnlAdmnistrador1.Controls.Add(this.contaComboBox);
            this.pnlAdmnistrador1.Controls.Add(this.comboBox2);
            this.pnlAdmnistrador1.Controls.Add(label4);
            this.pnlAdmnistrador1.Controls.Add(this.comboBox3);
            this.pnlAdmnistrador1.Controls.Add(label9);
            this.pnlAdmnistrador1.Controls.Add(label8);
            this.pnlAdmnistrador1.Controls.Add(iDFUNCIONARIOLabel);
            this.pnlAdmnistrador1.Location = new System.Drawing.Point(3, 31);
            this.pnlAdmnistrador1.Name = "pnlAdmnistrador1";
            this.pnlAdmnistrador1.Size = new System.Drawing.Size(719, 52);
            this.pnlAdmnistrador1.TabIndex = 2;
            // 
            // comboBox4
            // 
            this.comboBox4.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox4.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox4.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osUsuarioBindingSource, "PLANO_CONTA", true));
            this.comboBox4.DataSource = this.a30PLANO_CONTABindingSource;
            this.comboBox4.DisplayMember = "Descricao";
            this.comboBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(354, 29);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(328, 21);
            this.comboBox4.TabIndex = 3;
            this.comboBox4.ValueMember = "ID";
            // 
            // a30PLANO_CONTABindingSource
            // 
            this.a30PLANO_CONTABindingSource.DataMember = "A30PLANO_CONTA";
            this.a30PLANO_CONTABindingSource.DataSource = this.dtsPrincipal;
            this.a30PLANO_CONTABindingSource.Filter = "TIPO = \'R\'";
            this.a30PLANO_CONTABindingSource.Sort = "Descricao";
            // 
            // contaComboBox
            // 
            this.contaComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.contaComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.contaComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osUsuarioBindingSource, "PLANO_CONTA", true));
            this.contaComboBox.DataSource = this.a30PLANO_CONTABindingSource;
            this.contaComboBox.DisplayMember = "Conta";
            this.contaComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.contaComboBox.FormattingEnabled = true;
            this.contaComboBox.Location = new System.Drawing.Point(115, 29);
            this.contaComboBox.Name = "contaComboBox";
            this.contaComboBox.Size = new System.Drawing.Size(91, 21);
            this.contaComboBox.TabIndex = 2;
            this.contaComboBox.ValueMember = "ID";
            // 
            // comboBox2
            // 
            this.comboBox2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox2.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osUsuarioBindingSource, "ID", true));
            this.comboBox2.DataSource = this.bdsFuncionario;
            this.comboBox2.DisplayMember = "Nome";
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(354, 2);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(294, 21);
            this.comboBox2.TabIndex = 1;
            this.comboBox2.ValueMember = "ID";
            // 
            // comboBox3
            // 
            this.comboBox3.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox3.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox3.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osUsuarioBindingSource, "IDFUNCIONARIO", true));
            this.comboBox3.DataSource = this.bdsFuncionario;
            this.comboBox3.DisplayMember = "Funcionario";
            this.comboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(115, 2);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(91, 21);
            this.comboBox3.TabIndex = 0;
            this.comboBox3.ValueMember = "ID";
            // 
            // pnlAdmnistrador2
            // 
            this.pnlAdmnistrador2.Controls.Add(this.checkBox1);
            this.pnlAdmnistrador2.Controls.Add(this.aCESSA_PEDIDOCheckBox);
            this.pnlAdmnistrador2.Controls.Add(this.cmbTipoAcesso);
            this.pnlAdmnistrador2.Controls.Add(tIPO_ACESSOLabel);
            this.pnlAdmnistrador2.Location = new System.Drawing.Point(3, 162);
            this.pnlAdmnistrador2.Name = "pnlAdmnistrador2";
            this.pnlAdmnistrador2.Size = new System.Drawing.Size(414, 56);
            this.pnlAdmnistrador2.TabIndex = 6;
            // 
            // checkBox1
            // 
            this.checkBox1.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.osUsuarioBindingSource, "DIGITA_SELO_LACRE", true));
            this.checkBox1.Location = new System.Drawing.Point(229, 31);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(160, 24);
            this.checkBox1.TabIndex = 2;
            this.checkBox1.Text = "Digita selos e lacres na OS?";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // aCESSA_PEDIDOCheckBox
            // 
            this.aCESSA_PEDIDOCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.osUsuarioBindingSource, "ACESSA_PEDIDO", true));
            this.aCESSA_PEDIDOCheckBox.Location = new System.Drawing.Point(116, 31);
            this.aCESSA_PEDIDOCheckBox.Name = "aCESSA_PEDIDOCheckBox";
            this.aCESSA_PEDIDOCheckBox.Size = new System.Drawing.Size(104, 24);
            this.aCESSA_PEDIDOCheckBox.TabIndex = 1;
            this.aCESSA_PEDIDOCheckBox.Text = "Acessa Pedido?";
            this.aCESSA_PEDIDOCheckBox.UseVisualStyleBackColor = true;
            // 
            // cmbTipoAcesso
            // 
            this.cmbTipoAcesso.FormattingEnabled = true;
            this.cmbTipoAcesso.Items.AddRange(new object[] {
            "A - Administrador",
            "F - Funcionário"});
            this.cmbTipoAcesso.Location = new System.Drawing.Point(115, 4);
            this.cmbTipoAcesso.Name = "cmbTipoAcesso";
            this.cmbTipoAcesso.Size = new System.Drawing.Size(103, 21);
            this.cmbTipoAcesso.TabIndex = 0;
            // 
            // lblSenhaDigitos
            // 
            this.lblSenhaDigitos.AutoSize = true;
            this.lblSenhaDigitos.ForeColor = System.Drawing.Color.Red;
            this.lblSenhaDigitos.Location = new System.Drawing.Point(235, 118);
            this.lblSenhaDigitos.Name = "lblSenhaDigitos";
            this.lblSenhaDigitos.Size = new System.Drawing.Size(22, 13);
            this.lblSenhaDigitos.TabIndex = 48;
            this.lblSenhaDigitos.Text = "(...)";
            this.lblSenhaDigitos.Visible = false;
            // 
            // lblSenhaNaoConfere
            // 
            this.lblSenhaNaoConfere.AutoSize = true;
            this.lblSenhaNaoConfere.ForeColor = System.Drawing.Color.Red;
            this.lblSenhaNaoConfere.Location = new System.Drawing.Point(235, 142);
            this.lblSenhaNaoConfere.Name = "lblSenhaNaoConfere";
            this.lblSenhaNaoConfere.Size = new System.Drawing.Size(22, 13);
            this.lblSenhaNaoConfere.TabIndex = 47;
            this.lblSenhaNaoConfere.Text = "(...)";
            this.lblSenhaNaoConfere.Visible = false;
            // 
            // txtSenha
            // 
            this.txtSenha.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osUsuarioBindingSource, "SENHA", true));
            this.txtSenha.Location = new System.Drawing.Point(118, 113);
            this.txtSenha.MaxLength = 15;
            this.txtSenha.Name = "txtSenha";
            this.txtSenha.PasswordChar = '*';
            this.txtSenha.Size = new System.Drawing.Size(111, 20);
            this.txtSenha.TabIndex = 4;
            this.txtSenha.Validated += new System.EventHandler(this.txtSenha_Validated);
            // 
            // iDTextBox5
            // 
            this.iDTextBox5.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osUsuarioBindingSource, "ID", true));
            this.iDTextBox5.Location = new System.Drawing.Point(119, 7);
            this.iDTextBox5.Name = "iDTextBox5";
            this.iDTextBox5.ReadOnly = true;
            this.iDTextBox5.Size = new System.Drawing.Size(50, 20);
            this.iDTextBox5.TabIndex = 0;
            // 
            // nOMETextBox3
            // 
            this.nOMETextBox3.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osUsuarioBindingSource, "NOME", true));
            this.nOMETextBox3.Location = new System.Drawing.Point(118, 87);
            this.nOMETextBox3.MaxLength = 50;
            this.nOMETextBox3.Name = "nOMETextBox3";
            this.nOMETextBox3.Size = new System.Drawing.Size(296, 20);
            this.nOMETextBox3.TabIndex = 3;
            this.nOMETextBox3.Validated += new System.EventHandler(this.TplValidaCampoTexto);
            // 
            // txtConfirmaSenha
            // 
            this.txtConfirmaSenha.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osUsuarioBindingSource, "SENHA", true));
            this.txtConfirmaSenha.Location = new System.Drawing.Point(118, 139);
            this.txtConfirmaSenha.MaxLength = 15;
            this.txtConfirmaSenha.Name = "txtConfirmaSenha";
            this.txtConfirmaSenha.PasswordChar = '*';
            this.txtConfirmaSenha.Size = new System.Drawing.Size(111, 20);
            this.txtConfirmaSenha.TabIndex = 5;
            this.txtConfirmaSenha.Validated += new System.EventHandler(this.txtConfirmaSenha_Validated);
            // 
            // dT_CADASTRODateTimePicker5
            // 
            this.dT_CADASTRODateTimePicker5.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.osUsuarioBindingSource, "DT_CADASTRO", true));
            this.dT_CADASTRODateTimePicker5.Enabled = false;
            this.dT_CADASTRODateTimePicker5.Location = new System.Drawing.Point(422, 7);
            this.dT_CADASTRODateTimePicker5.Name = "dT_CADASTRODateTimePicker5";
            this.dT_CADASTRODateTimePicker5.Size = new System.Drawing.Size(300, 20);
            this.dT_CADASTRODateTimePicker5.TabIndex = 1;
            // 
            // osParSituacaoBindingSource
            // 
            this.osParSituacaoBindingSource.DataMember = "OsParSituacao";
            this.osParSituacaoBindingSource.DataSource = this.dtsPrincipal;
            this.osParSituacaoBindingSource.Sort = "NOME";
            this.osParSituacaoBindingSource.CurrentChanged += new System.EventHandler(this.osParSituacaoBindingSource_CurrentChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "label2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "label3";
            // 
            // osParSituacaoTableAdapter
            // 
            this.osParSituacaoTableAdapter.ClearBeforeFill = true;
            // 
            // a30CLIENTESTableAdapter
            // 
            this.a30CLIENTESTableAdapter.ClearBeforeFill = true;
            // 
            // osUsuarioTableAdapter
            // 
            this.osUsuarioTableAdapter.ClearBeforeFill = true;
            // 
            // a30FUNCIONARIOTableAdapter
            // 
            this.a30FUNCIONARIOTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.A30CAIXA_PROG_EXTableAdapter = null;
            this.tableAdapterManager.A30CAIXA_PROGTableAdapter = null;
            this.tableAdapterManager.A30CLIENTESTableAdapter = this.a30CLIENTESTableAdapter;
            this.tableAdapterManager.A30FUNCIONARIOTableAdapter = this.a30FUNCIONARIOTableAdapter;
            this.tableAdapterManager.A30PLANO_CONTATableAdapter = null;
            this.tableAdapterManager.A30POSTOSTableAdapter = null;
            this.tableAdapterManager.A30PRODUTOTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.OsClienteTableAdapter = null;
            this.tableAdapterManager.OsEtiquetaTableAdapter = null;
            this.tableAdapterManager.OsFlashDriveTableAdapter = null;
            this.tableAdapterManager.OsFormasPagamentoTableAdapter = null;
            this.tableAdapterManager.OsIbametroInmetroTableAdapter = null;
            this.tableAdapterManager.OsIbametroTableAdapter = null;
            this.tableAdapterManager.OsInstrumentoServicoTableAdapter = null;
            this.tableAdapterManager.OsItemServicoTableAdapter = null;
            this.tableAdapterManager.OsOrdemParcelaTableAdapter = null;
            this.tableAdapterManager.OsOrdemServicoTableAdapter = null;
            this.tableAdapterManager.OsParametrosTableAdapter = this.osParametrosTableAdapter;
            this.tableAdapterManager.OsParContasTableAdapter = this.osParContasTableAdapter;
            this.tableAdapterManager.OsParSituacaoTableAdapter = this.osParSituacaoTableAdapter;
            this.tableAdapterManager.OsProdutosServicosTableAdapter = this.osProdutosServicosTableAdapter;
            this.tableAdapterManager.OsReciboTableAdapter = null;
            this.tableAdapterManager.OsSituacoesOSTableAdapter = null;
            this.tableAdapterManager.OsUsuarioTableAdapter = this.osUsuarioTableAdapter;
            this.tableAdapterManager.OsVersaoTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = osExpress.OsExpressDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // osParametrosTableAdapter
            // 
            this.osParametrosTableAdapter.ClearBeforeFill = true;
            // 
            // osParContasTableAdapter
            // 
            this.osParContasTableAdapter.ClearBeforeFill = true;
            // 
            // osProdutosServicosTableAdapter
            // 
            this.osProdutosServicosTableAdapter.ClearBeforeFill = true;
            // 
            // a30PLANO_CONTATableAdapter
            // 
            this.a30PLANO_CONTATableAdapter.ClearBeforeFill = true;
            // 
            // tbpFormPag
            // 
            this.tbpFormPag.AutoScroll = true;
            this.tbpFormPag.Controls.Add(this.label26);
            this.tbpFormPag.Controls.Add(label6);
            this.tbpFormPag.Controls.Add(this.comboBox7);
            this.tbpFormPag.Controls.Add(this.osParContasDataGridView);
            this.tbpFormPag.Controls.Add(this.comboBox5);
            this.tbpFormPag.Controls.Add(iDLabel5);
            this.tbpFormPag.Controls.Add(this.iDTextBox1);
            this.tbpFormPag.Controls.Add(nOMELabel3);
            this.tbpFormPag.Controls.Add(this.nOMETextBox1);
            this.tbpFormPag.Controls.Add(iDCONTALabel);
            this.tbpFormPag.Location = new System.Drawing.Point(4, 22);
            this.tbpFormPag.Name = "tbpFormPag";
            this.tbpFormPag.Padding = new System.Windows.Forms.Padding(3);
            this.tbpFormPag.Size = new System.Drawing.Size(752, 496);
            this.tbpFormPag.TabIndex = 6;
            this.tbpFormPag.Text = "Formas Pagamento";
            this.tbpFormPag.UseVisualStyleBackColor = true;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Red;
            this.label26.Location = new System.Drawing.Point(505, 3);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(241, 13);
            this.label26.TabIndex = 15;
            this.label26.Text = "Não crie a conta FIADO, parametrize ela.";
            // 
            // comboBox7
            // 
            this.comboBox7.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox7.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox7.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osParContasBindingSource, "IDCONTA", true));
            this.comboBox7.DataSource = this.bdsPlanoContaFormaPag;
            this.comboBox7.DisplayMember = "Descricao";
            this.comboBox7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox7.FormattingEnabled = true;
            this.comboBox7.Location = new System.Drawing.Point(487, 65);
            this.comboBox7.Name = "comboBox7";
            this.comboBox7.Size = new System.Drawing.Size(259, 21);
            this.comboBox7.TabIndex = 13;
            this.comboBox7.ValueMember = "ID";
            // 
            // osParContasBindingSource
            // 
            this.osParContasBindingSource.DataMember = "OsParContas";
            this.osParContasBindingSource.DataSource = this.dtsPrincipal;
            // 
            // bdsPlanoContaFormaPag
            // 
            this.bdsPlanoContaFormaPag.DataMember = "A30PLANO_CONTA";
            this.bdsPlanoContaFormaPag.DataSource = this.dtsPrincipal;
            this.bdsPlanoContaFormaPag.Filter = "TIPO = \'N\'  AND APRESENTAR = 1";
            // 
            // osParContasDataGridView
            // 
            this.osParContasDataGridView.AllowUserToAddRows = false;
            this.osParContasDataGridView.AllowUserToDeleteRows = false;
            this.osParContasDataGridView.AutoGenerateColumns = false;
            this.osParContasDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.osParContasDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            this.osParContasDataGridView.DataSource = this.osParContasBindingSource;
            this.osParContasDataGridView.Location = new System.Drawing.Point(6, 92);
            this.osParContasDataGridView.Name = "osParContasDataGridView";
            this.osParContasDataGridView.ReadOnly = true;
            this.osParContasDataGridView.Size = new System.Drawing.Size(740, 398);
            this.osParContasDataGridView.TabIndex = 3;
            this.osParContasDataGridView.Enter += new System.EventHandler(this.tsbGravar_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "ID";
            this.dataGridViewTextBoxColumn1.HeaderText = "Código";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "NOME";
            this.dataGridViewTextBoxColumn2.HeaderText = "Nome";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "IDCONTA";
            this.dataGridViewTextBoxColumn3.HeaderText = "Conta";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // comboBox5
            // 
            this.comboBox5.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox5.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox5.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osParContasBindingSource, "IDCONTA", true));
            this.comboBox5.DataSource = this.bdsPlanoContaFormaPag;
            this.comboBox5.DisplayMember = "Conta";
            this.comboBox5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Location = new System.Drawing.Point(252, 65);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(100, 21);
            this.comboBox5.TabIndex = 2;
            this.comboBox5.ValueMember = "ID";
            // 
            // iDTextBox1
            // 
            this.iDTextBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osParContasBindingSource, "ID", true));
            this.iDTextBox1.Enabled = false;
            this.iDTextBox1.Location = new System.Drawing.Point(252, 13);
            this.iDTextBox1.Name = "iDTextBox1";
            this.iDTextBox1.Size = new System.Drawing.Size(66, 20);
            this.iDTextBox1.TabIndex = 0;
            // 
            // nOMETextBox1
            // 
            this.nOMETextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.nOMETextBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osParContasBindingSource, "NOME", true));
            this.nOMETextBox1.Location = new System.Drawing.Point(252, 39);
            this.nOMETextBox1.Name = "nOMETextBox1";
            this.nOMETextBox1.Size = new System.Drawing.Size(100, 20);
            this.nOMETextBox1.TabIndex = 1;
            // 
            // tabParametrizacao
            // 
            this.tabParametrizacao.AutoScroll = true;
            this.tabParametrizacao.Controls.Add(this.groupBox3);
            this.tabParametrizacao.Controls.Add(this.groupBox2);
            this.tabParametrizacao.Controls.Add(this.groupBox1);
            this.tabParametrizacao.Location = new System.Drawing.Point(4, 22);
            this.tabParametrizacao.Name = "tabParametrizacao";
            this.tabParametrizacao.Padding = new System.Windows.Forms.Padding(3);
            this.tabParametrizacao.Size = new System.Drawing.Size(752, 496);
            this.tabParametrizacao.TabIndex = 7;
            this.tabParametrizacao.Text = "Parametrização";
            this.tabParametrizacao.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.comboBox34);
            this.groupBox3.Controls.Add(this.comboBox35);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Controls.Add(this.comboBox44);
            this.groupBox3.Controls.Add(this.comboBox45);
            this.groupBox3.Controls.Add(this.label30);
            this.groupBox3.Location = new System.Drawing.Point(6, 411);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(740, 73);
            this.groupBox3.TabIndex = 75;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = " Lançamentos de Caixa ";
            // 
            // comboBox34
            // 
            this.comboBox34.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox34.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox34.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osProdutosServicosBindingSource, "FIADO", true));
            this.comboBox34.DataSource = this.bdsPlanoFiado;
            this.comboBox34.DisplayMember = "Descricao";
            this.comboBox34.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox34.FormattingEnabled = true;
            this.comboBox34.Location = new System.Drawing.Point(288, 19);
            this.comboBox34.Name = "comboBox34";
            this.comboBox34.Size = new System.Drawing.Size(446, 21);
            this.comboBox34.TabIndex = 91;
            this.comboBox34.ValueMember = "ID";
            // 
            // osProdutosServicosBindingSource
            // 
            this.osProdutosServicosBindingSource.DataMember = "OsProdutosServicos";
            this.osProdutosServicosBindingSource.DataSource = this.dtsPrincipal;
            // 
            // bdsPlanoFiado
            // 
            this.bdsPlanoFiado.DataMember = "A30PLANO_CONTA";
            this.bdsPlanoFiado.DataSource = this.dtsPrincipal;
            this.bdsPlanoFiado.Filter = "TIPO = \'N\' AND APRESENTAR = 1";
            // 
            // comboBox35
            // 
            this.comboBox35.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox35.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox35.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osProdutosServicosBindingSource, "FIADO", true));
            this.comboBox35.DataSource = this.bdsPlanoFiado;
            this.comboBox35.DisplayMember = "Conta";
            this.comboBox35.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox35.FormattingEnabled = true;
            this.comboBox35.Location = new System.Drawing.Point(182, 19);
            this.comboBox35.Name = "comboBox35";
            this.comboBox35.Size = new System.Drawing.Size(100, 21);
            this.comboBox35.TabIndex = 90;
            this.comboBox35.ValueMember = "ID";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(140, 22);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(36, 13);
            this.label22.TabIndex = 92;
            this.label22.Text = "Fiado:";
            // 
            // comboBox44
            // 
            this.comboBox44.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox44.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox44.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osProdutosServicosBindingSource, "VENDAS", true));
            this.comboBox44.DataSource = this.bdsPlanoVendas;
            this.comboBox44.DisplayMember = "Descricao";
            this.comboBox44.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox44.FormattingEnabled = true;
            this.comboBox44.Location = new System.Drawing.Point(288, 46);
            this.comboBox44.Name = "comboBox44";
            this.comboBox44.Size = new System.Drawing.Size(446, 21);
            this.comboBox44.TabIndex = 1;
            this.comboBox44.ValueMember = "ID";
            // 
            // bdsPlanoVendas
            // 
            this.bdsPlanoVendas.DataMember = "A30PLANO_CONTA";
            this.bdsPlanoVendas.DataSource = this.dtsPrincipal;
            this.bdsPlanoVendas.Filter = "TIPO = \'N\' AND APRESENTAR = 1";
            // 
            // comboBox45
            // 
            this.comboBox45.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox45.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox45.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osProdutosServicosBindingSource, "VENDAS", true));
            this.comboBox45.DataSource = this.bdsPlanoVendas;
            this.comboBox45.DisplayMember = "Conta";
            this.comboBox45.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox45.FormattingEnabled = true;
            this.comboBox45.Location = new System.Drawing.Point(182, 46);
            this.comboBox45.Name = "comboBox45";
            this.comboBox45.Size = new System.Drawing.Size(100, 21);
            this.comboBox45.TabIndex = 0;
            this.comboBox45.ValueMember = "ID";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(130, 50);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(46, 13);
            this.label30.TabIndex = 80;
            this.label30.Text = "Vendas:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.comboBox24);
            this.groupBox2.Controls.Add(this.comboBox25);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.comboBox26);
            this.groupBox2.Controls.Add(this.comboBox27);
            this.groupBox2.Controls.Add(this.comboBox28);
            this.groupBox2.Controls.Add(this.comboBox29);
            this.groupBox2.Controls.Add(this.comboBox30);
            this.groupBox2.Controls.Add(this.comboBox31);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this.label25);
            this.groupBox2.Controls.Add(this.comboBox15);
            this.groupBox2.Controls.Add(this.comboBox14);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Location = new System.Drawing.Point(6, 253);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(740, 152);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = " Planos de Contas ";
            // 
            // comboBox24
            // 
            this.comboBox24.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox24.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox24.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osProdutosServicosBindingSource, "PEDIDO", true));
            this.comboBox24.DataSource = this.bdsPlanoPedido;
            this.comboBox24.DisplayMember = "Descricao";
            this.comboBox24.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox24.FormattingEnabled = true;
            this.comboBox24.Location = new System.Drawing.Point(288, 125);
            this.comboBox24.Name = "comboBox24";
            this.comboBox24.Size = new System.Drawing.Size(446, 21);
            this.comboBox24.TabIndex = 91;
            this.comboBox24.ValueMember = "ID";
            // 
            // bdsPlanoPedido
            // 
            this.bdsPlanoPedido.DataMember = "A30PLANO_CONTA";
            this.bdsPlanoPedido.DataSource = this.dtsPrincipal;
            this.bdsPlanoPedido.Filter = "TIPO = \'R\'";
            // 
            // comboBox25
            // 
            this.comboBox25.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox25.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox25.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osProdutosServicosBindingSource, "PEDIDO", true));
            this.comboBox25.DataSource = this.bdsPlanoPedido;
            this.comboBox25.DisplayMember = "Conta";
            this.comboBox25.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox25.FormattingEnabled = true;
            this.comboBox25.Location = new System.Drawing.Point(182, 125);
            this.comboBox25.Name = "comboBox25";
            this.comboBox25.Size = new System.Drawing.Size(100, 21);
            this.comboBox25.TabIndex = 90;
            this.comboBox25.ValueMember = "ID";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(133, 128);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 92;
            this.label5.Text = "Pedido:";
            // 
            // comboBox26
            // 
            this.comboBox26.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox26.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox26.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osProdutosServicosBindingSource, "TRANSLADO", true));
            this.comboBox26.DataSource = this.bdsPlanoTranslado;
            this.comboBox26.DisplayMember = "Descricao";
            this.comboBox26.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox26.FormattingEnabled = true;
            this.comboBox26.Location = new System.Drawing.Point(288, 71);
            this.comboBox26.Name = "comboBox26";
            this.comboBox26.Size = new System.Drawing.Size(446, 21);
            this.comboBox26.TabIndex = 5;
            this.comboBox26.ValueMember = "ID";
            // 
            // bdsPlanoTranslado
            // 
            this.bdsPlanoTranslado.DataMember = "A30PLANO_CONTA";
            this.bdsPlanoTranslado.DataSource = this.dtsPrincipal;
            this.bdsPlanoTranslado.Filter = "TIPO = \'P\'  AND APRESENTAR = 1";
            // 
            // comboBox27
            // 
            this.comboBox27.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox27.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox27.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osProdutosServicosBindingSource, "TRANSLADO", true));
            this.comboBox27.DataSource = this.bdsPlanoTranslado;
            this.comboBox27.DisplayMember = "Conta";
            this.comboBox27.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox27.FormattingEnabled = true;
            this.comboBox27.Location = new System.Drawing.Point(182, 71);
            this.comboBox27.Name = "comboBox27";
            this.comboBox27.Size = new System.Drawing.Size(100, 21);
            this.comboBox27.TabIndex = 4;
            this.comboBox27.ValueMember = "ID";
            // 
            // comboBox28
            // 
            this.comboBox28.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox28.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox28.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osProdutosServicosBindingSource, "HOSPEDAGEM", true));
            this.comboBox28.DataSource = this.bdsPlanoHospedagem;
            this.comboBox28.DisplayMember = "Descricao";
            this.comboBox28.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox28.FormattingEnabled = true;
            this.comboBox28.Location = new System.Drawing.Point(288, 44);
            this.comboBox28.Name = "comboBox28";
            this.comboBox28.Size = new System.Drawing.Size(446, 21);
            this.comboBox28.TabIndex = 3;
            this.comboBox28.ValueMember = "ID";
            // 
            // bdsPlanoHospedagem
            // 
            this.bdsPlanoHospedagem.DataMember = "A30PLANO_CONTA";
            this.bdsPlanoHospedagem.DataSource = this.dtsPrincipal;
            this.bdsPlanoHospedagem.Filter = "TIPO = \'P\'  AND APRESENTAR = 1";
            // 
            // comboBox29
            // 
            this.comboBox29.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox29.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox29.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osProdutosServicosBindingSource, "HOSPEDAGEM", true));
            this.comboBox29.DataSource = this.bdsPlanoHospedagem;
            this.comboBox29.DisplayMember = "Conta";
            this.comboBox29.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox29.FormattingEnabled = true;
            this.comboBox29.Location = new System.Drawing.Point(182, 44);
            this.comboBox29.Name = "comboBox29";
            this.comboBox29.Size = new System.Drawing.Size(100, 21);
            this.comboBox29.TabIndex = 2;
            this.comboBox29.ValueMember = "ID";
            // 
            // comboBox30
            // 
            this.comboBox30.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox30.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox30.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osProdutosServicosBindingSource, "ALIMENTACAO", true));
            this.comboBox30.DataSource = this.bdsPlanoAlimentacao;
            this.comboBox30.DisplayMember = "Descricao";
            this.comboBox30.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox30.FormattingEnabled = true;
            this.comboBox30.Location = new System.Drawing.Point(288, 17);
            this.comboBox30.Name = "comboBox30";
            this.comboBox30.Size = new System.Drawing.Size(446, 21);
            this.comboBox30.TabIndex = 1;
            this.comboBox30.ValueMember = "ID";
            // 
            // bdsPlanoAlimentacao
            // 
            this.bdsPlanoAlimentacao.DataMember = "A30PLANO_CONTA";
            this.bdsPlanoAlimentacao.DataSource = this.dtsPrincipal;
            this.bdsPlanoAlimentacao.Filter = "TIPO = \'P\'  AND APRESENTAR = 1";
            // 
            // comboBox31
            // 
            this.comboBox31.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox31.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox31.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osProdutosServicosBindingSource, "ALIMENTACAO", true));
            this.comboBox31.DataSource = this.bdsPlanoAlimentacao;
            this.comboBox31.DisplayMember = "Conta";
            this.comboBox31.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox31.FormattingEnabled = true;
            this.comboBox31.Location = new System.Drawing.Point(182, 17);
            this.comboBox31.Name = "comboBox31";
            this.comboBox31.Size = new System.Drawing.Size(100, 21);
            this.comboBox31.TabIndex = 0;
            this.comboBox31.ValueMember = "ID";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(119, 74);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(57, 13);
            this.label23.TabIndex = 82;
            this.label23.Text = "Translado:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(103, 48);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(73, 13);
            this.label24.TabIndex = 81;
            this.label24.Text = "Hospedagem:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(108, 22);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(68, 13);
            this.label25.TabIndex = 80;
            this.label25.Text = "Alimentação:";
            // 
            // comboBox15
            // 
            this.comboBox15.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox15.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox15.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osProdutosServicosBindingSource, "OUTROS", true));
            this.comboBox15.DataSource = this.bdsPlanoOutros;
            this.comboBox15.DisplayMember = "Descricao";
            this.comboBox15.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox15.FormattingEnabled = true;
            this.comboBox15.Location = new System.Drawing.Point(288, 98);
            this.comboBox15.Name = "comboBox15";
            this.comboBox15.Size = new System.Drawing.Size(446, 21);
            this.comboBox15.TabIndex = 7;
            this.comboBox15.ValueMember = "ID";
            // 
            // bdsPlanoOutros
            // 
            this.bdsPlanoOutros.DataMember = "A30PLANO_CONTA";
            this.bdsPlanoOutros.DataSource = this.dtsPrincipal;
            this.bdsPlanoOutros.Filter = "TIPO = \'P\'  AND APRESENTAR = 1";
            // 
            // comboBox14
            // 
            this.comboBox14.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox14.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox14.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osProdutosServicosBindingSource, "OUTROS", true));
            this.comboBox14.DataSource = this.bdsPlanoOutros;
            this.comboBox14.DisplayMember = "Conta";
            this.comboBox14.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox14.FormattingEnabled = true;
            this.comboBox14.Location = new System.Drawing.Point(182, 98);
            this.comboBox14.Name = "comboBox14";
            this.comboBox14.Size = new System.Drawing.Size(100, 21);
            this.comboBox14.TabIndex = 6;
            this.comboBox14.ValueMember = "ID";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(85, 101);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(91, 13);
            this.label16.TabIndex = 71;
            this.label16.Text = "Outros Despesas:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.comboBox22);
            this.groupBox1.Controls.Add(this.comboBox23);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.comboBox32);
            this.groupBox1.Controls.Add(this.comboBox33);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.comboBox16);
            this.groupBox1.Controls.Add(this.comboBox17);
            this.groupBox1.Controls.Add(this.comboBox18);
            this.groupBox1.Controls.Add(this.comboBox19);
            this.groupBox1.Controls.Add(this.comboBox20);
            this.groupBox1.Controls.Add(this.comboBox21);
            this.groupBox1.Controls.Add(this.comboBox11);
            this.groupBox1.Controls.Add(this.comboBox12);
            this.groupBox1.Controls.Add(this.comboBox13);
            this.groupBox1.Controls.Add(this.comboBox10);
            this.groupBox1.Controls.Add(this.comboBox9);
            this.groupBox1.Controls.Add(this.comboBox8);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Location = new System.Drawing.Point(6, 14);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(740, 233);
            this.groupBox1.TabIndex = 74;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = " Produtos ";
            // 
            // comboBox22
            // 
            this.comboBox22.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox22.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox22.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osProdutosServicosBindingSource, "REEMBOLSO", true));
            this.comboBox22.DataSource = this.bdsProdutoReembolso;
            this.comboBox22.DisplayMember = "Descricao";
            this.comboBox22.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox22.FormattingEnabled = true;
            this.comboBox22.Location = new System.Drawing.Point(288, 202);
            this.comboBox22.Name = "comboBox22";
            this.comboBox22.Size = new System.Drawing.Size(446, 21);
            this.comboBox22.TabIndex = 94;
            this.comboBox22.ValueMember = "ID";
            // 
            // bdsProdutoReembolso
            // 
            this.bdsProdutoReembolso.DataMember = "A30PRODUTO";
            this.bdsProdutoReembolso.DataSource = this.dtsPrincipal;
            this.bdsProdutoReembolso.Filter = "TIPO = 2 ";
            // 
            // comboBox23
            // 
            this.comboBox23.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox23.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox23.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osProdutosServicosBindingSource, "REEMBOLSO", true));
            this.comboBox23.DataSource = this.bdsProdutoReembolso;
            this.comboBox23.DisplayMember = "Produto";
            this.comboBox23.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox23.FormattingEnabled = true;
            this.comboBox23.Location = new System.Drawing.Point(182, 202);
            this.comboBox23.Name = "comboBox23";
            this.comboBox23.Size = new System.Drawing.Size(100, 21);
            this.comboBox23.TabIndex = 93;
            this.comboBox23.ValueMember = "ID";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(113, 205);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(63, 13);
            this.label12.TabIndex = 95;
            this.label12.Text = "Reembolso:";
            // 
            // comboBox32
            // 
            this.comboBox32.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox32.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox32.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osProdutosServicosBindingSource, "OUTROS_SERVICOS", true));
            this.comboBox32.DataSource = this.bdsProdutoOutros;
            this.comboBox32.DisplayMember = "Descricao";
            this.comboBox32.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox32.FormattingEnabled = true;
            this.comboBox32.Location = new System.Drawing.Point(288, 175);
            this.comboBox32.Name = "comboBox32";
            this.comboBox32.Size = new System.Drawing.Size(446, 21);
            this.comboBox32.TabIndex = 13;
            this.comboBox32.ValueMember = "ID";
            // 
            // bdsProdutoOutros
            // 
            this.bdsProdutoOutros.AllowNew = false;
            this.bdsProdutoOutros.DataMember = "A30PRODUTO";
            this.bdsProdutoOutros.DataSource = this.dtsPrincipal;
            this.bdsProdutoOutros.Filter = "TIPO = 2 ";
            // 
            // comboBox33
            // 
            this.comboBox33.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox33.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox33.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osProdutosServicosBindingSource, "OUTROS_SERVICOS", true));
            this.comboBox33.DataSource = this.bdsProdutoOutros;
            this.comboBox33.DisplayMember = "Produto";
            this.comboBox33.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox33.FormattingEnabled = true;
            this.comboBox33.Location = new System.Drawing.Point(182, 175);
            this.comboBox33.Name = "comboBox33";
            this.comboBox33.Size = new System.Drawing.Size(100, 21);
            this.comboBox33.TabIndex = 12;
            this.comboBox33.ValueMember = "ID";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(91, 178);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(85, 13);
            this.label14.TabIndex = 92;
            this.label14.Text = "Outros Serviços:";
            // 
            // comboBox16
            // 
            this.comboBox16.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox16.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox16.DataSource = this.bdsProdutoKM;
            this.comboBox16.DisplayMember = "Descricao";
            this.comboBox16.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox16.FormattingEnabled = true;
            this.comboBox16.Location = new System.Drawing.Point(288, 149);
            this.comboBox16.Name = "comboBox16";
            this.comboBox16.Size = new System.Drawing.Size(446, 21);
            this.comboBox16.TabIndex = 11;
            this.comboBox16.ValueMember = "ID";
            // 
            // bdsProdutoKM
            // 
            this.bdsProdutoKM.AllowNew = false;
            this.bdsProdutoKM.DataMember = "A30PRODUTO";
            this.bdsProdutoKM.DataSource = this.dtsPrincipal;
            this.bdsProdutoKM.Filter = "TIPO = 2 ";
            // 
            // comboBox17
            // 
            this.comboBox17.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox17.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox17.DataSource = this.bdsProdutoRede;
            this.comboBox17.DisplayMember = "Descricao";
            this.comboBox17.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox17.FormattingEnabled = true;
            this.comboBox17.Location = new System.Drawing.Point(288, 122);
            this.comboBox17.Name = "comboBox17";
            this.comboBox17.Size = new System.Drawing.Size(446, 21);
            this.comboBox17.TabIndex = 9;
            this.comboBox17.ValueMember = "ID";
            // 
            // bdsProdutoRede
            // 
            this.bdsProdutoRede.AllowNew = false;
            this.bdsProdutoRede.DataMember = "A30PRODUTO";
            this.bdsProdutoRede.DataSource = this.dtsPrincipal;
            this.bdsProdutoRede.Filter = "TIPO = 2 ";
            // 
            // comboBox18
            // 
            this.comboBox18.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox18.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox18.DataSource = this.bdsProdutoTaxa;
            this.comboBox18.DisplayMember = "Descricao";
            this.comboBox18.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox18.FormattingEnabled = true;
            this.comboBox18.Location = new System.Drawing.Point(288, 94);
            this.comboBox18.Name = "comboBox18";
            this.comboBox18.Size = new System.Drawing.Size(446, 21);
            this.comboBox18.TabIndex = 7;
            this.comboBox18.ValueMember = "ID";
            // 
            // bdsProdutoTaxa
            // 
            this.bdsProdutoTaxa.AllowNew = false;
            this.bdsProdutoTaxa.DataMember = "A30PRODUTO";
            this.bdsProdutoTaxa.DataSource = this.dtsPrincipal;
            this.bdsProdutoTaxa.Filter = "TIPO = 2 ";
            // 
            // comboBox19
            // 
            this.comboBox19.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox19.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox19.DataSource = this.bdsProdutoExcedente;
            this.comboBox19.DisplayMember = "Descricao";
            this.comboBox19.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox19.FormattingEnabled = true;
            this.comboBox19.Location = new System.Drawing.Point(288, 67);
            this.comboBox19.Name = "comboBox19";
            this.comboBox19.Size = new System.Drawing.Size(446, 21);
            this.comboBox19.TabIndex = 5;
            this.comboBox19.ValueMember = "ID";
            // 
            // bdsProdutoExcedente
            // 
            this.bdsProdutoExcedente.AllowNew = false;
            this.bdsProdutoExcedente.DataMember = "A30PRODUTO";
            this.bdsProdutoExcedente.DataSource = this.dtsPrincipal;
            this.bdsProdutoExcedente.Filter = "TIPO = 2 ";
            // 
            // comboBox20
            // 
            this.comboBox20.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox20.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox20.DataSource = this.bdsProdutoDeslocamento;
            this.comboBox20.DisplayMember = "Descricao";
            this.comboBox20.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox20.FormattingEnabled = true;
            this.comboBox20.Location = new System.Drawing.Point(288, 40);
            this.comboBox20.Name = "comboBox20";
            this.comboBox20.Size = new System.Drawing.Size(446, 21);
            this.comboBox20.TabIndex = 3;
            this.comboBox20.ValueMember = "ID";
            // 
            // bdsProdutoDeslocamento
            // 
            this.bdsProdutoDeslocamento.AllowNew = false;
            this.bdsProdutoDeslocamento.DataMember = "A30PRODUTO";
            this.bdsProdutoDeslocamento.DataSource = this.dtsPrincipal;
            this.bdsProdutoDeslocamento.Filter = "TIPO = 2 ";
            // 
            // comboBox21
            // 
            this.comboBox21.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox21.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox21.DataSource = this.bdsProdutoVisita;
            this.comboBox21.DisplayMember = "Descricao";
            this.comboBox21.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox21.FormattingEnabled = true;
            this.comboBox21.Location = new System.Drawing.Point(288, 13);
            this.comboBox21.Name = "comboBox21";
            this.comboBox21.Size = new System.Drawing.Size(446, 21);
            this.comboBox21.TabIndex = 1;
            this.comboBox21.ValueMember = "ID";
            // 
            // bdsProdutoVisita
            // 
            this.bdsProdutoVisita.AllowNew = false;
            this.bdsProdutoVisita.DataMember = "A30PRODUTO";
            this.bdsProdutoVisita.DataSource = this.dtsPrincipal;
            this.bdsProdutoVisita.Filter = "TIPO = 2 ";
            // 
            // comboBox11
            // 
            this.comboBox11.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox11.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox11.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osProdutosServicosBindingSource, "DESLOCAMENTO_KM", true));
            this.comboBox11.DataSource = this.bdsProdutoKM;
            this.comboBox11.DisplayMember = "Produto";
            this.comboBox11.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox11.FormattingEnabled = true;
            this.comboBox11.Location = new System.Drawing.Point(182, 149);
            this.comboBox11.Name = "comboBox11";
            this.comboBox11.Size = new System.Drawing.Size(100, 21);
            this.comboBox11.TabIndex = 10;
            this.comboBox11.ValueMember = "ID";
            // 
            // comboBox12
            // 
            this.comboBox12.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox12.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox12.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osProdutosServicosBindingSource, "PONTO_REDE", true));
            this.comboBox12.DataSource = this.bdsProdutoRede;
            this.comboBox12.DisplayMember = "Produto";
            this.comboBox12.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox12.FormattingEnabled = true;
            this.comboBox12.Location = new System.Drawing.Point(182, 122);
            this.comboBox12.Name = "comboBox12";
            this.comboBox12.Size = new System.Drawing.Size(100, 21);
            this.comboBox12.TabIndex = 8;
            this.comboBox12.ValueMember = "ID";
            // 
            // comboBox13
            // 
            this.comboBox13.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox13.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox13.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osProdutosServicosBindingSource, "TAXA_INSTALACAO", true));
            this.comboBox13.DataSource = this.bdsProdutoTaxa;
            this.comboBox13.DisplayMember = "Produto";
            this.comboBox13.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox13.FormattingEnabled = true;
            this.comboBox13.Location = new System.Drawing.Point(182, 95);
            this.comboBox13.Name = "comboBox13";
            this.comboBox13.Size = new System.Drawing.Size(100, 21);
            this.comboBox13.TabIndex = 6;
            this.comboBox13.ValueMember = "ID";
            // 
            // comboBox10
            // 
            this.comboBox10.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox10.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox10.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osProdutosServicosBindingSource, "HORA_EXCEDENTE", true));
            this.comboBox10.DataSource = this.bdsProdutoExcedente;
            this.comboBox10.DisplayMember = "Produto";
            this.comboBox10.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox10.FormattingEnabled = true;
            this.comboBox10.Location = new System.Drawing.Point(182, 68);
            this.comboBox10.Name = "comboBox10";
            this.comboBox10.Size = new System.Drawing.Size(100, 21);
            this.comboBox10.TabIndex = 4;
            this.comboBox10.ValueMember = "ID";
            // 
            // comboBox9
            // 
            this.comboBox9.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox9.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox9.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osProdutosServicosBindingSource, "HORA_DESLOCAMENTO", true));
            this.comboBox9.DataSource = this.bdsProdutoDeslocamento;
            this.comboBox9.DisplayMember = "Produto";
            this.comboBox9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox9.FormattingEnabled = true;
            this.comboBox9.Location = new System.Drawing.Point(182, 41);
            this.comboBox9.Name = "comboBox9";
            this.comboBox9.Size = new System.Drawing.Size(100, 21);
            this.comboBox9.TabIndex = 2;
            this.comboBox9.ValueMember = "ID";
            // 
            // comboBox8
            // 
            this.comboBox8.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox8.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox8.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.osProdutosServicosBindingSource, "VISITA_TECNICA", true));
            this.comboBox8.DataSource = this.bdsProdutoVisita;
            this.comboBox8.DisplayMember = "Produto";
            this.comboBox8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox8.FormattingEnabled = true;
            this.comboBox8.Location = new System.Drawing.Point(182, 14);
            this.comboBox8.Name = "comboBox8";
            this.comboBox8.Size = new System.Drawing.Size(100, 21);
            this.comboBox8.TabIndex = 0;
            this.comboBox8.ValueMember = "ID";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(28, 153);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(148, 13);
            this.label19.TabIndex = 79;
            this.label19.Text = "Deslocamento p/Km Rodado:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(13, 125);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(163, 13);
            this.label20.TabIndex = 78;
            this.label20.Text = "Configuração de Ponto de Rede:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(7, 96);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(169, 13);
            this.label21.TabIndex = 77;
            this.label21.Text = "Taxa de Instalação de Programas:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(3, 70);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(173, 13);
            this.label18.TabIndex = 76;
            this.label18.Text = "Hora Técnica Excedente no Local:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(13, 44);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(163, 13);
            this.label17.TabIndex = 75;
            this.label17.Text = "Hora Técnica em Deslocamento:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(99, 18);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(77, 13);
            this.label15.TabIndex = 74;
            this.label15.Text = "Visita Técnica:";
            // 
            // bdsPlanoContaRec
            // 
            this.bdsPlanoContaRec.DataMember = "A30PLANO_CONTA";
            this.bdsPlanoContaRec.DataSource = this.dtsPrincipal;
            this.bdsPlanoContaRec.Filter = "TIPO = \'E\'  AND APRESENTAR = 1";
            // 
            // a30PRODUTOTableAdapter
            // 
            this.a30PRODUTOTableAdapter.ClearBeforeFill = true;
            // 
            // bdsPlanoOutrosServicos
            // 
            this.bdsPlanoOutrosServicos.DataMember = "A30PLANO_CONTA";
            this.bdsPlanoOutrosServicos.DataSource = this.dtsPrincipal;
            this.bdsPlanoOutrosServicos.Filter = "TIPO = \'E\'  AND APRESENTAR = 1";
            // 
            // tbpSituacoes
            // 
            this.tbpSituacoes.Controls.Add(this.cmbTipoSituacao);
            this.tbpSituacoes.Controls.Add(iDLabel);
            this.tbpSituacoes.Controls.Add(this.txtCodigoSituacao);
            this.tbpSituacoes.Controls.Add(nOMELabel);
            this.tbpSituacoes.Controls.Add(this.txtNomeSituacao);
            this.tbpSituacoes.Controls.Add(dESCRICAOLabel);
            this.tbpSituacoes.Controls.Add(this.txtDescricaoSituacao);
            this.tbpSituacoes.Controls.Add(tIPO_SITUACAOLabel);
            this.tbpSituacoes.Controls.Add(dT_CADASTROLabel);
            this.tbpSituacoes.Controls.Add(this.dtpDataCadastroSituacao);
            this.tbpSituacoes.Controls.Add(this.grdSituacao);
            this.tbpSituacoes.Location = new System.Drawing.Point(4, 22);
            this.tbpSituacoes.Name = "tbpSituacoes";
            this.tbpSituacoes.Padding = new System.Windows.Forms.Padding(3);
            this.tbpSituacoes.Size = new System.Drawing.Size(752, 496);
            this.tbpSituacoes.TabIndex = 8;
            this.tbpSituacoes.Text = "Situação";
            this.tbpSituacoes.UseVisualStyleBackColor = true;
            // 
            // cmbTipoSituacao
            // 
            this.cmbTipoSituacao.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbTipoSituacao.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbTipoSituacao.FormattingEnabled = true;
            this.cmbTipoSituacao.Items.AddRange(new object[] {
            "S - Solicitada",
            "E - Encontrada",
            "R - Realizada",
            "F - Final"});
            this.cmbTipoSituacao.Location = new System.Drawing.Point(122, 86);
            this.cmbTipoSituacao.MaxLength = 1;
            this.cmbTipoSituacao.Name = "cmbTipoSituacao";
            this.cmbTipoSituacao.Size = new System.Drawing.Size(140, 21);
            this.cmbTipoSituacao.TabIndex = 4;
            // 
            // txtCodigoSituacao
            // 
            this.txtCodigoSituacao.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osParSituacaoBindingSource, "ID", true));
            this.txtCodigoSituacao.Location = new System.Drawing.Point(122, 8);
            this.txtCodigoSituacao.Name = "txtCodigoSituacao";
            this.txtCodigoSituacao.ReadOnly = true;
            this.txtCodigoSituacao.Size = new System.Drawing.Size(50, 20);
            this.txtCodigoSituacao.TabIndex = 0;
            // 
            // txtNomeSituacao
            // 
            this.txtNomeSituacao.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osParSituacaoBindingSource, "NOME", true));
            this.txtNomeSituacao.Location = new System.Drawing.Point(122, 34);
            this.txtNomeSituacao.MaxLength = 30;
            this.txtNomeSituacao.Name = "txtNomeSituacao";
            this.txtNomeSituacao.Size = new System.Drawing.Size(157, 20);
            this.txtNomeSituacao.TabIndex = 2;
            // 
            // txtDescricaoSituacao
            // 
            this.txtDescricaoSituacao.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osParSituacaoBindingSource, "DESCRICAO", true));
            this.txtDescricaoSituacao.Location = new System.Drawing.Point(122, 60);
            this.txtDescricaoSituacao.MaxLength = 100;
            this.txtDescricaoSituacao.Name = "txtDescricaoSituacao";
            this.txtDescricaoSituacao.Size = new System.Drawing.Size(360, 20);
            this.txtDescricaoSituacao.TabIndex = 3;
            // 
            // dtpDataCadastroSituacao
            // 
            this.dtpDataCadastroSituacao.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.osParSituacaoBindingSource, "DT_CADASTRO", true));
            this.dtpDataCadastroSituacao.Enabled = false;
            this.dtpDataCadastroSituacao.Location = new System.Drawing.Point(446, 8);
            this.dtpDataCadastroSituacao.Name = "dtpDataCadastroSituacao";
            this.dtpDataCadastroSituacao.Size = new System.Drawing.Size(300, 20);
            this.dtpDataCadastroSituacao.TabIndex = 1;
            // 
            // grdSituacao
            // 
            this.grdSituacao.AllowUserToAddRows = false;
            this.grdSituacao.AllowUserToDeleteRows = false;
            this.grdSituacao.AutoGenerateColumns = false;
            this.grdSituacao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdSituacao.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.nOMEDataGridViewTextBoxColumn,
            this.dESCRICAODataGridViewTextBoxColumn,
            this.tIPOSITUACAODataGridViewTextBoxColumn,
            this.dTCADASTRODataGridViewTextBoxColumn});
            this.grdSituacao.DataSource = this.osParSituacaoBindingSource;
            this.grdSituacao.Location = new System.Drawing.Point(6, 113);
            this.grdSituacao.MultiSelect = false;
            this.grdSituacao.Name = "grdSituacao";
            this.grdSituacao.ReadOnly = true;
            this.grdSituacao.RowHeadersWidth = 30;
            this.grdSituacao.Size = new System.Drawing.Size(740, 377);
            this.grdSituacao.TabIndex = 5;
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "Código";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.ReadOnly = true;
            this.iDDataGridViewTextBoxColumn.Width = 50;
            // 
            // nOMEDataGridViewTextBoxColumn
            // 
            this.nOMEDataGridViewTextBoxColumn.DataPropertyName = "NOME";
            this.nOMEDataGridViewTextBoxColumn.HeaderText = "Nome";
            this.nOMEDataGridViewTextBoxColumn.Name = "nOMEDataGridViewTextBoxColumn";
            this.nOMEDataGridViewTextBoxColumn.ReadOnly = true;
            this.nOMEDataGridViewTextBoxColumn.Width = 150;
            // 
            // dESCRICAODataGridViewTextBoxColumn
            // 
            this.dESCRICAODataGridViewTextBoxColumn.DataPropertyName = "DESCRICAO";
            this.dESCRICAODataGridViewTextBoxColumn.HeaderText = "Descrição";
            this.dESCRICAODataGridViewTextBoxColumn.Name = "dESCRICAODataGridViewTextBoxColumn";
            this.dESCRICAODataGridViewTextBoxColumn.ReadOnly = true;
            this.dESCRICAODataGridViewTextBoxColumn.Width = 250;
            // 
            // tIPOSITUACAODataGridViewTextBoxColumn
            // 
            this.tIPOSITUACAODataGridViewTextBoxColumn.DataPropertyName = "TIPO_SITUACAO";
            this.tIPOSITUACAODataGridViewTextBoxColumn.HeaderText = "Tipo da Situação";
            this.tIPOSITUACAODataGridViewTextBoxColumn.Name = "tIPOSITUACAODataGridViewTextBoxColumn";
            this.tIPOSITUACAODataGridViewTextBoxColumn.ReadOnly = true;
            this.tIPOSITUACAODataGridViewTextBoxColumn.Width = 120;
            // 
            // dTCADASTRODataGridViewTextBoxColumn
            // 
            this.dTCADASTRODataGridViewTextBoxColumn.DataPropertyName = "DT_CADASTRO";
            this.dTCADASTRODataGridViewTextBoxColumn.HeaderText = "Data de Cadastro";
            this.dTCADASTRODataGridViewTextBoxColumn.Name = "dTCADASTRODataGridViewTextBoxColumn";
            this.dTCADASTRODataGridViewTextBoxColumn.ReadOnly = true;
            this.dTCADASTRODataGridViewTextBoxColumn.Width = 130;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox7);
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(752, 496);
            this.tabPage2.TabIndex = 9;
            this.tabPage2.Text = "Templates";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.eMAIL_PEDIDOTextBox);
            this.groupBox7.Location = new System.Drawing.Point(6, 252);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(740, 238);
            this.groupBox7.TabIndex = 2;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = " Email Pedido ";
            // 
            // eMAIL_PEDIDOTextBox
            // 
            this.eMAIL_PEDIDOTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osParametrosBindingSource, "EMAIL_PEDIDO", true));
            this.eMAIL_PEDIDOTextBox.Location = new System.Drawing.Point(6, 19);
            this.eMAIL_PEDIDOTextBox.Multiline = true;
            this.eMAIL_PEDIDOTextBox.Name = "eMAIL_PEDIDOTextBox";
            this.eMAIL_PEDIDOTextBox.Size = new System.Drawing.Size(728, 213);
            this.eMAIL_PEDIDOTextBox.TabIndex = 1;
            // 
            // osParametrosBindingSource
            // 
            this.osParametrosBindingSource.DataMember = "OsParametros";
            this.osParametrosBindingSource.DataSource = this.dtsPrincipal;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.eMAIL_ORDEMTextBox);
            this.groupBox4.Location = new System.Drawing.Point(6, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(740, 238);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = " Email Ordem de Serviço ";
            // 
            // eMAIL_ORDEMTextBox
            // 
            this.eMAIL_ORDEMTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.osParametrosBindingSource, "EMAIL_ORDEM", true));
            this.eMAIL_ORDEMTextBox.Location = new System.Drawing.Point(6, 17);
            this.eMAIL_ORDEMTextBox.Multiline = true;
            this.eMAIL_ORDEMTextBox.Name = "eMAIL_ORDEMTextBox";
            this.eMAIL_ORDEMTextBox.Size = new System.Drawing.Size(728, 215);
            this.eMAIL_ORDEMTextBox.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "ID";
            this.dataGridViewTextBoxColumn4.HeaderText = "ID";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "IDFUNCIONARIO";
            this.dataGridViewTextBoxColumn5.HeaderText = "IDFUNCIONARIO";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "IDPOSTO";
            this.dataGridViewTextBoxColumn6.HeaderText = "IDPOSTO";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "NOME";
            this.dataGridViewTextBoxColumn8.HeaderText = "NOME";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "TIPO_ACESSO";
            this.dataGridViewTextBoxColumn10.HeaderText = "TIPO_ACESSO";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "DT_CADASTRO";
            this.dataGridViewTextBoxColumn11.HeaderText = "DT_CADASTRO";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "ULTIMO_ACESSO";
            this.dataGridViewTextBoxColumn12.HeaderText = "ULTIMO_ACESSO";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "PLANO_CONTA";
            this.dataGridViewTextBoxColumn13.HeaderText = "PLANO_CONTA";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.DataPropertyName = "ACESSA_PEDIDO";
            this.dataGridViewCheckBoxColumn1.HeaderText = "ACESSA_PEDIDO";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            // 
            // dataGridViewCheckBoxColumn2
            // 
            this.dataGridViewCheckBoxColumn2.DataPropertyName = "DIGITA_SELO_LACRE";
            this.dataGridViewCheckBoxColumn2.HeaderText = "DIGITA_SELO_LACRE";
            this.dataGridViewCheckBoxColumn2.Name = "dataGridViewCheckBoxColumn2";
            // 
            // ManParOS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(784, 634);
            this.Controls.Add(this.bindingNavigator);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.MaximizeBox = true;
            this.MinimizeBox = true;
            this.Name = "ManParOS";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Manutenção de Parametrização e Cadastro da Ordem de Serviço";
            this.Load += new System.EventHandler(this.ManParOS_Load_1);
            this.Controls.SetChildIndex(this.bindingNavigator, 0);
            this.Controls.SetChildIndex(this.grpPrincipal, 0);
            this.Controls.SetChildIndex(this.tabPrincipal, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dtsPrincipal)).EndInit();
            this.grpPrincipal.ResumeLayout(false);
            this.grpPrincipal.PerformLayout();
            this.tabPrincipal.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.a30CLIENTESBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsFuncionario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator)).EndInit();
            this.bindingNavigator.ResumeLayout(false);
            this.bindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsFiltroUsuario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FiltroUsuario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsFiltroCliente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.osUsuarioBindingSource)).EndInit();
            this.tbpUsuarios.ResumeLayout(false);
            this.tbpUsuarios.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.osUsuarioDataGridView)).EndInit();
            this.pnlAdmnistrador1.ResumeLayout(false);
            this.pnlAdmnistrador1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.a30PLANO_CONTABindingSource)).EndInit();
            this.pnlAdmnistrador2.ResumeLayout(false);
            this.pnlAdmnistrador2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.osParSituacaoBindingSource)).EndInit();
            this.tbpFormPag.ResumeLayout(false);
            this.tbpFormPag.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.osParContasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPlanoContaFormaPag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.osParContasDataGridView)).EndInit();
            this.tabParametrizacao.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.osProdutosServicosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPlanoFiado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPlanoVendas)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPlanoPedido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPlanoTranslado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPlanoHospedagem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPlanoAlimentacao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPlanoOutros)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsProdutoReembolso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsProdutoOutros)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsProdutoKM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsProdutoRede)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsProdutoTaxa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsProdutoExcedente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsProdutoDeslocamento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsProdutoVisita)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPlanoContaRec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPlanoOutrosServicos)).EndInit();
            this.tbpSituacoes.ResumeLayout(false);
            this.tbpSituacoes.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdSituacao)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.osParametrosBindingSource)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingNavigator bindingNavigator;
        private System.Windows.Forms.ToolStripButton tsbInserir;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripButton tsbExcluir;
        private System.Windows.Forms.ToolStripButton tsbPrimeiro;
        private System.Windows.Forms.ToolStripButton tsbAnterior;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton tsbProximo;
        private System.Windows.Forms.ToolStripButton tsbUltimo;
        private System.Windows.Forms.TabPage tbpUsuarios;
        private System.Windows.Forms.ToolStripButton tsbGravar;
        private System.Windows.Forms.BindingSource osParSituacaoBindingSource;
        private OsExpressDataSetTableAdapters.OsParSituacaoTableAdapter osParSituacaoTableAdapter;
        //private OsExpressDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingSource osUsuarioBindingSource;
        private OsExpressDataSetTableAdapters.OsUsuarioTableAdapter osUsuarioTableAdapter;
        private System.Windows.Forms.TextBox iDTextBox5;
        private System.Windows.Forms.TextBox nOMETextBox3;
        private System.Windows.Forms.TextBox txtConfirmaSenha;
        private System.Windows.Forms.DateTimePicker dT_CADASTRODateTimePicker5;
        private System.Windows.Forms.BindingSource a30CLIENTESBindingSource;
        private OsExpressDataSetTableAdapters.A30CLIENTESTableAdapter a30CLIENTESTableAdapter;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.BindingSource bdsFuncionario;
        private OsExpressDataSetTableAdapters.A30FUNCIONARIOTableAdapter a30FUNCIONARIOTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn pROPRIETARIODataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cGCDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn uFDataGridViewTextBoxColumn;
        private System.Windows.Forms.TextBox txtSenha;
        private System.Windows.Forms.Label lblSenhaNaoConfere;
        private System.Windows.Forms.Label lblSenhaDigitos;
        private OsExpressDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingSource a30PLANO_CONTABindingSource;
        private OsExpressDataSetTableAdapters.A30PLANO_CONTATableAdapter a30PLANO_CONTATableAdapter;
        private System.Windows.Forms.TabPage tbpFormPag;
        private System.Windows.Forms.TextBox iDTextBox1;
        private System.Windows.Forms.TextBox nOMETextBox1;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.BindingSource osParContasBindingSource;
        private OsExpressDataSetTableAdapters.OsParContasTableAdapter osParContasTableAdapter;
        private System.Windows.Forms.DataGridView osParContasDataGridView;
        private System.Windows.Forms.ComboBox comboBox7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.ToolStripButton tsbCancelar;
        private System.Windows.Forms.TabPage tabParametrizacao;
        private OsExpressDataSetTableAdapters.OsProdutosServicosTableAdapter osProdutosServicosTableAdapter;
        private System.Windows.Forms.BindingSource osProdutosServicosBindingSource;
        private System.Windows.Forms.BindingSource bdsProdutoVisita;
        private OsExpressDataSetTableAdapters.A30PRODUTOTableAdapter a30PRODUTOTableAdapter;
        private System.Windows.Forms.BindingSource bdsProdutoDeslocamento;
        private System.Windows.Forms.BindingSource bdsProdutoOutros;
        private System.Windows.Forms.BindingSource bdsProdutoKM;
        private System.Windows.Forms.BindingSource bdsProdutoRede;
        private System.Windows.Forms.BindingSource bdsProdutoTaxa;
        private System.Windows.Forms.BindingSource bdsProdutoExcedente;
        private System.Windows.Forms.BindingSource bdsPlanoContaRec;
        private System.Windows.Forms.BindingSource bdsPlanoContaFormaPag;
        private System.Windows.Forms.BindingSource bdsPlanoAlimentacao;
        private System.Windows.Forms.BindingSource bdsPlanoHospedagem;
        private System.Windows.Forms.BindingSource bdsPlanoTranslado;
        private System.Windows.Forms.BindingSource bdsPlanoOutros;
        private System.Windows.Forms.BindingSource bdsProdutoReembolso;
        private System.Windows.Forms.BindingSource bdsPlanoOutrosServicos;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox comboBox26;
        private System.Windows.Forms.ComboBox comboBox27;
        private System.Windows.Forms.ComboBox comboBox28;
        private System.Windows.Forms.ComboBox comboBox29;
        private System.Windows.Forms.ComboBox comboBox30;
        private System.Windows.Forms.ComboBox comboBox31;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ComboBox comboBox15;
        private System.Windows.Forms.ComboBox comboBox14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox comboBox32;
        private System.Windows.Forms.ComboBox comboBox33;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox comboBox16;
        private System.Windows.Forms.ComboBox comboBox17;
        private System.Windows.Forms.ComboBox comboBox18;
        private System.Windows.Forms.ComboBox comboBox19;
        private System.Windows.Forms.ComboBox comboBox20;
        private System.Windows.Forms.ComboBox comboBox21;
        private System.Windows.Forms.ComboBox comboBox11;
        private System.Windows.Forms.ComboBox comboBox12;
        private System.Windows.Forms.ComboBox comboBox13;
        private System.Windows.Forms.ComboBox comboBox10;
        private System.Windows.Forms.ComboBox comboBox9;
        private System.Windows.Forms.ComboBox comboBox8;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.BindingSource bdsPlanoFiado;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TabPage tbpSituacoes;
        private System.Windows.Forms.ComboBox cmbTipoSituacao;
        private System.Windows.Forms.TextBox txtCodigoSituacao;
        private System.Windows.Forms.TextBox txtNomeSituacao;
        private System.Windows.Forms.TextBox txtDescricaoSituacao;
        private System.Windows.Forms.DateTimePicker dtpDataCadastroSituacao;
        private System.Windows.Forms.DataGridView grdSituacao;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nOMEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dESCRICAODataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tIPOSITUACAODataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dTCADASTRODataGridViewTextBoxColumn;
        private System.Windows.Forms.ComboBox comboBox22;
        private System.Windows.Forms.ComboBox comboBox23;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox comboBox24;
        private System.Windows.Forms.BindingSource bdsPlanoPedido;
        private System.Windows.Forms.ComboBox comboBox25;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox comboBox34;
        private System.Windows.Forms.ComboBox comboBox35;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox comboBox44;
        private System.Windows.Forms.ComboBox comboBox45;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.BindingSource bdsPlanoVendas;
        private System.Windows.Forms.BindingSource bdsFiltroCliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn dTCADASTRODataGridViewTextBoxColumn1;
        private System.Windows.Forms.BindingSource FiltroUsuario;
        private System.Windows.Forms.Panel pnlAdmnistrador1;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.ComboBox contaComboBox;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Panel pnlAdmnistrador2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox aCESSA_PEDIDOCheckBox;
        private System.Windows.Forms.ComboBox cmbTipoAcesso;
        private System.Windows.Forms.BindingSource bdsFiltroUsuario;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.BindingSource osParametrosBindingSource;
        private OsExpressDataSetTableAdapters.OsParametrosTableAdapter osParametrosTableAdapter;
        private System.Windows.Forms.TextBox eMAIL_PEDIDOTextBox;
        private System.Windows.Forms.TextBox eMAIL_ORDEMTextBox;
        private System.Windows.Forms.DataGridView osUsuarioDataGridView;
        private System.Windows.Forms.CheckBox cbPosto002;
        private System.Windows.Forms.CheckBox cbPosto001;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn2;
    }
}
