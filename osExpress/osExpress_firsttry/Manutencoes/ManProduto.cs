﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using TExpress;

namespace osExpress.Manutencoes
{
    public partial class ManProduto : osExpress.FrmPrincipal
    {
        public Decimal comissaoPercento;
        public Decimal comissaoValor;
        public Decimal preco;
        public Decimal vr_manutencao;
        string TIPO;
        string GRUPO;
        string UNIDADE;

        public BindingSource bs_tipo;
        public BindingSource bs_grupo;
        public BindingSource bs_unidade;
        public string DESCRICAO = "";

        public ManProduto()
        {
            InitializeComponent();
            DB.ConnectionName = "osExpress.Properties.Settings.OsExpressConnectionString";
            // this.WindowState = FormWindowState.Maximized;
        }

        private void ManProduto_Load(object sender, EventArgs e)
        {           
            int w = (int)(this.Parent.ClientSize.Width*0.9);
            int h = (int)(this.Parent.ClientSize.Height*0.9);
            this.Size = new Size(w,h);           
            int x = (int)(this.Parent.ClientSize.Width - w)/2;
            int y = (int)(this.Parent.ClientSize.Height - h)/2;
            this.Location = new Point(x,y);

            tableLayoutPanelEditar.Location = new Point(tableLayoutPanelConsultar.Location.X, tableLayoutPanelConsultar.Location.Y);
            tableLayoutPanelEditar.Dock = DockStyle.Fill;
            tableLayoutPanelEditar.Visible = false;


            //dataGridView1.AutoGenerateColumns = false;
            //DataGridViewTextBoxColumn col = new DataGridViewTextBoxColumn();
            //dataGridView1.Columns.Add(col);

            // nur hier
            dgvProduto.Columns.Clear();
            SetupDatagridView();

            #region stuff
            //System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("pt-BR");
            // this.BeginInvoke(new Action(() => { cmbGrupo.Select(0, 0); }));
            //Debug.WriteLine(this.tableLayoutPanelConsultar.RowCount);                     
            //foreach (Control c in this.tableLayoutPanelDados.Controls)            
            //{
            //    Debug.WriteLine(this.tableLayoutPanelDados.GetRow(c));
            //    Debug.WriteLine(c);
            //    Debug.WriteLine(c.Name);
            //    if (c.Name.Equals("PanelFillerRow1")) {
            //        var a = 1;                
            //    }
            //}           

            //DB.ConnectionName = "osExpress.Properties.Settings.OsExpressConnectionString";
            #endregion
            SetBindingsTipo();
            SetBindingsGrupo();
            SetBindingsUnidade();

            // setup layoute
            trackBar1.Value = 3;
            pictureBox2_Click(this.pictureBox2, null);
            trackBar1.Visible = false;
            tbTrackValue.Visible = false;
            lblTrackBar.Visible = false;
            pictureBox2.Enabled = false;

            tableLayoutPanelConsultar.BackColor = System.Drawing.Color.Transparent;
            tableLayoutFilter.BackColor = System.Drawing.Color.Transparent;
            panel4ConsultarBottom.BackColor = System.Drawing.Color.Transparent;
         
        }

        private void SetBindingsTipo()
        {
            // tabela Produto_tipo
            string sql_tipo =
                @"SELECT tipo, descricao from PRODUTO_TIPO ORDER BY descricao";
            // Bindingsource
            bs_tipo = new BindingSource();           
            bs_tipo.DataSource = DB.GetTableFromSQL(sql_tipo);
            bs_tipo.Sort = "descricao";                  
            // ComboBox
            cmbTipoProduto.DataSource = null;
            cmbTipoProduto.DataSource = bs_tipo;
            cmbTipoProduto.DisplayMember = "descricao";
            cmbTipoProduto.ValueMember = "tipo";           
        }

        private void SetBindingsGrupo()
        {
            // tabela Produto_tipo
            string sql_grupo =
                @"SELECT grupo, descricao from PRODUTO_GRUPO ORDER BY descricao";
            // Bindingsource
            bs_grupo = new BindingSource();
            bs_grupo.DataSource = DB.GetTableFromSQL(sql_grupo);
            bs_grupo.Sort = "descricao";
            // ComboBox
            cmbGrupo.DataSource = null;
            cmbGrupo.DataSource = bs_grupo;
            cmbGrupo.DisplayMember = "descricao";
            cmbGrupo.ValueMember = "grupo";
        }

        private void SetBindingsUnidade()
        {
            // tabela Produto_unidade
            string sql_unidade =
                @"SELECT unidade, descricao from PRODUTO_UNIDADE ORDER BY descricao";
            // Bindingsource
            bs_unidade = new BindingSource();
            bs_unidade.DataSource = DB.GetTableFromSQL(sql_unidade);
            bs_unidade.Sort = "descricao";
            // ComboBox
            cmbUnidade.DataSource = null;
            cmbUnidade.DataSource = bs_unidade;
            cmbUnidade.DisplayMember = "descricao";
            cmbUnidade.ValueMember = "unidade";
        }
       
        private void SetupDatagridView()
        {
            dgvProduto.AutoGenerateColumns = true;      
            DataTable datasource = GetProdutos(this.DESCRICAO);
            OsProdutoBindingSource.DataSource = datasource;
            // ajustar DatagridView
            //dgvProduto.AutoGenerateColumns = true;

            dgvProduto.Columns["vr_manutencao"].DefaultCellStyle.Format = "C2";
            dgvProduto.Columns["vr_manutencao"].DefaultCellStyle.FormatProvider = CultureInfo.GetCultureInfo("pt-BR");

            dgvProduto.Columns["preco"].DefaultCellStyle.Format = "C2";
            dgvProduto.Columns["preco"].DefaultCellStyle.FormatProvider = CultureInfo.GetCultureInfo("pt-BR");

            dgvProduto.Columns["comissao"].DefaultCellStyle.Format = "0.00\\%";
            dgvProduto.Columns["comissao"].DefaultCellStyle.FormatProvider = CultureInfo.GetCultureInfo("pt-BR");

            dgvProduto.Columns["vl_comis"].DefaultCellStyle.Format = "C2";
            dgvProduto.Columns["vl_comis"].DefaultCellStyle.FormatProvider = CultureInfo.GetCultureInfo("pt-BR");

            dgvProduto.Columns["vr_manutencao"].HeaderText = "Manutenção";
            dgvProduto.Columns["preco"].HeaderText = "Preço";
            dgvProduto.Columns["inativo"].HeaderText = "IA";
            dgvProduto.Columns["produto"].HeaderText = "Produto";
            dgvProduto.Columns["descricao"].HeaderText = "Descrição";
            dgvProduto.Columns["comissao"].HeaderText = "Com.(%)";
            dgvProduto.Columns["vl_comis"].HeaderText = "Comissão";
            dgvProduto.Columns["desc_tipo"].HeaderText = "Tipo";
            dgvProduto.Columns["desc_grupo"].HeaderText = "Grupo";
            dgvProduto.Columns["desc_unidade"].HeaderText = "Unidade";

            DataGridViewColumn column = dgvProduto.Columns["produto"];
            column.Width = 50;
            //DataGridViewTextBoxColumn
            column = dgvProduto.Columns["inativo"];
            column.Width = 20;
            column = dgvProduto.Columns["descricao"];
            column.Width = 200;

            dgvProduto.Columns["id"].Visible = false;
            //dgvProduto.Columns["postoDataGridViewTextBoxColumn"].Visible = false;
            dgvProduto.Columns["tipo"].Visible = false;
            dgvProduto.Columns["grupo"].Visible = false;
            dgvProduto.Columns["uni_venda"].Visible = false;


            this.a30PRODUTOTableAdapter1.Fill(this.dtsA30Produto.A30PRODUTO);    
        }

        private DataTable GetProdutos(string descricao = "")
        {
            string sql = @"select produto,inativo, vr_manutencao, A30Produto.descricao, preco,comissao, vl_comis, 
                                     PRODUTO_TIPO.descricao desc_tipo, 									
									 PRODUTO_GRUPO.descricao desc_grupo, 
									 PRODUTO_UNIDADE.descricao desc_unidade, 									                                    
                                     dt_lancto Lançamento,  A30Produto.id,
									 A30Produto.tipo tipo,
									 A30Produto.grupo grupo, 
									 uni_venda									 									                                   
                                     FROM A30Produto  
									 LEFT OUTER JOIN PRODUTO_TIPO
									 ON A30Produto.tipo = PRODUTO_TIPO.tipo
									 LEFT OUTER JOIN PRODUTO_GRUPO
									 ON A30Produto.grupo = PRODUTO_GRUPO.grupo
									 LEFT OUTER JOIN PRODUTO_UNIDADE
									 ON A30Produto.uni_venda = PRODUTO_UNIDADE.unidade
                                     WHERE 1=1 and (posto = '001' ) ";
            if (descricao.Length > 0)
            {
                sql += @" AND Upper( A30Produto.descricao) like '%" + descricao + "%'";
            }

            if (cbFiltroAtivo.Checked)
            {
                sql += @" AND inativo = 0 ";
            }
            else {
                sql += @" AND inativo = 1 ";
            }

            sql += @" ORDER BY dt_lancto desc,  A30Produto.descricao";
            DataTable data = DB.GetTableFromSQL(sql);
            return data;
        }

        private void limparCampos()
        {
            tbNome.Clear();
            tbComissao.Clear();
            tbComissaoValor.Clear();            
            tbPreco.Clear();
            tbManutencao.Clear();

            cbAtivo.Checked = true;
            cmbTipoProduto.SelectedIndex = -1;
            cmbGrupo.SelectedIndex = -1;
            cmbUnidade.SelectedIndex = -1;

            var x = "0,00000";
            tbComissao.Text = formatTextForTextBox(x, 4);           
            tbComissaoValor.Text = formatTextForTextBox(x, 8);            
            tbPreco.Text = formatTextForTextBox(x, 8);

        }

        private void UpdateMe()
        {           
            DataRowView current = (DataRowView)OsProdutoBindingSource.Current;
            string id = current["id"].ToString();
            string produto = tbCodigo.Text;
            string descricao = tbNome.Text;
            string inativo = cbAtivo.Checked ? "0" : "1";

            string s_comissaoPercento = comissaoPercento.ToString().Replace(",", ".");
            string s_comissaoValor = comissaoValor.ToString().Replace(",", ".");
            string s_preco = preco.ToString().Replace(",", ".");
            string s_vr_manutencao = vr_manutencao.ToString().Replace(",", ".");  
                          
            string sql_update = @"UPDATE A30Produto SET "
                              + "produto = '" + produto + "',"
                              + "descricao = '" + descricao + "',"
                              + "inativo = " + inativo + ","

                              + "vr_manutencao = " + s_vr_manutencao + ","

                              + "comissao = " + s_comissaoPercento + ","
                              + "vl_comis = " + s_comissaoValor + ","
                              + "preco = " + s_preco + ","
                              + "tipo = '" + TIPO + "',"
                              + "grupo = '" + GRUPO + "',"
                              + "uni_venda = '" + UNIDADE + "'"
                              + " WHERE id=" + id;                      
            try 
            {
                // update database        
                DB.ExecuteNonQuery(sql_update);
                // update BindingSource ...
                this.OsProdutoBindingSource.EndEdit();
                this.a30PRODUTOTableAdapter1.Update(this.dtsA30Produto.A30PRODUTO);

                SetupDatagridView();
            }
             catch (Exception ie) 
            {
                MessageBox.Show(ie.Message,
                              "E R R O - Update",
                              MessageBoxButtons.OK,
                              MessageBoxIcon.Information);
               
            }
            MessageBox.Show(@"Produto alterado com sucesso!");
        }

        private void DeleteMe()
        {           
            DataRowView current = (DataRowView)OsProdutoBindingSource.Current;
            var id = current["id"];
           
            string sql_delete = @"DELETE FROM A30Produto WHERE id = " + id;
            try
            {
                OsProdutoBindingSource.EndEdit();
                a30PRODUTOTableAdapter1.Update(this.dtsA30Produto);

                DB.ExecuteNonQuery(sql_delete);
                MessageBox.Show(@"Produto descartado com sucesso!");
                pesquisar();              
                bnDescartar.Enabled = false;
                SetupDatagridView();
                bnConsultar.PerformClick();
                dgvProduto.FirstDisplayedScrollingRowIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,
                                "E R R O - Delete",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }
        
        private int InsertMe()
        {                                              
            DateTime registro = DateTime.Now;

            string produto = tbCodigo.Text;
            string descricao = tbNome.Text;
            string inativo = cbAtivo.Checked ? "0" : "1";
            //string posto = "001";

            string s_comissaoPercento = comissaoPercento.ToString().Replace(",",".");
            string s_comissaoValor = comissaoValor.ToString().Replace(",", ".");
            string s_preco = preco.ToString().Replace(",", ".");
            string s_vr_manutencao = vr_manutencao.ToString().Replace(",", "."); 

            string sql = @"INSERT INTO A30Produto(produto, descricao, tipo, grupo, comissao, vr_manutencao,vl_comis, dt_lancto, preco, uni_venda, inativo, posto) 
                           VALUES('" + produto + "', '"
                                   + descricao + "', '"
                                   + TIPO + "', '"
                                   + GRUPO + "',"
                                   + s_comissaoPercento + ", "
                                   + s_vr_manutencao + ", "
                                   + s_comissaoValor + ", '"
                                   + registro + "', "
                                   + s_preco + ", '"
                                   + UNIDADE + "', "                                 
                                   + inativo
                                   + ",'001'"
                                   + ")";
            try
            {
                DB.ExecuteNonQuery(sql);
            }
            catch (Exception ie) 
            {
                MessageBox.Show(ie.Message,
                              "E R R O - Insert",
                              MessageBoxButtons.OK,
                              MessageBoxIcon.Error);
                return -1;
            }
            MessageBox.Show(@"Produto cadastrado com sucesso!");
            return 0;             
        }

        private void bnConsultar_Click(object sender, EventArgs e)
        {
            
            tableLayoutPanelConsultar.Visible = true;
            tableLayoutPanelEditar.Visible = false;

            OsProdutoBindingSource.CancelEdit();
            
            bnDescartar.Enabled = false;
            bnInserir.Enabled = true;
            bnPrimeiro.Enabled = true;
            bnAnterior.Enabled = true;            
           
            pesquisar();

            cmbTipoProduto.DroppedDown = false;
        }

        private void bnInserir_Click(object sender, EventArgs e)
        {
            // get next codigo produto A30PRODUTO.produto (varchar(6))
            tbCodigo.Text = proximoProduto();
            
            tableLayoutPanelConsultar.Visible = false;
            tableLayoutPanelEditar.Visible = true;

            inserir();
           
            limparCampos();

            bnConsultar.Enabled = true;
            bnCancelar.Enabled = true;
            bnInserir.Enabled = false;
            bnPrimeiro.Enabled = false;
            bnAnterior.Enabled = false;
            bnDescartar.Enabled = false;

            tbManutencao.Enabled = false;

            cmbTipoProduto.Focus();
            cmbTipoProduto.Select();
            cmbTipoProduto.DroppedDown = true;
            // HACK: porque o HIGHLIGHT (retângulo pontilhado não mostra quando feito visível pela primeira vez!)
            // veja: https://social.msdn.microsoft.com/Forums/vstudio/en-US/580d8072-e322-44c6-9772-0460b5824b45/combobox-doesnt-highlight-when-selected-on-form-load?forum=vbgeneral
            //SuspendLayout();
            //SendKeys.SendWait("{TAB}");
            //SendKeys.SendWait("+{TAB}");
            //cmbTipoProduto.Enter += cmb_DropMeDown;
            //ResumeLayout();
            
        }
        
        private string proximoProduto() {
            // get next string Produto (varchar(6)) with leading zeros.
            var sql = @"select RIGHT('00000' + cast( max(cast(produto as int) ) + 1 as varchar), 6)  from a30produto";

            return DB.GetSingleValue(sql).ToString();
        }
        
        private void errorMessage(string m)
        {
            MessageBox.Show(m,
                               "E R R O - Validação",
                               MessageBoxButtons.OK,
                               MessageBoxIcon.Error);
        }

        private Decimal validarDecimal(TextBox tb)
        {
            Decimal retval = -1.0M;

            string s = tb.Text;
            s = s.Replace("R$ ", "");
            s = s.Replace(",", ".");


            try {                
                retval = decimal.Parse(s, CultureInfo.InvariantCulture);
            }
            catch (Exception e) {
                return -2.0M;
            }

            return retval;
        }
        //private Decimal validarDecimal(MaskedTextBox mtb)
        //{
        //    Decimal retval = -1.0M;

        //    mtb.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
        //    if (String.IsNullOrWhiteSpace(mtb.Text))
        //    {
        //        mtb.Text = "0";                
        //    }

        //    mtb.TextMaskFormat = MaskFormat.IncludePromptAndLiterals;
        //    var x = mtb.Text;
        //    x = Regex.Replace(x, @"[^\d,]+", "");
          
        //    try
        //    {
        //        x = x.Replace(",", ".");
        //        // setze Dezimalpunkt, falls nicht vorhanden
        //        if (!(x.Contains("."))) {
        //            int lx = x.Length;
        //            string a = x.Substring(0, lx - 2);
        //            string b = x.Substring(lx-2);
        //            x = a + "." + b;
        //        }
                                
        //        retval = Convert.ToDecimal(x, CultureInfo.InvariantCulture);
        //        return retval;
        //    }
        //    catch (FormatException fe)
        //    {
        //        errorMessage(@"<Comissão - Valor> erro do formato."); 
        //        return retval;
        //    }
           
        //}

        private Boolean validar()
        {         
            // set global variables 
            // nome não pode ser vazio
            if (String.IsNullOrWhiteSpace(tbNome.Text))
            {
                errorMessage(@"<Nome> não pode ser vazio.");
                return false;
            }
            // comissao percento - global
            comissaoPercento = validarDecimal(tbComissao);
            if (comissaoPercento < 0)
            {
                return false;
            }
            // comissao valor - global
            comissaoValor = validarDecimal(tbComissaoValor);
            if (comissaoValor  < 0) {               
                return false;
            }
            // preço - global
            preco = validarDecimal(tbPreco);
            if (preco < 0)
            {
                return false;
            }
            // vr_manutencao - global       
            if (((DataRowView)cmbTipoProduto.SelectedItem)[1].Equals("Software"))
            {
                vr_manutencao = validarDecimal(tbManutencao);
                if (vr_manutencao < 0)
                {
                    return false;
                }
            }
            // comboboxes
            if (cmbTipoProduto.SelectedIndex == -1) {
                errorMessage("Favor, escolher o <tipo> do produto.");
                return false;
            }
            if (cmbGrupo.SelectedIndex == -1)
            {
                errorMessage("Favor, escolher o <grupo> do produto.");
                return false;
            }
            if (cmbUnidade.SelectedIndex == -1)
            {
                errorMessage("Favor, escolher a <unidade> do produto.");
                return false;
            }

            // get binded comboboxes values
            DataRowView currentTipo = (DataRowView)bs_tipo.Current;
            TIPO = currentTipo["tipo"].ToString();           
            DataRowView currentGrupo = (DataRowView)bs_grupo.Current;
            GRUPO = currentGrupo["grupo"].ToString();
            DataRowView currentUnidade = (DataRowView)bs_unidade.Current;
            UNIDADE = currentUnidade["unidade"].ToString();
            
            return true;
        }

        private void bnGravar_Click(object sender, EventArgs e)
        {
            DataRowView current = (DataRowView)OsProdutoBindingSource.Current;

            var xid = current["id"];
            var xdesc = current["descricao"];

            Boolean ok = validar();
            if (!ok)
            {
                return;
            }

            if (estadoCorrente == EstadoManutencao.smInserir)
            {
                int res = InsertMe();
                if (res == 0)
                {
                    bnInserir.Enabled = true;
                    pesquisar();                    
                    SetupDatagridView();                   
                    bnConsultar.PerformClick();
                    dgvProduto.FirstDisplayedScrollingRowIndex = 0;
                    try
                    {
                        dgvProduto.Rows[0].Selected = true;                       
                    }
                    catch (Exception de) { }
                   
                }               
            }

            if (estadoCorrente == EstadoManutencao.smEditar)
            {
                UpdateMe();
            }

            bnInserir.Enabled = true;
            bnPrimeiro.Enabled = true;
            bnAnterior.Enabled = true;
            bnConsultar.PerformClick();
        }

        private void bnCancelar_Click(object sender, EventArgs e)
        {
            OsProdutoBindingSource.CancelEdit();
            tableLayoutPanelConsultar.Visible = true;
            tableLayoutPanelEditar.Visible = false;
           
            bnCancelar.Enabled = false;
            bnDescartar.Enabled = false;
            bnInserir.Enabled = true;
            bnPrimeiro.Enabled = true;
            bnAnterior.Enabled = true;
           
            pesquisar();
           
        }

        private void bnDescartar_Click(object sender, EventArgs e)
        {
            string pergunta = "Deseja realmente descartar esse produto ?";
            if (MessageBox.Show(pergunta,
                               "alerta da confirmação",
                               MessageBoxButtons.YesNo,
                               MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                DeleteMe();
            }
        }

        private void dgvProduto_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            selectProduto();
        }

        private void dgvProduto_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.S)
            {
                selectProduto();
            }
        }

        private void selectProduto()
        {
            limparCampos();
            editar();
            tableLayoutPanelConsultar.Visible = false;
            tableLayoutPanelEditar.Visible = true;

            bnCancelar.Enabled = true;
            bnDescartar.Enabled = true;

            // preencher campos atuais
            DataRowView current = (DataRowView)OsProdutoBindingSource.Current;
           
            cmbGrupo.SelectedIndex = bs_grupo.Find("grupo", current["grupo"]);
            cmbUnidade.SelectedIndex = bs_unidade.Find("unidade", current["uni_venda"]);

            cbAtivo.Checked = current["inativo"].ToString().Equals("0");
            tbCodigo.Text = current["produto"].ToString();
            tbNome.Text = current["descricao"].ToString();

            var x = current["comissao"].ToString();
            tbComissao.Text = formatTextForTextBox(x, 4);

            x = current["vl_comis"].ToString();
            tbComissaoValor.Text = formatTextForTextBox(x, 8);

            x = current["preco"].ToString();
            tbPreco.Text = formatTextForTextBox(x, 8);

            x = current["vr_manutencao"].ToString();
            tbManutencao.Text = formatTextForTextBox(x, 8);

            tbManutencao.Enabled = false;
            cmbTipoProduto.SelectedIndex = bs_tipo.Find("tipo", current["tipo"]);
            // if "Software" ...
            //if (cmbTipoProduto.SelectedIndex == 2)
            if (((DataRowView)cmbTipoProduto.SelectedItem)[1].Equals("Software"))
            {
                tbManutencao.Enabled = true;               
            }
            else
            {
                tbManutencao.Enabled = false;               
            }
            cmbTipoProduto.Focus();
            cmbTipoProduto.Select();

            cmbTipoProduto.DroppedDown = true;
            // HACK: porque o HIGHLIGHT (retângulo pontilhado não mostra quando feito visível pela primeira vez!)
            // veja: https://social.msdn.microsoft.com/Forums/vstudio/en-US/580d8072-e322-44c6-9772-0460b5824b45/combobox-doesnt-highlight-when-selected-on-form-load?forum=vbgeneral
            //SuspendLayout();            
            //SendKeys.SendWait("{TAB}");
            //SendKeys.SendWait("+{TAB}"); // shift-tab
            //cmbTipoProduto.Enter += cmb_DropMeDown;
            //ResumeLayout(); 
            ////cmbTipoProduto.Focus();
            ////cmbTipoProduto.Select();
            //cmbTipoProduto.BeginInvoke(new MethodInvoker(cmbTipoProduto.SelectAll));
                      
        }
        
        /*
         * DB field decimal(14,5)
         * - die letzten 3 "0" enfernen
         * - komma entfernen
         * - lpad blanks -> fillTo
         * */
        private string formatTextForMask(string dec, int fillTo) 
        {
            string retval = dec;
            retval = retval.Substring(0, retval.Length - 3);
            retval = retval.Replace(",", "");
            retval = retval.PadLeft(fillTo,'0');
            return retval;
        }
        private string formatTextForTextBox(string dec, int fillTo)
        {
            string retval = dec;
            retval = retval.Substring(0, retval.Length - 3);
            //retval = retval.Replace(",", "");
            //retval = retval.PadLeft(fillTo, '0');
            if (fillTo == 8)
                retval = "R$ " + retval;
            return retval;
        }
        
        private void formatTextBoxOnLeave(TextBox tb) {
         tb.Text = "R$ " + tb.Text;
         if (tb.Text.IndexOf(",") == -1) tb.Text += ",00";
        }
        // teste - visualizar layoute
        private void pictureBox2_Click(object sender, EventArgs e)
        {
            //
            trackBar1.Visible = !trackBar1.Visible;
            tbTrackValue.Visible = !tbTrackValue.Visible;
            lblTrackBar.Visible = !lblTrackBar.Visible;
                
            //  
            if (panel2Editar.BackColor == System.Drawing.Color.Transparent)
                panel2Editar.BackColor = System.Drawing.Color.ForestGreen;
            else
                panel2Editar.BackColor = System.Drawing.Color.Transparent;
            //
            if (panel4Editar.BackColor == System.Drawing.Color.Transparent)
                panel4Editar.BackColor = System.Drawing.Color.ForestGreen;
            else
                panel4Editar.BackColor = System.Drawing.Color.Transparent;
            //
            if (tableLayoutPanelEditar.BackColor == System.Drawing.Color.Transparent)
                tableLayoutPanelEditar.BackColor = System.Drawing.Color.BurlyWood;
            else
                tableLayoutPanelEditar.BackColor = System.Drawing.Color.Transparent;
            // 
            if (labelDummyAtivo.Text.Equals("DUMMY"))
                labelDummyAtivo.Text = "";
            else
                labelDummyAtivo.Text = "DUMMY";
            if (tableLayoutPanelDados.CellBorderStyle == TableLayoutPanelCellBorderStyle.Single)
                tableLayoutPanelDados.CellBorderStyle = TableLayoutPanelCellBorderStyle.None;
            else
                tableLayoutPanelDados.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            //
            foreach (Control c in tableLayoutPanelDados.Controls)
            {
                if (c.GetType() == typeof(TableLayoutPanel))
                {                    
                    if (c.BackColor == System.Drawing.Color.Transparent)
                      c.BackColor = System.Drawing.Color.Silver;
                    else
                      c.BackColor = System.Drawing.Color.Transparent;
                }
            }
            TableLayoutColumnStyleCollection columnStyles;
            columnStyles = tableLayoutPanelDados.ColumnStyles;
            columnStyles[0].SizeType = SizeType.Absolute;
            //float w = columnStyles[0].Width;
            //float w12 = w * 12;
            float totalWidth = tableLayoutPanelDados.Width;
            float wpixel = totalWidth / 20;

            if (panelFillerRow1.BackColor == System.Drawing.Color.DarkRed)
            {
                panelFillerRow1.BackColor = System.Drawing.Color.Transparent;
                columnStyles[0].Width = 10; 
            }
            else {
                panelFillerRow1.BackColor = System.Drawing.Color.DarkRed;
                columnStyles[0].Width = wpixel; 
            }

            if (panelFillRow1ColLast.BackColor == System.Drawing.Color.DarkRed)
            {
                panelFillRow1ColLast.BackColor = System.Drawing.Color.Transparent;
                columnStyles[0].Width = 10;
            }
            else
            {
                panelFillRow1ColLast.BackColor = System.Drawing.Color.DarkRed;
                columnStyles[0].Width = wpixel;
            }
        }

        private void btnResizeColWidth_Click(object sender, EventArgs e)
        {
            // NOT IN USE
            TableLayoutRowStyleCollection rowStyles;
            TableLayoutColumnStyleCollection columnStyles;

            MessageBox.Show("Hi there!");
            rowStyles = tableLayoutPanelDados.RowStyles;
            columnStyles = tableLayoutPanelDados.ColumnStyles;

            float w = columnStyles[0].Width;
            float w12 = w*12;
            float totalWidth = tableLayoutPanelDados.Width;
            float wpixel = totalWidth / 20;

            columnStyles[0].SizeType = SizeType.Absolute;
            columnStyles[0].Width = wpixel*2; 
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            ////Debug.WriteLine(trackBar1.Value.ToString());            
            //float totalWidth = tableLayoutPanelDados.Width;
            //float wpixel = totalWidth / 20;

            //TableLayoutColumnStyleCollection columnStyles;
            //columnStyles = tableLayoutPanelDados.ColumnStyles;
            //columnStyles[19].SizeType = SizeType.Absolute;
            //columnStyles[19].Width = wpixel * trackBar1.Value;

            //tbTrackValue.Text = trackBar1.Value.ToString();
        }

        /*
         "Bindings" feitos "manualmente", já que é dificil um binding em "maskedTextBox" ...!!
         */

        private void bnPrimeiro_Click(object sender, EventArgs e)
        {
            selectProduto();
        }

        private void bnAnterior_Click(object sender, EventArgs e)
        {
            selectProduto();
        }

        private void bnProximo_Click(object sender, EventArgs e)
        {
            selectProduto();
        }

        private void bnUltimo_Click(object sender, EventArgs e)
        {
            selectProduto();
        }

        private void cbFiltroAtivo_CheckedChanged(object sender, EventArgs e)
        {
            SetupDatagridView();          
        }

        private void tbFiltroNome_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.DESCRICAO = tbFiltroNome.Text;
                SetupDatagridView();  
            }
        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            //Debug.WriteLine(trackBar1.Value.ToString());            
            float totalWidth = tableLayoutPanelDados.Width;
            float wpixel = totalWidth / 20;

            TableLayoutColumnStyleCollection columnStyles;
            columnStyles = tableLayoutPanelDados.ColumnStyles;
            columnStyles[19].SizeType = SizeType.Absolute;
            columnStyles[19].Width = wpixel * trackBar1.Value;

            tbTrackValue.Text = trackBar1.Value.ToString();
        }

        private void tbNome_Enter(object sender, EventArgs e)
        {
            tbNome.SelectionStart = 0;
            tbNome.SelectionLength = tbNome.Text.Length;
            tbNome.Select();          
        }

        private void tbNome_MouseUp(object sender, MouseEventArgs e)
        {
            tbNome.SelectionStart = 0;
            tbNome.SelectionLength = tbNome.Text.Length;
            tbNome.Select();           
        }

        private void tbComissao_Enter(object sender, EventArgs e)
        {           
            //mtbComissao.Mask = "####";
            //mtbComissao.BeginInvoke(new MethodInvoker(mtbComissao.SelectAll));
            tbComissao.Text = tbComissao.Text.Replace("R$ ", "");
            tbComissao.BeginInvoke(new MethodInvoker(tbComissao.SelectAll));
        }

        //private void mtbComissao_MouseUp(object sender, MouseEventArgs e)
        //{

        //}

        private void tbComissaoValor_Enter(object sender, EventArgs e)
        {           
          //  tbComissaoValor.Mask = "########";
          // tbComissaoValor.BeginInvoke(new MethodInvoker(mtbComissaoValor.SelectAll));
            tbComissaoValor.Text = tbComissaoValor.Text.Replace("R$ ", "");
            tbComissaoValor.BeginInvoke(new MethodInvoker(tbComissaoValor.SelectAll));
        }

        private void mtbComissaoValor_MouseUp(object sender, MouseEventArgs e)
        {

        }

        //private void mtbPreco_Enter(object sender, EventArgs e)
        //{          
        //    //mtbPreco.Mask = "########";
        //    tbPreco.Text = tbPreco.Text.Replace("R$ ", "");
        //    tbPreco.BeginInvoke(new MethodInvoker(tbPreco.SelectAll));
        //}

       // private void mtbPreco_MouseUp(object sender, MouseEventArgs e)
       // {
       // }

        private void tbComissaoValor_Leave(object sender, EventArgs e)
        {           
            //mtbComissaoValor.Text = mtbComissaoValor.Text.PadLeft(8, '0');
            //mtbComissaoValor.Mask = "$ ###,###.##";
            formatTextBoxOnLeave(tbComissaoValor);
        }       

       

        private void tbComissao_Leave(object sender, EventArgs e)
        {
            //mtbComissao.Text = mtbComissao.Text.PadLeft(4, '0');
            //mtbComissao.Mask = "##.##";
            formatTextBoxOnLeave(tbComissao);
        }

        private void tbManutencao_Enter(object sender, EventArgs e)
        {
            //mtbManutencao.Mask = "########";
            tbManutencao.Text = tbManutencao.Text.Replace("R$ ", "");
            tbManutencao.BeginInvoke(new MethodInvoker(tbManutencao.SelectAll));
        }

        private void tbManutencao_Leave(object sender, EventArgs e)
        {
            //tbManutencao.Text = tbManutencao.Text.PadLeft(8,'0');
            //mtbManutencao.Mask = "$ ###,###.##";
            //tbManutencao.Text = "R$ " + tbManutencao.Text;
            formatTextBoxOnLeave(tbManutencao);
        }

        private void tbManutencao_MouseUp(object sender, MouseEventArgs e)
        {

        }

        private void cmbTipoProduto_SelectionChangeCommitted(object sender, EventArgs e)
        {
            // sonst waere es moeglich, zuerst einen Wert einzugeben und dann auf etwas
            // anderes als Software zu changen ...
            tbManutencao.Clear();

            tbManutencao.Enabled = false;
            var item = cmbTipoProduto.SelectedItem;
            if (item != null)
                if (((DataRowView)item)[1].Equals("Software"))
                {
                    tbManutencao.Enabled = true;
                }
        }

        private void cmb_DropMeDown(object sender, EventArgs e)
        {
            ((ComboBox)sender).DroppedDown = true;
        }

        private void enter4tab_KeyDown(object sender, KeyEventArgs e)
        {
            
            
            Control ctrl = (Control)sender;
           
                if (e.KeyCode == Keys.Enter && !e.Shift)
                {
                    this.SelectNextControl(ctrl, true, true, true, true);
                }
                else if (e.KeyCode == Keys.Enter && e.Shift)
                {
                    this.SelectNextControl(ctrl, false, true, true, true);

                }
                else
                    return;            
        }

        private void cbAtivo_Enter(object sender, EventArgs e)
        {            
            //SendKeys.SendWait("{TAB}");
            //SendKeys.SendWait("+{TAB}");
        }

        private void tb_OnlyDigits_KeyPress(object sender, KeyPressEventArgs e)
        {          
            // Control, Digit and one ',' allowed
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && !(e.KeyChar == ','))
            {
                e.Handled = true;
            }
            // only allow one comma
            if (e.KeyChar == ','
               && (sender as TextBox).Text.IndexOf(',') > -1)
            {
                e.Handled = true;
            }
        }

        private void tbPreco_MouseUp(object sender, MouseEventArgs e)
        {

        }

        private void tbPreco_Enter(object sender, EventArgs e)
        {           
            tbPreco.Text = tbPreco.Text.Replace("R$ ", "");
            tbPreco.BeginInvoke(new MethodInvoker(tbPreco.SelectAll));
        }

        private void tbPreco_Leave(object sender, EventArgs e)
        {
            //mtbPreco.Text = mtbPreco.Text.PadLeft(8, '0');
            //mtbPreco.Mask = "$ ###,###.##";
            formatTextBoxOnLeave(tbPreco);
        }

        private void tbComissaoValor_MouseUp(object sender, MouseEventArgs e)
        {

        }

        private void tbComissao_MouseUp(object sender, MouseEventArgs e)
        {

        }

        private void cmbTipoProduto_Leave(object sender, EventArgs e)
        {
            tbManutencao.Clear();

            tbManutencao.Enabled = false;
            var item = cmbTipoProduto.SelectedItem;
            if (item != null)
                if (((DataRowView)item)[1].Equals("Software"))
                {
                    tbManutencao.Enabled = true;
                }
        }
    }
}