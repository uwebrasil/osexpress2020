﻿ using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using CrystalDecisions.ReportAppServer.DataDefModel;
using Microsoft.Win32;
using TExpress;
using System.Diagnostics;

namespace osExpress.Manutencoes
{
   
    public partial class ManPedido : osExpress.FrmPrincipal
    {
       
        private BindingSource bs_cabecalho;
        private BindingSource bs_empresa; // ... para administrar a variável global OsComfiguracoes.POSTO
        private BindingSource bs_postoos; // (KOPIE bs_empresa) ... para filtrar em OsOrdemServico por IDPOSTO

        private RelPedido relPedido;
        EnviaEmail enviaEmail;
        string corpoPedido;
        string isSingle = "0";      
        //hack

        private bool limpa_codigo_osexpress = true;
       
        public ManPedido()
        {
            InitializeComponent();
            corrigirLayout();
            pnlConsultar.Visible = true;
            // 29.05.2014 Uwe
            //tsbGerarCaixa.Enabled = false;      
     
            DB.ConnectionName = "osExpress.Properties.Settings.OsExpressConnectionString";
            #region Grupos de Produtos
            try
            {
                //DB.FillComboBox(cmbGrupoProduto,
                //    "select (grupo + ' - ' + descricao) val from a30grupo order by descricao");

                string xposto = OsConfiguracoes.idPosto.ToString();
                xposto = xposto.PadLeft(3, '0');
                string gsql =
                    @"select val from ( 
                               select (grupo + ' - ' + descricao) val from a30grupo
                                       where grupo in 
                               ( select grupo from a30produto where tipo <> 1 and inativo = 0 and posto ='";
                    gsql += xposto+@"')
                               union select '000 -  Todos os Produtos' val   
                               ) abc
                               order by substring(val,7,3),substring(val,1,3)"; 
                                              
                DB.FillComboBox(cmbGrupoProduto, gsql);
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message,@"E R R O ManPedido-ctor");
            }
            #endregion
            #region novo "BindingSource" para tirar linhas dubladas, tripladas ...
            // 27.02.2015 Uwe
            // escolher a linha de min(id) !!
            // Combobox DataSource era: a30ProdutoBindingSource
           
            // Tirar DataBindings de TexBox Preco
            // txtPrecoProduto.DataBindings ERA
            // TEXT: A30ProdutoBindingSource - preco

            cmbProdutosSetBindings();


            #endregion
            // mod 23.03.2015 uwe
            // 15.12.2020 uwe
            // Columns[3] = Total_Os => agora 4, pois IDPOSTO foi acrescendido
            this.osOrdemServicoDataGridView.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.osOrdemServicoDataGridView.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.osOrdemServicoDataGridView.Columns[4].DefaultCellStyle.Format = "c";
            //this.osOrdemServicoDataGridView.Columns[3].DefaultCellStyle.Format = "##0.00";
            // end mod
            // label 28 fica no mesmo lugar - data bound a bdsFiltroCliente-Contato A30Clientes !?
            // função de label30 : osCliente !? esta sem binding

            // esconder campo contato
            label30.SendToBack();

            label30.Visible = false;
            label28.Visible = false;
            label26.Visible = false;

            //// 2018 filtrar por cliente nome
            //string xsql =
            //   @"select id,nome from A30Clientes A where posto='" + OsConfiguracoes.codigoPosto;
            //xsql += "' and A.ID = (select MIN(id) from A30Clientes where cliente=a.cliente) ";

            //string xsql =
            //    @"select razao_social from OsCliente group by razao_social";

            /* 26.01.
            string xsql = @"
                select 
                c.nome
                 from osordemservico os, a30clientes c
                 where 1=1
                  and os.idcliente = c.id
                  and not os.idcliente is null 
                  and tipo_doc != 'S' 
                  and idposto = "+OsConfiguracoes.idPosto+@"  
                  group by c.nome
                union
                select 
                c.razao_social
                 from osordemservico os, oscliente c
                 where 1=1
                  and os.idcliente_pedido = c.id
                  and os.idcliente is null
                  and not os.idcliente_pedido is null 
                  and tipo_doc != 'S' 
                  and idposto = " + OsConfiguracoes.idPosto + @" 
                  group by c.razao_social  
                 order by 1
            ";
            BindingSource bs = new BindingSource();
            var mysource = DB.GetTableFromSQL(xsql);
            bs.DataSource = mysource;
            bs.Sort = "nome";

            // Nome
            cmbFiltroClienteNome.DataSource = null;
            cmbFiltroClienteNome.DataSource = bs;
            cmbFiltroClienteNome.DisplayMember = "nome";
            cmbFiltroClienteNome.ValueMember = "nome";
            */

            // Painel Consultar : Filtro por nome 
            string xsql = @"
                select 
                c.razao_social nome
                 from osordemservico os, oscliente c
                 where 1=1
                  and os.idcliente_pedido = c.id
                  and os.idcliente is null
                  and not os.idcliente_pedido is null 
                  and tipo_doc != 'S' 
                  and idposto = " + OsConfiguracoes.idPosto + @" 
                  group by c.razao_social  
                 order by 1
            ";
            BindingSource bs = new BindingSource();
            var mysource = DB.GetTableFromSQL(xsql);
            bs.DataSource = mysource;
            bs.Sort = "nome";

            // Nome
            cmbFiltroClienteNome.DataSource = null;
            cmbFiltroClienteNome.DataSource = bs;
            cmbFiltroClienteNome.DisplayMember = "nome";
            cmbFiltroClienteNome.ValueMember = "nome";


            #region Empresa
            // BindingSource bs_empresa = new BindingSource();
            var sql_empresa = @"SELECT id, razao, codigo FROM A30POSTOS";
            var empresa = DB.GetTableFromSQL(sql_empresa);
            bs_empresa = new BindingSource();
            bs_empresa.DataSource = empresa;
            bs_empresa.Sort = "codigo";

           
            cmbEmpresa.DataSource = null;
            cmbEmpresa.DataSource = bs_empresa;
            cmbEmpresa.DisplayMember = "razao";
            cmbEmpresa.ValueMember = "codigo";

            //cmbEmpresa.DataBindings.Add(
            //    new Binding("SelectedValue", bs_empresa, "codigo", true));
            cmbEmpresa.SelectedIndex = OsConfiguracoes.idPosto - 1;
            #endregion

            #region Posto OS ... filtrar empresa(posto) na consulta           
            var sql_postoos = @"SELECT id, razao, codigo FROM A30POSTOS";
            var postoos = DB.GetTableFromSQL(sql_postoos);
            bs_postoos = new BindingSource();
            bs_postoos.DataSource = empresa;
            bs_postoos.Sort = "codigo";

            cmbPostoOS.DataSource = null;
            cmbPostoOS.DataSource = bs_postoos;
            cmbPostoOS.DisplayMember = "razao";
            cmbPostoOS.ValueMember = "codigo";
           
            cmbPostoOS.SelectedIndex = - 1;
            #endregion
            #region Cabeçalho
            //// Tabpage CABEÇALHO
            //string sql_cabecalho =
            //    @"select id,nome,logotipo,cliente,cgc,inscricao,telefone1,endereco,contato,cidade,estado,obs01 from A30Clientes A where posto='" + OsConfiguracoes.codigoPosto;
            //sql_cabecalho += "' and inativo = '0' and tipo_pessoa = 'J";
            ////sql_cabecalho += "' and A.ID = (select MIN(id) from A30Clientes where cliente=a.cliente)";

            //sql_cabecalho += "' and A.ID = (select MIN(id) from A30Clientes where cliente=a.cliente and posto='"
            //                   + OsConfiguracoes.codigoPosto + "') ";

            //bs_cabecalho = new BindingSource();
            //var mysource_cabecalho = DB.GetTableFromSQL(sql_cabecalho);
            //bs_cabecalho.DataSource = mysource_cabecalho;
            //bs_cabecalho.Sort = "nome";           

            ///// duas "camadas" de ComboBoxes um em cima de outra ...
            //// NOVO - OSCLIENTE          
            //// EXISTENTE - A30CLIENTES
            //cmbNomeCLienteSigPosto.DataSource = null;
            //cmbNomeCLienteSigPosto.DataSource = bs_cabecalho;
            //cmbNomeCLienteSigPosto.DisplayMember = "nome";
            ////// ComboBox - CODIGO
            //// NOVO - OSCLIENTE           
            //// EXISTENTE - A30CLIENTES
            //cmbCodCLienteSigPosto.DataSource = null;
            //cmbCodCLienteSigPosto.DataSource = bs_cabecalho;
            //cmbCodCLienteSigPosto.DisplayMember = "cliente";

            //cmbCGCClienteSigposto.DataSource = null;
            //cmbCGCClienteSigposto.DataSource = bs_cabecalho;
            //cmbCGCClienteSigposto.DisplayMember = "cgc";

            //cmbLogotipoClienteSigposto.DataSource = null;
            //cmbLogotipoClienteSigposto.DataSource = bs_cabecalho;
            //cmbLogotipoClienteSigposto.DisplayMember = "logotipo";
        

            //// trocar osClienteBindingSource.SORT de DT_CADASTRO ASC por:          
            //osClienteBindingSource.Sort = "RAZAO_SOCIAL";

            //// redefinir bindings dos campos cabeçalho 
            //label22.DataBindings.Clear();
            //label22.DataBindings.Add(
            //    new Binding("Text", bs_cabecalho, "nome", true));
            //label20.DataBindings.Clear();
            //label20.DataBindings.Add(
            //    new Binding("Text", bs_cabecalho, "inscricao", true));
            //label21.DataBindings.Clear();
            //label21.DataBindings.Add(
            //    new Binding("Text", bs_cabecalho, "cgc", true));
            //label19.DataBindings.Clear();
            //label19.DataBindings.Add(
            //    new Binding("Text", bs_cabecalho, "telefone1", true));
            //label9.DataBindings.Clear();
            //label9.DataBindings.Add(
            //    new Binding("Text", bs_cabecalho, "endereco", true));
            //// Cidade, Estado - Cliente "velho"
            //lblCidade2.DataBindings.Clear();
            //lblCidade2.DataBindings.Add(
            //    new Binding("Text", bs_cabecalho, "cidade", true));
            //lblUF2.DataBindings.Clear();
            //lblUF2.DataBindings.Add(
            //    new Binding("Text", bs_cabecalho, "estado", true));
           
            #endregion

            //SetBindingsHeader(); 26.01.
            // trocar osClienteBindingSource.SORT de DT_CADASTRO ASC por:          
            osClienteBindingSource.Sort = "RAZAO_SOCIAL";
        }

        private void SetBindingsHeader()
        {
            return; // 26.01.
            // Tabpage CABEÇALHO
            string sql_cabecalho =
                @"select id,nome,logotipo,cliente,cgc,inscricao,telefone1,endereco,contato,cidade,estado,obs01 from A30Clientes A where posto='" + OsConfiguracoes.codigoPosto;
            sql_cabecalho += "' and inativo = inativo and tipo_pessoa = 'J";           
            sql_cabecalho += "' and A.ID = (select MIN(id) from A30Clientes where cliente=a.cliente and posto='"
                               + OsConfiguracoes.codigoPosto + "') ";

            bs_cabecalho = new BindingSource();
            var mysource_cabecalho = DB.GetTableFromSQL(sql_cabecalho);
            bs_cabecalho.DataSource = mysource_cabecalho;
            bs_cabecalho.Sort = "nome";

            /// duas "camadas" de ComboBoxes um em cima de outra ...
            // NOVO - OSCLIENTE          
            // EXISTENTE - A30CLIENTES
            cmbNomeCLienteSigPosto.DataSource = null;
            cmbNomeCLienteSigPosto.DataSource = bs_cabecalho;
            cmbNomeCLienteSigPosto.DisplayMember = "nome";
            //// ComboBox - CODIGO
            // NOVO - OSCLIENTE           
            // EXISTENTE - A30CLIENTES
            cmbCodCLienteSigPosto.DataSource = null;
            cmbCodCLienteSigPosto.DataSource = bs_cabecalho;
            cmbCodCLienteSigPosto.DisplayMember = "cliente";

            cmbCGCClienteSigposto.DataSource = null;
            cmbCGCClienteSigposto.DataSource = bs_cabecalho;
            cmbCGCClienteSigposto.DisplayMember = "cgc";

            cmbLogotipoClienteSigposto.DataSource = null;
            cmbLogotipoClienteSigposto.DataSource = bs_cabecalho;
            cmbLogotipoClienteSigposto.DisplayMember = "logotipo";


            // trocar osClienteBindingSource.SORT de DT_CADASTRO ASC por:          
            osClienteBindingSource.Sort = "RAZAO_SOCIAL";

            // redefinir bindings dos campos cabeçalho 
            label22.DataBindings.Clear();
            label22.DataBindings.Add(
                new Binding("Text", bs_cabecalho, "nome", true));
            label20.DataBindings.Clear();
            label20.DataBindings.Add(
                new Binding("Text", bs_cabecalho, "inscricao", true));
            label21.DataBindings.Clear();
            label21.DataBindings.Add(
                new Binding("Text", bs_cabecalho, "cgc", true));
            label19.DataBindings.Clear();
            label19.DataBindings.Add(
                new Binding("Text", bs_cabecalho, "telefone1", true));
            label9.DataBindings.Clear();
            label9.DataBindings.Add(
                new Binding("Text", bs_cabecalho, "endereco", true));
            // Cidade, Estado - Cliente "velho"
            lblCidade2.DataBindings.Clear();
            lblCidade2.DataBindings.Add(
                new Binding("Text", bs_cabecalho, "cidade", true));
            lblUF2.DataBindings.Clear();
            lblUF2.DataBindings.Add(
                new Binding("Text", bs_cabecalho, "estado", true));
           
        }

        private void corrigirLayout()
        {
            cmbEmpresa.SendToBack();
            lblEmpresa.SendToBack();
            // padronizar janelas, panels, tabcontrols
            pnlConsultar.Location = new Point(10, 90);  //Helper.Layout.LocationPedidoPanel;

            grpPrincipal.Location = new Point(10, 90+30);  //Helper.Layout.LocationPedidoGroupBox;
            tabPrincipal.Location = new Point(10, 300+30);  //Helper.Layout.LocationPedidoTabControl;

            pnlConsultar.Size = Helper.Layout.SizePedidoPanel;
            grpPrincipal.Size = Helper.Layout.SizePedidoGroupBox;
            tabPrincipal.Size = Helper.Layout.SizePedidoTabControl;

            //osOrdemServicoDataGridView.Size = new Size(785,416);
            //lblTotalOS.Location = new Point(3, 545);
            //osOrdemServicoDataGridView.Size = new Size(785, 461);

            osOrdemServicoDataGridView.Size = new Size(785, 411);
            
            lblTotalOS.Location = new Point(3, 590);
            // 
            lblSituacao2.Location = new Point(lblSituacao2.Location.X, lblSituacao2.Location.Y + 45);
            lblCabSituacao.Location = new Point(lblCabSituacao.Location.X, lblCabSituacao.Location.Y + 45);
            label72.Location = new Point(label72.Location.X, label72.Location.Y + 45);
            label15.Location = new Point(label15.Location.X, label15.Location.Y + 45);
            lblCabFatura.Location = new Point(lblCabFatura.Location.X, lblCabFatura.Location.Y + 45);
            lblCabTipoPed.Location = new Point(lblCabTipoPed.Location.X, lblCabTipoPed.Location.Y + 45);
            // Cidade + Estado
            lblCidade.Location  = new Point(9,134);
            lblCidade2.Location = new Point(87, 134);
            lblCidadeNovoCliente.Location = new Point(87, 134);

            lblUF.Location  = new Point(9, 154);
            lblUF2.Location  = new Point(87, 154);
            lblUFNovoCliente.Location = new Point(87, 154);
        }

        private void cmbProdutosSetBindings(string grupo = "")
        {
            string xposto = OsConfiguracoes.idPosto.ToString();
            xposto = xposto.PadLeft(3, '0');
            // 23.01.2021 mod "tipo <> 0", era "tipo <> 1"
            // tirar posto
//            string xsql = @"select id,descricao, produto, preco from
//                                        (
//                                        select min(id) id,produto,descricao, preco 
//                                        from a30produto where tipo <> 0 and inativo = 0 and posto='";
//            xsql += xposto;
            string xsql = @"select id,descricao, produto, preco from
                                        (
                                        select min(id) id,produto,descricao, preco 
                                        from a30produto where tipo <> 0 and inativo = 0 and '1'='1";
            //xsql += xposto;
            if (grupo != "" && grupo != "000")
                xsql += "' and grupo = '" + grupo;
            xsql += @"' group by produto,descricao, preco
                                        ) A
                                        order by descricao";

            BindingSource bs = new BindingSource();
            var mysource = DB.GetTableFromSQL(xsql);
            bs.DataSource = mysource;

            // Descricao (A30Produtos)
            cmbDescricaoProduto.DataSource = null;
            cmbDescricaoProduto.DataSource = bs;
            cmbDescricaoProduto.DisplayMember = "Descricao";

            // ID (A30Produtos) - ComboBox escondido
            cmbCodigoIDProduto.DataSource = null;
            cmbCodigoIDProduto.DataSource = bs;
            cmbCodigoIDProduto.DisplayMember = "ID";

            // Produto (A30Produtos)
            cmbCodigoProduto.DataSource = null;
            cmbCodigoProduto.DataSource = bs;
            cmbCodigoProduto.DisplayMember = "Produto";

            // Preco (A30Clientes)
            txtPrecoProduto.DataBindings.Clear();
            txtPrecoProduto.DataBindings.Add(
                new Binding("Text", bs, "preco", true));
        }
        public void FillOsClienteFromReflection() {
            this.osClienteTableAdapter.Fill(this.dtsPrincipal.OsCliente);
        }
        public void CallDataGridViewCellDoubleClickFromReflection(DataGridViewCellEventArgs ea )
        {           
               osOrdemServicoDataGridView_CellDoubleClick(
                      osOrdemServicoDataGridView, new DataGridViewCellEventArgs(ea.ColumnIndex, ea.RowIndex));
            pnlConsultar.Visible = false;
            // disable all BindingNavigator ToolstripButtons
            // enable IMPRIMIR

            var toolstrips = bindingNavigator.Items.OfType<ToolStripButton>();
            foreach (var toolstrip in toolstrips) {
                toolstrip.Enabled = false;
            }
            tsbImprimirPedido.Enabled = true;

        }
        public void MakePanelInvisible()
        {
            pnlConsultar.Visible = false;
            //osOrdemServicoDataGridView_RowHeaderMouseClick(null, null);   
        }
        //public void MakePanelVisible()
        //{
        //    pnlConsultar.Visible = true;           
        //}
        public void ClickConsultar() {
            toolStripButton3_Click_1(null, EventArgs.Empty);
        }

        private void ManOrdemServico_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dtsXOSFuncionario.XOsFuncionario' table. You can move, or remove it, as needed.
            this.xOsFuncionarioTableAdapter.Fill(this.dtsXOSFuncionario.XOsFuncionario);
            //this.xOsFuncionarioTableAdapter.Fill(this.bsXOSFuncionario.XOsFuncionario);

            int w = (int)(this.Parent.ClientSize.Width * 0.9);
            int h = (int)(this.Parent.ClientSize.Height * 0.9);
            this.Size = new Size(w, h);
            int x = (int)(this.Parent.ClientSize.Width - w) / 2;
            int y = (int)(this.Parent.ClientSize.Height - h) / 2;
            this.Location = new Point(x, y);

            //tableLayoutPanelEditar.Location = new Point(tableLayoutPanelConsultar.Location.X, tableLayoutPanelConsultar.Location.Y);
            //tableLayoutPanelEditar.Dock = DockStyle.Fill;
            //tableLayoutPanelEditar.Visible = false;

            // TODO: This line of code loads data into the 'dtsPrincipal.OsParametros' table. You can move, or remove it, as needed.
            this.osParametrosTableAdapter.Fill(this.dtsPrincipal.OsParametros);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsOrdemParcela' table. You can move, or remove it, as needed.
            this.osOrdemParcelaTableAdapter.Fill(this.dtsPrincipal.OsOrdemParcela);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsCliente' table. You can move, or remove it, as needed.
            this.osClienteTableAdapter.Fill(this.dtsPrincipal.OsCliente);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsCliente' table. You can move, or remove it, as needed.
            this.osClienteTableAdapter.Fill(this.dtsPrincipal.OsCliente);
            //
            this.a30CLIENTESTableAdapter.Fill(this.dtsPrincipal.A30CLIENTES);
            //
            //this.WindowState = FormWindowState.Maximized;
            //
            this.SuspendLayout();


            //
            // TODO: This line of code loads data into the 'dtsPrincipal.OsUsuario' table. You can move, or remove it, as needed.
            this.osUsuarioTableAdapter.Fill(this.dtsPrincipal.OsUsuario);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsParContas' table. You can move, or remove it, as needed.
            this.osParContasTableAdapter.Fill(this.dtsPrincipal.OsParContas);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsItemServico' table. You can move, or remove it, as needed.
            this.osItemServicoTableAdapter.Fill(this.dtsPrincipal.OsItemServico);
            // TODO: This line of code loads data into the 'dtsPrincipal.A30PRODUTO' table. You can move, or remove it, as needed.
            this.a30PRODUTOTableAdapter.Fill(this.dtsPrincipal.A30PRODUTO);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsParSituacao' table. You can move, or remove it, as needed.
            this.osParSituacaoTableAdapter.Fill(this.dtsPrincipal.OsParSituacao);
            // TODO: This line of code loads data into the 'dtsPrincipal.OsOrdemServico' table. You can move, or remove it, as needed.
            //

            // this.osOrdemServicoTableAdapter.SqlOrcamentos(this.dtsPrincipal.OsOrdemServico);
            this.osOrdemServicoTableAdapter.SqlOrcamentosXOSFuncionario(this.dtsPrincipal.OsOrdemServico);
            //
            this.ResumeLayout();

            estadoCorrente = EstadoManutencao.smPesquisar;
            cmbFiltroSituacao.SelectedIndex = 0;
            
            //osOrdemServicoBindingSource.Filter = " TIPO_DOC <> 'S' ";

            //osOrdemServicoBindingSource.Filter = " TIPO_DOC <> 'S' AND IDPOSTO="+OsConfiguracoes.idPosto;
            osOrdemServicoBindingSource.Filter = " TIPO_DOC <> 'S' ";

            tsbCancelar.Enabled = false;
            tsbInserir.Enabled = true;
            lblTotalOS.Text = "Total: " + osOrdemServicoBindingSource.Count.ToString();
            //cmbCodigoEquipamentoSelos.SelectedIndex = 0;
            //cmbCodigoSelos.SelectedIndex = 0;
            enviaEmail = new EnviaEmail();

            DataRowView template = (DataRowView)osParametrosBindingSource.Current;
            corpoPedido = template["EMAIL_PEDIDO"].ToString();

            // posicionar os labels fora da tela
            // nada de colocar Visible == false, pq, entao, o binding não funciona
            hideMensal();
        }

        #region ToolstripButtons       
        // CONSULTAR
        private void toolStripButton3_Click_1(object sender, EventArgs e)
        {
            toggleDTPVisibility(true);
            osOrdemServicoBindingSource.CancelEdit();
            pnlConsultar.Visible = true;
            //this.osOrdemServicoTableAdapter.SqlOrcamentos(this.dtsPrincipal.OsOrdemServico);
            this.osOrdemServicoTableAdapter.SqlOrcamentosXOSFuncionario(this.dtsPrincipal.OsOrdemServico);
            // 29.05.2014 Uwe
            //tsbGerarCaixa.Enabled = false; //26.01.
            pesquisar();
        }

        private void limparCamposInserir()
        {
            txtEmail.Text = "";

            lblCabCliente.Text = "";
            lblCabIE.Text = "";
            lblCabNCPJ.Text = "";
            lblCabTelefone.Text = "";
            lblCabObservacao.Text = "";

            lblCabSituacao.Text = "";

            lblCabTipoPed.Text = "";
            lblCabFatura.Text = "";

            label22.Text = "";
            label20.Text = "";
            label21.Text = "";
            label19.Text = "";
            label9.Text = "";

            // 17.11.2020
            lblCidade2.Text = "";
            lblUF2.Text = "";
            lblCidadeNovoCliente.Text = "";
            lblUFNovoCliente.Text = "";


            //cmbCodCliente.SelectedIndex = -1;
            //cmbNomeCliente.SelectedIndex = -1;
            //cmbCGCCliente.SelectedIndex = -1;
            //cmbLogotipoCliente.SelectedIndex = -1;
        }

        private void toggleDTPVisibility(bool b) {

           
            dtpDataCadastro.Visible = b;
            dtpDataFechamento.Visible = b;

            foreach (Control c in tabCabecalho.Controls)
            {
                if (c.GetType() == typeof(Label))
                {
                    //Debug.WriteLine(c.Name);
                    if (c.Name == "dT_CADASTROLabel") c.Visible = b;
                    if (c.Name == "dT_FECHAMENTOLabel") c.Visible = b;
                }
                
            }
        }
        // INSERIR
        private void tsbInserir_Click(object sender, EventArgs e)
        {

            //dT_CADASTROLabel.Text = "";
            //dT_FECHAMENTOLabel.Text = "";
            //lblU
            toggleDTPVisibility(false);

            txtEmail.Text = "";

            lblCabCliente.Text = "";
            lblCabIE.Text = "";
            lblCabNCPJ.Text = "";
            lblCabTelefone.Text = "";
            lblCabObservacao.Text = "";
            
            lblCabSituacao.Text = "";

            lblCabTipoPed.Text = "";
            lblCabFatura.Text = "";
                        
            label22.Text = "";
            label20.Text = "";
            label21.Text = "";
            label19.Text = "";
            label9.Text = "";

            // 17.11.2020
            lblCidade2.Text = "";
            lblUF2.Text = "";
            lblCidadeNovoCliente.Text = "";
            lblUFNovoCliente.Text = "";
            //

            tsbCancelar.Enabled = true;
            tsbInserir.Enabled = false;
            tsbSalvar.Enabled = true;
             
            // mod 23.03.2015 uwe
            toolStripButton3.Enabled = false; // botão CONSULTAR
            toolStripButton1.Enabled = false; // botão PRIMEIRO
            toolStripButton5.Enabled = false; // botão ANTERIOR

            toolStripButton6.Enabled = false; // botão PROXIMO 
            toolStripButton7.Enabled = false; // botão ULTIMO  
            // end mod 23.03.2015

            pnlConsultar.Visible = false;

            inserir();

            if (OsConfiguracoes.tipoAcesso == 'A')
            {
                cmbCodigoUsuario.Enabled = true;
                cmbNomeUsuario.Enabled = true;
            }
            else
            {
                cmbCodigoUsuario.Text = OsConfiguracoes.codigoUsuario.ToString();
                cmbNomeUsuario.Text = OsConfiguracoes.nomeUsuario;
                cmbCodigoUsuario.Enabled = false;
                cmbNomeUsuario.Enabled = false;
            }

            
            // REDMINE Tarefa #77 old
            // surgerir dia 5 de mes sequinte ate dia 15 atual (inclusive)
            // a partir de dia 16 surgerir dia 5 dois meses no futuro
            DateTime now = DateTime.Now;
            var day = now.Day;            
            now = now.AddMonths(1);
            if (day > 15) now = now.AddMonths(1);
            vENCIMENTO_MANU_MENSALDateTimePicker.Value = new DateTime(now.Year, now.Month, 05);

         
            
            lblNomeCliente_Click(null,null);
            lblLogotipoCliente_Click(null, null);

            // cmbNomeUsuario_Click(null, null);

            // cmbNomeCliente.SelectedIndex = -1;

            limparCamposInserir();
            //debug
            DataRowView x = (DataRowView)osOrdemServicoBindingSource.Current;
            //osOrdemServicoBindingSource.Position = 1;

            cmbLogotipoCliente.Focus();
        }
        // CANCELAR       
        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            toggleDTPVisibility(true);
            tsbCancelar.Enabled = false;
            tsbInserir.Enabled = true;
            tsbSalvar.Enabled = false;

            osOrdemServicoBindingSource.CancelEdit();
            pnlConsultar.Visible = true;
            // mod 23.03.2015 uwe
            // consultar button
            toolStripButton3.Enabled = true;
            pesquisar();
            //toolStripButton3_Click_1(null,null);
            toolStripButton3.PerformClick();
            // end mod 23.03.2015
        }
        // GRAVAR
        private void tsbSalvar_Click(object sender, EventArgs e)
        {
            if (cmbCodigoUsuario.Text.Equals("") || cmbCodigoUsuario.SelectedIndex == -1)
            {
                MessageBox.Show(@"Favor informar Usuário na aba de Cabeçalho!", @"ERRO - Usuário",
                                MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            GravarPedido();
            MessageBox.Show(@"Registro gravado com sucesso!");

            // SetBindingsHeader();
        }
        private void GravarPedido()
        {
            tsbCancelar.Enabled = false;
            tsbInserir.Enabled = true;

            int proximoPedido = (int)osOrdemServicoTableAdapter.SqlProximoPedido(OsConfiguracoes.codigoUsuario, OsConfiguracoes.idPosto);

            /*   if ((cmbNomeCliente.Text.Length == 0 || cmbNomeCliente.Text.Length == 0) || )
               {
                   MessageBox.Show("Informe o Código do Cliente Antes de Gravar a OS!");
                   return;
               }
               else*/
            {
                DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;

                //10.12.2020
                //muss hier gesetzt werden, da "Empresa" ueber neue ComboBox cmbEmpresa geaendert werden kann.
                currentOS["IDPOSTO"] = OsConfiguracoes.idPosto;

                if (estadoCorrente == EstadoManutencao.smInserir)
                {

                    currentOS["IDPOSTO"] = OsConfiguracoes.idPosto;
                    currentOS["DT_CADASTRO"] = DateTime.Now;
                    currentOS["ID_PEDIDO"] = proximoPedido;
                    currentOS["TIPO_DOC"] = 'O';
                    //ufunc20052021             

                    //DataRowView cu = (DataRowView)osUsuarioBindingSource.Current;
                    //DataRowView xos = (DataRowView)dtsXOSFuncionarioBindingSource.Current;
                    //currentOS["idusuario"] = xos["idusuario"];
                }


                string savetxt = txtObservacao.Text;
                string savetext2 = textBox1.Text;
                // textos perdem aqui alteracoes feitos caso ainda tem o foco 
                // uwe 05.12.2017
                //26.01.
                //if (rdbOrigOsExpress.Checked)
                //{
                    currentOS["ORIGEM_CLIENTE_PEDIDO"] = 'O';
                //}
                //else if (rdbOrigSigPosto.Checked)
                //{
                //    currentOS["ORIGEM_CLIENTE_PEDIDO"] = 'S';
                //}
                txtObservacao.Text = savetxt;
                textBox1.Text = savetext2;


                //if (rdbOrigOsExpress.Checked)
                //{
                    if (txtEmail.Text.Length == 0)
                    {
                        DataRowView currentCliente = (DataRowView)osClienteBindingSource.Current;
                        txtEmail.Text = currentCliente["EMAIL"].ToString();
                    }
                //}
                //else if (rdbOrigSigPosto.Checked)
                //{
                //    if (txtEmail.Text.Length == 0)
                //    {
                        //DataRowView currentCliente = (DataRowView)bdsFiltroCliente.Current;
                //        DataRowView currentCliente = (DataRowView)bs_cabecalho.Current;
                //        txtEmail.Text = currentCliente["obs01"].ToString();
                //    }
                //}

                //txtValorAPagar.Text = "0";
                txtValorAPagar.Text = (decimal.Parse(txtTotalFinalOS.Text) - decimal.Parse(txtDesconto.Text)).ToString();

                this.Validate();               
                this.osOrdemServicoBindingSource.EndEdit();

               // if (estadoCorrente == EstadoManutencao.smInserir)
               // {
                    DataRowView xos = (DataRowView)dtsXOSFuncionarioBindingSource.Current;
                    currentOS["idusuario"] = xos["idusuario"];
               // }

                this.osOrdemServicoTableAdapter.Update(this.dtsPrincipal);
                this.osItemServicoBindingSource.EndEdit();
                this.osOrdemParcelaBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(this.dtsPrincipal);

                if (estadoCorrente == EstadoManutencao.smInserir)
                {
                    fillFields();
                }

                //


                estadoCorrente = EstadoManutencao.smPesquisar;

                // novembro 2020 uwe
                toolStripButton3.Enabled = true;               

            }
        }       
        // FECHAR / ABRIR PEDIDO
        private void tsbFecharOS_Click(object sender, EventArgs e)
        {
            // Abrir Pedido fechado uwe 26.03.2018
            if (tsbFecharPedido.Text.Contains("Abri"))
            {

                var currentOsAbrirOs = (DataRowView)osOrdemServicoBindingSource.Current;
                var id = currentOsAbrirOs["ID"];

                //string sql = "Update OsOrdemServico set status_os = 'A' Where ID='" + id + "'";
                //DB.ExecuteNonQuery(sql);

                currentOsAbrirOs["STATUS_OS"] = "A";
                osOrdemServicoBindingSource.EndEdit();
                osOrdemServicoTableAdapter.Update(this.dtsPrincipal);

                syncBotaoFecharPedido((int)id, false);
                return;
            }

            String codigoOS = lblCodigoOS.Text;
            //String codigoOSFinal = lblNovoCodOS.Text;

            // 19.02.2015 Uwe
            // "FecharPedido" sò pode ser acionado caso haja, ao minimo, uma parcela definida!
            string psql = @"Select count(*) n from OsOrdemParcela where idordem=" + codigoOS;
            bool temParcela = (int)DB.GetSingleValue(psql) > 0;

            // todas as linhas de DataGridview foram apagados manualmente??
            int linhas = osOrdemParcelaDataGridView.RowCount;

            if (!temParcela || linhas == 0)
            {
                MessageBox.Show(@"Favor definir, ao minimo, uma parcela antes de fechar o pedido!",
                                 @"E R R O",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Stop);
                return;
            }

            // a soma das parcelas(osOrdemParcelas) e o Total_OS (osOrdemServiso) precisam ser identicos. 
            psql = @"select case when (a.val - b.val) = 0.0  then 'ok' else 'error' end from
                   ( select total_os - valor_desconto val from osOrdemServico where id=" + codigoOS + @") a,
                   ( select sum(valor_parcela) val from osOrdemParcela where idordem = " + codigoOS + ") b";

            if (!DB.GetSingleValue(psql).ToString().Equals("ok"))
            {
                MessageBox.Show(@"Favor verificar,\n a soma das parcelas(osOrdemParcelas)"
                                + @" e o Total_OS (osOrdemServiço) precisam ser identicos",
                                 @"E R R O",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Stop);
                return;
            }

            GravarPedido();

            relPedido = new RelPedido();

            DataRowView current = (DataRowView)osOrdemServicoBindingSource.Current;
            if (MessageBox.Show(@"Deseja Realmente Fechar esse Pedido?", "Verificar", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                NumeroPorExtenso extenso = new NumeroPorExtenso();
                extenso.SetNumero(decimal.Parse(txtValorAPagar.Text));
                OsConfiguracoes.literalExtenso = extenso.ToString();

                current["STATUS_OS"] = "F";
                current["DT_FECHAMENTO"] = DateTime.Now;
                current["VALOR_RESTANTE_EXTENSO"] = OsConfiguracoes.literalExtenso;
                current["TIPO_DOC"] = "P";

                osOrdemServicoBindingSource.EndEdit();
                osOrdemServicoTableAdapter.Update(this.dtsPrincipal);

                // desativar enviaremail testando
                //enviaEmail.enviarTecnoExpress(codigoOSFinal, OsConfiguracoes.nomeUsuario, "patricia@tecnoexpress.com.br", "P");
                //enviaEmail.enviarTecnoExpress(codigoOSFinal, OsConfiguracoes.nomeUsuario, "heberth@tecnoexpress.com.br", "P");

                MessageBox.Show(@"Pedido fechado com sucesso!");

                syncBotaoFecharPedido((int)current["ID"], true);

            }

        }
        // DESCARTAR
        private void tsbExcluir_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(@"Deseja realmente descartar esse Pedido?",
                                null,
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                DataRowView current = (DataRowView)osOrdemServicoBindingSource.Current;

                current["STATUS_OS"] = "D";

                osOrdemServicoBindingSource.EndEdit();
                osOrdemServicoTableAdapter.Update(this.dtsPrincipal);
            }
        }
        // IMPRIMIR
        private string getResponsavel()
        {
            //DataRowView currentCliente = (DataRowView)osClienteBindingSource.Current;
            //string id_cliente = currentCliente["id"].ToString();
            //select COALESCE(idcliente_pedido,idcliente) from osordemservico where id = 697901
            
            // ID-Cliente: idcliente_pedido, caso nulo idcliente.
            DataRowView current = (DataRowView)osOrdemServicoBindingSource.Current;
            string id = current["id"].ToString();
            string id_cliente = DB.GetSingleValue(@"select COALESCE(idcliente_pedido,idcliente) from osordemservico where id=" + id).ToString();

            int anz = (int)DB.GetSingleValue(@"SELECT COUNT(1) anz FROM OsClienteContato WHERE responsavel = 'S' and id_cliente="+id_cliente);
            if (anz == 0)
               return " ----- ";
            return DB.GetSingleValue(@"SELECT nome FROM OsClienteContato WHERE responsavel = 'S' and id_cliente=" + id_cliente).ToString();
        }
        private string getPrincipal()
        {
            //DataRowView currentCliente = (DataRowView)osClienteBindingSource.Current;
            //string id_cliente = currentCliente["id"].ToString();
            // ID-Cliente: idcliente_pedido, caso nulo idcliente.
            DataRowView current = (DataRowView)osOrdemServicoBindingSource.Current;
            string id = current["id"].ToString();
            string id_cliente = DB.GetSingleValue(@"select COALESCE(idcliente_pedido,idcliente) from osordemservico where id=" + id).ToString();
            int anz = (int)DB.GetSingleValue(@"SELECT COUNT(1) anz FROM OsClienteContato WHERE principal = 'S' and id_cliente=" + id_cliente);
            if (anz == 0)
                return " ----- ";
            return DB.GetSingleValue(@"SELECT nome FROM OsClienteContato WHERE principal = 'S' and id_cliente=" + id_cliente).ToString();
        }
        private string getPrincipal_celular()
        {
            //DataRowView currentCliente = (DataRowView)osClienteBindingSource.Current;
            //string id_cliente = currentCliente["id"].ToString();
            // ID-Cliente: idcliente_pedido, caso nulo idcliente.
            DataRowView current = (DataRowView)osOrdemServicoBindingSource.Current;
            string id = current["id"].ToString();
            string id_cliente = DB.GetSingleValue(@"select COALESCE(idcliente_pedido,idcliente) from osordemservico where id=" + id).ToString();
            int anz = (int)DB.GetSingleValue(@"SELECT COUNT(1) anz FROM OsClienteContato WHERE principal = 'S' and id_cliente=" + id_cliente);
            if (anz == 0)
                return " ----- ";
            return formatCelular(DB.GetSingleValue(@"SELECT celular FROM OsClienteContato WHERE principal = 'S' and id_cliente=" + id_cliente).ToString());
        }
        private string getPrincipal_email()
        {
            //DataRowView currentCliente = (DataRowView)osClienteBindingSource.Current;
            //string id_cliente = currentCliente["id"].ToString();
            // ID-Cliente: idcliente_pedido, caso nulo idcliente.
            DataRowView current = (DataRowView)osOrdemServicoBindingSource.Current;
            string id = current["id"].ToString();
            string id_cliente = DB.GetSingleValue(@"select COALESCE(idcliente_pedido,idcliente) from osordemservico where id=" + id).ToString();
            int anz = (int)DB.GetSingleValue(@"SELECT COUNT(1) anz FROM OsClienteContato WHERE principal = 'S' and id_cliente=" + id_cliente);
            if (anz == 0)
                return " ----- ";
            return DB.GetSingleValue(@"SELECT email FROM OsClienteContato WHERE principal = 'S' and id_cliente=" + id_cliente).ToString();
        }
        private void tsbImprimirOS_Click(object sender, EventArgs e)
        {
            imprimirPedido(lblCodigoOS.Text);
        }
        private void imprimirPedido(string codPedido)
        {
            relPedido = new RelPedido();
          
            string responsavel = getResponsavel();
            string principal = getPrincipal();
            string principal_celular = getPrincipal_celular();
            string principal_email = getPrincipal_email(); 
            
            isSingle = cbSingle.CheckState == CheckState.Checked ? "1" : "0";
                       
            relPedido.configuraPedido(codPedido, isSingle, responsavel, principal, principal_celular,principal_email);

            relPedido.Show();
        }
        // GERAR CAIXA
        private void tsbGerarCaixa_Click(object sender, EventArgs e)
        {
            //26.01.
            //int abc = 123;
            // Essa rotina é responsável faz a chamada da procedure de fechamento do caixa,
            //if (decimal.Parse(txtTotalFinalOS.Text) > 0)
            //{
            //    var connectionString = ConfigurationManager.ConnectionStrings["osExpress.Properties.Settings.OsExpressConnectionString"].ConnectionString;
            //    SqlConnection conn = new SqlConnection(connectionString);
            //    SqlCommand cmd = new SqlCommand("dbo.osPrcGeraCaixa", conn);
            //    cmd.CommandType = CommandType.StoredProcedure;
            //    SqlParameter param = new SqlParameter("@CODIGO_ORDEM", int.Parse(lblCodigoOS.Text));
            //    param.Direction = ParameterDirection.Input;
            //    param.DbType = DbType.Int32;

            //    cmd.Parameters.Add(param);
            //    cmd.Connection.Open();
            //    if (cmd.ExecuteNonQuery() > 0)
            //        MessageBox.Show(@"Caixa gerado com sucesso!");
            //    cmd.Connection.Close();

            //    // 25.06.2018
            //    tsbGerarCaixa.Enabled = false;

            //    tsbFecharPedido.Enabled = false;
            //    tsbFecharPedido.Text = @"&Fechar Pedido";
            //    tsbFecharPedido.ToolTipText = @"Fechar Pedido";
            //}
        }
        // ENVIAR EMAIL
        private void tsbEmail_Click(object sender, EventArgs e)
        {
            String codigoOS = lblCodigoOS.Text;

            relPedido = new RelPedido();
           
            string responsavel = getResponsavel();
            string principal = getPrincipal();
            string principal_celular = getPrincipal_celular();
            string principal_email = getPrincipal_email();
            relPedido.configuraPedido(codigoOS, isSingle, responsavel, principal, principal_celular, principal_email);

            if (MessageBox.Show(@"Deseja enviar o email para o cliente?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                enviaEmail.enviarCliente(txtEmail.Text, "Pedido", "Pedido nº " + codigoOS + ".pdf", corpoPedido);

                MessageBox.Show(@"Emai enviado com sucesso!");
            }
        }
        #endregion ToolstripButtons

               
        private void TplPermiteSoValor(object sender, KeyPressEventArgs e) 
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != ',')
            {
                e.Handled = true;
            }

            // Só permite apenas número um caracter de vírgula (decimal).
            if (e.KeyChar == ',' && (sender as TextBox).Text.IndexOf(',') > -1)
            {
                e.Handled = true;
            }
        }


        private void TplPermiteSoNumero(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }


        private void ManOrdemServico_Shown(object sender, EventArgs e)
        {
            if (OsConfiguracoes.tipoAcesso == 'A')
            {
                cmbCodigoUsuario.Enabled = true;
                cmbNomeUsuario.Enabled = true;
            }
            else
            {
                cmbCodigoUsuario.Enabled = false;
                cmbNomeUsuario.Enabled = false;
            }

           // UWE 2020 disabled porque não combina com 
           // pnlConsultar.Visible = true;
            
        }


        private void calculaItemPeca(object sender, EventArgs e)
        {
            if (txtPrecoProduto.Text.Length < 1)
            {
                txtPrecoProduto.Text = "0";
            }      

            if (numQtdeProduto.Value != 0)
                txtTotalProduto.Text = (numQtdeProduto.Value * 
                                        decimal.Parse(txtPrecoProduto.Text)).ToString();
        }


        /// <summary>
        /// GerarCaixa      habilitado      : se STATUS_OS == 'F' e FATURA_GERADA == 0
        /// Fechar/Abrir    habilitado      : se FATURA_GERADA == 0
        /// Gravar          habilitado      : se STATUS_OS == 'A' e em tela de detalhe, ( ou smInserir) 
        /// Descartar       habilitado      : se STATUS_OS == 'A'               
        /// </summary>
        private void ControlButtons()          
        {
            var tipoAcesso = OsConfiguracoes.tipoAcesso;

            var current = (DataRowView)osOrdemServicoBindingSource.Current;
            var statusOS = current["STATUS_OS"].ToString();

            switch (statusOS)
            {
                case "A":
                    lblCabSituacao.Text = @"Aberta";
                    lblCabSituacao.ForeColor = Color.Green;
                    lblSituacao2.ForeColor = Color.Green;
                    
                    tsbFecharPedido.Enabled = true;
                    // 21.06.2018                   
                    tsbFecharPedido.Text = @"&Fechar Pedido";
                    tsbFecharPedido.ToolTipText = @"Fechar Pedido";

                    tsbGerarCaixa.Enabled = false;
                    break;

                case "D":
                    lblCabSituacao.Text = @"Descartada";
                    lblCabSituacao.ForeColor = Color.Red;
                    lblSituacao2.ForeColor = Color.Red;
                    tsbGerarCaixa.Enabled = false;
                    break;

                case "F":
                    lblCabSituacao.Text = @"Fechada";
                    lblCabSituacao.ForeColor = Color.Blue;
                    lblSituacao2.ForeColor = Color.Blue;

                    if (((bool)current["FATURA_GERADA"] == false) && (tipoAcesso == 'A'))
                           {
                                tsbGerarCaixa.Enabled = true;

                                tsbFecharPedido.Enabled = true;
                                tsbFecharPedido.Text = @"Abri&r Pedido";
                                tsbFecharPedido.ToolTipText = @"Abrir Pedido";
                            }
                    else
                    {
                        tsbGerarCaixa.Enabled = false;
                        tsbFecharPedido.Enabled = false;
                        tsbFecharPedido.Text = @"&Fechar Pedido";
                        tsbFecharPedido.ToolTipText = @"Fechar Pedido";
                    }
                    break;
            }

            // descartar
            tsbDescartar.Enabled = (string) current["STATUS_OS"] == "A";
            
            // salvar
            tabCabecalho.Enabled = current["STATUS_OS"].ToString() == "A";
            tabPecas.Enabled = current["STATUS_OS"].ToString() == "A";
            tabParcelas.Enabled = current["STATUS_OS"].ToString() == "A";

            tsbSalvar.Enabled = current["STATUS_OS"].ToString() == "A";

            // disable Gravar if not in detailmode
            if (pnlConsultar.Visible) tsbSalvar.Enabled = false;
        }

        /// <summary>
        /// ao mudar pedido atual 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void osOrdemServicoBindingSource_CurrentChanged(object sender, EventArgs e)
        {
           
            DataRowView current = (DataRowView)osOrdemServicoBindingSource.Current;

            // Se existir registro...
            if (osOrdemServicoBindingSource.Count > 0)
            {
                // tratamento dos tipos de documento... todo verificar Orcamento/Pedido
                if ((string)current["TIPO_DOC"] == "O")
                {
                    lblCabTipoPed.Text = @"Orçamento";
                    tsbDescartar.Enabled = true;
                    tsbFecharPedido.Enabled = true;
                }
                else
                    if ((string)current["TIPO_DOC"] == "P")
                    {
                        lblCabTipoPed.Text = @"Pedido";
                        tsbDescartar.Enabled = false;
                        //tsbFecharPedido.Enabled = false;
                    }

                //breakpoint               
                ControlButtons();
                

                if (OsConfiguracoes.tipoAcesso == 'A')
                {
                    txtPrecoProduto.ReadOnly = false;
                    txtPrecoProduto.Enabled = true;
                }
                else
                {
                    txtPrecoProduto.ReadOnly = true;
                    txtPrecoProduto.Enabled = false;
                }
                            
                #region labels
                // 26.01.
                //if ((string)current["ORIGEM_CLIENTE_PEDIDO"] == "O")
                //{
                // check for sminserir


                    lblCabCliente.Visible = true;
                    lblCabIE.Visible = true;
                    lblCabNCPJ.Visible = true;
                    lblCabObservacao.Visible = true;
                    lblCabTelefone.Visible = true;
                    //
                    label22.Visible = false;
                    label20.Visible = false;
                    label21.Visible = false;
                    label19.Visible = false;
                    label9.Visible = false;
                    // 17.11.2020
                    lblCidade2.Visible = false;
                    lblUF2.Visible = false;
                
                //
                    //rdbOrigOsExpress.Checked = true;
                    //rdbOrigSigPosto.Checked = false;
                //}
                
                //if ((string)current["ORIGEM_CLIENTE_PEDIDO"] == "S")
                //{
                //    lblCabCliente.Visible = false;
                //    lblCabIE.Visible = false;
                //    lblCabNCPJ.Visible = false;
                //    lblCabObservacao.Visible = false;
                //    lblCabTelefone.Visible = false;

                //    lblCidadeNovoCliente.Visible = false;
                //    lblUFNovoCliente.Visible = false;

                //    //
                //    label22.Visible = true;
                //    label20.Visible = true;
                //    label21.Visible = true;
                //    label19.Visible = true;
                //    label9.Visible = true;

                //    lblCidade2.Visible = true;
                //    lblUF2.Visible = true;
                //    //
                //    rdbOrigSigPosto.Checked = false;
                //    rdbOrigSigPosto.Checked = true;
                //}
                #endregion labels

                if (tabPrincipal.SelectedIndex == 1)
                {
                    this.osItemServicoTableAdapter.SqlItensDaOrdem(this.dtsPrincipal.OsItemServico, (int)current["ID"]);
                }

                this.osOrdemParcelaTableAdapter.SqlParcela(this.dtsPrincipal.OsOrdemParcela, (int)current["ID"]);
            }
        }

        private void limpaAbaProdutos()
        {
            cmbCodigoProduto.SelectedIndex = -1;
            cmbDescricaoProduto.SelectedIndex = -1;
            txtPrecoProduto.Text = "0";
            txtTotalProduto.Text = "0";
        }

        private void tabPrincipal_Selected(object sender, TabControlEventArgs e)
        {   
            DataRowView current = (DataRowView) osOrdemServicoBindingSource.Current;

            limpaAbaProdutos();
            if (tabPrincipal.SelectedIndex == 1)
            {
                this.osItemServicoTableAdapter.SqlItensDaOrdem(this.dtsPrincipal.OsItemServico, (int)current["ID"]);
                //limpaAbaProdutos();
            }
            this.osOrdemParcelaTableAdapter.SqlParcela(this.dtsPrincipal.OsOrdemParcela, (int)current["ID"]);
            // 23.02.2015 uwe
            // inicializar o comboBoxIntervalo com o valor "mesualmente"
            if (comboBoxIntervalo.SelectedIndex == -1)
              comboBoxIntervalo.SelectedIndex = 0;
        }
       
        private void btnAdicionarPecas_Click(object sender, EventArgs e)
        {
            if (cmbCodigoIDProduto.Text.Equals("") || cmbCodigoProduto.Text.Equals(""))
            {
                MessageBox.Show("ID do Produto é vazio!!",@"E R R O",
                    MessageBoxButtons.OK,MessageBoxIcon.Stop);
                return;
            }
             if (estadoCorrente == EstadoManutencao.smInserir)
            {
                GravarPedido();
            }

            DataRowView current = (DataRowView)osItemServicoBindingSource.AddNew();

            current["IDORDEM"] = lblCodigoOS.Text;
            current["IDPRODUTO"] = cmbCodigoIDProduto.Text;
            current["QUANTIDADE"] = numQtdeProduto.Value;
            current["VALOR"] = txtPrecoProduto.Text;

            if (txtTotalProduto.Text.Length > 0)
                current["TOTAL"] = decimal.Parse(txtTotalProduto.Text);
            else
                current["TOTAL"] = 0;

            DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;

            if (estadoCorrente == EstadoManutencao.smInserir)
            {
                //currentOS["IDUSUARIO"] = OsConfiguracoes.codigoUsuario;
                currentOS["IDPOSTO"] = OsConfiguracoes.idPosto;
                currentOS["DT_CADASTRO"] = DateTime.Now;
            }

            if (txtTotalProduto.Text.Length < 1)
            {
                txtTotalProduto.Text = "0";
            }

           //currentOS["TOTAL_OS"] = (decimal)currentOS["TOTAL_OS"] + decimal.Parse(txtTotalProduto.Text);
                	        

            osItemServicoBindingSource.EndEdit();
            osItemServicoTableAdapter.Update(dtsPrincipal);

            currentOS["TOTAL_PECAS_OS"] = osItemServicoTableAdapter.SqlTotalProdutos((int)currentOS["ID"]);//(decimal)currentOS["TOTAL_PECAS_OS"] + decimal.Parse(txtTotalProduto.Text);
            currentOS["TOTAL_OS"] = osItemServicoTableAdapter.SqlTotalProdutos((int)currentOS["ID"]);

            osOrdemServicoBindingSource.EndEdit();
            osOrdemServicoTableAdapter.Update(dtsPrincipal);

            numQtdeProduto.Value = 1;
            numQtdeProduto.Focus();

            this.osItemServicoTableAdapter.SqlItensDaOrdem(this.dtsPrincipal.OsItemServico, (int)currentOS["ID"]);
            
        }

        private void syncBotaoFecharPedido(int id, bool fechar)
        {           
            #region syncronize

            if (pnlConsultar.Visible == false)
            {
                // acionar botão CONSULTA
                toolStripButton3.PerformClick();

                // set Filtro [ 0: Aberto, 1: Fechado, 2: Descartado]
                if (fechar)
                    cmbFiltroSituacao.SelectedIndex = 1; 
                else
                    cmbFiltroSituacao.SelectedIndex = 0;
                // set Filtro Situação Fechada
                chkFiltrarSituacao.Checked = false;
                chkFiltrarSituacao.Checked = true;

                // achar o OS recem fechado no DataGridView
                int idx = osOrdemServicoBindingSource.Find(@"Id", id);
                if (idx >= 0)
                {
                    osOrdemServicoBindingSource.Position = idx;
                    osOrdemServicoDataGridView.Rows[idx].Selected = true;
                    // acionar DoubleClick 
                    osOrdemServicoDataGridView_CellDoubleClick(
                        osOrdemServicoDataGridView, new DataGridViewCellEventArgs(0, idx));
                }
            }

            // toggle abrir fechar
            if (fechar)
            {
                tsbFecharPedido.Enabled = true;
                tsbFecharPedido.Text = @"&Abrir Pedido";
                tsbFecharPedido.ToolTipText = @"Abrir Pedido";

                tsbGerarCaixa.Enabled = true;
            }
            else
            {
                tsbFecharPedido.Enabled = true;
                tsbFecharPedido.Text = @"&Fechar Pedido";
                tsbFecharPedido.ToolTipText = @"Fechar Pedido";

                tsbGerarCaixa.Enabled = false;
            }

            #endregion
        }

        //private void button6_Click_1(object sender, EventArgs e)
        //{

        //    //if (estadoCorrente == EstadoManutencao.smInserir)
        //    //{
        //    //    DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;
        //    //    //currentOS["IDUSUARIO"] = OsConfiguracoes.codigoUsuario;
        //    //    currentOS["IDPOSTO"] = OsConfiguracoes.idPosto;
        //    //    currentOS["DT_CADASTRO"] = DateTime.Now;
        //    //}
        //    //osOrdemServicoBindingSource.EndEdit();
        //    //osOrdemServicoTableAdapter.Update(dtsPrincipal);


        //    /*cmbCodigoEquipamentoLacres.Text = "";
        //    comboBox11.Text = "";
        //    comboBox9.Text = "";
        //    comboBox10.Text = "";
        //    comboBox12.Text = "";
        //    cmbCodigoLacres.Text = "";
        //    comboBox7.Text = "";*/

        //}

        private void grdPecas_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;
            osItemServicoBindingSource.EndEdit();
            osItemServicoTableAdapter.Update(dtsPrincipal);

            currentOS["TOTAL_PECAS_OS"] = osItemServicoTableAdapter.SqlTotalProdutos((int)currentOS["ID"]);//(decimal)currentOS["TOTAL_PECAS_OS"] - (decimal)currentPecas["TOTAL"];
            currentOS["TOTAL_OS"] = osItemServicoTableAdapter.SqlTotalProdutos((int)currentOS["ID"]);//(decimal)currentOS["TOTAL_OS"] - (decimal)currentPecas["TOTAL"];

            osOrdemServicoBindingSource.EndEdit();
            osOrdemServicoTableAdapter.Update(dtsPrincipal);
        }

       
        //private void chkFiltrarSelecoes_CheckedChanged(object sender, EventArgs e)
        //{
        //    string sAnd = " AND ";
        //    string sFilter = "";

        //    // Limpa o filtro antes.
        //    osOrdemServicoBindingSource.Filter = "";

        //    sFilter = " TIPO_DOC <> 'S' " + sAnd;

        //    if (chkFiltrarSituacao.Checked)
        //    {
        //        sFilter = sFilter + " STATUS_OS = " + "'" + cmbFiltroSituacao.Text.Substring(0, 1) + "'" + sAnd;
        //    }

        //    // Se existir filtro...
        //    if (sFilter.Length > 0)
        //        osOrdemServicoBindingSource.Filter = sFilter.Substring(0, sFilter.Length - 5);

        //    lblTotalOS.Text = "Total: " + osOrdemServicoBindingSource.Count.ToString();
        

        private void chkFiltrarSelecoes_CheckedChanged(object sender, EventArgs e)
        {
            string sAnd = " AND ";
            string sFilter = "";

            // Limpa o filtro antes.
            osOrdemServicoBindingSource.Filter = "";

            // sFilter = " IDPOSTO=" + OsConfiguracoes.idPosto + " AND TIPO_DOC <> 'S' " + sAnd;            

            sFilter = " TIPO_DOC <> 'S' " + sAnd;

            if (chkPostoOS.Checked)
            {
                int selected = cmbPostoOS.SelectedIndex;
                if (selected >= 0)
                {
                    int posto = (int)((DataRowView)cmbPostoOS.SelectedItem)[0];
                    sFilter = sFilter + " IDPOSTO=" + posto.ToString() + sAnd;
                }

            }

            if (chkFiltrarSituacao.Checked)
            {
                sFilter = sFilter + " STATUS_OS = " + "'" + cmbFiltroSituacao.Text.Substring(0, 1) + "'" + sAnd;
            }

            // filtrar for nome do cliente
            // ou A30Clientes (novo)
            // ou OsCliente (existente)
            if (chkFiltrarCliente.Checked)
            {
               
                string t = cmbFiltroClienteNome.Text.Trim();
                string sql_os = @"select id from oscliente where razao_social like '%" + t + "%'";
                //string sql_cl = @"select id from a30clientes where nome like '%" + t + "%'";
                //26.01.
                //List<int> lx_cl = DB.GetList<int>(sql_cl);
                List<int> lx_os = DB.GetList<int>(sql_os);

                string v = "";

                //if (lx_cl.Count > 0)
                if (1==2)
                {
                    //v = " IDCLIENTE IN (";
                    //foreach (int id in lx_cl)
                    //    v += id + ",";
                    //v = v.Substring(0, v.Length - 1);
                    //v += ")" + sAnd;
                }
                else if (lx_os.Count > 0)
                {
                    v = " IDCLIENTE_PEDIDO IN (";
                    foreach (int id in lx_os)
                        v += id + ",";
                    v = v.Substring(0, v.Length - 1);
                    v += ")" + sAnd;
                }

                sFilter += v;
               
            }

            // Se existir filtro...
            if (sFilter.Length > 0)
                osOrdemServicoBindingSource.Filter = sFilter.Substring(0, sFilter.Length - 5);

            lblTotalOS.Text = @"Total: " + osOrdemServicoBindingSource.Count.ToString();
        }
       
        /// <summary>
        /// CONSULTAR - CellDoubleClick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void osOrdemServicoDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //26.01.
            tabPrincipal.SelectedTab = tabCabecalho;
            // 29.05.2014 Uwe
            pnlConsultar.Visible = false;           
            ControlButtons();

            DataRowView current = (DataRowView)osOrdemServicoBindingSource.Current;
            var id = current["id"];
            var obs = current["obs"];
            var idcliente = current["idcliente"];

            //int index = bs_cabecalho.Find("id", idcliente);
            //if (index >= 0)
            //{
            //    bs_cabecalho.Position = index;
            //}
            foreach (var binding in this.BindingContext[osOrdemServicoBindingSource].Bindings.Cast<Binding>())
                binding.ReadValue();

            string user = current["idusuario"].ToString();

            string sql = @"SELECT unome FROM XOSFUNCIONARIO WHERE idusuario='" + user + "'";
            string unome = DB.GetSingleValue(sql).ToString();
            int index = cmbNomeUsuario.FindString(unome);
            cmbNomeUsuario.SelectedIndex = index;

            index = cmbCodigoUsuario.FindString(user);
            cmbCodigoUsuario.SelectedIndex = index;

           

            //foreach (var binding in this.BindingContext[bs_cabecalho].Bindings.Cast<Binding>())
            //    binding.ReadValue();
          
            //DataRowView curr = (DataRowView)bs_cabecalho.Current;

            //var aa = pnlClienteSigposto.Visible;
            //var bb = pnlClienteOsExpress.Visible;

            //var cc = label22.Visible;
            //var dd = lblCabCliente.Visible;
            
        }
       
        /// <summary>
        /// CONSULTAR - KeyPress 'S'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void osOrdemServicoDataGridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.S)
            {
                tabPrincipal.SelectedTab = tabCabecalho;
                // 29.05.2014 Uwe    
                pnlConsultar.Visible = false;                         
                ControlButtons();

                var x = 1;
                DataRowView current = (DataRowView)osOrdemServicoBindingSource.Current;
                var id = current["id"];
                var obs = current["obs"];
                var idcliente = current["idcliente"];

                //int index = bs_cabecalho.Find("id", idcliente);
                //if (index >= 0)
                //{
                //    bs_cabecalho.Position = index;
                //}

                DataRowView currentCliente = (DataRowView)osClienteBindingSource.Current;
                var razao_social = currentCliente["razao_social"];

                foreach (var binding in this.BindingContext[osOrdemServicoBindingSource].Bindings.Cast<Binding>())
                    binding.ReadValue();


                // combobox codigo usuario 
                string user = current["idusuario"].ToString();
                string sql = @"SELECT unome FROM XOSFUNCIONARIO WHERE idusuario='" + user + "'";
                string unome = DB.GetSingleValue(sql).ToString();
                int index = cmbNomeUsuario.FindString(unome);
                cmbNomeUsuario.SelectedIndex = index;

                index = cmbCodigoUsuario.FindString(user);
                cmbCodigoUsuario.SelectedIndex = index;

                //foreach (var binding in this.BindingContext[bs_cabecalho].Bindings.Cast<Binding>())
                //    binding.ReadValue();

              
                //DataRowView curr = (DataRowView)bs_cabecalho.Current;

                //var aa = pnlClienteSigposto.Visible;
                //var bb = pnlClienteOsExpress.Visible;

                //var cc = label22.Visible;
                //var dd = lblCabCliente.Visible;

            }
        }

        /// <summary>
        /// 24.02.2015 uwe
        /// Parcelar o valor total. Caso a soma de todas as parcelas não 
        /// corresponde com o total, a entrada, respetivamente a primeira
        /// parcela, será modificada.
        /// </summary>
        /// <param name="entrada">a entrada.</param>
        /// <param name="total">o total.</param>
        /// <param name="parcelas">as parcelas.</param>
        /// <returns></returns>
        private Queue<string> scheduleValores(decimal entrada, decimal total, int parcelas)
        {
            var ret = new Queue<string>();
            var dec = new List<decimal>();
            if (entrada > 0)
            {
                parcelas -= 1;
                dec.Add(decimal.Round(entrada, 2));
                total = total - entrada;
            }
            decimal valorParcela = decimal.Round(total / parcelas, 2);
            for (var i = 0; i < parcelas; i++)
            {
                dec.Add(valorParcela);
            }
            decimal sumParcelas = dec.Sum(x => x);
            decimal centavo = 0.01M;
            centavo = (total + entrada) < sumParcelas ? centavo * -1.0M : centavo;
            while ((total + entrada) != sumParcelas)
            {
                dec[0] += centavo;
                sumParcelas = dec.Sum(x => x);
            }
            foreach (var p in dec)
            {
                ret.Enqueue(p.ToString("F"));
            }
            return ret;
        }
        private int CheckDateTime(int ano, int mes, int dia)
        {           
            bool ok = false;
            DateTime checkDateTime;
            while (!ok)
            {
                try
                {
                    checkDateTime = new DateTime(ano, mes, dia);
                    ok = true;
                }
                catch (Exception)
                {
                    dia -= 1;
                }
            }
            return dia;
        }
        /// <summary>
        /// Schedules the specified initialize date.
        /// </summary>
        /// <param name="initDate">data inicial</param>
        /// <param name="parcelas">int - número das parcelas</param>
        /// <param name="diaUm">a partir de dia 1 (senão a partir de data atual).</param>
        /// <param name="cbIintervalo">string - valor(selecteditem) de combobox</param>
        /// <returns></returns>
        private Queue<string> schedule(DateTime initDate, int parcelas, bool diaUm, string cbIntervalo)
        {
            var ret = new Queue<string>();

            int anoAtual = initDate.Year;
            int mesAtual = initDate.Month;
            int diaAtual = initDate.Day;

            // intervalo especificado
            #region Intervalo selecionado
            if (!cbIntervalo.Equals("mesualmente"))
            {
                int intervalo = Convert.ToInt32(cbIntervalo.Split()[0]);

                if (diaUm)
                {
                    diaAtual = 1;
                    mesAtual += 1;
                    if (mesAtual == 13)
                    {
                        mesAtual = 1;
                        anoAtual += 1;
                    }
                }
                if (!diaUm)
                {
                    DateTime tomorrow = initDate.AddDays(1);
                    diaAtual = tomorrow.Day;
                    if (initDate.Month != tomorrow.Month)
                    {
                        mesAtual += 1;
                        if (mesAtual == 13)
                        {
                            mesAtual = 1;
                            anoAtual += 1;
                        }
                    }
                }
                DateTime parcelaAtual = new DateTime(anoAtual, mesAtual, diaAtual);
                for (int i = 0; i < parcelas; i++)
                {
                    ret.Enqueue(parcelaAtual.ToString("dd/MM/yyyy"));
                    parcelaAtual = parcelaAtual.AddDays(intervalo);
                }
                return ret;
            }
            #endregion
            // intervalo mesualmente
            #region dia 1
            if (diaUm)
            {
                diaAtual = 1;
                for (int i = 0; i < parcelas; i++)
                {
                    mesAtual += 1;
                    if (mesAtual == 13)
                    {
                        mesAtual = 1;
                        anoAtual += 1;
                    }
                    ret.Enqueue(new DateTime(anoAtual, mesAtual, diaAtual).ToString("dd/MM/yyyy"));
                }
            }
            #endregion
            #region a partir do dia atual
            if (!diaUm)
            {
                DateTime tomorrow = initDate.AddDays(1);
                diaAtual = tomorrow.Day;
                if (initDate.Month != tomorrow.Month)
                {
                    mesAtual += 1;
                    if (mesAtual == 13)
                    {
                        mesAtual = 1;
                        anoAtual += 1;
                    }
                }
                for (int i = 0; i < parcelas; i++)
                {
                    int checkedDia = CheckDateTime(anoAtual, mesAtual, diaAtual);
                    ret.Enqueue(new DateTime(anoAtual, mesAtual, checkedDia).ToString("dd/MM/yyyy"));

                    mesAtual += 1;
                    if (mesAtual == 13)
                    {
                        mesAtual = 1;
                        anoAtual += 1;
                    }
                }
            }
            #endregion
            return ret;
        }
        private void btnInserirParcela_Click(object sender, EventArgs e)
        {
            if (numQtdeParcela.Value == 0)
            {
                MessageBox.Show(@"ATENÇÃO: Informe a quantidade de parcelas.");
                return;
            }

            // 23.02.2015 uwe
            // reset parcelas
            var c_idordem = (DataRowView) osOrdemServicoBindingSource.Current;
            var idordem = c_idordem["ID"];
            var sql = "DELETE FROM osOrdemParcela where idordem=" + idordem;
            DB.ExecuteNonQuery(sql);

            // reset DataGridView 
            // tudo isso não funciona!!!
            //osOrdemParcelaDataGridView.Rows.Clear();
            //osOrdemParcelaDataGridView.Refresh();
            //osOrdemParcelaBindingSource.ResetBindings(true);
            //osOrdemParcelaDataGridView.DataSource = null;
            //osOrdemParcelaDataGridView.DataSource = osOrdemParcelaBindingSource;

            // funciona
            tabPrincipal.SelectedTab = tabPecas;
            tabPrincipal.SelectedTab = tabParcelas;

            
            // obter as parcelas (data)
            Queue<string> scheduledParcelas =
                schedule(DateTime.Now, (int) numQtdeParcela.Value,
                    rdbAPartirDiaPrimeiro.Checked,
                    comboBoxIntervalo.SelectedItem.ToString());
            
            // obter as parcelas (valores)
            Queue<string> scheduledValores =
                scheduleValores(decimal.Parse(txtEntrada.Text),
                    decimal.Parse(txtValorAPagar.Text),
                    (int) numQtdeParcela.Value);

            DataRowView current;

            int contador = 1;

            if (numQtdeParcela.Value > 0)
            {                
                Decimal totalAPagar = decimal.Parse(txtValorAPagar.Text);

                Decimal valorEntrada = decimal.Parse(txtEntrada.Text);

                int qtdeParcela = (int) numQtdeParcela.Value;

                if (valorEntrada > 0)
                {
                    qtdeParcela = qtdeParcela - 1;
                }

                if (qtdeParcela == 0)
                {
                    qtdeParcela = 1;
                }

                Decimal valorParcela = decimal.Round((totalAPagar - valorEntrada) / (qtdeParcela), 2);

                Decimal diferencaParcela = (valorParcela * (qtdeParcela) - totalAPagar + valorEntrada);             

                if (valorEntrada > 0)
                {
                    current = (DataRowView)osOrdemParcelaBindingSource.AddNew();

                    current["IDORDEM"] = lblCodigoOS.Text;

                    //if (diferencaParcela + totalAPagar > totalAPagar)
                    //{
                    //    current["VALOR_PARCELA"] = valorEntrada - diferencaParcela;
                    //}
                    //else
                    //{
                    //    current["VALOR_PARCELA"] = valorEntrada + diferencaParcela;
                    //}

                    // uwe 24.02.2015
                    current["VALOR_PARCELA"] = scheduledValores.Dequeue(); 
                    // uwe 23.02.2015 
                    current["DATA_PARCELA"] = scheduledParcelas.Dequeue();                   

                    current["NUMERO_PARCELA"] = contador;
                    contador = contador + 1;
                }

                // valor a pagar: 107
                // parcelas: 3
                // Parcela 1: 35,66
                // Parcela 2: 35,66
                // Parcela 3: 35,66

                if (valorEntrada > 0)
                {
                    qtdeParcela = qtdeParcela + 1;
                }

                for (int i = contador; i < qtdeParcela+1; i++)
                {
                    current = (DataRowView)osOrdemParcelaBindingSource.AddNew();

                    current["IDORDEM"] = lblCodigoOS.Text;

                    //if (i == 1)
                    //{
                    //    if (diferencaParcela + totalAPagar > totalAPagar)
                    //    {
                    //        current["VALOR_PARCELA"] = valorParcela - diferencaParcela;
                    //    }
                    //    else
                    //    {
                    //        current["VALOR_PARCELA"] = valorParcela + diferencaParcela;
                    //    }
                    //}
                    //else
                    //{
                    //    current["VALOR_PARCELA"] = valorParcela;
                    //}

                    // uwe 24.02.2015
                    current["VALOR_PARCELA"] = scheduledValores.Dequeue(); 
                    // uwe 23.02.2015
                    current["DATA_PARCELA"] = scheduledParcelas.Dequeue();                   

                    current["NUMERO_PARCELA"] = i;
                }


                DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;

                if (estadoCorrente == EstadoManutencao.smInserir)
                {
                    currentOS["IDPOSTO"] = OsConfiguracoes.idPosto;
                    currentOS["DT_CADASTRO"] = DateTime.Now;
                }

                osOrdemServicoBindingSource.EndEdit();
                osOrdemServicoTableAdapter.Update(dtsPrincipal);

                osOrdemParcelaBindingSource.EndEdit();
                osOrdemParcelaTableAdapter.Update(dtsPrincipal);
            }
            else
            {
                MessageBox.Show("ATENÇÃO: Informe a quantidade de parcelas.");
            }
        }

        private void rdbChangedOrigCliente(object sender, EventArgs e)
        {

            //txtEmail.Text = "";
            //cmbCGCClienteSigposto.SelectedIndex = -1;
            //cmbCodCLienteSigPosto.SelectedIndex = -1;
            //cmbNomeCLienteSigPosto.SelectedIndex = -1;
            //cmbLogotipoClienteSigposto.SelectedIndex = -1;
                

            string v21 = label21.Visible.ToString();
            string v21A30 = lblCabNCPJ.Visible.ToString();

            //if (rdbOrigOsExpress.Checked)
            //{
                pnlClienteOsExpress.Visible = true;
                pnlClienteSigposto.Visible = false;
                // campos de cabecalho
                // A30Clientes
                label22.Visible = false;
                label20.Visible = false;
                label21.Visible = false;
                label19.Visible = false;
                label9.Visible = false;
                lblCidade2.Visible = false;
                lblUF2.Visible = false;
                // OsCliente
                lblCabCliente.Visible = true;
                lblCabIE.Visible = true;
                lblCabNCPJ.Visible = true;
                lblCabTelefone.Visible = true;
                lblCabObservacao.Visible = true;
                lblCidadeNovoCliente.Visible = true;
                lblUFNovoCliente.Visible = true;

                // redefinir binding contato
                grpPrincipal.DataBindings.Clear();
                grpPrincipal.DataBindings.Add(
                    new Binding("Text", osClienteBindingSource, "contato", true));

                txtEmail.DataBindings.Clear();
                txtEmail.DataBindings.Add(
                    new Binding("Text", osClienteBindingSource, "email", true));


                // hack limpar campo
                if (limpa_codigo_osexpress)
                {
                    limpa_codigo_osexpress = false;
                    cmbCodCliente.SelectedIndex = -1;
                    lblUFNovoCliente.Text = "";
                    txtObservacao.Text = "";

                    //DataRowView current = (DataRowView)osClienteBindingSource.Current;
                   
                }
            //}
            //else if (rdbOrigSigPosto.Checked)
            //{
            //    pnlClienteOsExpress.Visible = false;
            //    pnlClienteSigposto.Visible = true;
            //    // campos de cabecalho
            //    //label21.Visible = false;        //CNPJ
            //    // A30Clientes
            //    label22.Visible = true;
            //    label20.Visible = true;
            //    label21.Visible = true;
            //    label19.Visible = true;
            //    label9.Visible = true;
            //    // OsCliente
            //    lblCabCliente.Visible = false;
            //    lblCabIE.Visible = false;
            //    lblCabNCPJ.Visible = false;
            //    lblCabTelefone.Visible = false;
            //    lblCabObservacao.Visible = false;

            //    // redefinir binding contato
            //    grpPrincipal.DataBindings.Clear();
            //    grpPrincipal.DataBindings.Add(
            //        new Binding("Text", bs_cabecalho, "contato", true));

            //    txtEmail.DataBindings.Clear();
            //    txtEmail.DataBindings.Add(
            //        new Binding("Text", bs_cabecalho, "obs01", true));

            //}
        }

        private void osOrdemParcelaDataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            decimal valorParcela = 0;

            osOrdemParcelaBindingSource.MoveFirst();

            txtTotalDasParcelas.Text = "0,00";
            txtDifParcelaTotal.Text = "0,00";

            for (int i = 0; i < osOrdemParcelaBindingSource.Count; i++)
            {
                DataRowView current = (DataRowView)osOrdemParcelaBindingSource.Current;
                /*if ((string)current["VALOR_PARCELA"] != "")
                {
                    MessageBox.Show("Existem parcelas sem valor, verifique");
                    return;
                }*/
                
                valorParcela = valorParcela + (decimal) current["VALOR_PARCELA"];
                osOrdemParcelaBindingSource.MoveNext();
            }

            txtTotalDasParcelas.Text = valorParcela.ToString();
            txtDifParcelaTotal.Text = (valorParcela - decimal.Parse(txtValorAPagar.Text)).ToString();

            DataRowView currentOS = (DataRowView)osOrdemServicoBindingSource.Current;

            if (valorParcela > ((decimal)currentOS["TOTAL_OS"] - (decimal)currentOS["VALOR_PAGO"] - (decimal)currentOS["VALOR_DESCONTO"]))
            {
                MessageBox.Show("ATENÇÃO! O valor das parcelas ultrapassa o VALOR A PAGAR.");
                osOrdemParcelaBindingSource.CancelEdit();
            }
            else if (valorParcela < ((decimal)currentOS["TOTAL_OS"] - (decimal)currentOS["VALOR_PAGO"] - (decimal)currentOS["VALOR_DESCONTO"]))
            {
                MessageBox.Show("ATENÇÃO! O valor das parcelas é menor que o VALOR A PAGAR.");
                osOrdemParcelaBindingSource.CancelEdit();
            }
            
        }

        private void bindingNavigator_RefreshItems(object sender, EventArgs e)
        {
            var dummy = 1;
        }
      
                       
        private void cmbNomeCliente_Click(object sender, EventArgs e)
        {
            //osClienteBindingSource.Sort = "RAZAO_SOCIAL";
            var debug = 1;
        }

        private void cmbCodCliente_Click(object sender, EventArgs e)
        {
           // osClienteBindingSource.Sort = "ID";
        }

        private void cmbCodCLienteSigPosto_Click(object sender, EventArgs e)
        {
            /*
            if (bs_cabecalho.Sort.Equals("cliente"))
                return;

            var cmb = (ComboBox)sender;

            int selectedValue =-1;
            if (cmb.SelectedValue != null)
             selectedValue = (int)cmb.SelectedValue;

            cmb.BeginUpdate();
            bs_cabecalho.Sort = "cliente";
            cmb.SelectedValue = selectedValue;

            cmb.DroppedDown = true;

            cmb.EndUpdate();
             */
        }
        private void cmbNomeCLienteSigPosto_Click(object sender, EventArgs e)
        {
            var debug = 1;
            /*
            if (bs_cabecalho.Sort.Equals("nome"))
                return;

            var cmb = (ComboBox)sender;
            int selectedValue = -1;
            if (cmb.SelectedValue != null)
                selectedValue = (int)cmb.SelectedValue;

            cmb.BeginUpdate();
            bs_cabecalho.Sort = "nome";
            cmb.SelectedValue = selectedValue;

            cmb.DroppedDown = true;
            cmb.EndUpdate();
             */

        }
        private void cmbCGCClienteSigposto_Click(object sender, EventArgs e)
        {
            /*
            if (bs_cabecalho.Sort.Equals("cgc"))
                return;

            var cmb = (ComboBox)sender;
            int selectedValue = -1;
            if (cmb.SelectedValue != null)
                selectedValue = (int)cmb.SelectedValue;
            cmb.BeginUpdate();
            bs_cabecalho.Sort = "cgc";
            cmb.SelectedValue = selectedValue;

            cmb.DroppedDown = true;
            cmb.EndUpdate();
             */
        }
        private void cmbLogotipoClienteSigposto_Click(object sender, EventArgs e)
        {

            var debug = 1;
            /*
            if (bs_cabecalho.Sort.Equals("logotipo"))
                return;

            var cmb = (ComboBox)sender;
            int selectedValue = -1;
            if (cmb.SelectedValue != null)
                selectedValue = (int)cmb.SelectedValue;
            cmb.BeginUpdate();
            bs_cabecalho.Sort = "logotipo";
            cmb.SelectedValue = selectedValue;

            cmb.DroppedDown = true;
            cmb.EndUpdate();
              */
        }

        private void cmbGrupoProduto_SelectionChangeCommitted(object sender, EventArgs e)
        {
            //MessageBox.Show(cmbGrupoProduto.SelectedItem.ToString());
            var grupo = cmbGrupoProduto.SelectedItem.ToString();
            var codigo = grupo.Substring(0, 3);

            cmbProdutosSetBindings(codigo);
            //var descricao = grupo.Substring(6);
            //cmbCodigoProduto.DataSource = null;
            //cmbDescricaoProduto.DataSource = null;
            //cmbCodigoProduto.Items.Clear();
            //cmbDescricaoProduto.Items.Clear();
            //DB.FillComboBox(cmbCodigoProduto, "select produto val from a30produto where grupo ='"+codigo+"'order by produto");
            //DB.FillComboBox(cmbDescricaoProduto, "select descricao val from a30produto where grupo ='" + codigo + "'order by descricao");
        }

        private void cmbCodCLienteSigPosto_MouseEnter(object sender, EventArgs e)
        {
            //bdsFiltroCliente.Sort = "Cliente";
        }

        private void lblNomeCliente_MouseEnter(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void lblNomeCliente_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }


         //int selectedValue = -1;
         //   if (cmb.SelectedValue != null)
         //       selectedValue = (int)cmb.SelectedValue;
         //   cmb.BeginUpdate();
         //   bs_cabecalho.Sort = "cgc";
         //   cmb.SelectedValue = selectedValue;

            //cmb.DroppedDown = true;
            //cmb.EndUpdate();

        private void lblNomeCliente_Click(object sender, EventArgs e)
        {
            // 26.01.
            //bool novo = rdbOrigOsExpress.Checked;
            //var cmb = novo ? cmbNomeCliente : cmbNomeCLienteSigPosto;
            var cmb = cmbNomeCliente;
            int selectedValue = -1;
            if (cmb.SelectedValue != null)
                   selectedValue = (int)cmb.SelectedValue;

            cmb.BeginUpdate();           
            //if (novo)
            //{
                // sort osCliente
                osClienteBindingSource.Sort = "RAZAO_SOCIAL";
            //}
            //else
            //{
                // sort A30Clientes
            //    bs_cabecalho.Sort = "NOME";
            //}
            cmb.SelectedValue = selectedValue;
            cmb.EndUpdate();
        }
        //
       
        private void lblCodigoCliente_Click(object sender, EventArgs e)
        {
            //bool novo = rdbOrigOsExpress.Checked;
            //var cmb = novo ? cmbCodCliente : cmbCodCLienteSigPosto;
            var cmb = cmbCodCliente;

            int selectedValue = -1;
            if (cmb.SelectedValue != null)
                selectedValue = (int)cmb.SelectedValue;
            cmb.BeginUpdate();
           
            //if (novo)
            //{
                // sort osCliente
                osClienteBindingSource.Sort = "ID";
            //}
            //else
            //{
                // sort A30Clientes
                //bs_cabecalho.Sort = "cliente";
            //}
            cmb.SelectedValue = selectedValue;
            cmb.EndUpdate();
        }
        private void lblCGCCliente_Click(object sender, EventArgs e)
        {
            //bool novo = rdbOrigOsExpress.Checked;
            //var cmb = novo ? cmbCGCCliente : cmbCGCClienteSigposto;
            var cmb = cmbCGCCliente ;

            int selectedValue = -1;
            if (cmb.SelectedValue != null)
                selectedValue = (int)cmb.SelectedValue;
            cmb.BeginUpdate();
            
            //if (novo)
            //{
                // sort osCliente               
                osClienteBindingSource.Sort = "CNPJ";
            //}
            //else
            //{
                // sort A30Clientes
                //bs_cabecalho.Sort = "cgc";
            //}
            cmb.SelectedValue = selectedValue;
            cmb.EndUpdate();
        }

        private void lblLogotipoCliente_Click(object sender, EventArgs e)
        {
            //bool novo = rdbOrigOsExpress.Checked;
            //var cmb = novo ? cmbLogotipoCliente : cmbLogotipoClienteSigposto;
            var cmb =  cmbLogotipoCliente ;

            int selectedValue = -1;
            if (cmb.SelectedValue != null)
                selectedValue = (int)cmb.SelectedValue;
            cmb.BeginUpdate();
          
            //if (novo)
            //{
                // sort osCliente               
                osClienteBindingSource.Sort = "NOME_FANTASIA";
            //}
            //else
            //{
                // sort A30Clientes
                //bs_cabecalho.Sort = "logotipo";
            //}
            cmb.SelectedValue = selectedValue;
            cmb.EndUpdate();
        }

        private void lblCodigoCliente_MouseEnter(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void lblCodigoCliente_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void lblCGCCliente_MouseEnter(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void lblCGCCliente_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void lblLogotipoCliente_MouseEnter(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void lblLogotipoCliente_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }


        #region DONOTHING
        /// <summary>
        /// DO NOTHING
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCodCLienteSigPosto_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// DO NOTHING
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbNomeCLienteSigPosto_SelectionChangeCommitted(object sender, EventArgs e)
        {
            var debug = 1;
        }

        /// <summary>
        /// DO NOTHING
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCodCliente_SelectionChangeCommitted(object sender, EventArgs e)
        {
           
        }

        /// <summary>
        /// DO NOTHING
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbNomeCliente_SelectionChangeCommitted(object sender, EventArgs e)
        {
            var debug = 1;
        }
        #endregion

        private bool isOsCliente(string id) {
            string sql = @"select count(1) from OsCliente where id = " + id;
            var cnt = (int)DB.GetSingleValue(sql);
            if (cnt > 0) return true;
            return false;
        }
        // fill fields if empty ...
        private bool fillFields()
        {
            DataRowView current = (DataRowView)osOrdemServicoBindingSource.Current;
            var id = current["id"];
            var obs = current["obs"];
            var idcliente = current["idcliente"];
            var idcliente_pedido = current["idcliente_pedido"];


            string sql = "";
            if ((lblCabCliente.Visible == true) && (lblCabCliente.Text == "")) 
            {
                sql = @"select id, razao_social, nome_fantasia, cnpj,telefone,ie,endereco,uf,cidade from oscliente where id="+idcliente_pedido;
                DataTable table = DB.GetTableFromSQL(sql);
                if (table != null && table.Rows != null && table.Rows.Count > 0) { 
                    lblCabCliente.Text = table.Rows[0]["razao_social"].ToString();
                    lblCabIE.Text = table.Rows[0]["ie"].ToString();
                    lblCabNCPJ.Text = table.Rows[0]["cnpj"].ToString();
                    lblCabTelefone.Text = table.Rows[0]["telefone"].ToString();

                    lblCabObservacao.Text = table.Rows[0]["endereco"].ToString();
                    lblCidadeNovoCliente.Text = table.Rows[0]["cidade"].ToString();
                    lblUFNovoCliente.Text = table.Rows[0]["uf"].ToString();

                    cmbCGCCliente.SelectedValue = idcliente_pedido;
                }
            }
            return true;
        }
        // proximo
        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            var x = 1;
            DataRowView current = (DataRowView)osOrdemServicoBindingSource.Current;
            var id  = current["id"];
            var obs = current["obs"];
            //var idcliente = current["idcliente"];
            //var idcliente_pedido = current["idcliente_pedido"];
            //// no effect
            //var idcliente_pedido = current["idcliente_pedido"];
            //int idx = osClienteBindingSource.Find(@"Id", idcliente_pedido);
            //if (idx >= 0)
            //{
            //    osClienteBindingSource.Position = idx;                            
            //}
            //foreach (var binding in this.BindingContext[osClienteBindingSource].Bindings.Cast<Binding>())
            //    binding.ReadValue();

            DataRowView currentCliente = (DataRowView)osClienteBindingSource.Current;
            var razao_social = currentCliente["razao_social"];
            
            foreach (var binding in this.BindingContext[osOrdemServicoBindingSource].Bindings.Cast<Binding>())
                binding.ReadValue();
            fillFields();
        }
        // anterior
        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            var x = 1;
            DataRowView current = (DataRowView)osOrdemServicoBindingSource.Current;
            var id = current["id"];
            var obs = current["obs"];
            DataRowView currentCliente = (DataRowView)osClienteBindingSource.Current;
            var razao_social = currentCliente["razao_social"];

            foreach (var binding in this.BindingContext[osOrdemServicoBindingSource].Bindings.Cast<Binding>())
                binding.ReadValue();

            fillFields();
        }
       
        private void tabCabecalho_Enter(object sender, EventArgs e)
        {
            var dummy = 1;
            DataRowView current = (DataRowView)osOrdemServicoBindingSource.Current;
            var id = current["id"];
            var obs = current["obs"];
            Debug.WriteLine("OBS: "+ obs);
            Debug.WriteLine("OBS-Textbox: " + txtObservacao.Text);

            DataRowView currentCliente = (DataRowView)osClienteBindingSource.Current;
            var razao_social = currentCliente["razao_social"];

            foreach (var binding in this.BindingContext[osOrdemServicoBindingSource].Bindings.Cast<Binding>())
                binding.ReadValue();
            dummy = 2;
        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            foreach (var binding in this.BindingContext[osOrdemServicoBindingSource].Bindings.Cast<Binding>())
                binding.ReadValue();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            foreach (var binding in this.BindingContext[osOrdemServicoBindingSource].Bindings.Cast<Binding>())
                binding.ReadValue();
        }

        private void hideMensal() {
            vENCIMENTO_MANU_MENSALDateTimePicker.Location = new Point(-32000, -32000);                       
            mANUTENCAO_MENSALTextBox.Location = new Point(-32000, -32000);

            foreach (Control c in tabPrincipal.TabPages["tabCabecalho"].Controls)
            {
                if (c.GetType() == typeof(Label))
                {
                    if (c.Name == "ven_man_men_label")
                        c.Location = new Point(-32000, -32000);
                    if (c.Name == "val_man_men_label")
                        c.Location = new Point(-32000, -32000);                    
                }
            }
        }
        private void showMensal()
        {
            vENCIMENTO_MANU_MENSALDateTimePicker.Location = new Point(708, 11);
            mANUTENCAO_MENSALTextBox.Location = new Point(706, 38);

            foreach (Control c in tabPrincipal.TabPages["tabCabecalho"].Controls)
            {
                if (c.GetType() == typeof(Label))
                {
                    if (c.Name == "ven_man_men_label")
                        c.Location = new Point(509,18);
                    if (c.Name == "val_man_men_label")
                        c.Location = new Point(544, 44);
                }
            }
        }
        private void cbSingle_CheckedChanged(object sender, EventArgs e)
        {
            if (cbSingle.Checked)
            {
                showMensal();
            }
            else 
            {
                hideMensal();
            }
        }

        private string formatCelular(string celular)
        {
            if (celular.Length != 11) return celular;
            string ret = Convert.ToUInt64(celular).ToString(@"(00) 0 0000-0000");
            return ret;
        }

        private void cmbEmpresa_SelectionChangeCommitted(object sender, EventArgs e)
        {
            DataRowView currentEmp = (DataRowView)bs_empresa.Current;

            OsConfiguracoes.idPosto = (int)((DataRowView)cmbEmpresa.SelectedItem)["id"];

            OsConfiguracoes.nomePosto = ((DataRowView)cmbEmpresa.SelectedItem)["razao"].ToString();

            OsConfiguracoes.codigoPosto = ((DataRowView)cmbEmpresa.SelectedItem)["codigo"].ToString();
        }

        private void cmbNomeUsuario_Click(object sender, EventArgs e)
        {
            DataRowView x = (DataRowView)osOrdemServicoBindingSource.Current;
            DataRowView y = (DataRowView)osClienteBindingSource.Current;
            var debug = 1;

        }
        private void cmbNomeUsuario_SelectionChangeCommitted(object sender, EventArgs e)
        {
            DataRowView x = (DataRowView)osOrdemServicoBindingSource.Current;
            DataRowView y = (DataRowView)osClienteBindingSource.Current;
            var debug = 1;
        }

        private void cmbNomeUsuario_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataRowView x = (DataRowView)osOrdemServicoBindingSource.Current;
            DataRowView y = (DataRowView)osClienteBindingSource.Current;
            var debug = 1;
        }

        private void osOrdemServicoBindingSource_ListChanged(object sender, System.ComponentModel.ListChangedEventArgs e)
        {
            DataRowView x = (DataRowView)osOrdemServicoBindingSource.Current;
            DataRowView y = (DataRowView)osClienteBindingSource.Current;
            var debug = 1;
        }

        private void osClienteBindingSource_CurrentChanged(object sender, EventArgs e)
        {
            DataRowView x = (DataRowView)osOrdemServicoBindingSource.Current;
            DataRowView y = (DataRowView)osClienteBindingSource.Current;
            var debug = 1;
        }

        private void osClienteBindingSource_ListChanged(object sender, System.ComponentModel.ListChangedEventArgs e)
        {
            DataRowView x = (DataRowView)osOrdemServicoBindingSource.Current;
            DataRowView y = (DataRowView)osClienteBindingSource.Current;
            var debug = 1;
        }
              
    }
}
