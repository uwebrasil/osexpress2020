﻿namespace osExpress.Manutencoes
{
    partial class ManFuncionario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label label_rg;
            System.Windows.Forms.Label fAXLabel;
            System.Windows.Forms.Label tELEFONELabel;
            System.Windows.Forms.Label bAIRROLabel;
            System.Windows.Forms.Label eNDERECOLabel;
            System.Windows.Forms.Label cIDADELabel;
            System.Windows.Forms.Label uFLabel;
            System.Windows.Forms.Label labelContatosNumero;
            System.Windows.Forms.Label labelContatosObs;
            System.Windows.Forms.Label labelContatosCPF;
            System.Windows.Forms.Label labelContatosCelular;
            System.Windows.Forms.Label labelContatosEmail;
            System.Windows.Forms.Label labelContatosNome;
            System.Windows.Forms.Label labelCodigoSimp;
            System.Windows.Forms.Label tIPO_BOMBALabel;
            System.Windows.Forms.Label qTDE_BICOSLabel;
            System.Windows.Forms.Label qTDE_BOMBASLabel;
            System.Windows.Forms.Label bANDEIRALabel;
            System.Windows.Forms.Label mODELO_CPULabel;
            System.Windows.Forms.Label cODIGO_SIGPOSTOLabel;
            System.Windows.Forms.Label labelEventosNumero;
            System.Windows.Forms.Label lblLacre;
            System.Windows.Forms.Label lblFabricante;
            System.Windows.Forms.Label lblDescricao;
            System.Windows.Forms.Label lblIlha;
            System.Windows.Forms.Label lblQBicos;
            System.Windows.Forms.Label lblSerie;
            System.Windows.Forms.Label lblMarcaInstrumento;
            System.Windows.Forms.Label labelNome;
            System.Windows.Forms.Label nOME_FANTASIALabel;
            System.Windows.Forms.Label tIPO_PESSOALabel;
            System.Windows.Forms.Label iDLabel;
            System.Windows.Forms.Label cNPJLabel;
            System.Windows.Forms.Label labelEmail;
            System.Windows.Forms.Label cPFLabel;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ManFuncionario));
            this.labelCep = new System.Windows.Forms.Label();
            this.labelCadastrarFuncionario = new System.Windows.Forms.Label();
            this.pnlConsultar = new System.Windows.Forms.Panel();
            this.tbLike = new System.Windows.Forms.TextBox();
            this.labelLike = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label70 = new System.Windows.Forms.Label();
            this.lblTotalOS = new System.Windows.Forms.Label();
            this.dgvFuncionario = new System.Windows.Forms.DataGridView();
            this.CNPJ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.postoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.funcionarioDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.turnoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipoPessoaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cgcDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.inscricaoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.enderecoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bairroDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cidadeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.estadoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cepDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs01DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs02DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs03DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs04DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs05DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs06DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs07DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs08DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs09DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs10DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.noturnoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.inativoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtAlterDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs11DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs12DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs13DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs14DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs15DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs16DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs17DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs18DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs19DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obs20DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rFIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.biometriaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cTACaixaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipoVIPDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uNomeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uSenhaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipoacessoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtcadastroDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ultimoacessoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.planocontaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.acessapedidoDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.digitaselolacreDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.idusuarioDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isusuarioDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bsXOSFuncionario = new System.Windows.Forms.BindingSource(this.components);
            this.dtsXOSFuncionario = new osExpress.XOSFuncionario();
            this.xOsFuncionarioTableAdapter = new osExpress.XOSFuncionarioTableAdapters.XOsFuncionarioTableAdapter();
            this.FuncionarioBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bnInserir = new System.Windows.Forms.ToolStripButton();
            this.bnCount = new System.Windows.Forms.ToolStripLabel();
            this.bnConsultar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.bnGravar = new System.Windows.Forms.ToolStripButton();
            this.bnCancelar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.bnPrimeiro = new System.Windows.Forms.ToolStripButton();
            this.bnAnterior = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bnPosition = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bnProximo = new System.Windows.Forms.ToolStripButton();
            this.bnUltimo = new System.Windows.Forms.ToolStripButton();
            this.bnDescartar = new System.Windows.Forms.ToolStripButton();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.labelSenha = new System.Windows.Forms.Label();
            this.labelLogin = new System.Windows.Forms.Label();
            this.tbSenha = new System.Windows.Forms.TextBox();
            this.tbLogin = new System.Windows.Forms.TextBox();
            this.checkboxIsUsuario = new System.Windows.Forms.CheckBox();
            this.tb_rg = new System.Windows.Forms.TextBox();
            this.tabChild = new System.Windows.Forms.TabControl();
            this.tabEndereco = new System.Windows.Forms.TabPage();
            this.tELEFONETextBox = new System.Windows.Forms.TextBox();
            this.fAXTextBox = new System.Windows.Forms.TextBox();
            this.mtbTelefone = new System.Windows.Forms.MaskedTextBox();
            this.tbBairro = new System.Windows.Forms.TextBox();
            this.tbEndereco = new System.Windows.Forms.TextBox();
            this.tbCidade = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.tbCep = new System.Windows.Forms.TextBox();
            this.tabContatos = new System.Windows.Forms.TabPage();
            this.checkboxPrincipal = new System.Windows.Forms.CheckBox();
            this.listView1 = new System.Windows.Forms.ListView();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.textboxContatosObs = new System.Windows.Forms.TextBox();
            this.maskedTextboxContatosCpf = new System.Windows.Forms.MaskedTextBox();
            this.buttonContatosDeletar = new System.Windows.Forms.Button();
            this.buttonContatosCancelar = new System.Windows.Forms.Button();
            this.buttonContatosGravar = new System.Windows.Forms.Button();
            this.maskedTextboxContatosCelular = new System.Windows.Forms.MaskedTextBox();
            this.textboxContatosEmail = new System.Windows.Forms.TextBox();
            this.textboxContatosNome = new System.Windows.Forms.TextBox();
            this.textboxHiddenId = new System.Windows.Forms.TextBox();
            this.tabOutros = new System.Windows.Forms.TabPage();
            this.textboxCodigoSimp = new System.Windows.Forms.TextBox();
            this.textboxQuantSumpers = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textboxQuantTanques = new System.Windows.Forms.TextBox();
            this.checkboxRFID = new System.Windows.Forms.CheckBox();
            this.labelConsentrador = new System.Windows.Forms.Label();
            this.labelQuantSumpers = new System.Windows.Forms.Label();
            this.labelQuantTanques = new System.Windows.Forms.Label();
            this.qTDE_BOMBASTextBox = new System.Windows.Forms.TextBox();
            this.tIPO_BOMBATextBox = new System.Windows.Forms.TextBox();
            this.qTDE_BICOSTextBox = new System.Windows.Forms.TextBox();
            this.bANDEIRATextBox = new System.Windows.Forms.TextBox();
            this.mODELO_CPUTextBox = new System.Windows.Forms.TextBox();
            this.tabObservacoes = new System.Windows.Forms.TabPage();
            this.oBSERVACAOTextBox = new System.Windows.Forms.TextBox();
            this.cODIGO_SIGPOSTOTextBox = new System.Windows.Forms.TextBox();
            this.tabEventos = new System.Windows.Forms.TabPage();
            this.btnListEventos = new System.Windows.Forms.Button();
            this.textboxHiddenIdEvento = new System.Windows.Forms.TextBox();
            this.buttonEventosDeletar = new System.Windows.Forms.Button();
            this.buttonEventosGravar = new System.Windows.Forms.Button();
            this.buttonEventosCancelar = new System.Windows.Forms.Button();
            this.datetimeRealizacao = new System.Windows.Forms.DateTimePicker();
            this.listviewEventos = new System.Windows.Forms.ListView();
            this.checkboxAlertar = new System.Windows.Forms.CheckBox();
            this.textboxApontamento = new System.Windows.Forms.TextBox();
            this.labelApontamento = new System.Windows.Forms.Label();
            this.labelDataEHora = new System.Windows.Forms.Label();
            this.labelEM = new System.Windows.Forms.Label();
            this.tabOrcamentos = new System.Windows.Forms.TabPage();
            this.dataGridViewOrcamentos = new System.Windows.Forms.DataGridView();
            this.tabPedidos = new System.Windows.Forms.TabPage();
            this.dataGridViewPedidos = new System.Windows.Forms.DataGridView();
            this.tabServicos = new System.Windows.Forms.TabPage();
            this.dataGridViewServicos = new System.Windows.Forms.DataGridView();
            this.tabInstrumentos = new System.Windows.Forms.TabPage();
            this.rtbHelp2 = new System.Windows.Forms.RichTextBox();
            this.rtbHelp0 = new System.Windows.Forms.RichTextBox();
            this.btnLimparBomba = new System.Windows.Forms.Button();
            this.pnlEditarBomba = new System.Windows.Forms.Panel();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.rtbHelp1 = new System.Windows.Forms.RichTextBox();
            this.btnGerarLacre = new System.Windows.Forms.Button();
            this.btnDeletarBomba = new System.Windows.Forms.Button();
            this.btnGravarBomba = new System.Windows.Forms.Button();
            this.dgvLacre = new System.Windows.Forms.DataGridView();
            this.tbLacre = new System.Windows.Forms.TextBox();
            this.tbFabricante = new System.Windows.Forms.TextBox();
            this.tbDescricao = new System.Windows.Forms.TextBox();
            this.tbIlha = new System.Windows.Forms.TextBox();
            this.tbQBicos = new System.Windows.Forms.TextBox();
            this.tbSerie = new System.Windows.Forms.TextBox();
            this.cmbMarcaInstrumento = new System.Windows.Forms.ComboBox();
            this.dgvBombas = new System.Windows.Forms.DataGridView();
            this.tbNome = new System.Windows.Forms.TextBox();
            this.nOME_FANTASIATextBox = new System.Windows.Forms.TextBox();
            this.mtbCPF = new System.Windows.Forms.MaskedTextBox();
            this.mtbCNPJ = new System.Windows.Forms.MaskedTextBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.tbIdusuario = new System.Windows.Forms.TextBox();
            this.tb_email = new System.Windows.Forms.TextBox();
            this.cPFTextBox = new System.Windows.Forms.TextBox();
            this.cNPJTextBox = new System.Windows.Forms.TextBox();
            label_rg = new System.Windows.Forms.Label();
            fAXLabel = new System.Windows.Forms.Label();
            tELEFONELabel = new System.Windows.Forms.Label();
            bAIRROLabel = new System.Windows.Forms.Label();
            eNDERECOLabel = new System.Windows.Forms.Label();
            cIDADELabel = new System.Windows.Forms.Label();
            uFLabel = new System.Windows.Forms.Label();
            labelContatosNumero = new System.Windows.Forms.Label();
            labelContatosObs = new System.Windows.Forms.Label();
            labelContatosCPF = new System.Windows.Forms.Label();
            labelContatosCelular = new System.Windows.Forms.Label();
            labelContatosEmail = new System.Windows.Forms.Label();
            labelContatosNome = new System.Windows.Forms.Label();
            labelCodigoSimp = new System.Windows.Forms.Label();
            tIPO_BOMBALabel = new System.Windows.Forms.Label();
            qTDE_BICOSLabel = new System.Windows.Forms.Label();
            qTDE_BOMBASLabel = new System.Windows.Forms.Label();
            bANDEIRALabel = new System.Windows.Forms.Label();
            mODELO_CPULabel = new System.Windows.Forms.Label();
            cODIGO_SIGPOSTOLabel = new System.Windows.Forms.Label();
            labelEventosNumero = new System.Windows.Forms.Label();
            lblLacre = new System.Windows.Forms.Label();
            lblFabricante = new System.Windows.Forms.Label();
            lblDescricao = new System.Windows.Forms.Label();
            lblIlha = new System.Windows.Forms.Label();
            lblQBicos = new System.Windows.Forms.Label();
            lblSerie = new System.Windows.Forms.Label();
            lblMarcaInstrumento = new System.Windows.Forms.Label();
            labelNome = new System.Windows.Forms.Label();
            nOME_FANTASIALabel = new System.Windows.Forms.Label();
            tIPO_PESSOALabel = new System.Windows.Forms.Label();
            iDLabel = new System.Windows.Forms.Label();
            cNPJLabel = new System.Windows.Forms.Label();
            labelEmail = new System.Windows.Forms.Label();
            cPFLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dtsPrincipal)).BeginInit();
            this.tabPrincipal.SuspendLayout();
            this.pnlConsultar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFuncionario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsXOSFuncionario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtsXOSFuncionario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FuncionarioBindingNavigator)).BeginInit();
            this.FuncionarioBindingNavigator.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabChild.SuspendLayout();
            this.tabEndereco.SuspendLayout();
            this.tabContatos.SuspendLayout();
            this.tabOutros.SuspendLayout();
            this.tabObservacoes.SuspendLayout();
            this.tabEventos.SuspendLayout();
            this.tabOrcamentos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOrcamentos)).BeginInit();
            this.tabPedidos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPedidos)).BeginInit();
            this.tabServicos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewServicos)).BeginInit();
            this.tabInstrumentos.SuspendLayout();
            this.pnlEditarBomba.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLacre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBombas)).BeginInit();
            this.SuspendLayout();
            // 
            // grpPrincipal
            // 
            this.grpPrincipal.Visible = false;
            // 
            // tabPrincipal
            // 
            this.tabPrincipal.Controls.Add(this.tabPage1);
            this.tabPrincipal.Size = new System.Drawing.Size(960, 580);
            // 
            // label_rg
            // 
            label_rg.AutoSize = true;
            label_rg.Location = new System.Drawing.Point(360, 10);
            label_rg.Name = "label_rg";
            label_rg.Size = new System.Drawing.Size(29, 13);
            label_rg.TabIndex = 52;
            label_rg.Text = "R.G.";
            // 
            // fAXLabel
            // 
            fAXLabel.AutoSize = true;
            fAXLabel.Location = new System.Drawing.Point(300, 95);
            fAXLabel.Name = "fAXLabel";
            fAXLabel.Size = new System.Drawing.Size(24, 13);
            fAXLabel.TabIndex = 66;
            fAXLabel.Text = "Fax";
            fAXLabel.Visible = false;
            // 
            // tELEFONELabel
            // 
            tELEFONELabel.AutoSize = true;
            tELEFONELabel.Location = new System.Drawing.Point(10, 95);
            tELEFONELabel.Name = "tELEFONELabel";
            tELEFONELabel.Size = new System.Drawing.Size(49, 13);
            tELEFONELabel.TabIndex = 61;
            tELEFONELabel.Text = "Telefone";
            // 
            // bAIRROLabel
            // 
            bAIRROLabel.AutoSize = true;
            bAIRROLabel.Location = new System.Drawing.Point(450, 55);
            bAIRROLabel.Name = "bAIRROLabel";
            bAIRROLabel.Size = new System.Drawing.Size(34, 13);
            bAIRROLabel.TabIndex = 60;
            bAIRROLabel.Text = "Bairro";
            // 
            // eNDERECOLabel
            // 
            eNDERECOLabel.AutoSize = true;
            eNDERECOLabel.Location = new System.Drawing.Point(10, 55);
            eNDERECOLabel.Name = "eNDERECOLabel";
            eNDERECOLabel.Size = new System.Drawing.Size(53, 13);
            eNDERECOLabel.TabIndex = 58;
            eNDERECOLabel.Text = "Endereço";
            // 
            // cIDADELabel
            // 
            cIDADELabel.AutoSize = true;
            cIDADELabel.Location = new System.Drawing.Point(240, 10);
            cIDADELabel.Name = "cIDADELabel";
            cIDADELabel.Size = new System.Drawing.Size(40, 13);
            cIDADELabel.TabIndex = 57;
            cIDADELabel.Text = "Cidade";
            // 
            // uFLabel
            // 
            uFLabel.AutoSize = true;
            uFLabel.Location = new System.Drawing.Point(170, 10);
            uFLabel.Name = "uFLabel";
            uFLabel.Size = new System.Drawing.Size(40, 13);
            uFLabel.TabIndex = 55;
            uFLabel.Text = "Estado";
            // 
            // labelContatosNumero
            // 
            labelContatosNumero.AutoSize = true;
            labelContatosNumero.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            labelContatosNumero.Location = new System.Drawing.Point(10, 105);
            labelContatosNumero.Name = "labelContatosNumero";
            labelContatosNumero.Size = new System.Drawing.Size(135, 13);
            labelContatosNumero.TabIndex = 35;
            labelContatosNumero.Text = "Número dos contatos: ";
            // 
            // labelContatosObs
            // 
            labelContatosObs.AutoSize = true;
            labelContatosObs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            labelContatosObs.Location = new System.Drawing.Point(10, 55);
            labelContatosObs.Name = "labelContatosObs";
            labelContatosObs.Size = new System.Drawing.Size(65, 13);
            labelContatosObs.TabIndex = 24;
            labelContatosObs.Text = "Observação";
            // 
            // labelContatosCPF
            // 
            labelContatosCPF.AutoSize = true;
            labelContatosCPF.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            labelContatosCPF.Location = new System.Drawing.Point(530, 55);
            labelContatosCPF.Name = "labelContatosCPF";
            labelContatosCPF.Size = new System.Drawing.Size(27, 13);
            labelContatosCPF.TabIndex = 22;
            labelContatosCPF.Text = "CPF";
            // 
            // labelContatosCelular
            // 
            labelContatosCelular.AutoSize = true;
            labelContatosCelular.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            labelContatosCelular.Location = new System.Drawing.Point(530, 10);
            labelContatosCelular.Name = "labelContatosCelular";
            labelContatosCelular.Size = new System.Drawing.Size(39, 13);
            labelContatosCelular.TabIndex = 17;
            labelContatosCelular.Text = "Celular";
            // 
            // labelContatosEmail
            // 
            labelContatosEmail.AutoSize = true;
            labelContatosEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            labelContatosEmail.Location = new System.Drawing.Point(270, 10);
            labelContatosEmail.Name = "labelContatosEmail";
            labelContatosEmail.Size = new System.Drawing.Size(32, 13);
            labelContatosEmail.TabIndex = 15;
            labelContatosEmail.Text = "Email";
            // 
            // labelContatosNome
            // 
            labelContatosNome.AutoSize = true;
            labelContatosNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            labelContatosNome.Location = new System.Drawing.Point(10, 10);
            labelContatosNome.Name = "labelContatosNome";
            labelContatosNome.Size = new System.Drawing.Size(35, 13);
            labelContatosNome.TabIndex = 13;
            labelContatosNome.Text = "Nome";
            // 
            // labelCodigoSimp
            // 
            labelCodigoSimp.AutoSize = true;
            labelCodigoSimp.Location = new System.Drawing.Point(510, 10);
            labelCodigoSimp.Name = "labelCodigoSimp";
            labelCodigoSimp.Size = new System.Drawing.Size(69, 13);
            labelCodigoSimp.TabIndex = 61;
            labelCodigoSimp.Text = "Código SIMP";
            // 
            // tIPO_BOMBALabel
            // 
            tIPO_BOMBALabel.AutoSize = true;
            tIPO_BOMBALabel.Location = new System.Drawing.Point(510, 60);
            tIPO_BOMBALabel.Name = "tIPO_BOMBALabel";
            tIPO_BOMBALabel.Size = new System.Drawing.Size(82, 13);
            tIPO_BOMBALabel.TabIndex = 50;
            tIPO_BOMBALabel.Text = "Tipo de Bomba:";
            // 
            // qTDE_BICOSLabel
            // 
            qTDE_BICOSLabel.AutoSize = true;
            qTDE_BICOSLabel.Location = new System.Drawing.Point(260, 60);
            qTDE_BICOSLabel.Name = "qTDE_BICOSLabel";
            qTDE_BICOSLabel.Size = new System.Drawing.Size(80, 13);
            qTDE_BICOSLabel.TabIndex = 52;
            qTDE_BICOSLabel.Text = "Qtde. de Bicos:";
            // 
            // qTDE_BOMBASLabel
            // 
            qTDE_BOMBASLabel.AutoSize = true;
            qTDE_BOMBASLabel.Location = new System.Drawing.Point(10, 60);
            qTDE_BOMBASLabel.Name = "qTDE_BOMBASLabel";
            qTDE_BOMBASLabel.Size = new System.Drawing.Size(92, 13);
            qTDE_BOMBASLabel.TabIndex = 51;
            qTDE_BOMBASLabel.Text = "Qtde. de Bombas:";
            // 
            // bANDEIRALabel
            // 
            bANDEIRALabel.AutoSize = true;
            bANDEIRALabel.Location = new System.Drawing.Point(10, 10);
            bANDEIRALabel.Name = "bANDEIRALabel";
            bANDEIRALabel.Size = new System.Drawing.Size(52, 13);
            bANDEIRALabel.TabIndex = 45;
            bANDEIRALabel.Text = "Bandeira:";
            // 
            // mODELO_CPULabel
            // 
            mODELO_CPULabel.AutoSize = true;
            mODELO_CPULabel.Location = new System.Drawing.Point(260, 10);
            mODELO_CPULabel.Name = "mODELO_CPULabel";
            mODELO_CPULabel.Size = new System.Drawing.Size(80, 13);
            mODELO_CPULabel.TabIndex = 46;
            mODELO_CPULabel.Text = "Marca/Modelo:";
            // 
            // cODIGO_SIGPOSTOLabel
            // 
            cODIGO_SIGPOSTOLabel.AutoSize = true;
            cODIGO_SIGPOSTOLabel.Location = new System.Drawing.Point(527, 170);
            cODIGO_SIGPOSTOLabel.Name = "cODIGO_SIGPOSTOLabel";
            cODIGO_SIGPOSTOLabel.Size = new System.Drawing.Size(88, 13);
            cODIGO_SIGPOSTOLabel.TabIndex = 64;
            cODIGO_SIGPOSTOLabel.Text = "Código SigPosto:";
            cODIGO_SIGPOSTOLabel.Visible = false;
            // 
            // labelEventosNumero
            // 
            labelEventosNumero.AutoSize = true;
            labelEventosNumero.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            labelEventosNumero.Location = new System.Drawing.Point(17, 93);
            labelEventosNumero.Name = "labelEventosNumero";
            labelEventosNumero.Size = new System.Drawing.Size(131, 13);
            labelEventosNumero.TabIndex = 36;
            labelEventosNumero.Text = "Número dos eventos: ";
            // 
            // lblLacre
            // 
            lblLacre.AutoSize = true;
            lblLacre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblLacre.Location = new System.Drawing.Point(21, 100);
            lblLacre.Name = "lblLacre";
            lblLacre.Size = new System.Drawing.Size(34, 13);
            lblLacre.TabIndex = 54;
            lblLacre.Text = "Lacre";
            // 
            // lblFabricante
            // 
            lblFabricante.AutoSize = true;
            lblFabricante.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblFabricante.Location = new System.Drawing.Point(213, 49);
            lblFabricante.Name = "lblFabricante";
            lblFabricante.Size = new System.Drawing.Size(57, 13);
            lblFabricante.TabIndex = 52;
            lblFabricante.Text = "Fabricante";
            // 
            // lblDescricao
            // 
            lblDescricao.AutoSize = true;
            lblDescricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblDescricao.Location = new System.Drawing.Point(21, 49);
            lblDescricao.Name = "lblDescricao";
            lblDescricao.Size = new System.Drawing.Size(55, 13);
            lblDescricao.TabIndex = 50;
            lblDescricao.Text = "Descrição";
            // 
            // lblIlha
            // 
            lblIlha.AutoSize = true;
            lblIlha.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblIlha.Location = new System.Drawing.Point(456, 5);
            lblIlha.Name = "lblIlha";
            lblIlha.Size = new System.Drawing.Size(24, 13);
            lblIlha.TabIndex = 48;
            lblIlha.Text = "Ilha";
            // 
            // lblQBicos
            // 
            lblQBicos.AutoSize = true;
            lblQBicos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblQBicos.Location = new System.Drawing.Point(351, 5);
            lblQBicos.Name = "lblQBicos";
            lblQBicos.Size = new System.Drawing.Size(77, 13);
            lblQBicos.TabIndex = 46;
            lblQBicos.Text = "Qtde. de Bicos";
            // 
            // lblSerie
            // 
            lblSerie.AutoSize = true;
            lblSerie.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblSerie.Location = new System.Drawing.Point(21, 5);
            lblSerie.Name = "lblSerie";
            lblSerie.Size = new System.Drawing.Size(51, 13);
            lblSerie.TabIndex = 44;
            lblSerie.Text = "Série (ID)";
            // 
            // lblMarcaInstrumento
            // 
            lblMarcaInstrumento.AutoSize = true;
            lblMarcaInstrumento.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblMarcaInstrumento.Location = new System.Drawing.Point(405, 48);
            lblMarcaInstrumento.Name = "lblMarcaInstrumento";
            lblMarcaInstrumento.Size = new System.Drawing.Size(95, 13);
            lblMarcaInstrumento.TabIndex = 42;
            lblMarcaInstrumento.Text = "Marca Instrumento";
            // 
            // labelNome
            // 
            labelNome.AutoSize = true;
            labelNome.Location = new System.Drawing.Point(100, 100);
            labelNome.Name = "labelNome";
            labelNome.Size = new System.Drawing.Size(38, 13);
            labelNome.TabIndex = 47;
            labelNome.Text = "Nome ";
            // 
            // nOME_FANTASIALabel
            // 
            nOME_FANTASIALabel.AutoSize = true;
            nOME_FANTASIALabel.Location = new System.Drawing.Point(100, 140);
            nOME_FANTASIALabel.Name = "nOME_FANTASIALabel";
            nOME_FANTASIALabel.Size = new System.Drawing.Size(78, 13);
            nOME_FANTASIALabel.TabIndex = 48;
            nOME_FANTASIALabel.Text = "Nome Fantasia";
            nOME_FANTASIALabel.Visible = false;
            // 
            // tIPO_PESSOALabel
            // 
            tIPO_PESSOALabel.AutoSize = true;
            tIPO_PESSOALabel.Location = new System.Drawing.Point(25, 10);
            tIPO_PESSOALabel.Name = "tIPO_PESSOALabel";
            tIPO_PESSOALabel.Size = new System.Drawing.Size(66, 13);
            tIPO_PESSOALabel.TabIndex = 0;
            tIPO_PESSOALabel.Text = "Tipo Pessoa";
            tIPO_PESSOALabel.Visible = false;
            // 
            // iDLabel
            // 
            iDLabel.AutoSize = true;
            iDLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            iDLabel.Location = new System.Drawing.Point(25, 100);
            iDLabel.Name = "iDLabel";
            iDLabel.Size = new System.Drawing.Size(46, 13);
            iDLabel.TabIndex = 0;
            iDLabel.Text = "Código";
            // 
            // cNPJLabel
            // 
            cNPJLabel.AutoSize = true;
            cNPJLabel.Location = new System.Drawing.Point(110, 55);
            cNPJLabel.Name = "cNPJLabel";
            cNPJLabel.Size = new System.Drawing.Size(34, 13);
            cNPJLabel.TabIndex = 0;
            cNPJLabel.Text = "CNPJ";
            cNPJLabel.Visible = false;
            // 
            // labelEmail
            // 
            labelEmail.AutoSize = true;
            labelEmail.Location = new System.Drawing.Point(360, 55);
            labelEmail.Name = "labelEmail";
            labelEmail.Size = new System.Drawing.Size(32, 13);
            labelEmail.TabIndex = 0;
            labelEmail.Text = "Email";
            // 
            // cPFLabel
            // 
            cPFLabel.AutoSize = true;
            cPFLabel.Location = new System.Drawing.Point(110, 10);
            cPFLabel.Name = "cPFLabel";
            cPFLabel.Size = new System.Drawing.Size(27, 13);
            cPFLabel.TabIndex = 0;
            cPFLabel.Text = "CPF";
            // 
            // labelCep
            // 
            this.labelCep.AutoSize = true;
            this.labelCep.Location = new System.Drawing.Point(10, 10);
            this.labelCep.Name = "labelCep";
            this.labelCep.Size = new System.Drawing.Size(28, 13);
            this.labelCep.TabIndex = 50;
            this.labelCep.Text = "CEP";
            // 
            // labelCadastrarFuncionario
            // 
            this.labelCadastrarFuncionario.AutoSize = true;
            this.labelCadastrarFuncionario.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCadastrarFuncionario.Location = new System.Drawing.Point(217, 9);
            this.labelCadastrarFuncionario.Name = "labelCadastrarFuncionario";
            this.labelCadastrarFuncionario.Size = new System.Drawing.Size(309, 31);
            this.labelCadastrarFuncionario.TabIndex = 13;
            this.labelCadastrarFuncionario.Text = "Cadastro Funcionários";
            this.labelCadastrarFuncionario.Visible = false;
            // 
            // pnlConsultar
            // 
            this.pnlConsultar.BackColor = System.Drawing.SystemColors.Control;
            this.pnlConsultar.Controls.Add(this.tbLike);
            this.pnlConsultar.Controls.Add(this.labelLike);
            this.pnlConsultar.Controls.Add(this.label8);
            this.pnlConsultar.Controls.Add(this.pictureBox1);
            this.pnlConsultar.Controls.Add(this.label70);
            this.pnlConsultar.Controls.Add(this.lblTotalOS);
            this.pnlConsultar.Controls.Add(this.dgvFuncionario);
            this.pnlConsultar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnlConsultar.Location = new System.Drawing.Point(934, 121);
            this.pnlConsultar.Name = "pnlConsultar";
            this.pnlConsultar.Size = new System.Drawing.Size(717, 350);
            this.pnlConsultar.TabIndex = 14;
            // 
            // tbLike
            // 
            this.tbLike.Location = new System.Drawing.Point(6, 10);
            this.tbLike.Name = "tbLike";
            this.tbLike.Size = new System.Drawing.Size(100, 20);
            this.tbLike.TabIndex = 22;
            this.tbLike.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbLike_KeyDown);
            // 
            // labelLike
            // 
            this.labelLike.AutoSize = true;
            this.labelLike.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelLike.Location = new System.Drawing.Point(284, 169);
            this.labelLike.Name = "labelLike";
            this.labelLike.Size = new System.Drawing.Size(264, 13);
            this.labelLike.TabIndex = 19;
            this.labelLike.Text = "Filtro LIKE: %Nome%  - Acione <ENTER> para acionar";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(3, 181);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(331, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Clique em \'S\' ou dois cliques do mouse para selecionar o Funcionário";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::osExpress.Properties.Resources.search;
            this.pictureBox1.Location = new System.Drawing.Point(336, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(51, 50);
            this.pictureBox1.TabIndex = 16;
            this.pictureBox1.TabStop = false;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.Location = new System.Drawing.Point(385, 15);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(122, 31);
            this.label70.TabIndex = 15;
            this.label70.Text = "Consulta";
            // 
            // lblTotalOS
            // 
            this.lblTotalOS.AutoSize = true;
            this.lblTotalOS.Location = new System.Drawing.Point(3, 487);
            this.lblTotalOS.Name = "lblTotalOS";
            this.lblTotalOS.Size = new System.Drawing.Size(37, 13);
            this.lblTotalOS.TabIndex = 13;
            this.lblTotalOS.Text = "Total: ";
            this.lblTotalOS.Visible = false;
            // 
            // dgvFuncionario
            // 
            this.dgvFuncionario.AllowUserToAddRows = false;
            this.dgvFuncionario.AllowUserToDeleteRows = false;
            this.dgvFuncionario.AutoGenerateColumns = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvFuncionario.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvFuncionario.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFuncionario.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CNPJ,
            this.postoDataGridViewTextBoxColumn,
            this.funcionarioDataGridViewTextBoxColumn,
            this.nomeDataGridViewTextBoxColumn,
            this.turnoDataGridViewTextBoxColumn,
            this.tipoPessoaDataGridViewTextBoxColumn,
            this.cgcDataGridViewTextBoxColumn,
            this.inscricaoDataGridViewTextBoxColumn,
            this.enderecoDataGridViewTextBoxColumn,
            this.bairroDataGridViewTextBoxColumn,
            this.cidadeDataGridViewTextBoxColumn,
            this.estadoDataGridViewTextBoxColumn,
            this.cepDataGridViewTextBoxColumn,
            this.obs01DataGridViewTextBoxColumn,
            this.obs02DataGridViewTextBoxColumn,
            this.obs03DataGridViewTextBoxColumn,
            this.obs04DataGridViewTextBoxColumn,
            this.obs05DataGridViewTextBoxColumn,
            this.obs06DataGridViewTextBoxColumn,
            this.obs07DataGridViewTextBoxColumn,
            this.obs08DataGridViewTextBoxColumn,
            this.obs09DataGridViewTextBoxColumn,
            this.obs10DataGridViewTextBoxColumn,
            this.noturnoDataGridViewTextBoxColumn,
            this.iDDataGridViewTextBoxColumn,
            this.inativoDataGridViewTextBoxColumn,
            this.dtAlterDataGridViewTextBoxColumn,
            this.obs11DataGridViewTextBoxColumn,
            this.obs12DataGridViewTextBoxColumn,
            this.obs13DataGridViewTextBoxColumn,
            this.obs14DataGridViewTextBoxColumn,
            this.obs15DataGridViewTextBoxColumn,
            this.obs16DataGridViewTextBoxColumn,
            this.obs17DataGridViewTextBoxColumn,
            this.obs18DataGridViewTextBoxColumn,
            this.obs19DataGridViewTextBoxColumn,
            this.obs20DataGridViewTextBoxColumn,
            this.rFIDDataGridViewTextBoxColumn,
            this.biometriaDataGridViewTextBoxColumn,
            this.cTACaixaDataGridViewTextBoxColumn,
            this.tipoVIPDataGridViewTextBoxColumn,
            this.uNomeDataGridViewTextBoxColumn,
            this.uSenhaDataGridViewTextBoxColumn,
            this.tipoacessoDataGridViewTextBoxColumn,
            this.dtcadastroDataGridViewTextBoxColumn,
            this.ultimoacessoDataGridViewTextBoxColumn,
            this.planocontaDataGridViewTextBoxColumn,
            this.acessapedidoDataGridViewCheckBoxColumn,
            this.digitaselolacreDataGridViewCheckBoxColumn,
            this.idusuarioDataGridViewTextBoxColumn,
            this.isusuarioDataGridViewTextBoxColumn});
            this.dgvFuncionario.DataSource = this.bsXOSFuncionario;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvFuncionario.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvFuncionario.Location = new System.Drawing.Point(6, 200);
            this.dgvFuncionario.Name = "dgvFuncionario";
            this.dgvFuncionario.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvFuncionario.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvFuncionario.Size = new System.Drawing.Size(700, 199);
            this.dgvFuncionario.TabIndex = 11;
            this.dgvFuncionario.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFuncionario_CellDoubleClick);
            this.dgvFuncionario.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvFuncionario_KeyDown);
            // 
            // CNPJ
            // 
            this.CNPJ.DataPropertyName = "CNPJ";
            this.CNPJ.HeaderText = "CNPJ";
            this.CNPJ.Name = "CNPJ";
            this.CNPJ.ReadOnly = true;
            // 
            // postoDataGridViewTextBoxColumn
            // 
            this.postoDataGridViewTextBoxColumn.DataPropertyName = "Posto";
            this.postoDataGridViewTextBoxColumn.HeaderText = "Posto";
            this.postoDataGridViewTextBoxColumn.Name = "postoDataGridViewTextBoxColumn";
            this.postoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // funcionarioDataGridViewTextBoxColumn
            // 
            this.funcionarioDataGridViewTextBoxColumn.DataPropertyName = "Funcionario";
            this.funcionarioDataGridViewTextBoxColumn.HeaderText = "Funcionario";
            this.funcionarioDataGridViewTextBoxColumn.Name = "funcionarioDataGridViewTextBoxColumn";
            this.funcionarioDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nomeDataGridViewTextBoxColumn
            // 
            this.nomeDataGridViewTextBoxColumn.DataPropertyName = "Nome";
            this.nomeDataGridViewTextBoxColumn.HeaderText = "Nome";
            this.nomeDataGridViewTextBoxColumn.Name = "nomeDataGridViewTextBoxColumn";
            this.nomeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // turnoDataGridViewTextBoxColumn
            // 
            this.turnoDataGridViewTextBoxColumn.DataPropertyName = "Turno";
            this.turnoDataGridViewTextBoxColumn.HeaderText = "Turno";
            this.turnoDataGridViewTextBoxColumn.Name = "turnoDataGridViewTextBoxColumn";
            this.turnoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tipoPessoaDataGridViewTextBoxColumn
            // 
            this.tipoPessoaDataGridViewTextBoxColumn.DataPropertyName = "Tipo_Pessoa";
            this.tipoPessoaDataGridViewTextBoxColumn.HeaderText = "Tipo_Pessoa";
            this.tipoPessoaDataGridViewTextBoxColumn.Name = "tipoPessoaDataGridViewTextBoxColumn";
            this.tipoPessoaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cgcDataGridViewTextBoxColumn
            // 
            this.cgcDataGridViewTextBoxColumn.DataPropertyName = "Cgc";
            this.cgcDataGridViewTextBoxColumn.HeaderText = "Cgc";
            this.cgcDataGridViewTextBoxColumn.Name = "cgcDataGridViewTextBoxColumn";
            this.cgcDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // inscricaoDataGridViewTextBoxColumn
            // 
            this.inscricaoDataGridViewTextBoxColumn.DataPropertyName = "Inscricao";
            this.inscricaoDataGridViewTextBoxColumn.HeaderText = "Inscricao";
            this.inscricaoDataGridViewTextBoxColumn.Name = "inscricaoDataGridViewTextBoxColumn";
            this.inscricaoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // enderecoDataGridViewTextBoxColumn
            // 
            this.enderecoDataGridViewTextBoxColumn.DataPropertyName = "Endereco";
            this.enderecoDataGridViewTextBoxColumn.HeaderText = "Endereco";
            this.enderecoDataGridViewTextBoxColumn.Name = "enderecoDataGridViewTextBoxColumn";
            this.enderecoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // bairroDataGridViewTextBoxColumn
            // 
            this.bairroDataGridViewTextBoxColumn.DataPropertyName = "Bairro";
            this.bairroDataGridViewTextBoxColumn.HeaderText = "Bairro";
            this.bairroDataGridViewTextBoxColumn.Name = "bairroDataGridViewTextBoxColumn";
            this.bairroDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cidadeDataGridViewTextBoxColumn
            // 
            this.cidadeDataGridViewTextBoxColumn.DataPropertyName = "Cidade";
            this.cidadeDataGridViewTextBoxColumn.HeaderText = "Cidade";
            this.cidadeDataGridViewTextBoxColumn.Name = "cidadeDataGridViewTextBoxColumn";
            this.cidadeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // estadoDataGridViewTextBoxColumn
            // 
            this.estadoDataGridViewTextBoxColumn.DataPropertyName = "Estado";
            this.estadoDataGridViewTextBoxColumn.HeaderText = "Estado";
            this.estadoDataGridViewTextBoxColumn.Name = "estadoDataGridViewTextBoxColumn";
            this.estadoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cepDataGridViewTextBoxColumn
            // 
            this.cepDataGridViewTextBoxColumn.DataPropertyName = "Cep";
            this.cepDataGridViewTextBoxColumn.HeaderText = "Cep";
            this.cepDataGridViewTextBoxColumn.Name = "cepDataGridViewTextBoxColumn";
            this.cepDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs01DataGridViewTextBoxColumn
            // 
            this.obs01DataGridViewTextBoxColumn.DataPropertyName = "obs01";
            this.obs01DataGridViewTextBoxColumn.HeaderText = "obs01";
            this.obs01DataGridViewTextBoxColumn.Name = "obs01DataGridViewTextBoxColumn";
            this.obs01DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs02DataGridViewTextBoxColumn
            // 
            this.obs02DataGridViewTextBoxColumn.DataPropertyName = "obs02";
            this.obs02DataGridViewTextBoxColumn.HeaderText = "obs02";
            this.obs02DataGridViewTextBoxColumn.Name = "obs02DataGridViewTextBoxColumn";
            this.obs02DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs03DataGridViewTextBoxColumn
            // 
            this.obs03DataGridViewTextBoxColumn.DataPropertyName = "obs03";
            this.obs03DataGridViewTextBoxColumn.HeaderText = "obs03";
            this.obs03DataGridViewTextBoxColumn.Name = "obs03DataGridViewTextBoxColumn";
            this.obs03DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs04DataGridViewTextBoxColumn
            // 
            this.obs04DataGridViewTextBoxColumn.DataPropertyName = "obs04";
            this.obs04DataGridViewTextBoxColumn.HeaderText = "obs04";
            this.obs04DataGridViewTextBoxColumn.Name = "obs04DataGridViewTextBoxColumn";
            this.obs04DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs05DataGridViewTextBoxColumn
            // 
            this.obs05DataGridViewTextBoxColumn.DataPropertyName = "obs05";
            this.obs05DataGridViewTextBoxColumn.HeaderText = "obs05";
            this.obs05DataGridViewTextBoxColumn.Name = "obs05DataGridViewTextBoxColumn";
            this.obs05DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs06DataGridViewTextBoxColumn
            // 
            this.obs06DataGridViewTextBoxColumn.DataPropertyName = "obs06";
            this.obs06DataGridViewTextBoxColumn.HeaderText = "obs06";
            this.obs06DataGridViewTextBoxColumn.Name = "obs06DataGridViewTextBoxColumn";
            this.obs06DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs07DataGridViewTextBoxColumn
            // 
            this.obs07DataGridViewTextBoxColumn.DataPropertyName = "obs07";
            this.obs07DataGridViewTextBoxColumn.HeaderText = "obs07";
            this.obs07DataGridViewTextBoxColumn.Name = "obs07DataGridViewTextBoxColumn";
            this.obs07DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs08DataGridViewTextBoxColumn
            // 
            this.obs08DataGridViewTextBoxColumn.DataPropertyName = "obs08";
            this.obs08DataGridViewTextBoxColumn.HeaderText = "obs08";
            this.obs08DataGridViewTextBoxColumn.Name = "obs08DataGridViewTextBoxColumn";
            this.obs08DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs09DataGridViewTextBoxColumn
            // 
            this.obs09DataGridViewTextBoxColumn.DataPropertyName = "obs09";
            this.obs09DataGridViewTextBoxColumn.HeaderText = "obs09";
            this.obs09DataGridViewTextBoxColumn.Name = "obs09DataGridViewTextBoxColumn";
            this.obs09DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs10DataGridViewTextBoxColumn
            // 
            this.obs10DataGridViewTextBoxColumn.DataPropertyName = "obs10";
            this.obs10DataGridViewTextBoxColumn.HeaderText = "obs10";
            this.obs10DataGridViewTextBoxColumn.Name = "obs10DataGridViewTextBoxColumn";
            this.obs10DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // noturnoDataGridViewTextBoxColumn
            // 
            this.noturnoDataGridViewTextBoxColumn.DataPropertyName = "Noturno";
            this.noturnoDataGridViewTextBoxColumn.HeaderText = "Noturno";
            this.noturnoDataGridViewTextBoxColumn.Name = "noturnoDataGridViewTextBoxColumn";
            this.noturnoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // inativoDataGridViewTextBoxColumn
            // 
            this.inativoDataGridViewTextBoxColumn.DataPropertyName = "Inativo";
            this.inativoDataGridViewTextBoxColumn.HeaderText = "Inativo";
            this.inativoDataGridViewTextBoxColumn.Name = "inativoDataGridViewTextBoxColumn";
            this.inativoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dtAlterDataGridViewTextBoxColumn
            // 
            this.dtAlterDataGridViewTextBoxColumn.DataPropertyName = "Dt_Alter";
            this.dtAlterDataGridViewTextBoxColumn.HeaderText = "Dt_Alter";
            this.dtAlterDataGridViewTextBoxColumn.Name = "dtAlterDataGridViewTextBoxColumn";
            this.dtAlterDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs11DataGridViewTextBoxColumn
            // 
            this.obs11DataGridViewTextBoxColumn.DataPropertyName = "Obs11";
            this.obs11DataGridViewTextBoxColumn.HeaderText = "Obs11";
            this.obs11DataGridViewTextBoxColumn.Name = "obs11DataGridViewTextBoxColumn";
            this.obs11DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs12DataGridViewTextBoxColumn
            // 
            this.obs12DataGridViewTextBoxColumn.DataPropertyName = "Obs12";
            this.obs12DataGridViewTextBoxColumn.HeaderText = "Obs12";
            this.obs12DataGridViewTextBoxColumn.Name = "obs12DataGridViewTextBoxColumn";
            this.obs12DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs13DataGridViewTextBoxColumn
            // 
            this.obs13DataGridViewTextBoxColumn.DataPropertyName = "Obs13";
            this.obs13DataGridViewTextBoxColumn.HeaderText = "Obs13";
            this.obs13DataGridViewTextBoxColumn.Name = "obs13DataGridViewTextBoxColumn";
            this.obs13DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs14DataGridViewTextBoxColumn
            // 
            this.obs14DataGridViewTextBoxColumn.DataPropertyName = "Obs14";
            this.obs14DataGridViewTextBoxColumn.HeaderText = "Obs14";
            this.obs14DataGridViewTextBoxColumn.Name = "obs14DataGridViewTextBoxColumn";
            this.obs14DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs15DataGridViewTextBoxColumn
            // 
            this.obs15DataGridViewTextBoxColumn.DataPropertyName = "Obs15";
            this.obs15DataGridViewTextBoxColumn.HeaderText = "Obs15";
            this.obs15DataGridViewTextBoxColumn.Name = "obs15DataGridViewTextBoxColumn";
            this.obs15DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs16DataGridViewTextBoxColumn
            // 
            this.obs16DataGridViewTextBoxColumn.DataPropertyName = "Obs16";
            this.obs16DataGridViewTextBoxColumn.HeaderText = "Obs16";
            this.obs16DataGridViewTextBoxColumn.Name = "obs16DataGridViewTextBoxColumn";
            this.obs16DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs17DataGridViewTextBoxColumn
            // 
            this.obs17DataGridViewTextBoxColumn.DataPropertyName = "Obs17";
            this.obs17DataGridViewTextBoxColumn.HeaderText = "Obs17";
            this.obs17DataGridViewTextBoxColumn.Name = "obs17DataGridViewTextBoxColumn";
            this.obs17DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs18DataGridViewTextBoxColumn
            // 
            this.obs18DataGridViewTextBoxColumn.DataPropertyName = "Obs18";
            this.obs18DataGridViewTextBoxColumn.HeaderText = "Obs18";
            this.obs18DataGridViewTextBoxColumn.Name = "obs18DataGridViewTextBoxColumn";
            this.obs18DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs19DataGridViewTextBoxColumn
            // 
            this.obs19DataGridViewTextBoxColumn.DataPropertyName = "Obs19";
            this.obs19DataGridViewTextBoxColumn.HeaderText = "Obs19";
            this.obs19DataGridViewTextBoxColumn.Name = "obs19DataGridViewTextBoxColumn";
            this.obs19DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obs20DataGridViewTextBoxColumn
            // 
            this.obs20DataGridViewTextBoxColumn.DataPropertyName = "Obs20";
            this.obs20DataGridViewTextBoxColumn.HeaderText = "Obs20";
            this.obs20DataGridViewTextBoxColumn.Name = "obs20DataGridViewTextBoxColumn";
            this.obs20DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // rFIDDataGridViewTextBoxColumn
            // 
            this.rFIDDataGridViewTextBoxColumn.DataPropertyName = "RFID";
            this.rFIDDataGridViewTextBoxColumn.HeaderText = "RFID";
            this.rFIDDataGridViewTextBoxColumn.Name = "rFIDDataGridViewTextBoxColumn";
            this.rFIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // biometriaDataGridViewTextBoxColumn
            // 
            this.biometriaDataGridViewTextBoxColumn.DataPropertyName = "Biometria";
            this.biometriaDataGridViewTextBoxColumn.HeaderText = "Biometria";
            this.biometriaDataGridViewTextBoxColumn.Name = "biometriaDataGridViewTextBoxColumn";
            this.biometriaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cTACaixaDataGridViewTextBoxColumn
            // 
            this.cTACaixaDataGridViewTextBoxColumn.DataPropertyName = "CTA_Caixa";
            this.cTACaixaDataGridViewTextBoxColumn.HeaderText = "CTA_Caixa";
            this.cTACaixaDataGridViewTextBoxColumn.Name = "cTACaixaDataGridViewTextBoxColumn";
            this.cTACaixaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tipoVIPDataGridViewTextBoxColumn
            // 
            this.tipoVIPDataGridViewTextBoxColumn.DataPropertyName = "Tipo_VIP";
            this.tipoVIPDataGridViewTextBoxColumn.HeaderText = "Tipo_VIP";
            this.tipoVIPDataGridViewTextBoxColumn.Name = "tipoVIPDataGridViewTextBoxColumn";
            this.tipoVIPDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // uNomeDataGridViewTextBoxColumn
            // 
            this.uNomeDataGridViewTextBoxColumn.DataPropertyName = "uNome";
            this.uNomeDataGridViewTextBoxColumn.HeaderText = "uNome";
            this.uNomeDataGridViewTextBoxColumn.Name = "uNomeDataGridViewTextBoxColumn";
            this.uNomeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // uSenhaDataGridViewTextBoxColumn
            // 
            this.uSenhaDataGridViewTextBoxColumn.DataPropertyName = "uSenha";
            this.uSenhaDataGridViewTextBoxColumn.HeaderText = "uSenha";
            this.uSenhaDataGridViewTextBoxColumn.Name = "uSenhaDataGridViewTextBoxColumn";
            this.uSenhaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tipoacessoDataGridViewTextBoxColumn
            // 
            this.tipoacessoDataGridViewTextBoxColumn.DataPropertyName = "tipo_acesso";
            this.tipoacessoDataGridViewTextBoxColumn.HeaderText = "tipo_acesso";
            this.tipoacessoDataGridViewTextBoxColumn.Name = "tipoacessoDataGridViewTextBoxColumn";
            this.tipoacessoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dtcadastroDataGridViewTextBoxColumn
            // 
            this.dtcadastroDataGridViewTextBoxColumn.DataPropertyName = "dt_cadastro";
            this.dtcadastroDataGridViewTextBoxColumn.HeaderText = "dt_cadastro";
            this.dtcadastroDataGridViewTextBoxColumn.Name = "dtcadastroDataGridViewTextBoxColumn";
            this.dtcadastroDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // ultimoacessoDataGridViewTextBoxColumn
            // 
            this.ultimoacessoDataGridViewTextBoxColumn.DataPropertyName = "ultimo_acesso";
            this.ultimoacessoDataGridViewTextBoxColumn.HeaderText = "ultimo_acesso";
            this.ultimoacessoDataGridViewTextBoxColumn.Name = "ultimoacessoDataGridViewTextBoxColumn";
            this.ultimoacessoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // planocontaDataGridViewTextBoxColumn
            // 
            this.planocontaDataGridViewTextBoxColumn.DataPropertyName = "plano_conta";
            this.planocontaDataGridViewTextBoxColumn.HeaderText = "plano_conta";
            this.planocontaDataGridViewTextBoxColumn.Name = "planocontaDataGridViewTextBoxColumn";
            this.planocontaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // acessapedidoDataGridViewCheckBoxColumn
            // 
            this.acessapedidoDataGridViewCheckBoxColumn.DataPropertyName = "acessa_pedido";
            this.acessapedidoDataGridViewCheckBoxColumn.HeaderText = "acessa_pedido";
            this.acessapedidoDataGridViewCheckBoxColumn.Name = "acessapedidoDataGridViewCheckBoxColumn";
            this.acessapedidoDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // digitaselolacreDataGridViewCheckBoxColumn
            // 
            this.digitaselolacreDataGridViewCheckBoxColumn.DataPropertyName = "digita_selo_lacre";
            this.digitaselolacreDataGridViewCheckBoxColumn.HeaderText = "digita_selo_lacre";
            this.digitaselolacreDataGridViewCheckBoxColumn.Name = "digitaselolacreDataGridViewCheckBoxColumn";
            this.digitaselolacreDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // idusuarioDataGridViewTextBoxColumn
            // 
            this.idusuarioDataGridViewTextBoxColumn.DataPropertyName = "idusuario";
            this.idusuarioDataGridViewTextBoxColumn.HeaderText = "idusuario";
            this.idusuarioDataGridViewTextBoxColumn.Name = "idusuarioDataGridViewTextBoxColumn";
            this.idusuarioDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // isusuarioDataGridViewTextBoxColumn
            // 
            this.isusuarioDataGridViewTextBoxColumn.DataPropertyName = "isusuario";
            this.isusuarioDataGridViewTextBoxColumn.HeaderText = "isusuario";
            this.isusuarioDataGridViewTextBoxColumn.Name = "isusuarioDataGridViewTextBoxColumn";
            this.isusuarioDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // bsXOSFuncionario
            // 
            this.bsXOSFuncionario.DataMember = "XOsFuncionario";
            this.bsXOSFuncionario.DataSource = this.dtsXOSFuncionario;
            // 
            // dtsXOSFuncionario
            // 
            this.dtsXOSFuncionario.DataSetName = "dtsXOSFuncionario";
            this.dtsXOSFuncionario.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // xOsFuncionarioTableAdapter
            // 
            this.xOsFuncionarioTableAdapter.ClearBeforeFill = true;
            // 
            // FuncionarioBindingNavigator
            // 
            this.FuncionarioBindingNavigator.AddNewItem = this.bnInserir;
            this.FuncionarioBindingNavigator.BindingSource = this.bsXOSFuncionario;
            this.FuncionarioBindingNavigator.CountItem = this.bnCount;
            this.FuncionarioBindingNavigator.CountItemFormat = "de {0}";
            this.FuncionarioBindingNavigator.DeleteItem = null;
            this.FuncionarioBindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.FuncionarioBindingNavigator.ImageScalingSize = new System.Drawing.Size(48, 48);
            this.FuncionarioBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bnConsultar,
            this.toolStripSeparator3,
            this.bnInserir,
            this.bnGravar,
            this.bnCancelar,
            this.toolStripSeparator4,
            this.bnPrimeiro,
            this.bnAnterior,
            this.toolStripSeparator1,
            this.bnPosition,
            this.bnCount,
            this.toolStripSeparator2,
            this.bnProximo,
            this.bnUltimo,
            this.bnDescartar});
            this.FuncionarioBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.FuncionarioBindingNavigator.MoveFirstItem = this.bnPrimeiro;
            this.FuncionarioBindingNavigator.MoveLastItem = this.bnUltimo;
            this.FuncionarioBindingNavigator.MoveNextItem = this.bnProximo;
            this.FuncionarioBindingNavigator.MovePreviousItem = this.bnAnterior;
            this.FuncionarioBindingNavigator.Name = "FuncionarioBindingNavigator";
            this.FuncionarioBindingNavigator.PositionItem = this.bnPosition;
            this.FuncionarioBindingNavigator.Size = new System.Drawing.Size(1184, 70);
            this.FuncionarioBindingNavigator.TabIndex = 15;
            this.FuncionarioBindingNavigator.Text = "FuncionarioBindingNavigator";
            // 
            // bnInserir
            // 
            this.bnInserir.Image = global::osExpress.Properties.Resources.insert;
            this.bnInserir.Name = "bnInserir";
            this.bnInserir.RightToLeftAutoMirrorImage = true;
            this.bnInserir.Size = new System.Drawing.Size(52, 67);
            this.bnInserir.Text = "&Inserir";
            this.bnInserir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnInserir.Click += new System.EventHandler(this.bnInserir_Click);
            // 
            // bnCount
            // 
            this.bnCount.Name = "bnCount";
            this.bnCount.Size = new System.Drawing.Size(37, 67);
            this.bnCount.Text = "de {0}";
            this.bnCount.ToolTipText = "Total number of items";
            // 
            // bnConsultar
            // 
            this.bnConsultar.Image = global::osExpress.Properties.Resources.search;
            this.bnConsultar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnConsultar.Name = "bnConsultar";
            this.bnConsultar.Size = new System.Drawing.Size(62, 67);
            this.bnConsultar.Text = "Consultar";
            this.bnConsultar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnConsultar.Click += new System.EventHandler(this.bnConsultar_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 70);
            // 
            // bnGravar
            // 
            this.bnGravar.Image = global::osExpress.Properties.Resources.save;
            this.bnGravar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnGravar.Name = "bnGravar";
            this.bnGravar.Size = new System.Drawing.Size(52, 67);
            this.bnGravar.Text = "Grav&ar";
            this.bnGravar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnGravar.Click += new System.EventHandler(this.bnGravar_Click);
            // 
            // bnCancelar
            // 
            this.bnCancelar.Enabled = false;
            this.bnCancelar.Image = global::osExpress.Properties.Resources.clean;
            this.bnCancelar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnCancelar.Name = "bnCancelar";
            this.bnCancelar.Size = new System.Drawing.Size(57, 67);
            this.bnCancelar.Text = "Cancelar";
            this.bnCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnCancelar.Click += new System.EventHandler(this.bnCancelar_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 70);
            // 
            // bnPrimeiro
            // 
            this.bnPrimeiro.Image = global::osExpress.Properties.Resources.first;
            this.bnPrimeiro.Name = "bnPrimeiro";
            this.bnPrimeiro.RightToLeftAutoMirrorImage = true;
            this.bnPrimeiro.Size = new System.Drawing.Size(56, 67);
            this.bnPrimeiro.Text = "&Primeiro";
            this.bnPrimeiro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // bnAnterior
            // 
            this.bnAnterior.Image = global::osExpress.Properties.Resources.previous;
            this.bnAnterior.Name = "bnAnterior";
            this.bnAnterior.RightToLeftAutoMirrorImage = true;
            this.bnAnterior.Size = new System.Drawing.Size(54, 67);
            this.bnAnterior.Text = "Anteri&or";
            this.bnAnterior.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 70);
            // 
            // bnPosition
            // 
            this.bnPosition.AccessibleName = "Position";
            this.bnPosition.AutoSize = false;
            this.bnPosition.Name = "bnPosition";
            this.bnPosition.Size = new System.Drawing.Size(50, 23);
            this.bnPosition.Text = "0";
            this.bnPosition.ToolTipText = "Current position";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 70);
            // 
            // bnProximo
            // 
            this.bnProximo.Image = global::osExpress.Properties.Resources.next;
            this.bnProximo.Name = "bnProximo";
            this.bnProximo.RightToLeftAutoMirrorImage = true;
            this.bnProximo.Size = new System.Drawing.Size(56, 67);
            this.bnProximo.Text = "Próxi&mo";
            this.bnProximo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // bnUltimo
            // 
            this.bnUltimo.Image = global::osExpress.Properties.Resources.last;
            this.bnUltimo.Name = "bnUltimo";
            this.bnUltimo.RightToLeftAutoMirrorImage = true;
            this.bnUltimo.Size = new System.Drawing.Size(52, 67);
            this.bnUltimo.Text = "&Último";
            this.bnUltimo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // bnDescartar
            // 
            this.bnDescartar.Enabled = false;
            this.bnDescartar.Image = global::osExpress.Properties.Resources.remove;
            this.bnDescartar.Name = "bnDescartar";
            this.bnDescartar.RightToLeftAutoMirrorImage = true;
            this.bnDescartar.Size = new System.Drawing.Size(60, 67);
            this.bnDescartar.Text = "&Descartar";
            this.bnDescartar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bnDescartar.Click += new System.EventHandler(this.bnDescartar_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.AutoScroll = true;
            this.tabPage1.Controls.Add(this.labelSenha);
            this.tabPage1.Controls.Add(this.labelLogin);
            this.tabPage1.Controls.Add(this.tbSenha);
            this.tabPage1.Controls.Add(this.tbLogin);
            this.tabPage1.Controls.Add(this.checkboxIsUsuario);
            this.tabPage1.Controls.Add(label_rg);
            this.tabPage1.Controls.Add(this.tb_rg);
            this.tabPage1.Controls.Add(this.tabChild);
            this.tabPage1.Controls.Add(labelNome);
            this.tabPage1.Controls.Add(this.tbNome);
            this.tabPage1.Controls.Add(nOME_FANTASIALabel);
            this.tabPage1.Controls.Add(this.nOME_FANTASIATextBox);
            this.tabPage1.Controls.Add(this.mtbCPF);
            this.tabPage1.Controls.Add(this.mtbCNPJ);
            this.tabPage1.Controls.Add(this.comboBox2);
            this.tabPage1.Controls.Add(tIPO_PESSOALabel);
            this.tabPage1.Controls.Add(iDLabel);
            this.tabPage1.Controls.Add(this.tbIdusuario);
            this.tabPage1.Controls.Add(cNPJLabel);
            this.tabPage1.Controls.Add(labelEmail);
            this.tabPage1.Controls.Add(this.tb_email);
            this.tabPage1.Controls.Add(cPFLabel);
            this.tabPage1.Controls.Add(this.cPFTextBox);
            this.tabPage1.Controls.Add(this.cNPJTextBox);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(952, 554);
            this.tabPage1.TabIndex = 1;
            this.tabPage1.Text = " Dados";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // labelSenha
            // 
            this.labelSenha.AutoSize = true;
            this.labelSenha.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSenha.Location = new System.Drawing.Point(732, 139);
            this.labelSenha.Name = "labelSenha";
            this.labelSenha.Size = new System.Drawing.Size(47, 13);
            this.labelSenha.TabIndex = 61;
            this.labelSenha.Text = "Senha:";
            // 
            // labelLogin
            // 
            this.labelLogin.AutoSize = true;
            this.labelLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLogin.Location = new System.Drawing.Point(732, 98);
            this.labelLogin.Name = "labelLogin";
            this.labelLogin.Size = new System.Drawing.Size(42, 13);
            this.labelLogin.TabIndex = 60;
            this.labelLogin.Text = "Login:";
            // 
            // tbSenha
            // 
            this.tbSenha.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsXOSFuncionario, "uSenha", true));
            this.tbSenha.Location = new System.Drawing.Point(732, 155);
            this.tbSenha.Name = "tbSenha";
            this.tbSenha.Size = new System.Drawing.Size(142, 20);
            this.tbSenha.TabIndex = 59;
            // 
            // tbLogin
            // 
            this.tbLogin.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsXOSFuncionario, "uNome", true));
            this.tbLogin.Location = new System.Drawing.Point(732, 114);
            this.tbLogin.Name = "tbLogin";
            this.tbLogin.Size = new System.Drawing.Size(142, 20);
            this.tbLogin.TabIndex = 58;
            // 
            // checkboxIsUsuario
            // 
            this.checkboxIsUsuario.AutoSize = true;
            this.checkboxIsUsuario.Checked = true;
            this.checkboxIsUsuario.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkboxIsUsuario.Location = new System.Drawing.Point(732, 70);
            this.checkboxIsUsuario.Name = "checkboxIsUsuario";
            this.checkboxIsUsuario.Size = new System.Drawing.Size(78, 17);
            this.checkboxIsUsuario.TabIndex = 57;
            this.checkboxIsUsuario.Text = "É Usuário?";
            this.checkboxIsUsuario.UseVisualStyleBackColor = true;
            this.checkboxIsUsuario.CheckedChanged += new System.EventHandler(this.checkboxIsUsuario_CheckedChanged);
            // 
            // tb_rg
            // 
            this.tb_rg.Location = new System.Drawing.Point(360, 25);
            this.tb_rg.MaxLength = 50;
            this.tb_rg.Name = "tb_rg";
            this.tb_rg.Size = new System.Drawing.Size(320, 20);
            this.tb_rg.TabIndex = 53;
            this.tb_rg.Text = " ";
            // 
            // tabChild
            // 
            this.tabChild.Controls.Add(this.tabEndereco);
            this.tabChild.Controls.Add(this.tabContatos);
            this.tabChild.Controls.Add(this.tabOutros);
            this.tabChild.Controls.Add(this.tabObservacoes);
            this.tabChild.Controls.Add(this.tabEventos);
            this.tabChild.Controls.Add(this.tabOrcamentos);
            this.tabChild.Controls.Add(this.tabPedidos);
            this.tabChild.Controls.Add(this.tabServicos);
            this.tabChild.Controls.Add(this.tabInstrumentos);
            this.tabChild.Location = new System.Drawing.Point(10, 207);
            this.tabChild.Name = "tabChild";
            this.tabChild.SelectedIndex = 0;
            this.tabChild.Size = new System.Drawing.Size(980, 248);
            this.tabChild.TabIndex = 51;
            // 
            // tabEndereco
            // 
            this.tabEndereco.Controls.Add(this.tELEFONETextBox);
            this.tabEndereco.Controls.Add(fAXLabel);
            this.tabEndereco.Controls.Add(this.fAXTextBox);
            this.tabEndereco.Controls.Add(this.mtbTelefone);
            this.tabEndereco.Controls.Add(tELEFONELabel);
            this.tabEndereco.Controls.Add(bAIRROLabel);
            this.tabEndereco.Controls.Add(this.tbBairro);
            this.tabEndereco.Controls.Add(eNDERECOLabel);
            this.tabEndereco.Controls.Add(this.tbEndereco);
            this.tabEndereco.Controls.Add(cIDADELabel);
            this.tabEndereco.Controls.Add(this.tbCidade);
            this.tabEndereco.Controls.Add(this.comboBox1);
            this.tabEndereco.Controls.Add(uFLabel);
            this.tabEndereco.Controls.Add(this.labelCep);
            this.tabEndereco.Controls.Add(this.tbCep);
            this.tabEndereco.Location = new System.Drawing.Point(4, 22);
            this.tabEndereco.Name = "tabEndereco";
            this.tabEndereco.Padding = new System.Windows.Forms.Padding(3);
            this.tabEndereco.Size = new System.Drawing.Size(972, 222);
            this.tabEndereco.TabIndex = 0;
            this.tabEndereco.Text = "Endereço";
            this.tabEndereco.UseVisualStyleBackColor = true;
            // 
            // tELEFONETextBox
            // 
            this.tELEFONETextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsXOSFuncionario, "obs02", true));
            this.tELEFONETextBox.Location = new System.Drawing.Point(6, 167);
            this.tELEFONETextBox.MaxLength = 20;
            this.tELEFONETextBox.Name = "tELEFONETextBox";
            this.tELEFONETextBox.Size = new System.Drawing.Size(220, 20);
            this.tELEFONETextBox.TabIndex = 64;
            this.tELEFONETextBox.TabStop = false;
            // 
            // fAXTextBox
            // 
            this.fAXTextBox.Location = new System.Drawing.Point(300, 110);
            this.fAXTextBox.MaxLength = 20;
            this.fAXTextBox.Name = "fAXTextBox";
            this.fAXTextBox.Size = new System.Drawing.Size(243, 20);
            this.fAXTextBox.TabIndex = 65;
            this.fAXTextBox.Visible = false;
            // 
            // mtbTelefone
            // 
            this.mtbTelefone.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsXOSFuncionario, "obs02", true));
            this.mtbTelefone.Location = new System.Drawing.Point(10, 110);
            this.mtbTelefone.Mask = "(##)####-####";
            this.mtbTelefone.Name = "mtbTelefone";
            this.mtbTelefone.Size = new System.Drawing.Size(243, 20);
            this.mtbTelefone.TabIndex = 62;
            // 
            // tbBairro
            // 
            this.tbBairro.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsXOSFuncionario, "Bairro", true));
            this.tbBairro.Location = new System.Drawing.Point(450, 70);
            this.tbBairro.MaxLength = 30;
            this.tbBairro.Name = "tbBairro";
            this.tbBairro.Size = new System.Drawing.Size(246, 20);
            this.tbBairro.TabIndex = 5;
            // 
            // tbEndereco
            // 
            this.tbEndereco.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsXOSFuncionario, "Endereco", true));
            this.tbEndereco.Location = new System.Drawing.Point(10, 70);
            this.tbEndereco.MaxLength = 50;
            this.tbEndereco.Name = "tbEndereco";
            this.tbEndereco.Size = new System.Drawing.Size(408, 20);
            this.tbEndereco.TabIndex = 4;
            // 
            // tbCidade
            // 
            this.tbCidade.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsXOSFuncionario, "Cidade", true));
            this.tbCidade.Location = new System.Drawing.Point(240, 25);
            this.tbCidade.MaxLength = 30;
            this.tbCidade.Name = "tbCidade";
            this.tbCidade.Size = new System.Drawing.Size(456, 20);
            this.tbCidade.TabIndex = 3;
            // 
            // comboBox1
            // 
            this.comboBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox1.DataBindings.Add(new System.Windows.Forms.Binding("SelectedItem", this.bsXOSFuncionario, "Estado", true));
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.ItemHeight = 13;
            this.comboBox1.Items.AddRange(new object[] {
            "AC",
            "AL",
            "AP",
            "AM",
            "BA",
            "CE",
            "DF",
            "ES",
            "GO",
            "MA",
            "MT",
            "MS",
            "MG",
            "PA",
            "PB",
            "PR",
            "PE",
            "PI",
            "RJ",
            "RN",
            "RS",
            "RO",
            "RR",
            "SC",
            "SP",
            "SE",
            "TO"});
            this.comboBox1.Location = new System.Drawing.Point(170, 25);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(50, 21);
            this.comboBox1.TabIndex = 2;
            // 
            // tbCep
            // 
            this.tbCep.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsXOSFuncionario, "Cep", true));
            this.tbCep.Location = new System.Drawing.Point(10, 25);
            this.tbCep.MaxLength = 8;
            this.tbCep.Name = "tbCep";
            this.tbCep.Size = new System.Drawing.Size(134, 20);
            this.tbCep.TabIndex = 1;
            // 
            // tabContatos
            // 
            this.tabContatos.Controls.Add(this.checkboxPrincipal);
            this.tabContatos.Controls.Add(this.listView1);
            this.tabContatos.Controls.Add(labelContatosNumero);
            this.tabContatos.Controls.Add(this.checkBox1);
            this.tabContatos.Controls.Add(this.textboxContatosObs);
            this.tabContatos.Controls.Add(labelContatosObs);
            this.tabContatos.Controls.Add(labelContatosCPF);
            this.tabContatos.Controls.Add(this.maskedTextboxContatosCpf);
            this.tabContatos.Controls.Add(this.buttonContatosDeletar);
            this.tabContatos.Controls.Add(this.buttonContatosCancelar);
            this.tabContatos.Controls.Add(this.buttonContatosGravar);
            this.tabContatos.Controls.Add(labelContatosCelular);
            this.tabContatos.Controls.Add(this.maskedTextboxContatosCelular);
            this.tabContatos.Controls.Add(this.textboxContatosEmail);
            this.tabContatos.Controls.Add(labelContatosEmail);
            this.tabContatos.Controls.Add(this.textboxContatosNome);
            this.tabContatos.Controls.Add(labelContatosNome);
            this.tabContatos.Controls.Add(this.textboxHiddenId);
            this.tabContatos.Location = new System.Drawing.Point(4, 22);
            this.tabContatos.Name = "tabContatos";
            this.tabContatos.Padding = new System.Windows.Forms.Padding(3);
            this.tabContatos.Size = new System.Drawing.Size(972, 222);
            this.tabContatos.TabIndex = 1;
            this.tabContatos.Text = "Contatos";
            this.tabContatos.UseVisualStyleBackColor = true;
            // 
            // checkboxPrincipal
            // 
            this.checkboxPrincipal.AutoSize = true;
            this.checkboxPrincipal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkboxPrincipal.Location = new System.Drawing.Point(445, 103);
            this.checkboxPrincipal.Name = "checkboxPrincipal";
            this.checkboxPrincipal.Size = new System.Drawing.Size(65, 17);
            this.checkboxPrincipal.TabIndex = 37;
            this.checkboxPrincipal.Text = "principal";
            this.checkboxPrincipal.UseVisualStyleBackColor = true;
            // 
            // listView1
            // 
            this.listView1.Location = new System.Drawing.Point(15, 136);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(1020, 120);
            this.listView1.TabIndex = 36;
            this.listView1.UseCompatibleStateImageBehavior = false;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.Location = new System.Drawing.Point(444, 70);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(83, 17);
            this.checkBox1.TabIndex = 34;
            this.checkBox1.Text = "responsável";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // textboxContatosObs
            // 
            this.textboxContatosObs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textboxContatosObs.Location = new System.Drawing.Point(10, 70);
            this.textboxContatosObs.MaxLength = 50;
            this.textboxContatosObs.Name = "textboxContatosObs";
            this.textboxContatosObs.Size = new System.Drawing.Size(425, 20);
            this.textboxContatosObs.TabIndex = 25;
            // 
            // maskedTextboxContatosCpf
            // 
            this.maskedTextboxContatosCpf.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maskedTextboxContatosCpf.Location = new System.Drawing.Point(530, 70);
            this.maskedTextboxContatosCpf.Mask = "###.###.###-##";
            this.maskedTextboxContatosCpf.Name = "maskedTextboxContatosCpf";
            this.maskedTextboxContatosCpf.Size = new System.Drawing.Size(250, 20);
            this.maskedTextboxContatosCpf.TabIndex = 23;
            // 
            // buttonContatosDeletar
            // 
            this.buttonContatosDeletar.ForeColor = System.Drawing.Color.Red;
            this.buttonContatosDeletar.Location = new System.Drawing.Point(788, 105);
            this.buttonContatosDeletar.Name = "buttonContatosDeletar";
            this.buttonContatosDeletar.Size = new System.Drawing.Size(75, 23);
            this.buttonContatosDeletar.TabIndex = 21;
            this.buttonContatosDeletar.Text = "Deletar";
            this.buttonContatosDeletar.UseVisualStyleBackColor = true;
            this.buttonContatosDeletar.Visible = false;
            // 
            // buttonContatosCancelar
            // 
            this.buttonContatosCancelar.Location = new System.Drawing.Point(788, 24);
            this.buttonContatosCancelar.Name = "buttonContatosCancelar";
            this.buttonContatosCancelar.Size = new System.Drawing.Size(75, 23);
            this.buttonContatosCancelar.TabIndex = 20;
            this.buttonContatosCancelar.Text = "Limpar";
            this.buttonContatosCancelar.UseVisualStyleBackColor = true;
            // 
            // buttonContatosGravar
            // 
            this.buttonContatosGravar.Location = new System.Drawing.Point(788, 68);
            this.buttonContatosGravar.Name = "buttonContatosGravar";
            this.buttonContatosGravar.Size = new System.Drawing.Size(75, 23);
            this.buttonContatosGravar.TabIndex = 19;
            this.buttonContatosGravar.Text = "Gravar";
            this.buttonContatosGravar.UseVisualStyleBackColor = true;
            // 
            // maskedTextboxContatosCelular
            // 
            this.maskedTextboxContatosCelular.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maskedTextboxContatosCelular.Location = new System.Drawing.Point(530, 25);
            this.maskedTextboxContatosCelular.Mask = "(##)#####-####";
            this.maskedTextboxContatosCelular.Name = "maskedTextboxContatosCelular";
            this.maskedTextboxContatosCelular.Size = new System.Drawing.Size(250, 20);
            this.maskedTextboxContatosCelular.TabIndex = 18;
            // 
            // textboxContatosEmail
            // 
            this.textboxContatosEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textboxContatosEmail.Location = new System.Drawing.Point(270, 25);
            this.textboxContatosEmail.MaxLength = 50;
            this.textboxContatosEmail.Name = "textboxContatosEmail";
            this.textboxContatosEmail.Size = new System.Drawing.Size(250, 20);
            this.textboxContatosEmail.TabIndex = 16;
            // 
            // textboxContatosNome
            // 
            this.textboxContatosNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textboxContatosNome.Location = new System.Drawing.Point(10, 25);
            this.textboxContatosNome.MaxLength = 50;
            this.textboxContatosNome.Name = "textboxContatosNome";
            this.textboxContatosNome.Size = new System.Drawing.Size(250, 20);
            this.textboxContatosNome.TabIndex = 14;
            // 
            // textboxHiddenId
            // 
            this.textboxHiddenId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textboxHiddenId.Location = new System.Drawing.Point(760, 44);
            this.textboxHiddenId.MaxLength = 50;
            this.textboxHiddenId.Name = "textboxHiddenId";
            this.textboxHiddenId.Size = new System.Drawing.Size(20, 20);
            this.textboxHiddenId.TabIndex = 12;
            this.textboxHiddenId.Visible = false;
            // 
            // tabOutros
            // 
            this.tabOutros.Controls.Add(this.textboxCodigoSimp);
            this.tabOutros.Controls.Add(labelCodigoSimp);
            this.tabOutros.Controls.Add(this.textboxQuantSumpers);
            this.tabOutros.Controls.Add(this.textBox2);
            this.tabOutros.Controls.Add(this.textboxQuantTanques);
            this.tabOutros.Controls.Add(this.checkboxRFID);
            this.tabOutros.Controls.Add(this.labelConsentrador);
            this.tabOutros.Controls.Add(this.labelQuantSumpers);
            this.tabOutros.Controls.Add(this.labelQuantTanques);
            this.tabOutros.Controls.Add(tIPO_BOMBALabel);
            this.tabOutros.Controls.Add(this.qTDE_BOMBASTextBox);
            this.tabOutros.Controls.Add(this.tIPO_BOMBATextBox);
            this.tabOutros.Controls.Add(this.qTDE_BICOSTextBox);
            this.tabOutros.Controls.Add(qTDE_BICOSLabel);
            this.tabOutros.Controls.Add(qTDE_BOMBASLabel);
            this.tabOutros.Controls.Add(this.bANDEIRATextBox);
            this.tabOutros.Controls.Add(bANDEIRALabel);
            this.tabOutros.Controls.Add(this.mODELO_CPUTextBox);
            this.tabOutros.Controls.Add(mODELO_CPULabel);
            this.tabOutros.Location = new System.Drawing.Point(4, 22);
            this.tabOutros.Name = "tabOutros";
            this.tabOutros.Size = new System.Drawing.Size(972, 222);
            this.tabOutros.TabIndex = 2;
            this.tabOutros.Text = "   Outros";
            this.tabOutros.UseVisualStyleBackColor = true;
            // 
            // textboxCodigoSimp
            // 
            this.textboxCodigoSimp.Location = new System.Drawing.Point(510, 25);
            this.textboxCodigoSimp.MaxLength = 50;
            this.textboxCodigoSimp.Name = "textboxCodigoSimp";
            this.textboxCodigoSimp.Size = new System.Drawing.Size(220, 20);
            this.textboxCodigoSimp.TabIndex = 62;
            // 
            // textboxQuantSumpers
            // 
            this.textboxQuantSumpers.Location = new System.Drawing.Point(260, 125);
            this.textboxQuantSumpers.MaxLength = 3;
            this.textboxQuantSumpers.Name = "textboxQuantSumpers";
            this.textboxQuantSumpers.Size = new System.Drawing.Size(220, 20);
            this.textboxQuantSumpers.TabIndex = 60;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(510, 125);
            this.textBox2.MaxLength = 50;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(220, 20);
            this.textBox2.TabIndex = 59;
            // 
            // textboxQuantTanques
            // 
            this.textboxQuantTanques.Location = new System.Drawing.Point(10, 125);
            this.textboxQuantTanques.MaxLength = 3;
            this.textboxQuantTanques.Name = "textboxQuantTanques";
            this.textboxQuantTanques.Size = new System.Drawing.Size(220, 20);
            this.textboxQuantTanques.TabIndex = 58;
            // 
            // checkboxRFID
            // 
            this.checkboxRFID.AutoSize = true;
            this.checkboxRFID.Location = new System.Drawing.Point(750, 25);
            this.checkboxRFID.Name = "checkboxRFID";
            this.checkboxRFID.Size = new System.Drawing.Size(91, 17);
            this.checkboxRFID.TabIndex = 57;
            this.checkboxRFID.Text = "Possui RFID?";
            this.checkboxRFID.UseVisualStyleBackColor = true;
            // 
            // labelConsentrador
            // 
            this.labelConsentrador.AutoSize = true;
            this.labelConsentrador.Location = new System.Drawing.Point(510, 110);
            this.labelConsentrador.Name = "labelConsentrador";
            this.labelConsentrador.Size = new System.Drawing.Size(128, 13);
            this.labelConsentrador.TabIndex = 56;
            this.labelConsentrador.Text = "Concentrador Automação";
            // 
            // labelQuantSumpers
            // 
            this.labelQuantSumpers.AutoSize = true;
            this.labelQuantSumpers.Location = new System.Drawing.Point(260, 110);
            this.labelQuantSumpers.Name = "labelQuantSumpers";
            this.labelQuantSumpers.Size = new System.Drawing.Size(77, 13);
            this.labelQuantSumpers.TabIndex = 55;
            this.labelQuantSumpers.Text = "Qtde. Sumpers";
            // 
            // labelQuantTanques
            // 
            this.labelQuantTanques.AutoSize = true;
            this.labelQuantTanques.Location = new System.Drawing.Point(10, 110);
            this.labelQuantTanques.Name = "labelQuantTanques";
            this.labelQuantTanques.Size = new System.Drawing.Size(78, 13);
            this.labelQuantTanques.TabIndex = 54;
            this.labelQuantTanques.Text = "Qtde. Tanques";
            // 
            // qTDE_BOMBASTextBox
            // 
            this.qTDE_BOMBASTextBox.Location = new System.Drawing.Point(10, 75);
            this.qTDE_BOMBASTextBox.MaxLength = 3;
            this.qTDE_BOMBASTextBox.Name = "qTDE_BOMBASTextBox";
            this.qTDE_BOMBASTextBox.Size = new System.Drawing.Size(220, 20);
            this.qTDE_BOMBASTextBox.TabIndex = 48;
            this.qTDE_BOMBASTextBox.Text = " ";
            // 
            // tIPO_BOMBATextBox
            // 
            this.tIPO_BOMBATextBox.Location = new System.Drawing.Point(510, 75);
            this.tIPO_BOMBATextBox.MaxLength = 20;
            this.tIPO_BOMBATextBox.Name = "tIPO_BOMBATextBox";
            this.tIPO_BOMBATextBox.Size = new System.Drawing.Size(220, 20);
            this.tIPO_BOMBATextBox.TabIndex = 47;
            // 
            // qTDE_BICOSTextBox
            // 
            this.qTDE_BICOSTextBox.Location = new System.Drawing.Point(260, 75);
            this.qTDE_BICOSTextBox.MaxLength = 5;
            this.qTDE_BICOSTextBox.Name = "qTDE_BICOSTextBox";
            this.qTDE_BICOSTextBox.Size = new System.Drawing.Size(220, 20);
            this.qTDE_BICOSTextBox.TabIndex = 49;
            this.qTDE_BICOSTextBox.Text = "  ";
            // 
            // bANDEIRATextBox
            // 
            this.bANDEIRATextBox.Location = new System.Drawing.Point(10, 25);
            this.bANDEIRATextBox.MaxLength = 20;
            this.bANDEIRATextBox.Name = "bANDEIRATextBox";
            this.bANDEIRATextBox.Size = new System.Drawing.Size(220, 20);
            this.bANDEIRATextBox.TabIndex = 43;
            // 
            // mODELO_CPUTextBox
            // 
            this.mODELO_CPUTextBox.Location = new System.Drawing.Point(260, 25);
            this.mODELO_CPUTextBox.MaxLength = 50;
            this.mODELO_CPUTextBox.Name = "mODELO_CPUTextBox";
            this.mODELO_CPUTextBox.Size = new System.Drawing.Size(220, 20);
            this.mODELO_CPUTextBox.TabIndex = 44;
            // 
            // tabObservacoes
            // 
            this.tabObservacoes.Controls.Add(this.oBSERVACAOTextBox);
            this.tabObservacoes.Controls.Add(this.cODIGO_SIGPOSTOTextBox);
            this.tabObservacoes.Controls.Add(cODIGO_SIGPOSTOLabel);
            this.tabObservacoes.Location = new System.Drawing.Point(4, 22);
            this.tabObservacoes.Name = "tabObservacoes";
            this.tabObservacoes.Size = new System.Drawing.Size(972, 222);
            this.tabObservacoes.TabIndex = 3;
            this.tabObservacoes.Text = "Observações";
            this.tabObservacoes.UseVisualStyleBackColor = true;
            // 
            // oBSERVACAOTextBox
            // 
            this.oBSERVACAOTextBox.AcceptsReturn = true;
            this.oBSERVACAOTextBox.AcceptsTab = true;
            this.oBSERVACAOTextBox.Location = new System.Drawing.Point(18, 21);
            this.oBSERVACAOTextBox.MaxLength = 200;
            this.oBSERVACAOTextBox.Multiline = true;
            this.oBSERVACAOTextBox.Name = "oBSERVACAOTextBox";
            this.oBSERVACAOTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.oBSERVACAOTextBox.Size = new System.Drawing.Size(940, 180);
            this.oBSERVACAOTextBox.TabIndex = 1;
            // 
            // cODIGO_SIGPOSTOTextBox
            // 
            this.cODIGO_SIGPOSTOTextBox.Enabled = false;
            this.cODIGO_SIGPOSTOTextBox.Location = new System.Drawing.Point(526, 186);
            this.cODIGO_SIGPOSTOTextBox.Name = "cODIGO_SIGPOSTOTextBox";
            this.cODIGO_SIGPOSTOTextBox.ReadOnly = true;
            this.cODIGO_SIGPOSTOTextBox.Size = new System.Drawing.Size(220, 20);
            this.cODIGO_SIGPOSTOTextBox.TabIndex = 63;
            this.cODIGO_SIGPOSTOTextBox.Visible = false;
            // 
            // tabEventos
            // 
            this.tabEventos.Controls.Add(this.btnListEventos);
            this.tabEventos.Controls.Add(this.textboxHiddenIdEvento);
            this.tabEventos.Controls.Add(labelEventosNumero);
            this.tabEventos.Controls.Add(this.buttonEventosDeletar);
            this.tabEventos.Controls.Add(this.buttonEventosGravar);
            this.tabEventos.Controls.Add(this.buttonEventosCancelar);
            this.tabEventos.Controls.Add(this.datetimeRealizacao);
            this.tabEventos.Controls.Add(this.listviewEventos);
            this.tabEventos.Controls.Add(this.checkboxAlertar);
            this.tabEventos.Controls.Add(this.textboxApontamento);
            this.tabEventos.Controls.Add(this.labelApontamento);
            this.tabEventos.Controls.Add(this.labelDataEHora);
            this.tabEventos.Controls.Add(this.labelEM);
            this.tabEventos.Location = new System.Drawing.Point(4, 22);
            this.tabEventos.Name = "tabEventos";
            this.tabEventos.Size = new System.Drawing.Size(972, 222);
            this.tabEventos.TabIndex = 4;
            this.tabEventos.Text = "Eventos";
            this.tabEventos.UseVisualStyleBackColor = true;
            // 
            // btnListEventos
            // 
            this.btnListEventos.Location = new System.Drawing.Point(138, 68);
            this.btnListEventos.Margin = new System.Windows.Forms.Padding(2);
            this.btnListEventos.Name = "btnListEventos";
            this.btnListEventos.Size = new System.Drawing.Size(130, 19);
            this.btnListEventos.TabIndex = 38;
            this.btnListEventos.Text = "Listar Eventos";
            this.btnListEventos.UseVisualStyleBackColor = true;
            // 
            // textboxHiddenIdEvento
            // 
            this.textboxHiddenIdEvento.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textboxHiddenIdEvento.Location = new System.Drawing.Point(250, 56);
            this.textboxHiddenIdEvento.MaxLength = 50;
            this.textboxHiddenIdEvento.Name = "textboxHiddenIdEvento";
            this.textboxHiddenIdEvento.Size = new System.Drawing.Size(20, 20);
            this.textboxHiddenIdEvento.TabIndex = 37;
            this.textboxHiddenIdEvento.Visible = false;
            // 
            // buttonEventosDeletar
            // 
            this.buttonEventosDeletar.ForeColor = System.Drawing.Color.Red;
            this.buttonEventosDeletar.Location = new System.Drawing.Point(835, 77);
            this.buttonEventosDeletar.Name = "buttonEventosDeletar";
            this.buttonEventosDeletar.Size = new System.Drawing.Size(75, 23);
            this.buttonEventosDeletar.TabIndex = 23;
            this.buttonEventosDeletar.Text = "Deletar";
            this.buttonEventosDeletar.UseVisualStyleBackColor = true;
            this.buttonEventosDeletar.Visible = false;
            // 
            // buttonEventosGravar
            // 
            this.buttonEventosGravar.Location = new System.Drawing.Point(835, 48);
            this.buttonEventosGravar.Name = "buttonEventosGravar";
            this.buttonEventosGravar.Size = new System.Drawing.Size(75, 23);
            this.buttonEventosGravar.TabIndex = 22;
            this.buttonEventosGravar.Text = "Gravar";
            this.buttonEventosGravar.UseVisualStyleBackColor = true;
            // 
            // buttonEventosCancelar
            // 
            this.buttonEventosCancelar.Location = new System.Drawing.Point(835, 19);
            this.buttonEventosCancelar.Name = "buttonEventosCancelar";
            this.buttonEventosCancelar.Size = new System.Drawing.Size(75, 23);
            this.buttonEventosCancelar.TabIndex = 21;
            this.buttonEventosCancelar.Text = "Limpar";
            this.buttonEventosCancelar.UseVisualStyleBackColor = true;
            // 
            // datetimeRealizacao
            // 
            this.datetimeRealizacao.CustomFormat = "dd/MM/yyyy HH:mm";
            this.datetimeRealizacao.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datetimeRealizacao.Location = new System.Drawing.Point(50, 30);
            this.datetimeRealizacao.Name = "datetimeRealizacao";
            this.datetimeRealizacao.Size = new System.Drawing.Size(220, 20);
            this.datetimeRealizacao.TabIndex = 1;
            // 
            // listviewEventos
            // 
            this.listviewEventos.Location = new System.Drawing.Point(15, 110);
            this.listviewEventos.Name = "listviewEventos";
            this.listviewEventos.Size = new System.Drawing.Size(940, 105);
            this.listviewEventos.TabIndex = 6;
            this.listviewEventos.UseCompatibleStateImageBehavior = false;
            // 
            // checkboxAlertar
            // 
            this.checkboxAlertar.AutoSize = true;
            this.checkboxAlertar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkboxAlertar.Location = new System.Drawing.Point(20, 70);
            this.checkboxAlertar.Name = "checkboxAlertar";
            this.checkboxAlertar.Size = new System.Drawing.Size(74, 17);
            this.checkboxAlertar.TabIndex = 3;
            this.checkboxAlertar.Text = "Alertar ?";
            this.checkboxAlertar.UseVisualStyleBackColor = true;
            // 
            // textboxApontamento
            // 
            this.textboxApontamento.AcceptsReturn = true;
            this.textboxApontamento.AcceptsTab = true;
            this.textboxApontamento.Location = new System.Drawing.Point(300, 30);
            this.textboxApontamento.Multiline = true;
            this.textboxApontamento.Name = "textboxApontamento";
            this.textboxApontamento.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textboxApontamento.Size = new System.Drawing.Size(500, 70);
            this.textboxApontamento.TabIndex = 2;
            // 
            // labelApontamento
            // 
            this.labelApontamento.AutoSize = true;
            this.labelApontamento.Location = new System.Drawing.Point(300, 10);
            this.labelApontamento.Name = "labelApontamento";
            this.labelApontamento.Size = new System.Drawing.Size(70, 13);
            this.labelApontamento.TabIndex = 2;
            this.labelApontamento.Text = "Apontamento";
            // 
            // labelDataEHora
            // 
            this.labelDataEHora.AutoSize = true;
            this.labelDataEHora.Location = new System.Drawing.Point(60, 10);
            this.labelDataEHora.Name = "labelDataEHora";
            this.labelDataEHora.Size = new System.Drawing.Size(65, 13);
            this.labelDataEHora.TabIndex = 1;
            this.labelDataEHora.Text = "Data e Hora";
            // 
            // labelEM
            // 
            this.labelEM.AutoSize = true;
            this.labelEM.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEM.Location = new System.Drawing.Point(15, 35);
            this.labelEM.Name = "labelEM";
            this.labelEM.Size = new System.Drawing.Size(29, 13);
            this.labelEM.TabIndex = 0;
            this.labelEM.Text = "EM:";
            // 
            // tabOrcamentos
            // 
            this.tabOrcamentos.Controls.Add(this.dataGridViewOrcamentos);
            this.tabOrcamentos.Location = new System.Drawing.Point(4, 22);
            this.tabOrcamentos.Name = "tabOrcamentos";
            this.tabOrcamentos.Size = new System.Drawing.Size(972, 222);
            this.tabOrcamentos.TabIndex = 5;
            this.tabOrcamentos.Text = "Orçamentos";
            this.tabOrcamentos.UseVisualStyleBackColor = true;
            // 
            // dataGridViewOrcamentos
            // 
            this.dataGridViewOrcamentos.AllowUserToAddRows = false;
            this.dataGridViewOrcamentos.AllowUserToDeleteRows = false;
            this.dataGridViewOrcamentos.AllowUserToOrderColumns = true;
            this.dataGridViewOrcamentos.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dataGridViewOrcamentos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewOrcamentos.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewOrcamentos.Name = "dataGridViewOrcamentos";
            this.dataGridViewOrcamentos.ReadOnly = true;
            this.dataGridViewOrcamentos.Size = new System.Drawing.Size(550, 222);
            this.dataGridViewOrcamentos.TabIndex = 1;
            // 
            // tabPedidos
            // 
            this.tabPedidos.Controls.Add(this.dataGridViewPedidos);
            this.tabPedidos.Location = new System.Drawing.Point(4, 22);
            this.tabPedidos.Name = "tabPedidos";
            this.tabPedidos.Size = new System.Drawing.Size(972, 222);
            this.tabPedidos.TabIndex = 6;
            this.tabPedidos.Text = "Pedidos";
            this.tabPedidos.UseVisualStyleBackColor = true;
            // 
            // dataGridViewPedidos
            // 
            this.dataGridViewPedidos.AllowUserToAddRows = false;
            this.dataGridViewPedidos.AllowUserToDeleteRows = false;
            this.dataGridViewPedidos.AllowUserToOrderColumns = true;
            this.dataGridViewPedidos.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dataGridViewPedidos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPedidos.Location = new System.Drawing.Point(211, 0);
            this.dataGridViewPedidos.Name = "dataGridViewPedidos";
            this.dataGridViewPedidos.ReadOnly = true;
            this.dataGridViewPedidos.Size = new System.Drawing.Size(550, 222);
            this.dataGridViewPedidos.TabIndex = 2;
            // 
            // tabServicos
            // 
            this.tabServicos.Controls.Add(this.dataGridViewServicos);
            this.tabServicos.Location = new System.Drawing.Point(4, 22);
            this.tabServicos.Name = "tabServicos";
            this.tabServicos.Size = new System.Drawing.Size(972, 222);
            this.tabServicos.TabIndex = 7;
            this.tabServicos.Text = "Serviços";
            this.tabServicos.UseVisualStyleBackColor = true;
            // 
            // dataGridViewServicos
            // 
            this.dataGridViewServicos.AllowUserToAddRows = false;
            this.dataGridViewServicos.AllowUserToDeleteRows = false;
            this.dataGridViewServicos.AllowUserToOrderColumns = true;
            this.dataGridViewServicos.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dataGridViewServicos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewServicos.Location = new System.Drawing.Point(211, 0);
            this.dataGridViewServicos.Name = "dataGridViewServicos";
            this.dataGridViewServicos.ReadOnly = true;
            this.dataGridViewServicos.Size = new System.Drawing.Size(550, 222);
            this.dataGridViewServicos.TabIndex = 2;
            // 
            // tabInstrumentos
            // 
            this.tabInstrumentos.Controls.Add(this.rtbHelp2);
            this.tabInstrumentos.Controls.Add(this.rtbHelp0);
            this.tabInstrumentos.Controls.Add(this.btnLimparBomba);
            this.tabInstrumentos.Controls.Add(this.pnlEditarBomba);
            this.tabInstrumentos.Controls.Add(this.dgvBombas);
            this.tabInstrumentos.Location = new System.Drawing.Point(4, 22);
            this.tabInstrumentos.Name = "tabInstrumentos";
            this.tabInstrumentos.Size = new System.Drawing.Size(972, 222);
            this.tabInstrumentos.TabIndex = 8;
            this.tabInstrumentos.Text = "Instrumentos";
            this.tabInstrumentos.UseVisualStyleBackColor = true;
            // 
            // rtbHelp2
            // 
            this.rtbHelp2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.rtbHelp2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbHelp2.Location = new System.Drawing.Point(0, 32);
            this.rtbHelp2.Name = "rtbHelp2";
            this.rtbHelp2.Size = new System.Drawing.Size(35, 15);
            this.rtbHelp2.TabIndex = 12;
            this.rtbHelp2.Text = resources.GetString("rtbHelp2.Text");
            this.rtbHelp2.Visible = false;
            // 
            // rtbHelp0
            // 
            this.rtbHelp0.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.rtbHelp0.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbHelp0.Cursor = System.Windows.Forms.Cursors.Default;
            this.rtbHelp0.Location = new System.Drawing.Point(18, 200);
            this.rtbHelp0.Name = "rtbHelp0";
            this.rtbHelp0.Size = new System.Drawing.Size(245, 120);
            this.rtbHelp0.TabIndex = 3;
            this.rtbHelp0.TabStop = false;
            this.rtbHelp0.Text = resources.GetString("rtbHelp0.Text");
            // 
            // btnLimparBomba
            // 
            this.btnLimparBomba.Location = new System.Drawing.Point(496, 121);
            this.btnLimparBomba.Name = "btnLimparBomba";
            this.btnLimparBomba.Size = new System.Drawing.Size(85, 23);
            this.btnLimparBomba.TabIndex = 11;
            this.btnLimparBomba.Text = "&Nova Bomba";
            this.btnLimparBomba.UseVisualStyleBackColor = true;
            // 
            // pnlEditarBomba
            // 
            this.pnlEditarBomba.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlEditarBomba.Controls.Add(this.btnCancelar);
            this.pnlEditarBomba.Controls.Add(this.rtbHelp1);
            this.pnlEditarBomba.Controls.Add(this.btnGerarLacre);
            this.pnlEditarBomba.Controls.Add(this.btnDeletarBomba);
            this.pnlEditarBomba.Controls.Add(this.btnGravarBomba);
            this.pnlEditarBomba.Controls.Add(this.dgvLacre);
            this.pnlEditarBomba.Controls.Add(lblLacre);
            this.pnlEditarBomba.Controls.Add(this.tbLacre);
            this.pnlEditarBomba.Controls.Add(lblFabricante);
            this.pnlEditarBomba.Controls.Add(this.tbFabricante);
            this.pnlEditarBomba.Controls.Add(lblDescricao);
            this.pnlEditarBomba.Controls.Add(this.tbDescricao);
            this.pnlEditarBomba.Controls.Add(lblIlha);
            this.pnlEditarBomba.Controls.Add(this.tbIlha);
            this.pnlEditarBomba.Controls.Add(lblQBicos);
            this.pnlEditarBomba.Controls.Add(this.tbQBicos);
            this.pnlEditarBomba.Controls.Add(lblSerie);
            this.pnlEditarBomba.Controls.Add(this.tbSerie);
            this.pnlEditarBomba.Controls.Add(lblMarcaInstrumento);
            this.pnlEditarBomba.Controls.Add(this.cmbMarcaInstrumento);
            this.pnlEditarBomba.Location = new System.Drawing.Point(90, 10);
            this.pnlEditarBomba.Name = "pnlEditarBomba";
            this.pnlEditarBomba.Size = new System.Drawing.Size(600, 325);
            this.pnlEditarBomba.TabIndex = 1;
            this.pnlEditarBomba.Visible = false;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(400, 166);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(85, 23);
            this.btnCancelar.TabIndex = 56;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            // 
            // rtbHelp1
            // 
            this.rtbHelp1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.rtbHelp1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbHelp1.Cursor = System.Windows.Forms.Cursors.Default;
            this.rtbHelp1.Location = new System.Drawing.Point(19, 170);
            this.rtbHelp1.Name = "rtbHelp1";
            this.rtbHelp1.Size = new System.Drawing.Size(245, 80);
            this.rtbHelp1.TabIndex = 2;
            this.rtbHelp1.TabStop = false;
            this.rtbHelp1.Text = "Os lacres gerados ficam editaveis no DataGridView.\nEditar e acionar <ENTER> para " +
    "salvar.\nPara deletar um lacre selecione a e acione  a tecla <DELETE>.";
            // 
            // btnGerarLacre
            // 
            this.btnGerarLacre.Location = new System.Drawing.Point(19, 142);
            this.btnGerarLacre.Name = "btnGerarLacre";
            this.btnGerarLacre.Size = new System.Drawing.Size(85, 23);
            this.btnGerarLacre.TabIndex = 55;
            this.btnGerarLacre.Text = "Inclui&r Lacre";
            this.btnGerarLacre.UseVisualStyleBackColor = true;
            // 
            // btnDeletarBomba
            // 
            this.btnDeletarBomba.ForeColor = System.Drawing.Color.Red;
            this.btnDeletarBomba.Location = new System.Drawing.Point(493, 166);
            this.btnDeletarBomba.Name = "btnDeletarBomba";
            this.btnDeletarBomba.Size = new System.Drawing.Size(85, 23);
            this.btnDeletarBomba.TabIndex = 12;
            this.btnDeletarBomba.Text = "&Deletar Bomba";
            this.btnDeletarBomba.UseVisualStyleBackColor = true;
            // 
            // btnGravarBomba
            // 
            this.btnGravarBomba.Location = new System.Drawing.Point(20, 175);
            this.btnGravarBomba.Name = "btnGravarBomba";
            this.btnGravarBomba.Size = new System.Drawing.Size(85, 23);
            this.btnGravarBomba.TabIndex = 10;
            this.btnGravarBomba.Text = "&Gravar Bomba";
            this.btnGravarBomba.UseVisualStyleBackColor = true;
            // 
            // dgvLacre
            // 
            this.dgvLacre.AllowUserToAddRows = false;
            this.dgvLacre.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLacre.Location = new System.Drawing.Point(312, 100);
            this.dgvLacre.Name = "dgvLacre";
            this.dgvLacre.Size = new System.Drawing.Size(240, 190);
            this.dgvLacre.TabIndex = 8;
            this.dgvLacre.TabStop = false;
            // 
            // tbLacre
            // 
            this.tbLacre.Location = new System.Drawing.Point(19, 116);
            this.tbLacre.MaxLength = 15;
            this.tbLacre.Name = "tbLacre";
            this.tbLacre.Size = new System.Drawing.Size(180, 20);
            this.tbLacre.TabIndex = 7;
            // 
            // tbFabricante
            // 
            this.tbFabricante.Location = new System.Drawing.Point(211, 65);
            this.tbFabricante.Name = "tbFabricante";
            this.tbFabricante.Size = new System.Drawing.Size(180, 20);
            this.tbFabricante.TabIndex = 5;
            // 
            // tbDescricao
            // 
            this.tbDescricao.Location = new System.Drawing.Point(19, 65);
            this.tbDescricao.Name = "tbDescricao";
            this.tbDescricao.Size = new System.Drawing.Size(180, 20);
            this.tbDescricao.TabIndex = 4;
            // 
            // tbIlha
            // 
            this.tbIlha.Location = new System.Drawing.Point(453, 21);
            this.tbIlha.Name = "tbIlha";
            this.tbIlha.Size = new System.Drawing.Size(100, 20);
            this.tbIlha.TabIndex = 3;
            // 
            // tbQBicos
            // 
            this.tbQBicos.Location = new System.Drawing.Point(349, 21);
            this.tbQBicos.MaxLength = 1;
            this.tbQBicos.Name = "tbQBicos";
            this.tbQBicos.Size = new System.Drawing.Size(100, 20);
            this.tbQBicos.TabIndex = 2;
            // 
            // tbSerie
            // 
            this.tbSerie.Location = new System.Drawing.Point(19, 21);
            this.tbSerie.Name = "tbSerie";
            this.tbSerie.Size = new System.Drawing.Size(316, 20);
            this.tbSerie.TabIndex = 1;
            // 
            // cmbMarcaInstrumento
            // 
            this.cmbMarcaInstrumento.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbMarcaInstrumento.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbMarcaInstrumento.FormattingEnabled = true;
            this.cmbMarcaInstrumento.Items.AddRange(new object[] {
            "Daruma",
            "Extratema",
            "GBR Veeder Root",
            "Gilbarco Mecânica",
            "Gilbarco Pro",
            "Gilbarco Pulma Fit",
            "Tokheim",
            "Wayne 3G",
            "Wayne Duplex",
            "Wayne Mecânica",
            "Wayne Minnow"});
            this.cmbMarcaInstrumento.Location = new System.Drawing.Point(402, 64);
            this.cmbMarcaInstrumento.MaxLength = 30;
            this.cmbMarcaInstrumento.Name = "cmbMarcaInstrumento";
            this.cmbMarcaInstrumento.Size = new System.Drawing.Size(151, 21);
            this.cmbMarcaInstrumento.Sorted = true;
            this.cmbMarcaInstrumento.TabIndex = 6;
            // 
            // dgvBombas
            // 
            this.dgvBombas.AllowUserToAddRows = false;
            this.dgvBombas.AllowUserToDeleteRows = false;
            this.dgvBombas.AllowUserToOrderColumns = true;
            this.dgvBombas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBombas.Location = new System.Drawing.Point(15, 10);
            this.dgvBombas.Name = "dgvBombas";
            this.dgvBombas.ReadOnly = true;
            this.dgvBombas.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvBombas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvBombas.Size = new System.Drawing.Size(440, 150);
            this.dgvBombas.TabIndex = 0;
            // 
            // tbNome
            // 
            this.tbNome.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsXOSFuncionario, "Nome", true));
            this.tbNome.Location = new System.Drawing.Point(100, 115);
            this.tbNome.MaxLength = 50;
            this.tbNome.Name = "tbNome";
            this.tbNome.Size = new System.Drawing.Size(610, 20);
            this.tbNome.TabIndex = 4;
            // 
            // nOME_FANTASIATextBox
            // 
            this.nOME_FANTASIATextBox.Location = new System.Drawing.Point(100, 155);
            this.nOME_FANTASIATextBox.MaxLength = 20;
            this.nOME_FANTASIATextBox.Name = "nOME_FANTASIATextBox";
            this.nOME_FANTASIATextBox.Size = new System.Drawing.Size(610, 20);
            this.nOME_FANTASIATextBox.TabIndex = 5;
            this.nOME_FANTASIATextBox.Visible = false;
            // 
            // mtbCPF
            // 
            this.mtbCPF.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsXOSFuncionario, "Cgc", true));
            this.mtbCPF.Location = new System.Drawing.Point(110, 25);
            this.mtbCPF.Mask = "###.###.###-##";
            this.mtbCPF.Name = "mtbCPF";
            this.mtbCPF.Size = new System.Drawing.Size(220, 20);
            this.mtbCPF.TabIndex = 2;
            // 
            // mtbCNPJ
            // 
            this.mtbCNPJ.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsXOSFuncionario, "Cgc", true));
            this.mtbCNPJ.Location = new System.Drawing.Point(110, 70);
            this.mtbCNPJ.Mask = "##.###.###/####-##";
            this.mtbCNPJ.Name = "mtbCNPJ";
            this.mtbCNPJ.Size = new System.Drawing.Size(220, 20);
            this.mtbCNPJ.TabIndex = 2;
            this.mtbCNPJ.Visible = false;
            // 
            // comboBox2
            // 
            this.comboBox2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox2.DataBindings.Add(new System.Windows.Forms.Binding("SelectedItem", this.bsXOSFuncionario, "Tipo_Pessoa", true));
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "F",
            "J"});
            this.comboBox2.Location = new System.Drawing.Point(25, 25);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(38, 21);
            this.comboBox2.TabIndex = 1;
            this.comboBox2.Visible = false;
            this.comboBox2.SelectionChangeCommitted += new System.EventHandler(this.comboBox2_SelectionChangeCommitted);
            // 
            // tbIdusuario
            // 
            this.tbIdusuario.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsXOSFuncionario, "idusuario", true));
            this.tbIdusuario.Enabled = false;
            this.tbIdusuario.Location = new System.Drawing.Point(25, 115);
            this.tbIdusuario.Name = "tbIdusuario";
            this.tbIdusuario.ReadOnly = true;
            this.tbIdusuario.Size = new System.Drawing.Size(50, 20);
            this.tbIdusuario.TabIndex = 1;
            // 
            // tb_email
            // 
            this.tb_email.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsXOSFuncionario, "obs01", true));
            this.tb_email.Location = new System.Drawing.Point(360, 70);
            this.tb_email.MaxLength = 100;
            this.tb_email.Name = "tb_email";
            this.tb_email.Size = new System.Drawing.Size(320, 20);
            this.tb_email.TabIndex = 3;
            // 
            // cPFTextBox
            // 
            this.cPFTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsXOSFuncionario, "Cgc", true));
            this.cPFTextBox.Location = new System.Drawing.Point(10, 461);
            this.cPFTextBox.MaxLength = 11;
            this.cPFTextBox.Name = "cPFTextBox";
            this.cPFTextBox.Size = new System.Drawing.Size(220, 20);
            this.cPFTextBox.TabIndex = 0;
            this.cPFTextBox.TabStop = false;
            // 
            // cNPJTextBox
            // 
            this.cNPJTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsXOSFuncionario, "Cgc", true));
            this.cNPJTextBox.Location = new System.Drawing.Point(240, 461);
            this.cNPJTextBox.MaxLength = 15;
            this.cNPJTextBox.Name = "cNPJTextBox";
            this.cNPJTextBox.Size = new System.Drawing.Size(220, 20);
            this.cNPJTextBox.TabIndex = 0;
            this.cNPJTextBox.TabStop = false;
            this.cNPJTextBox.Visible = false;
            // 
            // ManFuncionario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1184, 711);
            this.Controls.Add(this.FuncionarioBindingNavigator);
            this.Controls.Add(this.pnlConsultar);
            this.Controls.Add(this.labelCadastrarFuncionario);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.MaximizeBox = true;
            this.MinimizeBox = true;
            this.Name = "ManFuncionario";
            this.Text = "ManFuncionario";
            this.Load += new System.EventHandler(this.ManFuncionario_Load);
            this.Controls.SetChildIndex(this.grpPrincipal, 0);
            this.Controls.SetChildIndex(this.tabPrincipal, 0);
            this.Controls.SetChildIndex(this.labelCadastrarFuncionario, 0);
            this.Controls.SetChildIndex(this.pnlConsultar, 0);
            this.Controls.SetChildIndex(this.FuncionarioBindingNavigator, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dtsPrincipal)).EndInit();
            this.tabPrincipal.ResumeLayout(false);
            this.pnlConsultar.ResumeLayout(false);
            this.pnlConsultar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFuncionario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsXOSFuncionario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtsXOSFuncionario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FuncionarioBindingNavigator)).EndInit();
            this.FuncionarioBindingNavigator.ResumeLayout(false);
            this.FuncionarioBindingNavigator.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabChild.ResumeLayout(false);
            this.tabEndereco.ResumeLayout(false);
            this.tabEndereco.PerformLayout();
            this.tabContatos.ResumeLayout(false);
            this.tabContatos.PerformLayout();
            this.tabOutros.ResumeLayout(false);
            this.tabOutros.PerformLayout();
            this.tabObservacoes.ResumeLayout(false);
            this.tabObservacoes.PerformLayout();
            this.tabEventos.ResumeLayout(false);
            this.tabEventos.PerformLayout();
            this.tabOrcamentos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOrcamentos)).EndInit();
            this.tabPedidos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPedidos)).EndInit();
            this.tabServicos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewServicos)).EndInit();
            this.tabInstrumentos.ResumeLayout(false);
            this.pnlEditarBomba.ResumeLayout(false);
            this.pnlEditarBomba.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLacre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBombas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelCadastrarFuncionario;
        private System.Windows.Forms.Panel pnlConsultar;
        private System.Windows.Forms.TextBox tbLike;
        private System.Windows.Forms.Label labelLike;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label lblTotalOS;
        private System.Windows.Forms.DataGridView dgvFuncionario;
        private System.Windows.Forms.DataGridViewTextBoxColumn CNPJ;
        private System.Windows.Forms.BindingSource bsXOSFuncionario;
        private XOSFuncionario dtsXOSFuncionario;
        private XOSFuncionarioTableAdapters.XOsFuncionarioTableAdapter xOsFuncionarioTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn postoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn funcionarioDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn turnoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipoPessoaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cgcDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn inscricaoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn enderecoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bairroDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cidadeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn estadoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cepDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs01DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs02DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs03DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs04DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs05DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs06DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs07DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs08DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs09DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs10DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn noturnoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn inativoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtAlterDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs11DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs12DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs13DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs14DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs15DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs16DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs17DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs18DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs19DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obs20DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rFIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn biometriaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cTACaixaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipoVIPDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn uNomeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn uSenhaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipoacessoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtcadastroDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ultimoacessoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn planocontaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn acessapedidoDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn digitaselolacreDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idusuarioDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn isusuarioDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingNavigator FuncionarioBindingNavigator;
        private System.Windows.Forms.ToolStripButton bnInserir;
        private System.Windows.Forms.ToolStripLabel bnCount;
        private System.Windows.Forms.ToolStripButton bnConsultar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton bnGravar;
        private System.Windows.Forms.ToolStripButton bnCancelar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton bnPrimeiro;
        private System.Windows.Forms.ToolStripButton bnAnterior;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripTextBox bnPosition;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton bnProximo;
        private System.Windows.Forms.ToolStripButton bnUltimo;
        private System.Windows.Forms.ToolStripButton bnDescartar;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.CheckBox checkboxIsUsuario;
        private System.Windows.Forms.TextBox tb_rg;
        private System.Windows.Forms.TabControl tabChild;
        private System.Windows.Forms.TabPage tabEndereco;
        private System.Windows.Forms.TextBox fAXTextBox;
        private System.Windows.Forms.TextBox tELEFONETextBox;
        private System.Windows.Forms.MaskedTextBox mtbTelefone;
        private System.Windows.Forms.TextBox tbBairro;
        private System.Windows.Forms.TextBox tbEndereco;
        private System.Windows.Forms.TextBox tbCidade;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox tbCep;
        private System.Windows.Forms.TabPage tabContatos;
        private System.Windows.Forms.CheckBox checkboxPrincipal;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TextBox textboxContatosObs;
        private System.Windows.Forms.MaskedTextBox maskedTextboxContatosCpf;
        private System.Windows.Forms.Button buttonContatosDeletar;
        private System.Windows.Forms.Button buttonContatosCancelar;
        private System.Windows.Forms.Button buttonContatosGravar;
        private System.Windows.Forms.MaskedTextBox maskedTextboxContatosCelular;
        private System.Windows.Forms.TextBox textboxContatosEmail;
        private System.Windows.Forms.TextBox textboxContatosNome;
        private System.Windows.Forms.TextBox textboxHiddenId;
        private System.Windows.Forms.TabPage tabOutros;
        private System.Windows.Forms.TextBox textboxCodigoSimp;
        private System.Windows.Forms.TextBox textboxQuantSumpers;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textboxQuantTanques;
        private System.Windows.Forms.CheckBox checkboxRFID;
        private System.Windows.Forms.Label labelConsentrador;
        private System.Windows.Forms.Label labelQuantSumpers;
        private System.Windows.Forms.Label labelQuantTanques;
        private System.Windows.Forms.TextBox qTDE_BOMBASTextBox;
        private System.Windows.Forms.TextBox tIPO_BOMBATextBox;
        private System.Windows.Forms.TextBox qTDE_BICOSTextBox;
        private System.Windows.Forms.TextBox bANDEIRATextBox;
        private System.Windows.Forms.TextBox mODELO_CPUTextBox;
        private System.Windows.Forms.TabPage tabObservacoes;
        private System.Windows.Forms.TextBox oBSERVACAOTextBox;
        private System.Windows.Forms.TextBox cODIGO_SIGPOSTOTextBox;
        private System.Windows.Forms.TabPage tabEventos;
        private System.Windows.Forms.Button btnListEventos;
        private System.Windows.Forms.TextBox textboxHiddenIdEvento;
        private System.Windows.Forms.Button buttonEventosDeletar;
        private System.Windows.Forms.Button buttonEventosGravar;
        private System.Windows.Forms.Button buttonEventosCancelar;
        private System.Windows.Forms.DateTimePicker datetimeRealizacao;
        private System.Windows.Forms.ListView listviewEventos;
        private System.Windows.Forms.CheckBox checkboxAlertar;
        private System.Windows.Forms.TextBox textboxApontamento;
        private System.Windows.Forms.Label labelApontamento;
        private System.Windows.Forms.Label labelDataEHora;
        private System.Windows.Forms.Label labelEM;
        private System.Windows.Forms.TabPage tabOrcamentos;
        private System.Windows.Forms.DataGridView dataGridViewOrcamentos;
        private System.Windows.Forms.TabPage tabPedidos;
        private System.Windows.Forms.DataGridView dataGridViewPedidos;
        private System.Windows.Forms.TabPage tabServicos;
        private System.Windows.Forms.DataGridView dataGridViewServicos;
        private System.Windows.Forms.TabPage tabInstrumentos;
        private System.Windows.Forms.RichTextBox rtbHelp2;
        private System.Windows.Forms.RichTextBox rtbHelp0;
        private System.Windows.Forms.Button btnLimparBomba;
        private System.Windows.Forms.Panel pnlEditarBomba;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.RichTextBox rtbHelp1;
        private System.Windows.Forms.Button btnGerarLacre;
        private System.Windows.Forms.Button btnDeletarBomba;
        private System.Windows.Forms.Button btnGravarBomba;
        private System.Windows.Forms.DataGridView dgvLacre;
        private System.Windows.Forms.TextBox tbLacre;
        private System.Windows.Forms.TextBox tbFabricante;
        private System.Windows.Forms.TextBox tbDescricao;
        private System.Windows.Forms.TextBox tbIlha;
        private System.Windows.Forms.TextBox tbQBicos;
        private System.Windows.Forms.TextBox tbSerie;
        private System.Windows.Forms.ComboBox cmbMarcaInstrumento;
        private System.Windows.Forms.DataGridView dgvBombas;
        private System.Windows.Forms.TextBox tbNome;
        private System.Windows.Forms.TextBox nOME_FANTASIATextBox;
        private System.Windows.Forms.MaskedTextBox mtbCPF;
        private System.Windows.Forms.MaskedTextBox mtbCNPJ;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.TextBox tbIdusuario;
        private System.Windows.Forms.TextBox tb_email;
        private System.Windows.Forms.TextBox cPFTextBox;
        private System.Windows.Forms.TextBox cNPJTextBox;
        private System.Windows.Forms.TextBox tbSenha;
        private System.Windows.Forms.TextBox tbLogin;
        private System.Windows.Forms.Label labelSenha;
        private System.Windows.Forms.Label labelLogin;
        private System.Windows.Forms.Label labelCep;

    }
}