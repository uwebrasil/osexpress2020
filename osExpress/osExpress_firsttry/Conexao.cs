﻿using System;
using System.IO;
using osExpress.Helper;
using TecnoExpress.XML;

namespace osExpress
{
    /// <summary>
    /// Database connection with crypted password
    /// </summary>
    public class Conexao
    {
        public string Caminho { get; set; }

        public Conexao() { }
        public Conexao(string caminho)
        {
            Caminho = caminho;
            if (!File.Exists(caminho))
            {
                ip = String.Empty;                
                usuario = "sa";
                senha = MD5Crypt.Criptografar("@tecno12hpa");
                instancia = String.Empty;
                new XmlManager<Conexao>().Save(caminho, this);
            }
        }

        public string ip { get; set; }
                
        public string usuario { get; set; }

        public string instancia { get; set; }
        
        private string senha;
        public string Senha
        {
            get { return MD5Crypt.Descriptografar(senha); }
            set { senha = MD5Crypt.Criptografar(value); }
        }

    }
}
