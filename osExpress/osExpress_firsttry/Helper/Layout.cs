﻿#region COPYRIGHT
//   <copyright file=Layout company="TecnoExpress">
//    Copyright (c) 2015 All Rights Reserved
//   </copyright>
//   <author>Uwe Kristmann</author>
//   <date>3/24/2015 1:53:45 PM</date>   
#endregion
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace osExpress.Helper
{
    /// <summary>
    /// Serve para padronizar o layout das janelas.
    /// </summary>
    public static class Layout
    {
        // Cliente
        /*
        public static Point LocationClientePanel = new Point(10,90);
        public static Point LocationClienteTabControl = new Point(10, 90);

        public static Size SizeClientePanel = new Size(765, 355);
        public static Size SizeClienteTabControl = new Size(765, 355);
        */
        
        public static Point LocationClientePanel = new Point(10, 90);
        public static Point LocationClienteTabControl = new Point(10, 190);
        public static Size SizeClientePanel = new Size(980, 555);
        //public static Size SizeClienteTabControl = new Size(980, 455);
        public static Size SizeClienteTabControl = new Size(980, 555);

        // Pedido        

        public static Point LocationPedidoPanel = new Point(10, 90);
        public static Point LocationPedidoGroupBox = new Point(10, 90);
        //public static Point LocationPedidoTabControl = new Point(10, 255);
        public static Point LocationPedidoTabControl = new Point(10, 300);

        //public static Size SizePedidoPanel = new Size(830, 565);
        public static Size SizePedidoPanel = new Size(830, 610);
        //public static Size SizePedidoGroupBox = new Size(830, 165);
        public static Size SizePedidoGroupBox = new Size(830, 210);
        public static Size SizePedidoTabControl = new Size(830, 400);

        // Ordem de Serviço
        public static Point LocationOrdemPanel = new Point(10, 90);       
        public static Point LocationOrdemGroupBox = new Point(10, 90);
        //public static Point LocationOrdemTabControl = new Point(10, 255);
        public static Point LocationOrdemTabControl = new Point(10, 300);

        //public static Size SizeOrdemPanel = new Size(830, 565);
        public static Size SizeOrdemPanel = new Size(830, 610);
        //public static Size SizeOrdemGroupBox = new Size(830, 165);
        public static Size SizeOrdemGroupBox = new Size(830, 210);
        public static Size SizeOrdemTabControl = new Size(830, 400);
    }
}
