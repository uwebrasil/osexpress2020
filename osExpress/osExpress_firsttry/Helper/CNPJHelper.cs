﻿#region COPYRIGHT
//   <copyright file=CNPJHelper company="TecnoExpress">
//    Copyright (c) 2015 All Rights Reserved
//   </copyright>
//   <author>Uwe Kristmann</author>
//   <date>3/23/2015 2:20:55 PM</date>   
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace osExpress.Helper
{
    public class CNPJHelper
    {
        //Função obtida de http://www.macoratti.net/11/09/c_val1.htm
        public static bool IsCnpj(string cnpj)
        {
            // uwe:permitir só números 0-9
            if (!System.Text.RegularExpressions.Regex.IsMatch(cnpj, "^[0-9]*$"))
                return false;

            int[] multiplicador1 = new int[12] { 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };

            int[] multiplicador2 = new int[13] { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };

            int soma;

            int resto;

            string digito;

            string tempCnpj;

            cnpj = cnpj.Trim();

            cnpj = cnpj.Replace(".", "").Replace("-", "").Replace("/", "");
            if (cnpj.Length != 14)
                return false;

            tempCnpj = cnpj.Substring(0, 12);
            soma = 0;

            for (int i = 0; i < 12; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador1[i];
            resto = (soma % 11);

            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;

            digito = resto.ToString();

            tempCnpj = tempCnpj + digito;

            soma = 0;

            for (int i = 0; i < 13; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador2[i];

            resto = (soma % 11);

            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;

            digito = digito + resto.ToString();

            return cnpj.EndsWith(digito);
        }
    }
}
