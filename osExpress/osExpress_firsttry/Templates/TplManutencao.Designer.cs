﻿namespace osExpress
{
    partial class FrmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPrincipal));
            this.stsPrincipal = new System.Windows.Forms.StatusStrip();
            this.tssStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.dtsPrincipal = new osExpress.OsExpressDataSet();
            this.grpPrincipal = new System.Windows.Forms.GroupBox();
            this.tabPrincipal = new System.Windows.Forms.TabControl();
            this.stsPrincipal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtsPrincipal)).BeginInit();
            this.SuspendLayout();
            // 
            // stsPrincipal
            // 
            this.stsPrincipal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tssStatus});
            this.stsPrincipal.Location = new System.Drawing.Point(0, 570);
            this.stsPrincipal.Name = "stsPrincipal";
            this.stsPrincipal.Size = new System.Drawing.Size(784, 22);
            this.stsPrincipal.TabIndex = 7;
            this.stsPrincipal.Text = "statusStrip1";
            // 
            // tssStatus
            // 
            this.tssStatus.Name = "tssStatus";
            this.tssStatus.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.tssStatus.Size = new System.Drawing.Size(65, 17);
            this.tssStatus.Text = "Status: ";
            // 
            // dtsPrincipal
            // 
            this.dtsPrincipal.DataSetName = "OsExpressDataSet";
            this.dtsPrincipal.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // grpPrincipal
            // 
            this.grpPrincipal.Location = new System.Drawing.Point(12, 87);
            this.grpPrincipal.Name = "grpPrincipal";
            this.grpPrincipal.Size = new System.Drawing.Size(760, 94);
            this.grpPrincipal.TabIndex = 0;
            this.grpPrincipal.TabStop = false;
            this.grpPrincipal.Text = " Nome Cabeçalho Principal (Mudar) ";
            // 
            // tabPrincipal
            // 
            this.tabPrincipal.Location = new System.Drawing.Point(12, 187);
            this.tabPrincipal.Name = "tabPrincipal";
            this.tabPrincipal.SelectedIndex = 0;
            this.tabPrincipal.Size = new System.Drawing.Size(760, 380);
            this.tabPrincipal.TabIndex = 6;
            // 
            // FrmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 592);
            this.Controls.Add(this.stsPrincipal);
            this.Controls.Add(this.tabPrincipal);
            this.Controls.Add(this.grpPrincipal);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Template de Cadastro (Mudar)";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmPrincipal_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmPrincipal_KeyDown);
            this.stsPrincipal.ResumeLayout(false);
            this.stsPrincipal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtsPrincipal)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected System.Windows.Forms.StatusStrip stsPrincipal;
        protected System.Windows.Forms.ToolStripStatusLabel tssStatus;
        protected OsExpressDataSet dtsPrincipal;
        protected System.Windows.Forms.GroupBox grpPrincipal;
        public System.Windows.Forms.TabControl tabPrincipal;

    }
}