﻿using System;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace osExpress.Relatorios
{
    public partial class RelRelatorios : Form
    {
        public RelRelatorios()
        {
            InitializeComponent();
        }

        private void SetDBLogonForReport(ConnectionInfo connectionInfo, ReportDocument ArquivoReport)
        {
            Tables tables = ArquivoReport.Database.Tables;
            foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
            {
                TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                tableLogonInfo.ConnectionInfo = connectionInfo;
                table.ApplyLogOnInfo(tableLogonInfo);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ReportDocument rpt = new ReportDocument();

            string reportPath = "";

            if (cmbRelatorio.SelectedIndex == 0)
                reportPath = @"Relatorios\RelOrdemServico.rpt";
            else if (cmbRelatorio.SelectedIndex == 1)
                reportPath = @"Relatorios\RelIbametro.rpt";
            else if (cmbRelatorio.SelectedIndex == 2)
                reportPath = @"Relatorios\RelReciboServico.rpt";

            rpt.Load(reportPath);
            //
            ConnectionInfo minhaConexao = new ConnectionInfo();
            minhaConexao.ServerName = OsConfiguracoes.IpServidor + "\\sigtecno";
            minhaConexao.DatabaseName = "OsExpress";
            minhaConexao.UserID = "sa";
            minhaConexao.Password = "@tecno12hpa";
            SetDBLogonForReport(minhaConexao, rpt);
            rptPrincipal.ReportSource = rpt;
        }

        private void RelRelatorios_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'osExpressDataSet.A30CLIENTES' table. You can move, or remove it, as needed.
            this.a30CLIENTESTableAdapter.Fill(this.osExpressDataSet.A30CLIENTES);
            //
            this.WindowState = FormWindowState.Maximized;
            //
            cmbRelatorio.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //EnviaEmail.enviarCliente();
        }

        private void RelRelatorios_KeyDown(object sender, KeyEventArgs e)
        {
            // Se for pressionado a tecla ESC fecha a tela.
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
