﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TExpress;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Data.SqlClient;
using TecnoExpress;


namespace CRSituacao
{
    public partial class Form1 : TecnoExpress.Forms.TForm
    {
        public Form1()
        {
            InitializeComponent();
            DB.ConnectionName = "osExpress.Properties.Settings.OsExpressConnectionString";       
            AddControlType(typeof(Panel));
            dtp_inicio.Value = DateTime.Today.AddDays(-8);
            dtp_fim.Value = DateTime.Today.AddDays(-1);
        }
        //public CrystalDecisions.Windows.Forms.CrystalReportViewer getCRViewer()
        //{
        //    return crystalReportViewer1;
        //}
        private void Form1_Load(object sender, EventArgs e)
        {
            // escolher USUÁRIO
            string sql = @"select nome+' ('+cast(id as varchar)+')' val from OsUsuario order by nome";
            DB.FillComboBox(cb_usuario, sql);

            // disable GroupView
            crystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            crystalReportViewer1.ShowGroupTreeButton = false;
        }

        // id de usuário
        private string getUid()
        {
            string u = cb_usuario.SelectedItem.ToString().Split('(')[1];
            u = u.Substring(0, u.Length - 1);
            return u;
        }

        // nome de usuário
        private string getUName()
        {
            string u = cb_usuario.SelectedItem.ToString().Split('(')[0];
            u = u.Substring(0, u.Length - 1);
            return u;
        }

        private string createQuery(string inicio, string fim)
        {
            string sql = @"select                            
                             cast(s.hora_inicio as smalldatetime) as hora_inicio, 
                             cast(s.hora_fim as smalldatetime) as hora_fim,
                             cast(s.hora_total as smalldatetime) as hora_total,
                             os.ID_FINAL as OS, c.Logotipo as Cliente,
                             p.NOME, p.DESCRICAO,
                             datepart(hh,cast(s.hora_total as smalldatetime))*3600 + 
                             datepart(mi,cast(s.hora_total as smalldatetime)) * 60  SECONDS
                            from OsUsuario u, OsOrdemServico os, OsSituacoesOS s , OsParSituacao p
                                  ,A30CLIENTES c
                            where u.ID = os.IDUSUARIO
                              and u.IDPOSTO = os.idposto
                              and os.ID = s.idordem 
                              and s.IDSITUACAO = p.id  
                              and c.ID = os.idcliente
                              and s.HORA_INICIO is not null
                              and s.HORA_INICIO >= '" + inicio + @"'
                              and s.HORA_FIM <= '" + fim + @"'
                              and u.id =" + getUid()
                           + @" order by s.HORA_INICIO, s.HORA_FIM"; 
            return sql;
        }

        private ParameterField getParameterField(string name, string value)
        {            
            ParameterField paramField = new ParameterField();
            ParameterDiscreteValue paramDiscreteValue = new ParameterDiscreteValue();
            paramField.Name = name;
            paramDiscreteValue.Value = value;
            paramField.CurrentValues.Add(paramDiscreteValue);
            
            return paramField;
        }
        public void RunReport(string inicio, string fim)
        {            
            // entregar parâmetro ao crystal
            ReportDocument reportDocument = new ReportDocument();

            ParameterFields paramFields = new ParameterFields();
            ParameterField paramField = new ParameterField();
            ParameterDiscreteValue paramDiscreteValue = new ParameterDiscreteValue();

            paramField.Name = "@pUser";
            paramDiscreteValue.Value = getUName();            
            paramField.CurrentValues.Add(paramDiscreteValue);
            paramFields.Add(paramField);

            paramFields.Add(getParameterField("@pVON", dtp_inicio.Value.ToString("dd/MM/yyyy")));
            paramFields.Add(getParameterField("@pBIS", dtp_fim.Value.ToString("dd/MM/yyyy")));
          
            // este serve para a
            crystalReportViewer1.ParameterFieldInfo = paramFields;

            string query = createQuery(inicio,fim);
            if (query.Equals(string.Empty)) return;

            CrystalReport1 report = new CrystalReport1();

            DataSet1TableAdapters.DataTable1TableAdapter ta
                = new DataSet1TableAdapters.DataTable1TableAdapter();

            SqlConnection conn = new SqlConnection(DB.GetConnectionString());
            SqlDataAdapter Adapter = new SqlDataAdapter("", conn);
            Adapter.SelectCommand.CommandType = CommandType.Text;
            Adapter.SelectCommand.CommandText = query;            
            
            DataSet1.DataTable1DataTable table = new DataSet1.DataTable1DataTable();
            //DataSet1.EnforceConstraints = false;
          
            try
            {
                table.Clear();
                table.ClienteColumn.MaxLength = 20;
                //MessageBox.Show(table.ClienteColumn.MaxLength.ToString());
                Adapter.Fill(table);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\nFalha ao carregar o relatório.", "ERRO Adapter.Fill",
                                MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                report.SetDataSource(table.DefaultView);
            }
            catch (System.Runtime.InteropServices.COMException icomex)
            {
                MessageBox.Show(icomex.Message + "\nFalha ao carregar o relatório.", "ERRO",
                                MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            crystalReportViewer1.ReportSource = report;            
            // set zoom
            crystalReportViewer1.Zoom(88);
            crystalReportViewer1.Refresh();

        }

        private void btn_run_Click(object sender, EventArgs e)
        {
            DateTime inicio = dtp_inicio.Value;
            DateTime fim    = dtp_fim.Value;
            int timediff = DateTime.Compare(inicio, fim);
       
            if (timediff > 0)
            {
              
                MessageBox.Show("FIM não pode ser menor que INÍCIO!", "A V I S O", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (cb_usuario.SelectedItem == null)
            {
                MessageBox.Show("Favor escolher usuário!", "A V I S O", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            RunReport(inicio.ToString("dd/MM/yyyy"), fim.ToString("dd/MM/yyyy"));           
        }       
    }
}
