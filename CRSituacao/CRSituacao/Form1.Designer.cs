﻿namespace CRSituacao
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel_filtro = new System.Windows.Forms.Panel();
            this.lbl_fim = new System.Windows.Forms.Label();
            this.lbl_inicio = new System.Windows.Forms.Label();
            this.dtp_fim = new System.Windows.Forms.DateTimePicker();
            this.dtp_inicio = new System.Windows.Forms.DateTimePicker();
            this.lbl_usuario = new System.Windows.Forms.Label();
            this.btn_run = new System.Windows.Forms.Button();
            this.cb_usuario = new System.Windows.Forms.ComboBox();
            this.panel_crystal = new System.Windows.Forms.Panel();
            this.crystalReportViewer1 = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.CrystalReport11 = new CRSituacao.CrystalReport1();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel_filtro.SuspendLayout();
            this.panel_crystal.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel_filtro);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel_crystal);
            this.splitContainer1.Size = new System.Drawing.Size(1064, 662);
            this.splitContainer1.SplitterDistance = 120;
            this.splitContainer1.TabIndex = 0;
            // 
            // panel_filtro
            // 
            this.panel_filtro.Controls.Add(this.lbl_fim);
            this.panel_filtro.Controls.Add(this.lbl_inicio);
            this.panel_filtro.Controls.Add(this.dtp_fim);
            this.panel_filtro.Controls.Add(this.dtp_inicio);
            this.panel_filtro.Controls.Add(this.lbl_usuario);
            this.panel_filtro.Controls.Add(this.btn_run);
            this.panel_filtro.Controls.Add(this.cb_usuario);
            this.panel_filtro.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_filtro.Location = new System.Drawing.Point(0, 0);
            this.panel_filtro.Name = "panel_filtro";
            this.panel_filtro.Size = new System.Drawing.Size(1064, 120);
            this.panel_filtro.TabIndex = 0;
            // 
            // lbl_fim
            // 
            this.lbl_fim.AutoSize = true;
            this.lbl_fim.BackColor = System.Drawing.Color.Transparent;
            this.lbl_fim.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_fim.Location = new System.Drawing.Point(220, 68);
            this.lbl_fim.Name = "lbl_fim";
            this.lbl_fim.Size = new System.Drawing.Size(26, 13);
            this.lbl_fim.TabIndex = 6;
            this.lbl_fim.Text = "Fim";
            // 
            // lbl_inicio
            // 
            this.lbl_inicio.AutoSize = true;
            this.lbl_inicio.BackColor = System.Drawing.Color.Transparent;
            this.lbl_inicio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_inicio.Location = new System.Drawing.Point(220, 37);
            this.lbl_inicio.Name = "lbl_inicio";
            this.lbl_inicio.Size = new System.Drawing.Size(40, 13);
            this.lbl_inicio.TabIndex = 5;
            this.lbl_inicio.Text = "Início";
            // 
            // dtp_fim
            // 
            this.dtp_fim.Location = new System.Drawing.Point(265, 62);
            this.dtp_fim.Name = "dtp_fim";
            this.dtp_fim.Size = new System.Drawing.Size(250, 20);
            this.dtp_fim.TabIndex = 4;
            // 
            // dtp_inicio
            // 
            this.dtp_inicio.Location = new System.Drawing.Point(265, 31);
            this.dtp_inicio.Name = "dtp_inicio";
            this.dtp_inicio.Size = new System.Drawing.Size(250, 20);
            this.dtp_inicio.TabIndex = 3;
            // 
            // lbl_usuario
            // 
            this.lbl_usuario.AutoSize = true;
            this.lbl_usuario.BackColor = System.Drawing.Color.Transparent;
            this.lbl_usuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_usuario.Location = new System.Drawing.Point(30, 13);
            this.lbl_usuario.Name = "lbl_usuario";
            this.lbl_usuario.Size = new System.Drawing.Size(50, 13);
            this.lbl_usuario.TabIndex = 2;
            this.lbl_usuario.Text = "Usuário";
            // 
            // btn_run
            // 
            this.btn_run.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_run.Location = new System.Drawing.Point(30, 60);
            this.btn_run.Name = "btn_run";
            this.btn_run.Size = new System.Drawing.Size(152, 23);
            this.btn_run.TabIndex = 1;
            this.btn_run.Text = "Gerar Relatório";
            this.btn_run.UseVisualStyleBackColor = true;
            this.btn_run.Click += new System.EventHandler(this.btn_run_Click);
            // 
            // cb_usuario
            // 
            this.cb_usuario.FormattingEnabled = true;
            this.cb_usuario.Location = new System.Drawing.Point(30, 31);
            this.cb_usuario.Name = "cb_usuario";
            this.cb_usuario.Size = new System.Drawing.Size(152, 21);
            this.cb_usuario.TabIndex = 0;
            // 
            // panel_crystal
            // 
            this.panel_crystal.Controls.Add(this.crystalReportViewer1);
            this.panel_crystal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_crystal.Location = new System.Drawing.Point(0, 0);
            this.panel_crystal.Name = "panel_crystal";
            this.panel_crystal.Size = new System.Drawing.Size(1064, 538);
            this.panel_crystal.TabIndex = 0;
            // 
            // crystalReportViewer1
            // 
            this.crystalReportViewer1.ActiveViewIndex = -1;
            this.crystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crystalReportViewer1.Cursor = System.Windows.Forms.Cursors.Default;
            this.crystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crystalReportViewer1.Location = new System.Drawing.Point(0, 0);
            this.crystalReportViewer1.Name = "crystalReportViewer1";
            this.crystalReportViewer1.Size = new System.Drawing.Size(1064, 538);
            this.crystalReportViewer1.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1064, 662);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Relatório Situação Ordem de Serviço";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel_filtro.ResumeLayout(false);
            this.panel_filtro.PerformLayout();
            this.panel_crystal.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel_filtro;
        private System.Windows.Forms.ComboBox cb_usuario;
        private System.Windows.Forms.Panel panel_crystal;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer crystalReportViewer1;
        private CrystalReport1 CrystalReport11;
        private System.Windows.Forms.Button btn_run;
        private System.Windows.Forms.Label lbl_usuario;
        private System.Windows.Forms.DateTimePicker dtp_fim;
        private System.Windows.Forms.DateTimePicker dtp_inicio;
        private System.Windows.Forms.Label lbl_fim;
        private System.Windows.Forms.Label lbl_inicio;
    }
}

