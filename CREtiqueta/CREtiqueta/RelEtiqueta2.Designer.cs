﻿namespace CREtiqueta
{
    partial class RelEtiqueta2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_gerar = new System.Windows.Forms.Button();
            this.rb_usuario = new System.Windows.Forms.RadioButton();
            this.rb_etiqueta = new System.Windows.Forms.RadioButton();
            this.cb_tipo = new System.Windows.Forms.ComboBox();
            this.cb_utilizado = new System.Windows.Forms.ComboBox();
            this.lbl_tipo = new System.Windows.Forms.Label();
            this.lbl_utilizado = new System.Windows.Forms.Label();
            this.cbx_tipo = new System.Windows.Forms.CheckBox();
            this.cbx_utilizado = new System.Windows.Forms.CheckBox();
            this.lbl_atribuicao = new System.Windows.Forms.Label();
            this.lbl_cadastro = new System.Windows.Forms.Label();
            this.dtp_atribuicao = new System.Windows.Forms.DateTimePicker();
            this.dtp_cadastro = new System.Windows.Forms.DateTimePicker();
            this.lbl_usuario = new System.Windows.Forms.Label();
            this.lbl_numeroEtiqueta = new System.Windows.Forms.Label();
            this.cb_usuario = new System.Windows.Forms.ComboBox();
            this.cb_numeroEtiqueta = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.crystalReportViewer1 = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.dtp_atribuicao2 = new System.Windows.Forms.DateTimePicker();
            this.lbl_atribuicao_ate = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel2);
            this.splitContainer1.Size = new System.Drawing.Size(944, 562);
            this.splitContainer1.SplitterDistance = 180;
            this.splitContainer1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(944, 180);
            this.panel1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbl_atribuicao_ate);
            this.groupBox1.Controls.Add(this.dtp_atribuicao2);
            this.groupBox1.Controls.Add(this.btn_gerar);
            this.groupBox1.Controls.Add(this.rb_usuario);
            this.groupBox1.Controls.Add(this.rb_etiqueta);
            this.groupBox1.Controls.Add(this.cb_tipo);
            this.groupBox1.Controls.Add(this.cb_utilizado);
            this.groupBox1.Controls.Add(this.lbl_tipo);
            this.groupBox1.Controls.Add(this.lbl_utilizado);
            this.groupBox1.Controls.Add(this.cbx_tipo);
            this.groupBox1.Controls.Add(this.cbx_utilizado);
            this.groupBox1.Controls.Add(this.lbl_atribuicao);
            this.groupBox1.Controls.Add(this.lbl_cadastro);
            this.groupBox1.Controls.Add(this.dtp_atribuicao);
            this.groupBox1.Controls.Add(this.dtp_cadastro);
            this.groupBox1.Controls.Add(this.lbl_usuario);
            this.groupBox1.Controls.Add(this.lbl_numeroEtiqueta);
            this.groupBox1.Controls.Add(this.cb_usuario);
            this.groupBox1.Controls.Add(this.cb_numeroEtiqueta);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(919, 160);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filtros";
            // 
            // btn_gerar
            // 
            this.btn_gerar.Location = new System.Drawing.Point(279, 47);
            this.btn_gerar.Name = "btn_gerar";
            this.btn_gerar.Size = new System.Drawing.Size(75, 23);
            this.btn_gerar.TabIndex = 16;
            this.btn_gerar.Text = "Gerar";
            this.btn_gerar.UseVisualStyleBackColor = true;
            this.btn_gerar.Click += new System.EventHandler(this.btn_gerar_Click);
            // 
            // rb_usuario
            // 
            this.rb_usuario.AutoSize = true;
            this.rb_usuario.Checked = true;
            this.rb_usuario.Location = new System.Drawing.Point(7, 94);
            this.rb_usuario.Name = "rb_usuario";
            this.rb_usuario.Size = new System.Drawing.Size(14, 13);
            this.rb_usuario.TabIndex = 15;
            this.rb_usuario.TabStop = true;
            this.rb_usuario.UseVisualStyleBackColor = true;
            this.rb_usuario.CheckedChanged += new System.EventHandler(this.rb_usuario_CheckedChanged);
            // 
            // rb_etiqueta
            // 
            this.rb_etiqueta.AutoSize = true;
            this.rb_etiqueta.Location = new System.Drawing.Point(7, 51);
            this.rb_etiqueta.Name = "rb_etiqueta";
            this.rb_etiqueta.Size = new System.Drawing.Size(14, 13);
            this.rb_etiqueta.TabIndex = 14;
            this.rb_etiqueta.Tag = "etiqueta";
            this.rb_etiqueta.UseVisualStyleBackColor = true;
            this.rb_etiqueta.CheckedChanged += new System.EventHandler(this.rb_etiqueta_CheckedChanged);
            // 
            // cb_tipo
            // 
            this.cb_tipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_tipo.FormattingEnabled = true;
            this.cb_tipo.Location = new System.Drawing.Point(745, 91);
            this.cb_tipo.Name = "cb_tipo";
            this.cb_tipo.Size = new System.Drawing.Size(121, 21);
            this.cb_tipo.Sorted = true;
            this.cb_tipo.TabIndex = 13;
            // 
            // cb_utilizado
            // 
            this.cb_utilizado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_utilizado.FormattingEnabled = true;
            this.cb_utilizado.Location = new System.Drawing.Point(746, 47);
            this.cb_utilizado.Name = "cb_utilizado";
            this.cb_utilizado.Size = new System.Drawing.Size(121, 21);
            this.cb_utilizado.Sorted = true;
            this.cb_utilizado.TabIndex = 12;
            // 
            // lbl_tipo
            // 
            this.lbl_tipo.AutoSize = true;
            this.lbl_tipo.Location = new System.Drawing.Point(729, 76);
            this.lbl_tipo.Name = "lbl_tipo";
            this.lbl_tipo.Size = new System.Drawing.Size(32, 13);
            this.lbl_tipo.TabIndex = 11;
            this.lbl_tipo.Text = "Tipo";
            // 
            // lbl_utilizado
            // 
            this.lbl_utilizado.AutoSize = true;
            this.lbl_utilizado.Location = new System.Drawing.Point(729, 28);
            this.lbl_utilizado.Name = "lbl_utilizado";
            this.lbl_utilizado.Size = new System.Drawing.Size(56, 13);
            this.lbl_utilizado.TabIndex = 10;
            this.lbl_utilizado.Text = "Utilizado";
            // 
            // cbx_tipo
            // 
            this.cbx_tipo.AutoSize = true;
            this.cbx_tipo.Checked = true;
            this.cbx_tipo.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbx_tipo.Location = new System.Drawing.Point(729, 94);
            this.cbx_tipo.Name = "cbx_tipo";
            this.cbx_tipo.Size = new System.Drawing.Size(15, 14);
            this.cbx_tipo.TabIndex = 9;
            this.cbx_tipo.UseVisualStyleBackColor = true;
            // 
            // cbx_utilizado
            // 
            this.cbx_utilizado.AutoSize = true;
            this.cbx_utilizado.Checked = true;
            this.cbx_utilizado.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbx_utilizado.Location = new System.Drawing.Point(729, 50);
            this.cbx_utilizado.Name = "cbx_utilizado";
            this.cbx_utilizado.Size = new System.Drawing.Size(15, 14);
            this.cbx_utilizado.TabIndex = 8;
            this.cbx_utilizado.UseVisualStyleBackColor = true;
            // 
            // lbl_atribuicao
            // 
            this.lbl_atribuicao.AutoSize = true;
            this.lbl_atribuicao.Location = new System.Drawing.Point(431, 75);
            this.lbl_atribuicao.Name = "lbl_atribuicao";
            this.lbl_atribuicao.Size = new System.Drawing.Size(142, 13);
            this.lbl_atribuicao.TabIndex = 7;
            this.lbl_atribuicao.Text = "Data de Atribuição - DE";
            // 
            // lbl_cadastro
            // 
            this.lbl_cadastro.AutoSize = true;
            this.lbl_cadastro.Location = new System.Drawing.Point(431, 29);
            this.lbl_cadastro.Name = "lbl_cadastro";
            this.lbl_cadastro.Size = new System.Drawing.Size(106, 13);
            this.lbl_cadastro.TabIndex = 6;
            this.lbl_cadastro.Text = "Data de Cadastro";
            // 
            // dtp_atribuicao
            // 
            this.dtp_atribuicao.Location = new System.Drawing.Point(431, 91);
            this.dtp_atribuicao.Name = "dtp_atribuicao";
            this.dtp_atribuicao.ShowCheckBox = true;
            this.dtp_atribuicao.Size = new System.Drawing.Size(270, 20);
            this.dtp_atribuicao.TabIndex = 5;
            // 
            // dtp_cadastro
            // 
            this.dtp_cadastro.Location = new System.Drawing.Point(431, 48);
            this.dtp_cadastro.Name = "dtp_cadastro";
            this.dtp_cadastro.ShowCheckBox = true;
            this.dtp_cadastro.Size = new System.Drawing.Size(270, 20);
            this.dtp_cadastro.TabIndex = 4;
            // 
            // lbl_usuario
            // 
            this.lbl_usuario.AutoSize = true;
            this.lbl_usuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_usuario.ForeColor = System.Drawing.Color.Blue;
            this.lbl_usuario.Location = new System.Drawing.Point(20, 76);
            this.lbl_usuario.Name = "lbl_usuario";
            this.lbl_usuario.Size = new System.Drawing.Size(50, 13);
            this.lbl_usuario.TabIndex = 3;
            this.lbl_usuario.Text = "Usuário";
            // 
            // lbl_numeroEtiqueta
            // 
            this.lbl_numeroEtiqueta.AutoSize = true;
            this.lbl_numeroEtiqueta.Enabled = false;
            this.lbl_numeroEtiqueta.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_numeroEtiqueta.Location = new System.Drawing.Point(20, 29);
            this.lbl_numeroEtiqueta.Name = "lbl_numeroEtiqueta";
            this.lbl_numeroEtiqueta.Size = new System.Drawing.Size(119, 13);
            this.lbl_numeroEtiqueta.TabIndex = 2;
            this.lbl_numeroEtiqueta.Text = "Número de Etiqueta";
            // 
            // cb_usuario
            // 
            this.cb_usuario.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_usuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_usuario.FormattingEnabled = true;
            this.cb_usuario.Location = new System.Drawing.Point(25, 91);
            this.cb_usuario.Name = "cb_usuario";
            this.cb_usuario.Size = new System.Drawing.Size(350, 21);
            this.cb_usuario.Sorted = true;
            this.cb_usuario.TabIndex = 1;
            this.cb_usuario.Tag = "usuario";
            // 
            // cb_numeroEtiqueta
            // 
            this.cb_numeroEtiqueta.Enabled = false;
            this.cb_numeroEtiqueta.FormattingEnabled = true;
            this.cb_numeroEtiqueta.IntegralHeight = false;
            this.cb_numeroEtiqueta.Location = new System.Drawing.Point(25, 48);
            this.cb_numeroEtiqueta.Name = "cb_numeroEtiqueta";
            this.cb_numeroEtiqueta.Size = new System.Drawing.Size(150, 21);
            this.cb_numeroEtiqueta.Sorted = true;
            this.cb_numeroEtiqueta.TabIndex = 0;
            this.cb_numeroEtiqueta.SelectedIndexChanged += new System.EventHandler(this.cb_numeroEtiqueta_SelectedIndexChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.crystalReportViewer1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(944, 378);
            this.panel2.TabIndex = 0;
            // 
            // crystalReportViewer1
            // 
            this.crystalReportViewer1.ActiveViewIndex = -1;
            this.crystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crystalReportViewer1.Cursor = System.Windows.Forms.Cursors.Default;
            this.crystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crystalReportViewer1.Location = new System.Drawing.Point(0, 0);
            this.crystalReportViewer1.Name = "crystalReportViewer1";
            this.crystalReportViewer1.Size = new System.Drawing.Size(944, 378);
            this.crystalReportViewer1.TabIndex = 0;
            // 
            // dtp_atribuicao2
            // 
            this.dtp_atribuicao2.Location = new System.Drawing.Point(431, 131);
            this.dtp_atribuicao2.Name = "dtp_atribuicao2";
            this.dtp_atribuicao2.ShowCheckBox = true;
            this.dtp_atribuicao2.Size = new System.Drawing.Size(270, 20);
            this.dtp_atribuicao2.TabIndex = 17;
            // 
            // lbl_atribuicao_ate
            // 
            this.lbl_atribuicao_ate.AutoSize = true;
            this.lbl_atribuicao_ate.Location = new System.Drawing.Point(431, 114);
            this.lbl_atribuicao_ate.Name = "lbl_atribuicao_ate";
            this.lbl_atribuicao_ate.Size = new System.Drawing.Size(149, 13);
            this.lbl_atribuicao_ate.TabIndex = 18;
            this.lbl_atribuicao_ate.Text = "Data de Atribuição - ATÉ";
            // 
            // RelEtiqueta2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(944, 562);
            this.Controls.Add(this.splitContainer1);
            this.Name = "RelEtiqueta2";
            this.Text = "Relatório de Etiquetas";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer crystalReportViewer1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker dtp_atribuicao;
        private System.Windows.Forms.DateTimePicker dtp_cadastro;
        private System.Windows.Forms.Label lbl_usuario;
        private System.Windows.Forms.Label lbl_numeroEtiqueta;
        private System.Windows.Forms.ComboBox cb_usuario;
        private System.Windows.Forms.ComboBox cb_numeroEtiqueta;
        private System.Windows.Forms.Label lbl_atribuicao;
        private System.Windows.Forms.Label lbl_cadastro;
        private System.Windows.Forms.Label lbl_tipo;
        private System.Windows.Forms.Label lbl_utilizado;
        private System.Windows.Forms.CheckBox cbx_tipo;
        private System.Windows.Forms.CheckBox cbx_utilizado;
        private System.Windows.Forms.RadioButton rb_usuario;
        private System.Windows.Forms.RadioButton rb_etiqueta;
        private System.Windows.Forms.ComboBox cb_tipo;
        private System.Windows.Forms.ComboBox cb_utilizado;
        private System.Windows.Forms.Button btn_gerar;
        private System.Windows.Forms.Label lbl_atribuicao_ate;
        private System.Windows.Forms.DateTimePicker dtp_atribuicao2;
    }
}

