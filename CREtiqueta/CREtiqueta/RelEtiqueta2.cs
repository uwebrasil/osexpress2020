﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TExpress;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;


namespace CREtiqueta
{
    public partial class RelEtiqueta2 : Form
    {
        public bool numero_selecionado = false;
        public RelEtiqueta2()
        {
            InitializeComponent();
            //DB.ConnectionName = "OsExpressConnection";//osExpress.Properties.Settings.OsExpressConnectionString
            DB.ConnectionName = "osExpress.Properties.Settings.OsExpressConnectionString";
            dtp_cadastro.Checked = false;
        }
       
        private void Form1_Load(object sender, EventArgs e)
        {   
            string sql =
                               " select 'Todos os Usuários' val UNION select u_nome+' ('+f.nome+')' val from "
                               + " ("
                               + " select e.idusuario  , u.nome as u_nome, u.IDFUNCIONARIO "
                               + " from (select idusuario from OsEtiqueta group by idusuario) e "
                               + " inner join OsUsuario u on (e.IDUSUARIO = u.id) "
                               + " ) A "
                               + " left outer join A30FUNCIONARIO f "
                               + "on (A.IDFUNCIONARIO = f.id) ";      
            
            DB.FillComboBox(cb_usuario,sql);           
            cb_usuario.SelectedIndex = cb_usuario.FindStringExact("Todos os Usuários");
            
            sql = "select '' val UNION select num_etiqueta val from osEtiqueta";
            DB.FillComboBox(cb_numeroEtiqueta, sql);
            sql = "select 'Tudo' val UNION select case tipo when 'L' then 'Lacre' else 'Selo' end val from osEtiqueta group by tipo";
            DB.FillComboBox(cb_tipo, sql);
            sql = "select 'Tudo' val UNION select case utilizado when 0 then 'Nao' else 'Sim' end val from osEtiqueta group by utilizado";
            DB.FillComboBox(cb_utilizado, sql);

            cb_tipo.SelectedIndex = cb_tipo.FindStringExact("Tudo");
            cb_utilizado.SelectedIndex = cb_utilizado.FindStringExact("Tudo");

            // disable GroupView
            crystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            crystalReportViewer1.ShowGroupTreeButton = false;
        }

        private string getNome(string num_etiqueta) 
        {
            return DB.GetSingleValue(@"select nome from OsEtiqueta e, 
                                                        OsUsuario  u
                                        where e.IDUSUARIO = u.ID
                                          and e.NUM_ETIQUETA = '" + num_etiqueta + "'").ToString();
        }
        private void RunReport()
        {
            // entregar parâmetro ao crystal
            ReportDocument reportDocument = new ReportDocument();

            ParameterFields paramFields = new ParameterFields();
            ParameterField  paramField  = new ParameterField();
            ParameterDiscreteValue paramDiscreteValue = new ParameterDiscreteValue();

            paramField.Name = "@USER";
            paramDiscreteValue.Value = cb_usuario.SelectedItem.ToString().ToLower().Substring(0, 5);
            // modi 
            // caso tenha optado pela escolha "numero de etiqueta" procurar o nome do usuário 
            if (numero_selecionado)
                if ( cb_numeroEtiqueta.Text != null)
                    paramDiscreteValue.Value = getNome(cb_numeroEtiqueta.Text);
            
            paramField.CurrentValues.Add(paramDiscreteValue);
            paramFields.Add(paramField);
            // este serve para a
            crystalReportViewer1.ParameterFieldInfo = paramFields;

            string query = createQuery();
            if (query.Equals(string.Empty)) return;

            CrystalReport1 report = new CrystalReport1();

            DataSet1TableAdapters.DataTable1TableAdapter ta
                = new DataSet1TableAdapters.DataTable1TableAdapter();

            SqlConnection conn = new SqlConnection(DB.GetConnectionString());
            SqlDataAdapter Adapter = new SqlDataAdapter("", conn);
            Adapter.SelectCommand.CommandType = CommandType.Text;
            Adapter.SelectCommand.CommandText = query;

            DataSet1.DataTable1DataTable table = new DataSet1.DataTable1DataTable();
            Adapter.Fill(table);

            try
            {
                report.SetDataSource(table.DefaultView);
            }
            catch (System.Runtime.InteropServices.COMException icomex)
            {
                MessageBox.Show(icomex.Message + "\nFalha ao carregar o relatório.", "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            crystalReportViewer1.ReportSource = report;

            // set zoom
            crystalReportViewer1.Zoom(88);
            crystalReportViewer1.Refresh();
             
        }

        private string createQuery()
        {
            
         string sql = @" select NUM_ETIQUETA,TIPO,DT_ATRIBUICAO,abc.DT_CADASTRO,ois.IDORDEM,oos.ID_FINAL,
                     Utilizado,
                     U_ID,E_ID,NOME
                     from (
                     SELECT 
                     OsEtiqueta.DT_ATRIBUICAO,OsEtiqueta.DT_CADASTRO,
                     OsUsuario.ID U_ID, OsUsuario.NOME, OsEtiqueta.NUM_ETIQUETA, 
                     OsEtiqueta.UTILIZADO, OsEtiqueta.ID E_ID,
                     case OsEtiqueta.TIPO when 'S' then 'Selo' when 'L' then 'Lacre' end as TIPO  
                     FROM OsExpress.dbo.OsEtiqueta OsEtiqueta 
                     INNER JOIN OsExpress.dbo.OsUsuario OsUsuario  
                     ON OsEtiqueta.IDUSUARIO=OsUsuario.ID 
                     ) abc
                     left outer join OsInstrumentoServico ois
                     on abc.E_ID = ois.IDETIQUETA 
                     left outer join OsOrdemServico oos
                     on ois.IDORDEM = oos.ID WHERE 1=1 ";
                    //+ usuario + dataCadastro + dataAtribuicao + utilizados;

            // ETIQUETA 
            if (rb_etiqueta.Checked)
            {
                string selectedItem = string.Empty;
                if (cb_numeroEtiqueta.SelectedItem == null)
                {
                   // MessageBox.Show("Por favor, escolha etiqueta!","AVISO",MessageBoxButtons.OK,MessageBoxIcon.Information);
                   // return string.Empty;
                    selectedItem = cb_numeroEtiqueta.Text;
                }
                else
                  selectedItem = cb_numeroEtiqueta.SelectedItem.ToString();

                sql += " AND num_etiqueta = '" + selectedItem + "'";
                return sql;
            }

            // USUARIO
            if (rb_usuario.Checked)
            {
                string sItem = cb_usuario.SelectedItem.ToString();
                if (!(sItem.Substring(0,5).ToLower().Equals("todos")))
                {
                    sql += " AND nome = '" + sItem.Split('(')[0].Trim() + "'";
                }
            }
            // DATA_ATRIBUICAO
            if (dtp_atribuicao.Checked)
            {
                //sql += " AND dt_atribuicao = CONVERT(date,CONVERT(varchar,'" + dtp_atribuicao.Value.ToShortDateString() + "',103),103) ";
                sql += " AND dt_atribuicao >= CONVERT(date,CONVERT(varchar,'" + dtp_atribuicao.Value.ToShortDateString() + "',103),103) ";
                sql += " AND dt_atribuicao <= CONVERT(date,CONVERT(varchar,'" + dtp_atribuicao2.Value.ToShortDateString() + "',103),103) ";
            }
            // DATA_CADASTRO
            if (dtp_cadastro.Checked)
            {
                sql += " AND abc.dt_cadastro = CONVERT(date,CONVERT(varchar,'" + dtp_cadastro.Value.ToShortDateString() + "',103),103) ";
            }

            // TIPO
            if (cbx_tipo.Checked && !cb_tipo.SelectedItem.ToString().ToLower().Equals("tudo"))
            {
                sql += " AND tipo ='" + cb_tipo.SelectedItem.ToString() + "'";
            }
            // UTILIZADO
            if (cbx_utilizado.Checked && !cb_utilizado.SelectedItem.ToString().ToLower().Equals("tudo"))
            {
                string utilizado = cb_utilizado.SelectedItem.ToString().ToLower().Substring(0, 1);
                utilizado = utilizado.Equals("n") ? "0" : "1";
                sql += " AND utilizado ='" + utilizado + "'";
            }
            // modificação 12.02.2014 para tirar dubleta heberth:D93407524
            sql += @" group by NUM_ETIQUETA,TIPO,DT_ATRIBUICAO,abc.DT_CADASTRO,ois.IDORDEM,oos.ID_FINAL,
                     Utilizado,
                     U_ID,E_ID,NOME";
            sql += " ORDER BY NUM_ETIQUETA,TIPO,DT_ATRIBUICAO,abc.DT_CADASTRO,ois.IDORDEM,oos.ID_FINAL";
            return sql;

        }

        private void rb_etiqueta_CheckedChanged(object sender, EventArgs e)
        {
            foreach (Control c in groupBox1.Controls)
            {
                if (!(c is RadioButton) )
                   c.Enabled = false;
            }
            cb_numeroEtiqueta.Enabled  = true;
            lbl_numeroEtiqueta.Enabled = true;
            lbl_numeroEtiqueta.ForeColor = Color.Blue;
            lbl_usuario.ForeColor = Color.Black;
            btn_gerar.Enabled = true;
            numero_selecionado = true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void rb_usuario_CheckedChanged(object sender, EventArgs e)
        {
            numero_selecionado = false;
            foreach (Control c in groupBox1.Controls)
            {              
                    c.Enabled = true;
            }
            cb_numeroEtiqueta.Enabled  = false;
            lbl_numeroEtiqueta.Enabled = false;
            lbl_numeroEtiqueta.ForeColor = Color.Black;
            lbl_usuario.ForeColor = Color.Blue;
            cb_numeroEtiqueta.SelectedIndex = 0;// cb_numeroEtiqueta.Items.Count - 1;
            if (cb_numeroEtiqueta.Items.Count > 0)
                cb_numeroEtiqueta.SelectedIndex = 0;
        }

        private void btn_gerar_Click(object sender, EventArgs e)
        {                         
            RunReport();
        }

        private void cb_numeroEtiqueta_SelectedIndexChanged(object sender, EventArgs e)
        {
            btn_gerar.PerformClick();
        }

    }
}
